FROM php:7.1-fpm

# Install PHP 7 Repo
RUN apt-get update && apt-get install -my wget gnupg

# Install global system packages
RUN apt-get update && apt-get install -my \
    bzip2 \
    ca-certificates \
    curl \
    git \
    gnupg \
    libatk-bridge2.0.0 \
    libfontconfig \
    libfreetype6-dev \
    libgtk-3.0 \
    libjpeg62-turbo-dev \
    libmagickwand-dev \
    libmemcached-dev \
    libmcrypt-dev \
    libxml2-dev \
    nano \
    wget \
    zlib1g-dev

# Please install manually:
# libpng12-dev \ (mainline Docker)
# libpng-dev \ (edge Docker)

# OLD:
# chromium \

# Install Node.js 8.x
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash \
    && apt-get install -y nodejs\
    && mkdir -p /var/www/.config \
    && chmod 777 -R /var/www/.config

# Install common PHP extensions
RUN docker-php-ext-install -j$(nproc) iconv mcrypt mysqli pdo pdo_mysql soap zip exif

# Install `gd` PHP extension
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

# Install `memcached` PHP extension
RUN pecl install memcached-stable \
    && docker-php-ext-enable memcached

# Install `imagick` PHP extension
RUN pecl install imagick \
    && docker-php-ext-enable imagick

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy PHP configuration file
COPY php.ini /usr/local/etc/php/

# Copy Bash profiles
COPY .bashrc.global /etc/bash.bashrc
COPY .bashrc.root /root/.bashrc
