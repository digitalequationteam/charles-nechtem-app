namespace :laravel do

    # :key
    namespace :key do

        desc "Generate encryption key"
        task :generate do
            on roles fetch(:laravel_roles) do
                within release_path do
                    invoke "laravel:artisan", "key:generate"
                end
            end
        end

    end
    # END :key

end
