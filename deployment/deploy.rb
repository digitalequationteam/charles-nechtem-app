lock '3.10.2'

set :application,   'charles-nechtem-app'
set :repo_url,      'git@bitbucket.org:digitalequationteam/charles-nechtem-app.git'
set :repo_tree,     'app'
set :keep_releases, 3

set :npm_target_path, -> { fetch(:release_path) }

set :laravel_roles, :all
set :laravel_artisan_flags, "--env=#{fetch(:stage)}"
set :laravel_migration_roles, :all
set :laravel_migration_artisan_flags, "--force --env=#{fetch(:stage)}"
set :laravel_version, 5.6
set :laravel_upload_dotenv_file_on_deploy, true
set :laravel_server_user, 'www-data'
set :laravel_ensure_linked_dirs_exist, true
set :laravel_set_linked_dirs, true
set :laravel_5_linked_dirs, [
    'public/storage'
]
set :laravel_5_acl_paths, [
    'bootstrap/cache',
    'public/storage',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs'
]

set :slackistrano, {
    klass: Slackistrano::SlackMessaging,
    channel: '#deployment',
    webhook: 'https://hooks.slack.com/services/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',
    username: 'Capistrano',
    icon_url: 'https://pbs.twimg.com/profile_images/378800000067686459/5da4e1d78e930197cb7dc002ceafdfda.png'
}

def red(str)
    "\e[31m#{str}\e[0m"
end

def current_git_branch
    proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call
end

def git_short_remote_hash(branch)
    proc { `git rev-parse --short origin/#{branch}`.chomp }.call
end

Rake::Task['deploy:reverted'].prerequisites.delete('composer:install')

namespace :deploy do
    after :starting, :show_branch do
        puts "Deploying #{red(fetch :branch)}"
    end

    after :starting, 'composer:install_executable'

    after :publishing, 'laravel:migrate'

    # after :finished, 'opcache:reset'
    after :finished, 'php:restart'
end
