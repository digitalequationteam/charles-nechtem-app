/**
License editable input.
Internally value stored as {city: "Moscow", street: "Lenina", building: "15"}

@class license
@extends abstractinput
@final
@example
<a href="#" id="license" data-type="license" data-pk="1">awesome</a>
<script>
$(function(){
    $('#license').editable({
        url: '/post',
        title: 'Enter city, street and building #',
        value: {
            city: "Moscow", 
            street: "Lenina", 
            building: "15"
        }
    });
});
</script>
**/
(function ($) {
    "use strict";
    
    var License = function (options) {
        this.init('license', options, License.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(License, $.fn.editabletypes.abstractinput);

    $.extend(License.prototype, {
        /**
        Renders input from tpl

        @method render() 
        **/        
        render: function() {
           this.$input = this.$tpl.find('input');
           this.$select = this.$tpl.find('select');
        },
        
        /**
        Default method to show value in element. Can be overwritten by display option.
        
        @method value2html(value, element) 
        **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }
            var html = '<a href="#" class="btn btn-sm pull-right btn-danger remove-license-btn"><i class="fa fa-times"></i></a>' + value.title;

            $(element).html(html);

            var parent = $(element).parents('.license-item');

            parent.find('input.number').val(value.number);
            parent.find('input.title').val(value.title);
            parent.find('input.expiration_date').val(value.expiration_date);
        },
        
        /**
        Gets value from element's html
        
        @method html2value(html) 
        **/        
        html2value: function(html) {        
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g. 
            editable({
                value: {
                    city: "Moscow", 
                    street: "Lenina", 
                    building: "15"
                }
            });
          */ 
          return null;  
        },
      
       /**
        Converts value to string. 
        It is used in internal comparing (not for sending to server).
        
        @method value2str(value)  
       **/
       value2str: function(value) {
           var str = '';
           if(value) {
               for(var k in value) {
                   str = str + k + ':' + value[k] + ';';  
               }
           }
           return str;
       }, 
       
       /*
        Converts string to value. Used for reading value from 'data-value' attribute.
        
        @method str2value(str)  
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute. 
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },                
       
       /**
        Sets value of input.
        
        @method value2input(value) 
        @param {mixed} value
       **/         
       value2input: function(value) {
           if(!value) {
             return;
           }
           this.$input.filter('[name="number"]').val(value.number);
           this.$select.filter('[name="title"]').val(value.title);
           this.$input.filter('[name="expiration_date"]').val(value.expiration_date);
       },       
       
       /**
        Returns value of input.
        
        @method input2value() 
       **/          
       input2value: function() { 
           return {
              number: this.$input.filter('[name="number"]').val(), 
              title: this.$select.filter('[name="title"]').val(), 
              expiration_date: this.$input.filter('[name="expiration_date"]').val()
           };
       },        
       
        /**
        Activates input: sets focus on the first field.
        
        @method activate() 
       **/        
       activate: function() {
            this.$input.filter('[name="number"]').focus();
       },  
       
       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode
        
        @method autosubmit() 
       **/       
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }       
    });

    License.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-license"><label><span>Number: </span><input type="text" name="number" class="form-control" style="width: 200px;"></label></div>\
        <div class="editable-license"><label><span>Title: </span><select class="form-control" name="title" style="width: 200px;">\
    <option value="">None</option>\
    <option value="APRN">APRN</option>\
    <option value="EdD">EdD</option>\
    <option value="LCSW">LCSW</option>\
    <option value="LICSW">LICSW</option>\
    <option value="LMFT">LMFT</option>\
    <option value="LMHC">LMHC</option>\
    <option value="LMSW">LMSW</option>\
    <option value="LPC">LPC</option>\
    <option value="MD">MD</option>\
    <option value="PhD">PhD</option>\
    <option value="PMHNP">PMHNP</option>\
    <option value="PsyD">PsyD</option>\
    <option value="Other">Other</option>\
  </select>\
		    </label></div>\
        <div class="editable-license"><label><span>Expiration Date: </span><input type="date" name="expiration_date" class="form-control" style="width: 200px;"></label></div>',
             
        inputclass: ''
    });

    $.fn.editabletypes.license = License;

    $(".remove-license-btn").live("click", function(e) {
    	e.preventDefault();
    	
    	$(this).parents(".license-item").remove();
    });

}(window.jQuery));