/**
Address editable input.
Internally value stored as {city: "Moscow", street: "Lenina", building: "15"}

@class address
@extends abstractinput
@final
@example
<a href="#" id="address" data-type="address" data-pk="1">awesome</a>
<script>
$(function(){
    $('#address').editable({
        url: '/post',
        title: 'Enter city, street and building #',
        value: {
            city: "Moscow", 
            street: "Lenina", 
            building: "15"
        }
    });
});
</script>
**/
(function ($) {
    "use strict";
    
    var Address = function (options) {
        this.init('address', options, Address.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);

    $.extend(Address.prototype, {
        /**
        Renders input from tpl

        @method render() 
        **/        
        render: function(value) {
           this.$input = this.$tpl.find('input');
           this.$select = this.$tpl.find('select');

           this.$input.parents('.address-item').find("#country").trigger("change");

           //$("select").select2();
        },
        
        /**
        Default method to show value in element. Can be overwritten by display option.
        
        @method value2html(value, element) 
        **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }

            var html = value.street + '<br />' + value.city + ', ' + value.state + ', ' + value.zip;
            $(element).html(html);

            var parent = $(element).parents('.address-item');

            parent.find('input.street').val(value.street);
            parent.find('input.city').val(value.city);
            parent.find('input.country').val(value.country);
            parent.find('input.state').val(value.state);
            parent.find('input.zip').val(value.zip);
        },
        
        /**
        Gets value from element's html
        
        @method html2value(html) 
        **/        
        html2value: function(html) {        
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g. 
            editable({
                value: {
                    city: "Moscow", 
                    street: "Lenina", 
                    building: "15"
                }
            });
          */ 
          return null;  
        },
      
       /**
        Converts value to string. 
        It is used in internal comparing (not for sending to server).
        
        @method value2str(value)  
       **/
       value2str: function(value) {
           var str = '';
           if(value) {
               for(var k in value) {
                   str = str + k + ':' + value[k] + ';';  
               }
           }
           return str;
       }, 
       
       /*
        Converts string to value. Used for reading value from 'data-value' attribute.
        
        @method str2value(str)  
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute. 
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },                
       
       /**
        Sets value of input.
        
        @method value2input(value) 
        @param {mixed} value
       **/         
       value2input: function(value) {
           if(!value) {
             return;
           }
           this.$input.filter('[name="street"]').val(value.street);
           this.$input.filter('[name="city"]').val(value.city);
           this.$select.filter('[name="country"]').val(value.country);
           this.$input.filter('[name="zip"]').val(value.zip);

           var that = this;
           setTimeout(function() {
	           that.$select.filter('[name="state"]').val(value.state);
	       }, 500);
       },       
       
       /**
        Returns value of input.
        
        @method input2value() 
       **/          
       input2value: function() { 
           return {
              street: this.$input.filter('[name="street"]').val(), 
              city: this.$input.filter('[name="city"]').val(), 
              country: this.$select.filter('[name="country"]').val(), 
              state: this.$select.filter('[name="state"]').val(), 
              zip: this.$input.filter('[name="zip"]').val()
           };
       },        
       
        /**
        Activates input: sets focus on the first field.
        
        @method activate() 
       **/        
       activate: function() {
            this.$input.filter('[name="first_name"]').focus();
       },  
       
       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode
        
        @method autosubmit() 
       **/       
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }       
    });

    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-address address_holder"><label><span>Street: </span><input type="text" name="street" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-address"><label><span>City: </span><input type="text" name="city" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-address"><label><span>State: </span><select id="state" name="state" class="form-control" style="width: 200px;"></select></label></div></div>'+
        '<div class="editable-address"><label><span>Zip Code: </span><input type="text" name="zip" class="form-control" style="width: 200px;"></label></div>' +
        '<div class=""><div class="editable-address"><label><span>Country: </span><select id="country" name="country" class="form-control" style="width: 200px;">\
        <option value="US">United States</option>\
        <option value="CA">Canada</option>\
      </select></label></div>\
      ',
             
        inputclass: ''
    });

    $.fn.editabletypes.address = Address;

    $(".remove-address-btn").live("click", function(e) {
    	e.preventDefault();
    	
    	$(this).parents(".address-item").remove();
    });

}(window.jQuery));