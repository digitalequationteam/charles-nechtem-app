/**
Dependent editable input.
Internally value stored as {city: "Moscow", street: "Lenina", building: "15"}

@class dependent
@extends abstractinput
@final
@example
<a href="#" id="dependent" data-type="dependent" data-pk="1">awesome</a>
<script>
$(function(){
    $('#dependent').editable({
        url: '/post',
        title: 'Enter city, street and building #',
        value: {
            city: "Moscow", 
            street: "Lenina", 
            building: "15"
        }
    });
});
</script>
**/
(function ($) {
    "use strict";
    
    var Dependent = function (options) {
        this.init('dependent', options, Dependent.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(Dependent, $.fn.editabletypes.abstractinput);

    $.extend(Dependent.prototype, {
        /**
        Renders input from tpl

        @method render() 
        **/        
        render: function(value) {
           this.$input = this.$tpl.find('input');
           this.$select = this.$tpl.find('select');

           this.$input.parents('.dependent-item').find("#country").trigger("change");
        },
        
        /**
        Default method to show value in element. Can be overwritten by display option.
        
        @method value2html(value, element) 
        **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }

            var html = '<a href="#" class="btn btn-sm pull-right btn-danger remove-dependent-btn"><i class="fa fa-times"></i></a>' + value.first_name + ' ' + value.last_name + ' (' + value.dependent_status + ')';
            $(element).html(html);

            var parent = $(element).parents('.dependent-item');

            parent.find('input.first_name').val(value.first_name);
			parent.find('input.last_name').val(value.last_name);
			parent.find('input.gender').val(value.gender);
			parent.find('input.cell_phone').val(value.cell_phone);
			parent.find('input.home_phone').val(value.home_phone);
			parent.find('input.work_phone').val(value.work_phone);
			parent.find('input.birthday').val(value.birthday);
			parent.find('input.dependent_status').val(value.dependent_status);
			parent.find('input.phone').val(value.phone);
			parent.find('input.cell_phone').val(value.cell_phone);
			parent.find('input.additional_info').val(value.additional_info);
			parent.find('input.street').val(value.street);
			parent.find('input.city').val(value.city);
			parent.find('input.country').val(value.country);
			parent.find('input.state').val(value.state);
			parent.find('input.zip').val(value.zip);
        },
        
        /**
        Gets value from element's html
        
        @method html2value(html) 
        **/        
        html2value: function(html) {        
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g. 
            editable({
                value: {
                    city: "Moscow", 
                    street: "Lenina", 
                    building: "15"
                }
            });
          */ 
          return null;  
        },
      
       /**
        Converts value to string. 
        It is used in internal comparing (not for sending to server).
        
        @method value2str(value)  
       **/
       value2str: function(value) {
           var str = '';
           if(value) {
               for(var k in value) {
                   str = str + k + ':' + value[k] + ';';  
               }
           }
           return str;
       }, 
       
       /*
        Converts string to value. Used for reading value from 'data-value' attribute.
        
        @method str2value(str)  
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute. 
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },                
       
       /**
        Sets value of input.
        
        @method value2input(value) 
        @param {mixed} value
       **/         
       value2input: function(value) {
           if(!value) {
             return;
           }

			this.$input.filter('[name="first_name"]').val(value.first_name);
			this.$input.filter('[name="last_name"]').val(value.last_name);
			this.$select.filter('[name="gender"]').val(value.gender);
			this.$input.filter('[name="cell_phone"]').val(value.cell_phone);
			this.$input.filter('[name="home_phone"]').val(value.home_phone);
			this.$input.filter('[name="work_phone"]').val(value.work_phone);
			this.$input.filter('[name="birthday"]').val(value.birthday);
			this.$select.filter('[name="dependent_status"]').val(value.dependent_status);
			this.$input.filter('[name="phone"]').val(value.phone);
			this.$input.filter('[name="cell_phone"]').val(value.cell_phone);
			this.$input.filter('[name="additional_info"]').val(value.additional_info);
			this.$input.filter('[name="street"]').val(value.street);
			this.$input.filter('[name="city"]').val(value.city);
			this.$select.filter('[name="country"]').val(value.country);
			this.$input.filter('[name="zip"]').val(value.zip);

           var that = this;
           setTimeout(function() {
	           that.$select.filter('[name="state"]').val(value.state);
	       }, 500);
       },       
       
       /**
        Returns value of input.
        
        @method input2value() 
       **/          
       input2value: function() { 
           return {
				first_name: this.$input.filter('[name="first_name"]').val(),
				last_name: this.$input.filter('[name="last_name"]').val(),
				gender: this.$select.filter('[name="gender"]').val(),
				cell_phone: this.$input.filter('[name="cell_phone"]').val(),
				home_phone: this.$input.filter('[name="home_phone"]').val(),
				work_phone: this.$input.filter('[name="work_phone"]').val(),
				birthday: this.$input.filter('[name="birthday"]').val(),
				dependent_status: this.$select.filter('[name="dependent_status"]').val(),
				phone: this.$input.filter('[name="phone"]').val(),
				cell_phone: this.$input.filter('[name="cell_phone"]').val(),
				additional_info: this.$input.filter('[name="additional_info"]').val(),
				street: this.$input.filter('[name="street"]').val(),
				city: this.$input.filter('[name="city"]').val(),
				country: this.$select.filter('[name="country"]').val(),
				state: this.$select.filter('[name="state"]').val(),
				zip: this.$input.filter('[name="zip"]').val(),
           };
       },        
       
        /**
        Activates input: sets focus on the first field.
        
        @method activate() 
       **/        
       activate: function() {
            this.$input.filter('[name="first_name"]').focus();
       },  
       
       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode
        
        @method autosubmit() 
       **/       
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }       
    });

    Dependent.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: 
        '<div class="editable-dependent"><label><span>First Name: </span><input type="text" name="first_name" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-dependent"><label><span>Last Name: </span><input type="text" name="last_name" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-dependent"><label><span>Gender: </span><select name="gender" class="form-control" style="width: 200px;"><option value="Male">Male</option><option value="Female">Female</option></select></label></div>'+
        '<div class="editable-dependent"><label><span>Cell Phone: </span><input type="text" name="cell_phone" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-dependent"><label><span>Home Phone: </span><input type="text" name="home_phone" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-dependent"><label><span>Work Phone: </span><input type="text" name="work_phone" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-dependent"><label><span>Birthday: </span><input type="date" name="birthday" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-dependent"><label><span>Status: </span><select name="dependent_status" class="form-control" style="width: 200px;"><option value="Spouse">Spouse</option><option value="Child">Child</option><option value="Other">Other</option></select></label></div>'+

        '<div class="editable-dependent"><label><span>Street: </span><input type="text" name="street" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-dependent"><label><span>City: </span><input type="text" name="city" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="dependent_holder"><div class="editable-dependent"><label><span>Country: </span><select id="country" name="country" class="form-control" style="width: 200px;">\
		    <option value="US">United States</option>\
		    <option value="CA">Canada</option>\
		  </select></label></div>\
		  '+
        '<div class="editable-dependent"><label><span>State: </span><select id="state" name="state" class="form-control" style="width: 200px;"></select></label></div></div>'+
        '<div class="editable-dependent"><label><span>Zip Code: </span><input type="text" name="zip" class="form-control" style="width: 200px;"></label></div>',
             
        inputclass: ''
    });

    $.fn.editabletypes.dependent = Dependent;

    $(".remove-dependent-btn").live("click", function(e) {
    	e.preventDefault();
    	
    	$(this).parents(".dependent-item").remove();
    });

}(window.jQuery));