/**
Contact editable input.
Internally value stored as {city: "Moscow", street: "Lenina", building: "15"}

@class contact
@extends abstractinput
@final
@example
<a href="#" id="contact" data-type="contact" data-pk="1">awesome</a>
<script>
$(function(){
    $('#contact').editable({
        url: '/post',
        title: 'Enter city, street and building #',
        value: {
            city: "Moscow", 
            street: "Lenina", 
            building: "15"
        }
    });
});
</script>
**/
(function ($) {
    "use strict";
    
    var Contact = function (options) {
        this.init('contact', options, Contact.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(Contact, $.fn.editabletypes.abstractinput);

    $.extend(Contact.prototype, {
        /**
        Renders input from tpl

        @method render() 
        **/        
        render: function() {
           this.$input = this.$tpl.find('input');
           this.$textareas = this.$tpl.find('textarea');

            $('input.title').typeahead({
              hint: true,
              highlight: true,
              minLength: 0
            },
            {
              name: 'titles',
              limit: 1000,
              source: substringMatcher(contact_titles)
            });
        },
        
        /**
        Default method to show value in element. Can be overwritten by display option.
        
        @method value2html(value, element) 
        **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }
            var html = '<a href="#" class="btn btn-sm pull-right btn-danger remove-contact-btn"><i class="fa fa-times"></i></a>' + value.first_name + ' ' + value.last_name ;

            if (value.title)
              html += ' (' + value.title + ')';

            if (value.email)
              html += '<br /> ' + value.email;

            if (value.phone)
              html += '<br /> ' + value.phone;

            if (value.cell_phone)
              html += '<br /> ' + value.cell_phone;

            $(element).html(html);

            var parent = $(element).parents('.contact-item');
            console.log(parent);
            parent.find('input.title').val(value.title);
            parent.find('input.first_name').val(value.first_name);
            parent.find('input.last_name').val(value.last_name);
            parent.find('input.email').val(value.email);
            parent.find('input.phone').val(value.phone);
            parent.find('input.cell_phone').val(value.cell_phone);
            parent.find('input.fax').val(value.fax);
            parent.find('input.additional_info').val(value.additional_info);
        },
        
        /**
        Gets value from element's html
        
        @method html2value(html) 
        **/        
        html2value: function(html) {        
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g. 
            editable({
                value: {
                    city: "Moscow", 
                    street: "Lenina", 
                    building: "15"
                }
            });
          */ 
          return null;  
        },
      
       /**
        Converts value to string. 
        It is used in internal comparing (not for sending to server).
        
        @method value2str(value)  
       **/
       value2str: function(value) {
           var str = '';
           if(value) {
               for(var k in value) {
                   str = str + k + ':' + value[k] + ';';  
               }
           }
           return str;
       }, 
       
       /*
        Converts string to value. Used for reading value from 'data-value' attribute.
        
        @method str2value(str)  
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute. 
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },                
       
       /**
        Sets value of input.
        
        @method value2input(value) 
        @param {mixed} value
       **/         
       value2input: function(value) {
           if(!value) {
             return;
           }
           this.$input.filter('[name="title"]').val(value.title);
           this.$input.filter('[name="first_name"]').val(value.first_name);
           this.$input.filter('[name="last_name"]').val(value.last_name);
           this.$input.filter('[name="email"]').val(value.email);
           this.$input.filter('[name="phone"]').val(value.phone);
           this.$input.filter('[name="cell_phone"]').val(value.cell_phone);
           this.$input.filter('[name="fax"]').val(value.fax);
           this.$textareas.filter('[name="additional_info"]').val(value.additional_info);
       },       
       
       /**
        Returns value of input.
        
        @method input2value() 
       **/          
       input2value: function() { 
           return {
              title: this.$input.filter('[name="title"]').val(), 
              first_name: this.$input.filter('[name="first_name"]').val(), 
              last_name: this.$input.filter('[name="last_name"]').val(), 
              email: this.$input.filter('[name="email"]').val(), 
              phone: this.$input.filter('[name="phone"]').val(), 
              cell_phone: this.$input.filter('[name="cell_phone"]').val(), 
              fax: this.$input.filter('[name="fax"]').val(), 
              additional_info: this.$textareas.filter('[name="additional_info"]').val()
           };
       },        
       
        /**
        Activates input: sets focus on the first field.
        
        @method activate() 
       **/        
       activate: function() {
            this.$input.filter('[name="first_name"]').focus();
       },  
       
       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode
        
        @method autosubmit() 
       **/       
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }       
    });

    Contact.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-contact"><label><span>Title: </span><input name="title" class="form-control title" style="width: 200px;" placeholder="None">\
		    </label></div>\
		  '+
        '<div class="editable-contact"><label><span>First Name: </span><input type="text" name="first_name" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-contact"><label><span>Last Name: </span><input type="text" name="last_name" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-contact"><label><span>Email: </span><input type="text" name="email" class="form-control" style="width: 200px;"></label></div>'+
        '<div class="editable-contact"><label><span>Phone: </span><input type="text" name="phone" class="form-control" style="width: 200px;"></label></div>'+
             '<div class="editable-contact"><label><span>Cell Phone: </span><input type="text" name="cell_phone" class="form-control" style="width: 200px;"></label></div>'+
             '<div class="editable-contact"><label><span>Fax: </span><input type="text" name="fax" class="form-control" style="width: 200px;"></label></div>'+
             '<div class="editable-contact"><label><span>Additional Info: </span><textarea name="additional_info" class="form-control" style="width: 200px;"></textarea></label></div>',
             
        inputclass: ''
    });

    $.fn.editabletypes.contact = Contact;

    $(".remove-contact-btn").live("click", function(e) {
    	e.preventDefault();
    	
    	$(this).parents(".contact-item").remove();
    });

}(window.jQuery));