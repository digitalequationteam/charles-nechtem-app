function pasteHtmlAtCaret(html) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        
        if ($(sel.anchorNode.parentElement).closest(".note-editor").length == 0)
            return;

        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // only relatively recently standardized and is not supported in
            // some browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}

$(document).ready(function() {
    $('.check_all').on('click', function () {
       if ($(this).prop('checked') ==  true){
            $(this).closest('table').find('input:checkbox').prop('checked', true);
            $(this).closest('table').find('tr:not(.filters-tr)').addClass('selected');
        } else {
            $(this).closest('table').find('input:checkbox').prop('checked', false);
            $(this).closest('table').find('tr:not(.filters-tr)').removeClass('selected');
        }
    });

    $('.batch_id').on('click', function () {
       if ($(this).prop('checked') ==  true){
            $(this).closest('tr').addClass('selected');
        } else {
            $(this).closest('tr').removeClass('selected');
        }
    });

    $('.tooltips').tooltip();

    $("select#country").live("change", function() {
        var parent = $(this).parents(".address_holder");
        var state = (parent.length == 0) ? $("#state") : parent.find("#state");

        state.prop("disabled", true);
        state.html('');
        $.post("/api/get-states/" + $(this).val(), { _token: $('input[name="_token"]').val() }, function(data) {
            state.append('<option value="">Select state...</option>')
            for (var i in data.states) {
                state.append('<option value="' + i + '">' + data.states[i] + '</option>')
            }
            state.prop("disabled", false);
        });
    });
    
    $("select#state").each(function() {
        if ($(this).find("option").length == 1 || $(this).find("option").length == 0) {
            var parent = $(this).parents(".address_holder");
            var country = (parent.length == 0) ? $("#country") : parent.find("#country");
            country.trigger("change");
        }
    });

    $("select").select2();

      $('.ui-bootbox-alert').on('click', function () {
        var that = $(this);
        bootbox.confirm({
          message: $(this).attr("data-title"),
          callback: function(result) {
            if (result)
              window.location = that.attr('href');
          },
          className: "bootbox-sm"
        });
        return false;
      });

    $('a.btn.bg-danger[href*="delete"], a.btn.bg-danger[href*="remove"], a.btn.red, a.delete-btn').click(function(e) {
        e.preventDefault();
        var that = $(this);
        var message = $(this).attr("data-original-title");
        if (typeof(message) == "undefined")
            message = that.text();

        if (confirm(message + ". Are you sure?")) {
            window.location = that.attr('href');
        }

        return false;
    });

    $("#main-menu li.active").each(function() {
        $(this).parents("li").addClass("current").addClass("current").click();
    });

    $(window).resize(function() {
        processMenuScroll();
    });

    setTimeout(function() {
        processMenuScroll();
    }, 500);

    $('input.date').datepicker({
        "dateFormat": 'yy-mm-dd'
    });

    $('input.date').each(function() {
        $(this).datepicker("option", "dateFormat", 'mm/dd/yy');
    });
});

function processMenuScroll() {
    if ($("html").hasClass("sidebar-large")) {
        var new_height = $(window).height() - $("nav.navbar").height() - $(".footer-widget").height();
        console.log(new_height);
        $("ul.sidebar-nav").css("height", new_height + "px");

        $("ul.sidebar-nav li li.active").each(function() {
            $(this).closest("ul").addClass("in");
        });
    }
    else {
        $("ul.sidebar-nav li li.active").each(function() {
            $(this).closest("ul").removeClass("in");
        });        
    }
}