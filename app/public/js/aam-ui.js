$(document).ready(function() {
	$('#add-role-btn').bind('click', function (event) {
	    event.preventDefault();

	    var _this = this;

	    $('#new-role-name', '#new-role-level', '#add-role-modal').parent().removeClass('has-error');

	    var name = $.trim($('#new-role-name', '#add-role-modal').val());
	    var level = $.trim($('#new-role-level', '#add-role-modal').val());
	    var inherit = $.trim($('#inherit-role-list', '#add-role-modal').val());

	    if (name) {
	        $.ajax("/staff/save-role", {
	            type: 'POST',
	            dataType: 'json',
	            data: {
	                name: name,
	                level: level,
	                inherit_id: $("#inherit-role-list").val()
	            },
	            beforeSend: function () {
	                $(_this).text('Saving...').attr('disabled', true);
	            },
	            success: function (response) {
	                window.location = window.location.href;
	            }
	        });
	    } else {
	        $('#new-role-name').focus().parent().addClass('has-error');
	    }
	});

	$('#edit-role-btn').bind('click', function (event) {
        var _this = this;

        $('#edit-role-name').parent().removeClass('has-error');
        var name = $.trim($('#edit-role-name').val());
        var level = $.trim($('#edit-role-level').val());

        if (!name)
        	$('#edit-role-name').focus().parent().addClass('has-error');
        else if (!level)
        	$('#edit-role-level').focus().parent().addClass('has-error');
        else {
        	$.ajax("/staff/save-role/" + $("#edit_role_id").val(), {
                type: 'POST',
                dataType: 'json',
                data: {
                    name: name,
                    level: level
                },
	            beforeSend: function () {
	                $(_this).text('Saving...').attr('disabled', true);
	            },
	            success: function (response) {
	                window.location = window.location.href;
	            }
            });
        }
    });
});