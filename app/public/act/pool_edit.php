<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "pool_edit";

//Including necessary files
require_once 'include/util.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

$companies = $db->getAllCompanyNames();
$pool_id = (int)$_GET['pool_id'];
$pool_details = $db->getPoolDetail($pool_id);
$areacode = substr($pool_details['numbers'][count($pool_details['numbers'])-1], 0,3);
$comapnyname = $db->getCompanyName($pool_details['company_id']);
$company_id = $pool_details['company_id'];

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>       <!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                
                <div class="block" id="filter-box" style="width:465px; margin:0 auto; margin-bottom: 25px;">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2 id="act_update">
                            Edit Pool
                        </h2>
                         
                    </div>      <!-- .block_head ends -->
                    <div class="block_content">
                        <div id='edit_form_pool'  style="display:block">
                        <form action="#" method="POST" id="poolnumber" >
                            <input type="hidden" id="new_url" value="" />
                                <label>Company Name: </label><br/>
                                <input name="company_name" class="text small" style="border: 1px solid #DBDBDB;" type="text" id="company_name" value="<?php echo stripslashes(ucwords($comapnyname)); ?>" disabled="disabled"/>
                                <input name="company_id" class="text small" type="hidden" id="company_id" value="<?php echo $company_id; ?>" />
                                <br />
                                <br />
                            <label>Country: </label><br/>
                            <select id="country" style="width:426px;">
                                <option value="US" selected="selected" data-region="1">United States (+1)</option>
                                <?php
                                foreach(Util::$supported_countries as $country_code => $country){
                                    ?>
                                    <option value="<?php echo $country_code; ?>" data-region="<?php echo @$country[2]!="" ? $country[2]:0; ?>"><?php echo $country[0]. " (+".$country[1].")"; ?></option>
                                <?php
                                }
                                ?>
                            </select><br/>
                            <script>
                                function format(state) {
                                    if (!state.id) return state.text;
                                    return "<img class='flag' width='15' src='images/flags/" + state.id.toLowerCase() + ".gif'/>&nbsp;&nbsp;" + state.text;
                                }
                                $(document).ready(function() {

                                    $("#country").select2({
                                        formatResult: format,
                                        formatSelection: format,
                                        escapeMarkup: function(m) { return m; }
                                    });

                                    $("#company").select2();
                                });
                            </script>
                            <br/>
                            <label>Area Code or Matching this pattern (e.g. 415***EPIC): </label><input name="area_code" class="text small" type="text" id="code"/><br/>
                            <p style="font-size:10px;"><i>To search for area code, enter: 772******* (replace '772' with your area code)</i></p> 
                            <label>Pool Size: </label><input name="pool_number" class="text small" type="text" id="poolsize"/><br/>
                            <p style="font-size:10px;"><i></i></p> 
                            <input type="submit" value="Add Numbers" name="submit" id="search_num" class="submit mid" style="margin-bottom: 5px; float:right;">
                        </form>
                       </div>
                     <div id='show_numbres' class='number_content'>
                        
                    </div>
                    </div>      <!-- .block_content ends -->
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>      <!-- .block ends -->
                    
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>
                            Edit Pool Numbers
                        </h2>
                       <span><input type="submit" style="margin-top: 15px; float:right;" class="submit mid" id="delete_pool" name="submit" value="Delete Pool"></span>
                    </div>      <!-- .block_head ends -->
                    <div class="block_content">  
                        <form action="pool_edit_delete.php" method="post" id='pool_number'>
                                <table cellpadding="0" cellspacing="0" width="100%" class='pool_edit'>
                                     <thead>
                                      </thead>
                                    <tbody>
                                         <tr>
                                            <td class="pool_edit_style">
                                                
                                                <input id='pool_id' type="hidden" value="<?php echo $pool_id;?>" name="pool_id"/>
                                                <label>Current Numbers: </label>
                                                <div style="padding-top: 10px;">
                                                    <?php foreach($pool_details['numbers'] as $value) { ?>
                                                            <div style="float: left; padding-right: 12px; padding-bottom: 5px;">
                                                                <input class="checked_numbers" type="checkbox" name="number[]" value="<?php echo $value; ?>" style="display: inline-block !important;" /> <?php echo $value; ?>
                                                            </div>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="pool_edit_style">
                                             <span style='font-size:9px;' class="edit_delete_numbers">Checked number(s) will be deleted.</span>
                                            </td>
                                            
                                        </tr> 
                                        <tr>
                                            <td class="pool_edit_style">
                                            <span class="edit_delete_numbers"><label>Total Numbers: </label> <input name="numbers_pool" class="text small" type="text" id="poolsize" value="<?php echo count($pool_details['numbers']); ?>" disabled="disabled" style="display: inline-block !important; width: 22px;" /></span>
                                            </td>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td class="pool_edit_style">
                                            <span class="edit_delete_numbers">
                                                <input id="delete_btn" class="blue_button" class='save' type="submit"  value="Delete" />
                                                <a href="pool_number.php"><input class="blue_button" type="submit" value="Back" onclick="window.location = 'pool_number.php';return false;" /></a>
                                            </span>
                                            </td>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                       </div>
                        <div class="bendl"></div>
                        <div class="bendr"></div>
                    </div>
                </div> 

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>

                <script type="text/javascript">

                    $(document).ready(function() {
                        /*$('input[type="checkbox"]').click(function() {
                            if ($('input[type="checkbox"]:checked').length == 0)
                                $("#delete_btn").prop('disabled', true);
                            else
                                $("#delete_btn").prop('disabled', false);
                        });*/
                    });

                    $('#pool_number').submit(function(event) {
                            event.preventDefault();
                            var checked_numbers = [];
                            var poolId          = $('#pool_id').val();
                            var i               = 0;
                            $(".checked_numbers:checked").each(function(){
                               checked_numbers[i] = $(this).val();
                               ++i;
                            });
                            var countNumbers = checked_numbers.length;

                            if (countNumbers == 0) {
                                errMsgDialog("Please select numbers first.");
                                return false;
                            }

                            var newBlacklist = '<p style="font-size:15px;">\
                                Are you sure you want to delete '+ countNumbers +' numbers?\
                                </p>';
                                
                                var $dialog = $('<div></div>').html(newBlacklist).dialog({                                
                                    modal: true,                                
                                    autoOpen: true,                                
                                    minHeight: 177,                                
                                    maxWidth: 587,                                
                                    minWidth: 520,                                
                                    height: 'auto',
                                    minWidth: 520,
                                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\Delete these numbers',                                
                                    buttons: [{                                
                                        text: "Ok",                                
                                        click: function (event) { 
                                        
                                            poolNumberDelete(poolId, checked_numbers);
                                            
                                            $(this).dialog("close");
                                
                                            $(this).dialog('destroy').remove();
                                            
                                        }                                
                                    }, {                                
                                        text: "Cancel",                                
                                        click: function (event) {
                                            
                                            event.preventDefault();
                                            $(this).dialog("close");
                                
                                            $(this).dialog('destroy').remove();
                                
                                        }
                                    }]
                                });
                                $("#filter-box").unblock();
                                        return false;
                    }); 
                        
                        
                        $('#delete_pool').click(function(event){
                            event.preventDefault();
                            var poolId = $('#pool_id').val();
                            var newBlacklist = '<p style="font-size:15px;">\
                            Are you sure you want to delete this pool?\
                            </p>';
                            
                            var $dialog = $('<div></div>').html(newBlacklist).dialog({                            
                                modal: true,                            
                                autoOpen: true,                            
                                minHeight: 177,                            
                                maxWidth: 587,                            
                                minWidth: 520,                            
                                height: 'auto',                            
                                minWidth: 520,                            
                                title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\Delete Pool',                            
                                buttons: [{                            
                                    text: "Ok",                            
                                    click: function () {

                                        $(this).dialog("close");
                                        
                                        $(this).dialog('destroy').remove(); 
                                        
                                        var newBlacklist = '<p style="font-size:15px;">\
                                        Deleting Pool will delete all numbers as well. Are you sure you want to do it?\
                                        </p>';
                                        
                                        var $dialog = $('<div></div>').html(newBlacklist).dialog({                                        
                                            modal: true,                                        
                                            autoOpen: true,                                        
                                            minHeight: 177,                                        
                                            maxWidth: 587,                                        
                                            minWidth: 520,                                        
                                            height: 'auto',                                        
                                            minWidth: 520,                                        
                                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\Delete Pool',                                        
                                            buttons: [{                                        
                                                text: "Ok",                                        
                                                click: function () { 
                                                    
                                                    poolDelete(poolId);
                                                    
                                                    $(this).dialog("close");
                                        
                                                    $(this).dialog('destroy').remove();
                                                    
                                                }
                                            }, {                                        
                                                text: "Cancel",                                        
                                                click: function () {
                                        
                                                    $(this).dialog("close");
                                        
                                                    $(this).dialog('destroy').remove();
                                        
                                                }
                                            }]
                                        
                                        });
                                        $("#filter-box").unblock();
                                        return false;
                                        
                                    }
                            
                                }, {                            
                                    text: "Cancel",                            
                                    click: function () {
                            
                                        $(this).dialog("close");
                            
                                        $(this).dialog('destroy').remove();
                            
                                    }
                            
                                }]
                            
                            });
                            $("#filter-box").unblock();
                            return false;   

                        });
                        
                    $('#search_num').click(function(event){
                        event.preventDefault();
                        
                        var poolId          = $('#pool_id').val();
                        var company_name    = $('#company_name').val();
                        var company_id      = $('#company_id').val();
                        var country         = $('#country').val();
                        var pool_size       = $('#poolsize').val();
                        var area_code       = $('#code').val();
                        
                        $("#filter-box").block({
                            message: '<h1 style="color:#fff">Loading</h1>',
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .7,
                                color: '#fff !important'
                            }
                        });
                            
                        var ajaxparams = {

                            area_code   :       area_code,
                            company     :       company_id,
                            poolsize    :       pool_size,
                            country     :       country,
                            edit        :       'edit',
                            pool_id     :       poolId
                            
                        };

                        var response = JSONajaxCall({
                            func: "SYSTEM_TWILIO_GET_ADD_NUMBER_POOL",
                            data: ajaxparams,
                            async: true
                        });
                        
                        response.done(function (msg) {
                            if(msg.result == "error"){
                                errMsgDialog(err_msg);
                                $("#filter-box").unblock();
                                return false;
                            }
                            if(msg.result == "success"){
                                $( "#edit_form_pool" ).hide();
                                $("#show_numbres").append(msg.details);
                                $("#act_update").html("Avaliable Numbers ("+msg.count+")");
                                $("#filter-box").unblock();
                                return false;       
                            }
                        });
                    });


                    $("#checkAll").live("click", function () {
                            var checked_numbers =   [];
                            
                            if ($("#checkAll").is(':checked')) {
                                $(".num_select").each(function () {
                                    $(this).prop("checked", true);
                                    var i = 0;
                                    $(".num_select:checked").each(function(){
                                       checked_numbers[i] = $(this).val();
                                       i++;
                                    });
                                    var count = checked_numbers.length;
                                    $('#select_count').html(count);
                                });

                            } else {
                                $(".num_select").each(function () {
                                    $(this).prop("checked", false);
                                    $('#select_count').html('0');
                                });
                            }
                    });

                    $('.num_select').live("click",function(){
                        $('#checkAll').prop("checked", false);
                        var checked_numbers =   [];
                        var i = 0;
                        $(".num_select:checked").each(function(){
                               checked_numbers[i] = $(this).val();
                               i++;
                        });
                        var count = checked_numbers.length;
                        $('#select_count').html(count);
                    });

                    $( "#buy_numbers" ).live( "submit", function( event ) {
                        event.preventDefault();
                        var poolId          =   $('#edit_pool_id').val();
                        var company_name    =   $('#company_name').val();
                        var checked_numbers =   [];
                        var i = 0;
                        $(".num_select:checked").each(function(){
                               checked_numbers[i] = $(this).val();
                               i++;
                        });
                        var count = checked_numbers.length;

                        if (count == 0) {
                            errMsgDialog("There are no numbers selected.");
                            $("#filter-box").unblock();
                            return false;
                        }
                        
                        var newBlacklist = '<p style="font-size:15px;">\
                        Are you sure you want to buy '+ count +' numbers.\
                        </p>';
                        var $dialog = $('<div></div>').html(newBlacklist).dialog({
                        
                            modal: true,
                        
                            autoOpen: true,
                        
                            minHeight: 177,
                        
                            maxWidth: 587,
                        
                            minWidth: 520,
                        
                            height: 'auto',
                        
                            minWidth: 520,
                        
                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\Add Numbers',
                        
                            buttons: [{
                        
                                text: "Ok",
                        
                                click: function (event) { 
                                                
                                    addNumbersPool( poolId, checked_numbers, company_name);
                                    
                                    $(this).dialog("close");
                        
                                    $(this).dialog('destroy').remove();
                                    
                                }
                        
                            }, {
                        
                                text: "Cancel",
                        
                                click: function (event) {
                                    
                                    event.preventDefault();
                                    $(this).dialog("close");
                        
                                    $(this).dialog('destroy').remove();
                        
                                }
                        
                            }]
                        
                        }); 
                    });

                    function addNumbersPool(poolId, add_numbers, company_name){

                        $("#filter-box").block({
                            message: '<h1 style="color:#fff">Saving Numbers</h1>',
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .7,
                                color: '#fff !important'
                            }
                        });
                            
                        var ajaxparams = {
                            pool_id: poolId,
                            add_numbers: add_numbers,
                            company_id: <?php echo $company_id; ?>,
                            country: $('#country').val(),
                            url: "http://" + window.location.hostname,
                            call_handler_url: document.location.href.split("pool_edit.php")[0] + 'handle_incoming_call.php'
                        };

                        var response = JSONajaxCall({
                            func: "SYSTEM_TWILIO_ADDING_SELECTED_NUMBER_POOL",
                            data: ajaxparams,
                            async: true
                        });
                        
                        response.done(function (msg) {
                            if(msg.result == "error"){
                                errMsgDialog(msg.msg);
                                $("#filter-box").unblock();
                                return false;
                            }
                            if(msg.result == "success"){
                                msgDialog(msg.msg);
                                $('.ui-button').click(function(){;
                                    if('.ui-icon-info'){
                                        var url = 'pool_edit.php?pool_id='+poolId;
                                        window.location.replace(url);
                                    }
                                });
                                $("#filter-box").unblock();
                                return false;
                            }
                        });
                        
                    }

                        
                    function poolNumberDelete(poolId, checked_numbers){
                        
                        $("#filter-box").block({
                                message: '<h1 style="color:#fff">Deleting number(s) of this particular pool</h1>',
                                css: {
                                    border: 'none',
                                    padding: '15px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .7,
                                    color: '#fff !important'
                                }
                            });
                        var ajaxparams = {
                            pool_id: poolId,
                            checked_numbers: checked_numbers,
                        };

                        var response = JSONajaxCall({
                            func: "SYSTEM_TWILIO_DELETE_POOL_NUMBER",
                            data: ajaxparams,
                            async: true
                        });


                        response.done(function (msg) {
                            if(msg.result == "error"){
                                errMsgDialog(msg.msg);
                                $("#filter-box").unblock();
                                return false;
                            }
                            if(msg.result == "success"){
                                msgDialog(msg.msg);
                                $('.ui-button').click(function(){;
                                    if('.ui-icon-info'){
                                        var url = 'pool_edit.php?pool_id='+poolId;
                                        window.location.replace(url);
                                    }
                                });
                                $("#filter-box").unblock();
                                return false;
                            }
                        });
                        
                    }
                        
                    function  poolDelete(poolId){
                        
                        $("#filter-box").block({
                                message: '<h1 style="color:#fff">Deleting Whole Pool</h1>',
                                css: {
                                    border: 'none',
                                    padding: '15px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .7,
                                    color: '#fff !important'
                                }
                            });
                        
                        var ajaxparams = {
                            pool_id: poolId,
                        };

                        var response = JSONajaxCall({
                            func: "SYSTEM_TWILIO_DELETE_POOL",
                            data: ajaxparams,
                            async: true
                        });


                        response.done(function (msg) {
                            if(msg.result == "error"){
                                errMsgDialog(msg.msg);
                                $("#filter-box").unblock();
                                return false;
                            }
                            if(msg.result == "success"){
                                msgDialog(msg.msg);
                                $('.ui-button').click(function(){;
                                    if('.ui-icon-info'){
                                        var url = 'pool_number.php';
                                        window.location.replace(url);
                                    }
                                });
                                $("#filter-box").unblock();
                                return false;
                            }
                        });
                        
                    }
                </script>
            </div>
        </div>
    </body>
</html>