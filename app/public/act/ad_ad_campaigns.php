<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_auto_dialer";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object 
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

//Check for user perm
if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10006)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

//By default setting null values to action responses
$action_response = '';
$action_type = '';

//If current request is to perform some action on current page
if (isset($_GET['action'])):

    //Switching to appropriate action code
    switch ($_GET['action']):

        //If action is to perform delete operation
        case 'confirm_delete':

            //If campaign ID is set in request URL
            if (isset($_GET['id'])):

                //Retrieving the campaign IDX to delete
                $del_campaign_idx = $_GET['id'];

                //Setting the alert response messages to show
                $action_response = 'Are you sure? You are trying to delete "<b>' . ad_ad_get_campaign_name($del_campaign_idx) . '</b>" campaign and associated data. <a href="ad_ad_campaigns.php?id=' . $del_campaign_idx . '&action=delete">Click here</a> to confirm.';
                $action_type = 'warning';

            //Ending condition checking campaign ID in request URL
            endif;

            break;

        //If action is to perform delete operation
        case 'delete':

            //If campaign ID is set in request URL
            if (isset($_GET['id'])):

                //Retrieving the campaign IDX to delete
                $del_campaign_idx = $_GET['id'];

                //Trying to delete the campaign
                ad_ad_delete_campaign($del_campaign_idx);

                //If campaign deleted successfully
                //Setting the success response messages to show
                $action_response = 'Campaign deleted!';
                $action_type = 'success';

            //Ending condition checking campaign ID in request URL
            endif;

            break;

        default:
            break;
    endswitch;

//Enditing condition checking action request
endif;

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                <div class="block bulk_dial_status" style="display: none;background: none;">
                    <div class="block_head"> 
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2 style="color:red;" class="ad_ad_bulk_dial_status_text"></h2>
                    </div>
                </div>
                <div class="clear"></div>

                <!--Auto dialer campaigns listing section starts here-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Auto Dialer Campaigns</h2>
                        <div id="searchbox">
                            <input type="text" id="search" value="<?php echo @$_GET['search']; ?>" placeholder="Campaign Name" />
                            <a href="#" onclick="window.location='ad_ad_campaigns.php';"><img src="images/ic_cancel.png" width="16" class="search-reset tt" style="<?php echo isset($_GET['search']) ? "display:inline-block !important;":""; ?>" title="Reset"></a>
                        </div>
                        <ul>
                            <li><a href="ad_ad_add_campaign.php">Add a Campaign</a></li>
                            <li><a href="ad_ad_campaigns.php">Campaigns List</a></li>
                            <li><a href="ad_contactlist_log.php">Contacts</a></li>
                            <li><a href="ad_ad_logs.php">Logs</a></li>
                        </ul>
                    </div>
                    <!-- .block_head ends -->
                    <div class="block_content">
                        <?php
                        //Checkin if action message is set or not
                        if (!s8_is_str_blank($action_response)):

                            //If action message is set, switching to type of action message
                            switch ($action_type):

                                //If action message is of success type
                                case 'success':
                                    //Displaying success message
                                    ?>
                                    <div class="message errormsg">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                                //If action message is of failure type
                                case 'failed':
                                    //Displaying failure message
                                    ?>
                                    <div class="message success">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                                //If action message is of failure type
                                case 'warning':
                                    //Displaying failure message
                                    ?>
                                    <div class="message warning">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                            endswitch;

                        endif;
                        ?>
                        <form action="" method="post">

                            <table cellpadding="0" cellspacing="0" width="100%" class="sortable">
                                <thead>
                                    <tr>
                                        <th>Campaign Name</th>
                                        <th>Phone Number</th>
                                        <th>Status</th>
                                        <th>Progress</th>
                                        <th id="last_ran">Last Ran</th>
                                        <th>User</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $campaigns = ad_ad_get_campaigns(@$_GET['search'],isset($_GET['page']) ? $_GET['page'] : '1');

                                    if (count($campaigns) > 0):
                                        foreach ($campaigns as $single_campaign_data):
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    echo '<a href="' . dirname(s8_get_current_webpage_uri()) . '/ad_ad_edit_campaign.php?idx=' . $single_campaign_data['idx'] . '">' . $single_campaign_data['campaign_name'] . '</a>';
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo format_phone($db->format_phone_db($single_campaign_data['phone_number']));
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $single_campaign_data['calls_status'];
                                                    ?>
                                                </td>
                                                <td>
                                                    <span style="display: none;"><?php
                                                        $prog = ad_ad_get_campaign_progress($single_campaign_data['idx']);
                                                        $divprog = explode("/",$prog);
                                                            echo $divprog[0];

                                                        ?></span>
                                                    <a href="ad_ad_logs.php?camp_idx=<?php echo $single_campaign_data['idx']; ?>">
                                                        <?php
                                                        echo $prog
                                                        ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (!s8_is_str_blank($single_campaign_data['last_ran']) && is_numeric($single_campaign_data['last_ran'])):
                                                        echo "<span style='display:none;'>".$single_campaign_data['last_ran']."</span>".Util::convertToLocalTZ("@".$single_campaign_data['last_ran'])->format(Util::STANDARD_LOG_DATE_FORMAT);
                                                    else:
                                                        echo 'NA';
                                                    endif;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo ad_get_user($single_campaign_data['user']);
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="ad_ad_dialing_details.php?campaign_idx=<?php echo $single_campaign_data['idx']; ?>"><img class="tt" title="Start Campaign" width="16" src="images/play-icon.png"></a>
                                                    &nbsp;<a href="ad_ad_campaigns.php?id=<?php echo $single_campaign_data['idx']; ?>&action=confirm_delete"><img class="tt" title="Delete Campaign" width="16" src="images/delete.gif"></a>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        ?>
                                        <tr>
                                            <td>No records exist.</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php
                                    endif;
                                    ?>
                                </tbody>
                            </table>

                            <?php
                            $total_campaigns = ad_ad_get_campaigns_count(@$_GET['search']);

                            $pagination = new Pagination();
                            $page_n = 1;
                            if (isset($_GET['page'])){
                                $page_n = (int) $_GET['page'];
                            }
                            if(strpos($_SERVER['REQUEST_URI'],"page") === false)
                            {
                                $url="//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                if (empty($_SERVER['QUERY_STRING']))
                                    $pagination->setLink($url."?page=%s");
                                else
                                    $pagination->setLink($url."&page=%s");
                            }else{
                                $url="//".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['page']));
                                $pagination->setLink($url."%s");
                            }
                            $pagination->setPage($page_n);
                            $pagination->setSize(20);
                            $pagination->setTotalRecords($total_campaigns);

                            echo $pagination->create_links();
                            ?>
                        </form>
                    </div>		
                    <!-- .block_content ends -->

                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Auto dialer campaigns listing ends here-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>

        <!--//Notification bar html-->
        <div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
    <script type="text/javascript">
        $(".tt").tooltipster({
            position:'bottom',
            theme:'.tooltipster-shadow'
        });
        $(document).ready(function(){
            $("#last_ran").trigger("click");
            $("#last_ran").trigger("click");
            $("#search").keyup(function(e){
                if(e.which == 13){
                    var query = $("#search").val();
                    if(query.length>0)
                        window.location = "ad_ad_campaigns.php?search="+encodeURIComponent(query)+"&page=1";
                    else
                        window.location = "ad_ad_campaigns.php";
                }
            });
        });
    </script>
    </body>
</html>