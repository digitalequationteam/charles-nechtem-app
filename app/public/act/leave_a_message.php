<?php

require_once('include/config.php');
require_once('include/db.php');
require_once('include/Services/Twilio.php');
require_once('include/twilio_header.php');

$db = new DB();

header("Content-Type: text/xml");

if (strlen($_REQUEST['exten'])) {
	$exten = $_REQUEST['exten'];
	$mailbox = $db->getMailbox($exten);

	//output TwiML to record the message
	$response = new Services_Twilio_Twiml();
//	$response->say('Leave a message for ' . $mailbox['desc'] . ' at the beep');

	$company_settings = $db->getCompanySettings($exten);

	if ($company_settings->voicemail_type  == 'Text' ) {
        $language = $company_settings->voicemail_text_language;
        $voice = $company_settings->voicemail_text_voice;
        $language = str_replace("|M", "", str_replace("|W", "", $language));

        if (empty($language))
            $response->say($company_settings->voicemail_content);
        else
            $response->say($company_settings->voicemail_content, array("language" => $language, "voice" => $voice));
    }
    elseif ($company_settings->voicemail_type  == 'Audio' )   
        $response->play('audio/'.$exten.'/'.$company_settings->voicemail_content, array("loop" => "1"));
    elseif ($company_settings->voicemail_type  == 'MP3_URL' || $company_settings->voicemail_type  == 'RECORD_AUDIO' )   
        $response->play($company_settings->voicemail_content, array("loop" => "1"));
    else
    	$response->say('Leave a message for at the beep');

    $send_transcript = $company_settings->send_transcript;
    $transcriptEmail = $company_settings->transcriptEmail;

    $params = array(
		'action' => dirname(s8_get_current_webpage_uri())."/handle_message.php?exten=$exten",
		'maxLength' => '120',
		'playBeep' => 'true'
	);

    if ($send_transcript == 1) {
        $params['transcribe'] = 'true';
        $params['transcribeCallback'] = dirname(s8_get_current_webpage_uri()).'/handle_transcription.php?email='.urlencode($transcriptEmail);
    }

	$response->record($params);

	// record will post to this url if it receives a message
	// otherwise it falls through to the next verb
	$response->gather()
		->say("A message was not received, press any key to try again");

	print $response;
}

?>