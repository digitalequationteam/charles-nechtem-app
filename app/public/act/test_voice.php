<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "pool_number";

//Including necessary files
require_once 'include/util.php';
require_once 'include/twilio_header.php';

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="//static.twilio.com/libs/twiliojs/1.1/twilio.min.js"></script>
        <script type="text/javascript" src="js/callpanel_slider_jsfile.js"></script>
    </head>
    <body>
        <?php
        $voice = isset($_REQUEST['voice']) ? $_REQUEST['voice'] : "man";
        $language = isset($_REQUEST['language']) ? $_REQUEST['language'] : "en";
        $text = isset($_REQUEST['text']) ? $_REQUEST['text'] : "Please enter some text first.";
        $text = addslashes($text);
        ?>
        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                Twilio.Device.setup("<?php echo generate_twilio_testvoice_auth_token(); ?>");
                Twilio.Device.connect( { voice: "<?php echo $voice; ?>", language: "<?php echo $language; ?>", text: "<?php echo $text; ?>" } );
            });
        </script>
    </body>
</html>