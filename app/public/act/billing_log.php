<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
require_once('include/Pagination.php');
$db = new DB();

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if ($_SESSION['permission'] < 1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once("include/db.client.php");
require_once("include/db.invoice.php");

//function to send emails.
function sendEmail($to, $subject, $body) {
	require_once "include/class.emailtemplates.php";
	global $SITE_NAME;
	
	$db = new DB();

	$email_params = array(
        "subject" => $subject,
        "msg" => $body,
        "emails" => array($to)
    );
    Util::sendEmail($email_params);
}

if (isset($_REQUEST["ajaxRequest"])) {
	
	$ajaxRequest = $_REQUEST["ajaxRequest"];
	
	//delete invoice.
	if ($ajaxRequest == "deleteInvoice") {
	
		$invoiceId = $_REQUEST["invoiceId"];
		
		$invoice = new Invoice();
		
		//delete invoice.
		$invoice->deleteInvoice(array("invoice_id" => $invoiceId));
	}
	//set invoice offline payment
	else if ($ajaxRequest == "markInvoicePaidOffline") {

        $invoiceTotalAmount = 0;
        $invoiceId = $_REQUEST['invoiceId'];
        $invoice = new Invoice();

        $invoicesArray = $invoice->getInvoices(array(
            "invoice_id" => $invoiceId
        ));

        $invoiceArray = $invoicesArray[0];

        $invoiceDetailsArray = unserialize($invoiceArray['invoice_details']);

        if(is_array($invoiceDetails) || is_object($invoiceDetails))
            foreach ($invoiceDetails as $invoiceDetailArray) {
                $invoiceTotalAmount += $invoiceDetailArray["totalPrice"];
            }

        $invoice->updateInvoicePaymentInfo(array(
            "amount_paid" => $invoiceTotalAmount,
            "invoice_id" => $invoiceId
        ));

        $stmt = $db->customExecute("UPDATE invoices SET is_offline = 1 WHERE id = ?");

        $stmt->execute(array($invoiceId));

        die();

	}
    else if($ajaxRequest == "markInvoicePaidOfflineReverse"){
        $invoiceId = $_REQUEST['invoiceId'];

        $stmt = $db->customExecute("update invoices set is_offline = 0, is_paid = 0, amount_paid = '0.00' where id = ?");
        $stmt->execute(array($invoiceId));

        die();
    }
	//email invoice.
	else if ($ajaxRequest == "emailInvoice") {
        require_once("include/class.emailtemplates.php");

		$invoiceId = $_REQUEST["invoiceId"];

		$invoice = new Invoice();
        $client = new Client();

		//get invoice info.
		$invoicesArray = $invoice->getInvoices(array(
			"invoice_id" => $invoiceId
		));

		$invoiceArray = $invoicesArray[0];
        $invoiceDetailsArray = unserialize($invoiceArray['invoice_details']);

        $clientsArray = $client->getClients(array(
            "client_id" => $invoiceArray['client_id']
        ));

        $clientArray = $clientsArray[0];

		$userToSendBillTo = $invoiceArray["user_to_send_bill_to"];

		$_GET["invoice_id"] = str_replace("-", "", $invoiceArray["date_mysql_format"]) . $invoiceId;

        $db = new DB();
        $templ = new EmailTemplate();
        $template = $templ->getEmailTemplate(1);

        $totalPrice = 0;
        $leadsnum   = 0;

        foreach($invoiceDetailsArray as $invoicedetail){
            $totalPrice = $totalPrice + $invoicedetail['totalPrice'];
            if(substr($invoicedetail['desc'],0,4)=="Lead" && $invoicedetail['desc']!="Lead Gen - Free Leads"){
                $leadsnum = $invoicedetail['quantity'];
            }
        }

        switch($invoiceArray['amount_currency']){
            case "HUF":
            case "TWD":
            case "JPY":
            {
                $totalPrice = round($totalPrice);
                break;
            }
        }

        global $SITE_NAME;
        if($SITE_NAME!="")
            $site_name = $SITE_NAME;
        else
            $site_name = "Call Tracking";

        if(substr($_SERVER["HTTP_HOST"],0,4)=="www.")
            $host = substr($_SERVER["HTTP_HOST"],4,strlen($_SERVER["HTTP_HOST"]-4));
        else
            $host = $_SERVER["HTTP_HOST"];

        $host = "http://".$host.str_replace("/billing_log.php","",$_SERVER['SCRIPT_NAME']);

        $from_date = new DateTime($invoiceArray['from_date_sql']);
        $to_date = new DateTime($invoiceArray['to_date_sql']);


        $msg = str_replace("[invoiceperiod]",          $from_date->format("F j, Y")." - ".$to_date->format("F j, Y"),$template->content);
        $msg = str_replace("[cashtotalinvoiceamount]", $totalPrice,$msg);
        $msg = str_replace("[companyinfo]",            $db->getVar("company_info"),$msg);
        $msg = str_replace("[invoicenumber]",          "INV-".str_replace("-", "", date("Y-m-d", strtotime($invoiceArray['to_date']))).@$invoiceArray['id'],$msg);
        $msg = str_replace("[currentdate]",            date("F j, Y", strtotime("now")),$msg);
        $msg = str_replace("[fullname]",               $clientArray['client_name'],$msg);
        $msg = str_replace("[useraddress]",            $clientArray['address'],$msg);
        $msg = str_replace("[usercity]",               $clientArray['city'],$msg);
        $msg = str_replace("[userstate]",              $clientArray['state'],$msg);
        $msg = str_replace("[userzip]",                $clientArray['zip_code'],$msg);
        $msg = str_replace("[url]",                    $host."/billing_log_invoice.php?invoice_id=".str_replace("-", "", date("Y-m-d", $now)).@$invoiceArray['id']."&rkey=".$invoiceArray['sess_key'],$msg);

        if($clientArray['bill_for'] == 'call-tracking-service'){
            $msg = str_replace("[numberofleads]","",$msg);
            $msg = str_replace("[cashperlead]","",$msg);
        }else{
            $msg = str_replace("[numberofleads]",$leadsnum,$msg);
            $msg = str_replace("[cashperlead]",$clientArray["lead_gen_amount_per_qualified_lead"],$msg);
        }

        $email_params = array(
	        "subject" => $template->subject,
	        "msg" => $msg,
	        "emails" => array($clientArray['user_to_send_bill_to'])
	    );
	    Util::sendEmail($email_params);
	}
	//export invoices.
	else if ($ajaxRequest == "exportCsv") {
	
		$clientId = $_REQUEST["clientId"];
		
		$invoice = new Invoice();
		
		//get invoices.
		$invoicesArray = $invoice->getInvoices(array(
			"client_id" => $clientId,
			"invoice_type" => $invoiceType,
			"page" => $pageIndex,
			"page_size" => $pageSize
		));
	}
	
	exit();
}

//if client id is not passed in the URL and not set in the session.
if (!isset($_GET["client_id"]) && !isset($_SESSION["billing_log"]["client_id"])) {
	
	exit();
}
else {
	
	if (isset($_GET["client_id"])) {
		
		$_SESSION["billing_log"]["client_id"] = $_GET["client_id"];
		
		$_GET["invoice_type"] = "normal";
	}
}

$clientId = $_SESSION["billing_log"]["client_id"];

//if invoice type is not passed in the URL and not set in the session.
if (!isset($_GET["invoice_type"]) && !isset($_SESSION["billing_log"]["invoice_type"])) {
	
	$invoiceType = "normal";
}
else {
	
	if (isset($_GET["invoice_type"])) {
		
		$_SESSION["billing_log"]["invoice_type"] = $_GET["invoice_type"];
	}
}

$invoiceType = $_SESSION["billing_log"]["invoice_type"];

//what page.
if (!isset($_GET["page"])) {
	
	$pageIndex = 0;
}
else {
	
	$pageIndex = intval($_GET["page"]);
}

//how many entries in one page.
$pageSize = 15;

//max number of pages buttons to show. Should be odd number.
$maxPagesButtons = 5;

$client = new Client();

//get clients.
$clientsArray = $client->getClients(array("client_id" => $clientId));

$invoice = new Invoice();

//get invoices.
$invoicesArray = $invoice->getInvoices(array(
	"client_id" => $clientId,
	"invoice_type" => $invoiceType,
	"page" => $pageIndex,
	"page_size" => $pageSize
));

$invoicesTotalAmount = 0;

//loop through invoices.
for ($i = 0; $i < count($invoicesArray); $i++) {
	
	$invoiceArray = $invoicesArray[$i];
	
	$billNotPaidPastDueDays = $invoiceArray["bill_not_paid_past_due_days"];
	$invoiceDateMysqlFormat = $invoiceArray["date_mysql_format"];
	
	$invoiceDateArray = explode("-", $invoiceDateMysqlFormat);
	
	$invoiceDateTS = mktime(0, 0, 0, $invoiceDateArray[1], $invoiceDateArray[2], $invoiceDateArray[0]);
	
	$dueDateTS = strtotime("+" . $billNotPaidPastDueDays . " day", $invoiceDateTS);
	
	$dueDateFormatted = date("m-d-Y", $dueDateTS);
	
	$invoicesArray[$i]["dueDate"] = $dueDateFormatted;
	
	$invoiceDetails = unserialize($invoiceArray["invoice_details"]);
	
	$invoiceTotalAmount = 0;
	
	if ($invoiceDetails) {
	
		foreach ($invoiceDetails as $invoiceDetailArray) {
			
			$invoiceTotalAmount += $invoiceDetailArray["totalPrice"];
		}
	}
	
	if ($invoiceTotalAmount < 0) {
		
		$invoiceTotalAmount = 0;
	}
	
	$invoicesTotalAmount += $invoiceTotalAmount;

    switch($invoiceArray['amount_currency']){
        case "HUF":
        case "TWD":
        case "JPY":
        {
            $invoiceTotalAmount = round($invoiceTotalAmount);
            break;
        }
    }

    $invoicesArray[$i]["startdate"] = strtotime($invoiceArray['from_date_sql']);
    $invoicesArray[$i]["enddate"] = strtotime($invoiceArray['to_date_sql']);
	
	$invoicesArray[$i]["totalAmount"] = $invoiceTotalAmount;
}

//get all invoices count.
$invoicesCount = $invoice->getInvoicesCount(array(
	"client_id" => $clientId,
	"invoice_type" => $invoiceType
));

/*
var_dump($invoicesArray);
echo("<br><br>");
var_dump($invoicesCount);
*/

//how many pages buttons we need to cover all invoices.
$pagesButtonsCount = ceil($invoicesCount / $pageSize);

$pagesButtonsHtml = "<div class='pagination right'>";

//if no entries, or one page only, show no buttons.
if ($pagesButtonsCount <= 1) {}
else {
	
	$firstPageIndex = 0;
	$lastPageIndex = $pagesButtonsCount - 1;
	
	//the very first page.
	$pagesButtonsHtml .= "<a href='?page={$firstPageIndex}'>&laquo;</a>";
	
	//how many (max) pages buttons before and after the current page button.
	$pagesButtonsBeforeAndAfter = ($maxPagesButtons - 1) / 2;
	
	for ($i = $pagesButtonsBeforeAndAfter; $i > 0; $i--) {
		
		if (($pageIndex - $i) >= 0) {
			
			$pagesButtonsHtml .= "<a href='?page=" . ($pageIndex - $i) . "'>" . (($pageIndex - $i) + 1) . "</a>";
		}
	}
	
	$pagesButtonsHtml .= "<a class='active' href='?page=" . $pageIndex . "'>" . ($pageIndex + 1) . "</a>";
	
	for ($i = 1; $i <= $pagesButtonsBeforeAndAfter; $i++) {
		
		if (($pageIndex + $i) <= $lastPageIndex) {
			
			$pagesButtonsHtml .= "<a href='?page=" . ($pageIndex + $i) . "'>" . (($pageIndex + $i) + 1) . "</a>";
		}
	}
	
	//the very last page.
	$pagesButtonsHtml .= "<a href='?page={$lastPageIndex}'>&raquo;</a>";
}

$pagesButtonsHtml .= "</div>";


?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=7" />

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>Call Tracking - Billing Log</title>
	
    <?php include "include/css.php"; ?>
	
	<!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->
	
	<link rel="stylesheet" type="text/css" href="css/billing_log.css" />
	<script type="text/javascript" src="jsmain/billing_log.js"></script>
	<script type="text/javascript">
		var invoicesArray = <?php echo(json_encode($invoicesArray));?>;
	</script>

</head>





<body>
	<div id="hld">
		<div class="wrapper">		<!-- wrapper begins -->

			<?php include_once("include/nav.php"); ?>
			<!-- #header ends -->
			
			<div class="block">
			
				<div class="block_head">
				
					<div class="bheadl"></div>
					<div class="bheadr"></div>
					
					<h2>
						Billing Log for <?php echo($clientsArray[0]["client_name"]) ?> &nbsp; &nbsp; 
						<a href="manage_billing.php" >Manage Billing</a> &nbsp; &nbsp;
                        <!-- <a href="?invoice_type=normal" class="<?php echo($invoiceType == 'normal' ? ' active-anchor' : ''); ?>" id="normal-invoices-button">Invoices</a> &nbsp; &nbsp;
						<a href="?invoice_type=regenerate" class="<?php echo($invoiceType == 'regenerate' ? ' active-anchor' : ''); ?>" id="regenerate-invoices-button">Regenerate Invoices</a> &nbsp; &nbsp; -->
					</h2>
					
					<!--<a href="#" style="float:right;" id="exportCsvButton" clientid="<?php echo($clientId); ?>"><img src="images/excel_img.png" title="CSV Export" style="width:50px;" /></a>
					-->
				</div>		<!-- .block_head ends -->
				
				<div class="block_content">
					
					<table id="invoices-table" cellpadding="0" cellspacing="0" width="100%" class="sortable">
					
						<thead>
							<tr>
								<th>Invoice</th>
								<th>Invoice Date</th>
								<th>From Date</th>
								<th>To Date</th>
								<th>Amount</th>
								<th>Amount Paid</th>
								<th>Past Due</th>
								<th></th>
							</tr>
						</thead>
						
						<tbody></tbody>
					</table>
					
					<?php echo($pagesButtonsHtml); ?>
				
				</div>		<!-- .block_content ends -->
	
				<div class="bendl"></div>
				<div class="bendr"></div>
			
			</div>		<!-- .block ends -->
			
			<?php include "include/footer.php"; ?>
</body>
</html>