<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "pool_number";

//Including necessary files
require_once 'include/util.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
global $AccountSid, $AuthToken, $ApiVersion;

if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

$companyNames = $db->getAllCompanyNames();
$companies = $db->getAllCompanies();

$client = new TwilioRestClient($AccountSid, $AuthToken);

$companiesGroup = $db->getAllPoolNumbersGroupBy();

foreach($companiesGroup as $key => $value){
    foreach($companyNames as $entry){
        if(isset($value[$entry]['number']))
            $comp[$entry]['numbers'][$key]  =   $value[$entry]['number'];
        if(!isset($comp[$entry]['reset_call_time']) && isset($value[$entry]['reset_call_time']) )
            $comp[$entry]['reset_call_time'] = $value[$entry]['reset_call_time'];
        if(!isset($comp[$entry]['company_id']) && isset($value[$entry]['company_id']))  
            $comp[$entry]['company_id'] = $value[$entry]['company_id'];
    }
}

$pool_data = $db->getAllPoolNumbersGroupByPoolId();


//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>       <!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                
                <div class="block" id="filter-box" style="width:465px; margin:0 auto; margin-bottom: 25px;">



                <div class="block_head">

                    <div class="bheadl"></div>
                    <div class="bheadr"></div>

                    <h2 id="act_update">Pooling a number</h2>

                </div>      <!-- .block_head ends -->

                <div class="block_content">
                
                    <div id='form_pool'  style="display:block">

                    <form action="#" method="POST" id="poolnumber" >

                        <input type="hidden" id="new_url" value="" />

                        <span>All field required.</span><br/>

                        <label>Country: </label><br/>
                        <select id="country" style="width:426px;">
                            <option value="US" selected="selected" data-region="1">United States (+1)</option>
                            <?php
                            foreach(Util::$supported_countries as $country_code => $country){
                                ?>
                                <option value="<?php echo $country_code; ?>" data-region="<?php echo @$country[2]!="" ? $country[2]:0; ?>"><?php echo $country[0]. " (+".$country[1].")"; ?></option>
                            <?php
                            }
                            ?>
                        </select><br/>
                        <script>
                        function format(state) {
                            if (!state.id) return state.text;
                            return "<img class='flag' width='15' src='images/flags/" + state.id.toLowerCase() + ".gif'/>&nbsp;&nbsp;" + state.text;
                        }
                        $(document).ready(function() {

                            $("#country").select2({
                                formatResult: format,
                                formatSelection: format,
                                escapeMarkup: function(m) { return m; }
                            });

                            $("#company").select2();
                        });
                        </script>
                        <br/>
                        <label>Company Name: </label><br/>

                        <select id="company" name="company" style="width:426px;">
                            <?php foreach($companies as $company){ echo "<option value='".$company['idx']."'>".stripslashes($company['company_name'])."</option>"; } ?>
                        </select>

                        <br />
                        <br />

                        <label>Area Code or Matching this pattern (e.g. 415***EPIC): </label><input name="area_code" class="text small" type="text" id="code"/><br/>

                        <p style="font-size:10px;"><i>To search for area code, enter: 772******* (replace '772' with your area code)</i></p> 

                        <label>Pool Size: </label><input name="pool_number" class="text small" type="text" id="poolsize"/><br/>

                        <p style="font-size:10px;"><i></i></p> 

                        <input type="button" value="Search TOLL-FREE" id="search_tollfree" class="submit long" style="margin-bottom: 5px; float:left">
                        <input type="submit" value="Search" name="submit" id="search_num" class="submit mid" style="margin-bottom: 5px; float:right;">

                    </form>
                    
                    </div>
                    <div id='show_numbres' class='number_content'></div>
                </div>      <!-- .block_content ends -->

                <div class="bendl"></div>
                <div class="bendr"></div>

            </div>      <!-- .block ends -->

             <div class="block">

                <div class="block_head">

                    <div class="bheadl"></div>
                    <div class="bheadr"></div>

                    <h2>Your pool numbers</h2>

                </div>      <!-- .block_head ends -->
                    <div class="block_content">

                        <table  class="sortable" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width: 200px;">Company Name</th>
                                    <th>Pool Numbers</th>
                                    <th style="width: 150px;">Number Display Time</th>
                                    <th style="width: 100px;">Action</th>
                                </tr>
                            </thead>
                            <tbody style="font-size:12px;">
                                <?php 
                                if (sizeof($pool_data) > 0) {
                                    foreach($pool_data as $k => $v) {
                                        if (!empty($v['company_name'])) { ?>
                                            <tr>
                                                <td><?php echo stripslashes($v['company_name']) ;?></td>
                                                <td><?php echo implode(", ", $v['numbers']); ?></td>
                                                <td>
                                                    <input type="text" style="width:50px; height:17px; display: inline-block !important;" disabled="true" value="<?php echo $v['reset_call_time']/3600; ?>"  name="reset_call_time_<?php echo $v['company_id']; ?>"> 
                                                    <a id="SYSTEM_EDIT_RESET_CALL_TIME" href="#" data-params="<?php echo $v['company_id']; ?>"><img title="Edit reset call time" style="vertical-align: middle;padding-left: 10px;" src="images/cog_edit.png"></a>
                                                </td>
                                                <td>
                                                    <a href="pool_edit.php?pool_id=<?php echo $v['pool_id'];?>"><input class="blue_button" type="submit" value="Edit Pool"></a>
                                                </td>

                                            </tr>
                                <?php 
                                        }
                                    }
                                }
                                else {
                                    ?>
                                    <tr>
                                        <td colspan="3">You have no pool numbers.</td>
                                    </tr>
                                    <?php
                                } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="bendl"></div>
                    <div class="bendr"></div>
              </div>

            <!-- #header ends -->
            <?php include "include/footer.php"; ?>

            <script type="text/javascript">

                function updateNumber(pool_id){
                    var url = $('#url_'+pool_id).val();
                    
                    var ajaxparams = {
                            url: url,
                            pool_id: pool_id,
                        };
                    
                    var response = JSONajaxCall({
                        func: "SYSTEM_TWILIO_UPDATE_VOICE_URL",
                        data: ajaxparams,
                        async: true
                    });
                    
                    response.done(function (msg) {
                        if(msg.result == "error"){
                            errMsgDialog(msg.msg);
                            $("#filter-box").unblock();
                            return false;
                        }
                        
                        if(msg.result == "success"){
                            msgDialog('Url Updated!');
                            $("#filter-box").unblock();
                            return false;
                        }
                    });
                }

                $("#buy_numbers").live("submit", function(event) {
                    event.preventDefault();
                    var company_name = $('#company_name').val();
                    var checked_numbers = [];
                    var i = 0;
                    $(".num_select:checked").each(function(){
                           checked_numbers[i] = $(this).val();
                           i++;
                    });
                    var count = checked_numbers.length;

                    if (count == 0) {
                        errMsgDialog("There are no numbers selected.");
                        $("#filter-box").unblock();
                        return false;
                    }

                    var newBlacklist = '<p style="font-size:15px;">\
                    Are you sure you want to buy '+ count +' numbers.\
                    </p>';
                    
                    var $dialog = $('<div></div>').html(newBlacklist).dialog({
                        modal: true,
                        autoOpen: true,
                        minHeight: 177,
                        maxWidth: 587,
                        minWidth: 520,
                        height: 'auto',
                        minWidth: 520,
                        title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\Buy numbers',
                    
                        buttons: [{
                            text: "Ok",
                            click: function (event) { 
                                addNumbersPool( checked_numbers, company_name);
                                $(this).dialog("close");
                                $(this).dialog('destroy').remove();
                            }
                        }, {
                            text: "Cancel",
                            click: function (event) {
                                event.preventDefault();
                                $(this).dialog("close");
                                $(this).dialog('destroy').remove();
                            }
                        }]
                    });
                });

                $("#checkAll").live("click", function () {
                        var checked_numbers =   [];
                        
                        if ($("#checkAll").is(':checked')) {
                            $(".num_select").each(function () {
                                $(this).prop("checked", true);
                                var i = 0;
                                $(".num_select:checked").each(function(){
                                   checked_numbers[i] = $(this).val();
                                   i++;
                                });
                                var count = checked_numbers.length;
                                $('#select_count').html(count);
                            });

                        } else {
                            $(".num_select").each(function () {
                                $(this).prop("checked", false);
                                $('#select_count').html('0');
                            });
                        }
                });

                $('.num_select').live("click",function(){
                    $('#checkAll').prop("checked", false);
                    var checked_numbers =   [];
                    var i = 0;
                    $(".num_select:checked").each(function(){
                           checked_numbers[i] = $(this).val();
                           i++;
                    });
                    var count = checked_numbers.length;
                    $('#select_count').html(count);
                });


                $('#poolnumber').submit(function(event){
                    event.preventDefault();

                    searchNumbers(false);
                });

                $('#search_tollfree').click(function(event){
                    event.preventDefault();

                    searchNumbers(true);
                });

                function searchNumbers(toll_free) {
                    $("#filter-box").block({
                        message: '<h1 style="color:#fff">Loading</h1>',
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .7,
                            color: '#fff !important'
                        }
                    });

                    var code = $("#code").val();
                    var poolsize = $("#poolsize").val();
                    var company = $("#company").val();
                    var country = $("#country").val();

//                    if(code == ""){
//                        errMsgDialog("Please enter code");
//                        $("#filter-box").unblock();
//                        event.preventDefault();
//                    }else if(!$.isNumeric(code)){
//                        errMsgDialog("Please enter a valid code");
//                        $("#filter-box").unblock();
//                        event.preventDefault();
//                    }else
                    if(poolsize == ""){
                        errMsgDialog("Please enter pool size");
                        $("#filter-box").unblock();
                        event.preventDefault();
                    }else if(!$.isNumeric(poolsize)){
                        errMsgDialog("Please enter a valid pool size");
                        $("#filter-box").unblock();
                        event.preventDefault();
//                    }else
//                    if(code.length>3){
//                        errMsgDialog("Invalid Code Please enter a valid code");
//                        $("#filter-box").unblock();
//                        event.preventDefault();
                    }else{

                    /*

                    else if(!check_code(code,poolsize,company,source)){

                        event.preventDefault();

                    }*/
                        var buy = 0;
                        buyNumbers(code, poolsize, country, company, buy, toll_free);

                    }
                }

                function buyNumbers(code, poolsize, country, company, buy, toll_free){
                    var ajaxparams = {
                            poolsize : poolsize,
                            area_code: code,
                            company: company,
                            country: country,
                        };

                        if (toll_free) ajaxparams.toll_free = true;

                        var response = JSONajaxCall({
                            func: "SYSTEM_TWILIO_GET_ADD_NUMBER_POOL",
                            data: ajaxparams,
                            async: true
                        });                        

                        response.done(function (msg) {

                            //console.log(msg);

                            if(msg.result == "error")
                            {
                                errMsgDialog(msg.err_msg);
                                $("#filter-box").unblock();
                                return false;
                            }

                            if(msg.result == "none")
                            {
                                infoMsgDialog("No numbers were found. Please try again.");
                                $("#filter-box").unblock();
                                return false;
                            }

                            if(msg.result == "success"){
                                $( "#form_pool" ).hide();
                                $("#show_numbres").append(msg.details);
                                $("#act_update").html("Avaliable Numbers ("+msg.count+")");
                                $("#filter-box").unblock();
                                return false;
                            }
                        });
                    
                }

                function check_code(code,poolsize,company){

                    var ajaxparams = {
                        postal_code: code,
                        pool_number: poolsize,
                        company    : company,
                    };

                    var response = JSONajaxCall({
                        func: "SYSTEM_TWILIO_GET_AVAIL_NUMBERS_AREACODE",
                        data: ajaxparams,
                        async: true
                    });

                    response.done(function (msg) {

                        //console.log(msg);

                        if(msg.result == "error")
                        {
                            errMsgDialog(msg.err_msg);
                            $("#filter-box").unblock();
                            return false;
                        }

                        if(msg.result == "none")
                        {
                            infoMsgDialog("No numbers were found. Please try again.");
                            $("#filter-box").unblock();
                            return false
                        }

                        if(msg.result == "success"){
                            msgDialog('Polling is done Thanks');
                            $("#filter-box").unblock();
                            return false;
                        }
                    }); 
                }

                function addNumbersPool( add_numbers, company_name){

                    $("#filter-box").block({
                        message: '<h1 style="color:#fff">Saving Numbers</h1>',
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .7,
                            color: '#fff !important'
                        }
                    });
                    
                    var ajaxparams = {
                        add_numbers: add_numbers,
                        company_name: company_name,
                        country: $('#country').val(),
                        call_handler_url: document.location.href.split("pool_number.php")[0] + 'handle_incoming_call.php'
                    };

                    var response = JSONajaxCall({
                        func: "SYSTEM_TWILIO_ADDING_SELECTED_NUMBER_NEW_POOL",
                        data: ajaxparams,
                        async: true
                    });
                    
                    response.done(function (msg) {
                        if(msg.result == "error"){
                            errMsgDialog(msg.msg);
                            $("#filter-box").unblock();
                            return false;
                        }
                        if(msg.result == "success"){
                            msgDialog(msg.msg);
                            $('.ui-button').click(function(){;
                                if('.ui-icon-info'){
                                    var url = 'pool_number.php';
                                    window.location.replace(url);
                                }
                            });
                            $("#filter-box").unblock();
                            return false;
                        }
                    });
                }
                </script>

            </div>
        </div>
    </body>
</html>