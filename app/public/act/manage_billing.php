<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
require_once('include/Pagination.php');
$db = new DB();

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if ($_SESSION['permission'] < 1 || $lcl<2) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once("include/db.company.php");
require_once("include/db.client.php");

function curPageURL() {
    $pageURL = 'http';
    $request_uri = substr($_SERVER['REQUEST_URI'],0,stripos($_SERVER['REQUEST_URI'],"manage_billing.php"));

    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["HTTP_HOST"].":".$_SERVER["SERVER_PORT"].$request_uri;
    } else {
        $pageURL .= $_SERVER["HTTP_HOST"].$request_uri;
    }

    return $pageURL;
}

//send requests to the cronapi to add/edit/delete client jobs.
function sendCronApiRequest($command, $params) {
	
	$curlHandle = curl_init();
	
	curl_setopt($curlHandle, CURLOPT_URL, "http://cronapi.web1syndication.com/{$command}");
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curlHandle, CURLOPT_POST, true);
	
	$postBody = "";
	
	foreach ($params as $key => $value) {
		
		$postBody .= "{$key}=" . urlencode($value) . "&";
	}
	
	rtrim($postBody, '&');
	
	curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postBody);
	
	$response = curl_exec($curlHandle);
	
	curl_close($curlHandle);
	
	return ($response);
}

if (isset($_REQUEST["ajaxRequest"])) {
	
	$ajaxRequest = $_REQUEST["ajaxRequest"];

	//add or edit a client.
	if ($ajaxRequest == "saveAddClient" || $ajaxRequest == "saveEditClient") {
		
		$companyId = $_REQUEST["companyId"];
		
		if ($companyId == "0") {
			
			$companyId = null;
		}
		
		$outgoingNumbers = $_REQUEST["outgoingNumbers"];
		
		if ($outgoingNumbers == "") {
			
			$outgoingNumbers = null;
		}
		else {
			
			$outgoingNumbers = explode(",", $outgoingNumbers);
			
			$outgoingNumbers = serialize($outgoingNumbers);
		}
		
		$billFor = $_REQUEST["billFor"];
		
		if ($billFor == "") {
			
			$billFor = null;
		}
		
		$leadGenBillFor = $_REQUEST["leadGenBillFor"];
		
		if ($leadGenBillFor == "") {
			
			$leadGenBillFor = null;
		}
		
		$billByPhoneCode = $_REQUEST["billByPhoneCode"];
		
		if ($billByPhoneCode == "") {
			
			$billByPhoneCode = null;
		}
		else {
			
			$billByPhoneCode = explode(",", $billByPhoneCode);
			
			$billByPhoneCode = serialize($billByPhoneCode);
		}
		
		$freeLeadsNumber = $_REQUEST["freeLeadsNumber"];

		$lead_gen_delay_period = $_REQUEST["lead_gen_delay_period"];
		$lead_gen_delay_email = $_REQUEST["lead_gen_delay_email"];
		
		$amountPerQualifiedLead = $_REQUEST["amountPerQualifiedLead"];
		$amountPerQualifiedLeadCurrency = $_REQUEST["amountPerQualifiedLeadCurrency"];
		
		$flatRatePerMonth = $_REQUEST["flatRatePerMonth"];
		$perNumberFee = $_REQUEST["perNumberFee"];
		$perMinuteFee = $_REQUEST["perMinuteFee"];
		$callTrackingCurrency = $_REQUEST["callTrackingCurrency"];
		
		$userToSendBillTo = $_REQUEST["userToSendBillTo"];
		
		if ($userToSendBillTo == "") {
			
			$userToSendBillTo = null;
		}
		
		$frequency = $_REQUEST["frequency"];
		
		$billNotPaidPastDueDays = $_REQUEST["billNotPaidPastDueDays"];

        $date = new DateTime($_REQUEST["startDate"],new DateTimeZone("UTC"));

        $timechange = "";
        switch($frequency)
        {
            case "daily":{
                $timechange = "+1 day";
                break;
            }
            case "weekly":{
                $timechange = "+1 week";
                break;
            }
            case "monthly":{
                $timechange = "+1 month";
                break;
            }
            case "bimonthly":{
                $timechange = "+2 months";
                break;
            }
            case "yearly":{
                $timechange = "+1 year";
                break;
            }
        }

        $date->modify($timechange);
		
		$startDate = $date->format("m/d/Y");
		
		if (trim($startDate) != "") {
			
			$startDateArray = explode("/", $startDate);
			
			$startDate = $startDateArray[2] . "-" . $startDateArray[0] . "-" . $startDateArray[1];
		}
		else {
			
			$startDate = null;
		}
		
		$invoiceEmailTemplate = $_REQUEST["invoiceEmailTemplate"];
		
		$clientName = $_REQUEST["clientName"];
		$address = $_REQUEST["address"];
		$city = $_REQUEST["city"];
		$state = $_REQUEST["state"];
		$zipCode = $_REQUEST["zipCode"];
		
		
		$client = new Client();
		
		if ($ajaxRequest == "saveAddClient") {
			
			//add client.
			$clientId = $client->addClient(array(
				"company_id" => $companyId,
				"outgoing_numbers" => $outgoingNumbers,
				"bill_for" => $billFor,
				"lead_gen_bill_for" => $leadGenBillFor,
				"lead_gen_bill_by_phone_codes" => $billByPhoneCode,
				"lead_gen_free_leads_number" => $freeLeadsNumber,
				"lead_gen_delay_period" => $lead_gen_delay_period,
				"lead_gen_delay_email" => $lead_gen_delay_email,
				"lead_gen_amount_per_qualified_lead" => $amountPerQualifiedLead,
				"lead_gen_amount_per_qualified_lead_currency" => $amountPerQualifiedLeadCurrency,
				"call_tracking_flat_rate_per_month" => $flatRatePerMonth,
				"call_tracking_per_number_fee" => $perNumberFee,
				"call_tracking_per_minute_fee" => $perMinuteFee,
				"call_tracking_currency" => $callTrackingCurrency,
				"user_to_send_bill_to" => $userToSendBillTo,
				"frequency" => $frequency,
				"bill_not_paid_past_due_days" => $billNotPaidPastDueDays,
				"startdate" => $startDate,
				"invoice_email_template" => $invoiceEmailTemplate,
				"client_name" => $clientName,
				"address" => $address,
				"city" => $city,
				"state" => $state,
				"zip_code" => $zipCode
			));

            $date = new DateTime($startDate,new DateTimeZone("UTC"));



            $startDate = $date->format("m/d/Y");
			//send request to add cronapi.
			$response = sendCronApiRequest("addCron", array(
				"url" => curPageURL()."invoice.php?client_id={$clientId}",
				"date" => $date->format("Y-m-d"),
				"freq" => $frequency
			));

			//decode the json response.
			$responseArray = json_decode($response, true);
			
			$cronId = $responseArray["cronid"];
			
			//save the cronid for the client.
			$client->updateClientCronId($clientId, $cronId);
		}
		else if ($ajaxRequest == "saveEditClient") {
		
			$clientId = $_REQUEST["clientId"];
			$cronId = $_REQUEST["cronId"];
		
			//edit client.
			$client->editClient(array(
				"company_id" => $companyId,
				"outgoing_numbers" => $outgoingNumbers,
				"bill_for" => $billFor,
				"lead_gen_bill_for" => $leadGenBillFor,
				"lead_gen_bill_by_phone_codes" => $billByPhoneCode,
				"lead_gen_free_leads_number" => $freeLeadsNumber,
				"lead_gen_delay_period" => $lead_gen_delay_period,
				"lead_gen_delay_email" => $lead_gen_delay_email,
				"lead_gen_amount_per_qualified_lead" => $amountPerQualifiedLead,
				"lead_gen_amount_per_qualified_lead_currency" => $amountPerQualifiedLeadCurrency,
				"call_tracking_flat_rate_per_month" => $flatRatePerMonth,
				"call_tracking_per_number_fee" => $perNumberFee,
				"call_tracking_per_minute_fee" => $perMinuteFee,
				"call_tracking_currency" => $callTrackingCurrency,
				"user_to_send_bill_to" => $userToSendBillTo,
				"frequency" => $frequency,
				"bill_not_paid_past_due_days" => $billNotPaidPastDueDays,
				"startdate" => $startDate,
				"invoice_email_template" => $invoiceEmailTemplate,
				"client_name" => $clientName,
				"address" => $address,
				"city" => $city,
				"state" => $state,
				"zip_code" => $zipCode,
				"client_id" => $clientId
			));
			
			//send request to edit cronapi.
			$response = sendCronApiRequest("editCron", array(
				"cronid" => $cronId,
				"url" => curPageURL()."invoice.php?client_id={$clientId}",
				"freq" => $frequency
			));
		}
		
	}
	//delete a client.
	else if ($ajaxRequest == "deleteClient") {
		
		$clientId = $_REQUEST["clientId"];
		$cronId = $_REQUEST["cronId"];
		
		$client = new Client();
		
		//delete client.
		$client->deleteClient(array("client_id" => $clientId));
		
		//send request to delete cronapi.
		$response = sendCronApiRequest("removeCron", array(
			"cronid" => $cronId
		));
	}
    else if($ajaxRequest == "saveTemplate") {
        if(isset($_REQUEST['template']) && $_REQUEST['template']!=""){
            $db->setVar("invoice_template_colors",serialize($_REQUEST['template']));
        }
    }
	//save paypal integration info.
	else if ($ajaxRequest == "saveIntegratePaypalData") {
		
		$paypalIntegrationOption = $_REQUEST["paypalIntegrationOption"];
		$paypalIntegrationId = $_REQUEST["paypalIntegrationId"];
		
		$db->setVar("paypal_integration_option", $paypalIntegrationOption);
		$db->setVar("paypal_integration_id", $paypalIntegrationId);
	}
	//check paypal login
	else if ($ajaxRequest == "verifyPaypalLogin") {
	    $email = $_POST['email'];
	    $pass = $_POST['pass'];
	    
	    $return = array(
		"status" => "ok"
	    );
	    
	    $cookie_file = "cookie";

	    $post = array(
		"bp_ks1" => "",
		"bp_ks2" => "",
		"bp_ks3" => "",
		"bp_mid" => "v=1;a1=na~a2=na~a3=na~a4=Mozilla~a5=Netscape~a6=5.0 (Windows)~a7=na~a8=na~a9=true~a10=na~a11=true~a12=Win32~a13=na~a14=Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0~a15=false~a16=na~a17=na~a18=www.paypal.com~a19=na~a20=na~a21=na~a22=na~a23=1920~a24=1080~a25=24~a26=1040~a27=na~a28=na~a29=na~a30=na~a31=na~a32=na~a33=na~a34=na~a35=na~a36=na~a37=na~a38=na~a39=na~a40=na~a41=na~a42=na~",
		"browser_name" => "Firefox",
		"browser_version" => "22",
		"browser_version_full" => "22.0",
		"flow_name" => "",
		"fso" => "2gnunCNhhhMFT6Y4Ql8TpWp5ZXFNMKB-LqdITAdy5lfqDT9oLMFJD8jMrDnQ7mq9XIEwYW",
		"login_email" => $email,
		"login_password" => $pass,
		"operating_system" => "Windows",
		"submit.x" => "Log In"
	    );
	    
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; Windows NT 6.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5");
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_HEADER, 1);
	    curl_setopt($ch, CURLOPT_URL, "https://www.paypal.com/us/cgi-bin/webscr?cmd=_login-submit");
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
	    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
	    
	    $html = curl_exec($ch);
	    
	    $exploded = explode('name="login_email"', $html);
	    
	    if (sizeof($exploded) > 1) {
		$return["status"] = "error";
		$return["error"] = "Invalid login details.";
	    }
	    else {
            $error = addslashes(curl_error($ch));
            curl_close($ch);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; Windows NT 6.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_URL, "https://www.paypal.com/webapps/customerprofile/summary.view");
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);

            $html = curl_exec($ch);

            $error = addslashes(curl_error($ch));
            curl_close($ch);

            $exploded = explode('<div id="merchant" class="section">', $html);

            if (sizeof($exploded) == 0) {
                $return["options"] = array(
                "email" => $email
                );
            }
            else {
                $exploded2 = explode('<div class="details">', $exploded[1]);
                $exploded3 = explode('</div>', $exploded2[1]);

                $merchantID = trim(strip_tags($exploded3[0]));

                $db->setVar("paypal_login_email", $email);
				$db->setVar("paypal_login_merchantID", $merchantID);

                $return["options"] = array(
                "email" => $email,
                "merchantID" => $merchantID
                );
            }
	    }
	    
	    echo json_encode($return);
	}
	//clear Paypal integration
	else if ($ajaxRequest == "clearPaypalIntegration") {
	    $db->setVar("paypal_integration_option", "");
	    $db->setVar("paypal_integration_id", "");
	}
	
	exit();
}

$client = new Client();

//get clients.
$clientsArray = $client->getClients(array("all" => "all"));

$company = new Company();

//get companies.
$companiesArray = $company->getCompanies();

//get outgoing numbers for all companies.
$outgoingNumbersArray = $company->getOutgoingNumbers(array("all" => "all"));

//get phone codes for all companies.
$phoneCodesArray = $company->getPhoneCodes();

$paypalIntegrationOption = $db->getVar("paypal_integration_option");
$paypalIntegrationId = $db->getVar("paypal_integration_id");

$paypalIntegrationInfo = array($paypalIntegrationOption, $paypalIntegrationId);

$custom = @unserialize($db->getVar('invoice_template_colors'));

?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=7" />

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>Call Tracking - Manage Billing</title>
	
    <?php include "include/css.php"; ?>
	
	<!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->
	
	<link rel="stylesheet" type="text/css" href="css/manage_billing.css<?php echo "?".$build_number; ?>" />
	<script type="text/javascript" src="jsmain/manage_billing.js<?php echo "?".$build_number; ?>"></script>
    <script type="text/javascript" src="js/PhoneFormat.js<?php echo "?".$build_number; ?>"></script>
 	<script type="text/javascript">
		var clientsArray = <?php echo(json_encode($clientsArray));?>;
		var companiesArray = <?php echo(json_encode($companiesArray)); ?>;
		var outgoingNumbersArray = <?php echo(json_encode($outgoingNumbersArray)); ?>;
		var phoneCodesArray = <?php echo(json_encode($phoneCodesArray)); ?>;
		var paypalIntegrationInfo = <?php echo(json_encode($paypalIntegrationInfo)); ?>;
        var users = <?php echo json_encode($db->getAllUsersLimited()); ?>;
        var timezone = <?php global $TIMEZONE; $tz = new DateTimeZone($TIMEZONE); echo $tz->getOffset(new DateTime("now")); ?>;
	</script>
    <style type="text/css">
        .show-client-info-dialog {
            top: 80px !important;
        }
    </style>
</head>





<body>
	<div id="hld">
		<div class="wrapper">		<!-- wrapper begins -->

			<?php include_once("include/nav.php"); ?>
			<!-- #header ends -->
			
			
			
			<div class="block">
			
				<div class="block_head">
				
					<div class="bheadl"></div>
					<div class="bheadr"></div>
					
					<h2>
						Manage Billing &nbsp; &nbsp; 
						<a href="#" id="add-client-button">Add Client</a> &nbsp; &nbsp; 
						<a href="#" id="integrate-paypal-button">Integrate Paypal</a> &nbsp; &nbsp;
						<a href="#" id="invoice-settings-button">Setup Invoice Template</a> &nbsp; &nbsp; 
						<a href="email_tracking_setup.php">Email Tracking</a>
					</h2>
					
				</div>		<!-- .block_head ends -->
				

				<div class="block_content">
					
					<table id="clients-table" cellpadding="0" cellspacing="0" width="100%" class="sortable">
					
						<thead>
							<tr>
								<th>Client Name</th>
								<th>Type</th>
								<th>Frequency</th>
								<th>Last Invoice</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						
						<tbody>
						</tbody>
					</table>
				
				</div>		<!-- .block_content ends -->
	
				<div class="bendl"></div>
				<div class="bendr"></div>
			
			</div>		<!-- .block ends -->
			
			<?php include "include/footer.php"; ?>
	
	<div id="invoice-settings-dialog" style="display:none;">
	
		<div class="block" style="z-index: 9000;">
		
			<div class="block_head">
			
				<div class="bheadl"></div>
				<div class="bheadr"></div>
				
				<h2>Invoice Settings</h2>
			</div>		<!-- .block_head ends -->

            <script type="text/javascript">
                var template_defaults = [
                    '#43a7c8',
                    '#f6f6f6',
                    '#656666',
                    '#3f4445',
                    '#ffffff',
                    '#43a7c8',
                    '#ffffff'
                ];
                function resetTemplate()
                {
                    $(".color-preview").each(function(i,v){
                        $(v).attr("style","background-color:"+template_defaults[i]);
                    });
                }
            </script>

			<div class="block_content dialog-content">
				<form action="javascript: void(0);">
				
					<div class="color-option">
						<label>From &amp; To Heading Box Color:</label>
						<div class="color-preview" style="background-color:<?php echo $custom[0] != "" ? $custom[0] : "#43a7c8"; ?>;"></div>
						<div class="color-wheel-wrapper"><div class="color-wheel"></div></div>
					</div>

                    <div class="color-option">
                        <label>From &amp; To Text Background Color:</label>
                        <div class="color-preview" style="background-color:<?php echo $custom[1] != "" ? $custom[1] : "#f6f6f6"; ?>;"></div>
                        <div class="color-wheel-wrapper" style="display:none;"><div class="color-wheel"></div></div>
                    </div>

                    <div class="color-option">
                        <label>From &amp; To Text Color:</label>
                        <div class="color-preview" style="background-color:<?php echo $custom[2] != "" ? $custom[2] : "#656666"; ?>;"></div>
                        <div class="color-wheel-wrapper" style="display:none;"><div class="color-wheel"></div></div>
                    </div>
					
					<div class="color-option">
						<label>Items Heading Background Color:</label>
						<div class="color-preview" style="background-color:<?php echo $custom[3] != "" ? $custom[3] : "#3f4445"; ?>;"></div>
						<div class="color-wheel-wrapper" style="display:none;"><div class="color-wheel"></div></div>
					</div>
					
					<div class="color-option">
						<label>Items Heading Text Color:</label>
						<div class="color-preview" style="background-color:<?php echo $custom[4] != "" ? $custom[4] : "#ffffff"; ?>;"></div>
						<div class="color-wheel-wrapper" style="display:none;"><div class="color-wheel"></div></div>
					</div>
					
					<div class="color-option">
						<label>Amount Due Heading Color:</label>
						<div class="color-preview" style="background-color:<?php echo $custom[5] != "" ? $custom[5] : "#43a7c8"; ?>;"></div>
						<div class="color-wheel-wrapper" style="display:none;"><div class="color-wheel"></div></div>
					</div>
					
					<div class="color-option">
						<label>Text Overlay Color:</label>
						<div class="color-preview" style="background-color:<?php echo $custom[6] != "" ? $custom[6] : "#ffffff"; ?>;"></div>
						<div class="color-wheel-wrapper" style="display:none;"><div class="color-wheel"></div></div>
					</div>
					
					<input type="submit" name="submit" style="display: none !important;" />
					
				</form>
			</div>
			
			<div class="block_content">
				<form action="javascript:void(0)">
					<div class="bottom-buttons-div">
						<input type="submit" value="Save" class="submit small" id="invoice-template-save">
						<input type="submit" onclick="previewInvoiceTemplate();" value="Preview" class="submit small">
                        <input type="submit" onclick="resetTemplate();" value="Reset" class="submit small"/>
						<input type="submit" value="Cancel" class="submit small" id="cancel-invoice-settings-button">
					</div>
				</form>
			</div>

			<script type="text/javascript">
				function previewInvoiceTemplate() {

					var custom_colors_array = [];

					$(".color-option").each(function() {
						var color = $(this).find(".color-preview").css("background-color");
						custom_colors_array.push(color);
					});


					window.open('billing_log_invoice.php?invoice_id=0&custom=' + encodeURIComponent(custom_colors_array.join('|||')));
				}
			</script>
			
			<div class="bendl"></div>
			<div class="bendr"></div>
			
		</div>
		
	</div>
	
	<div id="add-client-dialog" style="display:none;">
	
		<div class="block">
		
			<div class="block_head">
			
				<div class="bheadl"></div>
				<div class="bheadr"></div>
				
				<h2>Add Client</h2>
			</div>		<!-- .block_head ends -->
			
			<div class="block_content dialog-content">
				<form action="javascript:void(0)">
				
					<div>
						<div class="form-row">
							<label class="input-label">Company:</label>
							<select id="add-client-companies-select"></select>
						</div>
						
						<div class="form-row">
							<label class="input-label">Outgoing Numbers:</label>
							<div id="outgoing-numbers-div"></div>
							<div class="clear-float"></div>
						</div>
						
						<div class="form-row">
							<label>Bill For:</label><br />
							<div class="left-col">
								<input type="radio" class="radio" name="bill-for-radiogroup" id="lead-gen" value="lead-gen" /> <label for="lead-gen">Lead Gen</label><br />
								<input type="radio" class="radio" name="bill-for-radiogroup" id="call-tracking-service" value="call-tracking-service" /> <label for="call-tracking-service">Call Tracking</label>
							</div>
							<div class="clear-float"></div>
						</div>
						
						<div class="form-row" showForLeadGen style="display:none;">
							<label>Lead Gen - Bill For:</label><br />
                            <div class="left-col">
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-all" value="all" /> <label for="lead-gen-bill-for-all">All</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-45" value="45" /> <label for="lead-gen-bill-for-45">Calls Over 45 seconds</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-90" value="90" /> <label for="lead-gen-bill-for-90">Calls Over 90 seconds</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-180" value="180" /> <label for="lead-gen-bill-for-180">Calls Over 3 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-300" value="300" /> <label for="lead-gen-bill-for-300">Calls Over 5 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-420" value="420" /> <label for="lead-gen-bill-for-420">Calls Over 7 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-540" value="540" /> <label for="lead-gen-bill-for-540">Calls Over 9 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-1200" value="1200" /> <label for="lead-gen-bill-for-1200">Calls Over 20 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-2400" value="2400" /> <label for="lead-gen-bill-for-2400">Calls Over 40 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-answered" value="answered" /> <label for="lead-gen-bill-for-answered">Answered Calls</label><br />
                            </div>
                            <div class="right-col">
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-30" value="30" /> <label for="lead-gen-bill-for-30">Calls Over 30 seconds</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-60" value="60" /> <label for="lead-gen-bill-for-60">Calls Over 60 seconds</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-120" value="120" /> <label for="lead-gen-bill-for-120">Calls Over 2 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-240" value="240" /> <label for="lead-gen-bill-for-240">Calls Over 4 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-360" value="360" /> <label for="lead-gen-bill-for-360">Calls Over 6 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-480" value="480" /> <label for="lead-gen-bill-for-480">Calls Over 8 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-600" value="600" /> <label for="lead-gen-bill-for-600">Calls Over 10 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-1800" value="1800" /> <label for="lead-gen-bill-for-1800">Calls Over 30 minutes</label><br />
                                <input type="radio" class="radio" name="lead-gen-bill-for-radiogroup" id="lead-gen-bill-for-missed" value="missed" /> <label for="lead-gen-bill-for-missed">Missed Calls</label><br />
                            </div>
							<div class="clear-float"></div>
						</div>
						
						<div class="form-row" showForLeadGen style="display:none;">
							<label>Bill By Phone Codes:</label><br />
							<div id="bill-by-phone-code-div"></div>
							<div class="clear-float"></div>
						</div>
						
						<div class="form-row" showForLeadGen showForPhoneCodes style="display:none;">
							<label class="input-label">Review Settings:</label>
							<br/ >
							<label class="input-label">Window days:</label>
							<select id="lead_gen_delay_period">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7" selected="selected">7</option>
							</select>
							<div style="padding-top: 10px;">
								<label class="input-label">Notification Email:</label>
								<input type="text" class="text dialog-medium-text" id="lead_gen_delay_email" />
							</div>
						</div>
						
						<div class="form-row" showForLeadGen style="display:none;">
							<label class="input-label">Number Of Free Leads:</label>
							<input type="text" class="text dialog-small-text" id="free-leads-number" />
						</div>
						
						<div class="form-row" showForLeadGen style="display:none;">
							<label class="input-label">Amount Per Qualified Lead:</label>
							<select id="amount-per-qualified-lead-currency-select">
                                <option value="USD">US Dollar (USD)</option>
                                <option value="CAD">Canadian Dollar (CAD)</option>
                                <option value="GBP">British Pound (GBP)</option>
                                <option value="AUD">Australian Dollar (AUD)</option>
                                <option value="EUR">Euro (EUR)</option>
                                <option value="CZK">Czech Koruna (CZK)</option>
                                <option value="DKK">Danish Krone (DKK)</option>
                                <option value="HKD">Hong Kong Dollar (HKD)</option>
                                <option value="HUF">Hungarian Forint (HUF)</option>
                                <option value="ILS">Israeli New Sheqel (ILS)</option>
                                <option value="JPY">Japanese Yen (JPY)</option>
                                <option value="MXN">Mexican Peso (MXN)</option>
                                <option value="NOK">Norwegian Krone (NOK)</option>
                                <option value="NZD">New Zealand Dollar (NZD)</option>
                                <option value="PHP">Philippine Peso (PHP)</option>
                                <option value="PLN">Polish Zloty (PLN)</option>
                                <option value="SGD">Singapore Dollar (SGD)</option>
                                <option value="SEK">Swedish Krona (SEK)</option>
                                <option value="CHF">Swiss Franc (CHF)</option>
                                <option value="TWD">Taiwan New Dollar (TWD)</option>
                                <option value="THB">Thai Baht (THB)</option>
							</select>
							<input type="text" class="text dialog-small-text" id="amount-per-qualified-lead" />
						</div>
						
						<div class="form-row" showForCallTracking style="display:none;">
							<label class="input-label">Flat Rate Per Month:</label>
							<select id="flat-rate-per-month-currency-select">
                                <option value="USD">US Dollar (USD)</option>
                                <option value="CAD">Canadian Dollar (CAD)</option>
                                <option value="GBP">British Pound (GBP)</option>
                                <option value="AUD">Australian Dollar (AUD)</option>
                                <option value="EUR">Euro (EUR)</option>
                                <option value="CZK">Czech Koruna (CZK)</option>
                                <option value="DKK">Danish Krone (DKK)</option>
                                <option value="HKD">Hong Kong Dollar (HKD)</option>
                                <option value="HUF">Hungarian Forint (HUF)</option>
                                <option value="ILS">Israeli New Sheqel (ILS)</option>
                                <option value="JPY">Japanese Yen (JPY)</option>
                                <option value="MXN">Mexican Peso (MXN)</option>
                                <option value="NOK">Norwegian Krone (NOK)</option>
                                <option value="NZD">New Zealand Dollar (NZD)</option>
                                <option value="PHP">Philippine Peso (PHP)</option>
                                <option value="PLN">Polish Zloty (PLN)</option>
                                <option value="SGD">Singapore Dollar (SGD)</option>
                                <option value="SEK">Swedish Krona (SEK)</option>
                                <option value="CHF">Swiss Franc (CHF)</option>
                                <option value="TWD">Taiwan New Dollar (TWD)</option>
                                <option value="THB">Thai Baht (THB)</option>
							</select>
							<input type="text" class="text dialog-small-text" id="flat-rate-per-month" />
						</div>
						
						<div class="form-row" showForCallTracking style="display:none;">
							<label class="input-label">Per Number Fee:</label>
							<select id="per-number-fee-currency-select">
                                <option value="USD">US Dollar (USD)</option>
                                <option value="CAD">Canadian Dollar (CAD)</option>
                                <option value="GBP">British Pound (GBP)</option>
                                <option value="AUD">Australian Dollar (AUD)</option>
                                <option value="EUR">Euro (EUR)</option>
                                <option value="CZK">Czech Koruna (CZK)</option>
                                <option value="DKK">Danish Krone (DKK)</option>
                                <option value="HKD">Hong Kong Dollar (HKD)</option>
                                <option value="HUF">Hungarian Forint (HUF)</option>
                                <option value="ILS">Israeli New Sheqel (ILS)</option>
                                <option value="JPY">Japanese Yen (JPY)</option>
                                <option value="MXN">Mexican Peso (MXN)</option>
                                <option value="NOK">Norwegian Krone (NOK)</option>
                                <option value="NZD">New Zealand Dollar (NZD)</option>
                                <option value="PHP">Philippine Peso (PHP)</option>
                                <option value="PLN">Polish Zloty (PLN)</option>
                                <option value="SGD">Singapore Dollar (SGD)</option>
                                <option value="SEK">Swedish Krona (SEK)</option>
                                <option value="CHF">Swiss Franc (CHF)</option>
                                <option value="TWD">Taiwan New Dollar (TWD)</option>
                                <option value="THB">Thai Baht (THB)</option>
							</select>
							<input type="text" class="text dialog-small-text" id="per-number-fee" />
						</div>
						
						<div class="form-row" showForCallTracking style="display:none;">
							<label class="input-label">Per Minute Usage Fee:</label>
							<select id="per-minute-fee-currency-select">
                                <option value="USD">US Dollar (USD)</option>
                                <option value="CAD">Canadian Dollar (CAD)</option>
                                <option value="GBP">British Pound (GBP)</option>
                                <option value="AUD">Australian Dollar (AUD)</option>
                                <option value="EUR">Euro (EUR)</option>
                                <option value="CZK">Czech Koruna (CZK)</option>
                                <option value="DKK">Danish Krone (DKK)</option>
                                <option value="HKD">Hong Kong Dollar (HKD)</option>
                                <option value="HUF">Hungarian Forint (HUF)</option>
                                <option value="ILS">Israeli New Sheqel (ILS)</option>
                                <option value="JPY">Japanese Yen (JPY)</option>
                                <option value="MXN">Mexican Peso (MXN)</option>
                                <option value="NOK">Norwegian Krone (NOK)</option>
                                <option value="NZD">New Zealand Dollar (NZD)</option>
                                <option value="PHP">Philippine Peso (PHP)</option>
                                <option value="PLN">Polish Zloty (PLN)</option>
                                <option value="SGD">Singapore Dollar (SGD)</option>
                                <option value="SEK">Swedish Krona (SEK)</option>
                                <option value="CHF">Swiss Franc (CHF)</option>
                                <option value="TWD">Taiwan New Dollar (TWD)</option>
                                <option value="THB">Thai Baht (THB)</option>
							</select>
							<input type="text" class="text dialog-small-text" id="per-minute-fee" />
						</div>
						
						<div class="form-row">
							<label class="input-label">User To Send Bill To:</label>
							<select id="user-to-send-bill-to"></select>
						</div>
						
						<div class="form-row">
							<label class="input-label">Frequency:</label>
							<select id="frequency">
								<option value="weekly">Weekly</option>
								<option value="monthly">Monthly</option>
								<option value="bimonthly">Bi Monthly</option>
							</select>
						</div>
						
						<div class="form-row">
							<label>Days Till Bill Is Past Due If Not Paid:</label>
							<input type="text" class="text dialog-small-text" id="bill-not-paid-past-due-days" />
						</div>
						
						<div class="form-row">
							<label class="input-label">Start Date:</label>
							<input type="text" class="text dialog-small-text" id="add-client-start-date" />
						</div>
						
						<div class="form-row" style="display:none;">
							<label>Email Template To Use When Sending Invoice/Auto Bill:</label>
							<select id="invoice-email-template">
								<option value="template1">Template 1</option>
								<option value="template2">Template 2</option>
								<option value="template3">Template 3</option>
							</select>
						</div>
						
						<div class="form-row">
							<label>Company Info:</label><br />
						</div>
						
						<div class="form-row">
							<label class="input-label">Client Name:</label>
							<input type="text" class="text dialog-medium-text" id="client-name" />
						</div>
						
						<div class="form-row">
							<label class="input-label">Address:</label>
							<input type="text" class="text dialog-medium-text" id="address" />
						</div>
						
						<div class="form-row">
							<label class="input-label">City:</label>
							<input type="text" class="text dialog-medium-text" id="city" />
						</div>
						
						<div class="form-row">
							<label class="input-label">State/Region:</label>
							<input type="text" class="text dialog-medium-text" id="state" />
						</div>
						
						<div class="form-row">
							<label class="input-label">Zip Code/Postal Code:</label>
							<input type="text" class="text dialog-small-text" id="zip-code" />
						</div>
					</div>
				
				</form>
			</div>
			
			<div class="block_content">
				<form action="javascript:void(0)">
					<div class="bottom-buttons-div">
						<input type="submit" value="Save" class="submit small" id="save-add-client-button">
						<input type="submit" value="Cancel" class="submit small" id="cancel-add-client-button">
					</div>
				</form>
			</div>
			
			<div class="bendl"></div>
			<div class="bendr"></div>
		</div>
	</div>
	
	<div id="show-client-info-dialog" style="display:none;">
	
		<div class="block">
		
			<div class="block_head">
			
				<div class="bheadl"></div>
				<div class="bheadr"></div>
				
				<h2>Client Info</h2>
			</div>		<!-- .block_head ends -->
			
			<div class="block_content dialog-content">
				<form action="javascript:void(0)">
				
					<div id="show-client-info-div"></div>
				
				</form>
			</div>
			
			<div class="block_content">
				<form action="javascript:void(0)">
					<div class="bottom-buttons-div">
						<input type="submit" value="Close" class="submit small" id="close-client-info-button">
					</div>
				</form>
			</div>
			
			<div class="bendl"></div>
			<div class="bendr"></div>
		</div>
	</div>
	
	<div id="integrate-paypal-dialog" style="display:none;">
	
		<div class="block">
		
			<div class="block_head">
			
				<div class="bheadl"></div>
				<div class="bheadr"></div>
				
				<h2>Integrate Paypal</h2>
			</div>		<!-- .block_head ends -->
			
			<?php
			$paypal_integration_option = $db->getVar("paypal_integration_option");
			$paypal_integration_id = $db->getVar("paypal_integration_id");
			
			$paypal_login_email = $db->getVar("paypal_login_email");
			$paypal_login_merchantID = $db->getVar("paypal_login_merchantID");
			?>
			
			<div class="block_content dialog-content">
			    
			    <div id="paypal-login-wrapper" style="display: <?php if (!empty($paypal_integration_option) && !empty($paypal_integration_id) && !empty($paypal_login_email) && !empty($paypal_login_merchantID)) { ?>none<?php } else { ?>block<?php } ?>;">
			    
				<h2>Step 1 - Login with your Paypal account</h2>
			    
				<form action="javascript:void(0)">
				
					<div>
						
						<div>
						
							<div class="form-row">
								<label for="paypal-email-address" style="width: 150px; float: left;">Paypal Email Address</label> <span><input type="text" class="text dialog-medium-text" id="paypal-email-address" /></span>
								<br clear="all" />
								<div style="padding-top: 5px;">
								    <label for="paypal-password" style="width: 150px; float: left;">Paypal Password</label> <span><input type="password" class="text dialog-medium-text" id="paypal-password" /></span>
								</div>
							</div>
							
							<div id="paypal-login-response" style="height: 20px; text-align: center; padding-top: 10px;"></div>
							
						</div>
						
					</div>
				
				</form>
			    </div>
			    
			    <div id="paypal-choose-option-wrapper" style="display: <?php if (!empty($paypal_integration_option) && !empty($paypal_integration_id) && !empty($paypal_login_email) && !empty($paypal_login_merchantID)) { ?>block<?php } else { ?>none<?php } ?>;">
			    
				<h2>Step 2 - Select Paypal Email or Paypal Merchant ID</h2>
			    
				<form action="javascript:void(0)">
				
					<div>
						
						<div>
						
							<div class="form-row">
								<div id="paypal-options-merchant-id-wrapper">
								    <input type="radio" class="radio" name="paypal-option-radiogroup" id="paypal-merchant-id" value="paypal-merchant-id" <?php if ($paypal_integration_id == $paypal_login_merchantID) { ?>checked="checked"<?php } ?> /> <label for="paypal-merchant-id">Paypal Merchant ID</label> <span><input type="text" class="text dialog-medium-text" id="paypal-merchant-id-text" style="background: #f8f8f8; width: 280px !important;" disabled="disabled" value="<?php echo $paypal_login_merchantID; ?>" /></span><br />
								</div>
								
								<div id="paypal-options-email-id-wrapper" style="padding-top: 5px;">
								    <input type="radio" class="radio" name="paypal-option-radiogroup" id="paypal-email" value="paypal-email" <?php if ($paypal_integration_id == $paypal_login_email) { ?>checked="checked"<?php } ?> /> <label for="paypal-email">Paypal Email</label> <span style="padding-left: 45px;"><input type="text" class="text dialog-medium-text" id="paypal-email-text" style="background: #f8f8f8; width: 280px !important;" disabled="disabled" value="<?php echo $paypal_login_email; ?>" /></span>
								</div>
							</div>
							
							
						</div>
						
					</div>
				
				</form>
			    </div>
			</div>
			
			<div class="block_content">
				<form action="javascript:void(0)">
					<div id="check-login-buttons" class="bottom-buttons-div" style="display: <?php if (!empty($paypal_integration_option) && !empty($paypal_integration_id) && !empty($paypal_login_email) && !empty($paypal_login_merchantID)) { ?>none<?php } else { ?>block<?php } ?>;">
					    <input type="submit" value="Verify" class="submit small" id="verify-paypal-login" />
					    <input type="submit" value="Cancel" class="submit small" id="cancel-integrate-paypal-button" />
					</div>
					
					<div id="save-option-buttons" class="bottom-buttons-div" style="display: <?php if (!empty($paypal_integration_option) && !empty($paypal_integration_id) && !empty($paypal_login_email) && !empty($paypal_login_merchantID)) { ?>block<?php } else { ?>none<?php } ?>;">
					    <input type="submit" value="Save" class="submit small" id="save-integrate-paypal-button" />
					    <input type="submit" value="Remove" class="submit small" id="change-account-integrate-paypal-button" onclick="clearPaypalIntegration();" />
					    <input type="submit" value="Cancel" class="submit small" id="cancel-integrate-paypal-button" />
					</div>
				</form>
			</div>
			
			<div class="bendl"></div>
			<div class="bendr"></div>
		</div>
	</div>
	
	<script type="text/javascript">
	    $("#verify-paypal-login").click(function() {
            $('#paypal-login-response').html('<img src="images/loading.gif" height="15" /> Verifying...');

            var email = $('#paypal-email-address').val();
            var pass = $('#paypal-password').val();

            $.post('manage_billing.php?ajaxRequest=verifyPaypalLogin', { email: email, pass: pass} , function(data) {
                if (data.status == "error") {
                $('#paypal-login-response').html('<span style="color: #FF0000;">' + data.error + '</span>');
                }
                else {
                $('#paypal-login-wrapper').hide();
                $('#paypal-choose-option-wrapper').show();
                $('#check-login-buttons').hide();
                $('#save-option-buttons').show();

                if (typeof(data.options.email) != "undefined") {
                    $('#paypal-email').prop('checked', 'true');
                    $('#paypal-options-email-id-wrapper').show();
                    $('#paypal-email-text').val(data.options.email);
                }
                else {
                    $('#paypal-options-email-id-wrapper').hide();
                    $('#paypal-email-text').val('');
                }

                if (typeof(data.options.merchantID) != "undefined") {
                    $('#paypal-merchant-id').prop('checked', 'true');
                    $('#paypal-options-merchant-id-wrapper').show();
                    $('#paypal-merchant-id-text').val(data.options.merchantID);
                }
                else {
                    $('#paypal-options-merchant-id-wrapper').hide();
                    $('#paypal-merchant-id-text').val('');
                }
                }
            }, "json");
	    });
	    
	    function clearPaypalIntegration() {
	    	if (confirm('Are you sure you want to remove the account ?')) {
	            $('#paypal-login-wrapper').show();
	            $('#paypal-choose-option-wrapper').hide();
	            $('#check-login-buttons').show();
	            $('#save-option-buttons').hide();

	            $("#paypal-email-address").val('');
	            $("#paypal-password").val('');
	            $("#paypal-login-response").html('');

	            $.get('manage_billing.php?ajaxRequest=clearPaypalIntegration', function() { });
            }
	    }
	</script>
	
</body>
</html>