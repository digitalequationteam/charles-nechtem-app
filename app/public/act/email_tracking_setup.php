<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$db = new DB();
//This is an admin-only page.
//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}

if(@$_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

global $AccountSid, $AuthToken, $ApiVersion;
$client = new TwilioRestClient($AccountSid, $AuthToken);

if(isset($_REQUEST['company_id']) && @$_REQUEST['delete']=="true")
{
    $company_id = $_REQUEST['company_id'];
    $tracking_settings = $db->getTrackingSettingsForCompany($company_id);
    $emails = json_decode($tracking_settings->emails);

    foreach ($emails as $email) {
        $jsonurl = "http://api.trackingyourleads.com/disableEmail.php?email=".$email;
        $json = $db->curlGetData($jsonurl);
    }

    $db->deleteTrackingSettings($company_id);
    exit();
}

$companies = $db->getAllCompanies();

require_once("include/db.client.php");
$client = new Client();

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'generate_email_address':

            $jsonurl = "http://api.trackingyourleads.com/createEmail.php?act_url=".$_REQUEST['act_url'];

            $json = $db->curlGetData($jsonurl);
            $json_output = json_decode($json);

            ?>
            <?php echo $json_output->email; ?>@trackingyourleads.com
            <?php
            exit();

        break;
        case 'add_email_ui':

        global $AccountSid;

        ?>
            <div class="block" style="margin-bottom: 0px; border-bottom: 0px; background: none;">
                <form action="" onsubmit="return false;">
                    <h3 style="margin-bottom: 10px;">Set Clients and Email Addresses</h3>

                    <strong>Company:</strong>
                    <select class="styled" id="company_id" onchange="loadClients(this.value);" style="width: 250px; display: inline-block !important; margin-bottom: 5px; height: 28px; padding: 4px; margin-right: 51px;">
                        <option value="">Select</option>
                        <?php
                        foreach($companies as $company)
                        {
                            if (!$db->companyHasTrackingSetup($company['idx']) || ($db->companyHasTrackingSetup($company['idx']) && @$_REQUEST['company_id'] == $company['idx'])) {
                                ?><option value="<?php echo $company['idx']?>" <?php if($company_id == $company['idx']){ echo 'selected="selected"';} ?>><?php echo stripslashes(ucwords($company['company_name']));?></option><?php echo "\n";
                            }
                        }
                        ?>
                    </select>

                    <br />
                    <strong>Clients: <span class="tooltip" title="Select Clients to bill.  Client is setup in Manage Billing."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></strong>
                    <br />

                    <div id="tracking_company_clients" style="border: solid 1px #7e7e7e; padding: 10px; margin-top: 5px; margin-bottom: 10px;">Select a company.</div>

                    <div id="emails_distribution" style="display: none;">
                        <strong>Distribution:</strong>

                        <select class="styled" name="distribution" id="distribution" style="width: 150px; display: inline-block !important; margin-bottom: 5px; height: 28px; padding: 4px; margin-right: 0px;">
                            <option value="all">Send to all</option>
                            <option value="even">Evenly Distribute</option>
                        </select>
                    </div>

                    <strong>Email Addresses:</strong> <input type="submit" value="Generate" class="submit mid" id="generate-email-address" style="float: right; margin-right: 0px;" />
                    <br clear="all" />
                    <div id="tracking_company_emails" style="border: solid 1px #7e7e7e; margin-top: 5px; clear: both; margin-bottom: 10px;">
                        <div class="email-wrapper default"><span class="email">Click generate.</span></div>
                    </div>

                    <strong>Billing Criteria: <span class="tooltip" title="Select to Auto Tag or Manually tag your emails for billing."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></strong>

                    <select class="styled" name="billing_tag" id="billing_tag" onchange="changeBillingTag(this.value);" style="width: 100px; display: inline-block !important; margin-bottom: 5px; height: 28px; padding: 4px; margin-right: 0px;">
                        <option value="auto">Auto Tag</option>
                        <option value="manual">Manual Tag</option>
                    </select>

                    <br />

                    <div style="margin-top: 5px; padding-top: 5px; border-top: solid 1px #7e7e7e;">

                        <h3 style="margin-bottom: 10px;">Manage Tags</h3>

                        <label for="tags_client_id" style="display: inline-block; width: 40px;">Client:</label>
                        <select class="styled" id="tags_client_id" style="width: 200px; display: inline-block !important; margin-bottom: 5px; height: 28px; padding: 4px; margin-right: 20px;">
                            <option value="">Select</option>
                        </select>

                        <label for="tags_email" style="display: inline-block; width: 120px;">Email Address: <span class="tooltip" title="Generated email address used for tracking leads."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></label>
                        <select class="styled" id="tags_email" style="width: 200px; display: inline-block !important; margin-bottom: 5px; height: 28px; padding: 4px; margin-right: 0px;">
                            <option value="">Select</option>
                        </select>

                        <div id="tags_area">

                            <div style="width: 98%; margin: 0 auto; background:#AAA; height:1px;"></div>
                            <table class="sortable" style="width: 100%;" id="tags_table">
                                <thead>
                                    <tr>
                                        <th align="left">Tag <span class="tooltip" title="Name used to identify tag."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></th>
                                        <th class="trigger_col" align="left">Trigger <span class="tooltip" title="Keyword identifier for auto tagging."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></th>
                                        <th class="rule_col" align="left">Rule <span class="tooltip" title="Select to exclude a keyword."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></th>
                                        <th align="left">Cost <span class="tooltip" title="Amount per email lead."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></th>
                                        <th align="left">Order <span class="tooltip" title="Position of tags in list."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></th>
                                        <th align="left">Delete</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <td><input type="text" id="tag" class="ui-widget-content ui-corner-all" style="display:inline-block !important;height: 20px;width: 100px; height: 24px; padding: 2px;" /></td>
                                        <td class="trigger_col"><input type="text" id="keyword" class="ui-widget-content ui-corner-all" style="display:inline-block !important;height: 20px;width: 100px; height: 24px; padding: 2px;" /></td>
                                        <td class="rule_col">
                                            <select class="styled" id="rule" onchange="if (this.value == 'But Not') { $('#cost').prop('disabled', true).css('background', '#CCCCCC'); } else { $('#cost').prop('disabled', false).css('background', 'white'); }" style="width: 80px; display: inline-block !important; margin-bottom: 5px; height: 28px; padding: 4px;">
                                                <option value="None">None</option>
                                                <option value="But Not">But NOT</option>
                                            </select>
                                        </td>
                                        <td><input type="text" id="cost" class="ui-widget-content ui-corner-all" style="display:inline-block !important;height: 20px;width: 60px; height: 24px; padding: 2px;" /></td>
                                        <td><input type="text" id="order" class="ui-widget-content ui-corner-all" style="display:inline-block !important;height: 20px; width: 59px; height: 24px; padding: 2px;" /></td>
                                        <td><button style="width: 54px; margin-left: 5px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" id="add_tag"><span class="ui-button-text">Add</span></button></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        <?php

        ?>
            <script type="text/javascript">
                var companies = [];
                var client_email_tags = {};

                <?php
                foreach ($companies as $company) {
                    ?>
                    companies[<?php echo $company['idx']; ?>] = {
                        id: <?php echo $company['idx']; ?>,
                        clients: []
                    };

                    <?php
                        $clients = $client->getClients(array("company_id" => $company['idx']));
                        foreach ($clients as $company_client) {
                    ?>
                        companies[<?php echo $company['idx']; ?>].clients.push({
                            id: <?php echo $company_client['id']; ?>,
                            name: '<?php echo $company_client["client_name"]; ?>',
                        });
                    <?php
                        }
                }
                ?>

                $(document).ready(function() {
                    <?php
                    if (isset($_REQUEST['company_id'])) {
                        $company_id = $_REQUEST['company_id'];
                        $tracking_settings = $db->getTrackingSettingsForCompany($company_id);
                        $clients = json_decode($tracking_settings->clients);
                        $emails = json_decode($tracking_settings->emails);
                        $client_email_tags = json_decode($tracking_settings->client_email_tags);
                        ?>
                        $("#company_id").val(<?php echo $company_id; ?>);
                        $("#company_id").trigger('change');
                        $("#company_id").prop('disabled', true);
                        <?php
                        foreach ($clients as $client) {
                            ?>
                                $("#company_client_<?php echo $client->id; ?>").prop('checked', true);
                                $("#company_client_<?php echo $client->id; ?>").trigger('click');
                                $("#company_client_<?php echo $client->id; ?>").prop('checked', true);
                            <?php
                        }
                        ?>
                        $("#distribution").val('<?php echo $tracking_settings->distribution; ?>');
                        $("#tracking_company_emails .default").remove();
                        <?php
                        foreach ($emails as $email) {
                            ?>
                                $("#tracking_company_emails").append('<div class="email-wrapper"><span class="email"><?php echo $email; ?>@trackingyourleads.com</span> <a href="javascript: void(0);" class="close" onclick="removeEmailAddress(this);">x</a></div>');
                                $("#tags_email").append('<option value="<?php echo $email; ?>@trackingyourleads.com"><?php echo $email; ?>@trackingyourleads.com</option>');
                            <?php
                        }
                        ?>
                        $("#billing_tag").val('<?php echo $tracking_settings->billing_tag; ?>');
                        client_email_tags = $.parseJSON('<?php echo $tracking_settings->client_email_tags; ?>');
                        <?php
                    }
                    ?>

                    $('.tooltip').tooltipster({
                        theme: '.tooltipster-shadow'
                    });
                });

                function loadClients(company_id) {
                    $("#emails_distribution").hide();
                    $("#tags_client_id").html('<option value="">Select</option>');

                    if (!company_id) {
                        $('#tracking_company_clients').text('Select a company.');
                        return false;
                    }
                    var company = companies[company_id];
                    if (company.clients.length == 0) {
                        $('#tracking_company_clients').text('There are no clients in this company.');
                    }
                    else {
                        $('#tracking_company_clients').text('');

                        for (var i in company.clients) {
                            var this_client = company.clients[i];
                            $('#tracking_company_clients').append('<label><input type="checkbox" class="company_client" id="company_client_' + this_client.id + '" data-client-id="' + this_client.id + '" /> ' + this_client.name + '</label> &nbsp;&nbsp;');
                        }
                    }

                    $('#tracking_company_clients input[type="checkbox"]').click(function() {
                        $("#tags_client_id").html('<option value="">Select</option>');

                        var total_checked = $('#tracking_company_clients input[type="checkbox"]:checked').length;

                        if (total_checked > 1) {
                            $("#emails_distribution").show();
                        }
                        else {
                            $("#emails_distribution").hide();
                        }

                        $('#tracking_company_clients input[type="checkbox"]:checked').each(function() {
                            var client_id = $(this).attr('data-client-id');
                            var client_name = $(this).parents('label').text();
                            $("#tags_client_id").append('<option value="' + client_id + '">' + client_name + '</option>');
                        });
                    });
                }

                function changeBillingTag(billing_tag) {
                    if (billing_tag == "auto") {
                        $(".trigger_col").show();
                        $(".rule_col").show();
                        $("#keyword").css('background', 'white');
                    }
                    else {
                        $("#keyword").css('background', '#e8e8e8');
                        $("#keyword").val('');
                        $(".trigger_col").hide();
                        $(".rule_col").hide();
                        $("#rule").val("None");
                        $("#rule").trigger("change");
                    }
                }

                $("#generate-email-address").click(function() {
                    $.blockUI({message: 'Loading...', css: {padding: '5px'}});

                    $.get("email_tracking_setup.php?action=generate_email_address&act_url=" + encodeURIComponent(document.location.href.split("email_tracking_setup.php")[0]), function(data) {
                        $.unblockUI();
                        $("#tracking_company_emails .default").remove();

                        $("#tracking_company_emails").append('<div class="email-wrapper"><span class="email">' + data + '</span> <a href="javascript: void(0);" class="close" onclick="removeEmailAddress(this);">x</a></div>');
                        $("#tags_email").append('<option value="' + data + '">' + data + '</option>');
                    });
                });

                function removeEmailAddress(el) {
                    $(el).parents("div.email-wrapper").remove(); 
                    if ($("#tracking_company_emails div.email-wrapper").length == 0) { 
                        $("#tracking_company_emails").html('<div class="email-wrapper default"><span class="email">Click generate.</span></div>'); 
                    }

                    $("#tags_email").html('<option value="">Select</option>');
                    $('#tracking_company_emails div.email-wrapper:not(.default)').each(function() {
                        var email = $(this).find('.email').text();
                        $("#tags_email").append('<option value="' + email + '">' + email + '</option>');
                    });
                }

                $("#add_tag").click(function() {
                    var tags_client_id = $("#tags_client_id").val().trim();
                    var tags_email = $("#tags_email").val().trim().replace("@trackingyourleads.com", "");
                    var tag = $("#tag").val().trim();
                    var keyword = $("#keyword").val().trim();
                    var rule = $("#rule").val().trim();
                    var cost = $("#cost").val().trim();
                    var order = $("#order").val().trim();

                    if (!tags_client_id) {
                        errMsgDialog("Please select a client.");
                        return false;
                    }

                    if (!tags_email) {
                        errMsgDialog("Please select an email address.");
                        return false;
                    }

                    if (!tag) {
                        errMsgDialog("Please enter tag.");
                        return false;
                    }

                    if ($("#billing_tag").val() == "auto" && !keyword) {
                        errMsgDialog("Please enter keyword.");
                        return false;
                    }

                    if (!cost && rule == "None") {
                        errMsgDialog("Please enter cost.");
                        return false;
                    }

                    if (!$.isNumeric(cost) && rule == "None") {
                        errMsgDialog("Cost must be a number.");
                        return false;
                    }

                    if (!order) {
                        errMsgDialog("Please enter order.");
                        return false;
                    }

                    if (!$.isNumeric(order)) {
                        errMsgDialog("Order must be a number.");
                        return false;
                    }

                    if (!client_email_tags[tags_client_id]) {
                        client_email_tags[tags_client_id] = {};
                    }

                    if (!client_email_tags[tags_client_id][tags_email]) {
                        client_email_tags[tags_client_id][tags_email] = [];
                    }

                    client_email_tags[tags_client_id][tags_email].push({
                        tag: tag,
                        keyword: keyword,
                        rule: rule,
                        cost: cost,
                        order: order
                    });

                    $("#tags_table > tbody").html('');

                    for (var i in client_email_tags[tags_client_id][tags_email]) {
                        var tag = client_email_tags[tags_client_id][tags_email][i];
                        var row = '<tr><td>' + tag.tag + '</td><td class="trigger_col" style="display: ' + (($("#billing_tag").val() == "auto") ? 'table-cell' : 'none') + ';">' + tag.keyword + '</td><td class="rule_col" style="display: ' + (($("#billing_tag").val() == "auto") ? 'table-cell' : 'none') + ';">' + tag.rule + '</td><td>' + tag.cost + '</td><td>' + tag.order + '</td><td><a href="#" onclick="removeTag(this, \'' + tag.tag + '\');"><img src="images/delete.gif"></td></tr>';
                        $("#tags_table > tbody").append(row);
                    }

                    $("#tag").val("");
                    $("#keyword").val("");
                    $("#rule").val("None");
                    $("#rule").trigger("change");
                    $("#cost").val("");
                    $("#order").val("");
                    $("#tags_table").trigger('update');
                });

                function removeTag(el, removed_tag) {
                    $(el).parent().parent().remove();

                    var tags_client_id = $("#tags_client_id").val().trim();
                    var tags_email = $("#tags_email").val().trim().replace("@trackingyourleads.com", "");

                    if (tags_client_id && tags_email) {
                        for (var i in client_email_tags[tags_client_id][tags_email]) {
                            var tag = client_email_tags[tags_client_id][tags_email][i];
                            if (removed_tag == tag.tag) {
                                client_email_tags[tags_client_id][tags_email].splice(i, 1);
                            }
                        }
                    }
                }

                $("#tags_client_id, #tags_email").change(function() {
                    var tags_client_id = $("#tags_client_id").val().trim();
                    var tags_email = $("#tags_email").val().trim().replace("@trackingyourleads.com", "");
                    $("#tags_table > tbody").html('');
                    if (tags_client_id && tags_email) {
                        if (!client_email_tags[tags_client_id]) {
                            client_email_tags[tags_client_id] = {};
                        }

                        if (!client_email_tags[tags_client_id][tags_email]) {
                            client_email_tags[tags_client_id][tags_email] = [];
                        }

                        for (var i in client_email_tags[tags_client_id][tags_email]) {
                            var tag = client_email_tags[tags_client_id][tags_email][i];
                            var row = '<tr><td>' + tag.tag + '</td><td class="trigger_col" style="display: ' + (($("#billing_tag").val() == "auto") ? 'table-cell' : 'none') + ';">' + tag.keyword + '</td><td class="rule_col" style="display: ' + (($("#billing_tag").val() == "auto") ? 'table-cell' : 'none') + ';">' + tag.rule + '</td><td>' + tag.cost + '</td><td>' + tag.order + '</td><td><a href="#" onclick="removeTag(this, \'' + tag.tag + '\');"><img src="images/delete.gif"></td></tr>';
                            $("#tags_table > tbody").append(row);
                        }
                    }
                    $("#tags_table").trigger('update');
                });
            </script>
        <?php
        break;

        case 'view_tracking_emails':
            $company_id = $_REQUEST['company_id'];
            $tracking_settings = $db->getTrackingSettingsForCompany($company_id);
            $clients = json_decode($tracking_settings->clients);
            $emails = json_decode($tracking_settings->emails);
            $client_email_tags = json_decode($tracking_settings->client_email_tags);

            $emails_clients_tree = array();

            foreach ($emails as $email) {
                $emails_clients_tree[$email] = array();
            }

            foreach ($clients as $client) {
                if (isset($client_email_tags->{$client->id})) {
                    foreach ($emails as $email) {
                        if (isset($client_email_tags->{$client->id}->{$email})) {
                            if (count($client_email_tags->{$client->id}->{$email}) > 0)
                                $emails_clients_tree[$email][] = $client;
                        }
                    }
                }
            }
            ?>

            <table class="sortable" style="width: 100%;" id="tags_table">
                <thead>
                    <tr>
                        <th align="left">Email</th>
                        <th align="left">Clients</th>
                    </tr>
                </thead>
                <tbody>
            <?php

            foreach ($emails_clients_tree as $email => $clients) {
                ?>
                <tr>
                    <td align="left" style="border-top: solid 1px #7e7e7e;"><?php echo $email; ?>@trackingyourleads.com</td>
                    <td align="left" style="border-top: solid 1px #7e7e7e;">
                        <?php foreach ($clients as $client) { ?>
                            <?php echo $client->name; ?><br />
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }
            ?>
                </tbody>
            </table>
            <?php
        break;
    }
    exit();
}

$page="adminpage";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        #purchase_number, #reset_url, #update_number,#delete_number{
            width: 85px;
            height: 30px;
            line-height: 30px;
            background: url(images/btns.gif) top center no-repeat;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            text-transform: uppercase;
            color: white;
            text-shadow: 1px 1px 0 #0A5482;
            cursor: pointer;
            margin-right: 10px;
            vertical-align: middle;
            display: inline-block !important;
        }
        .ui-dialog label, input {
            display: inline-block !important;
        }
        .cmf-skinned-select{
            display: inline-block !important;
        }
        select#country option {
            background-repeat:no-repeat;
            background-position:bottom left;
            padding-left:30px;
        }
        select#country option[value="US"] {
            background-image:url(images/us.png);
        }
        select#country option[value="CA"] {
            background-image:url(images/ca.png);
        }
        select#country option[value="GB"] {
            background-image:url(images/gb.png);
        }
        .voice_url, .sms_url {
            font-size: 10px;
            width: 350px;
            height: 17px;
        }
        .incorrect-url {
            background-color: #FFFF5F  !important;
            border: 1px gray solid !important;
        }
    </style>

</head>

<body>

<div id="hld">
<div class="wrapper">		<!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->

    <div class="block">

        <div class="block_head">
            <div class="bheadl"></div>
            <div class="bheadr"></div>

            <h2>
                Email Lead Tracking &nbsp; <a href="javascript: void(0);" id="setup_tracking_btn">Setup Tracking</a>
            </h2>

        </div>		<!-- .block_head ends -->


        <div class="block_content">
            <form action="" onsubmit="return false;">
                <table cellpadding="0" cellspacing="0" width="100%" class="sortable">

                    <thead>
                    <tr>
                        <th style="width: 150px !important;">Company</th>
                        <th style="width: 150px !important;">Emails <span class="tooltip" title="Click View to view the emails associated with this company."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></th>
                        <th style="width: 115px !important;">Stats <span class="tooltip" title="View detailed report for company."><img src="images/info.gif" width="16" height="16" style="vertical-align: middle;" /></span></th>
                        <th style="width: 115px !important;">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                        <?php
                        $email_tracking_settings = $db->getTrackingSettings();
                        if (sizeof($email_tracking_settings) == 0) {
                            ?>
                            <tr>
                                <td colspan="6">No settings.
                            </tr>
                            <?php
                        }
                        else {
                            foreach ($email_tracking_settings as $settings) {
                                $company = $db->getCompanyName($settings->company_id);
                                if (!empty($company)) {
                        ?>
                        <tr id="tracking-email-settings-<?php echo $settings->company_id; ?>">
                                <td><?php echo $company; ?></td>
                                <td><a href="javascript: void(0);" onclick="viewTrackingEmails('<?php echo $settings->company_id; ?>');">View</a></td>
                                <td>
                                    <a href="email_tracking_stats.php?company_id=<?php echo $settings->company_id; ?>">View Stats</a>
                                </td>
                                <td>
                                    <select class="styled" onchange="selectAction(this, <?php echo $settings->company_id; ?>);" style="width: 100px;">
                                        <option value="">Select</option>
                                        <option value="edit">Edit</option>
                                        <option value="remove">Remove</option>
                                    </select>
                                </td>
                        </tr>
                    <?php }
                        }
                    } ?>
                </table>
            </form>
        </div>		<!-- .block_content ends -->

        <div class="bendl"></div>
        <div class="bendr"></div>
    </div>		<!-- .block ends -->

<?php include "include/footer.php"; ?>

    <script type="text/javascript">
            $(document).ready(function(){
                <?php
                if (isset($_REQUEST['settings_saved'])) {
                    ?>
                    viewTrackingEmails('<?php echo $_REQUEST['company_id']; ?>', true);
                    <?php
                }
                ?>

                $("#setup_tracking_btn").click(function() {
                    manageTrackingSettings();
                });
            });

            function manageTrackingSettings(company_id) {
                if ($("#add_email_wrapper").length == 0) {
                    $("body").append('<div id="add_email_wrapper"></div>')
                }

                var height = $(document).height() - 20;

                $("#add_email_wrapper").text('Loading...');
                $("#add_email_wrapper").dialog({
                    modal: true,
                    autoOpen: true,
                    height: 'auto',
                    width: 700,
                    position: ['top', 10],
                    height: height,
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>' +
                            'Tracking Emails Setup',
                    buttons: [{
                        text: "Save",
                        click: function() {
                            var company_id = $("#company_id").val();

                            var clients = [];

                            $('#tracking_company_clients input[type="checkbox"]:checked').each(function() {
                                var client_id = $(this).attr('data-client-id');
                                var client_name = $(this).parents('label').text();
                                clients.push({
                                    id: client_id,
                                    name: client_name
                                });
                            });

                            var distribution = $("#distribution").val();

                            var emails = [];

                            $('#tracking_company_emails .email-wrapper:not(.default)').each(function() {
                                emails.push($(this).find('.email').text().trim().replace("@trackingyourleads.com", ""));
                            });

                            var billing_tag = $("#billing_tag").val();

                            if (!company_id || clients.length == 0 || emails.length == 0 || !client_email_tags) {
                                errMsgDialog('Please setup all fields. Make sure you have selected at least one client, generated at least one email address and created tags.');
                                return false;
                            }

                            var el = $(this);
                            var response = JSONajaxCall({
                                func: "SYSTEM_SAVE_TRACKING_SETTINGS",
                                data: {
                                    company_id: company_id,
                                    clients: JSON.stringify(clients),
                                    distribution: distribution,
                                    emails: JSON.stringify(emails),
                                    billing_tag: billing_tag,
                                    client_email_tags: JSON.stringify(client_email_tags)
                                }
                            });

                            response.done(function(msg) {
                                if (msg == 0) {
                                    errMsgDialog('This company already has tracking settings.');
                                }
                                else {
                                    msgDialogCB("Tracking settings saved.",function(){
                                        window.location = 'email_tracking_setup.php?settings_saved=1&company_id=' + company_id;
                                    });
                                }
                            });
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }]
                });
    
                var ui_url = "email_tracking_setup.php?action=add_email_ui";
                if (company_id)
                    ui_url += "&company_id=" + company_id;

                $.post(ui_url, { act_url: document.location.href.split("email_tracking_setup.php")[0] }, function(data) {
                    $("#add_email_wrapper").html(data);
                });
            }

            function selectAction(el, company_id) {
                if (el.value == "edit")
                    editSettings(company_id);

                if (el.value == "remove")
                    removeSettings(company_id);

                if (el.value == "view_stats") {
                    window.location = "email_tracking_stats.php?company_id=" + company_id;
                }

                $(el).val('');
            }

            function viewTrackingEmails(company_id, show_prompt) {
                    if ($("#tracking_emails_wrapper").length == 0) {
                        $("body").append('<div id="tracking_emails_wrapper"></div>')
                    }

                    $.blockUI({message: 'Loading...', css: {padding: '5px'}});

                    $.get("email_tracking_setup.php?action=view_tracking_emails&company_id=" + company_id, function(data) {
                        $.unblockUI();
                        $("#tracking_emails_wrapper").html(data);

                        var $dialog = $("#tracking_emails_wrapper").dialog({
                            modal: true,
                            autoOpen: true,
                            height: 'auto',
                            width: 450,
                            title: '<span class="ui-button-icon-primary ui-icon-mail-closed" style="float:left; margin-right:5px;"></span>' +
                                    'View Tracking Emails',
                            buttons: [{
                                text: "Close",
                                click: function() {
                                    $(this).dialog("close");
                                }
                            }]
                        });

                        if (show_prompt)
                            infoMsgDialog("Here are the emails so you can start copying them and use them in your campaigns.");
                    });
                }

            function editSettings(company_id)
            {
                manageTrackingSettings(company_id);
            }

            function removeSettings(company_id)
            {
                promptMsg("Are you sure you want to permanently delete the email tracking settings for this company?",function(){
                    $.blockUI({message: 'Loading...', css: {padding: '5px'}});
                    $.get("email_tracking_setup.php?delete=true&company_id=" + company_id, function() {
                        $.unblockUI();
                        msgDialogCB("Settings removed.", function() {
                            window.location = 'email_tracking_setup.php';
                        });
                    });
                });
            }

            $("#dataTable").tablesorter({
                widgets: ['zebra']
            });
    </script>
</body>
</html>