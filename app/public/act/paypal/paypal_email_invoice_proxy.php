<?php

require_once("../include/util.php");
require_once("../include/db.invoice.php");

$invoiceId = $_GET["invoice_id"];

$invoice = new Invoice();

//get invoice info.
$invoicesArray = $invoice->getInvoices(array(
	"invoice_id" => $invoiceId
));

$invoiceArray = $invoicesArray[0];

if($invoiceArray['is_paid']==1)
{
    ?>
    <script type="text/javascript">
        alert('This invoice has already been paid.');
        window.location = "../billing_log_invoice.php?invoice_id=<?php echo str_replace("-", "", $invoiceArray["date_mysql_format"]) . $invoiceArray['id'] ?>";
    </script>
    <?php
    die();
}

$invoiceAmountCurrency = $invoiceArray["amount_currency"];
$invoiceDetails = unserialize($invoiceArray["invoice_details"]);
$billFor = $invoiceArray["bill_for"];

$itemDescription = ($billFor == "lead-gen" ? "Lead Gen" : "Call Tracking Service");

$invoiceTotalAmount = 0;

foreach ($invoiceDetails as $invoiceDetailArray) {
	
	$invoiceTotalAmount += $invoiceDetailArray["totalPrice"];
}

//in case of lead gen, the amount of free leads are more than consumed leads, we don't want to show negative values.
if ($invoiceTotalAmount < 0) {
	
	$invoiceTotalAmount = 0;
}

switch($invoiceAmountCurrency){
    case "HUF":
    case "TWD":
    case "JPY":
    {
        $invoiceTotalAmount = round($invoiceTotalAmount);
        break;
    }
}

$paypalIntegrationOption = $db->getVar("paypal_integration_option");
$paypalIntegrationId = $db->getVar("paypal_integration_id");

function curPageURL() {
    $pageURL = 'http';
    $request_uri = substr($_SERVER['REQUEST_URI'],0,stripos($_SERVER['REQUEST_URI'],"paypal_email_invoice_proxy.php"));

    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["HTTP_HOST"].":".$_SERVER["SERVER_PORT"].$request_uri;
    } else {
        $pageURL .= $_SERVER["HTTP_HOST"].$request_uri;
    }

    return $pageURL;
}

?>

<html>
	<head></head>
	<body>
		<div>
			<form method="post" action="https://www.paypal.com/cgi-bin/webscr" class="paypal-button">
				<input type="hidden" name="env" value="www">
				<input type="hidden" name="notify_url" value="<?php echo(curPageURL()); ?>paypal_invoice_ipn_handler.php">
				<input type="hidden" name="currency_code" value="<?php echo($invoiceAmountCurrency); ?>">
				<input type="hidden" name="amount" value="<?php echo $invoiceTotalAmount; ?>">
				<input type="hidden" name="quantity" value="1">
				<input type="hidden" name="item_name" value="<?php echo($itemDescription); ?>">
				<input type="hidden" name="button" value="buynow">
				<input type="hidden" name="cmd" value="_xclick">
				<input type="hidden" name="business" value="<?php echo($paypalIntegrationId); ?>">
				<input type="hidden" name="bn" value="JavaScriptButton_buynow">
				<input type="hidden" name="custom" value='<?php echo urlencode(json_encode(array("invoice_id" => $invoiceId))); ?>'>
			</form>
			<script type="text/javascript">
				document.forms[0].submit();
			</script>
		</div>
	</body>
</html>