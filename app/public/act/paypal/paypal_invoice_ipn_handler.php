<?php
//$act_support_debug = true;

set_time_limit(0);
ignore_user_abort(1);
error_reporting(E_ALL);
ini_set( 'display_errors','1');
//the db class.
require_once("../include/config.php");
require_once("../include/db.php");
require_once("../include/db.invoice.php");
	
//the IpnListener class.
require_once("../include/php-paypal-ipn/ipnlistener.php");

//instantiate the IpnListener class
$listener = new IpnListener();

//change to false for production.
$listener->use_sandbox = false;

try {

	//make sure this script is called using POST, Paypal uses POST.
	$listener->requirePostMethod();
	
	//send the data back to Paypal and make sure the response is VERIFIED (true) or INVALID (false).
	$verified = $listener->processIpn();
	
} catch (Exception $e) {

	//if there is an error, log it.
	error_log($e->getMessage());
	exit(0);
}

/*
The processIpn() method returned true if the IPN was "VERIFIED" and false if it
was "INVALID".
*/
if ($verified) {
    /*
    Once you have a verified IPN you need to do a few more checks on the POST
    fields--typically against data you stored in your database during when the
    end user made a purchase (such as in the "success" page on a web payments
    standard button). The fields PayPal recommends checking are:
    
        1. Check the $_REQUEST['payment_status'] is "Completed"
	    2. Check that $_REQUEST['txn_id'] has not been previously processed 
	    3. Check that $_REQUEST['receiver_email'] is your Primary PayPal email 
	    4. Check that $_REQUEST['payment_amount'] and $_REQUEST['payment_currency'] 
	       are correct
    
    Since implementations on this varies, I will leave these checks out of this
    example and just send an email using the getTextReport() method to get all
    of the details about the IPN.  
    */
    //mail('ahmad.abuomar@icloud.com', 'Verified IPN', $listener->getTextReport());

    if(isset($act_support_debug) && $act_support_debug != ""){
        $fileHandle = fopen("ipn.log", "a");
        fwrite($fileHandle, "Verified IPN\n" . $listener->getTextReport() . "\n\n\n\n");
        fwrite($fileHandle,"===[PaypalHit]===");
        fwrite($fileHandle,print_r($_REQUEST,true));
    }

	$errmsg = '';   // stores errors from fraud checks
	
	// 1. Make sure the payment status is "Completed"
	if ($_REQUEST['payment_status'] !== 'Completed') {

        if($_REQUEST['payment_status'] === 'Pending')
        {
            $customParamsArray = json_decode(urldecode($_REQUEST['custom']), true);
            $invoiceId = $customParamsArray["invoice_id"];
            $invoice = new Invoice();

            $invoice->setPending(1, $invoiceId);
        }
        if($_REQUEST['payment_status'] === 'Denied'){
            $customParamsArray = json_decode(urldecode($_REQUEST['custom']), true);
            $invoiceId = $customParamsArray["invoice_id"];
            $invoice = new Invoice();

            $invoice->setDeclined(1, $invoiceId);
        }
		// simply ignore any IPN that is not completed
		die();
	}
	
	//this holds the cutom param passed to Paypal.
	$customParamsArray = json_decode(urldecode($_REQUEST['custom']), true);
	
	$invoiceId = $customParamsArray["invoice_id"];
	
	$invoice = new Invoice();
	
	//get invoice info.
	$invoicesArray = $invoice->getInvoices(array(
		"invoice_id" => $invoiceId
	));

	$invoiceArray = $invoicesArray[0];
	
	$invoiceAmountCurrency = $invoiceArray["amount_currency"];
	$invoiceDetails = unserialize($invoiceArray["invoice_details"]);
	$invoiceIsPaid = intval($invoiceArray["is_paid"]);
	$invoiceAmountPaid = $invoiceArray["amount_paid"];
	
	$invoiceTotalAmount = 0;
	
	foreach ($invoiceDetails as $invoiceDetailArray) {
		
		$invoiceTotalAmount += $invoiceDetailArray["totalPrice"];
	}
	
	//in case of lead gen, the amount of free leads are more than consumed leads, we don't want to show negative values.
	if ($invoiceTotalAmount < 0) {
		
		$invoiceTotalAmount = 0;
	}

    switch($invoiceAmountCurrency){
        case "HUF":
        case "TWD":
        case "JPY":
        {
            $invoiceTotalAmount = round($invoiceTotalAmount);
            break;
        }
    }

	if ($_REQUEST['mc_currency'] != $invoiceAmountCurrency) {
	
		$errmsg .= "'mc_currency' does not match: ";
		$errmsg .= $_REQUEST['mc_currency']."\n";
	}

	if (!empty($errmsg)) {
        if(isset($act_support_debug) && $act_support_debug != ""){
            fwrite($fileHandle,"\n!Error");
        }
		
		// manually investigate errors from the fraud checking
		$body = "IPN failed fraud checks: \n$errmsg\n\n";
		
		error_log($body);

	} else {
		$invoice->updateInvoicePaymentInfo(array(
			"amount_paid" => ($invoiceAmountPaid + $_REQUEST['mc_gross']),
			"invoice_id" => $invoiceId
		));
	}

    if(isset($act_support_debug) && $act_support_debug != ""){
        fclose($fileHandle);
    }
} else {
    /*
    An Invalid IPN *may* be caused by a fraudulent transaction attempt. It's
    a good idea to have a developer or sys admin manually investigate any 
    invalid IPN.
    */
	
	error_log("Invalid IPN\n" . $listener->getTextReport() . "\n\n\n\n");
}
?>