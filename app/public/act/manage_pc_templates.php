<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$page = "adminpage";
$db = new DB();
$pc_templates = $db->getPhoneCodeTemplates();

global $TIMEZONE;

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

</head>





<body>

<div id="hld">

    <div class="wrapper">		<!-- wrapper begins -->


        <?php include_once("include/nav.php"); ?>
        <!-- #header ends -->


        <div class="block">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    <?php if(count($pc_templates)==1) echo("1 Phone Code Template &nbsp; &nbsp; <a href=\"#\" id=\"SYSTEM_ADD_PC_TEMPLATE\">Add Template</a>"); else
                    echo (count($pc_templates)." Phone Code Templates &nbsp; &nbsp; <a href=\"#\" id=\"SYSTEM_ADD_PC_TEMPLATE\">Add Template</a>");?>
                </h2>
                <ul>
                    <li><a href="manage_companies.php">Manage Companies</a></li>
                </ul>
            </div>		<!-- .block_head ends -->


            <div class="block_content">

                <table cellpadding="0" cellspacing="0" width="100%" class="sortable">

                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th rowspan="6">Phone Codes</th>
                        <th>User</th>
                        <th>Last Modified</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php foreach ($pc_templates as $pc_template) {
                    if($pc_template['type']==1)
                        $type = "Incoming";
                    else
                        $type = "Outgoing";
                    ?>
                    <tr>
                        <td><?php echo $pc_template['name']; ?></td>
                        <td><?php echo $type; ?></td>
                        <?php if($pc_template['phone_codes']=="") { ?>
                        <td><?php echo "<a href=\"#\" id=\"SYSTEM_EDIT_PC_TEMPLATE\" data-params='" . $pc_template['id'] . "' title='Edit Phone Codes' style='cursor:pointer'>None</a>"; ?></td>
                        <?php }else{ ?>
                        <td><?php echo "<a href=\"#\" id=\"SYSTEM_EDIT_PC_TEMPLATE\" data-params='" . $pc_template['id'] . "' title='Edit Phone Codes' style='cursor:pointer'>".Util::limit_text(str_replace("\n",", ", $pc_template['phone_codes']),40,"...")."</a>"; ?></td>
                        <?php } ?>
                        <td><?php echo $db->getUserName($pc_template['last_modified_by']); ?></td>
                        <td><?php $dt = new DateTime($pc_template['last_modified_date']); $dt->setTimezone(new DateTimeZone($TIMEZONE)); echo $dt->format("D, M j, Y g:ia"); ?></td>
                        <td><?php echo "<a href=\"#\" id='SYSTEM_DELETE_PC_TEMPLATE' data-params='" . $pc_template['id'] . "' title='Delete Template'><img src=\"images/delete.gif\" /></a>"; ?></td>
                    </tr>
                        <?php } ?>
                </table>

            </div>		<!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>		<!-- .block ends -->


        <?php include "include/footer.php"; ?>
</body>
</html>