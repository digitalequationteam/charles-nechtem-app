<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$page = "adminpage";
$db = new DB();
$blacklists = $db->getBlacklists();

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

</head>





<body>

<div id="hld">

    <div class="wrapper">		<!-- wrapper begins -->


        <?php include_once("include/nav.php"); ?>
        <!-- #header ends -->


        <div class="block">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    <?php if(count($blacklists)==1) echo("1 Blacklist &nbsp; &nbsp; <a href=\"#\" id=\"SYSTEM_ADD_BLACKLIST\">Add Blacklist</a>"); else
                    echo (count($blacklists)." Blacklists &nbsp; &nbsp; <a href=\"#\" id=\"SYSTEM_ADD_BLACKLIST\">Add Blacklist</a>");?>
                </h2>
                <ul>
                    <li><a href="manage_companies.php">Manage Companies</a></li>
                </ul>
            </div>		<!-- .block_head ends -->


            <div class="block_content">

                <table cellpadding="0" cellspacing="0" width="100%" class="sortable">

                    <thead>
                    <tr>
                        <th>Name</th>
                        <th rowspan="6">Numbers</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php foreach ($blacklists as $blacklist) { ?>
                    <tr>
                        <td><?php echo $blacklist[1]; ?></td>
                        <?php if($blacklist[2]=="") { ?>
                        <td><?php echo "<a href=\"#\" id=\"SYSTEM_EDIT_BLACKLIST\" data-params='" . $blacklist[0] . "' title='Edit Numbers' style='cursor:pointer'>None</a>"; ?></td>
                        <?php }else{ ?>
                        <td><?php echo "<a href=\"#\" id=\"SYSTEM_EDIT_BLACKLIST\" data-params='" . $blacklist[0] . "' title='Edit Numbers' style='cursor:pointer'>".Util::limit_text($blacklist[2],60)."</a>"; ?></td>
                        <?php } ?>
                        <td><?php echo "<a href=\"#\" id='SYSTEM_DELETE_BLACKLIST' data-params='" . $blacklist[0] . "' title='Delete Blacklist'><img src=\"images/delete.gif\" /></a>"; ?></td>
                    </tr>
                        <?php } ?>
                </table>

            </div>		<!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>		<!-- .block ends -->


        <?php include "include/footer.php"; ?>
</body>
</html>