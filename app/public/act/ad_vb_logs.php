<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_voice_broadcast";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10007)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

//Retrieving the logs
$logs = ad_vb_get_logs(isset($_GET['page']) ? $_GET['page'] : '1', 20, isset($_GET['camp_idx']) ? $_GET['camp_idx'] : FALSE);

$action_response = '';
$action_response_type = '';

//Action handlers
if (isset($_GET['action'])):

    //Switching over action type
    switch ($_GET['action']):
        //If action is set to export
        case 'export':

            //Setting the name of export file
            $export_name = 'export';

            //If logs displayed on page are of particular campaign
            //Setting the campaign name as name of export file
            if (isset($_GET['camp_idx']))
                $export_name = str_replace(' ', '_', ad_vb_get_campaign_name($_GET['camp_idx']));

            //if tmp directory does not exists
            //creating it
            if (!file_exists(dirname(__FILE__) . '/tmp/'))
                mkdir(dirname(__FILE__) . '/tmp/');

            //If file already exists
            //Deleting it
            if (file_exists(dirname(__FILE__) . '/tmp/' . $export_name . '.csv'))
                unlink(dirname(__FILE__) . '/tmp/' . $export_name . '.csv');

            //Creating a new file and retrieving the handler to perform operations on it
            $fp = fopen(dirname(__FILE__) . '/tmp/' . $export_name . '.csv', 'a+');

            if (is_resource($fp)):

                //Retrieving the column names
                $column_names[0] = @array_keys($logs[0]);

                //replacing campaign_idx name with campaign
                $first_row_campaign_name_key = @array_search('campaign_idx', $column_names[0]);
                $column_names[0][$first_row_campaign_name_key] = 'Campaign';

                //Building the final array
                $csv_data = array_merge($column_names, $logs);

                if (isset($_GET['camp_idx']))
                    unset($csv_data[0][$first_row_campaign_name_key]);

                //Looping over CSV data
                foreach ($csv_data as $single_row):

                    if (isset($single_row['campaign_idx']))
                        $single_row['campaign_idx'] = ad_vb_get_campaign_name($single_row['campaign_idx']);

                    //If logs are of particular campaign
                    //Excluding campaign name column from CSV file
                    //Because the export file is named with campaign name
                    if (isset($_GET['camp_idx']) && isset($single_row['campaign_idx']))
                        unset($single_row['campaign_idx']);

                    if (isset($single_row['user']))
                        $single_row['user'] = ad_get_user($single_row['user']);

                    if (isset($single_row['duration']))
                        $single_row['duration'] = Util::formatTime($single_row['duration']);

                    if (isset($single_row['from']))
                        $single_row['from'] = format_phone($db->format_phone_db($single_row['from']));

                    if (isset($single_row['to']))
                        $single_row['to'] = format_phone($db->format_phone_db($single_row['to']));

                    //Writing in CSV file
                    fwrite($fp, '"' . implode('","', $single_row) . "\"\n");

                endforeach;

                //Destroying the file handler
                fclose($fp);

                $action_response = '<a download="'. $export_name . '.csv" href="tmp/' . $export_name . '.csv">Click here</a> to download exported CSV file.';
                $action_response_type = 'success';

            endif;

            break;

        default:
            break;
    endswitch;

endif;

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
//Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                <!--Initializing the html part that will display the campaign list-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Messages: Logs <span style="text-transform: none;"><?php echo isset($_GET['camp_idx']) ? '(Campaign: ' . ad_vb_get_campaign_name($_GET['camp_idx']) . ')' : ''; ?></span></h2>
                        <ul>
                            <li>
                                <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('action' => 'export')); ?>">
                                    <img src="images/excel_img.png" width="50" alt="Export as CSV" title="Export as CSV" />
                                </a>
                            </li>
                        </ul>
                        <ul>
                            <li><a href="ad_vb_add_campaign.php">Add a Campaign</a></li>
                            <li><a href="ad_vb_campaigns.php">Campaigns List</a></li>
                            <li><a href="ad_contactlist_log.php">Contacts</a></li>
                            <li><a href="ad_vb_logs.php">Logs</a></li>
                        </ul>
                    </div>		
                    <!-- .block_head ends -->
                    <div class="block_content">
                        <?php
                        //If response message is set
                        if (!s8_is_str_blank($action_response)):
                            //Switching over type of message
                            switch ($action_response_type) {
                                //If response message is of success type
                                case 'success':
                                    //Displaying the success message.
                                    ?>
                                    <div class="message success"><p><?php echo $action_response; ?></p></div>
                                    <?php
                                    break;

                                default:
                                    break;
                            }
                        endif;
                        ?>
                        <form action="" method="post">
                            <table cellpadding="0" cellspacing="0" width="100%" class="sortable">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Campaign Name</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Duration</th>
                                        <th>Response</th>
                                        <th>Recording</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($logs) > 0):
                                        $i = 0;
                                        foreach ($logs as $single_call_log):
                                            $i = $i+1;
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $date = Util::convertToLocalTZ($single_call_log['date']);
                                                    echo $date->format("D n\/j Y g\:iA");
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo ad_vb_get_campaign_name($single_call_log['campaign_idx']);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo format_phone($db->format_phone_db($single_call_log['from']));
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo format_phone($db->format_phone_db($single_call_log['to']));
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo Util::formatTime($single_call_log['duration']);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $single_call_log['response'];
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if($single_call_log['response']=="SMS Sent"){
                                                        echo '<a href="#" id="ViewSMS_'.$i.'">SMS Contents</a>';
                                                        ?>
                                                        <script type="text/javascript">
                                                            $("#ViewSMS_<?php echo $i; ?>").tooltipster({
                                                                position:'top',
                                                                theme:'.tooltipster-shadow'
                                                            });
                                                            $("#ViewSMS_<?php echo $i; ?>").tooltipster("update",<?php echo str_replace('\r\n',"<br/>",json_encode($single_call_log['recording_url']));?>);
                                                        </script>
                                                        <?php
                                                    }else
                                                    echo Util::generateFlashAudioPlayer((!s8_is_str_blank($single_call_log['recording_url']) ? $single_call_log['recording_url'] : ' - '), "sm");
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        ?>
                                        <tr>
                                            <td>No records exist.</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php
                                    endif;
                                    ?>
                                </tbody>
                            </table>

                            <?php
                            $total_logs = ad_vb_get_logs_count(isset($_GET['camp_idx']) ? $_GET['camp_idx'] : FALSE);
                            $logs_num_pages = ad_num_pages($total_logs);

//Detecing previous and next page ids
                            $previous_page = isset($_GET['page']) && $_GET['page'] > 1 ? ($_GET['page'] - 1) : 1;
                            $next_page = isset($_GET['page']) && $_GET['page'] > 1 ? ($_GET['page'] + 1) : 2;

                            if ($logs_num_pages > 0):
                                ?>
                                <div class="pagination right">
                                    <?php
                                    if ($logs_num_pages > 1):
                                        ?>
                                        <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('page' => $previous_page, 'action' => FALSE)); ?>">&laquo;</a>
                                        <?php
                                    endif;
                                    for ($i = 1; $i <= $logs_num_pages; $i++):
                                        ?>
                                        <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('page' => $i, 'action' => FALSE)); ?>" <?php echo (isset($_GET['page']) && $_GET['page'] == $i) || (!isset($_GET['page']) && $i == 1) ? 'class="active"' : ''; ?>><?php echo $i; ?></a>
                                        <?php
                                    endfor;
                                    if ($logs_num_pages != $_GET['page'] && $logs_num_pages > 1):
                                        ?>
                                        <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('page' => $next_page, 'action' => FALSE)); ?>">&raquo;</a>
                                        <?php
                                    endif;
                                    ?>
                                </div>
                                <!-- .pagination ends -->
                                <?php
                            endif;
                            ?>

                        </form>
                    </div>		<!-- .block_content ends -->
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Exiting the HTML code that will display the campaign list-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>

        <!--//Notification bar html-->
        <div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
        <script type="text/javascript" language="javascript">
            if ($ === undefined) {
                $ = jQuery;
            }

            /**
             * This function will display the notification message on screen.
             * @param {string} message The message to be displayed in notification bar
             * @returns {undefined}
             */
            function ad_display_message(message) {
                $('.ad_notification').html(message);
                $('.ad_notification').slideDown();
                setTimeout(function() {
                    $('.ad_notification').fadeOut(3000);
                }, 3000);

            }

            /**
             * This function will check whether string is blank or not
             * @param {string} str The value to be checked
             * @returns {Boolean} returns boolean TRUE or FALSE based on check performed.
             */
            function ad_is_str_blank(str) {
                if (str === '' || str === ' ' || str === null || str === undefined) {
                    return true;
                } else {
                    return false;
                }
            }

            //When document is ready to attache events to its elements
            $(document).ready(function() {

                //Attaching the function to submit event of text dial
                $('#ad_vb_test_dial, #ad_vb_dial_numbers').submit(function(e) {

                    //Retrieving the caller id
                    var caller_id = $('[name=ad_vb_caller_id]').val();
                    //Retrieving the number to dial
                    if ($(this).attr('id') === 'ad_vb_test_dial') {
                        var to_num = $('[name=ad_vb_test_dial_number]').val();
                    } else {
                        var to_num = $('[name=ad_vb_bulk_dial_numbers]').val();
                    }

                    //CHecking for blank values
                    if (!ad_is_str_blank(caller_id) && !ad_is_str_blank(to_num)) {

                        //Preparing the ajax URL
                        var ajax_url = '<?php echo dirname(s8_get_current_webpage_uri()); ?>/include/ad_auto_dialer_files/ad_ajax.php?to=' + to_num + '&caller_id=' + caller_id + '&userid=<?php echo @$_SESSION['user_id']; ?>';
                        //Firing the AJAX request
                        $.get(ajax_url, function(ajax_response) {

                            //Displaying the notification
                            ad_display_message(ajax_response);

                        });
                    } else {

                        //If blank value detected
                        //Throwing the error on screen
                        ad_display_message('Either caller ID not selected or No number entered.');

                    }

                    //Preventing the default action of form submission
                    e.preventDefault();
                });

            });

        </script>
    </body>
</html>