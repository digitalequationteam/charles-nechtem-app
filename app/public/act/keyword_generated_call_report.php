<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "keyword_generated_call_report";

//Including necessary files
require_once 'include/util.php';
require_once('include/Pagination.php');

if(@$lcl<2) {
    header("Location: index.php");
    exit;
}

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

$display_report = false;

if(@$_REQUEST['sd']!="" && (@$_REQUEST['ed']=="" || @$_REQUEST['cm']==""))
{
    ?>
    <script type="text/javascript">
        alert('An error has occured. Redirecting.');
        window.location = "keyword_report.php";
    </script>
    <?php
    exit;
}

$companies = $db->getAllCompanies();

$startDate  =   '';
$endDate    =   ''; 

global $TIMEZONE;
$tz = new DateTimeZone($TIMEZONE);

if(isset($_REQUEST['submit'])) {
    $company_id = trim($_REQUEST['companyId']);
    $_SESSION['kcr_company'] = $company_id;
    
    if($_REQUEST['day_report'] != 'none') {

        $dayReport = $_REQUEST['day_report'];

        switch ($dayReport) {

            case "today":
                $startDate = strtotime("midnight", time());
                $endDate = strtotime("midnight +1 day -1 sec", time());
            break;           

            case "yesterday":
                $startDate = strtotime('-1 day', time());
                $endDate   = strtotime("midnight -1 sec", time());
            break;             

            case "lastweek":
                $startDate = strtotime('-7 day', time());
                $endDate   = strtotime("midnight -1 sec", time());
            break;           

            case "lastmonth":
                $startDate = strtotime('-30 day', time());
                $endDate   = strtotime("midnight -1 sec", time());
            break;
        }

    } else {
        if (@$_REQUEST['start_date'] != "") {
            $startDate = trim($_REQUEST['start_date']);
            $startDate = strtotime($startDate);
            $endDate   = trim($_REQUEST['end_date']);
            $endDate   = strtotime($endDate);
        }
        elseif (@$_REQUEST['startDate'] != "") {
            $startDate = trim($_REQUEST['startDate']);
            $endDate   = trim($_REQUEST['endDate']);
        }
    }

    if (!empty($startDate)) {
        $start_date = new DateTime(date("Y-m-d", $startDate));
        $year = $start_date->format("Y");
        $month = $start_date->format("n");
        $day = $start_date->format("j");
        $d1 = strtotime($start_date->format("Y-m-d H:i:s"));
        $start_date->setTimezone($tz);
        $d2 = strtotime($start_date->format("Y-m-d H:i:s"));
        $seconds_difference = abs($d2 - $d1);

        $startDate = $startDate + $seconds_difference;
        $endDate = $endDate + $seconds_difference;
    }
}

//reports functions

if (empty($companyId) && isset($_SESSION['kcr_company'])) {
    $company_id = $_SESSION['kcr_company'];
}

$pagination = new Pagination();
$page_n = isset($_REQUEST['page']) ? (int) $_REQUEST['page'] : 1;

$url="http://".$_SERVER['HTTP_HOST'].'/keyword_generated_call_report.php';
$pagination->setLink($url."?companyId=$company_id&startDate=".$startDate."&endDate=".$endDate."&day_report=".$_REQUEST['day_report']."&submit=1&page=%s");
$pagination->setPage($page_n);
$pagination->setSize(10);
$reportRowCount  =  $db->getKeywordGenreportCount($startDate, $endDate, $company_id);

$pagination->setTotalRecords($reportRowCount);

$limit = $pagination->getLimitSql();                    

$reportData  =  $db->getKeywordGenreport($startDate, $endDate, $company_id, $limit);

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>

        <script type="text/javascript">
                google.setOnLoadCallback(drawChart);
                function drawChart() {
                    var startDate ='<?php echo $startDate; ?>';
                    var endDate = '<?php echo $endDate; ?>';
                    var report_type = $("#rt").val();
                    var company_id = '<?php echo (empty($company_id) ? 0 : $company_id); ?>';

                    Web1Graphs.graphTypes.generatedCalls.draw_graph(
                        "call_gen_pie",
                        startDate,
                        endDate,
                        company_id
                    );

                    Web1Graphs.graphTypes.allMatureHits.draw_graph(
                       "all_call_pie",
                       startDate,
                       endDate,
                       company_id
                    );

                    $("#charts").unblock(); 
                }

        </script>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>       <!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                
                <div class="block" style="margin:0 auto;margin-bottom:25px;">
                    <div class="block_head">

                        <div class="bheadl"></div>

                        <div class="bheadr"></div>

                        <h2>Keyword Generated Call Report</h2>

                        <ul>
                            <li class="nobg"><a href="keyword_generated_call_report.php">Keyword Generated Call Report</a></li>
                            <li><a href="keyword_not_generated_call_report.php">Keyword Not Generated Call Report</a></li>
                        </ul>

                    </div>

                    <div class="block_content">

                        <table class="filter-calls" style="margin:0 auto;">
                            <tbody>
                            <tr>
                                <td colspan="2" style="border:none;"><center>

                                    <form action="" method="post">

                                        <label for="start_date">Start:</label> <input class="text small" type="text" id="start_date" name="start_date" value="<?php if(!empty($startDate)){ echo date('m/d/Y', $startDate);} ?>" style="width:120px; display: inline-block !important;" onclick="$('#day_report').val('none');" />
                                        <label for="end_date" style="margin-left:16px;">End:</label> <input class="text small" type="text" id="end_date" value="<?php if(!empty($endDate)){ echo date('m/d/Y', $endDate);} ?>"  name="end_date" style="width:120px; display: inline-block !important; margin-right: 16px;" onclick="$('#day_report').val('none');" />

                                        <select id="day_report" style="width:160px; display: inline-block !important;" name="day_report" onchange="selectExactDate(this);">
                                            <option value ="none">Select Day For Report</option>
                                            <option value ="today" <?php if($dayReport == 'today'){ ?> selected="selected"<?php } ?>>Today</option>
                                            <option value ="yesterday" <?php if($dayReport == 'yesterday'){ ?> selected="selected"<?php } ?>>Yesterday</option>
                                            <option value ="lastweek" <?php if($dayReport == 'lastweek'){ ?> selected="selected"<?php } ?>>Last Week</option>
                                            <option value ="lastmonth" <?php if($dayReport == 'lastmonth'){ ?> selected="selected"<?php } ?>>Last Month</option>
                                        </select>

                                        <select id="company_select" style="width:120px; display: inline-block !important; margin-left:16px; margin-right: 16px;" name="companyId">
                                            <?php if(empty($company_id)){$company_id = '-1' ;}?>

                                            <?php
                                            if($db->isUserAdmin($_SESSION['user_id']))
                                            {
                                                ?><option value="-1" <?php if($company_id == '-1'){ echo 'selected="selected"';} ?> >All Companies</option><?php echo "\n";
                                            }

                                            foreach($companies as $company)
                                            {
                                                ?><option value="<?php echo $company['idx']?>" <?php if($company_id == $company['idx']){echo 'selected="selected"';} ?>><?php echo stripslashes(ucwords($company['company_name']));?></option><?php echo "\n";
                                            }

                                            ?>
                                        </select>
                                        <br /><br />
                                        <input id="submit_REQUEST" class="submit keyword_report" type="submit" value="Generate Report" name="submit">
                                    </form>
                                </center>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>      <!-- .block_content ends -->

                    <div class="block_content">
                        <div id="call_gen_pie" style="width: 50%; height: 300px; position: relative; float:left;"></div>
                        <div id="all_call_pie" style="width: 50%; height: 300px; position: relative; float:right;"></div>
                    </div>      <!-- .block_content ends -->

                    <div class="bendl"></div>
                    <div class="bendr"></div>

                </div>      <!-- .block ends -->

                <div class="block">

                    <div class="block_head">
                         <div class="bheadl"></div>
                         <div class="bheadr"></div>

                         <h2>Visitor Statistics</h2>
                    </div>      <!-- .block_head ends -->

                    <div class="block_content">

                        <table  class="sortable" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Keyword</th>
                                    <th>Numbers Of Call</th>
                                    <th>Total Number of Hits</th>
                                    <th>Number of Unique Hits</th>
                                    <th>Source</th>
                                    <th>Medium</th>
                                    <th>Bounce Rate</th>
                                    <th>Average Time On Site</th>
                                </tr>
                            </thead>

                            <tbody style="font-size:12px;">
                                <?php if(!empty($reportData)){?>
                                    <?php foreach( $reportData as $key => $value ){ ?>
                                    <tr>
                                        <td><?php echo $key; ?></td>
                                        <td><?php echo $value['total_calls'];?></td>
                                        <td><?php echo $value['total_hits'];?></td>
                                        <td><?php if(!empty($value['unique_hit'])){ echo $value['unique_hit'];}else{?>0<?php }?></td>
                                        <td>
                                            <?php $arr = array_filter(explode(',',$value['source']));
                                            asort($arr);
                                            $unique_array = array_unique($arr);
                                            $i= 0;
                                            $source = array();
                                            foreach($unique_array as $row){
                                                foreach($arr as $entry){
                                                    if($entry == $row){
                                                        $i++;
                                                    }                                   
                                                }
                                                $source[] = $row.'('.$i.')'; 
                                                $i=0;
                                            }
                                            echo implode('<br/>',$source);?>
                                        </td>

                                        <td>
                                            <?php 
                                            $array = array_filter(explode(',',$value['medium']));
                                            asort($array);
                                            $uniq_array = array_unique($array);
                                            $i= 0;
                                            $medium = array();
                                            foreach($uniq_array as $row){
                                                foreach($array as $entry){
                                                    if($entry == $row){
                                                        $i++;
                                                    }                                   
                                                }
                                                $medium[] = $row.'('.$i.')'; 
                                                $i=0;
                                            }
                                            echo implode('<br/>',$medium);?>
                                        </td>
                                        <td><?php echo $value['bounce_rate'];?>%</td>
                                        <td><?php echo $value['average_time'];?> sec</td>
                                    </tr>
                                    <?php }?>

                                <?php }else{ ?>

                                    <tr>
                                        <td colspan="8"><center>No Record Found</center></td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <?php $navigation = $pagination->create_links();echo $navigation; ?> 
                    </div>  

                    <div class="bendl"></div>

                    <div class="bendr"></div>

                </div>

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>

                <script type="text/javascript">

                    $(document).ready(function(e){
                        
                        $("#export_btn").click(function(e){
                            $("#export").attr("src","include/excel_export_record.php"+window.location.search);
                        });

                        <?php if(!$display_report) { ?>

                            function getCookie2(c_name)
                            {
                                var i,x,y,ARRcookies=document.cookie.split(";");

                                for (i=0;i<ARRcookies.length;i++)
                                {
                                    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
                                    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
                                    x=x.replace(/^\s+|\s+$/g,"");

                                    if (x==c_name)
                                    {
                                        return unescape(y);
                                    }
                                }
                            }

                            if($("#company_select").val() != "none")
                            {
                                companySelected($("#company_select").val(),function(){
                                    var phone_code = getCookie2("selectedPhoneCode");

                                    if(!phone_code)
                                        phone_code = 0;
                                    else{
                                        console.log(phone_code);
                                        $("#phone_code option[value='"+phone_code+"']").attr("selected","selected");
                                    }
                                });
                            }

                        <?php } ?>
                    });

                    $("#submit_REQUEST").click(function(event){
                            if($("#start_date").val() != "" && $("#end_date").val() == ""){
                                errMsgDialog("Please select a end date");
                                event.preventDefault();
                                return false;   
                            }

                            if($("#end_date").val() != "" && $("#start_date").val() == "" ){
                                errMsgDialog("Please select a start date");
                                event.preventDefault();
                                return false;   
                            }
                        });

                </script>
            </div>
        </div>

        <script type="text/javascript">
            function selectExactDate(el) {
                if (el.value == 'today') {
                    $('#start_date').val('<?php echo date("m/d/Y", strtotime("midnight")); ?>');
                    $('#end_date').val('<?php echo date("m/d/Y", (strtotime("now") - 5)); ?>');
                }
                else if (el.value == 'yesterday') {
                    $('#start_date').val('<?php echo date("m/d/Y", strtotime("-1 day")); ?>');
                    $('#end_date').val('<?php echo date("m/d/Y", strtotime("midnight")); ?>');
                }
                else if (el.value == 'lastweek') {
                    $('#start_date').val('<?php echo date("m/d/Y", strtotime("-7 day")); ?>');
                    $('#end_date').val('<?php echo date("m/d/Y", strtotime("midnight")); ?>');
                }
                else if (el.value == 'lastmonth') {
                    $('#start_date').val('<?php echo date("m/d/Y", strtotime("-30 day")); ?>');
                    $('#end_date').val('<?php echo date("m/d/Y", strtotime("midnight")); ?>');
                }
            }
        </script>
    </body>
</html>