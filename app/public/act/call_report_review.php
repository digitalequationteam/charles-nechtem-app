<?php
session_start();
$page = "call_report";
require_once('include/util.php');
require_once('include/Pagination.php');
require_once("include/db.client.php");
require_once("include/db.company.php");
require_once("include/db.invoice.php");

$db = new DB();
global $RECORDINGS;
if(@$_SESSION['permission'] < 1)
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);
else
    $companies = $db->getAllCompanies();

//Page Variables
$user_id = $_SESSION['user_id'];
$display_report = false;

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if(@$_GET['sd']!="" && (@$_GET['ed']=="" || @$_GET['client_id']==""))
{
    ?>
<script type="text/javascript">
    alert('An error has occured. Redirecting.');
    window.location = "call_report.php";
</script>
<?php
    exit;
}

global $AccountSid, $AuthToken, $ApiVersion;
$client = new Services_Twilio($AccountSid,$AuthToken);

//Declare shore-hand variables
global $TIMEZONE;
$tz = new DateTimeZone($TIMEZONE);

$start_date = new DateTime($_GET['sd']);
$year = $start_date->format("Y");
$month = $start_date->format("n");
$day = $start_date->format("j");
$d1 = strtotime($start_date->format("Y-m-d H:i:s"));
$start_date->setTimezone($tz);
$d2 = strtotime($start_date->format("Y-m-d H:i:s"));
$seconds_difference = abs($d2 - $d1);
$start_date->setDate($year, $month, $day);
$start_date->setTime(0, 0, 0);

$end_date = new DateTime($_GET['ed']);
$year = $end_date->format("Y");
$month = $end_date->format("n");
$day = $end_date->format("j");
$end_date->setTimezone($tz);
$end_date->setDate($year, $month, $day);
$end_date->setTime(0, 0, 0);

$date_start = $start_date;
$date_end = $end_date;

$start_date = strtotime($start_date->format("Y-m-d H:i:s")) + $seconds_difference;
$end_date = strtotime($end_date->format("Y-m-d H:i:s")) + $seconds_difference;

$clientId = $_GET["client_id"];

if($clientId=="")
    die();

$clientClass = new Client();

//get client info.
$clientsArray = $clientClass->getClients(array("client_id" => $clientId));

$clientArray = $clientsArray[0];

$company         = $clientArray["company_id"];
$report_type     = "default";

//Check if user belongs to company
if(!$db->isUserInCompany($company,$user_id) and $_SESSION['permission'] < 1)
{
    ?>
<script type="text/javascript">
    alert('An error has occured. You don\'t belong to that company! Redirecting.');
    window.location = "call_report.php";
</script>
<?php
    exit;
}

$cpage = 1;
// we get the current page from $_GET
if (isset($_GET['cpage'])){
    $cpage = (int) $_GET['cpage'];
}

// create the pagination class
$pagination = new Pagination();
if(strpos($_SERVER['REQUEST_URI'],"cpage") === false)
{
    $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $pagination->setLink($url."&cpage=%s");
}else{
    $url="http://".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['cpage']));
    $pagination->setLink($url."%s");
}
$pagination->setPage($cpage);
$pagination->setSize(20);

//Generate Report from DB
$result = $db->getCallReport($_SESSION['user_id'],$start_date,$end_date,$company,"","",$pagination,"","");

//echo "<!-- ";
//print_r($result);
//echo " -->";

$call_count = $result[1];

$display_report = true;

if ($display_report) {
    $numbers = $db->getNumbersOfCompany($company, true);

    $campaigns = array();
    foreach ($client->account->incoming_phone_numbers as $number) {
        $this_phone_number = $db->format_phone_db($number->phone_number);
        foreach ($numbers as $number2) {
            if ($number2->number == $this_phone_number) {
                $number->friendly_name = Util::escapeString($number->friendly_name);
                if (!isset($campaigns[$number->friendly_name]))
                    $campaigns[$number->friendly_name] = array("numbers" => array(), "calls_total" => 0, "calls_percent" => 0);

                $campaigns[$number->friendly_name]["numbers"][] = $this_phone_number;
                break;
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        input.submit {
            width: 165px !important;
            background: url(images/btnb.gif) top center no-repeat !important;
            height: 30px !important;
            line-height: 30px !important;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif !important;
            font-size: 14px !important;
            font-weight: normal !important;
            text-transform: uppercase !important;
            color: white !important;
            text-shadow: 1px 1px 0 #0A5482;
            cursor: pointer !important;
            margin-right: 10px !important;
            vertical-align: middle !important;
        }
        .ui-state-highlight {
            background:#eee !important;
        }
        table { font-size: 12px !important;}
        <?php if(isset($report_type) && $report_type!="default") { ?>
        #report_logo {
            float:left !important;
            margin-bottom:10px;
            height: 75px;
        }
        <?php } ?>
        #company_info {
            float:right;
        }
        #company_info pre {
            font: 15px 'Helvetica' !important;
            line-height: 1.3em !important;
            margin-top: 5px;
        }
        #stats_table {
            height:285px;
            font: 16px 'Helvetica' !important;
            font-weight: bold !important;
        }
        #stats_table td {
            padding: 0 !important;
        }
        #stats_table td:nth-child(2) {
            font-weight: normal !important;
            text-align: left;
        }
        #header #nav, #header #nav *, #header #nav * * { z-index: 10000; }
    </style>

</head>





<body>

<div id="hld">

<div class="wrapper"<?php if(isset($report_type)) echo " style=\"width:960px\"";?>>		<!-- wrapper begins -->


<?php
if(!isset($report_type) || $report_type=="default")
    include('include/nav.php');
?>
<!-- #header ends -->

<?php
if(isset($report_type) && $report_type!="default") {
    $logoURL = "";
    if($db->getVar("company_logo"))
        $logoURL = $db->getVar("company_logo");

    $info = "";
    if($db->getVar("company_info"))
        $info = $db->getVar("company_info");

?>
    <div id="report_logo"><?php if($logoURL!="") echo "<img src=\"".$logoURL."\" />"; ?></div>

    <div id="company_info"><pre><?php echo $info; ?></pre></div>
<?php } ?>

        <div class="block" style="margin-bottom: 12px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>
                <?php
                if($company==-1)
                    $company_name = "ALL COMPANIES";
                else
                    $company_name = $db->getCompanyName($company);
                ?>
                <h2>Pending Invoice Call Log</span></h2>

            </div>      <!-- .block_head ends -->



            <div class="block_content" id="days" style="padding: 18px;">

                <?php
                $checkInvoiceCreated = $db->customExecute("SELECT COUNT(*) as total FROM invoices WHERE client_id = ? AND to_date LIKE ? AND active = 1 AND is_pending = 0");
                $checkInvoiceCreated->execute(array($clientId, '%'.date("Y-m-d", $end_date).'%'));
                $checkInvoiceCreated = $checkInvoiceCreated->fetch();

                if ($checkInvoiceCreated['total'] > 0) {
                    echo "Your invoice has already been generated for this period!";
                }
                else {

                    if (@$_REQUEST['action'] == "generateInvoice") {

                        $main_url = dirname(s8_get_current_webpage_uri());
                        $exploded = explode("?", $main_url);
                        $main_url = $exploded[0];

                        $generate_invoice_url = $main_url."/invoice.php?client_id=".$clientId."&start_date=".$start_date."&end_date=".$end_date;

                        $curlHandle = curl_init();
                        curl_setopt($curlHandle, CURLOPT_URL, $generate_invoice_url);
                        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);                    
                        $response = curl_exec($curlHandle);                    
                        curl_close($curlHandle);

                        $date = new DateTime(date("Y-m-d", $end_date), new DateTimeZone("UTC"));
                        //$date = new DateTime(date("Y-m-d H:i:s", $strtotime_now), new DateTimeZone("UTC"));

                        $timechange = "";
                        switch($clientArray['frequency'])
                        {
                            case "daily":{
                                $timechange = "+1 day";
                                break;
                            }
                            case "weekly":{
                                $timechange = "+1 week";
                                break;
                            }
                            case "monthly":{
                                $timechange = "+1 month";
                                break;
                            }
                            case "bimonthly":{
                                $timechange = "+2 months";
                                break;
                            }
                            case "yearly":{
                                $timechange = "+1 year";
                                break;
                            }
                        }
                        $date->modify($timechange);

                        $response = sendCronApiRequest("editCron", array(
                            "cronid" => $clientArray['cron_id'],
                            "url"  =>  $main_url."/invoice.php?client_id={$clientId}",
                            "date" => $date->format("Y-m-d"),
                            "freq" => $clientArray['frequency'],
                            "sentNotification" => "0"
                        ));
                        ?>
                        <br />
                            Invoice successfully generated! You may now close this page.
                        <?php
                    }
                    else { 
                    ?>
                        <?php
                        if ($clientArray['lead_gen_delay_period'] == 0)
                            $extra_days = 7;
                        elseif (empty($clientArray['lead_gen_delay_period']))
                            $extra_days = 1;
                        else
                            $extra_days = $clientArray['lead_gen_delay_period'];

                        $automatic_send_date = date("F j, Y", $end_date + ($extra_days * 86400));
                        ?>

                        Review the calls below and tag the ones that need to be invoiced. After tagging the calls, you can either click "Send Invoice" to have the invoice sent right now, or close this page
                        and the system will automatically send it on <?php echo $automatic_send_date; ?>.

                        <br /><br />
                        Client: <?php echo Util::escapeString($clientArray['client_name']); ?><br />
                        Company: <?php echo Util::escapeString($company_name); ?><br />
                        Billing Period: <?php echo date("F j, Y", $start_date); ?> - <?php echo date("F j, Y", $end_date); ?>

                        <br /><br />

                        <form action="" method="post">
                            <input type="hidden" name="action" value="generateInvoice" />
                            <input type="submit" class="submit" value="Send Invoice" />
                        </form>
                    <?php }
                } ?>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->


        <?php if (!isset($_REQUEST['action']) && $checkInvoiceCreated['total'] == 0) { ?>
            <div class="block">

                <div class="block_head">
                    <div class="bheadl"></div>
                    <div class="bheadr"></div>
                    <?php
                    
                    ?>

                    <h2>
                      <?php echo $call_count; ?> Calls from '<a href="#"><?php echo $date_start->format("D n\/j Y \a\\t g\:iA T"); ?></a>' to '<a href="#"><?php echo $date_end->format("D n\/j Y \a\\t g\:iA T"); ?></a>'.
                    </h2>
                    <div style="float:right;"><a href="#" id="export_btn"><img src="images/excel_img.png" width="50" alt="Export as CSV" title="Export as CSV" /></a></div>
                </div>		<!-- .block_head ends -->

                <div class="block_content">
                    <iframe id="export" src="" style="display:none;"></iframe>
                    <table cellpadding="0" cellspacing="0" width="100%" class="sortable">

                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Campaign</th>
                            <th>From</th>
                            <th>To</th>
                            <th>City, State</th>
                            <th>Duration</th>
                            <th>Status</th>
                            <?php if($RECORDINGS) { ?>
                            <th>Recording</th>
                            <?php } ?>
                        </tr>
                        </thead>

                        <tbody>

                            <?php
                            $twilio_numbers=Util::get_all_twilio_numbers();

                            $all_phone_codes = array();

                            $pagination->setTotalRecords($result[1]);
                            if( is_array($result[0]) || is_object($result[0]) )
                                foreach ($result[0] as $call) {
                                    if(!Util::isNumberIntl($call['CallTo']))
                                    {
                                        $company_id = $db->getCompanyOfNumber($db->format_phone_db($call['CallTo']));
                                    }else{
                                        $company_id = $db->getCompanyOfNumber(substr($call['CallTo'],1,(strlen($call['CallTo'])-1)));
                                    }
                                    $company_name = $db->getCompanyName($company_id);
                                    $all_phone_codes[$company_id] = $db->getPhoneCodes($company_id);

                                    if (array_key_exists(format_phone($call['CallTo']), $twilio_numbers))
                                        $campaign = $twilio_numbers[format_phone($call['CallTo'])];
                                    else
                                        $campaign = "";

                                    $tz = new DateTimeZone($TIMEZONE);
                                    $date = new DateTime($call['DateCreated']);
                                    $date->setTimezone($tz);

                                    if ($db->getVar("mask_recordings") == "true") {
                                        $call['RecordingUrl'] = Util::maskRecordingURL($call['RecordingUrl']);
                                    }

                                    //echo "<!-- ".var_dump($report_campaign). " -->";
                                ?>
                            <tr>
                                <td><span style="display:none;"><?php echo $date->format("U"); ?></span><?php echo "<a href=\"call_detail.php?id=".$call['CallSid']."\" title='Click for call details.'>" . $date->format("D n\/j Y g\:iA"). "</a>"; ?>
                                    <?php if(@$_REQUEST['cm']=="-1") { ?>
                                    <br><span style="font: 11px Verdana bold;"><?php echo $company_name; ?></span>
                                    <?php } ?>
                                </td>
                                <td><?php echo $campaign == "callTracking"? "Pooled Call" : $campaign; ?></td>
                                <td style="white-space: nowrap;"<?php if($db->isNumberBlocked($company_id,$call['CallFrom'])) echo " style=\"text-decoration: line-through;\" title=\"This number has been blacklisted.\""; ?>><?php echo format_phone($db->format_phone_db($call['CallFrom'])); ?></td>
                                <td style="white-space: nowrap;"><?php echo format_phone($db->format_phone_db($call['CallTo'])); ?></td>
                                <td>
                                    <?php
                                    if($call['FromCity'] == "" && $call['FromState'] == "")
                                        echo "N/A";
                                    elseif($call['FromCity']=="")
                                        echo $call['FromState'];
                                    else
                                        echo $call['FromCity'].", ".$call['FromState'];
                                    ?>
                                </td>
                                <td><?php echo Util::formatTime($call['DialCallDuration']); ?></td>
                                <td><?php echo $call['DialCallStatus']; ?></td>
                                <?php if($RECORDINGS && $db->isCompanyRecordingDisabled($company_id)==false) { ?>
                                    <?php if ($call['RecordingUrl']!="") { ?>
                                    <td><?php echo Util::generateFlashAudioPlayer($call['RecordingUrl'],"sm"); ?>
                                        <?php if($_SESSION['permission'] > 0 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])) { ?>
                                        <select style="margin-top: 3px;width: 120px;" class="sid_<?php echo $call['CallSid']; ?>" onchange="$(this).attr('disabled','true'); changePhoneCode('<?php echo $call['CallSid']; ?>',this.options[this.selectedIndex].value,(function(){$('.sid_<?php echo $call['CallSid']; ?>').removeAttr('disabled');}))">
                                            <?php
                                            if($call['PhoneCode']==0)
                                                echo "<option value='0' selected>None</option>";
                                            else
                                                echo "<option value='0'>None</option>";

                                            if(is_array($all_phone_codes[$company_id]) || is_object($all_phone_codes[$company_id]))
                                                foreach($all_phone_codes[$company_id] as $code)
                                                {
                                                    if($call['PhoneCode']==$code->idx)
                                                        echo "<option value='$code->idx' selected>$code->name</option>";
                                                    else
                                                        echo "<option value='$code->idx'>$code->name</option>";
                                                }
                                            ?>
                                        </select>
                                            <?php }else{ ?>
                                            <strong>
                                                <?php
                                                if($call['PhoneCode']==0)
                                                    echo "None";
                                                else{
                                                    foreach($all_phone_codes[$company_id] as $phone_code){
                                                        if($call['PhoneCode']==$phone_code->idx)
                                                            echo $phone_code->name;
                                                    }
                                                }
                                                ?>
                                            </strong>
                                            <?php } ?>
                                        <div style="display: inline-block !important;"><a href="#" data-companyid="<?php echo $company_id; ?>" data-params="<?php echo $call['CallFrom']; ?>" id="USR_ADD_NUM_TO_BLACKLIST"><img style="vertical-align: text-top;" src="images/blacklist_btn.png" alt="Add to blacklist" title="Add number to Blacklist"></a>
                                            <a href="#" id="USR_ADD_NOTE_FROM_LIST" title="Add Note" data-callsid="<?php echo $call['CallSid']; ?>"><img src="images/comment-edit-icon.png" style="width:16px; vertical-align: text-top;"></a></div>
                                    </td>
                                    <?php }else{ ?>
                                    <td>
                                        <?php if($_SESSION['permission'] > 0 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])) { ?>
                                        <select title="Select a Phone Code" style="margin-top: 3px; width: 120px;" class="sid_<?php echo $call['CallSid']; ?>" onchange="$(this).attr('disabled','true'); changePhoneCode('<?php echo $call['CallSid']; ?>',this.options[this.selectedIndex].value,(function(){$('.sid_<?php echo $call['CallSid']; ?>').removeAttr('disabled');}))">
                                            <?php
                                            if($call['PhoneCode']==0)
                                                echo "<option value='0' selected>None</option>";
                                            else
                                                echo "<option value='0'>None</option>";
                                            foreach($all_phone_codes[$company_id] as $code)
                                            {
                                                if($call['PhoneCode']==$code->idx)
                                                    echo "<option value='$code->idx' selected>$code->name</option>";
                                                else
                                                    echo "<option value='$code->idx'>$code->name</option>";
                                            }
                                            ?>
                                        </select>
                                        <?php }else{ ?>
                                        <strong>
                                            <?php
                                            if($call['PhoneCode']==0)
                                                echo "None";
                                            else{
                                                foreach($all_phone_codes[$company_id] as $phone_code){
                                                    if($call['PhoneCode']==$phone_code->idx)
                                                        echo $phone_code->name;
                                                }
                                            }
                                            ?>
                                        </strong>
                                        <?php } ?>
                                        <div style="display: inline-block !important;"><a href="#" data-companyid="<?php echo $company_id; ?>" data-params="<?php echo $call['CallFrom']; ?>" id="USR_ADD_NUM_TO_BLACKLIST"><img style="vertical-align: text-top;" src="images/blacklist_btn.png" alt="Add to blacklist" title="Add number to Blacklist"></a>
                                            <a href="#" id="USR_ADD_NOTE_FROM_LIST" title="Add Note" data-callsid="<?php echo $call['CallSid']; ?>"><img src="images/comment-edit-icon.png" style="width:16px; vertical-align: text-top;"></a></div>
                                    </td>
                                    <?php } ?>
                                <?php } ?>
                            </tr>
                                <?php  ?>
                            <?php } ?>
                    </table>

                    <?php $navigation = $pagination->create_links();
                    echo $navigation; ?>

                </div>		<!-- .block_content ends -->

                <div class="bendl"></div>
                <div class="bendr"></div>
            </div>		<!-- .block ends -->
        <?php } ?>

    <?php include "include/footer.php"; ?>
    <script type="text/javascript">
        $(document).ready(function(e){
            $("#export_btn").click(function(e){
                $("#export").attr("src","include/excel_export_record.php"+window.location.search);
            });
        });
    </script>

<?php
function sendCronApiRequest($command, $params) {
    
    $curlHandle = curl_init();
    
    curl_setopt($curlHandle, CURLOPT_URL, "http://cronapi.web1syndication.com/{$command}");
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlHandle, CURLOPT_POST, true);
    
    $postBody = "";
    
    foreach ($params as $key => $value) {
        
        $postBody .= "{$key}=" . urlencode($value) . "&";
    }
    
    rtrim($postBody, '&');
    
    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postBody);
    
    $response = curl_exec($curlHandle);
    
    curl_close($curlHandle);
    
    return ($response);
}
?>
</body>
</html>