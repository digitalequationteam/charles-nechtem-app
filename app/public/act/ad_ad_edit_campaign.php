<?php
//Initiaizing the session
session_start();

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

//Check for user perm
if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10006)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system,
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

$act = (isset($_GET['act']) ? $_GET['act'] : "");

if (  $act == 'uploadMp3' )
{   
    $uploaddir = 'audio/auto_dialer_files/'.$user_id.'/'; 
    
    @mkdir ('audio/auto_dialer_files/',0777,true);
    @chmod ('audio/auto_dialer_files/',0777);
    
    @mkdir ($uploaddir,0777,true);
    @chmod($uploaddir,0777);
    
    $file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name'])); 
     
    if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
    { 
        $filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
    
        echo "$filename"; 
    
    } else {
        echo "error";
    }

    exit();
}

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

//Setting the default value of campaign index to false
$campaign_idx = FALSE;

//If campaign index is supplied in request URL
//Assigning the campaign index to our custom variable
if (isset($_GET['idx']))
    $campaign_idx = $_GET['idx'];

//If campaign index is not set in request url.
//Redirecting the user to campaign listing page to select one campaign and come back to this page.
if ($campaign_idx === FALSE)
    header('location: ' . dirname(s8_get_current_webpage_uri()) . '/ad_ad_campaigns.php');

//Retrieving the campaign data
$campaign_data = ad_ad_get_campaign_data($campaign_idx);

$response = '';
$response_type = '';

//Checking form submission
if (isset($_POST['ad_ad_update_settings'])):


    //General variables
    //Retrieving the name of campaign
    $ad_ad_campaign_name = $_POST['ad_ad_campaign_name'];
    //Retrieving the twilio caller id from form
    $phone_number = $_POST['ad_ad_ac_phone_number'];

    //Retrieving the check of manuallly entered contact
    $ad_ad_ac_add_contacts_1_by_1 = isset($_POST['ad_ad_ac_add_contacts_1_by_1']) ? TRUE : FALSE;
    //if add contacts 1 by 1 status is set for contacts textarea
    if ($ad_ad_ac_add_contacts_1_by_1):
        //Contacts should be 1 per line therfore exploding them with invisible new line character
        //to retrieve all the contacts
        $ad_ad_ac_contacts = explode("\n", $_POST['ad_ad_ac_contacts']);
    else:
        //If add contacts 1 by 1 is not set. 
        //Setting the null value to contacts variable, 
        //which will be used to access data of contacts textarea
        $ad_ad_ac_contacts = array();
    endif;
    //Assuming that file is not uploaded and if uploaded no contact number exist in it.
    //Seeting the null value assuming both case if no other
    $ad_ad_csv_file_contacts = array();
    //Retrieving the check whether file will be uploaded or not
    $ad_ad_ac_upload_csv = isset($_POST['ad_ad_ac_upload_csv']) ? TRUE : FALSE;

    //Retrieving the call script tokens
    $ad_ad_call_script_tokens = $_POST['ad_ad_call_script_tokens'];
    //Retrieving the call script
    $ad_ad_call_script = $_POST['ad_ad_call_script'];

    //Files_variables. Setting system value as default for most.
    $ad_ad_ac_upload_csv_file = '';
    $ad_ad_ac_voicemail_message_mp3 = array();

    //if files are uploaded
    if (isset($_FILES)):
        //Looping over each file
        foreach ($_FILES as $file_key => $file_details):

            //If file that is being iterated is not voicemail mp3 file
            if ($file_key != 'ad_ad_ac_voicemail_message_mp3'):

                //If no error in currently iterated file
                if ($file_details['error'] == 0):

                    //Moving it from temporary location to stable one
                    if (move_uploaded_file($file_details['tmp_name'], dirname(__FILE__) . '/uploads/' . $file_details['name'])):
                        if ($file_key == 'ad_ad_ac_upload_csv_file'):
                            //Storing the web accessible URI in variable defined for it dynamically
                            ${$file_key} = dirname(__FILE__) . '/uploads/' . $file_details['name'];
                        else:
                            //Storing the web accessible URI in variable defined for it dynamically
                            ${$file_key} = dirname(s8_get_current_webpage_uri()) . '/uploads/' . $file_details['name'];
                        endif;
                    endif;

                endif;

            else:

                //Retriving the count of total uploaded files
                $ad_ad_total_uploaded_files = count($file_details['name']);

                //Looping over files
                for ($i = 0; $i < $ad_ad_total_uploaded_files; $i++):

                    //If no error in currently iterated file
                    if ($file_details['error'][$i] == 0):

                        //If file is not of mp3 type
                        if ($file_details['type'][$i] != 'audio/mpeg' && $file_details['type'][$i] != 'audio/x-mpeg-3' && $file_details['type'][$i] != 'audio/mp3'):
                            $response .= 'File uploaded for "Voicemail Message MP3 ' + ($i + 1) + '" is not a valid mp3 file. Please re-upload only mp3 files for voicemail messages.<br/>';
                            $response_type = 'failure';
                        endif;

                        //Moving it from temporary location to stable one
                        if (move_uploaded_file($file_details['tmp_name'][$i], dirname(__FILE__) . '/uploads/' . $file_details['name'][$i])):
                            //Storing the web accessible URI in variable defined for it dynamically
                            ${$file_key}[] = dirname(s8_get_current_webpage_uri()) . '/uploads/' . $file_details['name'][$i];
                        endif;

                    //Closing condition checking error in file
                    endif;

                //Closing loop iterating over voicemail mp3 files
                endfor;

            endif;

        //Ending loop iteration over all uploaded files
        endforeach;
    endif;

    //If csv file is uploaded
    if ($ad_ad_ac_upload_csv === TRUE && $ad_ad_ac_upload_csv_file != ''):
        //Retrieving the contacts from it to store in system DB.
        $fp = fopen($ad_ad_ac_upload_csv_file, 'r+');
        while ($row = fgetcsv($fp)):
            $ad_ad_csv_file_contacts[] = trim($row[0]);
        endwhile;
        fclose($fp);
    endif;

    //Merging the contacts into single array
    $single_set_contacts = array_unique(array_merge($ad_ad_ac_contacts, $ad_ad_csv_file_contacts));

    if (!s8_is_str_blank($ad_ad_ac_upload_csv_file) && strpos($_FILES['ad_ad_ac_upload_csv_file']['name'], '.csv') === FALSE) {
        $response .= 'File uploaded for "Upload CSV File" is not a valid CSV file.<br/>';
        $response_type = 'failure';
    }

    //If phone number is not blank
    if (!s8_is_str_blank($phone_number) && !s8_is_str_blank($ad_ad_campaign_name) && $response_type != 'failure'):

        //Voicemail Message MP3 files
        $ad_ad_ac_voicemail_message_mp3 = array_merge($ad_ad_ac_voicemail_message_mp3, explode(',', $campaign_data['voicemail_message_mp3']));

        //Removing blank files if there is any
        foreach ($ad_ad_ac_voicemail_message_mp3 as $key => $value):
            //Checking for blank string as file URL
            //Deleting the entry
            if (s8_is_str_blank($value))
                unset($ad_ad_ac_voicemail_message_mp3[$key]);
        endforeach;

        foreach ($_POST['content'] as $key => $value) {
            $content = $value;

            if (!empty($content)) {
                $type = $_POST['type'][$key];
                $voice = $_POST['voice'][$key];
                $language = $_POST['language'][$key];

                $voicemail_messages_array[] = array(
                    "content" => $content,
                    "type" => $type,
                    "voice" => $voice,
                    "language" => $language
                );
            }
        }

        //Defining the voice bradcast campaigns table column name
        $campaign_columns_data = array(
            'campaign_name' => $ad_ad_campaign_name,
            'phone_number' => $phone_number,
            'voicemail_message_mp3' => (is_array($ad_ad_ac_voicemail_message_mp3) ? implode(',', $ad_ad_ac_voicemail_message_mp3) : ''),
            'voicemail_messages' => json_encode($voicemail_messages_array),
            'call_script_tokens' => $ad_ad_call_script_tokens,
            'call_script_text' => $ad_ad_call_script,
            'shared' => isset($_REQUEST['ad_ad_shared']) ? 1:0
        );

        //Adding the cauto dialer ampaign into the system and retrieving its Unique ID from system
        $update_status = ad_ad_update_campaign($campaign_idx, $campaign_columns_data);

        //If insert operation executed successfully
        if ($update_status != FALSE):

            //If there is atleast one new contact that is going to be added
            if (is_array($single_set_contacts) && $single_set_contacts != NULL):

                //Adding contacts in system DB
                ad_advb_add_contacts('ad', $campaign_idx, $single_set_contacts);

            endif;

            //Setting the success message to be displayed on screen
            $response .= 'Successfully updated campaign. <a href="' . dirname(s8_get_current_webpage_uri()) . '/ad_ad_campaigns.php">Click here</a> to proceed.<br/>';
            $response_type = 'success';

            //Updating campaign data variable
            $campaign_data = ad_ad_get_campaign_data($campaign_idx);

        endif;

    else:

        //Adding failure message that will be displayed on screen
        if (s8_is_str_blank($ad_ad_campaign_name))
            $response .= 'Campaign name is blank.<br/>';

        if (s8_is_str_blank($phone_number))
            $response .= 'Phone number is not selected. Please submit form after selecting the number.<br/>';

        //Setting the response type to failure
        $response_type = 'failure';

    endif;

endif;

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
        <style type="text/css">
            #token_list {
                height: 152px;
                margin-left: 8px;
                width: 21%;
                margin-bottom: 7px;
                padding: 5px;
                background: #fefefe;
                border-radius: 3px;
                border: 1px solid #bbb;
                font-family: Helvetica,"Lucida Grande", Verdana, sans-serif;
                font-size: 14px;
                color: #333;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                outline: none;
            }
        </style>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>

               <!-- <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Campaign API URL to add contacts remotely:</h2>
                    </div>
                    <div class="block_content">
                        <form>
                            <input type="text" class="text big" style="font-weight:normal;" value="<?php echo dirname(s8_get_current_webpage_uri()) . '/ad_ajax.php?action=HARAD&k=' . base64_encode($campaign_idx) . '&phone=&firstname=&lastname=&email='; ?>" />
                        </form>
                    </div>
                    <!-- .block_content ends - ->
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <div class="clear"></div> -->

                <div class="block bulk_dial_status" style="display: none;background: none;">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2 style="color:red;" class="ad_ad_bulk_dial_status_text"></h2>
                    </div>		
                </div>
                <div class="clear"></div>

                <!--Auto dialer campaigns listing section starts here-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Auto Dialer: Update Campaign "<?php echo ad_ad_get_campaign_name($campaign_idx) ?>"</h2>
                        <ul>
                            <li><a href="ad_ad_add_campaign.php">Add a Campaign</a></li>
                            <li><a href="ad_ad_campaigns.php">Campaigns List</a></li>
                            <li><a href="ad_contactlist_log.php">Contacts</a></li>
                            <li><a href="ad_ad_logs.php">Logs</a></li>
                        </ul>
                    </div>		<!-- .block_head ends -->
                    <div class="block_content">
                        <?php
                        //If response message is of success type
                        if ($response_type == 'success'):
                            //Printing thesuccess message
                            ?>
                            <div class="message success"><p><?php echo $response; ?></p></div>
                            <?php
                        //ELSE if response message is of failure type
                        elseif ($response_type == 'failure'):
                            //Pringitng hte error message
                            ?>
                            <div class="message errormsg"><p><?php echo $response; ?></p></div>
                            <?php
                        endif;
                        ?>
                        <form enctype="multipart/form-data" action="<?php echo s8_get_current_webpage_uri(); ?>" method="post">
                            <p>
                                <label>Campaign Name:</label><br />
                                <input type="text" readonly="readonly" class="text big" name="ad_ad_campaign_name" value="<?php echo $campaign_data['campaign_name']; ?>" />
                            </p>
                            <p>
                                <label>Phone Number:</label><br />
                                <select name="ad_ad_ac_phone_number" class="styled">
                                    <option value="">Select Number</option>
                                    <?php
                                    $numbers = $db->getCompanyNum($_SESSION['sel_co']);
//echo "<pre>";print_r($numbers);
                                    for ($i = 0; $i <= count($numbers) - 1; $i++):
                                        $num = $numbers[$i];
                                        ?>
                                        <option <?php echo ($campaign_data['phone_number'] == $num) ? ' selected="selected" ' : ''; ?> value="<?php echo $num; ?>"><?php echo $num; ?></option>
                                        <?php
                                    endfor;
                                    ?>
                                </select>
                            </p>
                            <?php if($campaign_data['user']==@$_SESSION['user_id']){ ?>
                            <p>
                                <input style="display: inline-block !important;" type="checkbox" name="ad_ad_shared" id="ad_ad_shared"<?php if($campaign_data['shared']==1) { echo " checked";} ?>/> <label for="ad_ad_shared">Share this campaign with my companies.</label>
                            </p>
                            <?php } ?>
                            <br/>

                            <script type="text/javascript">
                                if ($ == undefined) {
                                    $ = jQuery;
                                }
                                $(document).ready(function() {
                                    $('#cbdemo1').click(function() {
                                        if ($(this).is(':checked')) {
                                            $('.ad_ad_ac_contacts').slideDown('fast');
                                        } else {
                                            $('.ad_ad_ac_contacts').slideUp('fast');
                                        }
                                    });
                                    $('#cbdemo2').click(function() {
                                        if ($(this).is(':checked')) {
                                            $('.ad_ad_ac_upload_csv_file').slideDown('fast');
                                        } else {
                                            $('.ad_ad_ac_upload_csv_file').slideUp('fast');
                                        }
                                    });

                                    //Delegating the click event to delete button
                                    $('body').delegate('.delete_contact', 'click', function(e) {
                                        var phone_number_to_delete = $(this).attr('rel');

                                        if (confirm('Are you sure? You are deleting this "' + phone_number_to_delete + '" contact.')) {
                                            $.get('ad_ajax.php?action=delete_contact&number=' + phone_number_to_delete + '&campaign_idx=<?php echo $campaign_idx; ?>', function(response) {
                                                alert(response);
                                                $('.display_contacts').trigger('click');
                                            });
                                        }
                                        e.preventDefault();
                                    });
                                });
                            </script>

                            <p>
                                <a class="display_contacts" href="ad_ajax.php?action=ad_ad_ec_display_contacts&ad_ad_ec_campaign_idx=<?php echo $campaign_idx; ?>" rel="facebox">
                                    <input type="button" class="submit long" value="Display Contacts" />
                                </a>
                            </p>

                            <p>
                                <input style="float:left;margin-top:3px !important;" name="ad_ad_ac_add_contacts_1_by_1" value="true" type="checkbox" class="checkbox" id="cbdemo1" <?php echo isset($_POST['ad_ad_ac_add_contacts_1_by_1']) && $response_type != 'success' ? ' checked="checked" ' : ''; ?> /> <label class="left marginleft10 lh0" for="cbdemo1">Add Contacts 1 By 1</label>
                            </p>

                            <p class="ad_ad_ac_contacts" style="display: none;">
                                <label>Contacts:</label><br />
                                <textarea name="ad_ad_ac_contacts" class=""><?php echo isset($_POST['ad_ad_ac_contacts']) && $response_type != 'success' ? $_POST['ad_ad_ac_contacts'] : '' ?></textarea>
                            </p>

                            <p>
                                <input style="float:left;margin-top:3px !important;" name="ad_ad_ac_upload_csv" value="true" type="checkbox" class="checkbox" id="cbdemo2" <?php echo isset($_POST['ad_ad_ac_upload_csv']) && $response_type != 'success' ? ' checked="checked" ' : '' ?> /> <label class="left marginleft10 lh0" for="cbdemo2">Upload CSV?</label>
                            </p>

                            <p class="fileupload ad_ad_ac_upload_csv_file" style="display: none;">
                                <label>Upload CSV File:</label><br />
                                <input type="file" id="fileupload1" name="ad_ad_ac_upload_csv_file" />
                            </p>

                            <br/>

                            <script type="text/javascript">
                                if ($ === undefined) {
                                    $ = jQuery;
                                }
                                var mp3added = 1;
                                $(document).ready(function() {
                                    var vm_mp3_clone = $('.ad_ad_voicemail_message_mp3_html').html();
                                    $('.ad_ad_add_mp3_files').click(function(e) {
                                        if(mp3added==8){
                                            errMsgDialog("You can only add a max of 8 mp3's");
                                            return false;
                                        }
                                        $('.ad_ad_voicemail_message_mp3_area').append(vm_mp3_clone);
                                        var total_fields = $('.ad_ad_voicemail_message_mp3').length;
                                        var last_index = total_fields - 1;
                                        $('.ad_ad_voicemail_message_mp3:eq(' + last_index + ') label').text('Voicemail Message MP3 ' + total_fields + ':');
                                        mp3added = mp3added+1;
                                        e.preventDefault();
                                    });
                                });
                            </script>  

                            Voicemail messages:
                            <br />
                            <br />

                            <?php
                            $voicemail_messages = (empty($campaign_data['voicemail_messages']) ? array() : json_decode($campaign_data['voicemail_messages']));

                            if(is_array($voicemail_messages) || is_object($voicemail_messages)){
                                foreach ($voicemail_messages as $voicemail_message) {
                                    ?>
                                    <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                        <input type="hidden" class="content" name="content[]" value="<?php echo $voicemail_message->content; ?>" />
                                        <input type="hidden" class="type" name="type[]" value="<?php echo $voicemail_message->type; ?>" />
                                        <input type="hidden" class="voice" name="voice[]" value="<?php echo $voicemail_message->voice; ?>" />
                                        <input type="hidden" class="language" name="language[]" value="<?php echo $voicemail_message->language; ?>" />
                                        <div class="ivr-Menu-selector" style="display: block">
                                            <a href="javascript: void(0);" onclick="$(this).parents('fieldset.ivr-Menu').remove();" style="display: block; position: absolute; right: 5px; top: 4px; color: red; z-index: 999; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;" /></a>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($voicemail_message->type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($voicemail_message->type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($voicemail_message->type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($voicemail_message->type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                            </div>
                                          </div>
                                          <div class="ivr-Menu-editor ">
                                            <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                              <div class="ivr-Menu-read-text" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <br>
                                                <div>
                                                  <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                                    <label class="field-label">
                                                        <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($voicemail_message->type == 'Text')? $voicemail_message->content:''); ?></textarea>

                                                        <?php
                                                        $voice = (($voicemail_message->type == 'Text')? $voicemail_message->voice:'');
                                                        $language = (($voicemail_message->type == 'Text')? $voicemail_message->language:'');
                                                        ?>
                                                        <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                        <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                          <?php
                                                          echo Util::getTwilioVoices($voice);
                                                          ?>
                                                        </select>

                                                        <br clear="all" />

                                                        <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                        <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                          <?php
                                                          echo Util::getTwilioLanguages($voice, $language);
                                                          ?>
                                                        </select>

                                                        <br clear="all" /><br />

                                                        <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                        <script type="text/javascript">
                                                          $(document).ready(function() {
                                                            $("#voice").trigger("change");
                                                          });
                                                        </script>

                                                        <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                                    </label>
                                                  </fieldset>
                                                </div>
                                                <br>
                                                <br>
                                              </div>
                                              <div class="ivr-audio-upload" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>

                                                    <span class="title" <?php if ( $voicemail_message->type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                                    <?php
                                              echo (($voicemail_message->type == 'Audio')? $voicemail_message->content:''); ?>
                                                    </strong></span> <br>


                                                    <span class="title">Click to select a file: </span>
                                                    <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                                    <span class="title" id="statusUpload">&nbsp;</span>
                                                  </div>

                                                </div>
                                              </div>
                                              <div class="ivr-mp3-url" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>

                                                    <span class="title">
                                                      <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($voicemail_message->type == 'MP3_URL')? $voicemail_message->content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                                      <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                                    </span>

                                                    <br /><br />

                                                    <span class="title" <?php if ( $voicemail_message->type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                                    <?php
                                              echo (($voicemail_message->type == 'MP3_URL')? $voicemail_message->content:''); ?>
                                                    </strong></span>

                                                  </div>


                                                </div>
                                              </div>
                                              <div class="ivr-record-audio" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>

                                                    Caller ID:

                                                    <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                      <option value="">Select number</option>
                                                      <?php
                                                      $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                                      foreach ($numbers as $number) {
                                                        ?>
                                                          <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                    </select>

                                                    Your phone number:

                                                    <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                                    <br /><br />

                                                    <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                                    <br /><br />

                                                    <span class="title" <?php if ( $voicemail_message->type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                                        <?php if ($voicemail_message->type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($voicemail_message->content, 'sm'); } ?>
                                                    </span>

                                                  </div>


                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </fieldset>
                                        <?php
                                }
                            }
                            ?>
                            <div class="ad_ad_voicemail_message_mp3_area">
                                <div class="ad_ad_voicemail_message_mp3_html">
                                    <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                        <input type="hidden" class="content" name="content[]" />
                                        <input type="hidden" class="type" name="type[]" />
                                        <input type="hidden" class="voice" name="voice[]" />
                                        <input type="hidden" class="language" name="language[]" />
                                        <div class="ivr-Menu-selector" style="display: block">
                                            <a href="javascript: void(0);" onclick="$(this).parents('fieldset.ivr-Menu').remove();" style="display: block; position: absolute; right: 5px; top: 4px; color: red; z-index: 999; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;" /></a>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                            </div>
                                          </div>
                                          <div class="ivr-Menu-editor ">
                                            <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                              <div class="ivr-Menu-read-text" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <br>
                                                <div>
                                                  <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                                    <label class="field-label">
                                                        <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"></textarea>

                                                        <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                        <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                          <?php
                                                          echo Util::getTwilioVoices();
                                                          ?>
                                                        </select>

                                                        <br clear="all" />

                                                        <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                        <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                          <?php
                                                          echo Util::getTwilioLanguages();
                                                          ?>
                                                        </select>

                                                        <br clear="all" /><br />

                                                        <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                        <script type="text/javascript">
                                                          $(document).ready(function() {
                                                            $("#voice").trigger("change");
                                                          });
                                                        </script>

                                                        <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                                    </label>
                                                  </fieldset>
                                                </div>
                                                <br>
                                                <br>
                                              </div>
                                              <div class="ivr-audio-upload" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>
                                                    
                                                    <span class="title" style="display:none" id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename"></strong></span> <br>
                                                    
                                                    
                                                    <span class="title">Click to select a file: </span>
                                                    <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                                    <span class="title" id="statusUpload">&nbsp;</span>
                                                  </div>                                                   
                                                
                                                </div>
                                              </div>
                                              <div class="ivr-mp3-url" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>           
                                                    
                                                    <span class="title">
                                                      <input type="text" name="mp3_url_text" id="mp3_url_text" value="" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                                      <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                                    </span>

                                                    <br /><br />

                                                    <span class="title" style="display:none" id="mp3UrlSaved" >MP3 to play: <strong></strong></span>

                                                  </div>
                                                
                                                
                                                </div>
                                              </div>
                                              <div class="ivr-record-audio" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>           
                                                    
                                                    Caller ID:

                                                    <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                      <option value="">Select number</option>
                                                      <?php
                                                      $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                                      foreach ($numbers as $number) {
                                                        ?>
                                                          <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                    </select>

                                                    Your phone number:

                                                    <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                                    <br /><br />

                                                    <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                                    <br /><br />

                                                    <span class="title" style="display:none"  id="recordedAudioSavedWrapper" ></span>

                                                  </div>
                                                
                                                
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </fieldset>
                                </div>
                            </div>

                            <br />
                            
                            <input type="button" class="submit small ad_ad_add_mp3_files" value="+Add" />

                            <br/>
                            <br/>

                            <p>
                                <label>Call Script:</label>
                                <input type="button" id="USR_AD_CSLOAD" class="submit small" style="display:inline-block !important;" value="Load">
                                <input type="button" id="USR_AD_CSSAVE" class="submit small" style="display:inline-block !important;" value="Save"><br>
                                <input type="hidden" class="text big" name="ad_ad_call_script_tokens" value="<?php echo $campaign_data['call_script_tokens']; ?>" /><br/>
                                <textarea name="ad_ad_call_script" style="width: 75%;"><?php echo $campaign_data['call_script_text']; ?></textarea>
                                <select id="token_list" name="ad_ad_tokens" size="10">
                                    <option value="[FirstName]">[FirstName]</option>
                                    <option value="[LastName]">[LastName]</option>
                                    <option value="[Email]">[Email]</option>
                                    <option value="[Phone]">[Phone]</option>
                                    <option value="[Address]">[Address]</option>
                                    <option value="[City]">[City]</option>
                                    <option value="[State]">[State]</option>
                                    <option value="[Zip]">[Zip]</option>
                                    <option value="[Website]">[Website]</option>
                                    <option value="[Business]">[Business]</option>
                                </select>
                            </p>

                            <p>
                                <input style="float:left;" type="submit" class="submit small" name="ad_ad_update_settings" value="Save" />
                                <input style="float:left;" type="button" class="submit small" value="Cancel" onclick="window.document.location = 'ad_ad_campaigns.php';" />
                            </p>
                        </form>
                    </div>
                    <!-- .block_content ends -->
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Auto dialer campaigns listing ends here-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>

        <!--//Notification bar html-->
        <div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
        <script type="text/javascript">
            $.fn.insertAtCaret = function (tagName) {
                return this.each(function(){
                    if (document.selection) {
                        //IE support
                        this.focus();
                        sel = document.selection.createRange();
                        sel.text = tagName;
                        this.focus();
                    }else if (this.selectionStart || this.selectionStart == '0') {
                        //MOZILLA/NETSCAPE support
                        startPos = this.selectionStart;
                        endPos = this.selectionEnd;
                        scrollTop = this.scrollTop;
                        this.value = this.value.substring(0, startPos) + tagName + this.value.substring(endPos,this.value.length);
                        this.focus();
                        this.selectionStart = startPos + tagName.length;
                        this.selectionEnd = startPos + tagName.length;
                        this.scrollTop = scrollTop;
                    } else {
                        this.value += tagName;
                        this.focus();
                    }
                });
            };
            $("#token_list").find("option").on('dblclick', function(){
                insertToken(this.value);
            });

            function insertToken(token){
                $("textarea[name='ad_ad_call_script']").insertAtCaret(token);
            }

            function showAudioText(obj)
            {
                
                
                var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
                audioChoice.hide();
                audioChoice.parent().children('.ivr-Menu-editor').show();
                var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');
                
                if ( obj.id  == 'txt' ) 
                {       
                    
                    subDiv.children('.ivr-Menu-read-text').show();
                    
                    //////////////// only to avoid  file button clickable in text area      
                    $('[name="uploadfile"]').css('z-index','-1');       
                
                }
                else if ( obj.id  == 'upload_mp3' ) {
                    $('[name="uploadfile"]').css('z-index','2147483583');
                    
                    subDiv.children('.ivr-audio-upload').show();    
                    SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');        
                    UploadFile(SubObj); 
                }
                else if ( obj.id  == 'mp3_url' ) {
                    subDiv.children('.ivr-mp3-url').show();
                }
                else if ( obj.id  == 'record_audio' ) {
                    subDiv.children('.ivr-record-audio').show();
                }
                
            }

            function CloseButton(obj)
            {
                var audioChoice = $(obj).closest('.ivr-Menu');
                
                var audioChoiceEditor   = audioChoice.children('.ivr-Menu-editor');
                var audioChoiceSelector = audioChoice.children('.ivr-Menu-selector');
                
                var subDiv  = audioChoiceEditor.children('.ivr-Menu-editor-padding');
                
                audioChoiceSelector.show();
                audioChoiceEditor.hide();
                subDiv.children('.ivr-audio-upload').hide();    
                subDiv.children('.ivr-Menu-read-text').hide();
                subDiv.children('.ivr-mp3-url').hide();
                subDiv.children('.ivr-record-audio').hide();
            }

            function testVoice(voice, language, text) {
                if ($("#test_voice_iframe_wrapper").length == 0) {
                    $("body").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
                                        '<form id="test_voice_form" action="test_voice.php" method="post" target="test_voice_iframe">' +
                                            '<input type="hidden" name="voice" id="voice" />' +
                                            '<input type="hidden" name="language" id="language" />' +
                                            '<input type="hidden" name="text" id="text" />' +
                                            '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                        '</form>' +
                                        '<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
                                    '</form>'
                    );
                }

                $("#test_voice_iframe_wrapper #voice").val(voice);
                $("#test_voice_iframe_wrapper #language").val(language);
                $("#test_voice_iframe_wrapper #text").val(text);
                $('#test_voice_iframe_wrapper #submit').click()
             }

             function SaveContent(obj, content_type) {
                var thisBlock = $(obj).closest('.ivr-Menu');
                switch (content_type) {
                    case "Text_mail":
                    {
                        var voice_text = $(thisBlock).find('#readtxt_mail').val();
                        var voice = $(thisBlock).find('#voice').val();
                        var language = $(thisBlock).find('#language').val();
                        $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find('.content').val(voice_text);
                        $(thisBlock).find('.type').val("Text");
                        $(thisBlock).find('.voice').val(voice);
                        $(thisBlock).find('.language').val(language);

                        break;
                    }
                    case "MP3_URL":
                    {

                        var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                        $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find('.content').val(mp3_url);
                        $(thisBlock).find('.type').val("MP3_URL");

                        break;
                    }
                }
            }

            function UploadFile(obj)
            {
                var status = $(obj).closest('.explanation').find('#statusUpload');
                var fileNameStatus = $(obj).closest('.explanation').find('#voicefilename');
                var fileNameStatusWrapper = $(obj).closest('.explanation').find('#voicefilenameWrapper');
                
                new AjaxUpload(obj, {
                    
                    
                    action: 'ad_ad_add_campaign.php?act=uploadMp3',
                    name: 'uploadfile',
                    onSubmit: function(file, ext){
                         if (! (ext && /^(mp3|wma)$/.test(ext))){ 
                            // extension is not allowed 
                            status.text('Only MP3 files are allowed');
                            return false;
                        }
                        (fileNameStatus).html('Uploading...');
                    },
                    onComplete: function(file, response){
                        //On completion clear the status
                        
                        //Add uploaded file to list
                        
                        if(response!="error"){
                            $(fileNameStatusWrapper).css('display' ,'block');
                            $(fileNameStatus).html(response);
                            var thisBlock = $(obj).closest('.ivr-Menu');

                            $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                            $(thisBlock).find('#upload_mp3').addClass('ivr-Menu-Selected');
                            $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                            $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                            $(thisBlock).find('.content').val(response);
                            $(thisBlock).find('.type').val("Audio");
                            $(thisBlock).find('.ivr-Menu-close-button').click();

                        } else{
                        }
                    }
                });
                    
            }

            var recordingId = "";
            function recordAudio(obj) {
                if ($(obj).val() == "Call Me") {
                    recordingId = createUUID();
                    var record_from = $(obj).parents('.ivr-Menu').find('#record_from').val();
                    var record_to = $(obj).parents('.ivr-Menu').find('#record_to').val();

                    if (record_from == "") {
                        errMsgDialog("Please select Caller ID.");
                        return false;
                    }

                    if (record_to == "") {
                        errMsgDialog("Please enter your phone number.")
                        return false;
                    }

                    $(obj).val('Stop');

                    if ($("#record_audio_iframe_wrapper").length == 0) {
                        $("body").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
                                            '<form id="record_audio_form" action="record_audio.php" method="post" target="record_audio_iframe">' +
                                                '<input type="hidden" name="recordingId" id="recordingId" />' +
                                                '<input type="hidden" name="record_from" id="record_from" />' +
                                                '<input type="hidden" name="record_to" id="record_to" />' +
                                                '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                            '</form>' +
                                            '<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
                                        '</form>'
                        );
                    }

                    $("#record_audio_iframe_wrapper #recordingId").val(recordingId);
                    $("#record_audio_iframe_wrapper #record_from").val(record_from);
                    $("#record_audio_iframe_wrapper #record_to").val(record_to);
                    $('#record_audio_iframe_wrapper #submit').click()
                }
                else {
                    $(obj).val('Please wait...');
                    $("#record_audio_iframe").contents().find("#disconnectBtn").click();

                    setTimeout(function() {
                        $.post("admin_ajax_handle.php", { func: "GET_RECORDING", recordingId: recordingId },  function(response) {
                            var thisBlock = $(obj).closest('.ivr-Menu');

                            $(thisBlock).find('#recordedAudioSavedWrapper').css('display' ,'block');
                            if (response) {
                                $(thisBlock).find('#recordedAudioSavedWrapper').html(response.playable);
                                
                                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                                $(thisBlock).find('.content').val(response.url);
                                $(thisBlock).find('.type').val("RECORD_AUDIO");
                                $(thisBlock).find('.ivr-Menu-close-button').click();
                            }
                            else {
                                errMsgDialog('Audio was not recorded, please try again.');
                                $(thisBlock).find('#recordedAudioSavedWrapper').html('');
                            }
                            $(obj).val('Call Me');
                        }, "json");
                    }, 2000);
                }
             }

             function createUUID() {
                // http://www.ietf.org/rfc/rfc4122.txt
                var s = [];
                var hexDigits = "0123456789abcdef";
                for (var i = 0; i < 36; i++) {
                    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
                }
                s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
                s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
                s[8] = s[13] = s[18] = s[23] = "-";

                var uuid = s.join("");
                return uuid;
            }
        </script>
    </body>
</html>