<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$db = new DB();
//This is an admin-only page.
//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}

if(@$_SESSION['permission']<1 && !$_SESSION['prems']['purchase_numbers']) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

if ($_SESSION['prems']['purchase_numbers']) {
    if(!isset($_SESSION['sel_co']))
    {
        header("Location: companies.php?sel=no");
        exit;
    }
}

global $AccountSid, $AuthToken, $ApiVersion;
$client = new TwilioRestClient($AccountSid, $AuthToken);

if(isset($_REQUEST['sid']) && isset($_REQUEST['num']) && @$_REQUEST['delete']=="true")
{
    //die($_REQUEST['num']);
    $response = @$client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers/" . $_REQUEST['sid'], "DELETE", $data);
    if($response->IsError)
    {
        ?>
    <script type="text/javascript">
        alert('Error deleting number.');
        window.location = "setup_phone_numbers.php";
    </script>
    <?php exit;
    }
    else
    {
        if(substr($_REQUEST['num'],0,1)=="1" || trim(substr($_REQUEST['num'],0,2)) == "1")
            $db->deleteNumber($db->format_phone_db($_REQUEST['num']));
        else
            $db->deleteNumber($_REQUEST['num']);
        ?>
    <script type="text/javascript">
        alert('Deleted number.');
        window.location = "setup_phone_numbers.php";
    </script>
    <?php
        if(isset($_SESSION['twilio_numbers'])){
            $_SESSION['twilio_numbers'] = null;
            $_SESSION['twilio_numbers_last_upd'] = null;
        }
        exit;
    }
}

$status_label = "";

if ( isset($_REQUEST['Sid'])){
    //update a Twilio number
    $data = array(
        "FriendlyName" => $_REQUEST['friendly_name_static'],
        "VoiceUrl" => $_REQUEST['url'],
        "SmsUrl" => $_REQUEST['sms_url']
    );
    $response = $client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers/" . $_REQUEST['Sid'], "POST", $data);
    if($response->IsError)
        die(json_encode(array("result"=>"fail","reason"=>$response->ErrorMessage)));
    else{
        if(isset($_SESSION['twilio_numbers'])){
            $_SESSION['twilio_numbers'] = null;
            $_SESSION['twilio_numbers_last_upd'] = null;
        }
        die(json_encode(array("result"=>"success","number"=>$response->ResponseXml->IncomingPhoneNumber->PhoneNumber)));
    }
}

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'forward_sms_ui':

        $phone_number_from = $_GET['phone_number'];
        $phone_number_to = $db->get_sms_forward($phone_number_from);
        ?>
            <div class="block" style="margin-bottom: 0px; border-bottom: 0px; background: none;">
                <form action="" onsubmit="return false;">
                    Enter phone number: <br />
                    <input type="hidden" class="text" id="sms_forward_number_from" value="<?php echo $phone_number_from; ?>" />
                    <input type="text" class="text" id="sms_forward_number_to" style="width: 220px;" value="<?php echo $phone_number_to; ?>" />
                </form>
            </div>
        <?php
        break;
    }
    exit();
}

$client = new Services_Twilio($AccountSid,$AuthToken);
$twilio_numbers_count = array($client->account->incoming_phone_numbers);

$page="adminpage";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        #purchase_number, #reset_url, #update_number,#delete_number{
            width: 85px;
            height: 30px;
            line-height: 30px;
            background: url(images/btns.gif) top center no-repeat;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            text-transform: uppercase;
            color: white;
            text-shadow: 1px 1px 0 #0A5482;
            cursor: pointer;
            margin-right: 10px;
            vertical-align: middle;
            display: inline-block !important;
        }
        .ui-dialog label, input {
            display: inline-block !important;
        }
        .cmf-skinned-select{
            display: inline-block !important;
        }
        select#country option {
            background-repeat:no-repeat;
            background-position:bottom left;
            padding-left:30px;
        }
        select#country option[value="US"] {
            background-image:url(images/us.png);
        }
        select#country option[value="CA"] {
            background-image:url(images/ca.png);
        }
        select#country option[value="GB"] {
            background-image:url(images/gb.png);
        }
        .voice_url, .sms_url {
            font-size: 10px;
            width: 350px;
            height: 17px;
        }
        .incorrect-url {
            background-color: #F9FF66 !important;
            border: 1px gray solid !important;
        }
        .capa {
            font-size: 10px;
            cursor: default;
            color: #fff;
            padding: 5px;
            border-radius: 3px;
            margin-right: 12px;
        }
        .voice {
            background: #4D98E9;
        }
        .sms {
            background: #37A366;
        }
        .mms {
            background: #DFA718;
        }
        .no-capa {
            display:none;
            /*background: #BDBDBD;*/
        }
    </style>

</head>

<body>

<div id="hld">
<div class="wrapper">		<!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->


    <div class="block" id="filter-box" style="width:465px; margin:0 auto; margin-bottom: 25px;">

        <div class="block_head">
            <div class="bheadl"></div>
            <div class="bheadr"></div>

            <h2>
                Purchase a number
            </h2>

        </div>		<!-- .block_head ends -->


        <div class="block_content">

            <form action="#" method="POST">
                <input type="hidden" id="new_url" value="" />
                <input type="hidden" id="new_sms_url" value="" />
                <span>At LEAST one field required.</span><br/><br/>
                <label>Country: </label><br/>
                <script type="text/javascript">
                    function checkRegion(elm){
                        var region_enabled = elm.selectedOptions[0].getAttribute('data-region');
                        if(region_enabled==1){
                            $("#postal_code_area").show();
                            $("#search_tollfree").attr('style','display:inline-block !important');
                        }else{
                            $("#postal_code_area").hide();
                            $("#search_tollfree").attr('style','display:none !important');
                        }
                    }
                </script>
                <select id="country" onchange="checkRegion(this);" style="width:426px;">
                    <option value="US" selected="selected" data-region="1">United States (+1)</option>
                    <?php
                        foreach(Util::$supported_countries as $country_code => $country){
                            ?>
                    <option value="<?php echo $country_code; ?>" data-region="<?php echo @$country[2]!="" ? $country[2]:0; ?>"><?php echo $country[0]. " (+".$country[1].")"; ?></option>
                            <?php
                        }
                    ?>
                </select><br/><br/>
                <script>
                    function format(state) {
                        if (!state.id) return state.text;
                        return "<img class='flag' width='15' src='images/flags/" + state.id.toLowerCase() + ".gif'/>&nbsp;&nbsp;" + state.text;
                    }
                    $(document).ready(function() { $("#country").select2({
                        formatResult: format,
                        formatSelection: format,
                        escapeMarkup: function(m) { return m; }
                    }); });
                </script>
                <span id="postal_code_area"><label>Near US/CA postal code (e.g. 94117): </label><input class="text small" type="text" id="postal_code"/><br/><br/></span>
                <label>Near this other number (e.g. +14156562345): </label><input class="text small" type="text" id="near_number"/><br/><br/>
                <label>Area Code or Matching this pattern (e.g. 415***EPIC): </label><input class="text small" type="text" id="contains"/><br/>
                <p style="font-size:10px;"><i>To search for area code, enter: 772******* (replace '772' with your area code)</i></p>
                <br/>
                <input type="submit" value="Search TOLL-FREE" id="search_tollfree" class="submit long" style="margin-bottom: 5px; float:left">
                <input type="submit" value="Search" id="search_num" class="submit mid" style="margin-bottom: 5px; float:right;">
            </form>

        </div>		<!-- .block_content ends -->

        <div class="bendl"></div>
        <div class="bendr"></div>
    </div>		<!-- .block ends -->


    <div class="block" id="results-box" style="margin:0 auto; margin-bottom: 25px; display:none;">

        <div class="block_head">
            <div class="bheadl"></div>
            <div class="bheadr"></div>

            <h2>
                Number Results &nbsp; &nbsp; <a href="#" id="reset_num_search">RESET SEARCH</a>
            </h2>
            <div style="float:right;">
                <form action="#" style="padding:12px 0;">
                    <input type="submit" class="submit long purchase_numbers" value="Purchase Number(s)">
                </form>
            </div>
        </div>		<!-- .block_head ends -->


        <div class="block_content">

            <!-- Results -->
            <table id="dataTable" cellpadding="0" cellspacing="0" width="100%" class="sortable">

                <thead>
                <tr>
                    <th></th>
                    <th>Formatted Number</th>
                    <th>Number</th>
                    <th>Location</th>
                    <!--<th>Postal Code</th>-->
                    <th>Capabilities</th>
                </tr>
                </thead>

                <tbody>

                </tbody>
            </table>
        </div>		<!-- .block_content ends -->

        <div class="bendl"></div>
        <div class="bendr"></div>
    </div>		<!-- .block ends -->


<?php
$valid_numbers = array();

foreach ($client->account->incoming_phone_numbers as $number){ 
    if ($_SESSION['permission'] == 0) {
        if (!$db->companyHasNum($_SESSION['sel_co'], $number->phone_number)) {
            continue;
        }
    }

    $valid_numbers[] = $number;
}
?>

    <div class="block">

        <div class="block_head">
            <div class="bheadl"></div>
            <div class="bheadr"></div>

            <h2>
                Your twilio numbers (<?php echo count($valid_numbers); ?>) &nbsp;&nbsp;&nbsp; <a href="manage_exceptions.php">Manage Exceptions</a>  &nbsp;&nbsp;&nbsp; <a href="javascript: void(0);" onclick="$('select.action').each(function() { $(this).val('reset_url'); $(this).trigger('change'); });">Reset All Numbers</a>
            </h2>

        </div>		<!-- .block_head ends -->


        <div class="block_content">
<?php if (count($twilio_numbers_count)>0){
        ?>
        <form action="" onsubmit="return false;">
            <table cellpadding="0" cellspacing="0" width="100%" class="sortable">

                <thead>
                <tr>
                    <th>Number</th>
                    <th style="width: 200px !important;">Campaign</th>
                    <th>URL</th>
                    <th style="width: 115px !important;">Action</th>
                </tr>
                </thead>

                <tbody>
                <?php
                foreach ($valid_numbers as $number) {
                    $Sid          = $number->sid;
                    $FriendlyName = Util::escapeString($number->friendly_name);
                    $PhoneNumber  = $number->phone_number;
                    $VoiceUrl     = $number->voice_url;
                    $SmsUrl     = $number->sms_url;

                    if(!Util::isNumberIntl($PhoneNumber))
                    {
                        $phone_number = Util::format_phone_us($PhoneNumber);
                        $company_name = $db->getCompanyName($db->getCompanyOfNumber($db->format_phone_db($PhoneNumber)));
                    }else{
                        $phone_number = format_phone($db->format_phone_db($PhoneNumber));
                        $company_name = $db->getCompanyName($db->getCompanyOfNumber(substr($PhoneNumber,1,(strlen($PhoneNumber)-1))));
                    }

                    $number_details = $db->getOutgoingNumberDetails($db->format_phone_db($PhoneNumber));

                    if ($number_details['pool_id'] == 0 || empty($number_details['pool_id'])) {
                    ?>
                    <tr id="<?php echo $Sid; ?>">
                            <input type="hidden" name="friendly_name_static" value="<?php echo($FriendlyName);?>">
                            <input type="hidden" name="Sid" value="<?php echo($Sid);?>">
                            <input type="hidden" name="number" value="<?php echo ($phone_number); ?>">
                            <td><span style="font: 9px Verdana bold;" title="<?php echo $company_name; ?>"><?php echo Util::limit_text($company_name,32); ?></span><br><?php echo $phone_number;?></td>
                            <td><input type="text" name="friendly_name" value="<?php echo($FriendlyName);?>" disabled="true" style="width:135px; height:17px; display: inline-block !important;"><a href="#" id="SYSTEM_EDIT_NUMBER_CAMPAIGN" data-params="<?php echo $Sid; ?>"><img src="images/cog_edit.png" style="vertical-align: middle;padding-left: 5px;" title="Edit Campaign" /></a></td>
                            <td>
                                Call: <input type="text" class="text" value="<?php echo($VoiceUrl);?>" class="voice_url" name="url" style="font-size: 10px; width: 300px; margin-left: 6px;" />
                                <div style="padding-top: 3px;">
                                    SMS: <input type="text" class="text" value="<?php echo($SmsUrl);?>" class="sms_url" name="sms_url" style="font-size: 10px; width: 300px; margin-left: 5px;" />
                                </div>
                            </td>
                            <td><!-- class="styled" -->
                                <select class="action" onchange="selectAction(this, '<?php echo $Sid; ?>', '<?php echo $db->format_phone_db($PhoneNumber); ?>');" style="width: 100px;">
                                    <option value="">Select</option>
                                    <option value="reset_url">Reset Url</option>
                                    <option value="update_number">Update</option>
                                    <option value="delete_number">Delete</option>
                                    <option value="sms_forward">SMS Forward</option>
                                </select>
                            </td>
                    </tr>
                <?php }
                } ?>
            </table>
        </form>
            <?php } else { ?>
            <p>
                Your account has not purchased any Twilio Incoming Numbers.  You may purchase numbers using the form above.
                <br/><br/>Or, you can use your existing Twilio numbers by setting your <br/>Voice URL to: <span id="sandbox"></span>
            </p>
            <?php } ?>
        </div>		<!-- .block_content ends -->

        <div class="bendl"></div>
        <div class="bendr"></div>
    </div>		<!-- .block ends -->

<?php include "include/footer.php"; ?>
    <script type="text/javascript">
        document.getElementById('new_url').value=document.location.href.split("setup_phone_numbers.php")[0] + 'handle_incoming_call.php';
        document.getElementById('new_sms_url').value=document.location.href.split("setup_phone_numbers.php")[0] + 'handle_incoming_sms.php';
        if (document.getElementById('sandbox')) document.getElementById('sandbox').innerHTML=document.location.href.split("setup_phone_numbers.php")[0] + 'handle_incoming_call.php';
    </script>
<script src="//maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
            $(document).ready(function(){
                $(".action").select2({
                    minimumResultsForSearch: -1
                });
                checkNumbers();
            });

            function checkNumbers(){
                $("[name='url']").each(function(i,v){
                    var match = new RegExp(document.location.host + document.location.pathname.split("setup_phone_numbers.php")[0] + "handle_incoming_call.php$","i");
                    var url = document.createElement('a');
                    url.href = $(v).val();
                    url = url.host + url.pathname;
                    if(!url.match(match))
                    {
                        $(v).addClass("incorrect-url");
                        $(v).attr("title","Calls to this number may not reach ACT.");
                        $(v).parents().parents().filter("tr").children().filter("td").children("#reset_url").show();
                    }else{
                        try{
                            $(v).tooltipster("destroy");
                            $(v).attr("title","");
                        }catch(ex){}
                    }
                });

                $("[name='sms_url']").each(function(i,s){
                    var match = new RegExp(document.location.host + document.location.pathname.split("setup_phone_numbers.php")[0] + "handle_incoming_sms.php$","i");
                    var url = document.createElement('a');
                    url.href = $(s).val();
                    url = url.host + url.pathname;
                    if(!$(s).val().match(match))
                    {
                        $(s).addClass("incorrect-url");
                        $(s).attr("title","SMS to this number may not reach ACT.");
                        $(s).parents().parents().filter("tr").children().filter("td").children("#reset_url").show();
                    }else{
                        try{
                            $(s).tooltipster("destroy");
                            $(s).attr("title","");
                        }catch(ex){}
                    }
                });
                $(".incorrect-url").tooltipster({
                    position: 'bottom',
                    theme: '.tooltipster-shadow'
                });
            }

            function selectAction(el, sid, phone_number) {
                if (el.value == "reset_url")
                    reseturl(sid);

                if (el.value == "update_number")
                    updateNumber(sid);

                if (el.value == "delete_number")
                    deleteNumber(sid, phone_number);

                if (el.value == "sms_forward")
                    SMSForward(phone_number);

                $(el).val('');
                $(el).select2('val','');
            }

            function reseturl(sid)
            {
                var number = $("#"+sid+" > input[name*='number']").val();
                $.post("setup_phone_numbers.php", {
                        Sid:sid,
                        url: document.location.href.split("setup_phone_numbers.php")[0] + 'handle_incoming_call.php',
                        sms_url: document.location.href.split("setup_phone_numbers.php")[0] + 'handle_incoming_sms.php',
                        friendly_name_static:  $("#"+sid+" > input[name*='friendly_name_static']").val()
                    },function(data){
                        if(data.result=="success"){
                            var elm = $("#"+sid+"> td input[name$='url']");
                            elm.val(document.location.href.split("setup_phone_numbers.php")[0] + 'handle_incoming_call.php');
                            elm.removeClass("incorrect-url");
                            elm.attr("title","");

                            $("#"+sid).children().filter("td").children("#reset_url").attr("style","display:none !important");

                            var elm2 = $("#"+sid+"> td input[name$='sms_url']");
                            elm2.val(document.location.href.split("setup_phone_numbers.php")[0] + 'handle_incoming_sms.php');
                            elm2.removeClass("incorrect-url");
                            elm2.attr("title","");

                            $("#"+sid).children().filter("td").children("#reset_url").attr("style","display:none !important");

                            notifyWithCheckmark("Updated number: " + number);

                            checkNumbers();
                        }

                        else
                            notifyWithCheckmark("Error updating number.");
                    },
                    'json');
                return false;
            }

            function updateNumber(sid)
            {
                var number = $("#"+sid+" > input[name*='number']").val();
                $.post("setup_phone_numbers.php", {
                    Sid:sid,
                    url: $("#"+sid+" > td input[name*='url']").val(),
                    sms_url: $("#"+sid+" > td input[name*='sms_url']").val(),
                    friendly_name_static:  $("#"+sid+" > input[name*='friendly_name_static']").val()
                },function(data){
                        if(data.result=="success"){
                            notifyWithCheckmark("Updated number: " + number);
                            checkNumbers();
                        }
                        else
                            notifyWithCheckmark("Error updating number.");
                    },
                'json');
                return false;
            }

            function deleteNumber(sid,number)
            {
                promptMsg("Are you sure you want to permanently delete this number?",function(){
                    document.location = "setup_phone_numbers.php?delete=true&sid="+sid+"&num="+number;
                });
            }

            function SMSForward(number) {
                if ($("#forward_sms_wrapper").length == 0) {
                    $("body").append('<div id="forward_sms_wrapper"></div>')
                }

                $("#forward_sms_wrapper").text('Loading...');

                $("#forward_sms_wrapper").dialog({
                    modal: true,
                    autoOpen: true,
                    height: 'auto',
                    width: 250,
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>' +
                            'SMS Forward',
                    buttons: [{
                        text: "Save",
                        click: function() {
                            var el = $(this);
                            var response = JSONajaxCall({
                                func: "SYSTEM_SAVE_SMS_FORWARD_NUMBER",
                                data: {
                                    from: $("#sms_forward_number_from").val(),
                                    to: $("#sms_forward_number_to").val()
                                }
                            });
                            response.done(function(msg) {
                                el.dialog("close");
                                el.dialog('destroy').remove();
                                infoMsgDialog("Saved forward number.");
                            });
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove();
                        }
                    }]
                });

                $.get("setup_phone_numbers.php?action=forward_sms_ui&phone_number=" + number, function(data) {
                    $("#forward_sms_wrapper").html(data);
                    $("#sms_forward_number_to").focus();
                });
            }

            $(".purchase_numbers").click(function(event){
                event.preventDefault();
                var num_to_buy = new Array();
                $(".add-to-buy").each(function(i,v){
                    if (typeof $(v).attr("checked") !== 'undefined' && $(v).attr("checked") !== false) {
                        num_to_buy.push($(v).val());
                    }
                });
                if(num_to_buy.length>10)
                {
                    errMsgDialog("You can only buy 10 numbers at a time.");
                    return false;
                }
                if(num_to_buy.length==0)
                    errMsgDialog("Please select some numbers.");
                else{
                    promptMsg("Are you sure you want to purchase "+num_to_buy.length+" numbers?",
                    function(){
                        $("#results-box").block({
                            message: '<h1 style="color:#fff">Purchasing...</h1>',
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .7,
                                color: '#fff !important'
                            }
                        });
                        var ajax_payload = {
                            numbers:num_to_buy,
                            url:$("#new_url").val(),
                            sms_url:$("#new_sms_url").val(),
                            country:$("#country").val()
                        };
                        var response = JSONajaxCall({
                            func: "SYSTEM_TWILIO_PURCHASE_NUMBERS",
                            data: ajax_payload
                        });
                        response.done(function (msg) {
                            //console.log(msg);
                            if(msg.result == "error")
                            {
                                errMsgDialog(msg.err_msg);
                                $("#results-box").unblock();
                                return false;
                            }
                            if(msg.result == "success")
                            {
                                msgDialogCB(msg.count + " Numbers Purchased!",function(){
                                    document.location.reload();
                                });
                            }
                        });
                    });
                }
                return false;
            });

            $("#dataTable").tablesorter({
                widgets: ['zebra']
            });

            window.globalcountry = "";

            $("#reset_num_search").click(function(event){
                $("#dataTable > tbody").html("");
                $("#results-box").fadeOut(300,function(e){$("#filter-box").fadeIn(300);});
                window.onhashchange = null;
            });
            $(".text.small").keydown(function(e) {
                if(e.keyCode==13)
                    $("#search_num").trigger("click");
            });


            $("#search_tollfree").click(function(event){
                $("#filter-box").block({
                    message: '<h1 style="color:#fff">Loading</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .7,
                        color: '#fff !important'
                    }
                });
                event.preventDefault();

                var near_number = $("#near_number").val();
                var contains    = $("#contains").val();
                var country     = $("#country").val();
                window.globalcountry = $("#country").val();

                var ajaxparams = {
                    near_number: near_number,
                    contains:    contains,
                    country:     country
                };

                var response = JSONajaxCall({
                    func: "SYSTEM_TWILIO_GET_AVAIL_TOLLFREE",
                    data: ajaxparams
                });

                response.done(function (msg) {
                    //console.log(msg);
                    if(msg.result == "error")
                    {
                        errMsgDialog(msg.err_msg);
                        $("#filter-box").unblock();
                        return false;
                    }
                    if(msg.result == "none")
                    {
                        infoMsgDialog("No numbers were found. Please try again.");
                        $("#filter-box").unblock();
                    }
                    else{
                        $(msg.data).each(function(i,v){
                            var loc = "N/A";
                            if((v.RateCenter == "" && v.Region == "") ||
                                v.RateCenter == "[object Object]" && v.Region == "[object Object]")
                                loc = "N/A";
                            else if(v.RateCenter=="" || v.RateCenter=="[object Object]")
                                loc = v.Region;
                            else
                                loc = v.RateCenter + ", " + v.Region;

                            if(v.PostalCode=="[object Object]")
                                v.PostalCode = "";

                            if((v.Longitude != "" && v.Latitude != "")||
                                v.Longitude != "[object Object]" && v.Latitude != "[object Object]")
                                loc = loc + '&nbsp; <a href="#" class="map_it" data-ratecenter="'+v.RateCenter+'" data-region="'+v.Region+'"' +
                                    ' data-longitude="'+v.Longitude+'" data-latitude="'+v.Latitude+'">' +
                                    '<img title="Show Map" style="vertical-align: top;" src="images/118617_36187_16_magnify_map_icon.png"/></a>';

                            $('#dataTable > tbody:last').append($("<tr>").append(
                                "<td><input type='checkbox' class='add-to-buy' value='"+v.PhoneNumber+"' /></td>" +
                                    "<td>"+formatLocal(v.IsoCountry,v.PhoneNumber)+"</td> " +
                                    "<td>"+v.PhoneNumber+"</td> <td>"+loc+"</td> <!--<td>"+v.PostalCode+"</td>-->"+
                                    "<td><span class='voice capa"+ (v.Capabilities.Voice == 'false'? " no-capa":"") +"' title='This number"+ (v.Capabilities.Voice == 'false'? " not":"") +" is voice capable.'>VOICE</span>"+
                                    "<span class='sms capa"+ (v.Capabilities.SMS == 'false'? " no-capa":"") +"' title='This number"+ (v.Capabilities.SMS == 'false'? " not":"") +" is SMS capable.'>SMS</span>" +
                                    "<span class='mms capa"+ (v.Capabilities.MMS == 'false'? " no-capa":"") +"' title='This number"+ (v.Capabilities.MMS == 'false'? " not":"") +" is MMS capable.'>MMS</span>" +
                                    "</td>"
                            ));
                        });

                        $(".capa").tooltipster({
                            position: 'bottom',
                            theme: '.tooltipster-shadow'
                        });

                        var usersTable = $("#dataTable");
                        usersTable.trigger("update");


                        $(".map_it").click(function(event){
                            event.preventDefault();

                            var mapHTML = '<div id="map" style="width: 426px; height: 265px; border: 1px solid #000055; font-family: verdana; margin:0 auto; font-size: 8pt;">\
                                <noscript>\
                                You need javascript to view this map.\
                                </noscript>\
                                </div>';

                            var $dialog = $('<div></div>').html(mapHTML).dialog({
                                modal: true,
                                autoOpen: true,
                                minHeight: 282,
                                minWidth: 480,
                                maxWidth: 587,
                                height: 'auto',
                                title: '<span class="ui-button-icon-primary ui-icon ui-icon-image" style="float:left; margin-right:5px;"></span>Map',
                                buttons: [{
                                    text: "Close",
                                    click: function () {
                                        $(this).dialog("close");
                                        $(this).dialog('destroy').remove()
                                    }
                                }]
                            });


                            if(window.globalcountry=="GB" || window.globalcountry=="CA" ||
                                $(event.currentTarget).attr("data-longitude") == "[object Object]"||
                                $(event.currentTarget).attr("data-latitude") == "[object Object]")
                            {
                                var searchs = "";
                                if(window.globalcountry=="CA" || window.globalcountry=="US")
                                    searchs = $(event.currentTarget).attr("data-ratecenter") + "," + $(event.currentTarget).attr("data-region");
                                else
                                    searchs = $(event.currentTarget).attr("data-region");

                                var response = JSONajaxCall({
                                    func: "SYSTEM_GET_LAT_LNG",
                                    data: searchs + ", " + window.globalcountry
                                });

                                response.done(function (msg) {
                                    var myLatlng = new google.maps.LatLng(msg.lat,msg.lng);
                                    var myOptions = {
                                        zoom: 12,
                                        center: myLatlng,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };

                                    var map = new google.maps.Map(document.getElementById("map"), myOptions);

                                    var marker = new google.maps.Marker({
                                        position: myLatlng
                                    });

                                    // To add the marker to the map, call setMap();
                                    marker.setMap(map);
                                });

                            }else{
                                var longitude = $(event.currentTarget).attr("data-longitude");
                                var latitude = $(event.currentTarget).attr("data-latitude");

                                var myLatlng = new google.maps.LatLng(latitude, longitude);

                                var myOptions = {
                                    zoom: 12,
                                    center: myLatlng,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };

                                var map = new google.maps.Map(document.getElementById("map"), myOptions);

                                var marker = new google.maps.Marker({
                                    position: myLatlng
                                });

                                // To add the marker to the map, call setMap();
                                marker.setMap(map);
                            }

                            return false;
                        });
                        $("#filter-box").unblock();
                        $("#filter-box").fadeOut(300,function(e){$("#results-box").fadeIn(300);});
                        window.location.hash = "#ns";
                        window.onhashchange = function(){
                            if(window.location.hash=="")
                                $("#reset_num_search").trigger("click");

                            //console.log("HASH: "+window.location.hash);
                        };
                    }
                });
            });
            $("#search_num").click(function(event){
                $("#filter-box").block({
                    message: '<h1 style="color:#fff">Loading</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .7,
                        color: '#fff !important'
                    }
                });
                event.preventDefault();
                var postal_code = $("#postal_code").val();
                var near_number = $("#near_number").val();
                var contains    = $("#contains").val();
                var country     = $("#country").val();
                window.globalcountry = $("#country").val();

//                if(postal_code == "" && near_number == "" && contains == "")
//                {
//                    errMsgDialog("All fields cannot be blank.");
//                    $("#filter-box").unblock();
//                    return false;
//                }

                var ajaxparams = {
                    postal_code: postal_code,
                    near_number: near_number,
                    contains:    contains,
                    country:     country
                };

                var response = JSONajaxCall({
                    func: "SYSTEM_TWILIO_GET_AVAIL_NUMBERS",
                    data: ajaxparams
                });

                response.done(function (msg) {
                    //console.log(msg);
                    if(msg.result == "error")
                    {
                        errMsgDialog(msg.err_msg);
                        $("#filter-box").unblock();
                        return false;
                    }
                    if(msg.result == "none")
                    {
                        infoMsgDialog("No numbers were found. Please try again.");
                        $("#filter-box").unblock();
                    }
                    else{
                        $(msg.data).each(function(i,v){
                            var loc = "N/A";
                            if((v.RateCenter == "" && v.Region == "") ||
                                v.RateCenter == "[object Object]" && v.Region == "[object Object]")
                                loc = "N/A";
                            else if(v.RateCenter=="" || v.RateCenter=="[object Object]")
                                loc = v.Region;
                            else
                                loc = v.RateCenter + ", " + v.Region;

                            if(v.PostalCode=="[object Object]")
                                v.PostalCode = "";

                            if((v.Longitude != "" && v.Latitude != "")||
                                v.Longitude != "[object Object]" && v.Latitude != "[object Object]")
                                loc = loc + '&nbsp; <a href="#" class="map_it" data-ratecenter="'+v.RateCenter+'" data-region="'+v.Region+'"' +
                                    ' data-longitude="'+v.Longitude+'" data-latitude="'+v.Latitude+'">' +
                                    '<img title="Show Map" style="vertical-align: top;" src="images/118617_36187_16_magnify_map_icon.png"/></a>';

                            $('#dataTable > tbody:last').append($("<tr>").append(
                                "<td><input type='checkbox' class='add-to-buy' value='"+v.PhoneNumber+"' /></td>" +
                                "<td>"+formatLocal(v.IsoCountry,v.PhoneNumber)+"</td> " +
                                "<td>"+v.PhoneNumber+"</td> <td>"+loc+"</td> <!-- <td>"+v.PostalCode+"</td> -->"+
                                "<td><span class='voice capa"+ (v.Capabilities.Voice == 'false'? " no-capa":"") +"' title='This number"+ (v.Capabilities.Voice == 'false'? " not":"") +" is voice capable.'>VOICE</span>"+
                                    "<span class='sms capa"+ (v.Capabilities.SMS == 'false'? " no-capa":"") +"' title='This number"+ (v.Capabilities.SMS == 'false'? " not":"") +" is SMS capable.'>SMS</span>" +
                                    "<span style='display: none;' class='mms capa"+ (v.Capabilities.MMS == 'false'? " no-capa":"") +"' title='This number"+ (v.Capabilities.MMS == 'false'? " not":"") +" is MMS capable.'>MMS</span>" +
                                    "</td>"
                            ));
                        });

                        $(".capa").tooltipster({
                            position: 'bottom',
                            theme: '.tooltipster-shadow'
                        });

                        var usersTable = $("#dataTable");
                        usersTable.trigger("update");


                        $(".map_it").click(function(event){
                            event.preventDefault();

                            var mapHTML = '<div id="map" style="width: 426px; height: 265px; border: 1px solid #000055; font-family: verdana; margin:0 auto; font-size: 8pt;">\
                                <noscript>\
                                You need javascript to view this map.\
                                </noscript>\
                                </div>';

                            var $dialog = $('<div></div>').html(mapHTML).dialog({
                                modal: true,
                                autoOpen: true,
                                minHeight: 282,
                                minWidth: 480,
                                maxWidth: 587,
                                height: 'auto',
                                title: '<span class="ui-button-icon-primary ui-icon ui-icon-image" style="float:left; margin-right:5px;"></span>Map',
                                buttons: [{
                                    text: "Close",
                                    click: function () {
                                        $(this).dialog("close");
                                        $(this).dialog('destroy').remove()
                                    }
                                }]
                            });


                            if(window.globalcountry=="GB" || window.globalcountry=="CA" ||
                                $(event.currentTarget).attr("data-longitude") == "[object Object]"||
                                $(event.currentTarget).attr("data-latitude") == "[object Object]")
                            {
                                var searchs = "";
                                if(window.globalcountry=="CA" || window.globalcountry=="US")
                                    searchs = $(event.currentTarget).attr("data-ratecenter") + "," + $(event.currentTarget).attr("data-region");
                                else
                                    searchs = $(event.currentTarget).attr("data-region");

                                var response = JSONajaxCall({
                                    func: "SYSTEM_GET_LAT_LNG",
                                    data: searchs + ", " + window.globalcountry
                                });

                                response.done(function (msg) {
                                    var myLatlng = new google.maps.LatLng(msg.lat,msg.lng);
                                    var myOptions = {
                                        zoom: 12,
                                        center: myLatlng,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };

                                    var map = new google.maps.Map(document.getElementById("map"), myOptions);

                                    var marker = new google.maps.Marker({
                                        position: myLatlng
                                    });

                                    // To add the marker to the map, call setMap();
                                    marker.setMap(map);
                                });

                            }else{
                                var longitude = $(event.currentTarget).attr("data-longitude");
                                var latitude = $(event.currentTarget).attr("data-latitude");

                                var myLatlng = new google.maps.LatLng(latitude, longitude);

                                var myOptions = {
                                    zoom: 12,
                                    center: myLatlng,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };

                                var map = new google.maps.Map(document.getElementById("map"), myOptions);

                                var marker = new google.maps.Marker({
                                    position: myLatlng
                                });

                                // To add the marker to the map, call setMap();
                                marker.setMap(map);
                            }

                            return false;
                        });
                        $("#filter-box").unblock();
                        $("#filter-box").fadeOut(300,function(e){$("#results-box").fadeIn(300);});
                        window.location.hash = "#ns";
                        window.onhashchange = function(){
                            if(window.location.hash=="")
                                $("#reset_num_search").trigger("click");

                            //console.log("HASH: "+window.location.hash);
                        };
                    }
                });

                return false;
            });
    </script>
    <script type="text/javascript" src="js/PhoneFormat.js<?php echo "?".$build_number; ?>"></script>
</body>
</html>