<?php
session_start();

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

if(!isset($_SESSION['user_id']) || !isset($_SESSION['sel_co']))
{
    die(json_encode(array("calls"=>array(), "session_expired" => true)));
}

require_once('include/config.php');
require_once('include/util.php');
require_once('include/Services/Twilio.php');

$db = new DB();
global $RECORDINGS;

$company_id = $_SESSION['sel_co'];

$numbers = $db->getNumbersOfCompany($company_id, true);
$numbers_arr = array();

$user_access_numbers = array();
$user_access_numbers_db = $db->getUserAccessNumbers($_SESSION['user_id']);
foreach($user_access_numbers_db as $number) { $user_access_numbers[] = $number->number; }

foreach ($numbers as $number) {
    if($_SESSION['permission'] < 1 && count($user_access_numbers) > 0 && !in_array($number->number,$user_access_numbers))
        continue;
    if (substr($number->number, 0, 2) != "+1")
        $number->number = "+1".$number->number;
    $numbers_arr[] = $number->number;
}

$numbers = implode("','", $numbers_arr);
$numbers = "('".$numbers."')";

$calls = $db->getLiveCalls($numbers);

if (!$calls) {
    $calls = array();
}
else {
	foreach ($calls as $key=>$call) {
		$calls[$key]->CompanyName = $db->getCompanyName($company_id);
	}
}

$client = new Services_Twilio($AccountSid, $AuthToken);
if (!isset($_SESSION['user_queue_sid'])) {
    foreach ($client->account->queues as $queue) {
        if ($queue->friendly_name == "user_".$_SESSION['user_id']) {
            $_SESSION['user_queue_sid'] = $queue->sid;
            break;
        }
    }
}

$user_queue = array();
$call_sids = array();
try {
    $queue = $client->account->queues->get($_SESSION['user_queue_sid']);
    foreach ($queue->members as $member) {
        $call = $db->getCallDetails($member->call_sid);
        $type = "phone_number";
        $user_id = 0;
        if (empty($call)) {
            $sth = $db->customExecute("SELECT idx, full_name, username FROM users WHERE last_call_sid = ?");
            $sth->execute(array($member->call_sid));
            $user = $sth->fetch();
            $name = trim($user['full_name']);
            if (empty($name)) $name = trim($user['username']);
            else $name .= " (".$user['username'].")";
            $from = "-";
            $to = $user['idx']."_".strtotime("now");
            $type = "agent";
            $user_id = $user['idx'];
        }
        else {
            $contact = $db->getContactByPhoneNumberIfInAList($call['CallFrom']);
            $name = $call['CallFrom'];
            if ($contact) {
                if (!empty($contact['first_name']) || !empty($contact['last_name']) || !empty($contact['business_name'])) {
                    $name = trim(Util::escapeString($contact['first_name']." ".$contact['last_name']));
                    if (empty($name)) $name = trim(Util::escapeString($contact['business_name']));
                    $type = "contact";
                }
            }
            $from = $call['CallFrom'];
            $to = $call['CallTo'];
        }

        $this_queue = array(
            "user_id" => $user_id,
            "type" => $type,
            "call_sid" => $member->call_sid,
            "wait_time" => $member->wait_time,
            "position" => $member->position,
            "From" => $from,
            "To" => $to,
            "Name" => $name
        );

        if (!empty($call['Department'])) {
            $this_queue['Department'] = $call['Department'];
        }
        $user_queue[] = $this_queue;
        $call_sids[] = $member->call_sid;
    }
}
catch (Exception $e) {}

if (count($user_queue) == 0) {
    if (!isset($_SESSION['company_queue_sid'])) {
        foreach ($client->account->queues as $queue) {
            if ($queue->friendly_name == "company_".$_SESSION['sel_co']) {
                $_SESSION['company_queue_sid'] = $queue->sid;
                break;
            }
        }
    }

    try {
        $queue = $client->account->queues->get($_SESSION['company_queue_sid']);
        foreach ($queue->members as $member) {
            $call = $db->getCallDetails($member->call_sid);
            $contact = $db->getContactByPhoneNumberIfInAList($call['CallFrom']);
            $type = "phone_number";
            $name = $call['CallFrom'];
            if ($contact) {

                if (!empty($contact['first_name']) || !empty($contact['last_name']) || !empty($contact['business_name'])) {
                    $name = trim(Util::escapeString($contact['first_name']." ".$contact['last_name']));
                    if (empty($name)) $name = trim(Util::escapeString($contact['business_name']));
                    $type = "contact";
                }
            }

            $this_queue = array(
                "user_id" => 0,
                "type" => $type,
                "call_sid" => $member->call_sid,
                "wait_time" => $member->wait_time,
                "position" => $member->position,
                "From" => $call['CallFrom'],
                "To" => $call['CallTo'],
                "Name" => $name
            );

            if (!empty($call['Department'])) {
                $this_queue['Department'] = $call['Department'];
            }
            $user_queue[] = $this_queue;
            $call_sids[] = $member->call_sid;
        }
    }
    catch (Exception $e) {}
}

$user_ongoing_queue = array();
$sth = $db->customExecute("SELECT name, `from`, `to`, answered, participants FROM call_conferences WHERE participants LIKE ?");
$sth->execute(array('%"user_id":"'.$_SESSION['user_id'].'"%'));
$conferences = $sth->fetchAll();

foreach ($conferences as $conference) {
    $participants = json_decode($conference['participants'], true);

    $host_participant = array();
    $me_participant = array();

    foreach ($participants as $key => $participant) {
        if ($participant['user_id'] == $_SESSION['user_id'] && @$participant['pending'] == 1) {
            foreach ($participants as $participant2) {
                if ($participant2['is_host'] == true) {
                    $wait_time = strtotime("now") - $participant['time_added'];
                    $add_it = true;
                    foreach ($user_queue as $queue_item) {
                        if ($queue_item['Name'] == $participant2['handle']) {
                            $add_it = false;
                            break;
                        }
                    }

                    if ($add_it && @$participant['dont_show'] != 1) {
                        $name = $participant2['handle'];
                        $incoming_call = 0;
                        if ($conference['from'] != $participant2['handle']) {
                            $incoming_call = 1;
                        }
                        $contact_name = $conference['from'];
                        $contact = $db->getContactByPhoneNumberIfInAList($contact_name);
                        if ($contact) {
                            $contact_name = trim(Util::escapeString($contact['first_name']." ".$contact['last_name']));
                            if (empty($contact_name)) $contact_name = trim(Util::escapeString($contact['business_name']));
                        }

                        $user_queue[] = array(
                            "call_sid" => ($conference['answered'] == '1') ? "none" : $participant2['call_sid'],
                            "wait_time" => $wait_time,
                            "position" => 0,
                            "From" => ($name == $contact_name) ? "-" : $conference['from'],
                            "To" => $conference['to'],
                            "Name" => $participant2['handle'],
                            "incoming_call" => $incoming_call,
                            "ContactName" => $contact_name
                        );
                    }
                    break;
                }
            }
            break;
        }
        elseif ($participant['user_id'] == $_SESSION['user_id'] && @$participant['on_hold'] == 1) {
            $names = array();
            foreach ($participants as $participant3) if ($participant3['user_id'] != $_SESSION['user_id']) $names[] = $participant3['handle'];
            if (count($names) > 0) {
                $user_ongoing_queue[] = array(
                    "conference_name" => $conference['name'],
                    "From" => $conference['from'],
                    "To" => $conference['to'],
                    "Name" => implode(", ", $names),
                    "i_am_host" => $participant['is_host'] ? "1" : "0"
                );
            }
            else {
                $sth = $db->customExecute("DELETE FROM call_conferences WHERE id = ?");
                $sth->execute($conference['id']);
            }
        }
    }
}

$online_agents = array();
$addons_list = Addons::getAddonList();
$fifteen_seconds_ago = strtotime("now") - 20;
$users = array();

if ($_SESSION['permission'] <= 1) {
    $sth = $db->customExecute("SELECT idx, full_name, username, status, status_update_time FROM users u WHERE 
        (
            (
                u.idx IN (SELECT user_id FROM user_company WHERE company_id IN (SELECT company_id FROM user_company WHERE user_id = ?))
            )
            OR
            (
                u.access_lvl >= 1
            )
        )
         AND
        (
          (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND uaa.addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') > 0 OR
          u.access_lvl >= 1
        ) AND 
        u.idx != '".$_SESSION['user_id']."' AND
        u.status_update_time >= '".$fifteen_seconds_ago."' AND
        (
            u.status = 'BUSY' OR u.status = 'ONLINE' OR u.status = 'AWAY'
        )
    ");
}
else {
    $sth = $db->customExecute("SELECT idx, full_name, username, status, status_update_time FROM users u WHERE 
        (
          (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND uaa.addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') > 0 OR
          u.access_lvl >= 1
        ) AND 
        u.idx != '".$_SESSION['user_id']."' AND
        u.status_update_time >= '".$fifteen_seconds_ago."' AND
        (
            u.status = 'BUSY' OR u.status = 'ONLINE' OR u.status = 'AWAY'
        )
    ");
}
$sth->execute(array($_SESSION['user_id']));
$users = $sth->fetchAll();

foreach ($users as $user) {
    $name = $user['full_name'];
    if (empty($name)) $name = $user['username'];
    else $name .= " (".$user['username'].")";
    $online_agents[] = array("user_id" =>  $user['idx'], "name" => $name, "status" => $user['status']);
}

$db->updateUserStatusTime($_SESSION['user_id']);

if (isset($_GET['popout_dialer']))
    $_SESSION['popout_dialer_last_time'] = time();

if (!$_SESSION['popout_dialer_last_time'])
    $_SESSION['popout_dialer_last_time'] = 0;

$popout_dialer_time_diff = time() - $_SESSION['popout_dialer_last_time'];

$conference['participants'] = json_decode($conference['participants'], true);

foreach ($user_queue as $queue_element) {
    $sth = $db->customExecute("SELECT COUNT(*) as total FROM calls_missed WHERE call_sid = ? AND user_id = ?");
    $sth->execute(array($queue_element['call_sid'], $_SESSION['user_id']));
    $mc_added = $sth->fetch();
    if ($mc_added['total'] == 0) {
        if ($queue_element['type'] == "agent") {
            $queue_element['From'] = $queue_element['user_id'];
        }
        $sth = $db->customExecute("INSERT INTO calls_missed(user_id, call_sid, `from`, from_type, from_number, date_added) VALUES(?, ?, ?, ?, ?, ?)");
        $sth->execute(array($_SESSION['user_id'], $queue_element['call_sid'], $queue_element['Name'], $queue_element['type'], $queue_element['From'], strtotime("now")));
    }
}

$missed_calls = array();
if ($_SESSION['permission'] < 2) {
    $sth = $db->customExecute("SELECT * FROM calls_missed WHERE user_id = ? ORDER BY date_added DESC");
    $sth->execute(array($_SESSION['user_id']));
}
else {
    $sth = $db->customExecute("SELECT * FROM calls_missed GROUP BY call_sid ORDER BY date_added DESC");
    $sth->execute();
}
$mcs = $sth->fetchAll();

foreach ($mcs as $missed_call) {
    if (!in_array($missed_call['call_sid'], $call_sids)) {
        $missed_calls[] = array(
            'call_sid' => $missed_call['call_sid'],
            'from' => $missed_call['from'],
            'from_type' => $missed_call['from_type'],
            'from_number' => $missed_call['from_number'],
            'timestamp' => Util::timeAgo($missed_call['date_added'])
        );
    }
}

if ($_SESSION['permission'] > 1) {
    foreach ($user_queue as $key => $value) {
        if ($value['type'] == "phone_number" || $value['type'] == "contact") {
            unset($user_queue[$key]);
        }
    }
}

$return = array(
    "calls" => $calls,
    "popout_dialer_time_diff" => $popout_dialer_time_diff,
    "browsersoftphone_mute_btn_display" => (@$_SESSION['browsersoftphone_muted']) ? "mute" : "unmute",
    "my_status" => $db->getUserStatus($_SESSION['user_id']),
    "user_queue" => $user_queue,
    "user_ongoing_queue" => $user_ongoing_queue,
    "online_agents" => $online_agents,
    "missed_calls" => $missed_calls
);

if (isset($_REQUEST['conference_name'])) {
    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array(trim($_REQUEST['conference_name'])));
    $conference = $sth->fetch(PDO::FETCH_OBJ);

    if (!empty($conference->moved_to_conference)) {
        $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
        $sth->execute(array(trim($conference->moved_to_conference)));
        $conference = $sth->fetch(PDO::FETCH_OBJ);
    }
    $conference->participants = json_decode($conference->participants, true);
    if ($conference->on_hold == 0) $return['conference'] = $conference;
}

echo json_encode($return);

$dbh = null;
?>