<?php
/**
 * Author: Vinny
 * Date: 2/26/12
 * Call Sync. This will sync calls from twilio inside the database.
 */

// Settings / Includes
ignore_user_abort(true);
set_time_limit(0);
ini_set('max_execution_time', -1);
require_once('include/util.php');
session_write_close();
$db = new DB();

$last_time = $db->getVar("call_sync_last_time");
$this_time = new DateTime("now - 4day",new DateTimeZone("UTC"));

if(isset($_REQUEST['start']) && $_REQUEST['start'] != ""){
    $last_time = $_REQUEST['start'];
}

if($last_time=="")
    $last_time = "944165322";

Util::error_log("[CallSyncM] Getting Calls in range: [".date(Util::STANDARD_LOG_DATE_FORMAT,$last_time)."] to [".date(Util::STANDARD_LOG_DATE_FORMAT,strtotime("now"))."]");

// Variables
$calls = $db->getCallsInRange(-1,$last_time,strtotime("now"));
$call_sids = array();
$call_sids_out = array();
$totalcount = 0;

// Local
foreach($calls as $call)
{
    $call_sids[] = (string)$call['CallSid'];
    $call_sids_out[] = (string)$call['DialCallSid'];
}

//Twilio
global $AccountSid,$AuthToken,$ApiVersion;
$client = new Services_Twilio($AccountSid,$AuthToken);
$start = new DateTime("@".$last_time,new DateTimeZone("UTC"));

Util::error_log("[CallSyncM] ".$start->format("Y-m-d"));

echo "<pre>";

foreach ($client->account->calls->getIterator(0, 50, array(
	'StartTime>' => $start->format("Y-m-d")
)) as $call) {
	if(!in_array((string)$call->sid,$call_sids,true) && $call->direction == "inbound")
	{
		unset($_REQUEST);
		$twilio_call = $call;

		$_REQUEST['CallSid']    = $twilio_call->sid;
		$_REQUEST['AccountSid'] = $AccountSid;
		$_REQUEST['From']       = $twilio_call->from;
		$_REQUEST['To']         = $twilio_call->to;
		$_REQUEST['CallStatus'] = $twilio_call->status;
		$_REQUEST['ApiVersion'] = $ApiVersion;
		$_REQUEST['Direction']  = $twilio_call->direction;
		$_REQUEST['DateCreated']= date('Y-m-d H:i:s',strtotime($twilio_call->start_time));

		if (!empty($twilio_call->parent_call_sid)) {
			$parent_call = $client->account->calls->get($twilio_call->parent_call_sid);
			unset($_REQUEST);

			$_REQUEST['CallSid'] = $parent_call->sid;
			$_REQUEST['DialCallSid'] = $twilio_call->sid;
			$_REQUEST['DialCallTo'] = $twilio_call->to;
			$_REQUEST['DialCallDuration'] = $twilio_call->duration;
	        $_REQUEST['DialCallStatus'] = $twilio_call->status;
			$rec_uri = "";
			foreach($parent_call->recordings as $recording) {
				$rec_uri = $recording->uri;
			}
			$_REQUEST['RecordingUrl']     = "http://api.twilio.com".$rec_uri;
			//print_r($_REQUEST);
		}
		else {
			$db->save_call();
			unset($_REQUEST);

			$_REQUEST['CallSid'] = $twilio_call->sid;
			$_REQUEST['DialCallSid'] = $twilio_call->sid;
			$_REQUEST['DialCallTo'] = $twilio_call->to;
			$_REQUEST['DialCallDuration'] = $twilio_call->duration;
	        $_REQUEST['DialCallStatus'] = $twilio_call->status;
			$rec_uri = "";
			foreach($twilio_call->recordings as $recording) {
				$rec_uri = $recording->uri;
			}
			$_REQUEST['RecordingUrl']     = "http://api.twilio.com".$rec_uri;
			//print_r($_REQUEST);
		}
		$db->save_dialed_call();

		Util::error_log("[CallSyncM] Call '".$_REQUEST['CallSid']."' (".$twilio_call->date_created.")(in ) saved.");
	}
}
exit();

foreach ($client->account->calls->getIterator(0, 50, array(
    'StartTime>' => $start->format("Y-m-d")
)) as $call) {
        if(!in_array((string)$call->sid,$call_sids_out,true) && $call->direction == "outbound-dial")
        {
			unset($_REQUEST);
			$twilio_call_out = $call;
			$_REQUEST['CallSid']          = $twilio_call_out->parent_call_sid;
			$_REQUEST['DialCallSid']      = $twilio_call_out->sid;
			$_REQUEST['DialCallDuration'] = $twilio_call_out->duration;
			$_REQUEST['To']       = $twilio_call_out->to;
			$_REQUEST['From']         = $twilio_call_out->from;
			$_REQUEST['DialCallStatus']   = $twilio_call_out->status;
			$_REQUEST['DateCreated']= date('Y-m-d H:i:s',strtotime($twilio_call_out->start_time));
			$rec_uri = "";
			foreach($client->account->recordings->getIterator(0, 50, array('CallSid' => $twilio_call_out->parent_call_sid)) as $recording) {
				$rec_uri = $recording->uri;
			}
			$_REQUEST['RecordingUrl']     = "http://api.twilio.com".$rec_uri;
			$_REQUEST['callsync'] = true;
			$db->save_outgoing_call();
			//$stmt = $db->getDb()->prepare("UPDATE calls SET DialCallSid = ?, DialCallDuration = ?, DialCallTo = ?, DialCallStatus = ?, RecordingUrl = ? WHERE CallSid = ?;");
			//if($stmt->execute(array($_REQUEST['DialCallSid'],$_REQUEST['DialCallDuration'],$_REQUEST['DialCallTo'],$_REQUEST['DialCallStatus'],$_REQUEST['RecordingUrl'],$_REQUEST['CallSid'])))
				Util::error_log("[CallSyncM] Call '".$_REQUEST['CallSid']."' (".$twilio_call_out->status.")(".$twilio_call_out->date_created.")(out) saved.");
			//else
			//	Util::error_log("[CallSyncM] Failed to save call..");
        }
    $totalcount++;
}

Util::error_log("[CallSyncM] ".$totalcount);

$this_time = new DateTime("now",new DateTimeZone("UTC"));
$db->setVar("call_sync_last_time",$this_time->format("U"));

die(json_encode(array("status"=>"DONE","results"=>array("parent_calls"=>$number_of_calls,"child_calls"=>$number_of_dialed)))."\r\n");
?>