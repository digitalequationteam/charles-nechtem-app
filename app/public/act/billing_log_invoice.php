<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
require_once('include/Pagination.php');
$db = new DB();

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Pre-load Checks
if(!isset($_SESSION['user_id']) && !isset($_GET['rkey']))
{
    header("Location: login.php");
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once("include/db.invoice.php");

$invoiceId = substr($_GET["invoice_id"],8);

$invoice = new Invoice();

//get invoice info.
$invoicesArray = $invoice->getInvoices(array(
	"invoice_id" => $invoiceId
));

$invoiceArray = $invoicesArray[0];

if($invoiceArray['sess_key']!=$_GET['rkey']){
    header("Location: login.php");
    exit;
}

$invoiceAmountCurrency = $invoiceArray["amount_currency"];
$billNotPaidPastDueDays = $invoiceArray["bill_not_paid_past_due_days"];
$invoiceDetails = unserialize($invoiceArray["invoice_details"]);
$invoiceAmountPaid = $invoiceArray["amount_paid"];

$invoiceDateMysqlFormat = $invoiceArray["date_mysql_format"];

$invoiceDateArray = explode("-", $invoiceDateMysqlFormat);

$invoiceDateTS = @mktime(0, 0, 0, $invoiceDateArray[1], $invoiceDateArray[2], $invoiceDateArray[0]);

$invoiceDateFormatted = date("F j, Y", $invoiceDateTS);

$dueDateTS = strtotime("+" . $billNotPaidPastDueDays . " day", $invoiceDateTS);

$dueDateFormatted = date("F j, Y", $dueDateTS);

//client info.

//client name.
$clientName = $invoiceArray["client_name"];

$billFor = $invoiceArray["bill_for"];

//client address.
$clientAddress = $invoiceArray["address"];
$clientCity = $invoiceArray["city"];
$clientState = $invoiceArray["state"];
$clientZipCode = $invoiceArray["zip_code"];

$clientAddressHtml = "";

if (trim($clientAddress) != "") {
	
	$clientAddressHtml .= "{$clientAddress}\n";
}

$clientAddressHtml .= $clientCity;

if (trim($clientState) != "") {
	
	$clientAddressHtml .= ", {$clientState}";
}

$clientAddressHtml .= "\n";

$clientAddressHtml .= "{$clientZipCode}";

$invoiceName = "INV-" . str_replace("-", "", $invoiceArray["date_mysql_format"]) . $invoiceId;

$itemDescription = ($billFor == "lead-gen" ? "Lead Gen" : "Call Tracking Service");

//invoice amount.
$invoiceAmountCurrencySign = "\$";

switch($invoiceAmountCurrency){
    case "EUR":{
        $invoiceAmountCurrencySign = "&euro;";
        break;
    }
    case "GBP":{
        $invoiceAmountCurrencySign = "&pound;";
        break;
    }
    case "JPY":{
        $invoiceAmountCurrencySign = "&yen;";
        break;
    }
    case "BRL":{
        $invoiceAmountCurrencySign = "R\$";
        break;
    }
    case "CZK":{
        $invoiceAmountCurrencySign = "K&#269;";
        break;
    }
    case "DKK":{
        $invoiceAmountCurrencySign = "kr.";
        break;
    }
    case "HUF":{
        $invoiceAmountCurrencySign = "Ft";
        break;
    }
    case "ILS":{
        $invoiceAmountCurrencySign = "&#8362;";
        break;
    }
    case "MYR":{
        $invoiceAmountCurrencySign = "RM";
        break;
    }
    case "NOK":{
        $invoiceAmountCurrencySign = "kr";
        break;
    }
    case "PHP":{
        $invoiceAmountCurrencySign = "&#8369;";
        break;
    }
    case "PLN":{
        $invoiceAmountCurrencySign = "z&#322;";
        break;
    }
    case "SEK":{
        $invoiceAmountCurrencySign = "kr";
        break;
    }
    case "CHF":{
        $invoiceAmountCurrencySign = "Fr.";
        break;
    }
    case "TWD":{
        $invoiceAmountCurrencySign = "NT$";
        break;
    }
    case "THB":{
        $invoiceAmountCurrencySign = "&#3647;";
        break;
    }
    case "TRY":{
        $invoiceAmountCurrencySign = "TL";
        break;
    }
}


$invoiceTotalAmount = 0;
$invoiceDetailsHtml = "";

if(is_array($invoiceDetails) || is_object($invoiceDetails))
foreach ($invoiceDetails as $invoiceDetailArray) {
	
	$invoiceDetailsHtml .= "<tr>";
	
	$invoiceDetailsHtml .= "<td>" . $invoiceDetailArray["quantity"] . "</td>";
	$invoiceDetailsHtml .= "<td>" . $invoiceDetailArray["desc"] . "</td>";
	$invoiceDetailsHtml .= "<td>" . $invoiceAmountCurrencySign . $invoiceDetailArray["unitPrice"] . "</td>";
	$invoiceDetailsHtml .= "<td>" . $invoiceAmountCurrencySign . $invoiceDetailArray["totalPrice"] . "</td>";
	
	$invoiceDetailsHtml .= "</tr>";
	
	$invoiceTotalAmount += $invoiceDetailArray["totalPrice"];
}

//in case of lead gen, the amount of free leads are more than consumed leads, we don't want to show negative values.
if ($invoiceTotalAmount < 0) {
	
	$invoiceTotalAmount = 0;
}

switch($invoiceAmountCurrency){
    case "HUF":
    case "TWD":
    case "JPY":
    {
        $invoiceTotalAmount = round($invoiceTotalAmount);
        break;
    }
}

$invoiceIsPaid = false;

//if the amount paid is 
if ($invoiceArray['is_paid']==1) {
	
	$invoiceIsPaid = true;
}

//company info.
$companyLogoUrl = $db->getVar("company_logo");
$companyInfo = $db->getVar("company_info");

$paypalIntegrationOption = $db->getVar("paypal_integration_option");
$paypalIntegrationId = $db->getVar("paypal_integration_id");


function curPageURL() {
    $pageURL = 'http';
    $request_uri = substr($_SERVER['REQUEST_URI'],0,stripos($_SERVER['REQUEST_URI'],"billing_log_invoice.php"));

    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["HTTP_HOST"].":".$_SERVER["SERVER_PORT"].$request_uri;
    } else {
        $pageURL .= $_SERVER["HTTP_HOST"].$request_uri;
    }

    return $pageURL;
}
$custom = @unserialize($db->getVar('invoice_template_colors'));

if (isset($_GET['custom']))
	$custom = explode("|||", urldecode($_GET['custom']));
?>
<!DOCTYPE html>
<html>
<head>
	<title>Invoice for <?php echo($clientName); ?></title>
	<style type="text/css">
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

#invoice_header {
	height: 170px;
	display:block;
}

#wrapper {
	width: 960px;
	margin: 0 auto;
}

#invoice_container {
	padding-left: 89px;
	padding-right: 89px;
}

#company_logo {
	margin-top: 5px;
	float:left;
}

#invoice_info {
	display: inline-block;
	position: relative;
	width: 400px;
	float: right;
	margin-top: 40px;
	text-align: right;
}

.invoice_label {
	font-family: 'Open Sans', sans-serif;
	font-size: 35px;
}

.invoice_date {
	font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	color: #656666;
	display: block;
	padding-bottom: 5px;
}

.invoice_duedate {
	font-family: 'Open Sans', sans-serif;
	color: #cb3130;
	font-size: 14px;
}
.invoice_duedate > span {
	font-weight: 600;
}

/* 
 *
 * Bottom Area
 *
 */

.info-box {
	width:100%;
	height: 178px;
}
.box-left {
	width: 389px;
	border: 1px solid #eeeded;
	float: left;
}
.box-right {
	width: 389px;
	border: 1px solid #eeeded;
	float: right;
}
.box-header {
	font-family: 'Open Sans', sans-serif;
	color: #FFF;
	font-weight: 600;
	margin: 3px;
	padding: 4px;
	padding-left: 7px;
	-webkit-border-radius: 5px;
	-webkit-border-bottom-right-radius: 2px;
	-webkit-border-bottom-left-radius: 2px;
	-moz-border-radius: 5px;
	-moz-border-radius-bottomright: 2px;
	-moz-border-radius-bottomleft: 2px;
	border-radius: 5px;
	border-bottom-right-radius: 2px;
	border-bottom-left-radius: 2px;
	background: <?php echo $custom[0] != "" ? $custom[0] : "#43a7c8"; ?>;
	border: 1px solid <?php echo $custom[0] != "" ? $custom[0] : "#43a7c8"; ?>;
}
.box-company-info {
	font-family: 'Open Sans', sans-serif;
	font-style: italic;
	background: <?php echo $custom[1] != "" ? $custom[1] : "#f6f6f6"; ?>;
	color: <?php echo $custom[2] != "" ? $custom[2] : "#656666"; ?>;
	height: 138px;
	display: block !important;
	margin: 3px;
	padding: 10px;
}

h1 {
	font-family: 'Open Sans', sans-serif;
	color: #3a3e3f;
	font-weight: 600;
}

#invoice_table {
	width: 100%;
	border: 1px solid #eeeded;
}

#invoice_table > thead > tr {
	background: #FFF;
	
}

#invoice_table > thead > tr > td {
	background: <?php echo $custom[3] != "" ? $custom[3] : "#3f4445"; ?>;
	font-family: 'Open Sans', sans-serif;
	color: <?php echo $custom[4] != "" ? $custom[4] : "#ffffff"; ?>;
	font-size: 13px;
	padding: 10px;
	
}

#invoice_table > thead > tr > :first-child {
	-webkit-border-top-left-radius: 4px;
	-webkit-border-bottom-left-radius: 5px;
	-moz-border-radius-topleft: 4px;
	-moz-border-radius-bottomleft: 4px;
	border-top-left-radius: 4px;
	border-bottom-left-radius: 4px;
	text-align: center;
}
#invoice_table > thead > tr > :last-child {
	-webkit-border-top-right-radius: 4px;
	-webkit-border-bottom-right-radius: 4px;
	-moz-border-radius-topright: 4px;
	-moz-border-radius-bottomright: 4px;
	border-top-right-radius: 4px;
	border-bottom-right-radius: 4px;
}

#invoice_table > tbody > tr > td {
	font-family: 'Open Sans', sans-serif;
	color: #656666;
	font-size: 13px;
	background: #f6f6f6;
	padding: 15px;
	border: 1px solid #fff;
	margin-top: 5px;
	margin-bottom: 5px;
}

#invoice_table > tbody > tr > :first-child {
	text-align: center;
}

#invoice_table > thead > tr > :last-child {
	text-align: center;
}
#invoice_table > tbody > tr > :last-child {
	text-align: center;
}

#totals_box {
	width: 270px;
	height: 280px;
	float: right;
}
#totals_header {
	font-family: 'Open Sans', sans-serif;
	color: <?php echo $custom[6] != "" ? $custom[6] : "#ffffff"; ?>;
	font-weight: 600;
	padding: 4px;
	padding-left: 7px;
	-webkit-border-radius: 5px;
	-webkit-border-bottom-right-radius: 0px;
	-webkit-border-bottom-left-radius: 0px;
	-moz-border-radius: 5px;
	-moz-border-radius-bottomright: 0px;
	-moz-border-radius-bottomleft: 0px;
	border-radius: 5px;
	border-bottom-right-radius: 0px;
	border-bottom-left-radius: 0px;
	background: <?php echo $custom[5] != "" ? $custom[5] : "#43a7c8"; ?>;
	border: 1px solid <?php echo $custom[5] != "" ? $custom[5] : "#43a7c8"; ?>;
	height: 40px;
}
#totals_header > span {
	position: absolute;
	margin-top: 12px;
	margin-left: 120px;
}
#totals_table {
	width: 100%;
	border: 1px solid #D8D8D8;
	border-collapse: separate;
	border-spacing: 2px;
}
#totals_table > tbody > tr > td {
	font-family: 'Open Sans', sans-serif;
	font-weight: 400;
	color: #6d6c6c;
	font-size: 16px;
	padding: 15px;
	background: #f6f6f6;
}
#totals_table > tbody > tr > :first-child {
	font-weight: 600;
}
#totals_table > tbody > tr > :last-child {
	text-align: right;
}
.total_label {
	font-size: 17px !important;
	color: #4d4c4c !important;
}
.total_price {
	font-size: 19px !important;
}

.paypal-button {
    white-space: nowrap;
}
.paypal-button a {
    background: none repeat scroll 0 0 #FFA823;
    border: 1px solid #FFA823;
    border-radius: 13px 13px 13px 13px;
    color: #0E3168;
    cursor: pointer;
    font-family: "Arial",bold,italic;
    font-style: italic;
    font-weight: bold;
    overflow: hidden;
    position: relative;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    white-space: nowrap;
    z-index: 0;
    text-decoration: none;
}
.paypal-button a:before {
    background: -moz-linear-gradient(center top , #FFAA00 0%, #FFAA00 80%, #FFF8FC 100%) repeat scroll 0 0 transparent;
    border-radius: 11px 11px 11px 11px;
    content: " ";
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: -2;
}
.paypal-button a:after {
    background: -moz-linear-gradient(center top , #FEFEFE 0%, #FED994 100%) repeat scroll 0 0 transparent;
    border-radius: 40px 40px 38px 38px;
    content: " ";
    height: 60%;
    left: 0;
    position: absolute;
    top: 0;
    transform: translateX(1%);
    width: 98%;
    z-index: -1;
}
.paypal-button a.small {
    font-size: 12px;
    padding: 3px 15px;
}
.paypal-button a.large {
    font-size: 14px;
    padding: 4px 19px;
}

.paypal-button-div {
	text-align: center;
}
	</style>

</head>
<body>
	<div id="wrapper">
		<div id="invoice_container">
			<div id="invoice_header">
				<img id="company_logo" src="<?php echo($companyLogoUrl); ?>" />
				<div id="invoice_info">
					<span class="invoice_label">Invoice <span class="invoice_number"><?php echo($invoiceName); ?></span></span><br><br>
					<span class="invoice_date"><?php echo($invoiceDateFormatted); ?></span>
					<span class="invoice_duedate">Payment due by <span><?php echo($dueDateFormatted); ?></span></span>
				</div>
			</div>

			<div id="invoice_body">
				<div class="info-box">
					<div class="box-left" style="width: 258px;">
						<div class="box-header">FROM</div>
						<div class="box-company-info">
							<pre><?php echo($companyInfo); ?></pre>
						</div>
					</div>
					<div class="box-left" style="width: 258px;">
						<div class="box-header">TO</div>
						<div class="box-company-info">
							<pre><?php echo($clientAddressHtml); ?></pre>
						</div>
					</div>
					<div class="box-right" style="width: 260px;">
						<div class="box-header">STATS</div>
						<div class="box-company-info">
							<?php
							$calls     = count($db->getCallsInRange($invoiceArray['company_id'],strtotime($invoiceArray['from_date_sql']),strtotime($invoiceArray['to_date_sql'])));
							$calls_at  = count($db->getCallsInRange($invoiceArray['company_id'],strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400)));
							$unique    = count($db->getCallsInRange($invoiceArray['company_id'],strtotime($invoiceArray['from_date_sql']),strtotime($invoiceArray['to_date_sql']), 0, 'all', true));
							$unique_at = count($db->getCallsInRange($invoiceArray['company_id'],strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400), 0, 'all', true));;
							?>
							Total Calls: <strong><?php echo $calls; ?></strong><br />
							Total Calls All Time: <strong><?php echo $calls_at; ?></strong><br />
							Unique Calls: <strong><?php echo $unique; ?> (<?php echo Util::percent($unique,$calls); ?>%)</strong><br />
							Unique Calls All Time: <strong><?php echo $unique_at; ?> (<?php echo Util::percent($unique_at,$calls_at); ?>%)</strong><br />
						</div>
					</div>
				</div>
				<br><br><br>
				<h1>ITEMS</h1><br>
				<table id="invoice_table">
					<thead>
						<tr>
							<td>Quantity</td>
							<td width="460">Description</td>
							<td>Unit Price</td>
							<td>TOTAL</td>
						</tr>
					</thead>
					<tbody><?php echo($invoiceDetailsHtml); ?></tbody>
				</table>
				<br><br>
				<div id="totals_box">
					<div id="totals_header"><span>AMOUNT DUE</span></div>
					<table id="totals_table" cellspacing="1">
						<tbody>
							<tr>
								<td>SUBTOTAL</td>
								<td>
									<?php echo($invoiceAmountCurrencySign . " " . $invoiceTotalAmount); ?>
								</td>
							</tr>
							<!-- <tr>
								<td>TAX</td>
								<td>
									<?php // echo($invoiceAmountCurrencySign); ?>0.00
								</td>
							</tr> -->
							<tr>
								<td class="total_label">TOTAL</td>
								<td class="total_price">
									<?php echo($invoiceAmountCurrencySign . " " . $invoiceTotalAmount); ?>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<?php if ($invoiceIsPaid) { ?>
                                        <?php if($invoiceArray['is_offline']!=1) { ?>
										<div style="text-align: center;"><img width="16" style="vertical-align: text-top;margin-right: 2px;" src="images/check48.png"/>  <span style="color:#1D7500;">Invoice Paid</span></div>
									    <?php }else{ ?>
                                        <div style="text-align: center;"><img width="16" style="vertical-align: text-top;margin-right: 2px;" src="images/check48.png"/>  <span style="color:#1D7500;">Invoice Paid Offline</span></div>
                                        <?php } ?>
									<?php } else if (trim($paypalIntegrationId) != "") {
                                            if($invoiceArray['is_declined']==1){
                                         ?>
                                                <div style="text-align: center;"><img width="16" style="vertical-align: text-top;margin-right: 2px;" src="images/exclaimation_ico.png"/> <span style="color:#950c12;">Declined</span></div><br>
                                        <?php }elseif($invoiceArray['is_pending']!=1){ ?>
                                        <?php if(strtotime('now') > $dueDateTS) { ?>
                                        <div style="text-align: center;"><img width="16" style="vertical-align: text-top;margin-right: 2px;" src="images/exclaimation_ico.png"/> <span style="color:#9a0000;">Payment Overdue</span></div><br>
			                            <?php } ?>

										<div class="paypal-button-div paypal-button">
											<a href="<?php echo curPageURL(); ?>paypal/paypal_email_invoice_proxy.php?invoice_id=<?php echo($invoiceId); ?>" class="paypal-button large submit long">Pay with Paypal</a>
										</div>
                                        <?php }else{ ?>
                                        <div style="text-align: center;"><img width="16" style="vertical-align: text-top;margin-right: 2px;" src="images/exclaimation_ico.png"/> <span style="color:#FFAA00;">Payment Pending</span></div><br>
                                        <?php }} ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>