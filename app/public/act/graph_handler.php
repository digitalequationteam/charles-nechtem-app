<?php
session_start();
require_once('include/util.php');
require_once('include/date_functions.php');
ignore_user_abort();
// Adding script execution timeout. To prevent deadlock.
// 300 seconds (5 minutes)
set_time_limit(300);

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

global $AccountSid, $AuthToken, $ApiVersion;
$client = new Services_Twilio($AccountSid,$AuthToken);

// Function Table
$valid_functions = array(
    'CALLS_THIS_WEEK'             => true,
    'CALLS_THIS_YEAR'             => true,
    'CALLS_DATE_RANGE_ALL_NUMBERS'=> true,
    'CALLS_DATE_RANGE'            => true,
    'CALLS_ANSWERED_PIE'          => true,
    "CAMPAIGN_PHONE_CODES_PIE"    => true,
    'CALLS_BY_HOUR'               => true,
    'CALLS_BY_WEEK'               => true,
    'CALLS_TODAY'                 => true,
    'CALLS_YESTERDAY'             => true,
    'CALLS_14_DAYS'               => true,
    'CALLS_30_DAYS'               => true,
    'CALLS_60_DAYS'               => true,
    'CALLS_90_DAYS'               => true,
    'CALLS_THIS_MONTH'            => true,
    'CALLS_LAST_MONTH'            => true,
    'CALLS_LAST_YEAR'             => true,
    'CALLS_ALL_TIME'              => true,
    'CAMPAIGN_DAILY_TRENDS'       => true,
    'CALLS_GEN_CALLS_PIE'         => true,
    'CALLS_All_HITS_PIE'          => true,
    'CALLS_NONGEN_CALL'           => true,
    'CALLS_HITS_NON_GEN'          => true,
    'EMAIL_TRACKING_CLIENTS'      => true
);

if(in_array($_POST['func'],$valid_functions) && isset($_POST['func']))
{
    $params = $_POST;
    unset($params['func']);
    if($valid_functions[$_POST['func']])
        $_POST['func']($params);
    else
        die(json_encode(array("result"=>"error","msg"=>"Function does not exist.")));
        //die("FUNCTION_NOT_EXIST:::".$_POST['func']);
}else{
    die(json_encode(array("result"=>"error","msg"=>"Missing function call.")));
}


function CALLS_ALL_TIME($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);

    $first_call = $db->cget_all_calls($company_id,$outgoing);

    if (!is_array($first_call[0])) {
        $date_to_use = "";
    }
    else {
        $first_call = array_pop($first_call[0]);
        $date_to_use = $first_call['DateCreated'];
    }

    $date_start = new DateTime($date_to_use);
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = new DateTime("",$tz);
    $now->setTimezone(new DateTimeZone("UTC"));

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $diff = DateUtil::diff($date_start,$now);

    $total_months = 0;

    for($i=0; $i < $diff->y; $i++)
        $total_months += 12;

    $total_months += $diff->m;

    for($i=0; $i < $total_months+2; $i++)
    {
        $_i = 0;
        if( is_array($company_numbers) || is_object($company_numbers) )
            foreach($company_numbers as $c_num)
            {
                $date_array[$_time->format("M 'y")][$_i] = 0;
                $_i++;
            }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("M 'y")][$_i] = 0;
            }
        }

        $_time->modify("+1 month");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("M 'y")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("M 'y")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}
function CALLS_LAST_YEAR($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    $date_start = new DateTime("@".strtotime('-1 year',strtotime(date('01/01/y'))));
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = clone $date_start;
    $now->modify("+12 months");

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    for($i=0; $i < 12; $i++)
    {
        $_i = 0;
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("M 'y")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("M 'y")][$_i] = 0;
            }
        }

        $_time->modify("+1 month");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("M 'y")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("M 'y")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array,"test"=>$date_start->format(DateTime::RFC2822))));
}
function CALLS_LAST_MONTH($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    date_default_timezone_set("UTC");

    $date_start = new DateTime("@".strtotime('-1 month',strtotime(date('m/01/y'))));
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = clone $date_start;
    $now->modify("+1 month");
    $now->modify("-1 day");

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $_time->setTimezone($tz);

    $datediff = $now->format("U") - $_time->format("U");

    //print_r($company_numbers);
    for($i=0; $i < floor($datediff/(60*60*24)); $i++)
    {
        $_i = 0;
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("n/j")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("n/j")][$_i] = 0;
            }
        }

        $_time->modify("+1 day");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("n/j")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("n/j")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array,"now"=>$now->format(DateTime::RFC2822))));
}
function CALLS_THIS_MONTH($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    $date_start = new DateTime("@".strtotime('this month',strtotime(date('m/01/y'))));
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = new DateTime("",$tz);
    $date_start->setTimezone(new DateTimeZone("UTC"));

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $_time->setTimezone($tz);
    for($i=0; $i < $now->format("j")+1; $i++)
    {
        $_i = 0;
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("n/j")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("n/j")][$_i] = 0;
            }
        }

        $_time->modify("+1 day");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("n/j")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("n/j")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}
function CALLS_90_DAYS($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    $date_start = new DateTime("-90 days");
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = clone $date_start;
    $now->modify("+90 days");

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $_time->setTimezone($tz);
    for($i=0; $i < 90; $i++)
    {
        $_i = 0;
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("n/j")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("n/j")][$_i] = 0;
            }
        }
        $_time->modify("+1 day");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("n/j")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("n/j")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}
function CALLS_60_DAYS($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    $date_start = new DateTime("-60 days");
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = clone $date_start;
    $now->modify("+60 days");

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $_time->setTimezone($tz);
    for($i=0; $i < 60; $i++)
    {
        $_i = 0;
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("n/j")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("n/j")][$_i] = 0;
            }
        }

        $_time->modify("+1 day");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("n/j")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("n/j")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}
function CALLS_30_DAYS($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    $date_start = new DateTime("-30 days");
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = clone $date_start;
    $now->modify("+30 days");

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $_time->setTimezone($tz);
    for($i=0; $i < 30; $i++)
    {
        $_i = 0;
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("n/j")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("n/j")][$_i] = 0;
            }
        }
        $_time->modify("+1 day");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("n/j")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("n/j")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}
function CALLS_14_DAYS($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');

    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    $date_start = new DateTime("-14 days");
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = clone $date_start;
    $now->modify("+14 days");

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $_time->setTimezone($tz);
    for($i=0; $i < 14; $i++)
    {
        $_i = 0;
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("l (n/j)")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers)) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("l (n/j)")][$_i] = 0;
            }
        }
        $_time->modify("+1 day");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("l (n/j)")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("l (n/j)")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}
function CALLS_YESTERDAY($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    $date_start = new DateTime("-1 day");
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = clone $date_start;
    $now->modify("+23 hours");

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $_time->setTimezone($tz);
    for($i=0; $i < 24; $i++)
    {
        $_i = 0;
        $_time->setTime($i,0,0);
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("g:00 A")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers)) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("g:00 A")][$_i] = 0;
            }
        }
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("g:00 A")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("g:00 A")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}

function CALLS_TODAY($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    $date_start = new DateTime();
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = new DateTime("now",$tz);

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $_time->setTimezone($tz);
    if($now->format('I')==1)
        $_dst = $now->format("G")+1;
    else
        $_dst = $now->format("G");
    for($i=0; $i < $_dst; $i++)
    {
        $_i = 0;
        $_time->setTime($i,0,0);
        foreach($company_numbers as $c_num)
        {
            $date_array[$_time->format("g:00 A")][$_i] = 0;
            $_i++;
        }

        if( is_array($company_pool_numbers) || is_object($company_pool_numbers)) {
            if (sizeof($company_pool_numbers) > 0) {
                $date_array[$_time->format("g:00 A")][$_i] = 0;
            }
        }
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);

            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("g:00 A")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers)) {
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                if($c_num[1])
                    $c_num[0] = "+".$c_num[0];
                else
                    $c_num[0] = "+1".$c_num[0];

                $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

                foreach($calls as $call)
                {
                    $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                    $call_date->setTimezone($tz);
                    $date_array[$call_date->format("g:00 A")][$num_inc]++;
                }
            }
            $columns[] = "Pooled Calls";
            $company_numbers[] = "Pooled Calls";
        }
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    $i=0;
    foreach($main_array as $line){
        $count = count($line)-1;

        if($count!=count($company_numbers))
        {
            do{
                $main_array[$i][] = 0;
            }while(count($main_array[$i])-1!=count($company_numbers));
        }
        $i++;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}
function CALLS_THIS_WEEK($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $_SESSION['GRAPH_TYPE_INDEX'] = 'line';
    $db = new DB();
    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Day');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            $columns[] = "Pooled Calls";
        }
    }

    $main_arr = array($columns);

    $index = 0;
    for($i = 7; $i >= 0; $i--)
    {
        $arr = array();
        if($i!=0){
            $date = new DateTime($i." days ago",$tz);
            $arr[] = $date->format("l \(n-d\)");
        }
        else{
            $date = new DateTime("0 days ago",$tz);
            $arr[] = $date->format("l");
        }

        $main_arr[] = $arr;
        $index++;
    }

    $index2 = 1;

    $admin = $db->isUserAdmin($_SESSION['user_id']);

    if(!$admin)
    {
        $disabled_numbers = $db->getUserAccessNumbers($_SESSION['user_id']);
    }

    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $disabled = false;
            if(!$admin)
            {
                foreach($disabled_numbers as $disabled_number)
                {
                    if($disabled_number==$c_num)
                        $disabled = true;
                }
            }
            if($disabled)
                continue;


            $last_7_days = $db->getCallsLastSeven($c_num[0],$c_num[1],$outgoing);

            $main_arr[1][$index2] = $last_7_days[7];
            $main_arr[2][$index2] = $last_7_days[6];
            $main_arr[3][$index2] = $last_7_days[5];
            $main_arr[4][$index2] = $last_7_days[4];
            $main_arr[5][$index2] = $last_7_days[3];
            $main_arr[6][$index2] = $last_7_days[2];
            $main_arr[7][$index2] = $last_7_days[1];
            $main_arr[8][$index2] = $last_7_days[0];

            $index2 = $index2+1;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) )
        if (sizeof($company_pool_numbers) > 0) {
            foreach($company_pool_numbers as $c_num)
            {
                $disabled = false;
                if(!$admin)
                {
                    foreach($disabled_numbers as $disabled_number)
                    {
                        if($disabled_number==$c_num)
                            $disabled = true;
                    }
                }
                if($disabled)
                    continue;


                $last_7_days = $db->getCallsLastSeven($c_num[0],$c_num[1],$outgoing);

                $main_arr[1][$index2] = (isset($main_arr[1][$index2])) ? $main_arr[1][$index2] + $last_7_days[7] : $last_7_days[7];
                $main_arr[2][$index2] = (isset($main_arr[2][$index2])) ? $main_arr[2][$index2] +  $last_7_days[6] : $last_7_days[6];
                $main_arr[3][$index2] = (isset($main_arr[3][$index2])) ? $main_arr[3][$index2] +  $last_7_days[5] : $last_7_days[5];
                $main_arr[4][$index2] = (isset($main_arr[4][$index2])) ? $main_arr[4][$index2] +  $last_7_days[4] : $last_7_days[4];
                $main_arr[5][$index2] = (isset($main_arr[5][$index2])) ? $main_arr[5][$index2] +  $last_7_days[3] : $last_7_days[3];
                $main_arr[6][$index2] = (isset($main_arr[6][$index2])) ? $main_arr[6][$index2] +  $last_7_days[2] : $last_7_days[2];
                $main_arr[7][$index2] = (isset($main_arr[7][$index2])) ? $main_arr[7][$index2] +  $last_7_days[1] : $last_7_days[1];
                $main_arr[8][$index2] = (isset($main_arr[8][$index2])) ? $main_arr[8][$index2] +  $last_7_days[0] : $last_7_days[0];
            }
        }

    //die(print_r($main_arr,true));

    die(json_encode(array("result"=>"success","data"=>$main_arr)));
}
function CALLS_THIS_YEAR($params='')
{
    $_SESSION['GRAPH_TYPE_INDEX'] = 'bar';
    $db = new DB();
    $company_id = $params['data']['company_id'];
    $outgoing = ($params['data']['outgoing'] === 'true');
    $columns = array('Month');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);


    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        if (sizeof($company_pool_numbers) > 0) {
            $columns[] = "Pooled Calls";
        }
    }

    $main_arr = array($columns);

    for($i = 1; $i <= 12; $i++)
    {
        $main_arr[] = array(getMonthString($i));
    }

    $index2 = 1;

    $admin = $db->isUserAdmin($_SESSION['user_id']);

    if(!$admin)
    {
        $disabled_numbers = $db->getUserAccessNumbers($_SESSION['user_id']);
    }

    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $disabled = false;
            if(!$admin)
            {
                foreach($disabled_numbers as $disabled_number)
                {
                    if($disabled_number==$c_num)
                        $disabled = true;
                }
            }
            if($disabled)
                continue;

            $this_year = $db->getCallsThisYear($c_num[0],$c_num[1],$outgoing);
            //die(print_r($this_year));

            $index3 = 1;
            foreach($this_year as $ent)
            {
                $main_arr[$index3][$index2] = $ent[1];
                $index3++;
            }
            $index2++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) )
        foreach($company_pool_numbers as $c_num)
        {
            $disabled = false;
            if(!$admin)
            {
                foreach($disabled_numbers as $disabled_number)
                {
                    if($disabled_number==$c_num)
                        $disabled = true;
                }
            }
            if($disabled)
                continue;

            $this_year = $db->getCallsThisYear($c_num[0],$c_num[1],$outgoing);
            //die(print_r($this_year));

            $index3 = 1;
            foreach($this_year as $ent)
            {
                $main_arr[$index3][$index2] = (isset($main_arr[$index3][$index2])) ? $main_arr[$index3][$index2] + $ent[1] : $ent[1];
                $index3++;
            }
        }

    die(json_encode(array("result"=>"success","data"=>$main_arr)));
}
function CALLS_DATE_RANGE_ALL_NUMBERS_NEW($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    $company_id  = $params['data']['company_id'];
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];
    $phone_code  = $params['data']['phone_code'];
    $call_result = $params['data']['call_result'];
    $report_type = $params['data']['report_type'];
    $outgoing_num = $params['data']['outgoing_number'];
    $outgoing = ($params['data']['outgoing'] == "true") ? true : false;

    if($outgoing==="false" || $outgoing == "")
        $outgoing = false;
    else
        $outgoing = true;

    $columns = array('Hours');
    $company_numbers = $db->getCompanyNumIntl($company_id);
    $company_pool_numbers = $db->getCompanyNumIntlPool($company_id);

    $first_call = $db->cget_all_calls($company_id);
    $first_call = array_pop($first_call[0]);

    $date_start = new DateTime($first_call['DateCreated']);
    $date_start->setTimezone($tz);
    $date_start->setTime(0,0,0);
    $date_start->setTimezone(new DateTimeZone("UTC"));
    $now = new DateTime("",$tz);
    $now->setTimezone(new DateTimeZone("UTC"));

    $date_array = array();

    $_time = new DateTime("@".$date_start->format("U"));
    $diff = DateUtil::diff($date_start,$now);

    $total_months = 0;

    for($i=0; $i < $diff->y; $i++)
        $total_months += 12;

    $total_months += $diff->m;

    for($i=0; $i < $total_months+2; $i++)
    {
        $_i = 0;
        if( is_array($company_numbers) || is_object($company_numbers) )
            foreach($company_numbers as $c_num)
            {
                $date_array[$_time->format("M 'y")][$_i] = 0;
                $_i++;
            }
        $_time->modify("+1 month");
    }

    $num_inc = 0;
    if( is_array($company_numbers) || is_object($company_numbers) )
        foreach($company_numbers as $c_num)
        {
            $columns[] = format_phone($c_num[0]);
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];
            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);
            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("M 'y")][$num_inc]++;
            }
            $num_inc++;
        }

    if( is_array($company_pool_numbers) || is_object($company_pool_numbers) ) {
        foreach($company_pool_numbers as $c_num)
        {
            if($c_num[1])
                $c_num[0] = "+".$c_num[0];
            else
                $c_num[0] = "+1".$c_num[0];

            $calls = $db->getCallsInRangeForNum($c_num[0],$date_start->format("U"),$now->format("U"),$outgoing);

            foreach($calls as $call)
            {
                $call_date = new DateTime($call['DateCreated'],new DateTimeZone("UTC"));
                $call_date->setTimezone($tz);
                $date_array[$call_date->format("M 'y")][$num_inc]++;
            }
            $num_inc++;
        }
        $columns[] = "Pooled Calls";
        $company_numbers[] = "Pooled Calls";
    }

    $main_array[] = $columns;

    foreach($date_array as $key => $value)
    {
        $_arr = array($key);
        foreach($value as $subval)
        {
            $_arr[] = $subval;
        }
        $main_array[] = $_arr;
    }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}
function CALLS_DATE_RANGE_ALL_NUMBERS($params='')
{
    global $TIMEZONE;
    $db = new DB();
    $company_id  = $params['data']['company_id'];
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];

    $tz = new DateTimeZone($TIMEZONE);
    $start_date = new DateTime(date("Y-m-d H:i:s", $start_date));
    $year = $start_date->format("Y");
    $month = $start_date->format("n");
    $day = $start_date->format("j");
    $d1 = strtotime($start_date->format("Y-m-d H:i:s"));
    $start_date->setTimezone($tz);
    $d2 = strtotime($start_date->format("Y-m-d H:i:s"));
    $seconds_difference = abs($d2 - $d1);

    $end_date = new DateTime(date("Y-m-d H:i:s", $end_date));
    $year = $end_date->format("Y");
    $month = $end_date->format("n");
    $day = $end_date->format("j");
    $end_date->setTimezone($tz);

    $start_date = strtotime($start_date->format("Y-m-d H:i:s")) + $seconds_difference;
    $end_date = strtotime($end_date->format("Y-m-d H:i:s")) + $seconds_difference;

    $phone_code  = $params['data']['phone_code'];
    $call_result = $params['data']['call_result'];
    $report_type = $params['data']['report_type'];
    $outgoing_num = $params['data']['outgoing_number'];
    $outgoing = ($params['data']['outgoing'] == "true") ? true : false;
    $user = $params['data']['user'];
    $campaign = $params['data']['campaign'];

    $agent = $params['data']['agent'];

    if($user==0)
        $user = "";

    if($outgoing==="false" || $outgoing == "")
        $outgoing = false;
    else
        $outgoing = true;

    //$months = array();

    $data = new GDataView();

    $data->addCol("date","Date");
    if($report_type=="ans_unans")
    {
        $data->addCol("number","Answered");
        $data->addCol("number", "Unanswered");
        $data->addCol("number", "Busy");
        $data->addCol("number","Error");
    }else{
        $data->addCol("number","Calls");
    }

    $currDate  = $start_date;

    $term = "";
    switch($report_type)
    {
        default:
            $term = "+24 hours";
            break;
    }

    for($j=1; $j <= ceil(($end_date - $start_date)/86400); $j++)
    {
        //$time = microtime(TRUE);
        //$mem = memory_get_usage();

        if($report_type=="default")
        {
            $calls = $db->getCallsInRangeCount($company_id,$currDate,strtotime($term,$currDate),$phone_code,$call_result,false,$outgoing_num,$outgoing,$user,$campaign,$agent);
            $date = new DateTime("@".$currDate, new DateTimeZone('UTC'));
            $date->setTimezone($tz);
            $data->addRow(array("c"=>array(
                array("v"=>"Date(".$date->format("Y").", ".($date->format("n")-1).", ".$date->format("j").", ".$date->format("G").")"),
                array("v"=>intval($calls))
            )));
        }
        if($report_type=="ans_unans")
        {
            $date = strtotime($term,$currDate);
            $ans    = 0;
            $unans  = 0;
            $busy   = 0;
            $failed = 0;
            if($call_result == "all"){
                $ans = $db->getCallsInRangeCount($company_id,$currDate,$date,$phone_code,"answered",false,$outgoing_num,$outgoing,$user,$campaign,$agent);
                $unans = $db->getCallsInRangeCount($company_id,$currDate,$date,$phone_code,"missed",false,$outgoing_num,$outgoing,$user,$campaign,$agent);
                $busy = $db->getCallsInRangeCount($company_id,$currDate,$date,$phone_code,"busy",false,$outgoing_num,$outgoing,$user,$campaign,$agent);
                $total = $db->getCallsInRangeCount($company_id,$currDate,$date,$phone_code,"",false,$outgoing_num,$outgoing,$user,$campaign,$agent);
                $failed = $total - $ans - $busy - $unans;
            }
            elseif($call_result!="answered" && $call_result != "missed"){
                $ans = $db->getCallsInRangeCount($company_id,$currDate,$date,$phone_code,$call_result,false,$outgoing_num,$outgoing,$user,$campaign,$agent);
                $unans =0;
                $busy = 0;
                $failed = 0;
            }else{
                $calls = $db->getCallsInRangeCount($company_id,$currDate,$date,$phone_code,$call_result,false,$outgoing_num,$outgoing,"",$campaign,$agent);
                if($call_result=="answered")
                    $ans = $calls;
                if($call_result=="missed")
                    $unans = $calls;
            }

            $date = new DateTime("@".$currDate, new DateTimeZone('UTC'));
            $date->setTimezone($tz);
            $data->addRow(array("c"=>array(
                array("v"=>"Date(".$date->format("Y").", ".($date->format("n")-1).", ".$date->format("j").", ".$date->format("G").")",
                      "f"=>$date->format("D, M, j Y")),
                array("v"=>intval($ans)),
                array("v"=>intval($unans)),
                array("v"=>intval($busy)),
                array("v"=>intval($failed))
            )));
        }

        $currDate = strtotime($term, $currDate);

        //echo("Pass $j: Execution took ".(microtime(TRUE) - $time)." seconds and used ".(memory_get_usage() - $mem) / (1024 * 1024)." mem.\r\n");

    }


    die(json_encode(array("result"=>"success","data"=>$data->getData())));
}
function CALLS_ANSWERED_PIE($params='')
{
    $db = new DB();
    $company_id  = $params['data']['company_id'];
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];
    $phone_code  = $params['data']['phone_code'];
    $call_result = $params['data']['call_result'];
    $outgoing_num = $params['data']['outgoing_number'];
    $outgoing    = $params['data']['outgoing'];
    $user = $params['data']['user'];
    $campaign = $params['data']['campaign'];
    $agent = $params['data']['agent'];

    if($user==0)
        $user = "";

    if($outgoing==="false" || $outgoing == "")
        $outgoing = false;
    else
        $outgoing = true;

    $answered   = 0;
    $unanswered = 0;
    $busy       = 0;
    $error      = 0;

    $data = new GDataView();
    $data->addCol("string","callstatus");
    $data->addcol("number","Calls");

    $calls = $db->getCallsInRange($company_id,$start_date,$end_date,$phone_code,$call_result,false,$outgoing_num,$outgoing,$user,$campaign,$agent);

    if( is_array($calls) || is_object($calls) )
        foreach($calls as $call)
        {
            //echo $call['DialCallStatus'];
            if($call['DialCallStatus']=="no-answer")
                $unanswered = $unanswered+1;
            elseif($call['DialCallStatus']=="busy")
                $busy = $busy+1;
            elseif($call['DialCallStatus']=="completed")
                $answered = $answered +1;
            else
                $error = $error+1;
        }

    $data->addRow(array("c"=>array(
        array("f"=>"Answered"),array("v"=>$answered)
    )));

    $data->addRow(array("c"=>array(
        array("f"=>"Unanswered"),array("v"=>$unanswered)
    )));

    $data->addRow(array("c"=>array(
        array("f"=>"Busy"),array("v"=>$busy)
    )));

    $data->addRow(array("c"=>array(
        array("f"=>"Error"),array("v"=>$error)
    )));

    die(json_encode(array("result"=>"success","data"=>$data->getData())));

}

function CAMPAIGN_PHONE_CODES_PIE($params='')
{
    global $client;
    $db = new DB();
    $company_id  = $params['data']['company_id'];
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];
    $phone_code  = $params['data']['phone_code'];
    $call_result = $params['data']['call_result'];
    $outgoing_num = $params['data']['outgoing_number'];
    $campaign = Util::escapeString($params['data']['campaign']);
    $outgoing = ($params['data']['outgoing'] == "true") ? true : false;

    $answered   = 0;
    $unanswered = 0;
    $busy       = 0;
    $error      = 0;

    $data = new GDataView();
    $data->addCol("string","callstatus");

    $numbers = $db->getNumbersOfCompany($company_id, true);

    $campaign_data = array("phone_codes" => array(), "numbers" => array());
    foreach ($client->account->incoming_phone_numbers as $number) {
        $number->friendly_name = Util::escapeString($number->friendly_name);
        if ($number->friendly_name == $campaign) {
            $this_phone_number = $db->format_phone_db($number->phone_number);
            foreach ($numbers as $number2) {
                if ($number2->number == $this_phone_number) {
                    $campaign_data["numbers"][] = $this_phone_number;
                    break;
                }
            }
        }
    }

    $data->addcol("number", "Calls");

    $calls = $db->getCallsInRange($company_id,$start_date,$end_date,$phone_code,$call_result,false,$outgoing_num, $outgoing);

    $phone_codes_set = array();
    if( is_array($calls) || is_object($calls) )
        foreach($calls as $call)
        {
            if ($outgoing)
                $callToFrom = $db->format_phone_db($call['CallFrom']);
            else
                $callToFrom = $db->format_phone_db($call['CallTo']);

            if (in_array($callToFrom, $campaign_data["numbers"])) {
                if ($call["PhoneCode"] != 0) {
                    $type = ($outgoing) ? 2 : 1;
                    $phone_code = $db->getPhoneCodeName($call["PhoneCode"], $type);

                    if (!isset($phone_codes_set[$phone_code])) {
                        $data->addcol("number", $phone_code);
                        $phone_codes_set[$phone_code] = 1;
                    }
                    else {
                        $phone_codes_set[$phone_code]++;
                    }
                }
            }
        }

    foreach ($phone_codes_set as $name=>$count) {
        if ($count > 0) {
            $data->addRow(array("c"=>array(
                array("f"=>$name),array("v"=>$count)
            )));
        }
    }

    die(json_encode(array("result"=>"success","data"=>$data->getData())));

}

function CALLS_BY_HOUR($params='')
{
    global $TIMEZONE;
    $db = new DB();
    $company_id  = $params['data']['company_id'];
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];
    $phone_code  = $params['data']['phone_code'];
    $call_result = $params['data']['call_result'];
    $day         = $params['data']['day'];
    $outgoing_num = $params['data']['outgoing_number'];
    $outgoing    = $params['data']['outgoing'];
    $user        = $params['data']['user'];
    $campaign        = $params['data']['campaign'];
    $agent        = $params['data']['agent'];
    $tz          = new DateTimeZone($TIMEZONE);

    if($user==0)
        $user = "";

    if($outgoing==="false" || $outgoing == "")
        $outgoing = false;
    else
        $outgoing = true;

    $hours = array();
    $data = new GDataView();
    $data->addCol("string","Hours");
    $data->addCol("number","Answered");
    $data->addCol("number","Unanswered");
    $data->addCol("number","Busy");
    $err_added = false;

    $calls = $db->getCallsInRange($company_id,$start_date,$end_date,$phone_code,$call_result,false,$outgoing_num,$outgoing,$user,$campaign,$agent);

    for($i=0; $i < 24; $i++)
    {
        $hours[$i] = array("ans"=>0,"unans"=>0,"busy"=>0,"err"=>0);
    }


    if( is_array($calls) || is_object($calls) )
        foreach($calls as $call)
        {
            $dateObj = new DateTime("@".strtotime($call['DateCreated']));
            $dateObj->setTimezone($tz);
            $date = $dateObj->format("G");
            $arr = &$hours[$date];

            if($day!='all' && $day!=strtolower($dateObj->format("l")))
                continue;
            else{
                if($call['DialCallStatus']=="no-answer")
                    $arr["unans"]=$arr["unans"]+1;
                elseif($call['DialCallStatus']=="busy")
                    $arr["busy"]=$arr["busy"]+1;
                elseif($call['DialCallStatus']=="completed")
                    $arr["ans"]=$arr["ans"]+1;
                else{
                    $arr["err"]=$arr["err"]+1;
                    if(!$err_added){
                        $data->addCol("number","Error");
                        $err_added = true;
                    }
                }
            }
        }

    foreach($hours as $hour=>$count)
    {
        //$diff = floor(($end_date - $start_date)/(60*60*24));

        $data->addRow(array("c"=>array(
            array("f"=>date("ga",strtotime($hour.":00"))),
            array("v"=>$count['ans']),
            array("v"=>$count['unans']),
            array("v"=>$count['busy']),
            array("v"=>$count['err']),
            //array("v"=>($count/$diff))
        )));
    }

    die(json_encode(array("result"=>"success","data"=>$data->getData(),"day"=>$day)));
}
function CALLS_BY_WEEK($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();
    $company_id  = $params['data']['company_id'];
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];
    $phone_code  = $params['data']['phone_code'];
    $call_result = $params['data']['call_result'];
    $outgoing_num = $params['data']['outgoing_number'];
    $outgoing    = $params['data']['outgoing'];
    $campaign    = $params['data']['campaign'];

    if($outgoing==="false" || $outgoing == "")
        $outgoing = false;
    else
        $outgoing = true;

    $day = array();
    $data = new GDataView();
    $data->addCol("string","Day");
    $data->addCol("number","Answered");
    $data->addCol("number","Unanswered");
    $data->addCol("number","Busy");
    $err_added = false;

    $calls = $db->getCallsInRange($company_id,$start_date,$end_date,$phone_code,$call_result,false,$outgoing_num,$outgoing,"",$campaign);

    for($i=0; $i < 7; $i++)
    {
        $day[$i] = array("ans"=>0,"unans"=>0,"busy"=>0,"err"=>0);
    }

    if( is_array($calls) || is_object($calls) )
        foreach($calls as $call)
        {
            $dateObj = new DateTime("@".strtotime($call['DateCreated']));
            $date = $dateObj->format("N")-1;
            //$date = date("N",strtotime($call['DateCreated']))-1;
            $arr = &$day[$date];

            if($call['DialCallStatus']=="no-answer")
                $arr["unans"]=$arr["unans"]+1;
            elseif($call['DialCallStatus']=="busy")
                $arr["busy"]=$arr["busy"]+1;
            elseif($call['DialCallStatus']=="completed")
                $arr["ans"]=$arr["ans"]+1;
            else{
                $arr["err"]=$arr["err"]+1;
                if(!$err_added)
                {
                    $data->addCol("number","Error");
                    $err_added = true;
                }
            }
        }

    foreach($day as $d=>$count)
    {
        //$diff = floor(($end_date - $start_date)/(60*60*24));

        $data->addRow(array("c"=>array(
            array("f"=>jddayofweek($d,1)),
            array("v"=>$count['ans']),
            array("v"=>$count['unans']),
            array("v"=>$count['busy']),
            array("v"=>$count['err']),
            //array("v"=>($count/$diff))
        )));
    }

    die(json_encode(array("result"=>"success","data"=>$data->getData())));
}

function CAMPAIGN_DAILY_TRENDS($params='')
{
    global $client;
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();
    $company_id  = $params['data']['company_id'];
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];
    $phone_code  = $params['data']['phone_code'];
    $call_result = $params['data']['call_result'];
    $outgoing_num = $params['data']['outgoing_number'];
    $outgoing = ($params['data']['outgoing'] == "true") ? true : false;

    $numbers = $db->getNumbersOfCompany($company_id, true);

    $campaigns = array();
    foreach ($client->account->incoming_phone_numbers as $number) {
        $this_phone_number = $db->format_phone_db($number->phone_number);
        foreach ($numbers as $number2) {
            if ($number2->number == $this_phone_number) {
                if (!isset($campaigns[$number->friendly_name]))
                    $campaigns[$number->friendly_name] = array();

                $campaigns[$number->friendly_name][] = $this_phone_number;
                break;
            }
        }
    }

    $day = array();
    $data = new GDataView();

    $data->addCol("string", "Day");
    foreach ($campaigns as $name=>$numbers) {
        $data->addCol("number", $name);
    }

    $err_added = false;

    $calls = $db->getCallsInRange($company_id,$start_date,$end_date,$phone_code,$call_result,false,$outgoing_num,$outgoing);

    //var_dump($calls);

    for($i=0; $i < 7; $i++)
    {
        $this_day = array();
        foreach ($campaigns as $name=>$numbers) {
            $this_day[$name] = 0;
        }
        $day[$i] = $this_day;
    }

    if( is_array($calls) || is_object($calls) )
        foreach($calls as $call)
        {
            $dateObj = new DateTime("@".strtotime($call['DateCreated']));
            $date = $dateObj->format("N")-1;
            //$date = date("N",strtotime($call['DateCreated']))-1;
            $arr = &$day[$date];

            if ($outgoing)
                $callToFrom = $db->format_phone_db($call['CallFrom']);
            else
                $callToFrom = $db->format_phone_db($call['CallTo']);

            foreach ($campaigns as $name=>$numbers) {
                if (in_array($callToFrom, $numbers)) {
                    $arr[$name] = $arr[$name] + 1;
                }
            }
        }

    foreach($day as $d=>$count)
    {
        //$diff = floor(($end_date - $start_date)/(60*60*24));

        $data_array = array(
            "c"=>array(
                array("f"=>jddayofweek($d,1))
            )
        );

        foreach ($campaigns as $name=>$numbers) {
            $data_array["c"][] = array("v"=>$count[$name]);
        }

        $data->addRow($data_array);
    }

    die(json_encode(array("result"=>"success","data"=>$data->getData())));
}

function CALLS_GEN_CALLS_PIE($params=''){
    $db = new DB();
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];
    $company_id    = $params['data']['company_id'];

    $totalHits   = 0;
    $totalCalls  = 0;

    $data = new GDataView();
    $data->addCol("string","Keywords Generated Calls");
    $data->addCol("number","Calls");

    $calls = $db->getTotalCallsDone($start_date,$end_date, $company_id);
    $totalCalls = $calls['totalCalls'];
    unset($calls['totalCalls']);
    unset($calls['totalHits']);
    if( is_array($calls) || is_object($calls) )
        foreach($calls as $key => $value)
        {
            $keyword = $key.'('.$value['total_calls'].')';
            $data->addcol("number", $keyword);
            $cal = round($value['total_calls'] / $totalCalls * 100, 0);
            $data->addRow(array("c"=>array(
                array("f"=>$keyword),array("v"=> $cal)
            )));
        }
        
    die(json_encode(array("result"=>"success","data"=>$data->getData())));
}

function CALLS_All_HITS_PIE($params=''){
    $db = new DB();
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date']; 
    $company_id    = $params['data']['company_id'];
    $totalHits   = 0;
    $totalCalls  = 0;   
    $data = new GDataView();
    $data->addCol("string","All Calls Hits");
    $data->addCol("number","Call");
    $calls = $db->getTotalCallsDone($start_date,$end_date,$company_id);
    $totalCalls = $calls['totalCalls'];
    $totalHits = $calls['totalHits'];
    $cal = round($totalCalls/$totalHits * 100, 0);
    $hit = 100 - $cal;
    $callLable = 'Generated Calls ('.$totalCalls.')';
    $hitLable  = 'Non Generated Calls ('.$totalHits.')';    
    $data->addRow(array("c"=>array(
                array("f"=> $callLable),array("v"=> $cal)
            )));
     $data->addRow(array("c"=>array(
        array("f"=>$hitLable),array("v"=> $hit)
    )));    
    die(json_encode(array("result"=>"success","data"=>$data->getData())));
}

function CALLS_NONGEN_CALL($params=''){
    $db = new DB();
    $start_date  = $params['data']['start_date'];
    $end_date    = $params['data']['end_date'];
    $company_id    = $params['data']['company_id'];

    $totalHits   = 0;
    $totalCalls  = 0;

    $data = new GDataView();
    $data->addCol("string","Keywords Non Generated Calls");
    $data->addCol("number","Hits");

    $calls = $db->getTotalHitsGraph($start_date,$end_date,$company_id);
    $totalHits = $calls['totalHits'];
    unset($calls['totalHits']);
    
    if( is_array($calls) || is_object($calls) )
        foreach($calls as $key => $value)
        {
            $keyword = $key.'('.$value['total_hits'].')';
            $data->addcol("number", $keyword);
            $cal = round($value['total_hits'] / $totalHits * 100, 0);
            $data->addRow(array("c"=>array(
                array("f"=>$keyword),array("v"=> $cal)
            )));
        }
        
    die(json_encode(array("result"=>"success","data"=>$data->getData())));
}

function EMAIL_TRACKING_CLIENTS($params='')
{
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);
    $db = new DB();

    require_once 'include/db.client.php';

    $company_id = $params['data']['company_id'];
    $start_date = $params['data']['start_date'];
    $end_date = $params['data']['end_date'];
    $columns = array('Hours');

    $tracking_settings = $db->getTrackingSettingsForCompany($company_id);
    $company_name = $db->getCompanyName($company_id);

    $clients = json_decode($tracking_settings->clients);

    $columns = array('Hours');

    if(is_array($clients) || is_object($clients))
        foreach ($clients as $client) {
            $columns[] = trim($client->name);
        }

    for($i=0; $i <= ceil((strtotime($end_date) - strtotime($start_date))/86400); $i++)
    {
        $this_date = date("Y-m-d", strtotime($start_date) + ($i * 86400));

        $_i = 0;
        if(is_array($clients) || is_object($clients))
            foreach($clients as $client)
            {
                $stmt = $db->customQuery("SELECT COUNT(*) as total FROM email_tracking_log WHERE client_id = '".$client->id."' AND email_date LIKE '".$this_date."%'");

                $stmt->execute();
                $result = $stmt->fetch(PDO::FETCH_OBJ);

                $date_array[date("n/j", strtotime($this_date))][$_i] = (int)$result->total;

                $_i++;
            }
    }

    $main_array[] = $columns;

    if(is_array($date_array) || is_object($date_array))
        foreach($date_array as $key => $value)
        {
            $_arr = array($key);
            foreach($value as $subval)
            {
                $_arr[] = $subval;
            }
            $main_array[] = $_arr;
        }

    die(json_encode(array("result"=>"success","data"=>$main_array)));
}


class GDataView
{
    private $columns;
    private $rows;

    public function __construct()
    {
        $this->columns = array();
        $this->rows    = array();
    }

    public function addCol($type, $label="", $pattern = "", $id = "")
    {
        $col = (object)array();

        if($id!="")
            $col->id      = $id;
        if($label!="")
            $col->label   = $label;
        if($pattern!="")
            $col->pattern = $pattern;
        $col->type    = $type;

        $this->columns[] = $col;
    }

    public function addRow($cells)
    {
        $this->rows[] = $cells;
    }

    public function getData()
    {
        $data = (object)array();

        $data->cols = $this->columns;
        $data->rows = $this->rows;

        return $data;
    }
}

function getMonthString($n)
{
    $timestamp = mktime(0, 0, 0, $n, 1, 2005);

    return date("M", $timestamp);
}

?>