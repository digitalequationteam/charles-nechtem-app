<?php
require_once('include/db.php');
require_once('include/config.php');


if(strlen(@$_POST['email']) >0)
{
    $db = new DB();

    $email = $_POST['email'];

    if(substr($_SERVER["HTTP_HOST"],0,4)=="www.")
        $host = substr($_SERVER["HTTP_HOST"],4,strlen($_SERVER["HTTP_HOST"]-4));
    else
        $host = $_SERVER["HTTP_HOST"];

    $host = $host.str_replace("/checkversion.php","",$_SERVER['SCRIPT_NAME']);

    global $AccountSid;

    $jsonurl = "http://license.web1syndication.com/abc123/checkACTLicense/".urlencode($email)."/".urlencode(base64_encode($host))."/".urlencode($AccountSid);

    $json = $db->curlGetData($jsonurl);
    $json_output = json_decode($json);

    $result = 100;

    $json_output->result = "success";

    if($json_output->result == "success")
        $result = 0;
    else{
        switch($json_output->reason)
        {
            case "NO_ACT_TAGS":
                {
                $result = 1;
                break;
                }
            case "LIMIT_EXCEEDED":
                {
                $result = 2;
                break;
                }
            default:
                {
                $result = 3;
                break;
                }
        }
    }

    switch($result)
    {
        case 0:
        {

            $arcver = new stdClass();

            $arcver->lce = $email;
            $arcver->lc  = strtotime(date("Y-m-d"));
            $arcver->lcl = $json_output->lcl;

            $db->setVar("arcver","");
            $db->setVar("arcver",base64_encode(serialize($arcver)));

            ?>
        <script type="text/javascript">
            window.location = "index.php";
        </script>
        <?php
            break;
        }

        case 1:
            {
            ?>
            <script type="text/javascript">
                alert('You have not purchased ACT. If you feel this is an error, please contact support at: web1support.com');
                window.location = "checkversion.php?failed=1";
            </script>
            <?php
            break;
            }

        case 2:
            {
            ?>
            <script type="text/javascript">
                alert('You have exceeded the max number of ACT installations for your account. If you need more, please visit web1support.com to open a ticket.');
                window.location = "checkversion.php?failed=1";
            </script>
            <?php
            break;
            }

        default:
            {
            ?>
            <script type="text/javascript">
                alert('An unknown error has occured. Please contact support at: web1support.com');
                window.location = "checkversion.php?failed=1";
            </script>
            <?php
            break;
            }
    }
    die();
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->
</head>




<body>

<div id="hld">

    <div class="wrapper">		<!-- wrapper begins -->
        <div class="block small center login">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>Authenticate Your ACT</h2>
                <ul>
                    <li></li>
                </ul>
            </div>		<!-- .block_head ends -->


            <div class="block_content">

                <?php if(@$_GET['failed']=="1"){ ?>
                <div class="message errormsg" style="width: 86%;margin: 0 auto;margin-top: 8px;"><p>ACT has failed to authenticate. Please enter your email to try again.</p></div><br>
                <?php } ?>

                <form action="checkversion.php" method="post" id="form1" name="form1">
                    <p>
                        <label>Email:</label> <br />
                        <input id="email" type="text" name="email" class="text" value="" />
                    </p>

                    <p>
                        <input type="submit" class="submit" style="float:right;" value="Submit" />
                    </p>
                </form>

            </div>		<!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>

        </div>		<!-- .login ends -->

    </div>						<!-- wrapper ends -->

</div>		<!-- #hld ends -->


<!--[if IE]><script type="text/javascript" src="js/excanvas.js"></script><![endif]-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.img.preload.js"></script>
<script type="text/javascript" src="js/jquery.filestyle.mini.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/jquery.date_input.pack.js"></script>
<script type="text/javascript" src="js/facebox.js"></script>
<script type="text/javascript" src="js/jquery.visualize.js"></script>
<script type="text/javascript" src="js/jquery.visualize.tooltip.js"></script>
<script type="text/javascript" src="js/jquery.select_skin.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/ajaxupload.js"></script>
<script type="text/javascript" src="js/jquery.pngfix.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
    $(document).ready(function(e) {
        $("#email").focus();
    });
</script>

</body>
</html>