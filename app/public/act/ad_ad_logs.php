<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_auto_dialer";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

//Check for user perm
if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10006)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

$logs = ad_ad_get_logs(isset($_GET['page']) ? $_GET['page'] : '1', 20, isset($_GET['camp_idx']) ? $_GET['camp_idx'] : FALSE, isset($_GET['filter_by']) ? $_GET['filter_by'] : FALSE);

$action_response = '';
$action_response_type = '';

//Action handlers
if (isset($_GET['action'])):

    //Switching over action type
    switch ($_GET['action']):
        //If action is set to export
        case 'export':

            $export_logs = ad_ad_get_logs(isset($_GET['page']) ? $_GET['page'] : '1', 20, isset($_GET['camp_idx']) ? $_GET['camp_idx'] : FALSE, isset($_GET['filter_by']) ? $_GET['filter_by'] : FALSE, true);

            //Setting the name of export file
            $export_name = 'export';

            //If logs displayed on page are of particular campaign
            //Setting the campaign name as name of export file
            if (isset($_GET['camp_idx']))
                $export_name = str_replace(' ', '_', ad_ad_get_campaign_name($_GET['camp_idx']));

            //if tmp directory does not exists
            //creating it
            if (!file_exists(dirname(__FILE__) . '/tmp/'))
                mkdir(dirname(__FILE__) . '/tmp/');

            //If file already exists on server
            //Deleting it
            if (file_exists(dirname(__FILE__) . '/tmp/' . $export_name . '.csv'))
                unlink(dirname(__FILE__) . '/tmp/' . $export_name . '.csv');

            //Creating a new file and retrieving the handler to perform operations on it
            $fp = fopen(dirname(__FILE__) . '/tmp/' . $export_name . '.csv', 'a+');

            //If file opened succefully to perform write operations
            if (is_resource($fp)):

                //Retrieving the column names
                $column_names[0] = array_keys($export_logs[0]);

                //replacing campaign_idx name with campaign
                $first_row_campaign_name_key = array_search('campaign_idx', $column_names[0]);
                $column_names[0][$first_row_campaign_name_key] = 'Campaign';

                //replacing code with phone code
                $first_row_code_key = array_search('code', $column_names[0]);
                $column_names[0][$first_row_code_key] = 'Phone Code';

                //Building the final array
                $csv_data = array_merge($column_names, $export_logs);

                //If campaign ID is set in request URL
                //Means logs displayed on page are of particular campaign
                //Deleting the campaign column from first row.
                if (isset($_GET['camp_idx']))
                    unset($csv_data[0][$first_row_campaign_name_key]);

                //Looping over CSV data
                foreach ($csv_data as $single_row):

                    if (isset($single_row['campaign_idx']))
                        $single_row['campaign_idx'] = ad_ad_get_campaign_name($single_row['campaign_idx']);

                    //If logs are of particular campaign
                    //Excluding campaign name column from CSV file
                    //Because the export file is named with campaign name
                    if (isset($_GET['camp_idx']) && isset($single_row['campaign_idx']))
                        unset($single_row['campaign_idx']);

                    if (isset($single_row['user']))
                        $single_row['user'] = ad_get_user($single_row['user']);

                    if (isset($single_row['duration']))
                        $single_row['duration'] = Util::formatTime($single_row['duration']);

                    if (isset($single_row['from']))
                        $single_row['from'] = format_phone($db->format_phone_db($single_row['from']));

                    if (isset($single_row['to']))
                        $single_row['to'] = format_phone($db->format_phone_db($single_row['to']));

                    //Writing in CSV file
                    fwrite($fp, '"' . implode('","', $single_row) . "\"\n");

                endforeach;

                //Setting the response message and its type
                //Displaying the download link.
                $action_response = '<a href="tmp/' . $export_name . '.csv">Click here</a> to download exported CSV file.';
                $action_response_type = 'success';

            endif;

            //Closing the connection to file to stop from furthur operations
            fclose($fp);

            break;

        default:
            break;

    endswitch;

//Ending condition checking whether action parameter is set in request URL or not
endif;

//Processing part ends here
//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                <div class="block bulk_dial_status" style="display: none;background: none;color: ">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2 style="color:red;" class="ad_ad_bulk_dial_status_text"></h2>
                    </div>
                </div>
                <div class="clear"></div>

                <!--Auto dialer campaigns listing section starts here-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Auto Dialer Logs <span style="text-transform: none;"><?php echo isset($_GET['camp_idx']) ? '(Campaign: ' . ad_ad_get_campaign_name($_GET['camp_idx']) . ')' : ''; ?></span></h2>
                        <ul>
                            <li>
                                <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('action' => 'export')); ?>"><img src="images/excel_img.png" width="50" alt="Export as CSV" title="Export as CSV" /></a>
                            </li>
                        </ul>
                        <ul>
                            <li><a href="ad_ad_add_campaign.php">Add a Campaign</a></li>
                            <li><a href="ad_ad_campaigns.php">Campaigns List</a></li>
                            <li><a href="ad_contactlist_log.php">Contacts</a></li>
                            <li><a href="ad_ad_logs.php">Logs</a></li>
                        </ul>
                    </div>		<!-- .block_head ends -->
                    <div class="block_content">
                        <div class="message info">
                            <p>
                                <b>Filter by Response: </b>
                                <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('filter_by' => 'completed', 'action' => FALSE, 'page' => 1)) ?>">completed</a> | 
                                <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('filter_by' => 'failed', 'action' => FALSE, 'page' => 1)) ?>">failed</a> | 
                                <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('filter_by' => 'busy', 'action' => FALSE, 'page' => 1)) ?>">busy</a> | 
                                <a href="<?php echo s8_add_params_to_url(s8_get_current_webpage_uri(), array('filter_by' => 'no-answer', 'action' => FALSE, 'page' => 1)) ?>">no-answer</a>
                            </p>
                        </div>
                        <?php
                        //If response message is set
                        if (!s8_is_str_blank($action_response)):
                            //Switching over type of message
                            switch ($action_response_type) {
                                //If response message is of success type
                                case 'success':
                                    //Displaying the success message.
                                    ?>
                                    <div class="message success"><p><?php echo $action_response; ?></p></div>
                                    <?php
                                    break;

                                default:
                                    break;
                            }
                        endif;
                        ?>
                        <form action="" method="post">
                            <table cellpadding="0" id="call_log" cellspacing="0" width="100%" class="sortable">
                                <thead>
                                    <tr>
                                        <th>To</th>
                                        <th>Date</th>
                                        <th>Campaign Name</th>
                                        <th>From</th>
                                        <th>Duration</th>
                                        <th>Response</th>
                                        <th>User</th>
                                        <th>Recording</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($logs) > 0):
                                        foreach ($logs as $single_call_log):
                                            ?>
                                            <tr>
                                                <td style="white-space: nowrap;">
                                                    <a href="ad_ajax.php?action=show_dd&contact_idx=<?php echo $single_call_log['to'] ?>" rel="facebox">
                                                        <?php
                                                        echo $single_call_log['to'];
                                                        ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <?php
                                                        $date = Util::convertToLocalTZ($single_call_log['date']);
                                                    echo $date->format("D n\/j Y g\:iA");
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo ad_ad_get_campaign_name($single_call_log['campaign_idx']);
                                                    ?>
                                                </td>
                                                <td style="white-space: nowrap;">
                                                    <?php
                                                    echo format_phone($db->format_phone_db($single_call_log['from']));
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo Util::formatTime($single_call_log['duration']);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $single_call_log['response'];
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo ad_get_user($single_call_log['user']);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    Util::generateFlashAudioPlayer((!s8_is_str_blank($single_call_log['recording_url']) ? $single_call_log['recording_url'] : ' - '), "sm");
                                                    if($single_call_log['company_id']!=""){ ?>
                                                    <select title="Select a Phone Code" style="margin-top: 3px;width: 135px;" class="sid_<?php echo $single_call_log['CallSid']; ?>" onchange="$(this).attr('disabled','true'); changePhoneCodeAD('<?php echo $single_call_log['CallSid']; ?>',this.options[this.selectedIndex].value,(function(){$('.sid_<?php echo $single_call_log['CallSid']; ?>').removeAttr('disabled');}),2)">
                                                        <?php
                                                        $phone_codes = $db->getPhoneCodes($single_call_log['company_id'],2);
                                                        if($single_call_log['code']==0)
                                                            echo "<option value='0' selected>None</option>";
                                                        else
                                                            echo "<option value='0'>None</option>";

                                                        if($phone_codes!=false){
                                                            foreach($phone_codes as $code)
                                                            {
                                                                if($single_call_log['code']==$code->idx)
                                                                    echo "<option value='$code->idx' selected>$code->name</option>";
                                                                else
                                                                    echo "<option value='$code->idx'>$code->name</option>";
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        ?>
                                        <tr>
                                            <td>No records exist.</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php
                                    endif;
                                    ?>
                                </tbody>
                            </table>

                            <?php
                            $total_logs = ad_ad_get_logs_count(isset($_GET['camp_idx']) ? $_GET['camp_idx'] : FALSE, isset($_GET['filter_by']) ? $_GET['filter_by'] : FALSE);

                            $pagination = new Pagination();
                            $page_n = 1;
                            if (isset($_GET['page'])){
                                $page_n = (int) $_GET['page'];
                            }
                            if(strpos($_SERVER['REQUEST_URI'],"page") === false)
                            {
                                $url="//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                $pagination->setLink($url."&page=%s");
                            }else{
                                $url="//".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['page']));
                                $pagination->setLink($url."%s");
                            }
                            $pagination->setPage($page_n);
                            $pagination->setSize(20);
                            $pagination->setTotalRecords($total_logs);

                            echo $pagination->create_links();
                            ?>

                        </form>
                    </div>		<!-- .block_content ends -->
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Auto dialer campaigns listing ends here-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>
        <script type="text/javascript" src="js/PhoneFormat.js<?php echo "?".$build_number; ?>"></script>
        <!--//Notification bar html-->
        <div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
        <script type="text/javascript">
            function changePhoneCodeAD(callsid, phonecode, func, type)
            {
                $.get('ad_ajax.php?action=ad_ad_call_change_pc&pc='+phonecode+'&callsid='+callsid,function(res){
                    func();
                });
            }
            $(document).ready(function(){
                var numbers = $("#call_log tr").find("td a:eq(0)");
                $(numbers).each(function(i,v){
                    var formatted = "";
                    var number = $(v).text().trim();
                    var country = (number.trim().length == 10) ? "" : countryForE164Number("+"+number);

                    console.log(country + " for "+number);

                    if(country!="" && country!="FR"){
                        formatted = formatLocal(country,"+" + number);
                    }else{
                        if(number[0]=="1")
                            formatted = formatLocal("US",number);
                        else
                            formatted = formatLocal("US", "+1" + number);
                    }

                    if(formatted!="" && typeof formatted != 'undefined')
                        $(v).text(formatted).addClass("formatted-number");
                });

            });
        </script>
    </body>
</html>