<?php

/*
 * This file will handle the tasks that would be done via cron job
 */
//Sending 200 OK response
header('HTTP/1.1 200 OK');

//Initiaizing the session
session_start();

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';


//If cron action param is set in url
if (isset($_GET['action'])):

    //Switching to action type
    switch ($_GET['action']):
        //If action is set to process campaigns
        case 'process_campaigns':
            ignore_user_abort(1);
            set_time_limit(300);
            //Retrieving the campaigns from the system
            $campaigns = ad_vb_get_campaigns_cron();
            //Looping over each campaign
            $count = 0;
            foreach ($campaigns as $single_campaign_detail):
                //If campaign property "when to run" is not set to strings "now" and "draft".
                if ($single_campaign_detail['when_to_run'] != 'now' && $single_campaign_detail['when_to_run'] != 'draft' && $single_campaign_detail['calls_status']=="scheduled"):
                    $count = $count+1;
                    //Retrieving the scheduled date of campaign
                    $camp_scheduled_date = $single_campaign_detail['when_to_run'];
                    //If current time is greater than or equals to campaign date
                    if (time()+60 >= $camp_scheduled_date){
                        //Processing the voice broadcast campaign
                        ad_vb_process_voice_broadcast_campaign($single_campaign_detail['idx'], dirname(s8_get_current_webpage_uri()) . '');
                    }
                endif;
            endforeach;
            echo "[$count] ".date('YmdHis');
            break;
        case 'get_system_time':
            echo date('m/d Y H:i:s A');
            break;
        case 'process_call_flow_sms':{
            if(isset($_REQUEST['From']) && isset($_REQUEST['To']) && isset($_REQUEST['wId'])){
                $db = new DB();
                $stmt = $db->customExecute("SELECT * FROM call_ivr_widget WHERE wId = ?");
                $stmt->execute(array($_REQUEST['wId']));
                $data = $stmt->fetch(PDO::FETCH_OBJ);
                if($data->content != ""){
                    global $AccountSid, $AuthToken;
                    $client = new Services_Twilio($AccountSid, $AuthToken);
                    $client->account->messages->sendMessage($_REQUEST['From'], $_REQUEST['To'], html_entity_decode($data->content));
                }
            }
            break;
        }
        case 'process_sequence':{
            $db = new DB();
            $schedule_id = $_GET['schedule_id'];
            $stmt = $db->customExecute("SELECT * FROM ad_vb_sequence_schedule WHERE id = ?");

            if($stmt->execute(array($schedule_id))){
                $schedule = $stmt->fetch(PDO::FETCH_OBJ);

                $stmt = $db->customExecute("SELECT * FROM ad_vb_sequences WHERE id = ?");
                $stmt->execute(array($schedule->sequence_id));
                $sequence = $stmt->fetch(PDO::FETCH_OBJ);

                $stmt = $db->customExecute("SELECT * FROM ad_advb_cl_contacts WHERE idx = ?");
                $stmt->execute(array($schedule->contact_id));
                $contact = $stmt->fetch(PDO::FETCH_OBJ);

                $stmt = $db->customExecute("SELECT * FROM ad_vb_campaigns WHERE idx = ?");
                $stmt->execute(array($sequence->campaign_id));
                $campaign = $stmt->fetch(PDO::FETCH_OBJ);

                global $AccountSid, $AuthToken;
                $client = new Services_Twilio($AccountSid, $AuthToken);

                $caller_id = $campaign->phone_number;
                $to = $contact->phone_number;
                $campaign_idx = $sequence->campaign_id;

                $list_details = $db->getContactListById($campaign->list_id);

                $opted_out = $db->checkNumberOptedOut($campaign->list_id, $db->format_phone_db($to));

                if (!$opted_out) {
                    switch($sequence->type){
                        case "AutoResponder":
                        case "SMSBroadcast":
                            $msg = $sequence->sms_message;
                            $msg = str_replace("[FirstName]",$contact->first_name,$msg);
                            $msg = str_replace("[LastName]",$contact->last_name,$msg);
                            $msg = str_replace("[Email]",$contact->email,$msg);
                            $msg = str_replace("[Phone]",$contact->phone_number,$msg);
                            $msg = str_replace("[Address]",$contact->address,$msg);
                            $msg = str_replace("[City]",$contact->city,$msg);
                            $msg = str_replace("[State]",$contact->state,$msg);
                            $msg = str_replace("[Zip]",$contact->zip,$msg);
                            $msg = str_replace("[Website]",$contact->website,$msg);
                            $msg = str_replace("[Business]",$contact->business_name,$msg);

                            $msg .= "\n\n".$list_details['sms_opt_out_message'];

                            $global_sms_opt_out_message = ($db->getVar("global_sms_opt_out_message")) ? $db->getVar("global_sms_opt_out_message") : "Reply with CANCEL ALL to stop.";

                            $msg .= "\n\n".$global_sms_opt_out_message;

                            $sms = $client->account->messages->sendMessage($caller_id, $to, $msg, null, array(
                                        "StatusCallback" => dirname(s8_get_current_webpage_uri()) . "/include/ad_auto_dialer_files/ad_status_callback.php?cmp_idx=$campaign_idx&seq_id=".$sequence->id."&cmp_type=vb&to=" . urlencode(trim($to)) . "&caller_id=" . urlencode(trim($caller_id)) . "&userid=" . trim($campaign->user)
                                    )
                                );

                            break;

                        case "VoiceBroadcast":
                            $client->account->calls->create(trim($caller_id), trim($to), dirname(s8_get_current_webpage_uri()) . "/include/ad_auto_dialer_files/ad_twiml.php?cmp_idx=$campaign_idx&cmp_type=vb&to=" . urlencode(trim($to)) . "&caller_id=" . urlencode(trim($caller_id))."&seq_id=".$sequence->id, array(
                                "Method" => "POST",
                                "IfMachine" => "Continue",
                                "Record" => true,
                                "StatusCallback" => dirname(s8_get_current_webpage_uri()) . "/include/ad_auto_dialer_files/ad_status_callback.php?cmp_idx=$campaign_idx&seq_id=".$sequence->id."&cmp_type=vb&to=" . urlencode(trim($to)) . "&caller_id=" . urlencode(trim($caller_id)) . "&userid=" . trim($campaign->user),
                                "StatusCallbackMethod" => "POST"
                            ));
                            break;
                    }
                }
                echo "DONE! ". $to;

                $stmt = $db->customExecute("DELETE FROM ad_vb_sequence_schedule WHERE id = ?");
                $stmt->execute(array($schedule->id));
                $stmt = $db->customExecute("UPDATE ad_vb_campaigns SET last_ran = ? WHERE idx = ?");
                $stmt->execute(array(strtotime("now"),$sequence->campaign_id));
            }else{
                echo "AR Error: 1";
            }
        }
    endswitch;

endif;
exit;
?>