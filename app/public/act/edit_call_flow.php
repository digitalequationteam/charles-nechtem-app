<?php
//Check for config..
if (!file_exists("include/config.php")) {
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');

$page = "adminpage";
$db = new DB();
$companies = $db->getAllCompanies();

//Pre-load Checks
if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit;
}
if(!isset($_GET['company'])) {
    header("Location: manage_companies.php");
    exit;
}
if(@$_SESSION['permission']<1 && !$_SESSION['prems']['customize_call_flow']) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

$company_name = $db->getCompanyName($_GET['company']);
$company_settings = $db->getCompanySettings($_GET['company']);

if(isset($_POST['call_flow']))
{
    if($_POST['call_flow']!="")
    {
        if($_POST['call_flow'] == 4 && @$lcl<2){
            ?>
            <script type="text/javascript"> alert('This is an enterprise-only feature.'); window.location = 'edit_call_flow.php?company=<?php echo $_GET['company']; ?>'; </script>
            <?php
            exit;
        }
        $company_id = addslashes($_GET['company']);
        $call_flow = addslashes($_POST['call_flow']);
        $db->customQuery("UPDATE companies SET call_flow = '".$call_flow."' WHERE idx = '".$company_id."'");
        header("Location: edit_call_flow.php?company=".$company_id."&updated=1");
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]>
    <style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        select.styled {
            width: 245px;
            height: 18px;
            margin-right: 20px;
            cursor: pointer;
        }
        .cmf-skinned-select {
            padding: 7px;
            display: block;
            margin-right: 20px;
            border: 1px solid #bbb;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            background: url('../images/sdd.jpg') center right no-repeat;
        }
        .cmf-skinned-select:hover {
            background: url('../images/sdd_.jpg') center right no-repeat;
        }
        div.cmf-skinned-text {
            padding: 5px 7px;
            font-family: "Lucida Grande", Verdana, sans-serif;
            font-size: 14px;
            color: #333;
        }
        .ui-dialog label, input {
            display: block !important;
            font-weight: bold;
        }
        .callflow {
            font-family: "Titillium999", "Trebuchet MS", Arial, sans-serif;
            font-size: 18px;
            font-weight: normal;
            color: #454545;
            margin-bottom: 10px;
            margin-left: 5px;

        }
    </style>

</head>

<body>

<div id="hld">

    <div class="wrapper">        <!-- wrapper begins -->


        <?php include_once("include/nav.php"); ?>
        <!-- #header ends -->

        <div class="block" id="filter-box" style="width:620px; margin:0 auto; margin-bottom: 25px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>Edit Call Flow For <span style="color:#777;"><?php echo $company_name; ?></span></h2>
                <div style="display:inline-block; float: right;"><h2 style="font-size: 16px;"><a href="manage_companies.php">Back</a></h2></div>
            </div>
            <!-- .block_head ends -->

            <div class="block_content">
                <?php if(isset($_GET['updated'])) { ?>
                <div class="message success" style="width: 86%; margin: 0px 0px 11px;"><p>Call Flow set to: <b><?php echo CallFlow::getCallFlowName($company_settings->call_flow); ?></b></p></div>
                <?php } ?>
                <form action="edit_call_flow.php?company=<?php echo $_GET['company']; ?>" method="post">
                    <input type="hidden" id="company_id" value="<?php echo $_GET['company']; ?>"/>
                    <?php

                    foreach(CallFlow::getCallFlows() as $idx => $call_flow)
                    {
                        if(isset($call_flow['disabled']) && $call_flow['disabled']==true)
                            continue;

                        if($idx==$company_settings->call_flow)
                            $selected=' checked="checked"';
                        else
                            $selected='';
                        ?>
                        <input type="radio"<?php if($idx == 4 && @$lcl<2) echo " disabled"; ?> name="call_flow"<?php echo $selected; ?> style="display:inline-block !important;vertical-align: text-top;margin-right: 2px;" value="<?php echo $idx; ?>"><span<?php if($idx == 4 && @$lcl<2) echo ' style="opacity: .5;"'; else echo ' style=" cursor: pointer;" onclick="selectCheck(\''.$idx.'\')"'; ?> class="callflow" id="label_<?php echo $idx; ?>"><?php echo $call_flow['name']; ?></span>
                        <?php
                        if($idx!=0){
                            if($idx == 4 && @$lcl<2)
                                echo "";
                            else
                                echo '<a href="#" onclick="update_flow(\''.$idx.'\' , \''.$call_flow['name'].'\') "><img src="images/call_flow_edit_settings.png" style="margin-left: 5px;" /></a>';
                        }
                        ?>
                        <p style="padding-left: 20px;"><?php echo $call_flow['desc']; ?></p>
                        <?php
                    }

                    ?>
                    <br/>
                    <div style="width:100%; height: 1px; background:#888; margin-bottom:10px;"></div>
                    <input type="submit" class="submit mid"
                           style="margin: 0 auto 10px auto; display: block !important;"
                           id="save_call_flow" value="Save"/>
                </form>
            </div>
            <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>


        <?php include "include/footer.php"; ?>
        <script type="text/javascript">
            function selectCheck(idx){ $("input[type='radio'][value='"+idx+"']").trigger("click"); }

            var companies = [];

            <?php
            foreach ($companies as $company) {
                $stmt = $db->customQuery("SELECT wId FROM call_ivr_widget WHERE companyId = '".$company["idx"]."' AND temporary = '0'");
                $result = $stmt->fetchAll();
                if ($result) {
                    ?>
                        companies.push({
                            idx: '<?php echo $company["idx"]; ?>',
                            company_name: '<?php echo addslashes($company["company_name"]); ?>'
                        });
                    <?php
                }
            }
            ?>
        </script>
</body>
</html>