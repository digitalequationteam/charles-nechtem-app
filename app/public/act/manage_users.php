<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$page = "adminpage";
$db = new DB();
$user_count = $db->getUserCount();
$users = $db->getAllUsers();

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

global $lcl;

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

</head>





<body>

<div id="hld">

<div class="wrapper">		<!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->


<div class="block">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>
            <?php if($user_count==1) echo ("1 User &nbsp; &nbsp; <a href=\"#\" id=\"SYSTEM_ADD_USER\">Add New User</a>"); else
            echo ($user_count." Users &nbsp; &nbsp; <a href=\"#\" id=\"SYSTEM_ADD_USER\">Add New User</a>");?>
        </h2>

    </div>		<!-- .block_head ends -->


    <div class="block_content">
        <input type="hidden" id="lcl" value="<?php echo $lcl; ?>" />
        <table cellpadding="0" cellspacing="0" width="100%" class="sortable" id="users">

            <thead>
            <tr>
                <th>User</th>
                <th>Name</th>
                <th>Email</th>
                <th style="width: 70px;">Companies</th>
                <th style="cursor: pointer;width: 150px;">Actions</th>
            </tr>
            </thead>

            <tbody>

            <?php foreach ($users as $user) {
                $admin="";
                $companies=array();
                $promote="";
                $deleteUser="";
                $demote="";
                $plugin_permissions="";
                $reset_password="";

                if($user['user_data']['access_lvl']>0)
                    $admin = "<div title='This user in an Admin' style=\"background: url(images/th_admin.gif) no-repeat 50% 50%;width: 16px;height: 16px;float: left;\"></div>&nbsp;";

                foreach($user['user_companies'] as $company)
                {
                    $companies[] = $company[1];
                }

                if($user['user_data']['access_lvl']==0)
                    $promote = "&nbsp;&nbsp;<a href=\"#\" id='SYSTEM_PROMOTE_USER' data-params='" . $user['user_data']['idx'] . "' title='Promote User to Admin'><img src=\"images/up_arrow_16.png\" /></a>";

                if($user['user_data']['access_lvl']>=1 && $user['user_data']['idx']!=$_SESSION['user_id'] && $user['user_data']['username']!="admin")
                    $demote = "&nbsp;&nbsp;<a href=\"#\" id='SYSTEM_DEMOTE_USER' data-params='" . $user['user_data']['idx'] . "' title='Demote the user'><img src=\"images/down_arrow_16.png\" /></a>";

                if($user['user_data']['username']!="admin" && $user['user_data']['idx']!=$_SESSION['user_id']){
                    $deleteUser="<a href=\"#\" id='SYSTEM_DELETE_USER' data-params='" . $user['user_data']['idx'] . "' title='Delete User'><img src=\"images/delete.gif\" /></a>";
                    $reset_password="&nbsp;&nbsp;<a href='#' id='SYSTEM_RESET_USER' data-params='".$user['user_data']['idx']."' title='Reset Password and Email it to the user'><img width='16' src='images/regenerate.png' /></a>";
                }
                if($user['user_data']['access_lvl']==0)
                    $plugin_permissions = '&nbsp;&nbsp;<a href="#" id="SYSTEM_USER_PRIV" data-params="'.$user['user_data']['idx'].'" title="Change the permissions for this user"><img src="images/1355869412_key.png" /></a>'.
                        '&nbsp;&nbsp;<a href="#" id="SYSTEM_USER_PHONE_PRIV" data-params="'.$user['user_data']['idx'].'" title="Change allowed phone numbers."><img src="images/phone-dg-32.png" width="16"></a>';

                $companies_nr = count($companies);
            ?>
            <tr data-key="<?php echo $user['user_data']['idx']; ?>">
                <td><?php echo "$admin " . $user['user_data']['username'] . ""; ?></td>
                <td><?php echo @$user['user_data']['full_name']; ?></td>
                <td><?php echo @$user['user_data']['email']; ?></td>
                <?php if($user['user_data']['username']=="admin") { ?>
                <td></td>
                <?php }else{ ?>
                <td><?php echo "<a href=\"#\" id='SYSTEM_EDIT_USER_COMPANY' data-params='" . $user['user_data']['idx'] . "' title='Edit Companies' style='cursor:pointer'>".$companies_nr."</a>"; ?></td>
                <?php } ?>
                <td style="min-width: 115px;"><?php echo $deleteUser.$promote.$demote.$plugin_permissions.$reset_password; ?>
                    &nbsp;&nbsp;<a href="#" id="SYSTEM_SET_FORWARDING_EMAIL" data-params="<?php echo $user['user_data']['idx']; ?>" title="Setup email forwarding"><img src="images/email-icon.gif" style="width:16px;"></a>
                </td>
            </tr>
                <?php } ?>
        </table>

    </div>		<!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>
</div>		<!-- .block ends -->


<?php include "include/footer.php"; ?>
</body>
</html>