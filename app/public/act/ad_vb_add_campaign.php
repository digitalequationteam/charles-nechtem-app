<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_voice_broadcast";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10007)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

$act = (isset($_GET['act']) ? $_GET['act'] : "");

if (  $act == 'uploadMp3' )
{   
    $uploaddir = 'audio/voice_broadcast_files/'.$user_id.'/'; 
    
    @mkdir ('audio/voice_broadcast_files/',0777,true);
    @chmod ('audio/voice_broadcast_files/',0777);
    
    @mkdir ($uploaddir,0777,true);
    @chmod($uploaddir,0777);
    
    $file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name'])); 
     
    if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
    { 
        $filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
    
        echo "$filename"; 
    
    } else {
        echo "error";
    }

    exit();
}

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

$response = '';
$response_type = '';

$ad_vb_ac_live_answer_content = "";
$ad_vb_ac_live_answer_type = "";
$ad_vb_ac_live_answer_voice = "";
$ad_vb_ac_live_answer_language = "";
$ad_vb_ac_voicemail_message_content = "";
$ad_vb_ac_voicemail_message_type = "";
$ad_vb_ac_voicemail_message_voice = "";
$ad_vb_ac_voicemail_message_language = "";

$voicemail_only = false;
$live_answer_only = false;

$allow_ivr = false;
$ivr_key = "";
$ivr_phone_number = "";

//Checking form submission
if (isset($_POST['ad_vb_process_settings_form'])):

    //General variables
    //Retrieving the name of campaign
    $ad_vb_campaign_name = @$_POST['ad_vb_campaign_name'];
    //Retrieving the twilio caller id from form
    $phone_number = @$_POST['ad_vb_ac_phone_number'];
    //Retrieving the selected list ID.
    $ad_vb_contacts_list_idx = @$_POST['ad_vb_ac_contact_list'];
    //Retrieving the typ of broadcast
    $broadcast_type = @$_POST['ad_vb_broadcast_type'];
    $ad_vb_broadcast_type = @$_POST['ad_vb_ac_broadcast_type'];
    $ad_vb_broadcast_type_str_format = @implode(',', $ad_vb_broadcast_type);
    //Retrieving the sms message
    $sms_message = @$_POST['sms_message'];

    //Retrieving the "When to run" status of this campaign.
    $ad_vb_ac_when_to_run = @$_POST['ad_vb_ac_when_to_run'];

    //Retriving the sequence settings
    $ad_vb_ac_seq_time = @$_POST['ad_vb_ac_seq_time'];
    $ad_vb_ac_seq_action = @$_POST['ad_vb_ac_seq_action'];
    $ad_vb_ac_seq_function = @$_POST['ad_vb_ac_seq_function'];

    global $TIMEZONE;
    //Retrieving the schedule data if campaign is not going to run now.
    try{
        $ad_vb_ac_scheduled_date = new DateTime(
            $_POST['ad_vb_ac_sd_year'] . '/' .
                ($_POST['ad_vb_ac_sd_month'] < 10 ? '0' . $_POST['ad_vb_ac_sd_month'] : $_POST['ad_vb_ac_sd_month']) . '/' .
                ($_POST['ad_vb_ac_sd_day'] < 10 ? '0' . $_POST['ad_vb_ac_sd_day'] : $_POST['ad_vb_ac_sd_day']) . ' ' .
                ($_POST['ad_vb_ac_sd_hour'] < 10 ? '0' . $_POST['ad_vb_ac_sd_hour'] : $_POST['ad_vb_ac_sd_hour']) . ':' .
                ($_POST['ad_vb_ac_sd_minute'] < 10 ? '0' . $_POST['ad_vb_ac_sd_minute'] : $_POST['ad_vb_ac_sd_minute']) . ':00' .
                (' ' . $_POST['ad_vb_ac_sd_period'])
        , new DateTimeZone($TIMEZONE));

        $ad_vb_ac_scheduled_date->setTimezone(new DateTimeZone("UTC"));
        $ad_vb_ac_scheduled_date = $ad_vb_ac_scheduled_date->format("U");
    }catch(Exception $ex){
        $ad_vb_ac_scheduled_date = strtotime("now");
    }
    
    $ad_vb_ac_live_answer_content = $_POST['live_answer_content'];
    $ad_vb_ac_live_answer_type = $_POST['live_answer_type'];
    $ad_vb_ac_live_answer_voice = $_POST['live_answer_voice'];
    $ad_vb_ac_live_answer_language = $_POST['live_answer_language'];
    $ad_vb_ac_voicemail_message_content = $_POST['voicemail_message_content'];
    $ad_vb_ac_voicemail_message_type = $_POST['voicemail_message_type'];
    $ad_vb_ac_voicemail_message_voice = $_POST['voicemail_message_voice'];
    $ad_vb_ac_voicemail_message_language = $_POST['voicemail_message_language'];

    $voicemail_only = ($_POST['machine_type'] == "voicemail_only");
    $live_answer_only = ($_POST['machine_type'] == "live_answer_only");

    $allow_ivr = !empty($_POST['allow_ivr']);
    $ivr_key = $_POST['ivr_key'];
    $ivr_phone_number = $_POST['ivr_phone_number'];

    //if files are uploaded
    if (isset($_FILES)):

        //Creating uploads directory if not already exists
        if (!file_exists(dirname(__FILE__) . '/uploads'))
            mkdir(dirname(__FILE__) . '/uploads');

        //Looping over each file
        foreach ($_FILES as $file_key => $file_details):
            //If no error in currently iterated file
            if ($file_details['error'] == 0):
                //Moving it from temporary location to stable one
                if (move_uploaded_file($file_details['tmp_name'], dirname(__FILE__) . '/uploads/' . $file_details['name'])):
                    //Storing the web accessible URI in variable defined for it dynamically
                    ${$file_key} = dirname(s8_get_current_webpage_uri()) . '/uploads/' . $file_details['name'];
                    ${$file_key . '_type'} = $file_details['type'];
                endif;
            endif;
        endforeach;

    endif;

    /**
     * Retrieving the contacts of list
     * @var array Multidimensional array cntaining cotacts data.
     */
    $list_contacts = ad_advb_cl_get_list_contacts($ad_vb_contacts_list_idx);

    //Merging the contacts into single array
    foreach ($list_contacts as $single_contact_data):
        $single_set_contacts[] = $single_contact_data['phone_number'];
    endforeach;

    //If list submitted is not blank
    if (s8_is_str_blank($ad_vb_contacts_list_idx)):

        //This will print the failure message
        $response .= 'You have to select atleast one contact list in order to proceed.';
        $response_type = 'failure';

    endif;

    if($ad_vb_ac_when_to_run=="sequence"){
        if(!is_numeric($_POST['seq_days_after']) || $_POST['seq_days_after']<0){
            $response .= "Please enter a valid number for 'Days After Contact Subscribed'.<br/>";
            $response_type = "failure";
        }
        if($_POST['delay_type'] == "send_at" && ($_POST['seq_hour']=="" || $_POST['seq_minute']=="")) {
            $response .= "Please enter the time to send the sequence.<br/>";
            $response_type = "failure";
        }
        if($_POST['delay_type'] == "delay" && $_POST['seq_delay']==""){
            $response .= "Please select the delay to send the sequence.<br/>";
            $response_type = "failure";
        }
        if($_POST['seq_name']==""){
            $response .= "Please enter a name for the sequence.<br/>";
            $response_type = "failure";
        }
    }

    if (!is_array($single_set_contacts) || $single_set_contacts == NULL):
        $response .= 'To ("Schedule" or "Run") Voice Broadcast selected list should have at least one contact in it. <br/>';
        $response_type = 'failure';
    endif;

    if (s8_is_str_blank($ad_vb_ac_live_answer_content) && $broadcast_type == "voice") :
        $response .= 'Please add Live Answer.<br/>';
        $response_type = 'failure';
        $ad_vb_ac_live_answer_content = '';
    endif;

    if (s8_is_str_blank($ad_vb_ac_voicemail_message_content) && $broadcast_type == "voice"):
        $response .= 'Please add Voicemail Message.<br/>';
        $response_type = 'failure';
        $ad_vb_ac_voicemail_message_content = '';
    endif;

    if(s8_is_str_blank($sms_message) && $broadcast_type == "sms")
        $response_type = 'failure';

    if(s8_is_str_blank($ivr_phone_number) && $allow_ivr) {
        $response .= 'Please enter phone number to be dialed.<br/>';
        $response_type = 'failure';
    }

    $list_details = $db->getContactListById($ad_vb_contacts_list_idx);

    if($list_details['vb_opt_out_key'] == $ivr_key && $broadcast_type == "voice") {
        $response .= 'Broadcast IVR key should be different than contact list opt-out key.<br/>';
        $response_type = 'failure';
    }

    //If replacable file is not uploaded and this is not a first try
    //We are loading the file previously tried operation
    //The same is for voicemail message mp3 too

    if (!s8_is_str_blank($phone_number) && !s8_is_str_blank($ad_vb_campaign_name) && $response_type != 'failure'):

        if($ad_vb_ac_when_to_run=="sequence"){
            $thetype = "AutoResponder";
        }else{
            if($broadcast_type=="voice")
                $thetype="VoiceBroadcast";
            else
                $thetype="SMSBroadcast";
        }

        //Defining the voice bradcast campaigns table column name
        $campaign_columns_data = array(
            'campaign_name' => $ad_vb_campaign_name,
            'phone_number' => $phone_number,
            'live_answer_content' => isset($ad_vb_ac_live_answer_content)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_live_answer_content:"",
            'live_answer_type' => isset($ad_vb_ac_live_answer_type)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_live_answer_type:"",
            'live_answer_voice' => isset($ad_vb_ac_live_answer_voice)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_live_answer_voice:"",
            'live_answer_language' => isset($ad_vb_ac_live_answer_language)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_live_answer_language:"",
            'voicemail_message_content' => isset($ad_vb_ac_voicemail_message_content)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message_content:"",
            'voicemail_message_type' => isset($ad_vb_ac_voicemail_message_type)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message_type:"",
            'voicemail_message_voice' => isset($ad_vb_ac_voicemail_message_voice)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message_voice:"",
            'voicemail_message_language' => isset($ad_vb_ac_voicemail_message_language)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message_language:"",
            'calls_status' => ($ad_vb_ac_when_to_run == 'now' ? 'NA' : $ad_vb_ac_when_to_run),
            'progress' => $ad_vb_ac_when_to_run=="sequence"?"Active":'NA',
            'last_ran' => 'NA',
            'when_to_run' => ($ad_vb_ac_when_to_run == 'scheduled' ? $ad_vb_ac_scheduled_date : $ad_vb_ac_when_to_run),
            'user' => @$_SESSION['user_id'],
            'shared' => isset($_REQUEST['ad_vb_shared'])? 1:0,
            'type' => $thetype,
            'sms_message' => isset($sms_message)&&$ad_vb_ac_when_to_run!="sequence"? $sms_message:"",
            'list_id' => $_POST['ad_vb_ac_contact_list'],
            "voicemail_only" => $voicemail_only,
            "live_answer_only" => $live_answer_only,
            "allow_ivr" => $allow_ivr,
            "ivr_key" => $ivr_key,
            "ivr_phone_number" => $ivr_phone_number
        );

        //Setting the proceed variable to TRUE
        $proceed = true;

        //If campaign is scheduled
        if ($ad_vb_ac_when_to_run == 'scheduled'):
            //Adding a cron task via API to run our script on scheduled interval
            $cron_response = ad_add_cron_task(date('Y-m-d H:', $ad_vb_ac_scheduled_date).($_POST['ad_vb_ac_sd_minute'] < 10 ? '0' . $_POST['ad_vb_ac_sd_minute'] : $_POST['ad_vb_ac_sd_minute']).":00", dirname(s8_get_current_webpage_uri()) . '/ad_cron.php?action=process_campaigns');

            //If cron is failed to add, setting the proceed variable to fales
            if (@$cron_response->result != 'success')
                $proceed = FALSE;

        endif;

        //If proceed variable to set to true
        if ($proceed == TRUE):

            //Adding the voice broadcast campaign into the system and retrieving its Unique ID from system
            $insert_status = ad_vb_add_campaign($campaign_columns_data);

            //If insert operation executed successfully
            if ($insert_status != FALSE):

                //Storing the campaign ID in custom variable
                $camp_id = $insert_status;

                //Adding contacts in system DB
                ad_advb_add_contacts('vb', $camp_id, $single_set_contacts);

                //Now, Updating the contact details too
                foreach ($list_contacts as $single_contact_data):
                    ad_advb_update_contact_details(ad_advb_get_contact_idx('vb', $camp_id, $single_contact_data['phone_number']), $single_contact_data);
                endforeach;

                //If voice broadcast is set to run now
                if ($ad_vb_ac_when_to_run == 'now'):

                    //Processing the voice broadcast campaign
                    ad_vb_process_voice_broadcast_campaign($camp_id, dirname(s8_get_current_webpage_uri()));

                endif;

                if ($_POST['delay_type'] == "send_at") {
                    $time = $_POST['seq_hour'].":".$_POST['seq_minute']." ".$_POST['seq_period'];
                    $main_time = $time;
                    if (!empty($_POST['seq_hour']) && !empty($_POST['seq_minute'])) {
                        $this_date = new DateTime("now", new DateTimeZone($TIMEZONE));
                        $this_date->modify($time);
                        $this_date->setTimezone(new DateTimeZone("UTC"));
                        $time = $this_date->format("g:i A");
                    }

                    $delay = "";
                }
                elseif ($_POST['delay_type'] == "delay") {
                    $date = new DateTime($access[0], new DateTimeZone("UTC"));
                    $time_return = strtotime($date->format("Y-m-d h:iA"));
                    $time_return = $time_return + $_POST["seq_delay"];

                    $main_time = date("g:i A", $time_return);
                    $time = $main_time;
                    $delay = $_POST["seq_delay"];
                }

                if($ad_vb_ac_when_to_run=="sequence"){
                    $db = new DB();
                    $stmt = $db->customExecute("INSERT INTO ad_vb_sequences (campaign_id,type,name,days_after,time,delay,sms_message,live_answer_content,live_answer_type,live_answer_voice,live_answer_language,voicemail_message_content,voicemail_message_type,voicemail_message_voice,voicemail_message_language) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    if($stmt->execute(array(
                        $camp_id,
                        $broadcast_type=="voice"?"VoiceBroadcast":"SMSBroadcast",
                        $_POST['seq_name'],
                        $_POST['seq_days_after'],
                        $main_time,
                        $delay,
                        $_POST['sms_message'],
                        $ad_vb_ac_live_answer_content,
                        $ad_vb_ac_live_answer_type,
                        $ad_vb_ac_live_answer_voice,
                        $ad_vb_ac_live_answer_language,
                        $ad_vb_ac_voicemail_message_content,
                        $ad_vb_ac_voicemail_message_type,
                        $ad_vb_ac_voicemail_message_voice,
                        $ad_vb_ac_voicemail_message_language
                    ))){
                        $sequence_id = $db->lastInsertId();

                        $date_now = new DateTime("now", new DateTimeZone("UTC"));

                        $stmt = $db->customExecute("SELECT contact_idx,date_added FROM ad_advb_cl_link WHERE list_idx = ?");
                        $stmt->execute(array($_POST['ad_vb_ac_contact_list']));
                        $contact_ids = $stmt->fetchAll(PDO::FETCH_OBJ);
                        if(is_array($contact_ids))
                            foreach($contact_ids as $contact_id){
                                $stmt = $db->customExecute("SELECT * FROM ad_advb_cl_contacts WHERE idx = ?");
                                $stmt->execute(array($contact_id->contact_idx));
                                $contact = $stmt->fetch(PDO::FETCH_OBJ);

                                if(is_object($contact)){
                                    $date_added = new DateTime($contact_id->date_added, new DateTimeZone("UTC"));
                                    $date_added->modify("+".$_POST['seq_days_after']." days");
                                    $date_added->modify($time);
                                    //echo $date_added->format(Util::STANDARD_LOG_DATE_FORMAT)." - ".$date_now->format(Util::STANDARD_LOG_DATE_FORMAT)."\n";

                                    if($date_added>$date_now){
                                        $scheduled_date = $date_added;
                                        $cron_date = $scheduled_date->format(Util::STANDARD_MYSQL_DATE_FORMAT);
                                        $scheduled_date->setTimezone(new DateTimeZone($TIMEZONE));

                                        $stmt = $db->customExecute("INSERT INTO ad_vb_sequence_schedule (sequence_id,cron_id,contact_id,`date`) VALUES (?,'',?,?)");
                                        $stmt->execute(array($sequence_id,$contact->idx,$scheduled_date->format(Util::STANDARD_MYSQL_DATE_FORMAT)));
                                        $schedule_id = $db->lastInsertId();

                                        $retn = @ad_add_cron_task($cron_date,dirname(s8_get_current_webpage_uri()) . '/ad_cron.php?action=process_sequence&schedule_id='.
                                            $schedule_id);

                                        if(@$retn->result == "success"){
                                            $cb = $retn->data;
                                            $stmt = $db->customExecute("UPDATE ad_vb_sequence_schedule SET cron_id = ? WHERE id = ?");
                                            $stmt->execute(array($cb->id,$schedule_id));
                                        }
                                    }
                                }
                            }

                        $response .= 'Successfully created autoresponder. <a href="ad_vb_sequences_edit.php?campaign_id='.$camp_id.'">Click here</a> to proceed.';
                        $response_type = 'success';
                    }else{
                        $response .= "Failed to create autoresponder.";
                        $response_type = 'failure';
                    }
                }else{
                    //Setting the success message to be displayed on screen
                    $response .= 'Successfully created campaign. <a href="ad_vb_campaigns.php">Click here</a> to proceed.';
                    $response_type = 'success';
                }
            else:

                $response = 'Campaign name already exists! Please use another.';
                $response_type = 'failure';

            endif;

        else:

            //Setting the success message to be displayed on screen
            $response .= '
                Campaign creation failed. System failure occured 
                while trying to schedule the campaign.
                ';
            $response_type = 'failure';

        endif;

    else:

        //Adding failure message that will be displayed on screen
        //If campaign name is not blank
        if (s8_is_str_blank($ad_vb_campaign_name))
            $response .= 'Campaign name not provided.<br/>';
        //If phone number is not blank
        if (s8_is_str_blank($phone_number))
            $response .= 'Phone number is not selected.<br/>';
        //If live answer mp3 file is not blank
        if (s8_is_str_blank($ad_vb_ac_live_answer_content) && $broadcast_type == "voice")
            $response .= 'Live Answer MP3 is not provided.<br/>';
        //If voicemail message mp3 file is not null
        if (s8_is_str_blank($ad_vb_ac_voicemail_message_content) && $broadcast_type == "voice")
            $response .= 'Voicemail Message MP3 is not provided.<br/>';
        if(s8_is_str_blank($sms_message) && $broadcast_type=="sms")
            $response .= 'SMS Message is not provided.<br/>';

        //Setting the response type to failure as all the above messages are failure ones.
        $response_type = 'failure';

    endif;

endif;

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
        <style type="text/css">
            #token_list {
                height: 152px;
                margin-left: 8px;
                width: 21%;
                margin-bottom: 7px;
                padding: 5px;
                background: #fefefe;
                border-radius: 3px;
                border: 1px solid #bbb;
                font-family: Helvetica,"Lucida Grande", Verdana, sans-serif;
                font-size: 14px;
                color: #333;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                outline: none;
            }
            .ui-widget-content {border-radius: 5px;}

            .ui-widget-content a {    position: absolute;
                right: 10px;
                top: 5px;}
        </style>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                <!--Initializing the HTML part that will display the create new campaign page-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Messages: Add Campaign</h2>
                        <ul>
                            <li><a href="ad_vb_add_campaign.php">Add a Campaign</a></li>
                            <li><a href="ad_vb_campaigns.php">Campaigns List</a></li>
                            <li><a href="ad_contactlist_log.php">Contacts</a></li>
                            <li><a href="ad_vb_logs.php">Logs</a></li>
                        </ul>
                    </div>		<!-- .block_head ends -->
                    <div class="block_content">
                      <!-- <div class="message errormsg"><p>An error message goes here</p></div>
                        <div class="message success"><p>A success message goes here</p></div>
                        <div class="message info"><p>An informative message goes here</p></div>
                        <div class="message warning"><p>A warning message goes here</p></div>-->
                        <?php
                        if ($response_type == 'success'):
                            ?>
                            <div class="message success"><p><?php echo $response; ?></p></div>
                            <?php
                        elseif ($response_type == 'failure'):
                            ?>
                            <div class="message errormsg"><p><?php echo $response; ?></p></div>
                            <?php
                        endif;

                        if ($response_type != 'success'):
                            ?>
                            <form enctype="multipart/form-data" action="" method="post">
                                <p>
                                    <label>Campaign Name:</label><br />
                                    <input type="text" class="text big" name="ad_vb_campaign_name" value="<?php echo isset($_POST['ad_vb_campaign_name']) ? Util::escapeString($_POST['ad_vb_campaign_name']) : ''; ?>" />
                                </p>
                                <p>
                                    <input style="display: inline-block !important;" type="checkbox" name="ad_vb_shared" id="ad_vb_shared"<?php if(isset($_POST['ad_vb_shared'])) { echo " checked";} ?>/> <label for="ad_vb_shared">Share this campaign with my companies.</label>
                                </p>
                                <p>
                                    <label>Phone Number:</label><br />
                                    <select name="ad_vb_ac_phone_number" class="styled">
                                        <option value="">Select Number</option>
                                        <?php
                                        $numbers = $db->getCompanyNum($_SESSION['sel_co']);
                                        //echo "<pre>";print_r($numbers);
                                        for ($i = 0; $i <= count($numbers) - 1; $i++) {
                                            $num = $numbers[$i];
                                            ?>
                                            <option <?php echo isset($_POST['ad_vb_ac_phone_number']) && $_POST['ad_vb_ac_phone_number'] == $num ? ' selected="selected" ' : ''; ?> value="<?php echo $num; ?>"><?php echo $num; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </p>

                                <br/>

                                <p>
                                    <?php
                                    $ad_advb_cl_contact_lists = ad_advb_cl_get_contact_lists();
                                    ?>
                                    <label>Select contact list: </label><br/>
                                    <select name="ad_vb_ac_contact_list" class="styled">
                                        <option value="">Select List</option>
                                        <?php
                                        foreach ($ad_advb_cl_contact_lists as $single_contact_list_data):
                                            ?>
                                            <option <?php echo (isset($_POST['ad_vb_ac_contact_list']) && $_POST['ad_vb_ac_contact_list'] == $single_contact_list_data['idx'] ? ' selected="selected" ' : ''); ?> value="<?php echo $single_contact_list_data['idx']; ?>"><?php echo Util::escapeString($single_contact_list_data['list_name']); ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </p>

                                <p style="">
                                    <label>Broadcast Type:</label><br />
                                    <input type="radio" style="margin-left: 3px;" name="ad_vb_broadcast_type" value="voice" id="ad_vb_broadcast_type_voice"<?php if(@$broadcast_type=="voice" || @$broadcast_type=="") echo " checked"; ?> class="checkbox left" /> <label class="marginleft10 left lh0" for="ad_vb_broadcast_type_voice">Voice Broadcast</label>
                                    <br/>
                                    <input type="radio" style="margin-left: 3px;" name="ad_vb_broadcast_type" value="sms" id="ad_vb_broadcast_type_sms"<?php if(@$broadcast_type=="sms") echo " checked"; ?> class="checkbox left" /> <label class="marginleft10 left lh0" for="ad_vb_broadcast_type_sms">SMS Broadcast</label>
                                </p>

                                <div class="fileupload">
                                    <label>Live Answer:</label><br />
                                    
                                   <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                    <input type="hidden" class="content" name="live_answer_content" value="<?php echo $ad_vb_ac_live_answer_content; ?>" />
                                    <input type="hidden" class="type" name="live_answer_type" value="<?php echo $ad_vb_ac_live_answer_type; ?>" />
                                    <input type="hidden" class="voice" name="live_answer_voice" value="<?php echo $ad_vb_ac_live_answer_voice; ?>" />
                                    <input type="hidden" class="language" name="live_answer_language" value="<?php echo $ad_vb_ac_live_answer_language; ?>" />
                                    <div class="ivr-Menu-selector" style="display: block">
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($ad_vb_ac_live_answer_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($ad_vb_ac_live_answer_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($ad_vb_ac_live_answer_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($ad_vb_ac_live_answer_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($ad_vb_ac_live_answer_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($ad_vb_ac_live_answer_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($ad_vb_ac_live_answer_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($ad_vb_ac_live_answer_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                        </div>
                                      </div>
                                      <div class="ivr-Menu-editor ">
                                        <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                          <div class="ivr-Menu-read-text" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <br>
                                            <div>
                                              <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                                <label class="field-label">
                                                    <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($ad_vb_ac_live_answer_type == 'Text')? $ad_vb_ac_live_answer_content:''); ?></textarea>

                                                    <?php
                                                    $voice = (($ad_vb_ac_live_answer_type == 'Text')? $ad_vb_ac_live_answer_voice:'');
                                                    $language = (($ad_vb_ac_live_answer_type == 'Text')? $ad_vb_ac_live_answer_language:'');
                                                    ?>
                                                    <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                    <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                      <?php
                                                      echo Util::getTwilioVoices($voice);
                                                      ?>
                                                    </select>

                                                    <br clear="all" />

                                                    <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                    <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                      <?php
                                                      echo Util::getTwilioLanguages($voice, $language);
                                                      ?>
                                                    </select>

                                                    <br clear="all" /><br />

                                                    <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                    <script type="text/javascript">
                                                      $(document).ready(function() {
                                                        $("#voice").trigger("change");
                                                      });
                                                    </script>

                                                    <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                                </label>
                                              </fieldset>
                                            </div>
                                            <br>
                                            <br>
                                          </div>
                                          <div class="ivr-audio-upload" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>
                                                
                                                <span class="title" <?php if ( $ad_vb_ac_live_answer_type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                                <?php 
                                          echo (($ad_vb_ac_live_answer_type == 'Audio')? $ad_vb_ac_live_answer_content:''); ?>
                                                </strong></span> <br>
                                                
                                                
                                                <span class="title">Click to select a file: </span>
                                                <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                                <span class="title" id="statusUpload">&nbsp;</span>
                                              </div>                                                   
                                            
                                            </div>
                                          </div>
                                          <div class="ivr-mp3-url" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>           
                                                
                                                <span class="title">
                                                  <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($ad_vb_ac_live_answer_type == 'MP3_URL')? $ad_vb_ac_live_answer_content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                                  <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                                </span>

                                                <br /><br />

                                                <span class="title" <?php if ( $ad_vb_ac_live_answer_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                                <?php 
                                          echo (($ad_vb_ac_live_answer_type == 'MP3_URL')? $ad_vb_ac_live_answer_content:''); ?>
                                                </strong></span>

                                              </div>
                                            
                                            
                                            </div>
                                          </div>
                                          <div class="ivr-record-audio" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>           
                                                
                                                Caller ID:

                                                <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                  <option value="">Select number</option>
                                                  <?php
                                                  $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                                  foreach ($numbers as $number) {
                                                    ?>
                                                      <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                    <?php
                                                  }
                                                  ?>
                                                </select>

                                                Your phone number:

                                                <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                                <br /><br />

                                                <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                                <br /><br />

                                                <span class="title" <?php if ( $ad_vb_ac_live_answer_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                                    <?php if ($ad_vb_ac_live_answer_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($ad_vb_ac_live_answer_content, 'sm'); } ?>
                                                </span>

                                              </div>
                                            
                                            
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </fieldset>
                                </div>

                                <div class="fileupload">
                                    <label>Voicemail Message:</label><br />
                                    
                                    <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                        <input type="hidden" class="content" name="voicemail_message_content" value="<?php echo $ad_vb_ac_voicemail_message_content; ?>" />
                                        <input type="hidden" class="type" name="voicemail_message_type" value="<?php echo $ad_vb_ac_voicemail_message_type; ?>" />
                                        <input type="hidden" class="voice" name="voicemail_message_voice" value="<?php echo $ad_vb_ac_voicemail_message_voice; ?>" />
                                        <input type="hidden" class="language" name="voicemail_message_language" value="<?php echo $ad_vb_ac_voicemail_message_language; ?>" />
                                        <div class="ivr-Menu-selector" style="display: block">
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <?php if ($ad_vb_ac_voicemail_message_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                              <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($ad_vb_ac_voicemail_message_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <?php if ($ad_vb_ac_voicemail_message_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                              <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($ad_vb_ac_voicemail_message_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <?php if ($ad_vb_ac_voicemail_message_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                              <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($ad_vb_ac_voicemail_message_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                            </div>
                                            <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                              <?php if ($ad_vb_ac_voicemail_message_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                              <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($ad_vb_ac_voicemail_message_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                            </div>
                                          </div>
                                          <div class="ivr-Menu-editor ">
                                            <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                              <div class="ivr-Menu-read-text" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <br>
                                                <div>
                                                  <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                                    <label class="field-label">
                                                        <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($ad_vb_ac_voicemail_message_type == 'Text')? $ad_vb_ac_voicemail_message_content:''); ?></textarea>

                                                        <?php
                                                        $voice = (($ad_vb_ac_voicemail_message_type == 'Text')? $ad_vb_ac_voicemail_message_voice:'');
                                                        $language = (($ad_vb_ac_voicemail_message_type == 'Text')? $ad_vb_ac_voicemail_message_language:'');
                                                        ?>
                                                        <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                        <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                          <?php
                                                          echo Util::getTwilioVoices($voice);
                                                          ?>
                                                        </select>

                                                        <br clear="all" />

                                                        <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                        <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                          <?php
                                                          echo Util::getTwilioLanguages($voice, $language);
                                                          ?>
                                                        </select>

                                                        <br clear="all" /><br />

                                                        <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                        <script type="text/javascript">
                                                          $(document).ready(function() {
                                                            $("#voice").trigger("change");
                                                          });
                                                        </script>

                                                        <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                                    </label>
                                                  </fieldset>
                                                </div>
                                                <br>
                                                <br>
                                              </div>
                                              <div class="ivr-audio-upload" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>
                                                    
                                                    <span class="title" <?php if ( $ad_vb_ac_voicemail_message_type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                                    <?php 
                                              echo (($ad_vb_ac_voicemail_message_type == 'Audio')? $ad_vb_ac_voicemail_message_content:''); ?>
                                                    </strong></span> <br>
                                                    
                                                    
                                                    <span class="title">Click to select a file: </span>
                                                    <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                                    <span class="title" id="statusUpload">&nbsp;</span>
                                                  </div>                                                   
                                                
                                                </div>
                                              </div>
                                              <div class="ivr-mp3-url" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>           
                                                    
                                                    <span class="title">
                                                      <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($ad_vb_ac_voicemail_message_type == 'MP3_URL')? $ad_vb_ac_voicemail_message_content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                                      <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                                    </span>

                                                    <br /><br />

                                                    <span class="title" <?php if ( $ad_vb_ac_voicemail_message_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                                    <?php 
                                              echo (($ad_vb_ac_voicemail_message_type == 'MP3_URL')? $ad_vb_ac_voicemail_message_content:''); ?>
                                                    </strong></span>

                                                  </div>
                                                
                                                
                                                </div>
                                              </div>
                                              <div class="ivr-record-audio" style="display: none;">
                                                <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                <div class="swfupload-container">
                                                  <div class="explanation"> <br>           
                                                    
                                                    Caller ID:

                                                    <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                      <option value="">Select number</option>
                                                      <?php
                                                      $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                                      foreach ($numbers as $number) {
                                                        ?>
                                                          <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                    </select>

                                                    Your phone number:

                                                    <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                                    <br /><br />

                                                    <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                                    <br /><br />

                                                    <span class="title" <?php if ( $ad_vb_ac_voicemail_message_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                                        <?php if ($ad_vb_ac_voicemail_message_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($ad_vb_ac_voicemail_message_content, 'sm'); } ?>
                                                    </span>

                                                  </div>
                                                
                                                
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </fieldset>
                                </div>

                                <script type="text/javascript">
                                    $('[name="ad_vb_broadcast_type"], #ad_vb_broadcast_type_sms, #ad_vb_broadcast_type_voice').click(function() {
                                        if ($('#ad_vb_broadcast_type_sms').is(':checked')) {
                                            $('.ad_vb_ac_sms_message').slideDown();
                                            $('.fileupload').slideUp();
                                        } else {
                                            $('.ad_vb_ac_sms_message').slideUp();
                                            $('.fileupload').slideDown();
                                        }
                                    });
                                    $(document).ready(function(){
                                        if ($('#ad_vb_broadcast_type_sms').is(':checked')) {
                                            $('.ad_vb_ac_sms_message').slideDown();
                                            $('.fileupload').slideUp();
                                        } else {
                                            $('.ad_vb_ac_sms_message').slideUp();
                                            $('.fileupload').slideDown();
                                        }
                                    });
                                </script>

                                <p class="ad_vb_ac_sms_message" style="display:none;">
                                    <label>Message:</label><br />
                                    <textarea name="sms_message" maxlength="1600" style="width: 700px; max-width: 700px;"><?php echo htmlentities(@$_POST['sms_message']); ?></textarea>
                                    <select id="token_list" name="ad_ad_tokens" size="10">
                                        <option value="[FirstName]">[FirstName]</option>
                                        <option value="[LastName]">[LastName]</option>
                                        <option value="[Email]">[Email]</option>
                                        <option value="[Phone]">[Phone]</option>
                                        <option value="[Address]">[Address]</option>
                                        <option value="[City]">[City]</option>
                                        <option value="[State]">[State]</option>
                                        <option value="[Zip]">[Zip]</option>
                                        <option value="[Website]">[Website]</option>
                                        <option value="[Business]">[Business]</option>
                                    </select>
                                </p>

                                <br />

                                <div class="fileupload">
                                    <label><input type="radio" name="machine_type" value="voicemail_only" style="display: inline-block !important;" <?php if ($voicemail_only) { ?>checked="checked"<?php } ?> /> Voicemail Only <span style="font-size: 11px; color: #939393; font-weight: normal;">(When this option is selected, your Voice Broadcast message will be delivered when ACT detects that a voicemail.)</span></label><br />
                                    <label><input type="radio" name="machine_type" value="live_answer_only" style="display: inline-block !important;" <?php if ($live_answer_only) { ?>checked="checked"<?php } ?> /> Live Answer Only <span style="font-size: 11px; color: #939393; font-weight: normal;">(When this option is selected, your message will only play when ACT detects that a live person has answered the call. No voicemail will be left.)</span></label><br />
                                    

                                    <div style="float: left; padding-top: 3px;">
                                        <label><input type="checkbox" name="allow_ivr" id="allow_ivr" style="display: inline-block !important;" <?php if ($allow_ivr) { ?>checked="checked"<?php } ?> /> Allow customer to press </label>
                                    </div>

                                    <div style="float: left;">
                                        <select class="styled" name="ivr_key" style="width: 50px;" onchange="$('#allow_ivr').prop('checked', true);">
                                            <option value="1" <?php if ($ivr_key == "1") { ?>selected="selected"<?php } ?>>1</option>
                                            <option value="2" <?php if ($ivr_key == "2") { ?>selected="selected"<?php } ?>>2</option>
                                            <option value="3" <?php if ($ivr_key == "3") { ?>selected="selected"<?php } ?>>3</option>
                                            <option value="4" <?php if ($ivr_key == "4") { ?>selected="selected"<?php } ?>>4</option>
                                            <option value="5" <?php if ($ivr_key == "5") { ?>selected="selected"<?php } ?>>5</option>
                                            <option value="6" <?php if ($ivr_key == "6") { ?>selected="selected"<?php } ?>>6</option>
                                            <option value="7" <?php if ($ivr_key == "7") { ?>selected="selected"<?php } ?>>7</option>
                                            <option value="8" <?php if ($ivr_key == "8") { ?>selected="selected"<?php } ?>>8</option>
                                            <option value="9" <?php if ($ivr_key == "9") { ?>selected="selected"<?php } ?>>9</option>
                                            <option value="0" <?php if ($ivr_key == "0") { ?>selected="selected"<?php } ?>>0</option>
                                            <option value="*" <?php if ($ivr_key == "*") { ?>selected="selected"<?php } ?>>*</option>
                                        </select>
                                    </div>

                                    <div style="float: left; padding-top: 5px; font-weight: bold; padding-right: 6px;">
                                        to dial
                                    </div>

                                    <div style="float: left;">
                                        <input type="text" name="ivr_phone_number" value="<?php echo $ivr_phone_number; ?>" class="text small" style="width: 120px;" placeholder="Phone#" onkeyup="$('#allow_ivr').prop('checked', !(this.value == ''));" />
                                    </div>

                                    <div style="clear: both;"></div>
                                    <br />
                                </div>

                                <br />

                                <p>
                                    <label>When To Run:</label><br />
                                    <input name="ad_vb_ac_when_to_run" style="margin-left: 3px;" type="radio" value="now" class="radio left" <?php echo isset($_POST['ad_vb_ac_when_to_run']) ? ($_POST['ad_vb_ac_when_to_run'] == 'now' ? ' checked="checked" ' : '') : ' checked="checked" '; ?> id="cbdemo3" /> <label class="marginleft10 left lh0" for="cbdemo3">Now</label>

                                    <input name="ad_vb_ac_when_to_run" type="radio" value="scheduled" class="radio left" id="cbdemo4" <?php echo isset($_POST['ad_vb_ac_when_to_run']) && $_POST['ad_vb_ac_when_to_run'] == 'scheduled' ? ' checked="checked" ' : ''; ?> /> <label class="marginleft10 left lh0" for="cbdemo4">Schedule</label>

                                    <input name="ad_vb_ac_when_to_run" type="radio" style="display: none;" value="sequence" class="radio left" id="cbdemo5" <?php echo isset($_POST['ad_vb_ac_when_to_run']) && $_POST['ad_vb_ac_when_to_run'] == 'sequence' ? ' checked="checked" ' : ''; ?> /> <label class="marginleft10 left lh0" for="cbdemo5">Sequence</label>

                                    <input name="ad_vb_ac_when_to_run" type="radio" value="draft" id="cbdemo6" class="radio left" <?php echo isset($_POST['ad_vb_ac_when_to_run']) && $_POST['ad_vb_ac_when_to_run'] == 'draft' ? ' checked="checked" ' : ''; ?> /> <label class="marginleft10 left lh0" for="cbdemo6">Save As Draft</label>
                                </p>

                                <script type="text/javascript">
                                    if ($ === undefined) {
                                        $ = jQuery;
                                    }

                                    /**
                                     * This function manages the display property of WHEN TO RUN
                                     * section of form.
                                     * @param {object} selector The js object referencing to selector
                                     * to perform various JS operations
                                     * @returns {undefined} Returns nothing.
                                     */
                                    function manage_ui_when_to_run(selector) {

                                        //Hiding every WHEN TO RUN extra settings section
                                        $('.ad_vb_ac_when_to_run_extra').hide();

                                        //If WHEN TO RUN value selected is schedules
                                        if ($(selector).attr('value') === 'scheduled' && $(selector).is(':checked')) {
                                            //Displaying scheduled data settings part
                                            $('#ad_vb_ac_scheduled_date').slideDown();

                                            //If WHEN TO RUN value selected is sequence
                                        }else if ($(selector).attr('value') === 'sequence' && $(selector).is(':checked')) {
                                            $('#ad_vb_ac_sequence_date').slideDown();
                                        }

                                    }

                                    //On document load
                                    //Updating the UI
                                    manage_ui_when_to_run($('[name=ad_vb_ac_when_to_run]'));

                                    //When any WHEN to RUN radio is clicked
                                    $('[name=ad_vb_ac_when_to_run]').click(function() {
                                        //Updating the UI
                                        manage_ui_when_to_run(this);
                                    });

                                    setInterval(function() {
                                        $.get("ad_ajax.php?action=get_system_time", function(ajax_response) {
                                            $('.ad_vb_sys_time').text(ajax_response);
                                        });
                                    }, 4000);
                                </script>

                                <div class="ad_vb_ac_when_to_run_extra" id="ad_vb_ac_sequence_date" style="<?php echo!isset($_POST['ad_vb_ac_when_to_run']) || $_POST['ad_vb_ac_when_to_run'] != 'sequence' ? 'display: none;' : ''; ?>">
                                    <br/>
                                <table>
                                    <tr>
                                        <td><label for="seq_name">Sequence Name: </label></td>
                                        <td><input type="text" class="text small" style="display: inline-block !important; width: 110px; text-align: center;" id="seq_name" name="seq_name" value="<?php echo @$_POST['seq_name']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td><label for="seq_days_after">Days After Contact Subscribed: </label></td>
                                        <td><input type="text" class="text small" style="display: inline-block !important; width: 55px; text-align: center;" id="seq_days_after" name="seq_days_after" value="<?php echo @$_POST['seq_days_after']; ?>" /></td>
                                    </tr>
                                </table>
                                    <label style="display: inline-block; width: 75px;"><input type="radio" name="delay_type" checked="checked" value="send_at" style="display: inline-block !important;" /> Send At:</label>
                                    <select name="seq_hour" style="display:inline-block">
                                        <option value="">Hour</option>
                                        <?php for($i=1; $i < 13; $i++) {
                                            $selected = ($i == @$_POST['seq_hour']) ? 'selected="selected"' : '';
                                            echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                                        } ?>
                                    </select>
                                    :
                                    <select name="seq_minute" style="display:inline-block">
                                        <option value="">Minute</option>
                                        <?php for($i=0; $i < 60; $i++) {
                                            $s = ($i<10 ? "0".$i : $i); 
                                            $selected = ($s == @$_POST['seq_minute']) ? 'selected="selected"' : '';
                                            echo '<option value="'.$s.'" '.$selected.'>'.$s.'</option>';
                                        } ?>
                                    </select>
                                    &nbsp;
                                    <select name="seq_period" style="display:inline-block">
                                        <option value="AM">AM</option>
                                        <option value="PM" <?php if (@$_POST['seq_period'] == "PM") { ?>selected="selected"<?php } ?>>PM</option>
                                    </select>
                                    <br /><br />
                                    <?php
                                    $delay_type = (isset($_POST['delay_type']) ? $_POST['delay_type'] : "");
                                    $seq_delay = (isset($_POST['seq_delay']) ? $_POST['seq_delay'] : "");
                                    ?>
                                    <label style="display: inline-block; width: 75px;"><input type="radio" name="delay_type" value="delay" <?php if ($delay_type == "delay") { ?>checked="checked"<?php } ?> style="display: inline-block !important;" /> Delay:</label>
                                    <select name="seq_delay" style="display:inline-block"> <!-- Values are in seconds -->
                                        <option value="">Delay</option>
                                        <option value="60" <?php if ($seq_delay == "60") { ?>selected="selected"<?php } ?>>Immediatly</option>
                                        <option value="900" <?php if ($seq_delay == "900") { ?>selected="selected"<?php } ?>>15 Minutes</option>
                                        <option value="1800" <?php if ($seq_delay == "1800") { ?>selected="selected"<?php } ?>>30 Minutes</option>
                                        <option value="3600" <?php if ($seq_delay == "3600") { ?>selected="selected"<?php } ?>>60 Minutes</option>
                                        <option value="5400" <?php if ($seq_delay == "5400") { ?>selected="selected"<?php } ?>>90 Minutes</option>
                                        <option value="7200" <?php if ($seq_delay == "7200") { ?>selected="selected"<?php } ?>>2 Hours</option>
                                        <option value="10800" <?php if ($seq_delay == "10800") { ?>selected="selected"<?php } ?>>3 Hours</option>
                                        <option value="14400" <?php if ($seq_delay == "14400") { ?>selected="selected"<?php } ?>>4 Hours</option>
                                        <option value="18000" <?php if ($seq_delay == "18000") { ?>selected="selected"<?php } ?>>5 Hours</option>
                                        <option value="21600" <?php if ($seq_delay == "21600") { ?>selected="selected"<?php } ?>>6 Hours</option>
                                        <option value="28800" <?php if ($seq_delay == "28800") { ?>selected="selected"<?php } ?>>8 Hours</option>
                                    </select>
                                    <br /><br />
                                </div>

                                <p class="ad_vb_ac_when_to_run_extra" id="ad_vb_ac_scheduled_date" style="<?php echo!isset($_POST['ad_vb_ac_when_to_run']) || $_POST['ad_vb_ac_when_to_run'] != 'scheduled' ? 'display: none;' : ''; ?>">
                                    <label>Schedule date (Current System Time: <span class="ad_vb_sys_time"><?php global $TIMEZONE;
                                        $currenttime = new DateTime("now",new DateTimeZone($TIMEZONE));
                                        echo $currenttime->format('m/d Y g:i A T'); ?></span>):</label><br/>
                                    Select: 
                                    <select name="ad_vb_ac_sd_month">
                                        <option value="">Month</option>
                                        <?php
                                        for ($i = 1; $i <= 12; $i++):
                                            echo '<option ' . (isset($_POST['ad_vb_ac_sd_month']) && $_POST['ad_vb_ac_sd_month'] == $i ? ' selected="selected" ' : '') . '  value="' . $i . '">' . $i . '</option>';
                                        endfor;
                                        ?>
                                    </select>/<select name="ad_vb_ac_sd_day">
                                        <option value="">Day</option>
                                        <?php
                                        for ($i = 1; $i <= 31; $i++):
                                            echo '<option ' . (isset($_POST['ad_vb_ac_sd_day']) && $_POST['ad_vb_ac_sd_day'] == $i ? ' selected="selected" ' : '') . '  value="' . $i . '">' . $i . '</option>';
                                        endfor;
                                        ?>
                                    </select>
                                    &nbsp;
                                    <select name="ad_vb_ac_sd_year">
                                        <option value="">Year</option>
                                        <?php
                                        for ($i = date('Y'); $i <= (date('Y') + 10); $i++):
                                            echo '<option ' . (isset($_POST['ad_vb_ac_sd_year']) && $_POST['ad_vb_ac_sd_year'] == $i ? ' selected="selected" ' : '') . ' value="' . $i . '">' . $i . '</option>';
                                        endfor;
                                        ?>
                                    </select>
                                    &nbsp;
                                    <select name="ad_vb_ac_sd_hour">
                                        <option value="">Hour</option>
                                        <?php
                                        for ($i = 1; $i <= 12; $i++):
                                            echo '<option ' . (isset($_POST['ad_vb_ac_sd_hour']) && $_POST['ad_vb_ac_sd_hour'] == $i ? ' selected="selected" ' : '') . '  value="' . $i . '">' . $i . '</option>';
                                        endfor;
                                        ?>
                                    </select>:<select name="ad_vb_ac_sd_minute">
                                        <option value="">Minute</option>
                                        <?php
                                        for ($i = 0; $i <= 59; $i++):
                                            if (is_int($i / 5)):
                                                echo '<option ' . (isset($_POST['ad_vb_ac_sd_minute']) && $_POST['ad_vb_ac_sd_minute'] == $i ? ' selected="selected" ' : '') . '  value="' . $i . '">' . $i . '</option>';
                                            endif;
                                        endfor;
                                        ?>
                                    </select>
                                    &nbsp;
                                    <select name="ad_vb_ac_sd_period">
                                        <option value="">Period</option>
                                        <option value="AM" <?php echo (isset($_POST['ad_vb_ac_sd_period']) && $_POST['ad_vb_ac_sd_period'] == 'AM' ? ' selected="selected" ' : ''); ?>>AM</option>
                                        <option value="PM" <?php echo (isset($_POST['ad_vb_ac_sd_period']) && $_POST['ad_vb_ac_sd_period'] == 'PM' ? ' selected="selected" ' : ''); ?>>PM</option>
                                    </select>
                                </p>
                                <hr />
                                <p>
                                    <input name="ad_vb_process_settings_form" style="float:left;" type="submit" class="submit small" value="Save" />
                                    <input style="float:left;" type="button" class="submit small" value="Cancel" onclick="window.document.location = 'ad_vb_campaigns.php';" />
                                </p>
                            </form>
                            <?php
                        endif;
                        ?>
                    </div>		<!-- .block_content ends -->
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Exiting the HTML code that was displaying the create new campaign form-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>

        <!--//Notification bar html-->
        <div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
        <script type="text/javascript">
            $.fn.insertAtCaret = function (tagName) {
                return this.each(function(){
                    if (document.selection) {
                        //IE support
                        this.focus();
                        sel = document.selection.createRange();
                        sel.text = tagName;
                        this.focus();
                    }else if (this.selectionStart || this.selectionStart == '0') {
                        //MOZILLA/NETSCAPE support
                        startPos = this.selectionStart;
                        endPos = this.selectionEnd;
                        scrollTop = this.scrollTop;
                        this.value = this.value.substring(0, startPos) + tagName + this.value.substring(endPos,this.value.length);
                        this.focus();
                        this.selectionStart = startPos + tagName.length;
                        this.selectionEnd = startPos + tagName.length;
                        this.scrollTop = scrollTop;
                    } else {
                        this.value += tagName;
                        this.focus();
                    }
                });
            };
            $("#token_list").find("option").on('dblclick', function(){
                insertToken(this.value);
            });

            function insertToken(token){
                $("textarea[name='sms_message']").insertAtCaret(token);
            }
        </script>
        <script type="text/javascript" language="javascript">
            if ($ === undefined) {
                $ = jQuery;
            }

            /**
             * This function will display the notification message on screen.
             * @param {string} message The message to be displayed in notification bar
             * @returns {undefined}
             */
            function ad_display_message(message) {
                $('.ad_notification').html(message);
                $('.ad_notification').slideDown();
                setTimeout(function() {
                    $('.ad_notification').fadeOut(3000);
                }, 3000);

            }

            /**
             * This function will check whether string is blank or not
             * @param {string} str The value to be checked
             * @returns {Boolean} returns boolean TRUE or FALSE based on check performed.
             */
            function ad_is_str_blank(str) {
                if (str === '' || str === ' ' || str === null || str === undefined) {
                    return true;
                } else {
                    return false;
                }
            }

            //When document is ready to attache events to its elements
            $(document).ready(function() {
                //Attaching the function to submit event of text dial
                $('#ad_vb_test_dial, #ad_vb_dial_numbers').submit(function(e) {

                    //Retrieving the caller id
                    var caller_id = $('[name=ad_vb_caller_id]').val();
                    //Retrieving the number to dial
                    if ($(this).attr('id') === 'ad_vb_test_dial') {
                        var to_num = $('[name=ad_vb_test_dial_number]').val();
                    } else {
                        var to_num = $('[name=ad_vb_bulk_dial_numbers]').val();
                    }

                    //CHecking for blank values
                    if (!ad_is_str_blank(caller_id) && !ad_is_str_blank(to_num)) {

                        //Preparing the ajax URL
                        var ajax_url = '<?php echo dirname(s8_get_current_webpage_uri()); ?>/include/ad_auto_dialer_files/ad_ajax.php?to=' + to_num + '&caller_id=' + caller_id + '&userid=<?php echo @$_SESSION['user_id']; ?>';
                        //Firing the AJAX request
                        $.get(ajax_url, function(ajax_response) {

                            //Displaying the notification
                            ad_display_message(ajax_response);

                        });
                    } else {

                        //If blank value detected
                        //Throwing the error on screen
                        ad_display_message('Either caller ID not selected or No number entered.');

                    }

                    //Preventing the default action of form submission
                    e.preventDefault();
                });

            });

            function showAudioText(obj)
            {
                
                
                var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
                audioChoice.hide();
                audioChoice.parent().children('.ivr-Menu-editor').show();
                var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');
                
                if ( obj.id  == 'txt' ) 
                {       
                    
                    subDiv.children('.ivr-Menu-read-text').show();
                    
                    //////////////// only to avoid  file button clickable in text area      
                    $('[name="uploadfile"]').css('z-index','-1');       
                
                }
                else if ( obj.id  == 'upload_mp3' ) {
                    $('[name="uploadfile"]').css('z-index','2147483583');
                    
                    subDiv.children('.ivr-audio-upload').show();    
                    SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');        
                    UploadFile(SubObj); 
                }
                else if ( obj.id  == 'mp3_url' ) {
                    subDiv.children('.ivr-mp3-url').show();
                }
                else if ( obj.id  == 'record_audio' ) {
                    subDiv.children('.ivr-record-audio').show();
                }
                
            }

            function CloseButton(obj)
            {
                var audioChoice = $(obj).closest('.ivr-Menu');
                
                var audioChoiceEditor   = audioChoice.children('.ivr-Menu-editor');
                var audioChoiceSelector = audioChoice.children('.ivr-Menu-selector');
                
                var subDiv  = audioChoiceEditor.children('.ivr-Menu-editor-padding');
                
                audioChoiceSelector.show();
                audioChoiceEditor.hide();
                subDiv.children('.ivr-audio-upload').hide();    
                subDiv.children('.ivr-Menu-read-text').hide();
                subDiv.children('.ivr-mp3-url').hide();
                subDiv.children('.ivr-record-audio').hide();
            }

            function testVoice(voice, language, text) {
                if ($("#test_voice_iframe_wrapper").length == 0) {
                    $("body").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
                                        '<form id="test_voice_form" action="test_voice.php" method="post" target="test_voice_iframe">' +
                                            '<input type="hidden" name="voice" id="voice" />' +
                                            '<input type="hidden" name="language" id="language" />' +
                                            '<input type="hidden" name="text" id="text" />' +
                                            '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                        '</form>' +
                                        '<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
                                    '</form>'
                    );
                }

                $("#test_voice_iframe_wrapper #voice").val(voice);
                $("#test_voice_iframe_wrapper #language").val(language);
                $("#test_voice_iframe_wrapper #text").val(text);
                $('#test_voice_iframe_wrapper #submit').click()
             }

             var closeBtnHtml = '<a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a>';

             function SaveContent(obj, content_type) {
                var thisBlock = $(obj).closest('.ivr-Menu');
                switch (content_type) {
                    case "Text_mail":
                    {
                        var voice_text = $(thisBlock).find('#readtxt_mail').val();
                        var voice = $(thisBlock).find('#voice').val();
                        var language = $(thisBlock).find('#language').val();
                        $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find('.content').val(voice_text);
                        $(thisBlock).find('.type').val("Text");
                        $(thisBlock).find('.voice').val(voice);
                        $(thisBlock).find('.language').val(language);

                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                        $(thisBlock).find('#txt').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        break;
                    }
                    case "MP3_URL":
                    {

                        var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                        $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find('.content').val(mp3_url);
                        $(thisBlock).find('.type').val("MP3_URL");

                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                        $(thisBlock).find('#mp3_url').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        break;
                    }
                }
            }

            function removeSelectedOption(obj) {
                promptMsg('Are you sure ?', function() {
                  var thisBlock = $(obj).closest('.ivr-Menu');
                  $(thisBlock).find(".ttsMwCloseBtn").remove();

                  $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                  $(thisBlock).find('.content').val('');
                  $(thisBlock).find('.type').val('');
                  $(thisBlock).find('.voice').val('');
                  $(thisBlock).find('.language').val('');
                });
            }

            function UploadFile(obj)
            {
                var status = $(obj).closest('.explanation').find('#statusUpload');
                var fileNameStatus = $(obj).closest('.explanation').find('#voicefilename');
                var fileNameStatusWrapper = $(obj).closest('.explanation').find('#voicefilenameWrapper');
                
                new AjaxUpload(obj, {
                    
                    
                    action: 'ad_vb_add_campaign.php?act=uploadMp3',
                    name: 'uploadfile',
                    onSubmit: function(file, ext){
                         if (! (ext && /^(mp3|wma)$/.test(ext))){ 
                            // extension is not allowed 
                            status.text('Only MP3 files are allowed');
                            return false;
                        }
                        (fileNameStatus).html('Uploading...');
                    },
                    onComplete: function(file, response){
                        //On completion clear the status
                        
                        //Add uploaded file to list
                        
                        if(response!="error"){
                            $(fileNameStatusWrapper).css('display' ,'block');
                            $(fileNameStatus).html(response);
                            var thisBlock = $(obj).closest('.ivr-Menu');

                            $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                            $(thisBlock).find('#upload_mp3').addClass('ivr-Menu-Selected');
                            $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                            $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                            $(thisBlock).find('.content').val(response);
                            $(thisBlock).find('.type').val("Audio");
                            $(thisBlock).find('.ivr-Menu-close-button').click();

                            $(thisBlock).find(".ttsMwCloseBtn").remove();
                            $(thisBlock).find('#upload_mp3').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        } else{
                        }
                    }
                });
                    
            }

            var recordingId = "";
            function recordAudio(obj) {
                if ($(obj).val() == "Call Me") {
                    recordingId = createUUID();
                    var record_from = $(obj).parents('.ivr-Menu').find('#record_from').val();
                    var record_to = $(obj).parents('.ivr-Menu').find('#record_to').val();

                    if (record_from == "") {
                        errMsgDialog("Please select Caller ID.");
                        return false;
                    }

                    if (record_to == "") {
                        errMsgDialog("Please enter your phone number.")
                        return false;
                    }

                    $(obj).val('Stop');

                    if ($("#record_audio_iframe_wrapper").length == 0) {
                        $("body").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
                                            '<form id="record_audio_form" action="../record_audio.php" method="post" target="record_audio_iframe">' +
                                                '<input type="hidden" name="recordingId" id="recordingId" />' +
                                                '<input type="hidden" name="record_from" id="record_from" />' +
                                                '<input type="hidden" name="record_to" id="record_to" />' +
                                                '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                            '</form>' +
                                            '<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
                                        '</form>'
                        );
                    }

                    $("#record_audio_iframe_wrapper #recordingId").val(recordingId);
                    $("#record_audio_iframe_wrapper #record_from").val(record_from);
                    $("#record_audio_iframe_wrapper #record_to").val(record_to);
                    $('#record_audio_iframe_wrapper #submit').click()
                }
                else {
                    $(obj).val('Please wait...');
                    $("#record_audio_iframe").contents().find("#disconnectBtn").click();

                    setTimeout(function() {
                        $.post("admin_ajax_handle.php", { func: "GET_RECORDING", recordingId: recordingId },  function(response) {
                            var thisBlock = $(obj).closest('.ivr-Menu');

                            $(thisBlock).find('#recordedAudioSavedWrapper').css('display' ,'block');
                            if (response) {
                                $(thisBlock).find('#recordedAudioSavedWrapper').html(response.playable);
                                
                                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                                $(thisBlock).find('.content').val(response.url);
                                $(thisBlock).find('.type').val("RECORD_AUDIO");
                                $(thisBlock).find('.ivr-Menu-close-button').click();

                                $(thisBlock).find(".ttsMwCloseBtn").remove();
                                $(thisBlock).find('#record_audio').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                            }
                            else {
                                errMsgDialog('Audio was not recorded, please try again.');
                                $(thisBlock).find('#recordedAudioSavedWrapper').html('');
                            }
                            $(obj).val('Call Me');
                        }, "json");
                    }, 2000);
                }
             }

             function createUUID() {
                // http://www.ietf.org/rfc/rfc4122.txt
                var s = [];
                var hexDigits = "0123456789abcdef";
                for (var i = 0; i < 36; i++) {
                    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
                }
                s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
                s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
                s[8] = s[13] = s[18] = s[23] = "-";

                var uuid = s.join("");
                return uuid;
            }

        </script>
    </body>
</html>