<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_voice_broadcast";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken, $title, $TIMEZONE;

if(!isset($_GET['campaign_id']) || $_GET['campaign_id']==""){
    ?>
<script type="text/javascript">
    alert('Invalid campaign!');
    window.location = "ad_vb_campaigns.php";
</script>
    <?php
}

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10007)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system,
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

$campaign_data = ad_vb_get_campaign_data($_GET['campaign_id']);

if($campaign_data==""){
    ?>
<script type="text/javascript">
    alert('Invalid campaign!');
    window.location = "ad_vb_campaigns.php";
</script>
<?php
}

$response = '';
$response_type = '';

//If action is set
if (isset($_GET['action'])) {

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $title; ?><?php echo $campaign_data['campaign_name'];?>: Sequences</title>
    <?php include "include/css.php"; ?>
</head>
<body>
<div id="hld">
    <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
        <?php
        //Displaying the navigation menu on page
        include('include/nav.php');
        ?>
        <!--Initializing the html part that will display the campaign list-->
        <div class="block">
            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>
                <h2><?php echo $campaign_data['campaign_name'];?>: Sequences</h2>
                <ul>
                    <li><a href="ad_vb_sequence_edit.php?campaign_id=<?php echo $_GET['campaign_id'];?>">Add Message</a></li>
                    <li><a href="ad_vb_campaigns.php">Campaigns List</a></li>
                </ul>
            </div>
            <div class="block_content">
                <?php
                if ($response_type == 'success'):
                    ?>
                    <div class="message success"><p><?php echo $response; ?></p></div>
                    <?php
                elseif ($response_type == 'failure'):
                    ?>
                    <div class="message errormsg"><p><?php echo $response; ?></p></div>
                    <?php
                elseif ($response_type == 'warning'):
                    ?>
                    <div class="message warning"><p><?php echo $response; ?></p></div>
                    <?php
                elseif ($response_type == 'info'):
                    ?>
                    <div class="message info"><p><?php echo $response; ?></p></div>
                    <?php
                endif;
                ?>
                <table cellpadding="0" cellspacing="0" width="100%" class="sortable">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Schedule</th>
                        <th>Name</th>
                        <th>Send Time</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $stmt = $db->customExecute("SELECT * FROM ad_vb_sequences WHERE campaign_id = ? ORDER BY days_after ASC");
                    $stmt->execute(array($_GET['campaign_id']));
                    $sequences = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    if (count($sequences) > 0):
                        foreach ($sequences as $sequence):
                            ?>
                        <tr data-bind="<?php echo $sequence['id'];?>">
                            <td>
                                <?php
                                switch($sequence['type']){
                                    case "SMSBroadcast":
                                    case "AutoResponder":
                                        echo "SMS Broadcast";
                                        break;
                                    case "VoiceBroadcast":
                                        echo "Voice Broadcast";
                                        break;
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if($sequence['days_after']=="0")
                                    echo "Immediately";
                                else
                                    echo "Day ".$sequence['days_after'];
                                ?>
                            </td>
                            <td><?php echo $sequence['name'];?></td>
                            <td>
                                <?php 
                                if (!empty($sequence['delay'])) {
                                    if ($sequence['delay'] == "60") {
                                        echo "Immediately";
                                    }
                                    else {
                                        echo $sequence['delay']." minutes delayed";
                                    }
                                }
                                else {
                                    $time = new DateTime("now",new DateTimeZone($TIMEZONE));
                                    echo "At ".$sequence['time']." ".$time->format("T");    
                                }                                
                                ?>
                            </td>
                            <td>
                                <a href="ad_vb_sequence_edit.php?campaign_id=<?php echo $campaign_data['idx'];?>&id=<?php echo $sequence['id'];?>"><img class="tt" title="Edit Sequence" width="16" src="images/cog_edit.png"/></a>
                                <a href="#" data-params="<?php echo $sequence['id'];?>" id="USR_SEQ_DELETE"><img class="tt" title="Delete Sequence" src="images/delete.gif"></a>
                            </td>
                        </tr>
                            <?php
                        endforeach;
                    else:
                        ?>
                    <tr>
                        <td>No records exist.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                        <?php
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>		<!-- .block_content ends -->
            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>
        <!--Exiting the HTML code that will display the campaign list-->

        <!-- #header ends -->
        <?php include "include/footer.php"; ?>
    </div>
</div>

<!--//Notification bar html-->
<div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
<script type="text/javascript" language="javascript">
    if ($ === undefined) {
        $ = jQuery;
    }

    /**
     * This function will display the notification message on screen.
     * @param {string} message The message to be displayed in notification bar
     * @returns {undefined}
     */
    function ad_display_message(message) {
        $('.ad_notification').html(message);
        $('.ad_notification').slideDown();
        setTimeout(function() {
            $('.ad_notification').fadeOut(3000);
        }, 3000);

    }

    /**
     * This function will check whether string is blank or not
     * @param {string} str The value to be checked
     * @returns {Boolean} returns boolean TRUE or FALSE based on check performed.
     */
    function ad_is_str_blank(str) {
        if (str === '' || str === ' ' || str === null || str === undefined) {
            return true;
        } else {
            return false;
        }
    }

    //When document is ready to attache events to its elements
    $(document).ready(function() {
        $(".tt").tooltipster({
            position:'bottom',
            theme:'.tooltipster-shadow'
        });
    });

</script>
</body>
</html>