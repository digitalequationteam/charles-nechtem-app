<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_auto_dialer";

//Including necessary files
require_once 'include/util.php';
require_once 'include/Pagination.php';
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

//Check for user perm
if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10006)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

//Redirecting to login page if not logged in
    header("Location: login.php");

//Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

//If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

//Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Retrieving the campign index from request url
if (isset($_REQUEST['campaign_idx']))
    $campaign_idx = $_REQUEST['campaign_idx'];
else
    header('location: ' . dirname(s8_get_current_webpage_uri()) . '/ad_ad_campaigns.php');

//Retrieving the campaign data
$campaign_data = ad_ad_get_campaign_data($campaign_idx);

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
        <script type="text/javascript" src="//static.twilio.com/libs/twiliojs/1.1/twilio.min.js"></script>
        <link href="css/callpanel_cssfile.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="js/callpanel_slider_jsfile.js"></script>
        <script type="text/javascript">

            /**
             * This function will check whether string is blank or not
             * @param {string} str The value to be checked
             * @returns {Boolean} returns boolean TRUE or FALSE based on check performed.
             */
            function ad_is_str_blank(str) {
                if (str === '' || str === ' ' || str === null || str === undefined) {
                    return true;
                } else {
                    return false;
                }
            }

            //Initializing the bulk dial numbers variable
<?php
$ad_numbers = ad_advb_retrieve_contacts('ad', $campaign_idx);
?>
            var connection = null;
            var mic_muted = false;
            var ad_ad_bulk_dial_numbers = [<?php echo "'" . implode("','", $ad_numbers) . "'"; ?>];
            var ad_ad_next_key = 0;

            var this_CallSid = "";

            //Setting up the twilio device
            Twilio.Device.setup("<?php echo generate_twilio_autodial_auth_token(); ?>");
            Twilio.Device.ready(function(device) {
                display_message("Ready");
            });

            Twilio.Device.error(function(error) {
                display_message("Error: " + error.message);
                hide_bulk_dial_status();
                connection = null;
            });
            Twilio.Device.connect(function(conn) {
                $('#client-ui-message').text('Call in Progress');
                $("div#client-make-call").css('display', 'none');
                $("div#client-ui-status, div#client-ui-actions").css('display', 'block');
                //$("button#client-ui-close").css('display', 'block');
                $("button#client-ui-hangup").css('display', 'block');
                toggleCallStatus();
                countdown();

                mic_muted = false;
                $("#mute_mic").attr("src","images/micon.png");
                connection = conn;
                display_message("Successfully established call");
                
                if (ad_ad_bulk_dial_numbers[ad_ad_next_key - 1])
                    bulk_dial_status('Connected to ' + ad_ad_bulk_dial_numbers[ad_ad_next_key - 1] + '.', 'success');

                $('[name=ad_ad_dd_contact_number], [name=ad_ad_dd_pnone]').val(ad_ad_bulk_dial_numbers[ad_ad_next_key - 1]);
                $.get('ad_ajax.php?action=ad_ad_dd_get_call_details&ad_ad_dd_campaign_idx=<?php echo $campaign_idx; ?>&ad_ad_dd_contact_number=' + encodeURIComponent(ad_ad_bulk_dial_numbers[ad_ad_next_key - 1]), function(ajax_response) {

                    //Parsing the contact details
                    var contact_details = $.parseJSON(ajax_response);

                    $("#note_btn").attr("href","ad_ajax.php?action=ad_ad_dd_notes&contact_idx="+contact_details.idx);
                    $("[name=ad_ad_dd_idx]").val(contact_details.idx);

                    $("[name=ad_ad_ext_idx]").val(contact_details.ext_idx);

                    //Displaying contact details in respective form fields
                    //If firstname is not blank
                    if (!ad_is_str_blank(contact_details.first_name)) {
                        $('[name=ad_ad_dd_firstname]').val(decodeURIComponent(contact_details.first_name));
                    }
                    else {
                        $('[name=ad_ad_dd_firstname]').val("");
                    }
                    //Lastname is not blank
                    if (!ad_is_str_blank(contact_details.last_name)) {
                        $('[name=ad_ad_dd_lastname]').val(decodeURIComponent(contact_details.last_name));
                    }
                    else {
                        $('[name=ad_ad_dd_lastname]').val("");
                    }
                    //email ID is not blank
                    if (!ad_is_str_blank(contact_details.email)) {
                        $('[name=ad_ad_dd_email]').val(decodeURIComponent(contact_details.email));
                    }
                    else {
                        $('[name=ad_ad_dd_email]').val("");
                    }
                    //email ID is not blank
                    if (!ad_is_str_blank(contact_details.business_name)) {
                        $('[name=ad_ad_dd_business]').val(decodeURIComponent(contact_details.business_name));
                    }
                    else {
                        $('[name=ad_ad_dd_business]').val("");
                    }
                    //email ID is not blank
                    if (!ad_is_str_blank(contact_details.address)) {
                        $('[name=ad_ad_dd_address]').val(decodeURIComponent(contact_details.address));
                    }
                    else {
                        $('[name=ad_ad_dd_address]').val("");
                    }
                    //email ID is not blank
                    if (!ad_is_str_blank(contact_details.city)) {
                        $('[name=ad_ad_dd_city]').val(decodeURIComponent(contact_details.city));
                    }
                    else {
                        $('[name=ad_ad_dd_city]').val("");
                    }
                    //email ID is not blank
                    if (!ad_is_str_blank(contact_details.state)) {
                        $('[name=ad_ad_dd_state]').val(decodeURIComponent(contact_details.state));
                    }
                    else {
                        $('[name=ad_ad_dd_state]').val("");
                    }
                    //email ID is not blank
                    if (!ad_is_str_blank(contact_details.zip)) {
                        $('[name=ad_ad_dd_zip]').val(decodeURIComponent(contact_details.zip));
                    }
                    else {
                        $('[name=ad_ad_dd_zip]').val("");
                    }
                    //email ID is not blank
                    if (!ad_is_str_blank(contact_details.website)) {
                        $('[name=ad_ad_dd_website]').val(decodeURIComponent(contact_details.website));
                    }
                    else {
                        $('[name=ad_ad_dd_website]').val("");
                    }

                    var full_address_arr = [$('[name=ad_ad_dd_address]').val(), $('[name=ad_ad_dd_city]').val(), $('[name=ad_ad_dd_state]').val(), $('[name=ad_ad_dd_zip]').val()];
                    var full_address = encodeURIComponent(full_address_arr.join(', '));
                    $.get('ad_ajax.php?action=getLatLong&address=' + full_address, function(ajax_response) {
                        var latLong = $.parseJSON(ajax_response);
                        map.setCenter(new google.maps.LatLng(latLong.lat, latLong.long));
                        $.get('ad_ajax.php?action=ad_get_timezone&lat='+map.center.d+'&lon='+map.center.e,function(data){
                            $("#contact_time").text("Contact's Current Time: "+data);
                        });
                    });

                });

            });

            //Attaching a handler to run when call disconnects
            Twilio.Device.disconnect(function(conn) {
                connection = null;
                mic_muted = false;
                $("#mute_mic").attr("src","images/micon.png");
                //Dispaying ntification
                display_message("Call ended");
                this_CallSid = conn.parameters.CallSid;
                //attaching a handler to run after 1 second 
                //dealing with execution of furthur code
                setTimeout(handle_call_disconnect, 1000);
            });

            Twilio.Device.incoming(function(conn) {
                bulk_dial_status("Connected to " + conn.parameters.From + ". Screenning the call for machine VS human.", 'success');
                // accept the incoming connection and start two-way audio
                conn.accept();
                mic_muted = false;
                $("#mute_mic").attr("src","images/micon.png");
                connection = conn;
            });

            /**
             * This function will establishes the call
             * @param {string} number_uid The unique array ID of number
             * @returns {void}
             */
            function call(number_uid) {
                console.log(number_uid);

                //If number is not blank
                if (!ad_is_str_blank(ad_ad_bulk_dial_numbers[number_uid])) {
                    
                    //Updating the bulk dial status
                    bulk_dial_status('Dialing ' + ad_ad_bulk_dial_numbers[ad_ad_next_key], 'info');

                    //Get the phone number to connect the call to
                    params = {
                        Fromcall: '<?php echo $campaign_data['phone_number']; ?>',
                        tocall: ad_ad_bulk_dial_numbers[number_uid],
                        userid: '<?php echo @$_SESSION['user_id']; ?>',
                        campaign_idx: '<?php echo $_REQUEST['campaign_idx']; ?>',
                        company_id: '<?php echo @$_SESSION['sel_co']; ?>'
                    };

                    //Trying to establish the call
                    Twilio.Device.connect(params);

                    $("#numbers_locations input").prop('disabled', true);

                } else {

                    //Updating the bulk dial status
                    bulk_dial_status('No number to call.', 'errormsg');

                    //Displaying the alert on screen
                    alert('No number left to call.');

                }
                is_reset = false;
            }

            /**
             * This function will run when hangup button is clicked.
             * @returns {void}
             */
            function hangup() {

                //This line will disconnect the currently established call.
                Twilio.Device.disconnectAll();

                $('#client-ui-message').text('Call ended');
                $('#divstatus').val('');
                $('#TimeStatus').val('');
                toggleCallStatus();
                $('#dial-input-button').show();
                $('#client-ui-action-button').hide();
                stop();
                $('#divstatus').val('');
                $('#divstatus').val('shown');
                $("div#client-make-call").css('display', 'block');
                $("div#client-ui-status, div#client-ui-actions").css('display', 'none');
                connection = null;
                $("#dial-phone-number").val('');
            }

            /**
             * This function will reset the auto dialer.
             * @returns {void}
             */

            var is_reset = false;
            function reset_autodial() {
                if (!is_reset && connection) {
                    //Restting the numbers list and next key
                    //ad_ad_bulk_dial_numbers = [];
                    //ad_ad_next_key = ad_ad_next_key + 1;
                    
                    is_reset = true;

                    //Disconnecting the call
                    hangup();

                    //Reloading the webpage
                    //window.document.location.reload();
                }
            }

            function mute_mic_(){
                var elm = $("#mute_mic");
                var micon  = "images/micon.png";
                var micoff = "images/micoff.png";
                if(connection!=null){
                    if(mic_muted){
                        mic_muted = false;
                        elm.attr("src",micon);
                        connection.unmute();
                    }else{
                        mic_muted = true;
                        elm.attr("src",micoff);
                        connection.mute();
                    }
                }
            }

            /**
             * This function will update the campaign progress.
             * @returns {undefined}
             */
            function ad_ad_update_campaign_progress_on_page() {
<?php
$campaign_progress = ad_ad_get_campaign_progress($campaign_idx);
$progress_vals = explode('/', $campaign_progress);
if (trim($progress_vals[0]) != trim($progress_vals[1])):
    ?>

                    //Executing the ajax request
                    $.get('ad_ajax.php?action=ad_retrive_progress&camp_type=ad&camp_idx=<?php echo $campaign_idx; ?>', function(response) {

                        //When ajax requst completes
                        //Updating campaign progress
                        $('.campaign_progress').text(response);

                    });

    <?php
endif;
?>
            }

            /**
             * This function will run when call will be disconnected
             * @returns {void}
             */
            function handle_call_disconnect() {

                //Updating campaign progress on call discoonnect to update user
                ad_ad_update_campaign_progress_on_page();

                //If next number to call does exists
                if (!ad_is_str_blank(ad_ad_bulk_dial_numbers[ad_ad_next_key])) {
                    if (!is_reset) {
                        //Establishing the call to next number in loop
                        call(ad_ad_next_key);

                        //Setting the next key
                        ad_ad_next_key += 1;
                    }
                } else {

                    //Resetting the bulk dial numbers list
                    ad_ad_bulk_dial_numbers = [];
                    //Setting the next key to first one 
                    //which does not exist after the execution of previous JS statement
                    ad_ad_next_key = 0;

                    //Stop displaying the notification bar
                    hide_bulk_dial_status();
                }

                //Setting the phone number fields of dialling details page to null
                //[name=ad_ad_dd_pnone]
                $('[name=ad_ad_dd_contact_number]').val('');
                $("#ad_ad_dd_phone_code").find("option:eq(0)").prop('selected',true);
            }

            /**
             * This function will display notification status on screen
             * @param {string} message The message to be displayed
             * @returns {void}
             */
            function display_message(message) {

                //Setting the message to be displayed as notification
                $('.ad_notification').html(message);

                //Dispalying notification by sliding it down
                $('.ad_notification').slideDown();

                //After 3 seconds fading out the notification from screen
                //While assuming that 3 seconds is enough time for user to read the message.
                setTimeout(function() {

                    //Hiding notification bar
                    $('.ad_notification').fadeOut(3000);

                }, 3000);
            }

            /**
             * Displays the status of bulk dial
             * @param {string} message The message to display
             * @param {string} type The type of message
             * @returns {void}
             */
            function bulk_dial_status(message, type) {
                //Intializing the message variable
                var message_html = '';

                //Switching over the type of message
                switch (type) {

                    //If message if of success type and same for others
                    case 'success':
                        message_html = '<div class="outmessage success"><p>' + message + '</p><span class="close" title="Dismiss"></span></div>';
                        break;
                    case 'errormsg':
                        message_html = '<div class="outmessage errormsg"><p>' + message + '</p><span class="close" title="Dismiss"></span></div>';
                        break;
                    case 'info':
                        message_html = '<div class="outmessage info"><p>' + message + '</p><span class="close" title="Dismiss"></span></div>';
                        break;
                    case 'warning':
                        message_html = '<div class="outmessage warning"><p>' + message + '</p><span class="close" title="Dismiss"></span></div>';
                        break;
                }

                //Displaying the message on screen
                $('.ad_ad_bulk_dial_status_text').html(message_html);
            }

            /**
             * This function will hide the status shown in notification plate
             * @returns {void}
             */
            function hide_bulk_dial_status() {

                //Hiding the status bar
                $('.ad_ad_bulk_dial_status_text').html('');
            }

            //If jQuery $ variable is not set
            if ($ === undefined) {

                //Assuming the jQuery variable did exist
                //Assigning the jQuery variable to $
                $ = jQuery;
            }

            function toggleCallStatus() {
                $('#dial-input-button').toggle();
                $('#hangup').toggle();
                $('#dialpad').toggle();
            }

            //iF DOCUMENT is ready to perform operations
            $(document).ready(function() {
                $("#dial-input-button").click(function() {
                    params = {"tocall": $('#dial-phone-number').val(),
                        "Fromcall": $("#caller-id-phone-number").val()};

                    connection = Twilio.Device.connect(params);
                });

                $("#client-ui-hangup").click(function() {
                    hangup();
                });

                $(".client-ui-button").click(function(){
                    if (connection) {
                        var value = $(this).find(".client-ui-button-number").text();
                        if (value != 'C' || value != "")
                            connection.sendDigits(value);
                    }
                });

                ///Every 20 seconds, We are updating the status displayed on screen
                //About current campaign just after campaign name
                setInterval(function() {
                    //Updating the campaign progress
                    ad_ad_update_campaign_progress_on_page();
                    //Defining the time interval
                }, 20000);
            });
        </script>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

        <!--//Setting the style of map-->
        <style>
            #map-canvas {
                margin: 0;
                padding: 0;
                height: 100%;
            }
        </style>

        <!--//Loading google maps JS library-->
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script>
<?php
            $ny_cordinates = array("longitude"=>"-74.006605","latitude"=>"40.714623");
?>
            //Defining variable that will contain google map object
            var map;

            //When document finishes loading further data
            $(window).load(function() {

                //Setting map options
                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng('<?php echo $ny_cordinates['latitude']; ?>', '<?php echo $ny_cordinates['longitude']; ?>'),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                //Displaying map on screen and storing the google map object 
                //in our custom varibale defined at first line
                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                $.get('ad_ajax.php?action=ad_get_timezone&lat=40.714623&lon=-74.006605',function(data){
                    $("#contact_time").text("Contact's Current Time: "+data);
                });
                setInterval(function(){
                    $.get('ad_ajax.php?action=ad_get_timezone&lat='+map.center.d+'&lon='+map.center.e,function(data){
                        $("#contact_time").text("Contact's Current Time: "+data);
                    });
                },20000);
            });

        </script>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
//Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                <div class="clear"></div>
                <div class="ad_ad_bulk_dial_status_text"></div>
                <div class="clear"></div>

                <div class="block" style="background: none;">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2><?php echo ad_ad_get_campaign_name($campaign_idx); ?> (STATUS: <span class="campaign_progress"><?php echo ad_ad_get_campaign_progress($campaign_idx); ?></span>)</h2>
                        <ul>
                            <li><a href="ad_ad_add_campaign.php">Add a Campaign</a></li>
                            <li><a href="ad_ad_campaigns.php">Campaigns List</a></li>
                            <li><a href="ad_contactlist_log.php">Contacts</a></li>
                            <li><a href="ad_ad_logs.php">Logs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clear"></div>

                <script type="text/javascript" language="javascript">
                    $(document).ready(function() {
                        var ad3f_obj = "#auto_dialer_dialling_details_form";
                        $(ad3f_obj).submit(function(e) {
                            if (!ad_is_str_blank($('[name=ad_ad_dd_contact_number]').val())) {

                                var ad_ad_dd_form_data = $(ad3f_obj).serialize();
                                $.post('ad_ajax.php?action=ad_ad_dd_process_form', ad_ad_dd_form_data, function(response) {
                                    alert(response);
                                    //$(ad3f_obj + ' input[type="text"]').val('');
                                });

                            } else {
                                alert('No call is in progress.');
                            }
                            e.preventDefault();
                        });
                    });
                    function savePC(elm){
                        var pc = $(elm).val();
                        var contact_id = $("input[name='ad_ad_ext_idx']").val();
                        if(contact_id!="")
                            $.post("ad_ajax.php?action=ad_ad_dd_phone_code&phone_code="+pc+"&contact_id="+contact_id, function(response){
                                alert("Updated phone code for call.");
                            });
                    }
                </script>

                <!--//Starting the Twilio Settings form-->
                <form action="" method="post" id="auto_dialer_dialling_details_form">
                    <input type="hidden" name="ad_ad_dd_campaign_idx" value="<?php echo $campaign_idx; ?>" />
                    <input type="hidden" name="ad_ad_dd_contact_number" value="" />

                    <!--Initializing the left hand portion of settings page-->
                    <div class="block small left" style="width: 320px;height: 445px;border-right: none;">
                        <div class="block_head" style="border-right: none;">
                            <div class="bheadl"></div>
                            <!--<div class="bheadr"></div>-->
                            <h2>Contact Details:</h2>
                        </div>
                        <!-- .block_head ends -->

                        <div class="block_content" style="height: 376px;border-right: none;">

                            <p>
                                <label>Firstname: </label><br/>
                                <input tabindex="1" type="text" class="text" name="ad_ad_dd_firstname" value="" />
                            </p>

                            <p>
                                <label>Email: </label>
                                <span class="email-contact" style="float: right;"><a href="mailto:">Email This Contact</a></span>
                                <br/>
                                <input tabindex="3" type="text" class="text" name="ad_ad_dd_email" value="" />
                            </p>

                            <p>
                                <label>Address: </label>
                                <input tabindex="5" type="text" class="text" name="ad_ad_dd_address" value="" />
                            </p>

                            <p>
                                <label>State: </label>
                                <input tabindex="7" type="text" class="text" name="ad_ad_dd_state" value="" />
                            </p>

                            <p>
                                <label>Website: </label>
                                <input tabindex="9" type="text" class="text" name="ad_ad_dd_website" value="" />

                            </p>

                            <!-- .block_content ends -->
                        </div>

                        <div class="bendl"></div>
                        <!--<div class="bendr"></div>-->
                        <!-- .block.small.right ends -->
                    </div>

                    <div class="block small left" style="width: 320px;margin-left: 0px;border-left: none;height: 445px;">
                        <div class="block_head" style="border-left: none;">
                            <!--<div class="bheadl"></div>-->
                            <div class="bheadr"></div>
                            <span id="contact_time">

                            </span>
                        </div>	
                        <!-- .block_head ends -->

                        <div class="block_content" style="height: 376px;border-left: none;" >
                            <input type="hidden" value="" name="ad_ad_dd_idx" />
                            <input type="hidden" value="" name="ad_ad_ext_idx" />
                            <p>
                                <label>Lastname: </label>
                                <input tabindex="2" type="text" class="text" name="ad_ad_dd_lastname" value="" />
                            </p>

                            <p>
                                <label>Phone: </label>
                                <input tabindex="4" type="text" class="text" name="ad_ad_dd_pnone" value="" />
                            </p>

                            <p>
                                <label>City: </label>
                                <input tabindex="6" type="text" class="text" name="ad_ad_dd_city" value="" />
                            </p>

                            <p>
                                <label>Zip: </label>
                                <input tabindex="8" type="text" class="text" name="ad_ad_dd_zip" value="" />
                            </p>

                            <p>
                                <label>Business: </label>
                                <input tabindex="9" type="text" class="text" name="ad_ad_dd_business" value="" />
                            </p>

                            <p class="mid" style="margin: auto auto;">
                                <input type="submit" class="submit mid" name="save_ad_ad_dd_settings" value="Save Setting" />
                            </p>
                            <!-- .block.small.right ends -->
                        </div>
                        <!--<div class="bendl"></div>-->
                        <div class="bendr"></div>

                        <!-- .block_content ends -->
                    </div>

                    <div class="block small left" style="width: 310px; margin-left: 10px; position: relative;">
                        <div class="block_head">
                            <div class="bheadl"></div>
                            <div class="bheadr"></div>
                            <h2>Actions</h2>
                        </div>      
                        <div class="block_content" style="padding-top: 10px; padding-bottom: 14px;">

                            <div style="float:left;width: 50%;padding-top: 10px;">
                                <p style="width: 100px;clear: none;margin: 0 auto;" class="mid">
                                    <a id="open_script" href="javascript: void(0);">
                                        <input type="button" class="submit mid" value="Open Script" />
                                    </a>
                                </p>

                                <p style="width: 100px;clear: none;margin: 0 auto; margin-bottom: 29px;" class="mid">
                                    <a id="note_btn" href="#" rel="facebox">
                                        <input type="button" class="submit mid" value="Notes" style="float: left;" />
                                    </a>
                                </p>

                                <p style="width: 100px;clear: none;margin: 0 auto;" class="mid">
                                    <a id="note_btn" href="javascript: void(0);">
                                        <input type="button" class="submit mid" value="Keypad" style="float: left;" onclick="$('#dialer').toggle(); return false;" />
                                    </a>
                                </p>
                            </div>

                            <div style="float:right;width: 40%; padding-top: 5px;">
                                <p style="width: 100px;clear: none;margin: 0 auto;">
                                    <img title="Start Calling" onclick="call(ad_ad_next_key); ad_ad_next_key = ad_ad_next_key + 1; " style="margin: 5px;float: left;cursor: pointer;" src="images/ad_play.png" />
                                    <img title="Mute Microphone" id="mute_mic" onclick="mute_mic_();" style="width: 32px; margin: 5px;float: left;cursor: pointer;" src="images/micon.png" />
                                </p>
                                <div class="clear" style="padding: 2px;"></div>
                                <p style="width: 100px;clear: none;margin: 0 auto;">

                                    <img title="Stop Calling" onclick="reset_autodial();" style="margin: 5px;float: left;cursor: pointer;" src="images/ad_stop.png" />

                                    <img title="Next Number" onclick="hangup();" style="margin: 5px;float: left;cursor: pointer;" src="images/ad_next.png" />
                                </p>
                            </div>

                            <div class="clearfix"></div>

                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $("#dialer").css('right', '22px');
                                    $("#dialer").css('margin-right', '0px');
                                    setInterval(function(){
                                        $("#dialer").css("top", "195px");
                                    }, 500);

                                    $("#open_script").click(function(){
                                        jQuery.facebox(function(){
                                            $.get('ad_ajax.php?action=ad_ad_dd_open_script&ad_ad_dd_campaign_idx=<?php echo $campaign_idx; ?>', function(data) {
                                                data = data.wordWrap(128,'\n',0);
                                                $.facebox(data);
                                            });
                                        });
                                    });
                                });
                            </script>

                            <div id="dialer" class="open" style="position: absolute; height: 367px; margin-bottom: 10px; top: 457px; display: none;">
                                <input type='hidden' id='divstatus' value="">
                                <input type='hidden' id='callOptionsInput' value="">

                                <div class="client-ui-tab open" style="visibility: hidden;">

                                    <div class="client-ui-bg-overlay" style="background: #f5f5f5; border: solid 1px #d6d6d6;"><!-- leave me alone! --></div>
                                    <div class="client-ui-inset">
                                        <div id="client-ui-tab-status">
                                            <div class="client-ui-tab-wedge">
                                                <img src="images/callmaker-phone-ico.png" class="callmaker-phone-ico" />
                                                <a class="callmaker" href="#dialer"><span class="symbol"></span> Hide</a>
                                            </div>
                                            <div class="client-ui-tab-status-inner">
                                                <div class="mic"></div>
                                                <h3 class="client-ui-timer">00:00</h3>
                                            </div><!-- .client-ui-tab-status-inner -->
                                        </div><!-- #client-ui-tab-status -->
                                    </div><!-- #client-ui-tab-inset -->

                                </div><!-- .client-ui-tab .open -->

                                <div class="client-ui-content">
                                    <div class="client-ui-bg-overlay" style="background: #f5f5f5; border: solid 1px #d6d6d6;"><!-- leave me alone! --></div>
                                    <div class="client-ui-inset">

                                        <div id="client-make-call">
                                            <div id="make-call-form" action="" method="post">

                                                <fieldset style="margin-left:16px; padding-top: 20px;">
                                                </fieldset>
                                            </div><!-- #make-call-form -->

                                        </div><!-- #client-make-call -->

                                        <div id="client-on-call" style="display: block;">
                                            <div id="client-ui-status" class="clearfix" style="display:none !important; padding-top: 10px; background: none; visibility: none;">
                                                <h2 id="client-ui-message">Ready</h2>
                                                <h3 class="client-ui-timer" style="float:left;margin-left:60px;width:25px;">
                                                    <div id="minutes"></div>
                                                    <h3>
                                                        <h3 class="client-ui-timer" style="float:left;margin-left:10px;width:10px;">:</h3>
                                                        <h3 class="client-ui-timer" style="float:left;width:50px;"><div id="seconds"></div></h3>
                                                            <input type=hidden id="TimeStatus" value="">

                                            </div>

                                            <style type="text/css">
                                                .client-ui-button {
                                                    padding: 9px;
                                                    margin: 4px;
                                                    background: #b3b3b3;
                                                    background: -moz-linear-gradient(100% 100% 90deg, #b3b3b3, #d6d6d6);
                                                    background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#d6d6d6), to(#b3b3b3));
                                                    color: #676666;
                                                    border: 1px solid #b3b3b3;
                                                }
                                            </style>
                                            <!-- #client-ui-status -->
                                            <div id="client-ui-pad" class="clearfix" style="width: 168px;margin: 15px auto 0 auto;">
                                                <div class="client-ui-button-row">
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">1</div>
                                                        <div class="client-ui-button-letters"></div>
                                                    </div>
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">2</div>
                                                        <div class="client-ui-button-letters">abc</div>
                                                    </div>
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">3</div>
                                                        <div class="client-ui-button-letters">def</div>
                                                    </div>
                                                </div>
                                                <div class="client-ui-button-row">
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">4</div>
                                                        <div class="client-ui-button-letters">ghi</div>
                                                    </div>
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">5</div>
                                                        <div class="client-ui-button-letters">jkl</div>
                                                    </div>
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">6</div>
                                                        <div class="client-ui-button-letters">mno</div>
                                                    </div>
                                                </div>
                                                <div class="client-ui-button-row">
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">7</div>
                                                        <div class="client-ui-button-letters">pqrs</div>
                                                    </div>
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">8</div>
                                                        <div class="client-ui-button-letters">tuv</div>
                                                    </div>
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number">9</div>
                                                        <div class="client-ui-button-letters">wxyz</div>
                                                    </div>
                                                </div>
                                                <div class="client-ui-button-row">
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number asterisk" style="margin-top: 8px;">*</div>
                                                        <!--<div class="client-ui-button-letters"></div>-->
                                                    </div>
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number" style="margin-top: 4px;">0</div>
                                                        <!--<div class="client-ui-button-letters"></div>-->
                                                    </div>
                                                    <div class="client-ui-button">
                                                        <div class="client-ui-button-number" style="margin-top: 4px;">#</div>
                                                        <!--<div class="client-ui-button-letters"></div>-->
                                                    </div>
                                                </div>
                                                <div class="client-ui-button-row">
                                                    <div class="client-ui-button"></div>
                                                    <div class="client-ui-button clear-ui-button-data">
                                                        <div class="client-ui-button-number" style="font-size: 20px;">C</div>
                                                        <div class="client-ui-button-letters" style="margin-left:-4px;">Clear</div>
                                                    </div>
                                                    <div class="client-ui-button"></div>
                                                </div>
                                            </div><!-- /client-ui-pad -->
                                            <div id="client-ui-actions" style="display:none;">
                                            </div><!-- #client-ui-actions -->
                                        </div><!-- #client-on-call -->
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="bendl"></div>
                        <div class="bendr"></div>
                        <!-- .block.small.right ends -->
                    </div>

                    <div class="block small left" style="width: 310px; margin-left: 10px;">
                        <div class="block_head">
                            <div class="bheadl"></div>
                            <div class="bheadr"></div>
                            <h2>Phone Code</h2>
                        </div>      
                        <div class="block_content" style="height: 36px; overflow-y: auto;">

                            <p style="clear: none;margin: 0 auto;">
                                <label>Phone Code: </label>
                                <select tabindex="10" name="ad_ad_dd_phone_code" id="ad_ad_dd_phone_code" onchange="savePC(this);">
                                    <option value="0">Select Code</option>
                                    <?php
                                    $phone_codes = ad_get_company_phone_codes($_SESSION['sel_co']);
                                    foreach ($phone_codes as $single_phone_code):
                                        echo '<option value="' . $single_phone_code['idx'] . '">' . $single_phone_code['name'] . '</option>';
                                    endforeach;
                                    ?>
                                </select>
                            </p>

                        </div>

                        <div class="bendl"></div>
                        <div class="bendr"></div>
                        <!-- .block.small.right ends -->
                    </div>

                    <div class="block small left" style="width: 310px; margin-left: 10px;">
                        <div class="block_head">
                            <div class="bheadl"></div>
                            <div class="bheadr"></div>
                            <h2>Voicemail Messages</h2>
                        </div>      
                        <div class="block_content" style="height: 98px; overflow-y: auto;">

                            <script type="text/javascript">
                                if ($ === undefined) {
                                    $ = jQuery;
                                }
                                $(document).ready(function() {
                                    setInterval(function(){$(".email-contact").find("a").attr("href","mailto:"+$("input[name='ad_ad_dd_email']").val());},1000);
                                    $('.play_mp3').click(function(e) {
                                        $.post('ad_ajax.php', { action: 'play_mp3', rel: $(this).attr('rel') }, function(response) {
                                            alert(response);
                                        });
                                        e.preventDefault();
                                    });

                                    $('.play_text').click(function(e) {
                                        $.get('ad_ajax.php?action=play_text&text=' + encodeURIComponent($(this).attr('text')) + '&language=' + encodeURIComponent($(this).attr('language')) + '&voice=' + encodeURIComponent($(this).attr('voice')), function(response) {
                                            alert(response);
                                        });
                                        e.preventDefault();
                                    });
                                });
                            </script>

                            <?php
                            $voicemail_messages = (empty($campaign_data['voicemail_messages']) ? array() : json_decode($campaign_data['voicemail_messages']));
                            if(is_array($voicemail_messages) || is_object($voicemail_messages)){
                                foreach ($voicemail_messages as $voicemail_message) {
                                    $content = $voicemail_message->content;
                                    $text = $voicemail_message->content;
                                    $language = $voicemail_message->language;
                                    $voice = $voicemail_message->voice;
                                    if ($voicemail_message->type == "Text") {
                                        ?>
                                            <a href="javascript: void(0);" class="play_text" text="<?php echo $text; ?>" language="<?php echo $language; ?>" voice="<?php echo $voice; ?>" style="font-weight: bold;">Text:</a> <?php echo $text; ?><br /><br />
                                        <?php
                                    }
                                    else {
                                        if (substr($content, 0, 4) != "http")
                                            $content = dirname(s8_get_current_webpage_uri())."/audio/auto_dialer_files/".$campaign_data['user']."/".$content;

                                        $full_url = $content;
                                        $url_to_display = (strlen($content) > 30) ? substr($content, 0, 30)."..." : $content;
                                        ?>
                                            <a href="javascript: void(0);" class="play_mp3" rel="<?php echo $full_url; ?>" style="font-weight: bold;">Audio:</a> <?php echo $url_to_display; ?><br /><br />
                                        <?php
                                    }
                                }
                            }

                            if (count($voicemail_messages) == 0)
                                echo '<div style="width: 100%;float: left;text-align: center;margin: 5px;">No voicemail messages for this campaign.</div>';
                            ?>

                        </div>

                        <div class="bendl"></div>
                        <div class="bendr"></div>
                        <!-- .block.small.right ends -->
                    </div>

                    <div class="block small left" style="width: 960px;">
                        <div class="block_head">
                            <div class="bheadl"></div>
                            <div class="bheadr"></div>
                            <h2>Phone numbers by location</h2>
                        </div>      
                        <div class="block_content">

                            <div id="numbers_locations">
                                Loading locations. Please wait.. <img src="images/loading.gif" style="width: 16px; height: 16px;" />
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function() {
                                    getNumbersLocations();
                                });

                                function getNumbersLocations() {

                                    var all_numbers = [];
                                    
                                    for (var i in ad_ad_bulk_dial_numbers) {
                                        if (i >= ad_ad_next_key) {
                                            var this_number = ad_ad_bulk_dial_numbers[i];
                                            if (this_number != "") {
                                                if (this_number.length == 10)
                                                    var country = "US";
                                                else
                                                    var country =  countryForE164Number("+" + this_number) || "US";

                                                var this_extension = "";
                                                try {
                                                    var this_number = cleanPhone(this_number);
                                                    var phoneUtil = i18n.phonenumbers.PhoneNumberUtil.getInstance();
                                                    var number = phoneUtil.parseAndKeepRawInput(this_number, country);
                                                    if (phoneUtil.isValidNumberForRegion(number, country)) {
                                                        var nationalSignificantNumber = phoneUtil.getNationalSignificantNumber(number);
                                                        var nationalDestinationCodeLength = phoneUtil.getLengthOfNationalDestinationCode(number);
                                                        var nationalDestinationCode = nationalSignificantNumber.substring(0,  nationalDestinationCodeLength);
                                                        this_extension = nationalDestinationCode;
                                                    }

                                                } catch (e) {}

                                                all_numbers.push({
                                                    number: this_number,
                                                    country: country,
                                                    extension: this_extension
                                                });
                                            }
                                        }
                                    }

                                    $.post('ad_ajax.php?action=getPhoneNumbersRegions', { phoneNumbers: all_numbers }, function(response) {
                                        $("#numbers_locations").html('');

                                        for (var i in response.regions) {
                                            var html = '<div style="display: inline-block; width: 200px; vertical-align: top;"><strong>' + i + '</strong><br />';

                                            for (var j in response.regions[i]) {
                                                html += '<label style="width: 200px; display: block;"><input type="checkbox" checked="checked" style="display: inline-block !important;" data-numbers="' + response.regions[i][j].join(",") + '" /> ' + j + ' (' + response.regions[i][j].length + ')</label>';
                                            }

                                            html += '</div>';

                                            $("#numbers_locations").append(html);
                                        }

                                        if ($("#numbers_locations input").length == 0)
                                            $("#numbers_locations").html('No numbers left to call.');

                                        $("#numbers_locations input").click(function() {
                                            ad_ad_bulk_dial_numbers = [];
                                            $("#numbers_locations input").each(function() {
                                                if ($(this).is(":checked")) {
                                                    var numbers = $(this).attr('data-numbers').split(",");

                                                    for (var j in numbers) {
                                                        ad_ad_bulk_dial_numbers.push(numbers[j]);
                                                    }
                                                }
                                            });
                                        });
                                    }, "json");
                                }
                            </script>

                        </div>

                        <div class="bendl"></div>
                        <div class="bendr"></div>
                        <!-- .block.small.right ends -->
                    </div>

                    <div class="clearfix"></div>

                    <!--//Twilio setting form ends here-->
                </form>

                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Google Map</h2>
                    </div>		<!-- .block_head ends -->
                    <div class="block_content" style="height:500px; padding-bottom: 20px;">
                        <script type="text/javascript">
                            if ($ === undefined) {
                                $ = jQuery;
                            }
                            $(document).ready(function() {
                                $('[name=ad_ad_dd_address],[name=ad_ad_dd_city],[name=ad_ad_dd_state],[name=ad_ad_dd_zip]').change(function() {
                                    var full_address_arr = [$('[name=ad_ad_dd_address]').val(), $('[name=ad_ad_dd_city]').val(), $('[name=ad_ad_dd_state]').val(), $('[name=ad_ad_dd_zip]').val()];
                                    var full_address = encodeURIComponent(full_address_arr.join(', '));
                                    $.get('ad_ajax.php?action=getLatLong&address=' + full_address, function(ajax_response) {
                                        var latLong = $.parseJSON(ajax_response);
                                        map.setCenter(new google.maps.LatLng(latLong.lat, latLong.long));
                                        $.get('ad_ajax.php?action=ad_get_timezone&lat='+map.getBounds().getCenter().nb+'&lon='+map.getBounds().getCenter().ob,function(data){
                                            $("#contact_time").text("Contact's Current Time: "+data);
                                        });
                                    });
                                });
                            });
                            $(document).bind('beforeReveal.facebox', function(d) {
                                $('#facebox .body').width('600px');
                            });
                            $(document).bind('reveal.facebox', function() {
                                var sele = $('#facebox .content pre:eq(0)');

                                var firstname = decodeURIComponent($("input[name='ad_ad_dd_firstname']").val());
                                var lastname = decodeURIComponent($("input[name='ad_ad_dd_lastname']").val());
                                var email = decodeURIComponent($("input[name='ad_ad_dd_email']").val());
                                var phone = decodeURIComponent($("input[name='ad_ad_dd_pnone']").val());
                                var address = decodeURIComponent($("input[name='ad_ad_dd_address']").val());
                                var city = decodeURIComponent($("input[name='ad_ad_dd_city']").val());
                                var state = decodeURIComponent($("input[name='ad_ad_dd_state']").val());
                                var zip = decodeURIComponent($("input[name='ad_ad_dd_zip']").val());
                                var website = decodeURIComponent($("input[name='ad_ad_dd_website']").val());
                                var business = decodeURIComponent($("input[name='ad_ad_dd_business']").val());

                                if(firstname.length>0)
                                    sele.text(sele.text().replaceAll("[FirstName]",firstname));
                                if(lastname.length>0)
                                    sele.text(sele.text().replaceAll("[LastName]",lastname));
                                if(email.length>0)
                                    sele.text(sele.text().replaceAll("[Email]",email));
                                if(phone.length>0)
                                    sele.text(sele.text().replaceAll("[Phone]",phone));
                                if(address.length>0)
                                    sele.text(sele.text().replaceAll("[Address]",address));
                                if(city.length>0)
                                    sele.text(sele.text().replaceAll("[City]",city));
                                if(state.length>0)
                                    sele.text(sele.text().replaceAll("[State]",state));
                                if(zip.length>0)
                                    sele.text(sele.text().replaceAll("[Zip]",zip));
                                if(website.length>0)
                                    sele.text(sele.text().replaceAll("[Website]",website));
                                if(business.length>0)
                                    sele.text(sele.text().replaceAll("[Business]",business));

                            });

                        </script>
                        <div id="map-canvas"></div>
                        <br>
                        <!-- .block_content ends -->
                        <div class="bendl"></div>
                        <div class="bendr"></div>
                    </div>

                    <!-- #header ends -->
                    <?php include "include/footer.php"; ?>
                </div>
            </div>

            <!--//Notification bar html-->
            <div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
    </body>
</html>