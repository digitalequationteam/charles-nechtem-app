<?php

require_once('include/config.php');
require_once('include/util.php');
require_once('include/Services/Twilio.php');
$db = new DB();

$con = mysqli_connect($DB_HOST,$DB_USER,$DB_PASS);
mysqli_select_db($con, $DB_NAME );

$filter     = "!@#$^&%*()+=-[]\/{}|:<>?,.";
$recording  = preg_replace("/[$filter]/", "", $_REQUEST['RecordingUrl']);
$transcript = preg_replace("/[$filter]/", "", $_REQUEST['TranscriptionText']);

if ($db->getVar("mask_recordings") == "true") {
    $recording = Util::maskRecordingURL($recording);
}

$from   = $_REQUEST['From'];
$to   = $_REQUEST['To'];

$company_id = $db->getCompanyofCall($_REQUEST['CallSid']);

$res = mysqli_query($con, "SELECT wId FROM call_ivr_widget WHERE companyId= '".$company_id."' AND flowtype = 'Voicemail'");
$row = mysqli_fetch_assoc($res);
$wId = $row['wId'];

if (isset($_REQUEST['email'])) {
    $email = urldecode($_REQUEST['email']);
}
else {
    $res = mysqli_query($con, "SELECT meta_data FROM call_ivr_data WHERE wId= '".$wId."' AND meta_key = 'TranscriptEmail'");
    $row = mysqli_fetch_assoc($res);
    $email = $row['meta_data'];
}

$company = $db->getCompanyName($company_id);

$subject = "You have a new voicemail transcription from " . $from;
$body = "You received voicemail." .
            "\n\nTimestamp: ".date("Y-m-d H:i:s").
            "\n\nFrom: ". $_REQUEST['From'] .
            "\n\nTo Company: ". urldecode($company) .
            "\n\nRecording URL: $recording" .
            "\n\nTranscription:\n $transcript";

$email_params = array(
    "subject" => $subject,
    "msg" => $body,
    "emails" => array($email)
);

Util::sendEmail($email_params);
?>