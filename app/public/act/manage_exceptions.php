<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$db = new DB();
//This is an admin-only page.
//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}

if(@$_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

// get exceptions
$exceptions = $db->getForwardedNumbers();


//$twilio_numbers = Util::get_all_twilio_numbers();
$page="adminpage";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        #purchase_number, #reset_url, #update_number{
            width: 85px;
            height: 30px;
            line-height: 30px;
            background: url(images/btns.gif) top center no-repeat;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            text-transform: uppercase;
            color: white;
            text-shadow: 1px 1px 0 #0A5482;
            cursor: pointer;
            margin-right: 10px;
            vertical-align: middle;
            display: inline-block !important;
        }
        .ui-dialog label, input {
            display: inline-block !important;
        }
        .cmf-skinned-select{
            display: inline-block !important;
        }
        select#country option {
            background-repeat:no-repeat;
            background-position:bottom left;
            padding-left:30px;
        }
        select#country option[value="US"] {
            background-image:url(images/us.png);
        }
        select#country option[value="CA"] {
            background-image:url(images/ca.png);
        }
        select#country option[value="GB"] {
            background-image:url(images/gb.png);
        }
        .voice_url {
            font-size: 10px;
            width: 350px;
            height: 17px;
        }
        .incorrect-url {
            background-color:#FFFF5F;
            border:1px gray solid;
        }
    </style>

</head>





<body>

<div id="hld">

<div class="wrapper">		<!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->


<div class="block" id="filter-box" style="width:465px; margin:0 auto; margin-bottom: 25px;">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>
            Add Exception
        </h2>

    </div>		<!-- .block_head ends -->


    <div class="block_content" id="top_box">

        <div class="message success" id="successmsg" style="width: 86%; margin: 0px 0px 11px; display: none !important;"><p>Added Call Exception!</p></div>
        <div class="message errormsg" id="failmsg" style="width: 86%; margin: 0px 0px 11px; display: none !important;"><p>The numbers entered were not valid!</p></div>


            <center>If &nbsp;<input type="text" name="called_number" id="called_number" /> calls
            <select id="twilio_number" name="twilio_number">
                <?php
                    $companies = $db->getAllCompanies();
                    foreach($companies as $company)
                    {
                        $numbers = $db->getCompanyNumIntl($company['idx']);
                        ?><optgroup label="<?php echo $company['company_name']; ?>"><?php
                        foreach($numbers as $number){
                            if(!$number[1])
                                $number[0] = "1".$number[0];

                            ?><option value="<?php echo $number[0]; ?>"><?php echo Util::format_phone_us($number[0]); ?></option><?php
                        }
                    }
                ?>
            </select><br/><br/> then forward to <input type="text" name="forward_number" id="forward_number" /></center>
            <br/>
            <form action="#" onsubmit="return false;"><input type="submit" onclick="addException();" value="Add" id="search_num" class="submit mid" style="margin-bottom: 5px; float:right;"></form>

    </div>		<!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>
</div>		<!-- .block ends -->


<div class="block">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>
            Exceptions
        </h2>

    </div>		<!-- .block_head ends -->


    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%" id="exception_table" class="sortable">

            <thead>
            <tr>
                <th>Company</th>
                <th>Caller</th>
                <th>Calls</th>
                <th>Then Ring To</th>
                <th style="width: 115px !important;">Action</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach($exceptions as $exception){
                if(substr($exception->twilio_number,0,1)=="1")
                    $exception->twilio_number = substr($exception->twilio_number,1,strlen($exception->twilio_number));
                ?>
            <tr id="exception_<?echo $exception->id; ?>">
                <td><?php echo $db->getCompanyName($db->getCompanyOfNumber($exception->twilio_number)); ?></td>
                <td><?php echo Util::format_phone_us($exception->caller_number); ?></td>
                <td><?php echo Util::format_phone_us($exception->twilio_number); ?></td>
                <td><?php echo Util::format_phone_us($exception->forward_number); ?></td>
                <td><a href="#" onclick="deleteException(<?php echo $exception->id; ?>);" title="Delete Exception"><img style="vertical-align: middle;" src="images/delete.gif"></a></td>
            </tr>
                <?php } ?>
        </table>

    </div>		<!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>
</div>		<!-- .block ends -->

<?php include "include/footer.php"; ?>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.block .message').hide();
    });

    function addException()
    {
        $("#top_box").block({
            message: '<h1 style="color:#fff">Loading...</h1>',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .7,
                color: '#fff !important'
            }
        });
        var caller_number  = $("#called_number").val();
        var twilio_number  = $("#twilio_number").val();
        var forward_number = $("#forward_number").val();

        var response = JSONajaxCall({
            func: 'SYSTEM_ADD_CALL_EXCEPTION',
            data: {caller_number:caller_number,twilio_number:twilio_number,forward_number:forward_number}
        });

        response.done(function(data){
            $("#successmsg").hide();
            $("#failmsg").hide();
            if(data.result == "success")
            {
                var id      = data.id
                var company = data.company;
                var caller  = data.caller;
                var twilio  = data.twilio;
                var forward = data.forward;

                $("#successmsg").css("display","block");

                $("#exception_table > tbody:last").append(
                        '<tr id="exception_' + id + '">' +
                                '<td>' + company + '</td>' +
                                '<td>' + caller + '</td>' +
                                '<td>' + twilio + '</td>' +
                                '<td>' + forward + '</td>' +
                                '<td><a href="#" onclick="deleteException('+ id +')" title="Delete Exception"><img style="vertical-align: middle;" src="images/delete.gif"></a></td>' +
                        '</tr>' );

                $("#called_number").val("");
                $("#twilio_number").val("");
                $("#forward_number").val("");

                $("#top_box").unblock();
            }else{
                $("#failmsg").css("display","block");
                $("#top_box").unblock();
            }
        });
    }
    function deleteException(exceptionid)
    {
        var response = JSONajaxCall({
            func: 'SYSTEM_DELETE_CALL_EXCEPTION',
            data: exceptionid
        });
        response.done(function(){
            infoMsgDialog("Exception Deleted!");
            $("#exception_"+exceptionid).remove();
        });
    }
</script>
</body>
</html>