<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "keyword_locked_number";

//Including necessary files
require_once 'include/util.php';
require_once('include/Pagination.php');

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

$display_report = false;

if(@$_GET['sd']!="" && (@$_GET['ed']=="" || @$_GET['cm']==""))
{
?>
    <script type="text/javascript">
        alert('An error has occured. Redirecting.');
        window.location = "keyword_report.php";
    </script>
<?php
    exit;
}

$company_id = '' ;

$startDate  =   '';
$endDate    =   ''; 

global $TIMEZONE;
$tz = new DateTimeZone($TIMEZONE);

if(isset($_REQUEST['submit'])){
    if($_REQUEST['companyId'] != '-1')
    $company_id = $_REQUEST['companyId'];

    if($_REQUEST['day_report'] != 'none') {

        $dayReport = $_REQUEST['day_report'];

        switch ($dayReport) {

            case "today":
                $startDate = strtotime("midnight", time());
                $endDate = strtotime("midnight +1 day -1 sec", time());
            break;           

            case "yesterday":
                $startDate = strtotime('-1 day', time());
                $endDate   = strtotime("midnight -1 sec", time());
            break;             

            case "lastweek":
                $startDate = strtotime('-7 day', time());
                $endDate   = strtotime("midnight -1 sec", time());
            break;           

            case "lastmonth":
                $startDate = strtotime('-30 day', time());
                $endDate   = strtotime("midnight -1 sec", time());
            break;
        }

    } else {
        if (@$_REQUEST['start_date'] != "") {
            $startDate = trim($_REQUEST['start_date']);
            $startDate = strtotime($startDate);
            $endDate   = trim($_REQUEST['end_date']);
            $endDate   = strtotime($endDate);
        }
        elseif (@$_REQUEST['startDate'] != "") {
            $startDate = trim($_REQUEST['startDate']);
            $endDate   = trim($_REQUEST['endDate']);
        }
    }

    if (!empty($startDate)) {
        $start_date = new DateTime(date("Y-m-d", $startDate));
        $year = $start_date->format("Y");
        $month = $start_date->format("n");
        $day = $start_date->format("j");
        $d1 = strtotime($start_date->format("Y-m-d H:i:s"));
        $start_date->setTimezone($tz);
        $d2 = strtotime($start_date->format("Y-m-d H:i:s"));
        $seconds_difference = abs($d2 - $d1);

        $startDate = $startDate + $seconds_difference;
        $endDate = $endDate + $seconds_difference;
    }
}

$reportData2 = $db->getDefaultNumberLastSessionReport($company_id);

//reports functions

$pagination = new Pagination();

$page_n = isset($_GET['page']) ? (int) $_GET['page'] : 1;

$url="http://".$_SERVER['HTTP_HOST'].'/keyword_locked_number.php';

$pagination->setLink($url."?company_id=$company_id&startDate=".$startDate."&endDate=".$endDate."&day_report=".$_REQUEST['day_report']."&submit=1&page=%s");

$pagination->setPage($page_n);
$pagination->setSize(25);

$reportRowCount  =  $db->getLastSessionreportCount($startDate, $endDate, $company_id);

$pagination->setTotalRecords($reportRowCount);
$limit = $pagination->getLimitSql();                    
$report =  $db->getLockedNumber($company_id);

$reportData = $db->getLastSessionreport($startDate, $endDate, $company_id, $limit);

//companies name
$companies = $db->getAllCompanies();

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>       <!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                
                <div class="block" style="margin:0 auto;margin-bottom:25px;">

                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>

                        <h2>Keyword Locked Number </h2>
                    </div>

                    <div class="block_content">

                        <table class="filter-calls" style="margin:0 auto; width: 100%;">
                            <tbody>
                            <tr>
                                <td><h3 style="margin-bottom:0;">Current in use</h3></td>
                            </tr>
                            <tr>
                                <td style="border:0;">
                                    <table style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Number</th>
                                                <th>Visit Time</th>
                                                <th>Total</th>
                                                <th>IP Address</th>
                                                <th>Referrer</th>
                                                <th>Browser</th>
                                                <th>OS</th>
                                                <th>Keyword</th>
                                                <th>Source</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        <?php if(!empty($report)){?>
                                            <?php foreach($report as $r){?>
                                                <tr>
                                                    <td><?php echo $r['number'];?></td>
                                                    <td>
                                                        <span style="display: none;"><?php echo $r['timestamp']; ?></span>
                                                        <?php
                                                        $tz = new DateTimeZone($TIMEZONE);
                                                        $date = new DateTime(date("Y-m-d H:i:s", $r['timestamp']));
                                                        $date->setTimezone($tz);
                                                        ?>
                                                        <?php
                                                        echo $date->format(Util::STANDARD_LOG_DATE_FORMAT);?>
                                                    </td>
                                                    <td>
                                                        <span style="display: none;"><?php echo time() + ($r['lastseen'] - $r['timestamp']); ?></span>
                                                        <?php echo $r['lastseen'] - $r['timestamp'].' sec';?>
                                                    </td>
                                                    <td><?php echo $r['remote_addr'];?></td>
                                                    <td><?php echo $r['host'];?></td>
                                                    <td><?php echo Util::getBrowser($r['user_agent']);?></td>
                                                    <td><?php echo Util::getOS($r['user_agent']);?></td>
                                                    <td><?php echo $r['keywords'];?></td>
                                                    <td><?php echo $r['source'];?></td>
                                                </tr>
                                             <?php }?>
                                         <?php }else{?>
                                            <tr>
                                                <td colspan="9"><center>No Record Found</center></td>
                                            </tr>
                                         <?php }?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>      <!-- .block_content ends -->

                    <div class="bendl"></div>
                    <div class="bendr"></div>

                </div>      <!-- .block ends -->

            <div class="block">
                <div class="block_head">
                    <div class="bheadl"></div>
                    <div class="bheadr"></div>
                    <h2>Default Number Log</h2>
                </div>  <!-- .block_head ends -->
                <div class="block_content">
                    <table  class="sortable" cellpadding="0" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Number</th>
                            <th>Visit Time</th>
                            <th>Total Time</th>
                            <th>IP Address</th>
                            <th>Referrer</th>
                            <th>Browser</th>
                            <th>OS</th>
                            <th>Keyword</th>
                            <th>Source</th>
                        </tr>
                        </thead>
                        <tbody style="font-size:12px;">
                        <?php if(!empty($reportData2)) {
                            foreach($reportData2 as $key => $value) {
                                ?>
                            <tr>
                                <td><?php echo $value['number'];?></td>
                                <td><span style="display: none;"><?php echo $value['timestamp']; ?></span>
                                    <?php
                                    $tz = new DateTimeZone($TIMEZONE);
                                    $date = new DateTime(date("Y-m-d H:i:s", $value['timestamp']));
                                    $date->setTimezone($tz);
                                    ?>
                                    <?php
                                    echo $date->format(Util::STANDARD_LOG_DATE_FORMAT);?>
                                </td>
                                <td><span style="display: none;"><?php echo time() + ($value['lastseen'] - $value['timestamp']); ?></span><?php echo $value['lastseen'] - $value['timestamp'].' sec';?></td>
                                <td><?php echo $value['remote_addr'];?></td>
                                <td><?php echo $value['host'];?></td>
                                <td><?php echo Util::getBrowser($value['user_agent']);?></td>
                                <td><?php echo Util::getOS($value['user_agent']);?></td>
                                <td><?php echo $value['keywords'];?></td>
                                <td><?php echo $value['source'];?></td>
                            </tr>
                                <?php
                            }
                        } else {
                            ?>
                        <tr>
                            <td colspan="9"><center>No Record Found</center></td>
                        </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="bendl"></div>
                <div class="bendr"></div>

            </div>

                <div class="block">

                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Sessions</h2>

                    </div>  <!-- .block_head ends -->

                    <div class="block_content">

                            <center>
                            <form action="" method="get">

                                <label for="start_date">Start:</label> <input class="text small" type="text" id="start_date" name="start_date" value="<?php if(!empty($startDate)){ echo date('m/d/Y', $startDate);} ?>" style="width:120px; display: inline-block !important;" onclick="$('#day_report').val('none');" />
                                <label for="end_date" style="margin-left:16px;">End:</label> <input class="text small" type="text" id="end_date" value="<?php if(!empty($endDate)){ echo date('m/d/Y', $endDate);} ?>"  name="end_date" style="width:120px; display: inline-block !important; margin-right: 16px;" onclick="$('#day_report').val('none');" />

                                <select id="day_report" style="width:160px; display: inline-block !important;" name="day_report" onchange="selectExactDate(this);">
                                    <option value ="none">Select Day For Report</option>
                                    <option value ="today" <?php if($dayReport == 'today'){ ?> selected="selected"<?php } ?>>Today</option>
                                    <option value ="yesterday" <?php if($dayReport == 'yesterday'){ ?> selected="selected"<?php } ?>>Yesterday</option>
                                    <option value ="lastweek" <?php if($dayReport == 'lastweek'){ ?> selected="selected"<?php } ?>>Last Week</option>
                                    <option value ="lastmonth" <?php if($dayReport == 'lastmonth'){ ?> selected="selected"<?php } ?>>Last Month</option>
                                </select>

                                <select id="company_select" style="width:120px; display: inline-block !important;" name="companyId">
                                    <?php if(empty($company_id)){$company_id = '-1';} ?>

                                    <?php
                                    if($db->isUserAdmin($_SESSION['user_id']))
                                    {
                                        ?><option value="-1" <?php if($company_id == '-1'){ echo 'selected="selected"';} ?> >All Companies</option><?php echo "\n";
                                    }

                                    foreach($companies as $company)
                                    {
                                        ?><option value="<?php echo $company['idx']?>" <?php if($company_id == $company['idx']){ echo 'selected="selected"';} ?>><?php echo stripslashes(ucwords($company['company_name']));?></option><?php echo "\n";
                                    }
                                    ?>
                                </select>

                                <input id="submit_get" class="submit keyword_report" type="submit" value="Generate Report" name="submit">
                            </form>
                        </center>

                        <br /><br />

                        <table  class="sortable" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                   <th>Number</th>
                                    <th>Visit Time</th>
                                    <th class="{sorter: 'digit'}">Total Time</th>
                                    <th>IP Address</th>
                                    <th>Referrer</th>
                                    <th>Browser</th>
                                    <th>OS</th>
                                    <th>Keyword</th>
                                    <th>Source</th>
                                </tr>
                            </thead>

                            <tbody style="font-size:12px;">
                                <?php if(!empty($reportData)){?>
                                    <?php foreach($reportData as $key => $value){ ?>
                                        <tr>
                                            <td><?php echo $value['number'];?></td>
                                            <td><span style="display: none;"><?php echo $value['timestamp']; ?></span>
                                                <?php
                                                $tz = new DateTimeZone($TIMEZONE);
                                                $date = new DateTime(date("Y-m-d H:i:s", $value['timestamp']));
                                                $date->setTimezone($tz);
                                                ?>
                                                <?php
                                                echo $date->format(Util::STANDARD_LOG_DATE_FORMAT);?>
                                            </td>
                                            <td><span style="display: none;"><?php echo time() + ($value['lastseen'] - $value['timestamp']); ?></span><?php echo $value['lastseen'] - $value['timestamp'].' sec';?></td>
                                            <td><?php echo $value['remote_addr'];?></td>
                                            <td><?php echo $value['host'];?></td>
                                            <td><?php echo Util::getBrowser($value['user_agent']);?></td>
                                            <td><?php echo Util::getOS($value['user_agent']);?></td>
                                            <td><?php echo $value['keywords'];?></td>
                                            <td><?php echo $value['source'];?></td>
                                        </tr>
                                    <?php }?>
                                <?php }else{ ?>
                                <tr>
                                    <td colspan="9"><center>No Record Found</center></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>

                    <?php $navigation = $pagination->create_links();echo $navigation; ?> 
                    </div>

                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>

                <script type="text/javascript">

                    $(document).ready(function(e){
                        $("#export_btn").click(function(e){
                            $("#export").attr("src","include/excel_export_record.php"+window.location.search);
                        });

                        <?php if(!$display_report) { ?>
                            function getCookie2(c_name)
                            {
                                var i,x,y,ARRcookies=document.cookie.split(";");
                                for (i=0;i<ARRcookies.length;i++)
                                {
                                    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
                                    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
                                    x=x.replace(/^\s+|\s+$/g,"");

                                    if (x==c_name)
                                    {
                                        return unescape(y);
                                    }
                                }
                            }

                            if($("#company_select").val() != "none")
                            {
                                companySelected($("#company_select").val(),function(){
                                    var phone_code = getCookie2("selectedPhoneCode");
                                    if(!phone_code)
                                        phone_code = 0;
                                    else{
                                        console.log(phone_code);
                                        $("#phone_code option[value='"+phone_code+"']").attr("selected","selected");
                                    }
                                });
                            }
                        <?php } ?>

                    });

                    $("#submit_get").click(function(event) {
                        if($("#start_date").val() != "" && $("#end_date").val() == ""){
                            errMsgDialog("Please select a end date");
                            event.preventDefault();
                            return false;   
                        }

                        if($("#end_date").val() != "" && $("#start_date").val() == "" ){
                            errMsgDialog("Please select a start date");
                            event.preventDefault();
                            return false;   
                        }
                    });

                </script>
            </div>
        </div>
    </body>
</html>