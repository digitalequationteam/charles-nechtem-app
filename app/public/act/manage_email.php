<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
require_once('include/class.emailtemplates.php');
$page = "adminpage";

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

$op = @$_GET['op'];

if(isset($_POST['id']) && $_POST['id']!=""){
    $db = new DB();
    $stmt = $db->customExecute("SELECT * FROM email_templates WHERE id = ?");
    $stmt->execute(array($_POST['id']));
    $template = $stmt->fetch(PDO::FETCH_OBJ);
    $templatedata = json_decode($template->data);
    $template->data = str_replace("\r", htmlspecialchars("\\r"), $template->data); 
    $template->data = str_replace("\n", htmlspecialchars("\\n"), $template->data);

    $templatedata->subject = @$_POST['email_subject'];
    $templatedata->content = @$_POST['email_message'];

    $stmt = $db->customExecute("UPDATE email_templates SET `data` = ? WHERE id = ?");
    $stmt->execute(array(json_encode($templatedata),$_POST['id']));
    header("Location: manage_email.php?op=edit&id=".$_POST['id']."&saved=1");
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->
    <style type="text/css">
        .ui-dialog label, input {
            display: inline-block !important;
        }

        .cmf-skinned-select {
            display: inline-block !important;
        }

        div.wysiwyg ul.panel {
            width: 422px !important;
        }
        textarea {
            width: 98%;
            height: 140px;
            padding: 5px;
            background: #fefefe;
            border: 1px solid #bbb;
            font-family: "Lucida Grande", Verdana, sans-serif;
            font-size: 14px;
            color: #333;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            outline: none;
        }
        input.submit {
            width: 85px;
            height: 30px;
            line-height: 30px;
            background: url(images/btns.gif) top center no-repeat;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            text-transform: uppercase;
            color: #fff;
            text-shadow: 1px 1px 0 #0a5482;
            cursor: pointer;
            margin-right: 10px;
            vertical-align: middle;
        }
        input.submit.mid {
            width: 115px;
            background: url(images/btnm.gif) top center no-repeat;
        }
        label{
            font-size: 14px !important;
        }
        #token_select{
            width: 225px;
            display: inline-block;
            position: relative;
            float: right;
            top: 94px;
            height: 563px;
        }
        #token_list{
            width: 100%;
            height: 100%;
        }
        #preview {
            position: absolute;
            margin-left: 561px;
            margin-top: -35px;
        }
    </style>
</head>





<body>

<div id="hld">

    <div class="wrapper">		<!-- wrapper begins -->


        <?php include_once("include/nav.php"); ?>
        <!-- #header ends -->


<?php if($op==""){ ?>
            <div class="block">

                <div class="block_head">
                    <div class="bheadl"></div>
                    <div class="bheadr"></div>

                    <h2>
                        Email Templates
                    </h2>

                </div>		<!-- .block_head ends -->


                <div class="block_content">
                    <?php
                    $smtp_settings = $db->getVar("smtp_settings");
                    if (empty($smtp_settings)) {
                        $smtp_username = $db->getVar("smtp_username");
                        $smtp_password = $db->getVar("smtp_password");

                        if (!empty($smtp_username) && !empty($smtp_password)) {
                            $smtp_settings = array(
                                "email" => $smtp_username,
                                "username" => $smtp_username,
                                "password" => $smtp_password,
                                "host" => "smtp.gmail.com",
                                "port" => 465,
                                "ssl_tls" => "SSL"
                            );
                        }
                        else {
                            $smtp_settings = array();
                        }
                    }
                    else {
                        $smtp_settings = json_decode($smtp_settings, true);
                    }
                    ?>
                    <p>
                        SMTP authentication Settings:<br />
                        (Leave blank and save to disable)<br />
                    <form>
                        <label style="width: 130px; display: inline-block; font-size: 12px !important;">Name: </label> <input id="smtp_name" style="width: 360px;font-size: 10px;height: 14px;" value="<?php echo @$smtp_settings['name']; ?>" class="text small" type="text" />
                        <br clear="all" /><br />
                        <label style="width: 130px; display: inline-block; font-size: 12px !important;">Email Address: </label> <input id="smtp_email" style="width: 360px;font-size: 10px;height: 14px;" onkeyup="checkGenericEmailProvider(this.value);" value="<?php echo @$smtp_settings['email']; ?>" class="text small" type="text" />
                        <br clear="all" /><br />
                        <label style="width: 130px; display: inline-block; font-size: 12px !important;">SMTP Host: </label> <input id="smtp_host" style="width: 360px;font-size: 10px;height: 14px;" value="<?php echo @$smtp_settings['host']; ?>" class="text small" type="text" />
                        <br clear="all" /><br />
                        <label style="width: 130px; display: inline-block; font-size: 12px !important;">SMTP Port: </label> <input id="smtp_port" style="width: 360px;font-size: 10px;height: 14px;" value="<?php echo @$smtp_settings['port']; ?>" class="text small" type="text" />
                        <br clear="all" /><br />
                        <label style="width: 130px; display: inline-block; font-size: 12px !important;">SMTP Username: </label> <input id="smtp_username" style="width: 360px;font-size: 10px;height: 14px;" value="<?php echo @$smtp_settings['username']; ?>" class="text small" type="text" />
                        <br clear="all" /><br />
                        <label style="width: 130px; display: inline-block; font-size: 12px !important;">SMTP Password: </label> <input id="smtp_password" style="width: 360px;font-size: 10px;height: 14px;" value="<?php echo @$smtp_settings['password']; ?>" class="text small" type="password" />
                        <br clear="all" /><br />
                        <label style="width: 130px; display: inline-block; font-size: 12px !important;">Use SSL/TLS: </label> <input type="checkbox" id="smtp_ssl_tls" style="vertical-align: middle;" <?php if (!empty($smtp_settings['ssl_tls'])) { ?>checked="checked"<?php } ?> />
                        
                        <input type="submit" style="position: absolute;margin-top: 0; margin-left: 20px;" class="submit mid" id="SYSTEM_SAVE_SMTP_INFO" value="Save Settings" />
                    </form>
                    </p>

                    <table cellpadding="0" cellspacing="0" width="100%" class="sortable">

                        <thead>
                        <tr>
                            <th>Template Name</th>
                            <th>Subject</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                            <?php

                            $templates = $db->customQuery("SELECT * FROM email_templates;");
                                foreach($templates as $template)
                                {
                                    $template['data'] = str_replace("\r", htmlspecialchars("\\r"), $template['data']); 
                                    $template['data'] = str_replace("\n", htmlspecialchars("\\n"), $template['data']);
                                    $templatedata = json_decode($template['data']);
                                    ?>
                                    <tr>
                                        <td><?php echo $templatedata->name; ?></td>
                                        <td><?php echo $templatedata->subject; ?></td>
                                        <td><a href="manage_email.php?op=edit&id=<?php echo $template['id']; ?>">Edit</a></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                    </table>

                </div>		<!-- .block_content ends -->

                <script type="text/javascript">
                    function checkGenericEmailProvider(email_address) {
                        var top_email_providers = [
                            {
                                name: "Gmail",
                                terms: ["gmail.com"],
                                settings: {
                                    host: "smtp.gmail.com",
                                    port: 465,
                                    ssl: true
                                }
                            },
                            {
                                name: "Yahoo",
                                terms: ["yahoo.com", "yahoomail.com"],
                                settings: {
                                    host: "smtp.mail.yahoo.com",
                                    port: 465,
                                    ssl: true
                                }
                            },
                            {
                                name: "MSN",
                                terms: ["outlook.com", "hotmail.com", "live.com", "hotmail.com", "windowlive.com"],
                                settings: {
                                    host: "smtp.live.com",
                                    port: 587,
                                    ssl: true
                                }
                            },
                            {
                                name: "AOL",
                                terms: ["aol.com"],
                                settings: {
                                    host: "smtp.aol.com",
                                    port: 587,
                                    ssl: true
                                }
                            },
                            {
                                name: "Apple",
                                terms: ["me.com", "mac.com", "icloud.com"],
                                settings: {
                                    host: "smtp.mail.me.com",
                                    port: 587,
                                    ssl: true
                                }
                            }
                        ];
                        
                        for (var i in top_email_providers) {
                            var provider = top_email_providers[i];
                            for (var j in provider.terms) {
                                if (email_address.indexOf(provider.terms[j]) != -1) {
                                    $("#smtp_host").val(provider.settings.host);
                                    $("#smtp_port").val(provider.settings.port);
                                    $("#smtp_ssl_tls").prop("checked", provider.settings.ssl);

                                    break;
                                }
                            }
                        }

                        $("#smtp_username").val(email_address);
                    }
                </script>

                <div class="bendl"></div>
                <div class="bendr"></div>
            </div>		<!-- .block ends -->
<?php }elseif($op=="edit"){
   $id = @$_GET['id'];

   if($id == "" && $op=="edit")
   {
       ?>
       <script type="text/javascript">
           alert('There appears to be an error.');
           window.location='manage_email.php';
       </script>
       <?php
       die();
   }

            $template_types = new EmailTemplate();
            $res = $db->customExecute("select * from email_templates where id = ?");
            $res->execute(array($_GET['id']));
            $template = $res->fetch(PDO::FETCH_OBJ);
            $template->data = str_replace("\r", htmlspecialchars("\\r"), $template->data); 
            $template->data = str_replace("\n", htmlspecialchars("\\n"), $template->data);
            $templatedata = json_decode($template->data);
?>
    <script type="text/javascript">
        var type_list = <?php echo json_encode($template_types->getList()); ?>;

        function generateToken(id){
            var token_list = "";
            $(type_list).each(function (i,v){
                if(v[0]==id){
                    if(typeof v[3] != "undefined")
                        token_list = v[3];
                }
            });

            var options = "";
            $(token_list).each(function(i,v){
                options += "<option value='"+v+"'>" + v + "</option>";
            });
            $("#token_list").html(options).attr("size",token_list.length);
            $("#token_list").find("option").on('dblclick', function(){
                insertToken(this.value);
            });
        }
        function insertToken(token){
            $("#email_message").insertAtCaret(token);
        }
        function previewMail(){
            var msg = $("#email_message").val();
            var response = JSONajaxCall({
                func: "SYSTEM_GET_EMAIL_PREVIEW",
                data: {msg:msg,id:$("input[name='id']").val()}
            });
            response.done(function(data){
                console.log(data);

                var $dialog2 = $('<div></div>').html(data.msg).dialog({
                    modal: true,
                    autoOpen: true,
                    minHeight: 500,
                    maxHeight: 500,
                    minWidth: 500,
                    maxWidth: 500,
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-mail-closed" style="float:left; margin-right:5px;"></span>Preview'
                });
            });
        }
    </script>
    <div class="block">

        <div class="block_head">
            <div class="bheadl"></div>
            <div class="bheadr"></div>
            <ul style="float: right;">
                <li><a href="manage_email.php">Back</a></li>
            </ul>
            <h2>
                <?php if($op=="edit") echo "Edit Email Template";
                        else echo $template->name; ?>
            </h2>

        </div>		<!-- .block_head ends -->
        <div class="block_content">

            <?php if(@$_GET['saved']==1) { ?>
        <div class="message success" id="successmsg" style="width: 860px; margin: 0px 0px 11px; display: none !important;"><p>Saved email template!</p></div>
            <?php } ?>

            <form action="manage_email.php" method="post">
                <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                <input type="hidden" name="type" value="<?php echo $templatedata->type; ?>">
                <label for="name">Template Name:</label> <span id="name" style="vertical-align: bottom;font-size: 13px;"><?php echo $templatedata->name; ?></span>
                <br><br>

                <div id="token_select">
                    <select id="token_list">

                    </select>
                </div>

                <label for="email_subject">Email Subject</label><br>
                <input class="text small" type="text" id="email_subject" name="email_subject" value="<?php echo $templatedata->subject; ?>"/>
                <br><br>

                <label for="email_message">Email Message</label><br>
                <input type="button" id="preview" name="preview" class="submit mid" onclick="previewMail()" value="Preview" />
                <textarea id="email_message" name="email_message" style="width: 664px; margin: 0px; height: 553px; padding-top:10px;"><?php echo htmlspecialchars($templatedata->content); ?></textarea>
                <center>
                    <input type="submit" style="margin-top:20px;" class="submit mid" value="Save" />
                </center>
            </form>

            <br>
        </div>		<!-- .block_content ends -->

        <div class="bendl"></div>
        <div class="bendr"></div>
    </div>		<!-- .block ends -->
    <script type="text/javascript">
        generateToken(<?php echo $templatedata->type; ?>);
    </script>
<?php } ?>

        <?php include "include/footer.php"; ?>
        <script type="text/javascript">
            $.fn.insertAtCaret = function (tagName) {
                return this.each(function(){
                    if (document.selection) {
                        //IE support
                        this.focus();
                        sel = document.selection.createRange();
                        sel.text = tagName;
                        this.focus();
                    }else if (this.selectionStart || this.selectionStart == '0') {
                        //MOZILLA/NETSCAPE support
                        startPos = this.selectionStart;
                        endPos = this.selectionEnd;
                        scrollTop = this.scrollTop;
                        this.value = this.value.substring(0, startPos) + tagName + this.value.substring(endPos,this.value.length);
                        this.focus();
                        this.selectionStart = startPos + tagName.length;
                        this.selectionEnd = startPos + tagName.length;
                        this.scrollTop = scrollTop;
                    } else {
                        this.value += tagName;
                        this.focus();
                    }
                });
            };
        </script>
</body>
</html>