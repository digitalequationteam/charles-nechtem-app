<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "pool_detail";

//Including necessary files
require_once 'include/util.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

$company = $db->getAllPoolNumbers();
$companies = $db->getAllCompanyNames();
$companiesGroup = $db->getAllPoolNumbersGroupBy();
$pool_id = $db->getAllPoolid();

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>       <!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                
                <div class="block">

                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                         <h2>POOL DETAILS</h2>  
                    </div>      <!-- .block_head ends -->

                    <div class="block_content">

                        <table cellpadding="0" cellspacing="0" width="100%" class="sortable">

                            <thead>
                            <tr>
                                <th>Company</th>
                                <th>Pool ID</th>
                                <th>Keyword</th>
                                <th rowspan="6">Numbers</th>
                            </tr>
                            </thead>

                            <tbody>

                                <?php 
                                echo "<pre>";
                            
                                $result = array();
                                foreach ($company as $data) {
                                    $id = $data['pool_id'];
                                    if (isset($number[$id])) {
                                        $number[$id][] = $data['number'];
                                    } else {
                                     $number[$id] = array($data['number']);
                                    }
                                }   
                                foreach($companiesGroup as $value){
                                ?>
                                    <tr>
                                        <?php if($company_name != $value['company_name']){ $comp_name = $value['company_name']; }else{$comp_name=''; }?>
                                        <td><?php echo $comp_name; ?></td>
                                        <td><?php echo $value['pool_id'];?></td>
                                        <td><?php echo $value['keyword'];?></td>
                                        <td><?php foreach($number[$value['pool_id']] as $key => $r){ echo $r.','; }?></td>
                                        <?php $company_name = $value['company_name'];?>
                                    </tr>
                                <?php } ?>
                        </table>

                    </div>      <!-- .block_content ends -->

                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>      <!-- .block ends -->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>
    </body>
</html>