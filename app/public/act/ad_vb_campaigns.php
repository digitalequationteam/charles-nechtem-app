<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_voice_broadcast";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10007)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

$response = '';
$response_type = '';

//If action is set
if (isset($_GET['action'])) {
    //switching over action type
    switch ($_GET['action']) {
        case 'run_campaign':
            //Retrieving the campaign IDX
            $campaign_idx = $_GET['idx'];
            //Processing the voice broadcast campaigns
            ad_vb_process_voice_broadcast_campaign($campaign_idx, dirname(s8_get_current_webpage_uri()));
            $response = 'Processing of "<b>' . ad_vb_get_campaign_name($campaign_idx) . '</b>" campaign started';
            $response_type = 'success';
            break;

        //If action is to perform delete operation
        case 'confirm_delete':

            //If campaign ID is set in request URL
            if (isset($_GET['id'])):

                //Retrieving the campaign IDX to delete
                $del_campaign_idx = $_GET['id'];

                //Setting the alert response messages to show
                $response = 'Are you sure? You are trying to delete "<b>' . ad_vb_get_campaign_name($del_campaign_idx) . '</b>" campaign and associated data. <a href="ad_vb_campaigns.php?id=' . $del_campaign_idx . '&action=delete">Click here</a> to confirm.';
                $response_type = 'warning';

            //Ending condition checking campaign ID in request URL
            endif;

            break;

        //If action is to perform delete operation
        case 'delete':

            //If campaign ID is set in request URL
            if (isset($_GET['id'])):

                //Retrieving the campaign IDX to delete
                $del_campaign_idx = $_GET['id'];

                //Trying to delete the campaign
                ad_vb_delete_campaign($del_campaign_idx);

                //If campaign deleted successfully
                //Setting the success response messages to show
                $response = 'Campaign deleted!';
                $response_type = 'success';

            //Ending condition checking campaign ID in request URL
            endif;

            break;

        default:
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                <!--Initializing the html part that will display the campaign list-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Broadcasts</h2>
                        <div id="searchbox" style="margin-left:215px;">
                            <input type="text" id="search" value="<?php echo @$_GET['search']; ?>" placeholder="Campaign Name" />
                            <a href="#" onclick="window.location='ad_vb_campaigns.php';"><img src="images/ic_cancel.png" width="16" class="search-reset tt" style="<?php echo isset($_GET['search']) ? "display:inline-block !important;":""; ?>" title="Reset"></a>
                        </div>
                        <ul>
                            <li><a href="ad_vb_add_campaign.php" class="ad_vb_add_a_campaign">Add a Campaign</a></li>
                            <li><a href="ad_vb_campaigns.php">Campaigns List</a></li>
                            <li><a href="ad_contactlist_log.php">Contacts</a></li>
                            <li><a href="ad_vb_logs.php">Logs</a></li>
                        </ul>
                    </div>		
                    <!-- .block_head ends -->
<!--                    <script type="text/javascript" language="javascript">
                        $('.ad_vb_add_a_campaign').click(function(e) {
                            $('.ad_vb_add_a_campaign_popup').show();
                            e.preventDefault();
                        });
                    </script>
                    <div class="ad_vb_add_a_campaign_popup" id="facebox" style="display: none;margin: 0 auto;">
                        Lorem ipsum dolor color sit amet.
                    </div>-->

                    <div class="block_content">
                        <?php
                        if ($response_type == 'success'):
                            ?>
                            <div class="message success"><p><?php echo $response; ?></p></div>
                            <?php
                        elseif ($response_type == 'failure'):
                            ?>
                            <div class="message errormsg"><p><?php echo $response; ?></p></div>
                            <?php
                        elseif ($response_type == 'warning'):
                            ?>
                            <div class="message warning"><p><?php echo $response; ?></p></div>
                            <?php
                        elseif ($response_type == 'info'):
                            ?>
                            <div class="message info"><p><?php echo $response; ?></p></div>
                            <?php
                        endif;
                        ?>
                        <table cellpadding="0" cellspacing="0" width="100%" class="sortable">
                            <thead>
                                <tr>
                                    <th>Campaign Name</th>
                                    <th>Type</th>
                                    <th>Phone Number</th>
                                    <th>Status</th>
                                    <th>Progress</th>
                                    <th>Last Ran</th>
                                    <th>User</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $campaigns = ad_vb_get_campaigns(@$_GET['search'], isset($_GET['page']) ? $_GET['page'] : '1');

                                if (count($campaigns) > 0):
                                    foreach ($campaigns as $single_campaign_data):
                                        ?>
                                        <tr>
                                            <td>
                                                <?php
                                                echo '<a href="ad_vb_edit_campaign.php?idx=' . $single_campaign_data['idx'] . '">' . $single_campaign_data['campaign_name'] . '</a>';
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    switch($single_campaign_data['type']){
                                                        case "SMSBroadcast":
                                                            echo "SMS Broadcast";
                                                            break;
                                                        case "VoiceBroadcast":
                                                            echo "Voice Broadcast";
                                                            break;
                                                        case "AutoResponder":
                                                            echo "Auto Responder";
                                                            break;
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo format_phone($db->format_phone_db($single_campaign_data['phone_number']));
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $prog = ad_vb_get_campaign_progress($single_campaign_data['idx']);
                                                    $divprog = explode("/",$prog);
                                                    $status_label = $single_campaign_data['calls_status'];
                                                    if($status_label == "error"){
                                                        $status_label = '<a href="ad_vb_campaigns.php?action=run_campaign&idx='.$single_campaign_data['idx'].'" class="tt" title="It seems there was an error, or a timeout. Click to try again.">error</a>';
                                                    }

                                                    if (!empty($single_campaign_data['last_ran']) && $single_campaign_data['last_ran'] != "NA") {
                                                        if(strtotime("+3 minutes",$single_campaign_data['last_ran']) < strtotime("now") && $status_label == 'running'){
                                                            $status_label = '<a href="ad_vb_campaigns.php?action=run_campaign&idx='.$single_campaign_data['idx'].'" class="tt" title="It seems there was an error, or a timeout. Click to try again.">stalled</a>';
                                                        }
                                                    }

                                                    if($divprog[0] == $divprog[1] && $status_label != "completed"){
                                                        $status_label = "completed";
                                                        ad_vb_update_campaign_progress($single_campaign_data['idx'], $status_label);
                                                    }

                                                    echo $status_label;
                                                ?>
                                            </td>
                                            <td>
                                                <span style="display: none;">
                                                    <?php
                                                    echo $divprog[0];
                                                    ?>
                                                </span>
                                                <a href="ad_vb_logs.php?camp_idx=<?php echo $single_campaign_data['idx']; ?>">
                                                    <?php
                                                    if($single_campaign_data['type']=="AutoResponder")
                                                        echo "active";
                                                    else
                                                        echo $prog;
                                                    ?>
                                                </a>
                                            </td>
                                            <td>
                                                <?php
                                                if (!s8_is_str_blank($single_campaign_data['last_ran']) && strtolower($single_campaign_data['last_ran']) != 'na'):
                                                    echo Util::convertToLocalTZ("@".$single_campaign_data['last_ran'])->format(Util::STANDARD_LOG_DATE_FORMAT);
                                                else:

                                                    if (strtolower($single_campaign_data['calls_status']) == 'scheduled'):
                                                        echo 'Scheduled';
                                                    elseif (strtolower($single_campaign_data['calls_status']) == 'draft'):
                                                        echo 'Draft';
                                                    else:
                                                        echo 'NA';
                                                    endif;

                                                endif;
                                                ?>
                                            </td>
                                            <td><?php echo ad_get_user($single_campaign_data['user']); ?></td>
                                            <td style="text-align: right; padding-right:25px;">
                                                <?php
                                                if (strtolower($single_campaign_data['calls_status']) == 'draft') {
                                                    echo '<a href="' . s8_add_params_to_url(s8_get_current_webpage_uri(), array('action' => 'run_campaign', 'idx' => $single_campaign_data['idx'])) . '"><img src="images/play-icon.png" width="16" class="tt" title="Run Campaign"></a> ';
                                                }
                                                if($single_campaign_data['type']=="AutoResponder"){
                                                    echo '<a href="ad_vb_sequences_edit.php?campaign_id='.$single_campaign_data['idx'].'"><img class="tt" title="Edit Sequence" src="images/cog_edit.png" width="16"/></a>&nbsp;';
                                                }
                                                ?><a href="ad_vb_campaigns.php?id=<?php echo $single_campaign_data['idx']; ?>&action=confirm_delete"><img class="tt" title="Delete Campaign" src="images/delete.gif"></a>
                                            </td>
                                        </tr>
                                        <?php
                                    endforeach;
                                else:
                                    ?>
                                    <tr>
                                        <td>No records exist.</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php
                                endif;
                                ?>
                            </tbody>
                        </table>

                        <?php
                        $total_campaigns = ad_vb_get_campaigns_count(@$_GET['search']);
                        $campaigns_num_pages = ad_num_pages($total_campaigns);

                        //Detecing previous and next page ids
                        $previous_page = isset($_GET['page']) && $_GET['page'] > 1 ? ($_GET['page'] - 1) : 0;
                        $next_page = isset($_GET['page']) && $_GET['page'] > 1 ? ($_GET['page'] + 1) : 2;

                        if ($campaigns_num_pages > 0):
                            ?>
                            <div class="pagination right">
                                <?php
                                if ($campaigns_num_pages > 1 && $previous_page != 0):
                                    ?>
                                    <a href="?page=<?php echo $previous_page; if(isset($_GET['search'])) echo "&search=".urlencode($_GET['search']); ?>">&laquo;</a>
                                    <?php
                                endif;
                                for ($i = 1; $i <= $campaigns_num_pages; $i++):
                                    ?>
                                    <a href="?page=<?php echo $i; if(isset($_GET['search'])) echo "&search=".urlencode($_GET['search']); ?>" <?php echo (isset($_GET['page']) && $_GET['page'] == $i) || (!isset($_GET['page']) && $i == 1) ? 'class="active"' : ''; ?>><?php echo $i; ?></a>
                                    <?php
                                endfor;
                                if ($campaigns_num_pages > 1 && $campaigns_num_pages != @$_GET['page']):
                                    ?>
                                    <a href="?page=<?php echo $next_page; if(isset($_GET['search'])) echo "&search=".urlencode($_GET['search']); ?>">&raquo;</a>
                                    <?php
                                endif;
                                ?>
                            </div>
                            <!-- .pagination ends -->
                            <?php
                        endif;
                        ?>

                    </div>		<!-- .block_content ends -->
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Exiting the HTML code that will display the campaign list-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>

        <!--//Notification bar html-->
        <div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
        <script type="text/javascript" language="javascript">
            if ($ === undefined) {
                $ = jQuery;
            }

            /**
             * This function will display the notification message on screen.
             * @param {string} message The message to be displayed in notification bar
             * @returns {undefined}
             */
            function ad_display_message(message) {
                $('.ad_notification').html(message);
                $('.ad_notification').slideDown();
                setTimeout(function() {
                    $('.ad_notification').fadeOut(3000);
                }, 3000);

            }

            /**
             * This function will check whether string is blank or not
             * @param {string} str The value to be checked
             * @returns {Boolean} returns boolean TRUE or FALSE based on check performed.
             */
            function ad_is_str_blank(str) {
                if (str === '' || str === ' ' || str === null || str === undefined) {
                    return true;
                } else {
                    return false;
                }
            }

            //When document is ready to attache events to its elements
            $(document).ready(function() {

                //Attaching the function to submit event of text dial
                $('#ad_vb_test_dial, #ad_vb_dial_numbers').submit(function(e) {

                    //Retrieving the caller id
                    var caller_id = $('[name=ad_vb_caller_id]').val();
                    //Retrieving the number to dial
                    if ($(this).attr('id') === 'ad_vb_test_dial') {
                        var to_num = $('[name=ad_vb_test_dial_number]').val();
                    } else {
                        var to_num = $('[name=ad_vb_bulk_dial_numbers]').val();
                    }

                    //CHecking for blank values
                    if (!ad_is_str_blank(caller_id) && !ad_is_str_blank(to_num)) {

                        //Preparing the ajax URL
                        var ajax_url = '<?php echo dirname(s8_get_current_webpage_uri()); ?>/include/ad_auto_dialer_files/ad_ajax.php?to=' + to_num + '&caller_id=' + caller_id + '&userid=<?php echo @$_SESSION['user_id']; ?>';
                        //Firing the AJAX request
                        $.get(ajax_url, function(ajax_response) {

                            //Displaying the notification
                            ad_display_message(ajax_response);

                        });
                    } else {

                        //If blank value detected
                        //Throwing the error on screen
                        ad_display_message('Either caller ID not selected or No number entered.');

                    }
                    //Preventing the default action of form submission
                    e.preventDefault();
                });
                $(".tt").tooltipster({
                    position:'bottom',
                    theme:'.tooltipster-shadow'
                });
                $("#search").keyup(function(e){
                    if(e.which == 13){
                        var query = $("#search").val();
                        if(query.length>0)
                            window.location = "ad_vb_campaigns.php?search="+encodeURIComponent(query)+"&page=1";
                        else
                            window.location = "ad_vb_campaigns.php";
                    }
                });
            });

        </script>
    </body>
</html>
