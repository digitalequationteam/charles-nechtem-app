SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `ad_advb_cl_contacts` (
`idx` int(10) unsigned NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `business_name` varchar(200) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` text,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `zip` varchar(30) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `opt_out` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2423 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_advb_cl_link`
--

CREATE TABLE IF NOT EXISTS `ad_advb_cl_link` (
`id` int(11) unsigned NOT NULL,
  `contact_idx` int(255) NOT NULL,
  `list_idx` int(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `add_type` varchar(100) NOT NULL DEFAULT 'Added Manually'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2500 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_advb_cl_notes`
--

CREATE TABLE IF NOT EXISTS `ad_advb_cl_notes` (
`idx` int(10) unsigned NOT NULL,
  `contact_idx` int(10) NOT NULL,
  `note` text NOT NULL,
  `type` enum('Manual','VoiceBroadcast','AutoDialer') DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_advb_contacts`
--

CREATE TABLE IF NOT EXISTS `ad_advb_contacts` (
`idx` int(10) unsigned NOT NULL,
  `contact_of` varchar(30) NOT NULL,
  `campaign_idx` int(10) NOT NULL,
  `contact_number` varchar(45) NOT NULL,
  `call_status` varchar(20) NOT NULL DEFAULT 'pending',
  `phone_code` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=265 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_advb_contact_lists`
--

CREATE TABLE IF NOT EXISTS `ad_advb_contact_lists` (
`idx` int(10) unsigned NOT NULL,
  `list_name` varchar(30) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `last_updated` datetime NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(50) DEFAULT NULL,
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `optin_keyword` varchar(50) DEFAULT NULL,
  `optin_number` varchar(50) DEFAULT NULL,
  `optin_response` varchar(255) DEFAULT NULL,
  `vb_opt_out_content` longtext NOT NULL,
  `vb_opt_out_type` varchar(100) NOT NULL,
  `vb_opt_out_voice` varchar(100) NOT NULL,
  `vb_opt_out_language` varchar(100) NOT NULL,
  `vb_opt_out_key` varchar(1) NOT NULL,
  `sms_opt_out_message` varchar(160) NOT NULL,
  `sms_opt_out_trigger` varchar(20) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_ad_call_log`
--

CREATE TABLE IF NOT EXISTS `ad_ad_call_log` (
`idx` int(10) unsigned NOT NULL,
  `campaign_idx` int(10) DEFAULT NULL,
  `from` varchar(45) NOT NULL,
  `to` varchar(45) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL,
  `response` varchar(30) DEFAULT NULL,
  `code` int(50) DEFAULT '0',
  `user` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recording_url` longtext,
  `company_id` int(11) DEFAULT NULL,
  `CallSid` varchar(50) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_ad_campaigns`
--

CREATE TABLE IF NOT EXISTS `ad_ad_campaigns` (
`idx` int(10) unsigned NOT NULL,
  `campaign_name` varchar(100) DEFAULT NULL,
  `phone_number` varchar(45) NOT NULL,
  `contacts_list_idx` bigint(20) NOT NULL,
  `voicemail_messages` longtext NOT NULL,
  `voicemail_message_mp3` longtext,
  `voicemail_message_mp3_2` longtext NOT NULL,
  `voicemail_message_mp3_3` longtext NOT NULL,
  `voicemail_message_mp3_4` longtext NOT NULL,
  `call_script_tokens` longtext,
  `call_script_text` longtext,
  `calls_status` varchar(30) DEFAULT NULL,
  `progress` varchar(20) DEFAULT NULL,
  `last_ran` varchar(50) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `shared` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_callscripts`
--

CREATE TABLE IF NOT EXISTS `ad_callscripts` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `contents` longtext
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_vb_call_log`
--

CREATE TABLE IF NOT EXISTS `ad_vb_call_log` (
`idx` int(10) unsigned NOT NULL,
  `campaign_idx` int(10) DEFAULT NULL,
  `from` varchar(45) NOT NULL,
  `to` varchar(45) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL,
  `response` varchar(30) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `user` int(50) DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recording_url` longtext
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3673 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_vb_campaigns`
--

CREATE TABLE IF NOT EXISTS `ad_vb_campaigns` (
`idx` int(10) unsigned NOT NULL,
  `voicemail_only` tinyint(1) NOT NULL,
  `live_answer_only` bigint(20) NOT NULL,
  `campaign_name` varchar(100) DEFAULT NULL,
  `type` enum('VoiceBroadcast','SMSBroadcast','AutoResponder') NOT NULL DEFAULT 'VoiceBroadcast',
  `list_id` int(10) unsigned DEFAULT '0',
  `phone_number` varchar(45) NOT NULL,
  `sms_message` longtext NOT NULL,
  `live_answer_content` longtext NOT NULL,
  `live_answer_type` varchar(100) NOT NULL,
  `live_answer_voice` varchar(100) NOT NULL,
  `live_answer_language` varchar(100) NOT NULL,
  `voicemail_message_content` longtext NOT NULL,
  `voicemail_message_type` varchar(100) NOT NULL,
  `voicemail_message_voice` varchar(100) NOT NULL,
  `voicemail_message_language` varchar(100) NOT NULL,
  `allow_ivr` tinyint(1) NOT NULL,
  `ivr_key` char(1) NOT NULL,
  `ivr_phone_number` varchar(100) NOT NULL,
  `calls_status` varchar(30) DEFAULT NULL,
  `progress` varchar(20) DEFAULT NULL,
  `last_ran` varchar(50) DEFAULT NULL,
  `when_to_run` varchar(50) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `shared` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=87 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_vb_sequences`
--

CREATE TABLE IF NOT EXISTS `ad_vb_sequences` (
`id` int(11) unsigned NOT NULL,
  `campaign_id` int(11) unsigned NOT NULL,
  `type` enum('VoiceBroadcast','SMSBroadcast') NOT NULL DEFAULT 'VoiceBroadcast',
  `name` varchar(255) NOT NULL DEFAULT '',
  `days_after` int(11) NOT NULL DEFAULT '1',
  `time` varchar(40) NOT NULL DEFAULT '',
  `delay` varchar(20) NOT NULL,
  `sms_message` longtext,
  `live_answer_content` longtext NOT NULL,
  `live_answer_type` varchar(100) NOT NULL,
  `live_answer_voice` varchar(100) NOT NULL,
  `live_answer_language` varchar(100) NOT NULL,
  `voicemail_message_content` longtext NOT NULL,
  `voicemail_message_type` varchar(100) NOT NULL,
  `voicemail_message_voice` varchar(100) NOT NULL,
  `voicemail_message_language` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ad_vb_sequence_schedule`
--

CREATE TABLE IF NOT EXISTS `ad_vb_sequence_schedule` (
`id` int(11) unsigned NOT NULL,
  `sequence_id` int(11) unsigned NOT NULL,
  `cron_id` int(11) unsigned NOT NULL,
  `contact_id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2281 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `blacklists`
--

CREATE TABLE IF NOT EXISTS `blacklists` (
`idx` bigint(20) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `numbers` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `calls`
--

CREATE TABLE IF NOT EXISTS `calls` (
`Index` bigint(255) unsigned NOT NULL,
  `CallSid` char(34) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DateCreated` datetime DEFAULT NULL,
  `ToCountry` longtext COLLATE utf8_unicode_ci,
  `ToZip` longtext COLLATE utf8_unicode_ci,
  `ToState` longtext COLLATE utf8_unicode_ci,
  `ToCity` longtext COLLATE utf8_unicode_ci,
  `FromCountry` longtext COLLATE utf8_unicode_ci,
  `FromZip` longtext COLLATE utf8_unicode_ci,
  `ApiVersion` longtext COLLATE utf8_unicode_ci,
  `Direction` longtext COLLATE utf8_unicode_ci,
  `CallStatus` longtext COLLATE utf8_unicode_ci,
  `AccountSid` longtext COLLATE utf8_unicode_ci,
  `CallerID` longtext COLLATE utf8_unicode_ci,
  `CallTo` longtext COLLATE utf8_unicode_ci,
  `CallFrom` longtext COLLATE utf8_unicode_ci,
  `Status` longtext COLLATE utf8_unicode_ci,
  `StartTime` datetime DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `FromCity` longtext COLLATE utf8_unicode_ci,
  `FromState` longtext COLLATE utf8_unicode_ci,
  `DialCallSid` longtext COLLATE utf8_unicode_ci,
  `DialCallTo` longtext COLLATE utf8_unicode_ci,
  `DialCallStatus` longtext COLLATE utf8_unicode_ci,
  `DialCallDuration` longtext COLLATE utf8_unicode_ci,
  `RecordingUrl` longtext COLLATE utf8_unicode_ci,
  `PhoneCode` bigint(20) unsigned DEFAULT '0',
  `SpId` int(11) DEFAULT NULL,
  `participants` text COLLATE utf8_unicode_ci NOT NULL,
  `Department` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1881 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `calls_missed`
--

CREATE TABLE IF NOT EXISTS `calls_missed` (
`id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `call_sid` varchar(100) NOT NULL,
  `from` varchar(255) NOT NULL,
  `from_type` enum('agent','phone_number','contact') NOT NULL,
  `from_number` varchar(20) NOT NULL,
  `date_added` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1113 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_conferences`
--

CREATE TABLE IF NOT EXISTS `call_conferences` (
`id` bigint(20) NOT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `moved_to_conference` varchar(255) NOT NULL,
  `answered` tinyint(1) NOT NULL,
  `on_hold` tinyint(1) NOT NULL,
  `participants` longtext NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1268 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_hours`
--

CREATE TABLE IF NOT EXISTS `call_hours` (
  `company_id` int(11) NOT NULL,
  `day` varchar(10) NOT NULL,
  `open_time` time NOT NULL,
  `close_time` time NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_ivr`
--

CREATE TABLE IF NOT EXISTS `call_ivr` (
`id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `read_txt` varchar(255) NOT NULL,
  `upload_mp3` varchar(255) NOT NULL,
  `repeat_time` int(11) NOT NULL,
  `dated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_ivr_data`
--

CREATE TABLE IF NOT EXISTS `call_ivr_data` (
`id` int(11) NOT NULL,
  `wId` int(11) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_data` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=3877 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_ivr_multiple_numbers`
--

CREATE TABLE IF NOT EXISTS `call_ivr_multiple_numbers` (
`idx` int(10) unsigned NOT NULL,
  `wId` int(11) NOT NULL,
  `number` varchar(45) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=410 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_ivr_round_robin`
--

CREATE TABLE IF NOT EXISTS `call_ivr_round_robin` (
`idx` int(10) unsigned NOT NULL,
  `wId` int(11) NOT NULL,
  `number` varchar(45) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=351 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_ivr_widget`
--

CREATE TABLE IF NOT EXISTS `call_ivr_widget` (
`wId` int(11) NOT NULL,
  `pId` int(11) NOT NULL,
  `flowtype` varchar(20) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `nId` int(11) NOT NULL,
  `data` varchar(255) NOT NULL,
  `companyId` int(11) NOT NULL,
  `keypress` varchar(1) NOT NULL DEFAULT '-',
  `meta_data` varchar(255) NOT NULL,
  `temporary` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=5283 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_notes`
--

CREATE TABLE IF NOT EXISTS `call_notes` (
`idx` bigint(20) unsigned NOT NULL,
  `CallSid` longtext COLLATE utf8_unicode_ci NOT NULL,
  `NoteContents` longtext COLLATE utf8_unicode_ci NOT NULL,
  `User` longtext COLLATE utf8_unicode_ci NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `call_recordings`
--

CREATE TABLE IF NOT EXISTS `call_recordings` (
`id` bigint(20) NOT NULL,
  `CallSid` varchar(255) NOT NULL,
  `DialCallDuration` int(11) NOT NULL,
  `RecordingUrl` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `cf_multiple_numbers`
--

CREATE TABLE IF NOT EXISTS `cf_multiple_numbers` (
`idx` int(10) unsigned NOT NULL,
  `company_id` int(11) NOT NULL,
  `number` varchar(45) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `cf_round_robin`
--

CREATE TABLE IF NOT EXISTS `cf_round_robin` (
`idx` int(10) unsigned NOT NULL,
  `company_id` int(11) NOT NULL,
  `number` varchar(45) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
`id` int(11) unsigned NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `outgoing_numbers` varchar(1000) DEFAULT '',
  `bill_for` varchar(255) DEFAULT NULL,
  `lead_gen_bill_for` varchar(255) DEFAULT NULL,
  `lead_gen_bill_by_phone_codes` varchar(1000) DEFAULT NULL,
  `lead_gen_free_leads_number` int(11) NOT NULL DEFAULT '0',
  `lead_gen_amount_per_qualified_lead` decimal(11,2) NOT NULL DEFAULT '0.00',
  `lead_gen_amount_per_qualified_lead_currency` varchar(255) DEFAULT NULL,
  `call_tracking_flat_rate_per_month` decimal(11,2) NOT NULL DEFAULT '0.00',
  `call_tracking_per_number_fee` decimal(11,2) NOT NULL DEFAULT '0.00',
  `call_tracking_per_minute_fee` decimal(11,2) NOT NULL DEFAULT '0.00',
  `call_tracking_currency` varchar(255) DEFAULT NULL,
  `user_to_send_bill_to` varchar(255) DEFAULT NULL,
  `frequency` varchar(255) DEFAULT NULL,
  `bill_not_paid_past_due_days` int(11) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `invoice_email_template` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `cron_id` int(11) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
`idx` int(11) NOT NULL,
  `company_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `call_flow` int(11) NOT NULL DEFAULT '0',
  `assigned_number` longtext COLLATE utf8_unicode_ci,
  `whisper` longtext COLLATE utf8_unicode_ci,
  `whisper_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `whisper_voice` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `whisper_language` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `recording_notification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recording_notification_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `recording_notification_voice` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `recording_notification_language` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `recording_disable` int(1) DEFAULT '0',
  `international` tinyint(1) NOT NULL DEFAULT '0',
  `international_closed` bigint(20) NOT NULL,
  `blacklist_id` bigint(20) NOT NULL DEFAULT '0',
  `callerid` longtext COLLATE utf8_unicode_ci,
  `reset_call_time` int(11) DEFAULT '3600',
  `last_roundrobin` int(11) DEFAULT NULL,
  `sip_endpoint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sip_msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sip_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sip_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sip_header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `voicemail` tinyint(1) NOT NULL,
  `voicemail_closed` bigint(20) NOT NULL,
  `close_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `opt_hours` tinyint(1) NOT NULL DEFAULT '0',
  `forward_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `forward_sec` int(3) NOT NULL,
  `voicemail_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `voicemail_content` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `voicemail_text_voice` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `voicemail_text_language` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ring_count` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `company_num`
--

CREATE TABLE IF NOT EXISTS `company_num` (
  `company_id` int(11) NOT NULL,
  `number` longtext COLLATE utf8_unicode_ci NOT NULL,
  `international` tinyint(1) NOT NULL DEFAULT '0',
  `pool_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `company_phone_code`
--

CREATE TABLE IF NOT EXISTS `company_phone_code` (
`idx` bigint(20) unsigned NOT NULL,
  `company_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(1) DEFAULT '1',
  `order` int(11) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
`id` int(11) unsigned NOT NULL,
  `data` longtext
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `email_tracking_log`
--

CREATE TABLE IF NOT EXISTS `email_tracking_log` (
`id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `to_email` varchar(255) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `message_id` varchar(255) NOT NULL,
  `email_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject` varchar(255) NOT NULL,
  `body` longtext NOT NULL,
  `billing_tags` varchar(100) NOT NULL,
  `invoiced` tinyint(1) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `email_tracking_settings`
--

CREATE TABLE IF NOT EXISTS `email_tracking_settings` (
`id` int(11) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `clients` longtext NOT NULL,
  `distribution` varchar(20) NOT NULL,
  `emails` longtext NOT NULL,
  `billing_tag` varchar(20) NOT NULL,
  `client_email_tags` longtext NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
`id` int(11) unsigned NOT NULL,
  `client_id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `amount_currency` varchar(255) NOT NULL DEFAULT '',
  `amount_paid` decimal(11,2) NOT NULL DEFAULT '0.00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `invoice_details` text NOT NULL,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `is_offline` tinyint(1) NOT NULL DEFAULT '0',
  `is_pending` tinyint(1) NOT NULL DEFAULT '0',
  `is_declined` tinyint(1) NOT NULL DEFAULT '0',
  `sess_key` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ivr_detail`
--

CREATE TABLE IF NOT EXISTS `ivr_detail` (
`id` int(11) NOT NULL,
  `ivr_id` int(11) NOT NULL,
  `keypress` int(11) NOT NULL,
  `read_txt` varchar(255) NOT NULL,
  `upload_mp3` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `act` varchar(20) NOT NULL DEFAULT 'dial'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `keyword_detail`
--

CREATE TABLE IF NOT EXISTS `keyword_detail` (
`keyword_id` int(11) NOT NULL,
  `keywords` text NOT NULL,
  `medium` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `campaign` varchar(255) NOT NULL,
  `glcid` varchar(255) NOT NULL,
  `bouncerate` int(11) NOT NULL DEFAULT '100',
  `first_hit` int(2) NOT NULL DEFAULT '0',
  `remote_addr` varchar(255) NOT NULL,
  `referrer` varchar(500) NOT NULL,
  `content` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL,
  `sp_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `keyword_pool`
--

CREATE TABLE IF NOT EXISTS `keyword_pool` (
`sp_id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `number` varchar(512) NOT NULL,
  `inuse` int(2) NOT NULL DEFAULT '0',
  `timestamp` int(11) NOT NULL,
  `lastseen` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `is_default_number` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`message_id` int(11) NOT NULL,
  `CallSid` char(34) DEFAULT NULL,
  `message_frn_vmb_extension` varchar(8) NOT NULL,
  `message_date` datetime NOT NULL,
  `message_from` varchar(16) DEFAULT NULL,
  `message_audio_url` varchar(1024) DEFAULT NULL,
  `message_flag` int(1) DEFAULT '0',
  `RecordingDuration` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=125 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `opt_out`
--

CREATE TABLE IF NOT EXISTS `opt_out` (
`id` int(11) NOT NULL,
  `list_id` bigint(20) NOT NULL,
  `number_opted_out` varchar(255) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `outbound_calls`
--

CREATE TABLE IF NOT EXISTS `outbound_calls` (
`Index` bigint(255) unsigned NOT NULL,
  `CallSid` longtext COLLATE utf8_unicode_ci NOT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `ToCountry` longtext COLLATE utf8_unicode_ci,
  `ToZip` longtext COLLATE utf8_unicode_ci,
  `ToState` longtext COLLATE utf8_unicode_ci,
  `ToCity` longtext COLLATE utf8_unicode_ci,
  `FromCountry` longtext COLLATE utf8_unicode_ci,
  `FromZip` longtext COLLATE utf8_unicode_ci,
  `ApiVersion` longtext COLLATE utf8_unicode_ci,
  `Direction` longtext COLLATE utf8_unicode_ci,
  `CallStatus` longtext COLLATE utf8_unicode_ci,
  `AccountSid` longtext COLLATE utf8_unicode_ci,
  `CallerID` longtext COLLATE utf8_unicode_ci,
  `CallTo` longtext COLLATE utf8_unicode_ci,
  `CallFrom` longtext COLLATE utf8_unicode_ci,
  `Status` longtext COLLATE utf8_unicode_ci,
  `StartTime` datetime DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `FromCity` longtext COLLATE utf8_unicode_ci,
  `FromState` longtext COLLATE utf8_unicode_ci,
  `DialCallSid` longtext COLLATE utf8_unicode_ci,
  `DialCallStatus` longtext COLLATE utf8_unicode_ci,
  `AnsweredBy` longtext COLLATE utf8_unicode_ci,
  `MadeUsing` longtext COLLATE utf8_unicode_ci,
  `CallDuration` longtext COLLATE utf8_unicode_ci,
  `RecordingUrl` longtext COLLATE utf8_unicode_ci,
  `UserSource` longtext COLLATE utf8_unicode_ci,
  `PhoneCode` bigint(20) unsigned DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1452 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `phone_code_templates`
--

CREATE TABLE IF NOT EXISTS `phone_code_templates` (
`id` bigint(20) unsigned NOT NULL,
  `type` int(1) DEFAULT '1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `phone_codes` longtext NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `pools`
--

CREATE TABLE IF NOT EXISTS `pools` (
`id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `recordings`
--

CREATE TABLE IF NOT EXISTS `recordings` (
`id` bigint(20) NOT NULL,
  `unique_hash` varchar(100) NOT NULL,
  `value` varchar(500) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sms_forward_number`
--

CREATE TABLE IF NOT EXISTS `sms_forward_number` (
`idx` bigint(20) NOT NULL,
  `sms_forward_number_from` varchar(100) NOT NULL,
  `sms_forward_number_to` varchar(100) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `twilio_forward_number`
--

CREATE TABLE IF NOT EXISTS `twilio_forward_number` (
`id` bigint(20) unsigned NOT NULL,
  `twilio_number` varchar(45) NOT NULL DEFAULT '',
  `caller_number` varchar(45) NOT NULL DEFAULT '',
  `forward_number` varchar(45) NOT NULL DEFAULT ''
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `url_visited`
--

CREATE TABLE IF NOT EXISTS `url_visited` (
`url_id` int(11) NOT NULL,
  `sp_id` int(11) NOT NULL,
  `urls` longtext NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`idx` int(11) NOT NULL,
  `username` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `access_lvl` int(11) NOT NULL,
  `full_name` longtext COLLATE utf8_unicode_ci,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extra_notification_emails` longtext COLLATE utf8_unicode_ci NOT NULL,
  `notification_type` int(11) NOT NULL DEFAULT '0',
  `access_from` datetime DEFAULT NULL,
  `access_type` int(1) NOT NULL DEFAULT '0',
  `disable_outbound_link` int(1) NOT NULL DEFAULT '0',
  `allow_assign_phone_code` int(1) NOT NULL DEFAULT '0',
  `status` enum('ONLINE','AWAY','BUSY','OFFLINE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'OFFLINE',
  `status_update_time` bigint(20) NOT NULL,
  `last_call_sid` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `user_addon_access`
--

CREATE TABLE IF NOT EXISTS `user_addon_access` (
`idx` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `addon_id` bigint(20) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `user_company`
--

CREATE TABLE IF NOT EXISTS `user_company` (
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `user_incoming_num_access`
--

CREATE TABLE IF NOT EXISTS `user_incoming_num_access` (
  `user_id` int(11) unsigned NOT NULL,
  `number` longtext CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
`ui_id` int(11) NOT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  `referer` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `firstvisit` int(11) NOT NULL DEFAULT '0',
  `sp_id` int(11) NOT NULL,
  `km_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=612 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `user_outgoing_num_access`
--

CREATE TABLE IF NOT EXISTS `user_outgoing_num_access` (
  `user_id` int(11) unsigned NOT NULL,
  `number` longtext CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `voicemailbox`
--

CREATE TABLE IF NOT EXISTS `voicemailbox` (
  `vmb_extension` varchar(8) NOT NULL,
  `vmb_description` varchar(32) NOT NULL,
  `vmb_passcode` varchar(8) NOT NULL,
  `vmb_last_checked` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `zapier_subscriptions`
--

CREATE TABLE IF NOT EXISTS `zapier_subscriptions` (
`id` int(11) unsigned NOT NULL,
  `event` varchar(255) NOT NULL,
  `subscription_url` text NOT NULL,
  `target_url` text NOT NULL,
  `company_id` int(11) unsigned DEFAULT NULL,
  `campaign_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ad_advb_cl_contacts`
--
ALTER TABLE `ad_advb_cl_contacts`
 ADD PRIMARY KEY (`idx`), ADD UNIQUE KEY `phone_number` (`phone_number`);

--
-- Indexes for table `ad_advb_cl_link`
--
ALTER TABLE `ad_advb_cl_link`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `list_idx` (`list_idx`,`contact_idx`);

--
-- Indexes for table `ad_advb_cl_notes`
--
ALTER TABLE `ad_advb_cl_notes`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `ad_advb_contacts`
--
ALTER TABLE `ad_advb_contacts`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `ad_advb_contact_lists`
--
ALTER TABLE `ad_advb_contact_lists`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `ad_ad_call_log`
--
ALTER TABLE `ad_ad_call_log`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `ad_ad_campaigns`
--
ALTER TABLE `ad_ad_campaigns`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `ad_callscripts`
--
ALTER TABLE `ad_callscripts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_vb_call_log`
--
ALTER TABLE `ad_vb_call_log`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `ad_vb_campaigns`
--
ALTER TABLE `ad_vb_campaigns`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `ad_vb_sequences`
--
ALTER TABLE `ad_vb_sequences`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_vb_sequence_schedule`
--
ALTER TABLE `ad_vb_sequence_schedule`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blacklists`
--
ALTER TABLE `blacklists`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `calls`
--
ALTER TABLE `calls`
 ADD PRIMARY KEY (`Index`), ADD KEY `SpId` (`SpId`);

--
-- Indexes for table `calls_missed`
--
ALTER TABLE `calls_missed`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `call_conferences`
--
ALTER TABLE `call_conferences`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `call_ivr`
--
ALTER TABLE `call_ivr`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `call_ivr_data`
--
ALTER TABLE `call_ivr_data`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `wId` (`wId`,`meta_key`);

--
-- Indexes for table `call_ivr_multiple_numbers`
--
ALTER TABLE `call_ivr_multiple_numbers`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `call_ivr_round_robin`
--
ALTER TABLE `call_ivr_round_robin`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `call_ivr_widget`
--
ALTER TABLE `call_ivr_widget`
 ADD PRIMARY KEY (`wId`);

--
-- Indexes for table `call_notes`
--
ALTER TABLE `call_notes`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `call_recordings`
--
ALTER TABLE `call_recordings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cf_multiple_numbers`
--
ALTER TABLE `cf_multiple_numbers`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `cf_round_robin`
--
ALTER TABLE `cf_round_robin`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `company_phone_code`
--
ALTER TABLE `company_phone_code`
 ADD UNIQUE KEY `idx` (`idx`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_tracking_log`
--
ALTER TABLE `email_tracking_log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_tracking_settings`
--
ALTER TABLE `email_tracking_settings`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `company_id` (`company_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ivr_detail`
--
ALTER TABLE `ivr_detail`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keyword_detail`
--
ALTER TABLE `keyword_detail`
 ADD PRIMARY KEY (`keyword_id`);

--
-- Indexes for table `keyword_pool`
--
ALTER TABLE `keyword_pool`
 ADD PRIMARY KEY (`sp_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
 ADD PRIMARY KEY (`key`);

--
-- Indexes for table `opt_out`
--
ALTER TABLE `opt_out`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `campaign_id` (`list_id`,`number_opted_out`);

--
-- Indexes for table `outbound_calls`
--
ALTER TABLE `outbound_calls`
 ADD PRIMARY KEY (`Index`);

--
-- Indexes for table `phone_code_templates`
--
ALTER TABLE `phone_code_templates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pools`
--
ALTER TABLE `pools`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recordings`
--
ALTER TABLE `recordings`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique_hash` (`unique_hash`);

--
-- Indexes for table `sms_forward_number`
--
ALTER TABLE `sms_forward_number`
 ADD PRIMARY KEY (`idx`), ADD UNIQUE KEY `sms_forward_number_from` (`sms_forward_number_from`);

--
-- Indexes for table `twilio_forward_number`
--
ALTER TABLE `twilio_forward_number`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `url_visited`
--
ALTER TABLE `url_visited`
 ADD PRIMARY KEY (`url_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `user_addon_access`
--
ALTER TABLE `user_addon_access`
 ADD PRIMARY KEY (`idx`,`user_id`,`addon_id`), ADD UNIQUE KEY `idx` (`idx`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`ui_id`);

--
-- Indexes for table `voicemailbox`
--
ALTER TABLE `voicemailbox`
 ADD PRIMARY KEY (`vmb_extension`);

--
-- Indexes for table `zapier_subscriptions`
--
ALTER TABLE `zapier_subscriptions`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ad_advb_cl_contacts`
--
ALTER TABLE `ad_advb_cl_contacts`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_advb_cl_link`
--
ALTER TABLE `ad_advb_cl_link`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_advb_cl_notes`
--
ALTER TABLE `ad_advb_cl_notes`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_advb_contacts`
--
ALTER TABLE `ad_advb_contacts`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_advb_contact_lists`
--
ALTER TABLE `ad_advb_contact_lists`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_ad_call_log`
--
ALTER TABLE `ad_ad_call_log`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_ad_campaigns`
--
ALTER TABLE `ad_ad_campaigns`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_callscripts`
--
ALTER TABLE `ad_callscripts`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_vb_call_log`
--
ALTER TABLE `ad_vb_call_log`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_vb_campaigns`
--
ALTER TABLE `ad_vb_campaigns`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_vb_sequences`
--
ALTER TABLE `ad_vb_sequences`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ad_vb_sequence_schedule`
--
ALTER TABLE `ad_vb_sequence_schedule`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `blacklists`
--
ALTER TABLE `blacklists`
MODIFY `idx` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `calls`
--
ALTER TABLE `calls`
MODIFY `Index` bigint(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `calls_missed`
--
ALTER TABLE `calls_missed`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `call_conferences`
--
ALTER TABLE `call_conferences`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `call_ivr`
--
ALTER TABLE `call_ivr`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `call_ivr_data`
--
ALTER TABLE `call_ivr_data`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `call_ivr_multiple_numbers`
--
ALTER TABLE `call_ivr_multiple_numbers`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `call_ivr_round_robin`
--
ALTER TABLE `call_ivr_round_robin`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `call_ivr_widget`
--
ALTER TABLE `call_ivr_widget`
MODIFY `wId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `call_notes`
--
ALTER TABLE `call_notes`
MODIFY `idx` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `call_recordings`
--
ALTER TABLE `call_recordings`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `cf_multiple_numbers`
--
ALTER TABLE `cf_multiple_numbers`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `cf_round_robin`
--
ALTER TABLE `cf_round_robin`
MODIFY `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `company_phone_code`
--
ALTER TABLE `company_phone_code`
MODIFY `idx` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `email_tracking_log`
--
ALTER TABLE `email_tracking_log`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `email_tracking_settings`
--
ALTER TABLE `email_tracking_settings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ivr_detail`
--
ALTER TABLE `ivr_detail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `keyword_detail`
--
ALTER TABLE `keyword_detail`
MODIFY `keyword_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `keyword_pool`
--
ALTER TABLE `keyword_pool`
MODIFY `sp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `opt_out`
--
ALTER TABLE `opt_out`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `outbound_calls`
--
ALTER TABLE `outbound_calls`
MODIFY `Index` bigint(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `phone_code_templates`
--
ALTER TABLE `phone_code_templates`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pools`
--
ALTER TABLE `pools`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `recordings`
--
ALTER TABLE `recordings`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `sms_forward_number`
--
ALTER TABLE `sms_forward_number`
MODIFY `idx` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `twilio_forward_number`
--
ALTER TABLE `twilio_forward_number`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `url_visited`
--
ALTER TABLE `url_visited`
MODIFY `url_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `user_addon_access`
--
ALTER TABLE `user_addon_access`
MODIFY `idx` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `ui_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `zapier_subscriptions`
--
ALTER TABLE `zapier_subscriptions`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

--
-- Preset data
--
insert  into `users`(`idx`,`username`,`password`,`access_lvl`,`full_name`) values (2,'admin','21232f297a57a5a743894a0e4a801fc3',2,NULL);
INSERT INTO `email_templates` (`id`, `data`)
VALUES
  (1,'{\"name\":\"Invoice\",\"type\":1,\"subject\":\"[Invoice] For Services\",\"content\":\"Hello [fullname],\\r\\n\\r\\nYour invoice: [invoicenumber] ([invoiceperiod]) is ready. \\r\\n\\r\\nYour total: $[cashtotalinvoiceamount]\\r\\n\\r\\nPlease log in here: [url]\"}'),
  (2,'{\"name\":\"User Email\",\"type\":2,\"subject\":\"[Welcome] Call Tracking Platform\",\"content\":\"Hey [fullname],\\r\\n\\r\\nBelow you will find your login credentials for the call tracking platform.\\r\\n\\r\\nLogin URL: [url]\\r\\nUser Name: [username]\\r\\nPassword: [userpass]\\r\\n\\r\\nHave a great day!\"}'),
  (3,'{\"name\":\"Voicemail\",\"type\":3,\"subject\":\"[Voicemail Notification]\",\"content\":\"You have received a new voicemail from [from] to [company] [to] \\r\\n\\r\\nDuration: [duration]\\r\\nRecording URL: [recordingurl]\"}'),
  (5,'{\"name\":\"All Calls\",\"type\":5,\"subject\":\"[Call Notification] All Calls\",\"content\":\"You have received a new call from [from] to [company] [to] \\r\\n\\r\\nDuration: [duration]\\r\\nRecording URL: [recordingurl]\"}'),
  (6,'{\"name\":\"Calls Missed\",\"type\":6,\"subject\":\"[Call Notification] Missed Calls\",\"content\":\"You have received a missed call from [from] to [company] [to] \"}'),
  (7,'{\"name\":\"Calls Answered\",\"type\":7,\"subject\":\"[Call Notification] Answered Calls\",\"content\":\"You have received a new call from [from] to [company] [to] \\r\\n\\r\\nDuration: [duration]\\r\\nRecording URL: [recordingurl]\"}'),
  (8,'{\"name\":\"Calls Over 30 Seconds\",\"type\":8,\"subject\":\"[Call Notification] +30 Seconds\",\"content\":\"You have received a new call from [from] to [company] [to] \\r\\n\\r\\nDuration: [duration]\\r\\nRecording URL: [recordingurl]\"}'),
  (9,'{\"name\":\"Calls Over 45 Seconds\",\"type\":9,\"subject\":\"[Call Notification] +40 Seconds\",\"content\":\"You have received a new call from [from] to [company] [to] \\r\\n\\r\\nDuration: [duration]\\r\\nRecording URL: [recordingurl]\"}'),
  (10,'{\"name\":\"Calls Over 60 Seconds\",\"type\":10,\"subject\":\"[Call Notification] +60 Seconds\",\"content\":\"You have received a new call from [from] to [company] [to] \\r\\n\\r\\nDuration: [duration]\\r\\nRecording URL: [recordingurl]\"}'),
  (11,'{\"name\":\"Calls Over 90 Seconds\",\"type\":11,\"subject\":\"[Call Notification] +90 Seconds\",\"content\":\"You have received a new call from [from] to [company] [to] \\r\\n\\r\\nDuration: [duration]\\r\\nRecording URL: [recordingurl]\"}'),
  (12,'{\"name\":\"Calls Over 2 Minutes\",\"type\":12,\"subject\":\"[Call Notification] +2 Minutes\",\"content\":\"You have received a new call from [from] to [company] [to] \\r\\n\\r\\nDuration: [duration]\\r\\nRecording URL: [recordingurl]\"}');
INSERT INTO `options`(`key`,`value`) VALUES ( 'global_recordings','true');
INSERT INTO `options`(`key`,`value`) VALUES ( 'act_version','2.0');
INSERT INTO `options`(`key`,`value`) VALUES ( 'db_version','46');