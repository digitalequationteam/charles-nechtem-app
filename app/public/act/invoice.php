<?php
	require_once('include/util.php');
	require_once("include/db.client.php");
	require_once("include/db.company.php");
	require_once("include/db.invoice.php");

	require_once "include/class.phpmailer.php";
	require_once "include/class.emailtemplates.php";
	$smtp_username = $db->getVar("smtp_username");
	$smtp_password = $db->getVar("smtp_password");

	global $SITE_NAME;
	if($SITE_NAME!="")
	    $site_name = $SITE_NAME;
	else
	    $site_name = "Call Tracking";

	if(substr($_SERVER["HTTP_HOST"],0,4)=="www.")
	    $host = substr($_SERVER["HTTP_HOST"],4,strlen($_SERVER["HTTP_HOST"]-4));
	else
	    $host = $_SERVER["HTTP_HOST"];

	$host = "http://".$host.str_replace("/invoice.php","",$_SERVER['SCRIPT_NAME']);

    set_time_limit(0);
    ignore_user_abort(1);



	$db = new DB();

	if(@$lcl<2){
	    header("Location: index.php");
	    exit;
	}
	
	$clientId = $_GET["client_id"];

    if($clientId=="")
        die();
	
	$client = new Client();
	
	//get client info.
	$clientsArray = $client->getClients(array("client_id" => $clientId));
	
	$clientArray = $clientsArray[0];
	
	$companyId = $clientArray["company_id"];
	$billFor = $clientArray["bill_for"];
	$frequency = $clientArray["frequency"];

	//$now = strtotime("now");
	//$now = strtotime("2014-06-01 23:59:59");

	//echo(date("c", $startDate) . " - " . date("c", $endDate) . "<br><br>");
	//exit();
	
	//get outgoing numbers for the company.
	$outgoingNumbersArray = $clientArray["outgoing_numbers"];
	if (empty($outgoingNumbersArray)) $outgoingNumbersArray = array();

	$now = strtotime("now");

    if(isset($_GET['start_date']) && isset($_GET['end_date'])) {
    	$startDate = $_GET['start_date'];

		$tz = new DateTimeZone($TIMEZONE);
		$date = new DateTime(date("Y-m-d H:i:s", $startDate));
	    $year = $date->format("Y");
	    $month = $date->format("n");
	    $day = $date->format("j");
	    $d1 = strtotime($date->format("Y-m-d H:i:s"));
	    $date->setTimezone($tz);
	    $d2 = strtotime($date->format("Y-m-d H:i:s"));
	    $seconds_difference = abs($d2 - $d1);
	    $date->setDate($year, $month, $day);
	    $date->setTime(0, 0, 0);

	    $startDate = strtotime($date->format("Y-m-d H:i:s")) + $seconds_difference;

    	$endDate = $_GET['end_date'];
	    $date = new DateTime(date("Y-m-d H:i:s", $endDate));
	    $endDate = strtotime($date->format("Y-m-d H:i:s")) + $seconds_difference;
    }
    else {
		if (isset($_REQUEST['date_now']))
			$now = $_REQUEST['date_now'];

		$date = new DateTime();

		$endDate = $now;

		$tz = new DateTimeZone($TIMEZONE);
		$date = new DateTime(date("Y-m-d H:i:s", $endDate));
	    $year = $date->format("Y");
	    $month = $date->format("n");
	    $day = $date->format("j");
	    $d1 = strtotime($date->format("Y-m-d H:i:s"));
	    $date->setTimezone($tz);
	    $d2 = strtotime($date->format("Y-m-d H:i:s"));
	    $seconds_difference = abs($d2 - $d1);
	    $date->setDate($year, $month, $day);
	    $date->setTime(0, 0, 0);

	    $endDate = strtotime($date->format("Y-m-d H:i:s")) + $seconds_difference;
		
		switch ($frequency) {
			
			case "daily":
				$startDate = strtotime("-1 day", $endDate);
				break;
			case "weekly":
				$startDate = strtotime("-1 week", $endDate);
				break;
			case "biweekly":
				$startDate = strtotime("-2 week", $endDate);
				break;
			case "monthly":
				$startDate = strtotime("-1 month", $endDate);
				break;
	        case "bimonthly":
	            $startDate = strtotime("-2 month", $endDate);
		}
	}

	

	if (isset($_GET['act'])) {
		switch ($_GET['act']) {
			case 'getDelayPeriod':
				ob_end_clean();

				if ($clientArray['lead_gen_delay_period'] == 0)
					echo 1;
				elseif (empty($clientArray['lead_gen_delay_period']))
					echo 1;
				else {
					echo $clientArray['lead_gen_delay_period'];

					$invoice = new Invoice();
					$retn = $invoice->addInvoice(array(
			            "client_id" => $clientId,
			            "date" => date("Y-m-d H:i:s"),
			            "from_date" => date("Y-m-d H:i:s", $startDate),
			            "to_date" => date("Y-m-d H:i:s", $endDate),
			            "amount_currency" => $clientArray["lead_gen_amount_per_qualified_lead_currency"],
			            "amount_paid" => 0,
			            "invoice_details" => serialize(array())
			        ));

			        $stmt = $db->customExecute("UPDATE invoices SET is_pending = 1 WHERE id = ?");
			        $stmt->execute(array($retn[0]));
				}
			break;

			case 'sendEmailRegardingInvoice':

				$template = new EmailTemplate();
                $email_template = $template->getEmailTemplate(13);

				$msg = str_replace("[clientname]",               $clientArray['client_name'], $email_template->content);
				$msg = str_replace("[invoiceperiod]",          date("F j, Y", $startDate)." - ".date("F j, Y", $endDate), $msg);
				$msg = str_replace("[url]",                    $host."/call_report_review.php?client_id=".$clientId."&sd=".date("Y-m-d", $startDate)."&ed=".date("Y-m-d", $endDate),$msg);

				if ($clientArray['lead_gen_delay_period'] == 0)
                    $extra_days = 7;
                elseif (empty($clientArray['lead_gen_delay_period']))
                    $extra_days = 1;
                else
                    $extra_days = $clientArray['lead_gen_delay_period'];

                $automatic_send_date = date("F j, Y", $endDate + ($extra_days * 86400));

				$msg = str_replace("[system_create_date]",     $automatic_send_date,$msg);

				$subject = $email_template->subject;

				$email_params = array(
				    "subject" => $subject,
				    "msg" => $msg,
				    "emails" => array($clientArray['lead_gen_delay_email'])
				);
				Util::sendEmail($email_params);				
			break;
		}

		exit();
	}
	
	/*
echo("<br><br>");
	print_r($outgoingNumbersArray);
	echo("<br><br>");
*/
	
	$invoiceDetailsArray = array();
	
	if ($billFor == "lead-gen") {
		
		$leadGenBillFor = $clientArray["lead_gen_bill_for"];
		$leadGenBillByPhoneCodes = $clientArray["lead_gen_bill_by_phone_codes"];
		$leadGenFreeLeadsNumber = $clientArray["lead_gen_free_leads_number"];
		$leadGenAmountPerQualifiedLead = $clientArray["lead_gen_amount_per_qualified_lead"];
		$leadGenAmountPerQualifiedLeadCurrency = $clientArray["lead_gen_amount_per_qualified_lead_currency"];
		
		/*
echo("<br><br>");
		print_r($leadGenBillByPhoneCodes);
		echo("<br><br>");
*/
		
		$calls = $db->getCallsInRange($companyId, $startDate, $endDate, 0, $leadGenBillFor);
		
//echo "<pre>";
//echo("<br><br>");
		//print_r($calls);
//		echo("<br><br>");
		//exit();
		$callsFilteredArray = array();
		$phoneCodesArray = array();
		
		//loop through calls and filter them.
		foreach ($calls as $callArray) {
			
			//loop through outgoing numbers.
			if (count($outgoingNumbersArray) == 0) {
				//loop through phone codes.
				foreach ($leadGenBillByPhoneCodes as $phoneCodeId) {
					
					if ($phoneCodeId == "unassigned") {
						
						if ($callArray["PhoneCode"] == "" || $callArray["PhoneCode"] == "0") {
							
							$callsFilteredArray[] = $callArray;
							break;
						}
					}
					else if ($callArray["PhoneCode"] == $phoneCodeId) {
						
						$callsFilteredArray[] = $callArray;
						break;
					}
				}
			}
			else {
				foreach ($outgoingNumbersArray as $outgoingNumber) {
					
					if ($callArray["DialCallTo"] == "+{$outgoingNumber}") {
						
						//loop through phone codes.
						foreach ($leadGenBillByPhoneCodes as $phoneCodeId) {
							
							if ($phoneCodeId == "unassigned") {
								
								if ($callArray["PhoneCode"] == "" || $callArray["PhoneCode"] == "0") {
									
									$callsFilteredArray[] = $callArray;
									break;
								}
							}
							else if ($callArray["PhoneCode"] == $phoneCodeId) {
								
								$callsFilteredArray[] = $callArray;
								break;
							}
						}
					}
				}
			}
		}
		
		//echo count($callsFilteredArray);
		//print_r($callsFilteredArray);
		//exit();


		$stmt = $db->customExecute("SELECT * FROM email_tracking_log WHERE client_id = ? AND email_date > ? AND email_date < ? AND invoiced = 0");
    	if ($stmt->execute(array($clientId, $startDate, $endDate))) {
			$emails_log = $stmt->fetchAll();

			$email_invoice = new Invoice();
			$emailInvoiceDetailsArray = array();
			$emails_to_tags = array();

	    	foreach ($emails_log as $email_log) {
	    		$billing_tags = json_decode($email_log['billing_tags']);

	    		foreach ($billing_tags as $tag) {
	    			if (!isset($emails_to_tags["Email Lead - ".$email_log["to_email"]."@trackingyourleads.com | Tag: ".$tag->tag]))
		    			$emails_to_tags["Email Lead - ".$email_log["to_email"]."@trackingyourleads.com | Tag: ".$tag->tag] = array(
		    				"email" => $email_log["to_email"],
		    				"cost" =>$tag->cost,
		    				"quantity" => 1
		    			);
		    		else
		    			$emails_to_tags["Email Lead - ".$email_log["to_email"]."@trackingyourleads.com | Tag: ".$tag->tag]['quantity']++;
	    		}

	    		$db->customQuery("UPDATE email_tracking_log SET invoiced = 1 WHERE id = '".$email_log['id']."'");
	    	}

	    	foreach ($emails_to_tags as $key => $email_to_tags) {
				$emailInvoiceDetailsArray[] = array(
					"quantity" => $email_to_tags['quantity'],
					"desc" => $key,
					"unitPrice" => $email_to_tags['cost'],
					"totalPrice" => ($email_to_tags['quantity'] * $email_to_tags['cost'])
				);
	    	}

	    	if (sizeof($emailInvoiceDetailsArray) > 0) {
				$retn = $email_invoice->addInvoice(array(
		            "client_id" => $clientId,
		            "date" => date("Y-m-d H:i:s", $now),
		            "from_date" => date("Y-m-d H:i:s", $startDate),
		            "to_date" => date("Y-m-d H:i:s", $endDate),
		            "amount_currency" => $leadGenAmountPerQualifiedLeadCurrency,
		            "amount_paid" => 0,
		            "invoice_details" => serialize($emailInvoiceDetailsArray)
		        ));
	        }
    	}		

//echo("<br><br>");
//		print_r($callsFilteredArray);
//		echo("<br><br>");

		
		$leadsNumber = count($callsFilteredArray);
		
		$invoiceAmountCurrency = $leadGenAmountPerQualifiedLeadCurrency;
		
		$leadGenBillForString = "";
		
		switch ($leadGenBillFor) {
			
			case "all":
		    	$leadGenBillForString = "All";
		    	break;
            case "30":
            case "45":
            case "60":
            case "90":
                $leadGenBillForString = "Calls Over $leadGenBillFor seconds";
                break;
            case "120":
            case "180":
            case "240":
            case "300":
            case "360":
            case "420":
            case "480":
            case "540":
            case "600":
            case "1200":
            case "1800":
            case "2400":
                $leadGenBillForString = "Calls Over ".($leadGenBillFor/60)." minutes";
                break;
		    case "missed":
		    	$leadGenBillForString = "Missed Calls";
		    	break;
		    case "answered":
		    	$leadGenBillForString = "Answered Calls";
		    	break;
            default:
                $leadGenBillForString = "Calls";
                break;
		}
		
		$invoiceDetailsArray[] = array(
			"quantity" => $leadsNumber,
			"desc" => "Lead Gen - ".$leadGenBillForString,
			"unitPrice" => $leadGenAmountPerQualifiedLead,
			"totalPrice" => $leadsNumber * $leadGenAmountPerQualifiedLead
		);
		
		//if there are free leads.
		if ($leadGenFreeLeadsNumber > 0) {
			if($leadsNumber!=0)
			$invoiceDetailsArray[] = array(
				"quantity" => $leadGenFreeLeadsNumber,
				"desc" => "Lead Gen - Free Leads",
				"unitPrice" => $leadGenAmountPerQualifiedLead * -1,
				"totalPrice" => $leadGenFreeLeadsNumber * $leadGenAmountPerQualifiedLead * -1
			);
		}
	}
	else if ($billFor == "call-tracking-service") {
		
		$callTrackingFlatRatePerMonth = $clientArray["call_tracking_flat_rate_per_month"];
		$callTrackingPerNumberFee = $clientArray["call_tracking_per_number_fee"];
		$callTrackingPerMinuteFee = $clientArray["call_tracking_per_minute_fee"];
		$callTrackingCurrency = $clientArray["call_tracking_currency"];

        $numbers = $db->getCompanyNumIntl($companyId);

        $validInc = array();

        foreach($numbers as $number)
        {
            if($number[1])
                $validInc[] = "+".$number[0];
            else
                $validInc[] = "+1".$number[0];
        }
		
		$callsFilteredArray = array();
		$twilioNumbersArray = array();
		
		$callsSeconds = 0;

		if (count($outgoingNumbersArray) == 0) {
			$calls = $db->getCallsInRange($companyId, $startDate, $endDate);
			foreach ($calls as $callArray) {
            
	            if (count($outgoingNumbersArray) == 0) {
	                if(in_array($callArray['CallTo'], $validInc)) {
	                    $twilioNumbersArray[] = $callArrayy['CallTo'];
	                    $callsFilteredArray[] = $callArray;
						$callsSeconds += $callArray['DialCallDuration'];
	                }
				}
			}
		}
		else {
			$calls = Util::get_all_calls($startDate, $endDate);

			foreach ($calls as $callArray) {
				//loop through outgoing numbers.
				foreach ($outgoingNumbersArray as $outgoingNumber) {
					
					if ($callArray->To == "+{$outgoingNumber}") {
						
						$callsFilteredArray[] = $callArray;
						$callsSeconds += $callArray->Duration;
						
						break;
					}else{
	                    if(in_array($callArray->To,$validInc))
	                        $twilioNumbersArray[] = $callArray->To;
	                }
				}
			}
		}
		
		/*
echo("<br><br>");
		print_r($callsFilteredArray);
		echo("<br><br>");
*/
		
		$twilioNumbersArray = array_unique($twilioNumbersArray);
        //print_r($twilioNumbersArray);
		/*
echo("<br><br>");
		print_r($twilioNumbersArray);
		echo("<br><br>");
		
		echo("<br><br>");
		print_r(count($twilioNumbersArray));
		echo("<br><br>");
		
		echo("<br><br>");
		print_r($callsSeconds);
		echo("<br><br>");
*/
		
		$callsMinutes = floor($callsSeconds / 60);
		

//        echo("<br><br>");
//		print_r($callsMinutes);
//		echo("<br><br>");

		
		$invoiceAmountCurrency = $callTrackingCurrency;
		
		if (floatval($callTrackingFlatRatePerMonth) > 0) {
		
			$invoiceDetailsArray[] = array(
				"quantity" => 1,
				"desc" => "Call Tracking Service - Flat Rate Per Month",
				"unitPrice" => floatval($callTrackingFlatRatePerMonth),
				"totalPrice" => 1 * floatval($callTrackingFlatRatePerMonth)
			);
		}
		
		if ((count($twilioNumbersArray) * floatval($callTrackingPerNumberFee)) > 0) {
		
			$invoiceDetailsArray[] = array(
				"quantity" => count($twilioNumbersArray),
				"desc" => "Call Tracking Service - Per Number Fee",
				"unitPrice" => floatval($callTrackingPerNumberFee),
				"totalPrice" => (count($twilioNumbersArray) * floatval($callTrackingPerNumberFee))
			);
		}
		
		if (($callsMinutes * floatval($callTrackingPerMinuteFee))) {
		
			$invoiceDetailsArray[] = array(
				"quantity" => $callsMinutes,
				"desc" => "Call Tracking Service - Per Minute Fee",
				"unitPrice" => floatval($callTrackingPerMinuteFee),
				"totalPrice" => ($callsMinutes * floatval($callTrackingPerMinuteFee))
			);
		}
		
		/*$parentCallsArray = array();
		
		//now we need to get the parent calls.
		foreach ($callsFilteredArray as $filteredCallArray) {
			
			foreach ($calls as $callArray) {
			
				if ($filteredCallArray->ParentCallSid == $callArray->Sid) {
					
					$parentCallsArray[] = $callArray;
				}
			}
		}
		
		$callsFilteredArray = array_merge($callsFilteredArray, $parentCallsArray);
		
		echo("<br><br>");
		print_r($callsFilteredArray);
		echo("<br><br>");*/
		
		
	}

//    echo("<br><br>");
//	print_r($invoiceAmount);
//	echo("<br><br>");

    $totalPrice = 0;

    foreach($invoiceDetailsArray as $invoicedetail){
        $totalPrice = $totalPrice + $invoicedetail['totalPrice'];
    }

    $stmt = $db->customExecute("DELETE FROM invoices WHERE is_pending = 1 AND client_id = ?");
	$stmt->execute(array($clientId));

    if($totalPrice<=0)
        die();

    // We need to round the total if the currency does not support decimals..
    switch($invoiceAmountCurrency){
        case "HUF":
        case "TWD":
        case "JPY":
        {
            $totalPrice = round($totalPrice);
            break;
        }
    }

	$invoice = new Invoice();
    $client = new Client();
	
	//add invoice to the invoices table.

    if(!isset($_GET['invoice_id'])){
        //$client->updateClientStartDate($clientId,date("Y-m-d H:i:s", $endDate));

        $retn = $invoice->addInvoice(array(
            "client_id" => $clientId,
            "date" => date("Y-m-d H:i:s"),
            "from_date" => date("Y-m-d H:i:s", $startDate),
            "to_date" => date("Y-m-d H:i:s", $endDate),
            "amount_currency" => $invoiceAmountCurrency,
            "amount_paid" => 0,
            "invoice_details" => serialize($invoiceDetailsArray)
        ));
        $_GET['invoice_id'] = $retn[0];
        $key = $retn[1];
    }else{
        //$client->updateClientStartDate($clientId,date("Y-m-d H:i:s", $endDate));

        $key = $invoice->regenerateInvoice(array(
            "invoice_id" => $_GET['invoice_id'],
            "client_id" => $clientId,
            "date" => date("Y-m-d H:i:s"),
            "from_date" => date("Y-m-d H:i:s", $startDate),
            "to_date" => date("Y-m-d H:i:s", $endDate),
            "amount_currency" => $invoiceAmountCurrency,
            "amount_paid" => 0,
            "invoice_details" => serialize($invoiceDetailsArray)
        ));
    }
//
// Mail the user.
//

$templ = new EmailTemplate();
$template = $templ->getEmailTemplate(1);

$invoiceAmountCurrencySign = "\$";

switch($invoiceAmountCurrency){
    case "EUR":{
        $invoiceAmountCurrencySign = "&euro;";
        break;
    }
    case "GBP":{
        $invoiceAmountCurrencySign = "&pound;";
        break;
    }
    case "JPY":{
        $invoiceAmountCurrencySign = "&yen;";
        break;
    }
    case "BRL":{
        $invoiceAmountCurrencySign = "R\$";
        break;
    }
    case "CZK":{
        $invoiceAmountCurrencySign = "K&#269;";
        break;
    }
    case "DKK":{
        $invoiceAmountCurrencySign = "kr.";
        break;
    }
    case "HUF":{
        $invoiceAmountCurrencySign = "Ft";
        break;
    }
    case "ILS":{
        $invoiceAmountCurrencySign = "&#8362;";
        break;
    }
    case "MYR":{
        $invoiceAmountCurrencySign = "RM";
        break;
    }
    case "NOK":{
        $invoiceAmountCurrencySign = "kr";
        break;
    }
    case "PHP":{
        $invoiceAmountCurrencySign = "&#8369;";
        break;
    }
    case "PLN":{
        $invoiceAmountCurrencySign = "z&#322;";
        break;
    }
    case "SEK":{
        $invoiceAmountCurrencySign = "kr";
        break;
    }
    case "CHF":{
        $invoiceAmountCurrencySign = "Fr.";
        break;
    }
    case "TWD":{
        $invoiceAmountCurrencySign = "NT$";
        break;
    }
    case "THB":{
        $invoiceAmountCurrencySign = "&#3647;";
        break;
    }
    case "TRY":{
        $invoiceAmountCurrencySign = "TL";
        break;
    }
}

$msg = str_replace("[invoiceperiod]",          date("F j, Y", $startDate)." - ".date("F j, Y", $endDate),$template->content);
$msg = str_replace("[cashtotalinvoiceamount]", $totalPrice,$msg);
$msg = str_replace("[companyinfo]",            $db->getVar("company_info"),$msg);
$msg = str_replace("[invoicenumber]",          "INV-".str_replace("-", "", date("Y-m-d", $now)).@$_GET['invoice_id'],$msg);
$msg = str_replace("[currentdate]",            date("F j, Y", $now),$msg);
$msg = str_replace("[fullname]",               $clientArray['client_name'],$msg);
$msg = str_replace("[useraddress]",            $clientArray['address'],$msg);
$msg = str_replace("[usercity]",               $clientArray['city'],$msg);
$msg = str_replace("[userstate]",              $clientArray['state'],$msg);
$msg = str_replace("[userzip]",                $clientArray['zip_code'],$msg);
$msg = str_replace("[url]",                    $host."/billing_log_invoice.php?invoice_id=".str_replace("-", "", date("Y-m-d", $now)).@$_GET['invoice_id']."&rkey=".$key,$msg);

if($billFor == 'call-tracking-service'){
    $msg = str_replace("[numberofleads]","",$msg);
    $msg = str_replace("[cashperlead]","",$msg);
}else{
    $msg = str_replace("[numberofleads]",$invoiceDetailsArray['quantity'],$msg);
    $msg = str_replace("[cashperlead]",$clientArray["lead_gen_amount_per_qualified_lead"],$msg);
}


$email_params = array(
    "subject" => $template->subject,
    "msg" => $msg,
    "emails" => array($clientArray['user_to_send_bill_to'])
);
Util::sendEmail($email_params);
?>