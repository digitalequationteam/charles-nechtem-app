<?php
require_once('include/config.php');
require_once('include/db.php');
require_once('include/Services/Twilio.php');

$db = new DB();
smsin();
global $RECORDINGS;

if (!empty($_REQUEST)) {
    $sms_body = $_REQUEST['Body'];
    $from = $_REQUEST['From'];
    $to = $_REQUEST['To'];

    //Handle Opt-in && Opt-out
    $stmt = $db->customExecute("SELECT * FROM ad_advb_contact_lists WHERE optin_number = ?");
    if (substr($_REQUEST['To'], 0, 2) == "+1")
        $to = substr($_REQUEST['To'], 2, strlen($_REQUEST['To']) - 1);
    else
        $to = substr($_REQUEST['To'], 1, strlen($_REQUEST['To']) - 1);
    $stmt->execute(array($to));
    $lists = $stmt->fetchAll(PDO::FETCH_OBJ);
    foreach($lists as $list){
        $keyword = $list->optin_keyword;

        //TWILIO RESUBSCRIBE KEYWORDS
        $keyword2 = "START";
        $keyword3 = "YES";
        //TWILIO RESUBSCRIBE KEYWORDS

        if(strpos(strtolower($_REQUEST['Body']),$keyword)!==false || strpos(strtolower($_REQUEST['Body']),$keyword2)!==false || strpos(strtolower($_REQUEST['Body']),$keyword3)!==false){
            $opt_in_number = $db->format_phone_db($from);
            require_once('include/ad_auto_dialer_files/lib/ad_lib_funcs.php');
            $db->customQuery("DELETE FROM opt_out WHERE list_id = '".$list->idx."' AND number_opted_out = '".$opt_in_number."'");

            ad_advb_cl_add_contacts($list->idx,array(
                array($_REQUEST['From'])
            ), "Opted-In");
            if($list->optin_response!="") {
                $client = new Services_Twilio($AccountSid, $AuthToken);

                $msg = $list->optin_response;
                $msg .= "\n\n".$list->sms_opt_out_message;

                $global_sms_opt_out_message = ($db->getVar("global_sms_opt_out_message")) ? $db->getVar("global_sms_opt_out_message") : "Reply with CANCEL ALL to stop.";

                $msg .= "\n\n".$global_sms_opt_out_message;

                $client->account->messages->sendMessage($_REQUEST['To'], $_REQUEST['From'], html_entity_decode($msg));
            }
        }

        if (strpos(strtolower($sms_body), strtolower($list->sms_opt_out_trigger)) !== false) {
            $opt_out_number = $db->format_phone_db($from);
            $db->customQuery("REPLACE INTO opt_out(list_id, number_opted_out) VALUES('".$list->idx."', '".$opt_out_number."')");
        }
    }

    //CHECK GLOBAL OPT-OUT
    $global_sms_opt_out_trigger = ($db->getVar("global_sms_opt_out_trigger")) ? $db->getVar("global_sms_opt_out_trigger") : "CANCEL ALL";
    if (strpos(strtolower($sms_body), strtolower($global_sms_opt_out_trigger)) !== false) {
        $opt_out_number = $db->format_phone_db($from);
        $db->customQuery("UPDATE ad_advb_cl_contacts SET opt_out = '1' WHERE phone_number = '".$opt_out_number."'");
    }
    //CHECK GLOBAL OPT-OUT

    //Handle SMS forwarding
    $sms_forward_nr = $db->get_sms_forward($db->format_phone_db($to));
    if (!empty($sms_forward_nr)) {
        global $AccountSid, $AuthToken;
        $client = new Services_Twilio($AccountSid, $AuthToken);
        $sms_body = "Incoming SMS from ".$from.": ".$sms_body;
        echo $sms_body;
        $client->account->messages->sendMessage($to, $sms_forward_nr, html_entity_decode($sms_body));

        echo "<pre>";
        var_dump($result);
    }
}
?>