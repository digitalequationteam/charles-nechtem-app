<?php
require_once('include/config.php');
require_once('include/db.php');
header('Content-type: text/xml');
$db = new DB();
$whisp_url = "";

if(substr($_REQUEST['num'],0,2)=="+1")
    $company_id = $db->getCompanyOfNumber($db->format_phone_db($_REQUEST['num']));
else
    $company_id = $db->getCompanyOfNumber(substr($_REQUEST['num'],1,strlen($_REQUEST['num'])-1));

if($company_id!=FALSE)
{
	$settings = $db->getCompanySettings($company_id);
}
?>
<Response>
    <?php if($settings->whisper != ""){ ?>
    	<?php
    	if ($settings->whisper_type == "Text") {
            $settings->whisper_language = str_replace("|W", "", $settings->whisper_language);
            $settings->whisper_language = str_replace("|M", "", $settings->whisper_language);
    		?>
    			<Say voice="<?php echo $settings->whisper_voice; ?>" language="<?php echo $settings->whisper_language; ?>"><?php echo $settings->whisper ?></Say>
    		<?php
    	}
    	else {
    		if (substr($settings->whisper, 0, 4) != "http") {
				require_once('include/twilio_header.php');
    			$settings->whisper = dirname(s8_get_current_webpage_uri())."/audio/".$company_id."/".$settings->whisper;
    		}

            if (strpos($settings->whisper, "api.twilio.com") !== false && substr_count($settings->whisper, ".") == 2) {
                $settings->whisper = $settings->whisper.".wav";
            }
    		?>
    			<Play><?php echo str_replace(" ", "%20", $settings->whisper); ?></Play>
    		<?php
    	}
    	?>    	
    <?php } ?>
</Response>