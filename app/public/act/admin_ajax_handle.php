<?php
session_start();
require_once('include/util.php');
ignore_user_abort();

if(!isset($_SESSION['user_id']))
{
    header("Location: index.php");
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

// Function Table
$valid_functions = array(
    'SYSTEM_ADD_COMPANY'                            =>true,
    'SYSTEM_ADD_USER'                               =>true,
    'SYSTEM_EDIT_USER_COMPANY'                      =>true,
    'SYSTEM_DELETE_USER'                            =>true,
    'SYSTEM_EDIT_USER'                              =>false,
    'SYSTEM_VIEW_USER_DETAIL'                       =>true,
    'SYSTEM_PROMOTE_USER'                           =>true,
    'SYSTEM_EDIT_COMPANY_NUMBERS'                   =>true,
    'SYSTEM_DEMOTE_USER'                            =>true,
    'SYSTEM_GET_COMPANY_LIST'                       =>true,
    'SYSTEM_GET_PHONE_NUMBER_LIST'                  =>true,
    'SYSTEM_DELETE_COMPANY'                         =>true,
    'SYSTEM_EDIT_OUTGOING_NUMBER'                   =>true,
    'SYSTEM_GET_SETTINGS'                           =>true,
    'SYSTEM_EDIT_COMPANY_SETTINGS'                  =>true,
    'SYSTEM_ADD_BLACKLIST'                          =>true,
    'SYSTEM_DELETE_BLACKLIST'                       =>true,
    'SYSTEM_GET_BLACKLIST'                          =>true,
    'SYSTEM_GET_BLACKLISTS'                         =>true,
    'SYSTEM_EDIT_BLACKLIST'                         =>true,
    'SYSTEM_EDIT_BLACKLIST_COMPANY'                 =>true,
    'SYSTEM_EDIT_BLACKLIST_FOR_COMPANY'             =>true,
    'SYSTEM_GET_COMPANY_REPORT'                     =>true,
    'SYSTEM_GET_CALLERID'                           =>true,
    'SYSTEM_EDIT_CALLERID'                          =>true,
    'SYSTEM_SET_CAMPAIGN'                           =>true,
    'SYSTEM_DISABLE_RECORDINGS'                     =>true,
    'SYSTEM_MASK_RECORDINGS'                        =>true,
    'SYSTEM_GET_PHONE_CODES'                        =>true,
    'SYSTEM_SAVE_PHONE_CODE'                        =>true,
    'SYSTEM_TWILIO_GET_AVAIL_NUMBERS'               =>true,
    'SYSTEM_GET_LAT_LNG'                            =>true,
    'SYSTEM_TWILIO_PURCHASE_NUMBERS'                =>true,
    'SYSTEM_SAVE_DB_VAR'                            =>true,
    'SYSTEM_INTL_DIALTONE'                          =>true,
    'SYSTEM_RING_TONE'                              =>true,
    'SYSTEM_USER_PRIV'                              =>true,
    'SYSTEM_UPDATE_USER_PRIV'                       =>true,
    'SYSTEM_USER_GET_PHONES'                        =>true,
    'SYSTEM_USER_SAVE_PHONES'                       =>true,
    'SYSTEM_GET_USER_EMAIL_SETTINGS'                =>true,
    'SYSTEM_SAVE_USER_EMAIL_SETTINGS'               =>true,
    'SYSTEM_SAVE_SMTP_INFO'                         =>true,
    'SYSTEM_ADD_CALL_EXCEPTION'                     =>true,
    'SYSTEM_DELETE_CALL_EXCEPTION'                  =>true,
    'SYSTEM_TWILIO_GET_AVAIL_TOLLFREE'              =>true,
    'SYSTEM_GET_OUTGOING_NUMBER'                    =>true,
    'SYSTEM_GET_ROUNDROBIN'                         =>true,
    'SYSTEM_EDIT_ROUND_ROBIN'                       =>true,
    'SYSTEM_GET_MN'                                 =>true,
    'SYSTEM_EDIT_MN'                                =>true,
    'SYSTEM_ADD_PC_TEMPLATE'                        =>true,
    'SYSTEM_GET_PHONE_CODES_FROM_TEMPLATE'          =>true,
    'SYSTEM_SAVE_PHONE_CODE_TO_TEMPLATE'            =>true,
    'SYSTEM_DELETE_PC_TEMPLATE'                     =>true,
    'SYSTEM_GET_PC_TEMPLATES'                       =>true,
    'SYSTEM_REPLACE_PC'                             =>true,
    'SYSTEM_GET_EMAIL_PREVIEW'                      =>true,
    'SYSTEM_RESET_USER'                             =>true,
    'SYSTEM_REAUTH_ACT'                             =>true,
    'SYSTEM_OPENCNAM_DISABLE'                       =>true,
    'SYSTEM_OPENCNAM_UPDATE'                        =>true,
    'SYSTEM_OPENCNAM_TEST'                          =>true,
    'SYSTEM_OPENCNAM_COST'                          =>true,
    'SYSTEM_OPENCNAM_RUNNER'                        =>true,
    'SYSTEM_EDIT_SIP'                            	=>true,
    'SYSTEM_GET_SIP'                            	=>true,
    'SYSTEM_VOICE_MAIL'				            	=>true,
    'SYSTEM_TWILIO_GET_AVAIL_NUMBERS_AREACODE' 		=>true,
    'SYSTEM_SET_RESET_CALL_TIME'					=>true,
    'SYSTEM_TWILIO_UPDATE_VOICE_URL'				=>true,
    'SYSTEM_TWILIO_GET_AVAIL_NUMBERS_AREA'			=>true,
    'SYSTEM_TWILIO_DELETE_POOL'						=>true,
    'SYSTEM_TWILIO_DELETE_POOL_NUMBER'				=>true,
    'SYSTEM_TWILIO_GET_ADD_NUMBER_POOL'				=>true,
    'SYSTEM_TWILIO_ADDING_SELECTED_NUMBER_POOL' 	=>true,
    'SYSTEM_TWILIO_ADDING_SELECTED_NUMBER_NEW_POOL' =>true,
    'SYSTEM_SAVE_SMS_FORWARD_NUMBER'                =>true,
    'SYSTEM_SAVE_TRACKING_SETTINGS'                 =>true,
    'SYSTEM_SAVE_TRACKING_EMAIL_SAVE_TAGS'          =>true,
    'SYSTEM_SAVE_GLOBAL_OPT_OUT_SETTINGS'           =>true,
    'GET_RECORDING'                                 =>true,
    'SYSTEM_DELETE_VOICEMAIL'                       =>true,
    'SYSTEM_READ_VOICEMAIL'                         =>true
);

if(in_array($_POST['func'],$valid_functions) && isset($_POST['func']))
{
	$params = $_POST;
	unset($params['func']);
	if($valid_functions[$_POST['func']])
        $_POST['func']($params);
	else
		die("FUNCTION_NOT_EXIST:::".$_POST['func']);
}else{
		die("MISSING_FUNCTION_CALL:::");
}


function SYSTEM_OPENCNAM_RUNNER($params = ''){
    set_time_limit(36000);

    $db = new DB();
    $opencnam_AccountSid = $db->getVar("opencnam_AccountSid");
    $opencnam_AuthKey = $db->getVar("opencnam_AuthKey");

    if($opencnam_AccountSid=="" || $opencnam_AuthKey=="")
        genericFail(1);

    $stmt = $db->customExecute("SELECT DISTINCT `CallFrom` FROM calls WHERE CallTo != '' AND CallerID IS NULL;");
    $stmt2 = $db->customExecute("SELECT DISTINCT `CallFrom` FROM outbound_calls WHERE CallTo != '' AND CallerID IS NULL;");
    $stmt->execute();
    $stmt2->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);
    $data2 = $stmt->fetchAll(PDO::FETCH_OBJ);

    foreach($data as $number){
        $retn = json_decode($db->curlGetData("https://$opencnam_AccountSid:$opencnam_AuthKey@api.opencnam.com/v2/phone/".@$number->CallFrom."?format=json&ref=act"));
        $stmt = $db->customExecute("UPDATE calls SET CallerID = ? WHERE CallFrom = ?");
        $stmt->execute(array($retn->name,$number->CallFrom));
    }

    foreach($data2 as $number){
        $retn = json_decode($db->curlGetData("https://$opencnam_AccountSid:$opencnam_AuthKey@api.opencnam.com/v2/phone/".@$number->CallFrom."?format=json&ref=act"));
        $stmt = $db->customExecute("UPDATE outbound_calls SET CallerID = ? WHERE CallFrom = ?");
        $stmt->execute(array($retn->name,$number->CallFrom));
    }

    genericSuccess();
}
function SYSTEM_OPENCNAM_COST($params = ''){
    $db = new DB();
    $opencnam_rate = 0.004;
    $total_cost = 0;

    $stmt = $db->customExecute("SELECT DISTINCT `CallFrom` FROM calls WHERE CallTo != '' AND CallerID IS NULL;");
    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    if(count($data)==0)
        genericFail(1);

    foreach($data as $call){
        $total_cost = $total_cost+$opencnam_rate;
    }
    genericSuccess(array("cost"=>round((float)$total_cost,2),"count"=>count($data),"time"=>Util::formatTime(count($data))));
}
function SYSTEM_OPENCNAM_TEST($params = ''){
    $db = new DB();
    $AccountSid = $params['data']['accountsid'];
    $AuthKey = $params['data']['authkey'];
    $retn = $db->curlGetData("https://$AccountSid:$AuthKey@api.opencnam.com/v2/phone/+18002530000?ref=act");

    if(startsWith($retn,"CNAM for phone ") || $retn==""){
        genericFail(1);
    }else{
        genericSuccess();
    }
}
function SYSTEM_OPENCNAM_UPDATE($params = ''){
    $db = new DB();
    $AccountSid = $params['data']['accountsid'];
    $AuthKey = $params['data']['authkey'];

    $db->setVar("opencnam_AccountSid",$AccountSid);
    $db->setVar("opencnam_AuthKey",$AuthKey);

    genericSuccess();
}
function SYSTEM_OPENCNAM_DISABLE($params = ''){
    $db = new DB();

    $db->setVar("opencnam_AccountSid","");
    $db->setVar("opencnam_AuthKey","");

    genericSuccess();
}
function SYSTEM_GET_EMAIL_PREVIEW($params=''){
    require_once("include/class.emailtemplates.php");

    $emailtemplates = new EmailTemplate();
    $id = urldecode($params['data']['id']);
    $msg = urldecode($params['data']['msg']);

    die(json_encode(array("msg"=>$emailtemplates->getExample($id,$msg))));
}
function SYSTEM_REPLACE_PC($params='')
{
    $db = new DB();
    $id = $params['data']['templateid'];
    $company_id = $params['data']['company_id'];
    $type = $params['data']['type'];
    unset($_SESSION['sel_co_pc']);

    $query = $db->customExecute("SELECT * FROM phone_code_templates WHERE id = ?");

    if($query->execute(array($id))){
        $template_data = $query->fetch(PDO::FETCH_OBJ);
        $phone_codes = explode("\n",$template_data->phone_codes);
        $phone_codes_corrected = array();

        foreach($phone_codes as $pc)
        {
            if($pc=="")
                continue;

            $phone_codes_corrected[] = $pc;
        }

        if(count($phone_codes_corrected)>0){
            $query = $db->customExecute("DELETE FROM company_phone_code WHERE company_id = ? AND `type` = ?");

            if($query->execute(array($company_id,$type))){
                $idx = 0;
                foreach($phone_codes_corrected as $pc)
                {
                    $idx++;
                    $db->addPhoneCode($company_id,$pc,$idx,$type);
                }
                die(json_encode(array("result"=>"success")));
            }else{
                die(json_encode(array("result"=>"fail")));
            }
        }else{
            die(json_encode(array("result"=>"no_pc_in_template")));
        }
    }else{
        die(json_encode(array("result"=>"fail")));
    }
}
function SYSTEM_GET_PC_TEMPLATES($params='')
{
    $db = new DB();
    $type = $params['data'];
    $templates = array();

    foreach($db->getPhoneCodeTemplates() as $template)
    {
        if($template['type'] == $type)
            $templates[] = array("id"=>$template['id'],"name"=>$template['name']);
    }

    die(json_encode(array("templates"=>$templates)));
}
function SYSTEM_DELETE_PC_TEMPLATE($params='')
{
    $db = new DB();
    $id = $params['data'];

    $query = $db->customExecute("DELETE FROM phone_code_templates WHERE id = ?");

    if($query->execute(array($id)))
        die(json_encode(array("result"=>"success")));
    else
        die(json_encode(array("result"=>"fail")));
}
function SYSTEM_SAVE_PHONE_CODE_TO_TEMPLATE($params='')
{
    $db = new DB();
    $phone_codes = $params['data']['phone_codes'];
    $id = $params['data']['id'];

    $phone_code_db = "";

    if(is_array($phone_codes))
        foreach($phone_codes as $pc)
        {
            $phone_code_db .= $pc."\n";
        }
    $phone_code_db = substr($phone_code_db,0,strlen($phone_code_db)-1);
    $query = $db->customExecute("UPDATE phone_code_templates SET phone_codes = ? WHERE id = ?");

    if($query->execute(array($phone_code_db,$id)))
        die(json_encode(array("result"=>"success")));
    else
        die(json_encode(array("result"=>"fail")));
}
function SYSTEM_GET_PHONE_CODES_FROM_TEMPLATE($params='')
{
    $db = new DB();
    $id = $params['data'];
    unset($_SESSION['sel_co_pc']);

    $query = $db->customExecute("SELECT * FROM phone_code_templates WHERE id = ?");

    $query->execute(array($id));
    $data = $query->fetch(PDO::FETCH_OBJ);

    $phone_codes = "";
    $idx = 0;
    if(count($data)>0){
        $data = explode("\n",$data->phone_codes);
        foreach($data as $pc)
        {
            if($pc=="")
                continue;
            $idx++;
            $phone_codes .= "<tr><td>".$pc."</td><td></td><td><a href=\"#\" id=\"delete_pc\" onclick=\"$(this).parent().parent().remove()\" title=\"Delete Phone Code\"><img src=\"images/delete.gif\"></a></td></tr>";
        }
    }
    die(json_encode(array("result"=>"success","table_data"=>$phone_codes)));
}
function SYSTEM_ADD_PC_TEMPLATE($params='')
{
    $db = new DB();
    $name = $params['data']['name'];

    // 1 = incoming
    // 2 = outgoing
    $type = $params['data']['outgoing'];

    $query = $db->customExecute("INSERT INTO phone_code_templates (`name`,`type`,last_modified_by,last_modified_date) VALUES (?,?,?,NOW())");

    if($query->execute(array($name,$type,$_SESSION['user_id']))){
        die(json_encode(array("result"=>"success")));
    }else{
        die(json_encode(array("result"=>"fail","reason"=>$query->errorInfo())));
    }
}
function SYSTEM_EDIT_MN($params='')
{
    $db = new DB();
    $company_id = $db->getDb()->escape($params['company_id']);

    $numbers = $params['numbers'];

    $query = $db->customExecute("DELETE FROM cf_multiple_numbers WHERE company_id = ?");

    $query->execute(array($company_id));

    if(count($numbers)>0)
        foreach($numbers as $number)
        {
            $stmt = $db->customExecute("INSERT INTO cf_multiple_numbers (`company_id`,`number`) VALUES (?,?)");
            $stmt->execute(array($company_id,$number));
        }
    die(json_encode(array("result"=>"success")));
}
function SYSTEM_GET_MN($params='')
{
    $db = new DB();
    $company_id = $db->getDb()->escape($params['data']);

    $numbers = $db->customQuery("SELECT * FROM cf_multiple_numbers WHERE company_id = $company_id GROUP BY idx ASC");
    $data['numbers']=$numbers->fetchAll();
    die(json_encode($data));
}
function SYSTEM_EDIT_ROUND_ROBIN($params='')
{
    $db = new DB();
    $company_id = $db->getDb()->escape($params['company_id']);

    $numbers = $params['numbers'];
    $forward_number = $params['forward_number'];
    $forward_sec = $params['forward_sec'];
    $intl=$params['international_number'];

    $db->setfowardNumber($forward_number,$forward_sec ,$intl,$company_id);

    $query = $db->customExecute("DELETE FROM cf_round_robin WHERE company_id = ?");

    $query->execute(array($company_id));

    if(count($numbers)>0)
        foreach($numbers as $number)
        {
            $stmt = $db->customExecute("INSERT INTO cf_round_robin (`company_id`,`number`) VALUES (?,?)");
            $stmt->execute(array($company_id,$number));
        }
    die(json_encode(array("result"=>"success")));
}
function SYSTEM_GET_ROUNDROBIN($params='')
{
    $db = new DB();
    $company_id = $db->getDb()->escape($params['data']);

    $numbers = $db->customQuery("SELECT * FROM cf_round_robin WHERE company_id = $company_id GROUP BY idx ASC");

    $data['numbers']=$numbers->fetchAll();

    $settings = $db->getcompanySettings($params['data']);
    $data['forward_number']=$settings->forward_number;
    $data['forward_sec']=$settings->forward_sec;
    $data['international']=$settings->international;

    die(json_encode($data));
}
function SYSTEM_GET_SIP($params='')
{
    $db = new DB();
    $company_id = $params['data'];
    $company_settings = $db->getCompanySettings($company_id);
    die(json_encode(array("sip_endpoint"=>$company_settings->sip_endpoint,"sip_msg"=>$company_settings->sip_msg	,"sip_username"=>$company_settings->sip_username	,"sip_password"=>$company_settings->sip_password	,"sip_header"=>$company_settings->sip_header	)));
}
function SYSTEM_GET_OUTGOING_NUMBER($params='')
{
    $db = new DB();
    $company_id = $params['data'];
    $company_settings = $db->getCompanySettings($company_id);

    die(json_encode(array("number"=>$company_settings->assigned_number,"intl"=>$company_settings->international,"voicemail"=>$company_settings->voicemail)));
}
function SYSTEM_TWILIO_GET_AVAIL_TOLLFREE($params='')
{
    global $AccountSid, $AuthToken, $ApiVersion;

    $near_number = $params['data']['near_number'];
    $contains    = $params['data']['contains'];
    $country     = $params['data']['country'];

    $SearchParams = array();
    $SearchParams['InPostalCode'] = !empty($postal_code)? trim($postal_code) : '';
    $SearchParams['NearNumber'] = !empty($near_number)? trim($near_number) : '';
    $SearchParams['Contains'] = !empty($contains)? trim($contains) : '' ;

    $client = new TwilioRestClient($AccountSid, $AuthToken);

    try {

        $response = $client->request("/$ApiVersion/Accounts/$AccountSid/AvailablePhoneNumbers/" . $country . "/TollFree", "GET", $SearchParams);

        /* If we did not find any phone numbers let the user know */
        if(empty($response)) {
            die(json_encode(array("result"=>"none")));
        }

        $available_numbers = array();
        if(!$response->IsError)
        {
            foreach($response->ResponseXml->AvailablePhoneNumbers->AvailablePhoneNumber as $number)
            {
                $available_numbers[] = $number;
            }
        }else{
            die(json_encode(array("result"=>"error", "details"=>$response, "err_msg"=>$response->ErrorMessage)));
        }

        if(count($available_numbers)==0)
            die(json_encode(array("result"=>"none")));

        die(json_encode(array("result"=>"success","data"=>$available_numbers)));

    } catch (Exception $e) {
        die(json_encode(array("result"=>"error")));
    }
}
function SYSTEM_EDIT_SIP($params='')
{
    $db = new DB();
    $result = $db->setSIP($params['data']['sip_endpoint'],$params['data']['sip_msg'],$params['data']['sip_username'],$params['data']['sip_password'],$params['data']['sip_header'],$params['data']['company_id']);
	if($result)
	{
        die("SUCCESS:::");
	}else{
        die("FAILURE:::");
	}
}
function SYSTEM_ADD_CALL_EXCEPTION($params='')
{
    $db = new DB();
    $caller_number = $params['data']['caller_number'];
    $twilio_number = $params['data']['twilio_number'];
    $forward_number = $params['data']['forward_number'];
    $error = false;

    if(($caller_number && $twilio_number && $forward_number) != ""
        && is_numeric($caller_number) && is_numeric($twilio_number) && is_numeric($forward_number))
    {
        $id = $db->add_forward_num($twilio_number,$caller_number,$forward_number);
    }else{
        $error = true;
    }

    if($error)
        die(json_encode(array("result"=>"error")));
    else{
        if(substr($twilio_number,0,1)=="1")
            $twilio_number = substr($twilio_number,1,strlen($twilio_number));

        $company_name = $db->getCompanyName($db->getCompanyOfNumber($twilio_number));
        $caller  = Util::format_phone_us($caller_number);
        $twilio  = Util::format_phone_us($twilio_number);
        $forward = Util::format_phone_us($forward_number);

        die(json_encode(array(
            "result"=>"success",
            "id"=>$id,
            "company"=>$company_name,
            "caller"=>$caller,
            "twilio"=>$twilio,
            "forward"=>$forward
        )));
    }

}
function SYSTEM_DELETE_CALL_EXCEPTION($params='')
{
    $db = new DB();
    $exceptionid = $params['data'];

    $db->delete_forward_num($exceptionid);

    die(json_encode(array("result"=>"success")));
}
function SYSTEM_SAVE_SMTP_INFO($params='')
{
    $db = new DB();
    require_once "include/class.phpmailer.php";

    $smtp_name = $params['data']['smtp_name'];
    $smtp_email = $params['data']['smtp_email'];
    $smtp_host = $params['data']['smtp_host'];
    $smtp_port = $params['data']['smtp_port'];
    $smtp_username = $params['data']['smtp_username'];
    $smtp_password = $params['data']['smtp_password'];
    $smtp_ssl_tls = $params['data']['smtp_ssl_tls'];

    $empty_items = 0;

    foreach ($params['data'] as $item) {
        if (empty($item)) {
            $empty_items++;
        }
    }

    if ($empty_items == 7) {
         $db->setVar('smtp_settings', json_encode(array()));
        die(json_encode(array("result"=>"success")));
    }

    $email_sent = array('email'=>'not_sent','error'=>'');
    $ssl_tls = "";

    try {
        $mail = new PHPMailer(true); 
        $mail->IsSMTP(); 
    
        $mail->SMTPDebug  = 0;                     
        $mail->SMTPAuth   = true;                  
        $mail->Host       = $smtp_host;
        $mail->Port       = $smtp_port;
        $mail->SMPTAuth = true;
        if($smtp_ssl_tls == 1)
        {
            $mail->SMTPSecure = "ssl";
            $ssl_tls = "ssl";
        }
        $mail->Username   = $smtp_username;
        $mail->Password   = $smtp_password;

        $mail->SetFrom($smtp_email, $smtp_name);
        $mail->AddReplyTo($smtp_email, $smtp_name);
        $mail->Subject = 'ACT Test Email Confirmation';
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; 
        $mail->MsgHTML('<div>This is a test email for checking your SMTP configurations for ACT.<br />
                            Receiving this email confirms your email settings are configured correctly.
                        ======</div>');

        try {
            $mail->AddAddress($smtp_email, '');
            $mail->Send();

          $email_sent['email'] = 'sent';
        } catch (phpmailerException $e) {
          $email_sent['email'] = 'not_sent';
          $email_sent['error'] = $e->errorMessage(); 
        } catch (Exception $e) {
          $email_sent['email'] = 'not_sent';
          $email_sent['error'] = $e->getMessage(); 
        }

        if ($email_sent["email"] == "not_sent") {
            $mail = new PHPMailer(true); 
            $mail->IsSMTP(); 
        
            $mail->SMTPDebug  = 0;                     
            $mail->SMTPAuth   = true;                  
            $mail->Host       = $smtp_host;
            $mail->Port       = $smtp_port;
            $mail->SMPTAuth = true;
            if($smtp_ssl_tls == 1)
            {
                $mail->SMTPSecure = "tls";
                $ssl_tls = "tls";
            }
            $mail->Username   = $smtp_username;
            $mail->Password   = $smtp_password;

            $mail->SetFrom($smtp_email, $smtp_name);
            $mail->AddReplyTo($smtp_email, $smtp_name);
            $mail->Subject = 'ACT Test Email Confirmation';
            $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; 
            $mail->MsgHTML('<div>This is a test email for checking your SMTP configurations for ACT.<br />
                                Receiving this email confirms your email settings are configured correctly.
                            ======</div>');

            try {
                $mail->AddAddress($smtp_email, '');
                $mail->Send();

              $email_sent['email'] = 'sent';
            } catch (phpmailerException $e) {
              $email_sent['email'] = 'not_sent';
              $email_sent['error'] = $e->errorMessage();
            } catch (Exception $e) {
              $email_sent['email'] = 'not_sent';
              $email_sent['error'] = $e->getMessage();
            }
        }
    }
    catch (Exception $e) {
        $email_sent = array('email'=>'not_sent','error'=>$e->getMessage());
    }

    if ($email_sent['email'] == "sent") {
        $smtp_settings = array(
            "name" => $smtp_name,
            "email" => $smtp_email,
            "host" => $smtp_host,
            "port" => $smtp_port,
            "username" => $smtp_username,
            "password" => $smtp_password,
            "ssl_tls" => $ssl_tls
        );

        $db->setVar('smtp_settings', json_encode($smtp_settings));
        die(json_encode(array("result"=>"success")));
    }
    else {
        die(json_encode(array("result"=>"error")));
    }
}
function SYSTEM_SAVE_USER_EMAIL_SETTINGS($params='')
{
    $db = new DB();
    $user_id = $params['data']['user_id'];
    $emails = $params['data']['emails'];
    $notification_id = $params['data']['notification_type'];

    $db->setUserEmail($user_id,$emails);
    $db->setUserNotificationSetting($user_id,$notification_id);

    die(json_encode(array("result"=>"success")));
}
function SYSTEM_GET_USER_EMAIL_SETTINGS($params='')
{
    $db = new DB();
    $user_id = $params['data'];

    $email = $db->getUserEmail($user_id);

    if(!$email)
        $email = "";

    $extra_notification_emails = $db->getUserExtraNotificationEmails($user_id);

    if(!$extra_notification_emails)
        $extra_notification_emails = "";

    die(json_encode(array("email"=>$email, "extra_notification_emails"=>$extra_notification_emails, "notification"=>$db->getUserNotificationSetting($user_id))));
}
function SYSTEM_USER_SAVE_PHONES($params='')
{
    $db = new DB();
    $user_id = $params['data']['id'];
    $disabled_phones = $params['data']['disabled'];
    $outgoing_number_filter = $params['data']['outgoing_number_filter'];

    $db->saveUserAccessNumbers($user_id,$disabled_phones);
    //TODO: Better Outgoing Number Filtering
    if(substr($outgoing_number_filter,0,1)!="+" && strlen($outgoing_number_filter)>0)
        $outgoing_number_filter = array("+".$outgoing_number_filter);
    else if(substr($outgoing_number_filter,0,1)=="+")
        $outgoing_number_filter = array($outgoing_number_filter);
    else
        $outgoing_number_filter = array();
    $db->saveUserOutgoingAccessNumbers($user_id,$outgoing_number_filter);

    die(json_encode(array("result"=>"success")));
}
function SYSTEM_USER_GET_PHONES($params='')
{
    $db = new DB();
    $user_id = $params['data'];

    $companies = $db->getCompaniesForUser($user_id);

    $companies_ = array();
    foreach($companies as $company)
    {
        $company = $company['company_id'];
        $numbers = $db->getCompanyNumIntl($company);

        $user_blocked_numbers = $db->getUserAccessNumbers($user_id);

        $numbers_ = array();
        foreach($numbers as $number)
        {
            if(!$number[1])
                $number[0] = "+1".$number[0];

            $number['noaccess']=false;

            foreach($user_blocked_numbers as $blocked_number)
            {
                if($blocked_number->number===$number[0])
                    $number['noaccess']=true;
            }

            $numbers_[] = $number;
        }

        $companies_[] = array('name'=>$db->getCompanyName($company), 'phones'=>$numbers_);
    }
    //TODO: Better Outgoing Number Access
    $outgoing_number_filter = @array_shift($db->getUserOutgoingAccessNumbers($user_id));

    if(!$outgoing_number_filter)
        $outgoing_number_filter = new ArrayObject(array("number"=>""));

    die(json_encode(array('companies'=>$companies_,'outgoing_number'=>$outgoing_number_filter->number)));
}
function SYSTEM_REAUTH_ACT($params=''){
    actchkusr($params);
}
function SYSTEM_UPDATE_USER_PRIV($params='')
{
    global $TIMEZONE;
    $db = new DB();
    $user_id              = $params['data']['id'];
    $checked              = $params['data']['checked'];
    $not_checked          = $params['data']['not_checked'];
    $call_restrict_enable = $params['data']['call_restriction_enabled'];
    $call_restrict_date   = $params['data']['call_restriction_date'];
    $outgoing_links       = $params['data']['disable_outgoing'];
    $enable_phone_code    = $params['data']['enable_phone_code'];

    if( is_array($checked) || is_object($checked) )
        foreach($checked as $addon)
        {
            if(!$db->checkAddonAccess($user_id,$addon))
            {
                $db->addAddonAccess($user_id,$addon);
            }
        }

    if( is_array($not_checked) || is_object($not_checked) )
        foreach($not_checked as $addon)
        {
            if($db->checkAddonAccess($user_id,$addon))
            {
                $db->removeAddonAccess($user_id,$addon);
            }
        }
    if($call_restrict_enable!=0)
    {
        if($call_restrict_date!="")
        {
            $date = new DateTime($call_restrict_date, new DateTimeZone($TIMEZONE));
            $date->setTimezone(new DateTimeZone("UTC"));

            $db->setUserAccessRange($user_id,$date->format("Y-m-d H:i:s"),$call_restrict_enable);
        }else{
            $db->setUserAccessRange($user_id,null,0);
        }
    }else{
        $db->setUserAccessRange($user_id,null,0);
    }

    $db->setUserOutboundLinkDisable($user_id,$outgoing_links);
    $db->setUserPhoneCodeAccess($user_id,$enable_phone_code);

    die(json_encode(array("result"=>"success")));
}
function SYSTEM_USER_PRIV($params='')
{
    global $TIMEZONE;
    $db = new DB();
    $user_id = $params['data'];

    $addons = Addons::getAddonList();

    $retn_data = array();
    foreach($addons as $addon_name => $addon_data)
    {
        if($db->checkAddonAccess($user_id,$addon_data['id']))
        {
            $addon_data['has_access'] = true;
        }else{
            $addon_data['has_access'] = false;
        }
        $retn_data[$addon_name] = $addon_data;
    }

    $date_return = false;
    $access_type = 0;

    if($db->getUserAccessRange($user_id)!=false)
    {
        $access = $db->getUserAccessRange($user_id);
        $date = new DateTime($access[0], new DateTimeZone("UTC"));
        $date->setTimezone(new DateTimeZone($TIMEZONE));
        $date_return = $date->format("m/d/Y h:i a");
        $access_type = $access[1];
    }

    $outbound_links = $db->isUserOutboundLinksDisabled($user_id);
    $allow_phone_codes = $db->isUserAbleToSetPhoneCodes($user_id);

    die(json_encode(array(
        "products"=>$retn_data,
        "access_type"=>$access_type,
        "access_from"=>$date_return,
        "outbound_links"=>$outbound_links,
        "allow_phone_code"=>$allow_phone_codes
    )));
}
function SYSTEM_INTL_DIALTONE($params='')
{
    $db = new DB();
    if($params['data']['status']=="disable")
    {
        if($db->setVar("intl_dialtone","no"))
            die(json_encode(array("result"=>"success")));
        else
            die(json_encode(array("result"=>"error")));
    }else{
        if($db->setVar("intl_dialtone","yes"))
            die(json_encode(array("result"=>"success")));
        else
            die(json_encode(array("result"=>"error")));
    }
}

function SYSTEM_RING_TONE($params='')
{
    $db = new DB();
    $db->setVar("option_ringtone", $params['data']['ring_tone']);
}

function SYSTEM_SAVE_DB_VAR($params='')
{
    $db = new DB();
    $dbvar = $params['data']['dbvar'];
    $dbval = $params['data']['dbval'];

    if($db->setVar($dbvar,$dbval)!=false)
        die(json_encode(array("result"=>"success")));
    else
        die(json_encode(array("result"=>"fail")));
}
function SYSTEM_TWILIO_PURCHASE_NUMBERS($params='')
{
    global $db, $AccountSid, $AuthToken, $ApiVersion;

    $call_handler_url = $params['data']['url'];
    $sms_handler_url = $params['data']['sms_url'];
    $country = $params['data']['country'];
    $count = 0;

    $twilio_responses = array();

    foreach($params['data']['numbers'] as $number)
    {
        $count++;
        $data = array(
            "PhoneNumber" => $number,
            "VoiceUrl" => $call_handler_url,
            "SmsUrl" => $sms_handler_url,
        );
        $client = new TwilioRestClient($AccountSid, $AuthToken);
        try {

            $response = $client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers/", "POST", $data);
            if(isset($response->ResponseXml->RestException) && $response->ResponseXml->RestException->Code == "21613"){
                die(json_encode(array("result"=>"error","err_msg"=>"Please purchase your first phone number for this country from <a href='https://www.twilio.com/user/account/phone-numbers/available' target='_blank'>Twilio</a>.<br/>You need to do this to agree to Twilio's terms of service for this country.")));
            }
            if(empty($response)) {
                die(json_encode(array("result"=>"error","err_msg"=>"An unknown error has occurred. 1")));
            }
            if(isset($_SESSION['twilio_numbers'])){
                $_SESSION['twilio_numbers'] = null;
                $_SESSION['twilio_numbers_last_upd'] = null;
            }
            $twilio_responses[] = $response;

            $international = 0;
            if($country != "US" && $country != "CA")
                $international = 1;

            if (isset($_SESSION['sel_co']) && $_SESSION['permission'] == 0) {
                $db->addNumberToCompany($_SESSION['sel_co'], $db->format_phone_db($number), $international);
            }
        } catch (Exception $e) {
            die(json_encode(array("result"=>"error","err_msg"=>"An unknown error has occurred. 2")));
        }
    }
    die(json_encode(array("result"=>"success","count"=>$count,$response)));
}
function SYSTEM_GET_LAT_LNG($params='')
{
    $location = $params['data'];

    $geo = Util::yahoo_geo($location);

    die(json_encode(array("lng"=>$geo['longitude'],"lat"=>$geo['latitude'])));
}
function SYSTEM_TWILIO_GET_AVAIL_NUMBERS($params='')
{
    global $AccountSid, $AuthToken, $ApiVersion;

    $postal_code = $params['data']['postal_code'];
    $near_number = $params['data']['near_number'];
    $contains    = $params['data']['contains'];
    $country     = $params['data']['country'];

    $SearchParams = array();
    $SearchParams['InPostalCode'] = !empty($postal_code)? trim($postal_code) : '';
    $SearchParams['NearNumber'] = !empty($near_number)? trim($near_number) : '';
    $SearchParams['Contains'] = !empty($contains)? trim($contains) : '' ;

    $client = new TwilioRestClient($AccountSid, $AuthToken);

    try {

        $response = $client->request("/$ApiVersion/Accounts/$AccountSid/AvailablePhoneNumbers/" . $country . "/Local", "GET", $SearchParams);

        /* If we did not find any phone numbers let the user know */
        if(empty($response)) {
            die(json_encode(array("result"=>"none")));
        }

        $available_numbers = array();
        if(!$response->IsError)
        {
            foreach($response->ResponseXml->AvailablePhoneNumbers->AvailablePhoneNumber as $number)
            {
                $available_numbers[] = $number;
            }
        }else{
            die(json_encode(array("result"=>"error", "details"=>$response, "err_msg"=>$response->ErrorMessage)));
        }

        if(count($available_numbers)==0)
            die(json_encode(array("result"=>"none")));

        die(json_encode(array("result"=>"success","data"=>$available_numbers)));

    } catch (Exception $e) {
        die(json_encode(array("result"=>"error")));
    }
}
function SYSTEM_SAVE_PHONE_CODE($params='')
{
    $db = new DB();
    $company_id = $params['data']['company_id'];
    $to_delete = $params['data']['deleted'];
    $to_add = $params['data']['added'];
    $type = $params['data']['type'];
    unset($_SESSION['sel_co_pc']);

    if(count($to_delete)>0){
        foreach($to_delete as $row)
        {
            $db->deletePhoneCode($company_id,$row);
        }
    }
    if(count($to_add)>0){
        foreach($to_add as $row)
        {
            if($row['dial_code']=="" || !is_numeric($row['dial_code']))
                $row['dial_code']=0;
            $db->addPhoneCode($company_id,$row['phone_code_name'],$row['dial_code'],$type);
        }
    }
    die(json_encode(array("result"=>"success")));
}
function SYSTEM_GET_PHONE_CODES($params='')
{
    $db = new DB();
    $data = $db->getPhoneCodes($params['data'],$params['type']);

    $markup = "";
    if($data!=false)
    {
        foreach($data as $row)
        {
            $markup .= "<tr><td>$row->name</td><td>$row->order</td><td><a href=\"#\" id=\"SUB_DELETE_PHONE_CODE\" data-params=\"$row->idx\" title=\"Delete Company\"><img src=\"images/delete.gif\"></a></td></tr>";
        }
    }
    die(json_encode(array("result"=>"success","table_data"=>$markup)));
}
function SYSTEM_DISABLE_RECORDINGS($params='')
{
    $db = new DB();
    if($params['data']['status']=="disable")
    {
        if($db->setVar("global_recordings","false"))
            die(json_encode(array("result"=>"success")));
        else
            die(json_encode(array("result"=>"error")));
    }else{
        if($db->setVar("global_recordings","true"))
            die(json_encode(array("result"=>"success")));
        else
            die(json_encode(array("result"=>"error")));
    }
}
function SYSTEM_MASK_RECORDINGS($params='')
{
    $db = new DB();
    if($db->setVar("mask_recordings", $params['data']['status']))
        die(json_encode(array("result"=>"success")));
    else
        die(json_encode(array("result"=>"error")));
}
function SYSTEM_SET_CAMPAIGN($params='')
{
    global $AccountSid, $AuthToken,$ApiVersion;
    $client = new TwilioRestClient($AccountSid, $AuthToken);
    $data = array(
        "FriendlyName" => $params['data']['campaign'],
    );
    $response = $client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers/" . $params['data']['number_sid'], "POST", $data);
    if($response->IsError)
        die(json_encode(array("result"=>"error")));
    else
        die(json_encode(array("result"=>"success")));
}
function SYSTEM_GET_CALLERID($params='')
{
    $db = new DB();
    $callerid = $db->getCallerId($params['data']);
    if(!$callerid)
        $callerid = "";
    die("SUCCESS:::".$callerid);
}
function SYSTEM_EDIT_CALLERID($params='')
{
    $company_id = $params['data']['id'];
    $number = $params['data']['number'];
    $db = new DB();
    if($db->setCallerId($company_id, $number))
        die("SUCCESS:::");
    else
        die("FAILURE:::");

}
function SYSTEM_GET_COMPANY_REPORT($params='')
{
    global $AccountSid, $AuthToken;
    global $TIMEZONE;
    $db = new DB();
    $data_array = array();

    if($params['data']['company_id']!=-1)
        $numbers = $db->getCompanyNumIntl($params['data']['company_id']);
    else
        $numbers = $db->getAllCompanyNum();

    $start_date = ($params['data']['start_date']/1000);
    $end_date = ($params['data']['end_date']/1000)+71999;
    $start_date = new DateTime("@".$start_date,new DateTimeZone($TIMEZONE));
    $end_date = new DateTime("@".$end_date,new DateTimeZone($TIMEZONE));
    $start_date->setTimezone(new DateTimeZone("UTC"));
    $start_date->modify("-1 day");
    $end_date->setTimezone(new DateTimeZone("UTC"));
    $end_date->modify("+1 day");

    $search_start_date = $start_date->format("Y-m-d");
    $search_end_date = $end_date->format("Y-m-d");

    $start_date->modify("+1 day");
    $end_date->modify("-1 day");
    $start_date->setTime(0,0,0);
    $end_date->setTime(23,59,59);
    $start_date->setTimezone(new DateTimeZone("UTC"));
    $end_date->setTimezone(new DateTimeZone("UTC"));
    $sd_ts = $start_date->format("U");
    $ed_ts = $end_date->format("U");

    $hasCalls = true;
    $start = 0;

    $client = new Services_Twilio($AccountSid, $AuthToken);

    $total_company_seconds = 0;
    $total_company_price = (float)0.00000;

    while ($hasCalls) {
        $calls_found = 0;
        foreach ($client->account->calls->getIterator($start, 50, array(
            'StartTime>' => $search_start_date,
            'StartTime<' => $search_end_date
        )) as $call) {
            //$call_date = DateTime::createFromFormat(DateTime::RFC2822, $call->date_created);

            $ts = strtotime($call->date_created);
            if($ts >= $sd_ts && $ts<=$ed_ts){                
                foreach($numbers as $number) {
                    $accum_seconds_inc = 0;
                    $accum_seconds_out = 0;
                    $accum_price_inc = (float)0.00000;
                    $accum_price_out = (float)0.00000;

                    if(!$number[1])
                        $number = "+1".$number[0];
                    else
                        $number = "+".$number[0];

                    if($number == $call->to)
                    {
                        $accum_seconds_inc += ceil($call->duration/60)*60;
                        $accum_price_inc = bcadd($accum_price_inc, abs((float)$call->price), 5);
                        $call_count++;
                    }elseif($number == $call->from){
                        $accum_seconds_out += ceil($call->duration/60)*60;
                        $accum_price_out = bcadd($accum_price_out, abs((float)$call->price), 5);
                    }

                    $total_company_price = bcadd($total_company_price, $accum_price_inc, 5);
                    $total_company_price = bcadd($total_company_price, $accum_price_out, 5);
                    $total_company_seconds += ceil($accum_seconds_inc/60)*60;
                    $total_company_seconds += ceil($accum_seconds_out/60)*60;

                    if (isset($data_array[$number." (incoming)"])) {
                        $data_array[$number." (incoming)"]["price"] = $data_array[$number." (incoming)"]["price"] + $accum_price_inc;
                        $data_array[$number." (incoming)"]["calls"] = $data_array[$number." (incoming)"]["calls"] + 1;
                        $data_array[$number." (incoming)"]["seconds"] = $data_array[$number." (incoming)"]["seconds"] + $accum_seconds_inc;
                    }
                    else {
                        $data_array[$number." (incoming)"] = array("price"=>$accum_price_inc,"calls"=>1 ,"seconds"=>$accum_seconds_inc  );
                    }

                    if (isset($data_array[$number." (outgoing)"])) {
                        $data_array[$number." (outgoing)"]["price"] = $data_array[$number." (outgoing)"]["price"] + $accum_price_inc;
                        $data_array[$number." (outgoing)"]["calls"] = $data_array[$number." (outgoing)"]["calls"] + 1;
                        $data_array[$number." (outgoing)"]["seconds"] = $data_array[$number." (outgoing)"]["seconds"] + $accum_seconds_out;
                    }
                    else {
                        $data_array[$number." (outgoing)"] = array("price"=>$accum_price_inc,"calls"=>1 ,"seconds"=>$accum_seconds_out  );
                    }
                }
            }
        }

        if ($calls_found == 0)
            $hasCalls = false;

        $start = $start + 1;
    }

    foreach ($data_array as $key => $value) {
        $data_array[$key]["price"] = number_format($data_array[$key]["price"], 2);
        $data_array[$key]["seconds"] = ceil($data_array[$key]["seconds"]/60)*60;
    }

    foreach($numbers as $number) {
        if (!isset($data_array[$number." (incoming)"]))
            $data_array[$number." (incoming)"] = array("price"=>0.00,"calls"=> 0,"seconds"=> 0);
            
        if (!isset($data_array[$number." (outgoing)"]))
            $data_array[$number." (outgoing)"] = array("price"=>0.00,"calls"=> 0,"seconds"=> 0);
    }

    header("Content-Type: application/json");
    die(json_encode(array($data_array,(float)$total_company_price,$total_company_seconds,$numbers)));
}
function SYSTEM_EDIT_BLACKLIST_FOR_COMPANY($params='')
{
    $db = new DB();
    $company_id = $params['data']['company_id'];
    $blacklist_id = $params['data']['blacklist_id'];
    $result = $db->editCompanyBlacklist($company_id,$blacklist_id);
    if($result)
        die("SUCCESS:::");
    else
        die("FAILURE:::");
}
function SYSTEM_GET_BLACKLISTS($params='')
{
    $db = new DB();
    $result = $db->getblacklists();
    if(count($result)==0)
        die("FAILURE:::");
    $markup = "";

    $stmt = $db->customExecute("SELECT blacklist_id FROM companies WHERE idx=:company_id");
    $stmt->execute(array(":company_id"=>$params['data']));
    $company = $stmt->fetch(PDO::FETCH_OBJ);

    foreach($result as $blacklst)
    {
        $selected = (($company->blacklist_id == $blacklst[0]) ? 'selected="selected"' : '');
        $markup .= "<option value=\"$blacklst[0]\" ".$selected.">$blacklst[1]</option>";
    }
    die("SUCCESS:::".$markup);
}
function SYSTEM_EDIT_BLACKLIST_COMPANY($params='')
{

}
function SYSTEM_EDIT_BLACKLIST($params='')
{
    $db = new DB();
    $result = $db->editBlacklist($params['data']['id'],str_replace(" ","",$params['data']['numbers']));
    if($result)
        die("SUCCESS:::");
    else
        die("FAILURE:::");
}
function SYSTEM_GET_BLACKLIST($params='')
{
    $db = new DB();
    $result = $db->getBlacklist($params['data']);
    die("SUCCESS:::".$result);
}
function SYSTEM_ADD_BLACKLIST($params='')
{
    $db = new DB();
    $result = $db->addBlacklist($params['data']['name']);
    if($result)
        die("SUCCESS:::");
    else
        die("FAILURE:::");
}
function SYSTEM_DELETE_BLACKLIST($params='')
{
    $db = new DB();
    $result = $db->deleteBlacklist($params['data']);
    if($result)
        die("SUCCESS:::");
    else
        die("FAILURE:::");
}
function actchkusr($params=''){
    $db = new DB();

    $arcver = unserialize(base64_decode($db->getVar("arcver")));
    $arcver->lc = 2001235;
    $db->setVar("arcver",base64_encode(serialize($arcver)));

    $dbver = $db->getDatabaseVersion();

    if($dbver){
        $add = "";
        if($dbver[1]>1)
            $add = "You have Enterprise!";
        die(json_encode(array("msg"=>"Successfully reauthorized! ".$add)));
    }else{
        die(json_encode(array("error"=>true)));
    }
}
function SYSTEM_EDIT_COMPANY_SETTINGS($params='')
{
    $db = new DB();
    $settings = array(
        "recording_notification"          =>$db->getDb()->escape($params['data']['recording_notification']),
        "recording_notification_type"     =>$db->getDb()->escape($params['data']['recording_notification_type']),
        "recording_notification_voice"    =>$db->getDb()->escape($params['data']['recording_notification_voice']),
        "recording_notification_language" =>$db->getDb()->escape($params['data']['recording_notification_language']),
        "whisper"                         =>$db->getDb()->escape($params['data']['whisper']),
        "whisper_type"                    =>$db->getDb()->escape($params['data']['whisper_type']),
        "whisper_voice"                   =>$db->getDb()->escape($params['data']['whisper_voice']),
        "whisper_language"                =>$db->getDb()->escape($params['data']['whisper_language']),
        "rec_disable"                     =>$db->getDb()->escape($params['data']['rec_disable'] == 'true' ? 1:0),
        "record_from_ring"                     =>$db->getDb()->escape($params['data']['record_from_ring'] == 'true' ? 1:0),
        "id"                              =>$db->getDb()->escape($params['data']['id'])
    );

    $db->setVar($params['data']['id']."[ga_id]",                @$db->getDb()->escape($params['data']['ga_id']));
    $db->setVar($params['data']['id']."[ga_domain]",            @$db->getDb()->escape($params['data']['ga_domain']));
    $db->setVar($params['data']['id']."[kiss_metrics_api_key]", @$db->getDb()->escape($params['data']['km_api_key']));

    if(@$params['data']['ga_id']=="")
        $db->deleteVar($params['data']['id']."[ga_id]");
    if(@$params['data']['ga_domain']=="")
        $db->deleteVar($params['data']['id']."[ga_domain]");
    if(@$params['data']['km_api_key']=="")
        $db->deleteVar($params['data']['id']."[kiss_metrics_api_key]");


    if($db->updateCompanySettings($settings))
        die("SUCCESS:::");
    else
        die("FAILURE:::");
}
function SYSTEM_GET_SETTINGS($params='')
{
    $db = new DB();
    $settings   = $db->getcompanySettings($params['data']);
    $ga_id      = $db->getVar($params['data']."[ga_id]");
    $ga_domain  = $db->getVar($params['data']."[ga_domain]");
    $km_api_key = $db->getVar($params['data']."[kiss_metrics_api_key]");

    if(!$settings)
        die(json_encode(array("result"=>"fail")));
    else
        die(json_encode(array("result"=>"success",
            "whisp"=>$settings->whisper,
            "rec"=>$settings->recording_warning_url,
            "rec_dis"=>$settings->recording_disable,
            "ga_id"=>$ga_id,
            "ga_domain"=>$ga_domain,
            "km_api_key"=>$km_api_key
        )));
}
function SYSTEM_ADD_COMPANY($params='')
{
	$db = new DB();
	$result = $db->addCompany($params['data']['name']);
	if($result)
		die("SUCCESS:::");
	else
		die("FAILURE:::");
}
function SYSTEM_RESET_USER($params=''){
    require_once("include/config.php");
    require_once("include/class.emailtemplates.php");

    $templ = new EmailTemplate();
    global $SITE_NAME;

    $db = new DB();
    $newpass = Util::rand_passwd();
    $db->changePassword($params['data'],$newpass);
    $userdata = $db->getUser($params['data']);

    $template = $templ->getEmailTemplate(2);
    $smtp_username = $db->getVar("smtp_username");
    $smtp_password = $db->getVar("smtp_password");

    $date = new DateTime("now");

    if(substr($_SERVER["HTTP_HOST"],0,4)=="www.")
        $host = substr($_SERVER["HTTP_HOST"],4,strlen($_SERVER["HTTP_HOST"]-4));
    else
        $host = $_SERVER["HTTP_HOST"];

    $host = "http://".$host.str_replace("/admin_ajax_handle.php","",$_SERVER['SCRIPT_NAME']);

    $msg = str_replace("[useremail]",$userdata->email,$template->content);
    $msg = str_replace("[fullname]",$userdata->full_name,$msg);
    $msg = str_replace("[username]",$userdata->username,$msg);
    $msg = str_replace("[userpass]",$newpass,$msg);
    $msg = str_replace("[companyinfo]",$db->getVar("company_info"),$msg);
    $msg = str_replace("[currentdate]",$date->format("F j, Y"),$msg);
    $msg = str_replace("[url]",$host,$msg);

    $email_params = array(
        "subject" => $template->subject,
        "msg" => $msg,
        "emails" => array($userdata->email)
    );
    Util::sendEmail($email_params);
}

function SYSTEM_ADD_USER($params='')
{
    require_once("include/config.php");
    require_once("include/class.emailtemplates.php");

    $templ = new EmailTemplate();
    global $SITE_NAME;
    
    global $TIMEZONE;
    
    $template = $templ->getEmailTemplate(2);

	$db = new DB();

    $result = $db->createNewUser(strtolower($params['data']['username']),$params['data']['full_name'],$params['data']['password'],$params['data']['email']);
    if($result === "exist"){
        die("EXISTS:::");
    }
	elseif($result)
	{
        $template = $templ->getEmailTemplate(2);
        $smtp_username = $db->getVar("smtp_username");
        $smtp_password = $db->getVar("smtp_password");

        $date = new DateTime("now");

        if(substr($_SERVER["HTTP_HOST"],0,4)=="www.")
            $host = substr($_SERVER["HTTP_HOST"],4,strlen($_SERVER["HTTP_HOST"]-4));
        else
            $host = $_SERVER["HTTP_HOST"];

        $host = "http://".$host.str_replace("/admin_ajax_handle.php","",$_SERVER['SCRIPT_NAME']);

        $msg = str_replace("[useremail]",$params['data']['email'],$template->content);
        $msg = str_replace("[fullname]",$params['data']['full_name'],$msg);
        $msg = str_replace("[username]",$params['data']['username'],$msg);
        $msg = str_replace("[userpass]",$params['data']['password'],$msg);
        $msg = str_replace("[companyinfo]",$db->getVar("company_info"),$msg);
        $msg = str_replace("[currentdate]",$date->format("F j, Y"),$msg);
        $msg = str_replace("[url]",$host,$msg);

        $email_params = array(
            "subject" => $template->subject,
            "msg" => $msg,
            "emails" => array($params['data']['email'])
        );
        Util::sendEmail($email_params);

        $user_data = $result;
        $user_id = $user_data->idx;

        $checked              = $params['data']['permissions']['checked'];
        $call_restrict_enable = $params['data']['permissions']['call_restriction_enabled'];
        $call_restrict_date   = $params['data']['permissions']['call_restriction_date'];
        $outgoing_links       = $params['data']['permissions']['disable_outgoing'];
        $enable_phone_code    = $params['data']['permissions']['enable_phone_code'];

        if( is_array($checked) || is_object($checked) )
            foreach($checked as $addon)
            {
                if(!$db->checkAddonAccess($user_id,$addon))
                {
                    $db->addAddonAccess($user_id,$addon);
                }
            }

        if($call_restrict_enable!=0)
        {
            if($call_restrict_date!="")
            {
                $date = new DateTime($call_restrict_date, new DateTimeZone($TIMEZONE));
                $date->setTimezone(new DateTimeZone("UTC"));

                $db->setUserAccessRange($user_id,$date->format("Y-m-d H:i:s"),$call_restrict_enable);
            }else{
                $db->setUserAccessRange($user_id,null,0);
            }
        }else{
            $db->setUserAccessRange($user_id,null,0);
        }

        $db->setUserOutboundLinkDisable($user_id,$outgoing_links);
        $db->setUserPhoneCodeAccess($user_id,$enable_phone_code);

        if(isset($params['data']['companies']))
        {
            foreach($params['data']['companies'] as $company)
            {
                $db->addUserToCompany($company, $user_id);
            }
        }


		die("SUCCESS:::".$user_data->idx.":::".$user_data->username.":::".$user_data->full_name.":::".$user_data->email);
	}else{
		die("FAILURE:::");
	}
}
function SYSTEM_EDIT_OUTGOING_NUMBER($params='')
{
	$db = new DB();
    $result = $db->setOutgoingNumber($params['data']['outgoing'],$params['data']['company_id'],$params['data']['intl'],$params['data']['voicemail']);
	if($result)
	{
		die("SUCCESS:::");
	}else{
		die("FAILURE:::");
	}
}
function SYSTEM_EDIT_USER_COMPANY($params='')
{
	$db = new DB();
	$user_id = $params['data']['id'];
	if(!isset($params['data']['companies_out']) &&
		!isset($params['data']['companies_in']))
	{
		die("FAILURE:::NOTHING_CHANGED");
	}else{
		if(isset($params['data']['companies_out']))
		{
			foreach($params['data']['companies_out'] as $company)
			{
				$db->deleteUserFromCompany($company,$user_id);
			}
		}
		if(isset($params['data']['companies_in']))
		{
			foreach($params['data']['companies_in'] as $company)
			{
				$db->addUserToCompany($company,$user_id);
			}
		}
		die("DONE:::");
	}	
}
function SYSTEM_DELETE_USER($params='')
{
	$db = new DB();
	$result = $db->deleteUser($params['data']);
	if($result)
	{
		die("SUCCESS:::".$result);
	}else{
		die("FAILURE:::");
	}
}
function SYSTEM_EDIT_USER($params='')
{
	die("NOT_IMPLEMENTED:::".__FUNCTION__);
}
function SYSTEM_VIEW_USER_DETAIL($params='')
{
	die("NOT_IMPLEMENTED:::".__FUNCTION__);
}
function SYSTEM_EDIT_COMPANY_NUMBERS($params='')
{
	$db = new DB();
	$user_id = $params['data']['id'];
	if(!isset($params['data']['numbers_out']) &&
		!isset($params['data']['numbers_in']))
	{
		die("FAILURE:::Nothing changed.");
	}else{
		if(isset($params['data']['numbers_out']))
		{
			foreach($params['data']['numbers_out'] as $number)
			{
				$db->deleteNumberFromCompany($user_id,$number);
			}
		}
        $has_failure = false;
		if(isset($params['data']['numbers_in']))
		{
			foreach($params['data']['numbers_in'] as $number)
			{
                if ($db->isNumberFree($number[0]))
                    $db->addNumberToCompany($user_id,$number[0],$number[1]);
                else
                    $has_failure = true;
			}

            if ($has_failure) {
                if (count($params['data']['numbers_in']) == 1)
                    die("FAILURE:::The number could not be saved because it already belongs to a different company!");
                else
                    die("FAILURE:::Some of the numbers could not be saved because they already belong to a different company!");
            }
		}

		die("DONE:::");
	}	
}
function SYSTEM_GET_COMPANY_LIST($params='')
{
	$db = new DB();
	$user_id = $params['data'];
	$companyList = $db->getAllCompanies();
	$companyFriendlyList = "";
	foreach($companyList as $company)
	{
		if($db->isUserInCompany($company['idx'],$user_id))
			$companyFriendlyList .= $company['idx'].":".$company['company_name'].":1::";
		else
			$companyFriendlyList .= $company['idx'].":".$company['company_name'].":0::";
	}
	if(strlen($companyFriendlyList)!=0)
	{
		die("SUCCESS:::".$companyFriendlyList);
	}else{
		die("FAILURE:::");
	}
}
function SYSTEM_GET_PHONE_NUMBER_LIST($params='')
{
	$db = new DB();
	$twilio_numbers = Util::get_all_twilio_numbers();
	$numbers = $db->getCompanyNumIntl($params['data']);
	$companyNumberList = "";
    $campaigns = $twilio_numbers;
	$twilio_numbers = array_keys($twilio_numbers);

	foreach($twilio_numbers as $number)
	{
        $stmt = $db->customExecute("SELECT pool_id FROM company_num WHERE number = :number");
        $stmt->execute(array(":number" => $db->format_phone_db($number)));
        $result = $stmt->fetch(PDO::FETCH_OBJ);

        if (!empty($result->pool_id)) {
            continue;
        }

        $campaign = $campaigns[$number];
        if(substr($number, 0, 2) == "+1" || substr($number, 0, 1) == "1"){
            $number = str_replace(array('(', ' ', ')', '-'), '', $number);
            $connected = 0;

            foreach($numbers as $list_number)
            {
                if("1".$list_number[0]==$number)
                    $connected=1;
            }

            if($db->isNumberFree(substr($number,1,(strlen($number)-1))))
            {
                $companyNumberList .= substr($number,1,(strlen($number)-1)).":".$connected.":0:".$campaign."::";
            }
            if($connected==1)
                $companyNumberList .= substr($number,1,(strlen($number)-1)).":1:0:".$campaign."::";
        }else{
            $connected = 0;
            $number = str_replace(array('(', ' ', ')', '-'), '', $number);
            foreach($numbers as $list_number)
            {
                if($list_number[0]==$number)
                    $connected = 1;
            }

            if($db->isNumberFree($number)){
                $companyNumberList .= $number.":".$connected.":1:".$campaign."::";
            }
            if($connected==1)
                $companyNumberList .= $number.":".$connected.":1:".$campaign."::";
        }
	}
	if(strlen($companyNumberList)!=0)
	{
		die("SUCCESS:::".$companyNumberList);
	}else{
		die("FAILURE:::");
	}
}
function SYSTEM_PROMOTE_USER($params='')
{
	$db = new DB();
	$user_id = $params['data'];
	$result = $db->promoteUserToAdmin($user_id);
	if($result>0)
	{
		die("SUCCESS:::".$result);
	}else{
		die("FAILURE:::");
	}
}
function SYSTEM_DEMOTE_USER($params='')
{
	$db = new DB();
	$user_id = $params['data'];
	$result = $db->demoteUser($user_id);
	if($result>0)
	{
		die("SUCCESS:::".$result);
	}else{
		die("FAILURE:::");
	}
}
function SYSTEM_DELETE_COMPANY($params='')
{
	$db = new DB();
	$company_id = $params['data'];
	$result = $db->deleteCompany($company_id);
	if($result)
	{
		die("SUCCESS:::".$result);
	}else{
		die("FAILURE:::");
	}
}
function SYSTEM_TWILIO_GET_AVAIL_NUMBERS_AREACODE($params='')
{
    $db = new DB();
    global $AccountSid, $AuthToken, $ApiVersion;
    $area_code 			= 	(int)$params['data']['postal_code'];
    $pool_size 			= 	(int)$params['data']['pool_number'];
    $company_name 		= 	$params['data']['company'];
    $buy 				=   $params['data']['buy'];
    $voiceURL			=	'http://'.$_SERVER['HTTP_HOST'].'/handle_incoming_call.php';
//$source 			= 	$params['data']['source'];
    $company_id 		= 	$db->getCompanyId($company_name);
    $client 			= 	new TwilioRestClient($AccountSid, $AuthToken);
    try {
        $response = $client->request("/$ApiVersion/Accounts/$AccountSid/AvailablePhoneNumbers/US/Local?AreaCode=".$area_code, "GET");
        /* If we did not find any phone numbers let the user know */
        if(empty($response)) {
            die(json_encode(array("result"=>"none")));
        }
        $available_numbers = array();
        if(!$response->IsError)
        {
            foreach($response->ResponseXml->AvailablePhoneNumbers->AvailablePhoneNumber as $number)
            {
                $available_numbers[] = $number;
            }
        }else{
            die(json_encode(array("result"=>"error", "details"=>$response, "err_msg"=>$response->ErrorMessage)));
        }
        if(count($available_numbers) < $pool_size  && $buy == 0)
            die(json_encode(array("result"=>"less number","avaialable_numbers"=>count($available_numbers))));
        if(count($available_numbers)==0)
            die(json_encode(array("result"=>"none")));
//we are getting numbers and working on pooling data
        $numbers = $response->ResponseXml->AvailablePhoneNumbers->AvailablePhoneNumber;
        if( $numbers ){
            foreach( $numbers as $phonenum ){
                $numberData[] = (array)$phonenum->PhoneNumber;
            }
        }
        $pool_id = $db->createPool($company_id, $pool_size);
        if( $numberData ){
            for( $i=0; $i < $pool_size; $i++ ){
                $number = $numberData[$i][0];
                $data = array();
                $data['pool_id'] = $pool_id;
                $data['number'] = $number;
                $data['international'] = 0;
                $data['company_id'] = $company_id;
                if ( isset($number)){
//purchase new Twilio number
                    $numberdata = array(
                        "FriendlyName" => 'callTracking',
                        "VoiceUrl" => $voiceURL,
                        "PhoneNumber" => $number
                    );
                    $response = $client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers", "POST", $numberdata);
                    if($response->IsError){
                        $message = "<p class='error'>Error, something went wrong, please try again.</p>";
                        echo "Error purchasing phone number: {$response->ErrorMessage}\n";
                    }else{
//number buy is doen now insert it into the company_num table
                        $db->poolNumberInsert($data);
                    }
                }
            }// end of for loop
        }
        die(json_encode(array("result"=>"success")));
    } catch (Exception $e) {
        die(json_encode(array("result"=>"error")));
    }
}
function SYSTEM_TWILIO_GET_AVAIL_NUMBERS_AREA($params=''){
    $db = new DB();
    global $AccountSid, $AuthToken, $ApiVersion;
    $area_code 			= 	$params['data']['postal_code'];
    $pool_size 			= 	(int)$params['data']['pool_number'];
    $company_id         =   $params['data']['company'];
    $voiceURL			=	'http://'.$_SERVER['HTTP_HOST'].'/handle_incoming_call.php';
    $client 			= 	new TwilioRestClient($AccountSid, $AuthToken);
    try {
        $response = $client->request("/$ApiVersion/Accounts/$AccountSid/AvailablePhoneNumbers/US/Local?AreaCode=".$area_code, "GET");
        /* If we did not find any phone numbers let the user know */
        if(empty($response)) {
            die(json_encode(array("result"=>"none")));
        }
        $available_numbers = array();
        if(!$response->IsError)
        {
            foreach($response->ResponseXml->AvailablePhoneNumbers->AvailablePhoneNumber as $number){
                $available_numbers[] = (array)$number->PhoneNumber;
            }
            die(json_encode(array("result"=>"numbers", "available_numbers"=>$available_numbers)));
        }else{
            die(json_encode(array("result"=>"error", "details"=>$response, "err_msg"=>$response->ErrorMessage)));
        }
    } catch (Exception $e) {
        die(json_encode(array("result"=>"error")));
    }
}
function SYSTEM_TWILIO_DELETE_POOL($params=''){
    $db = new DB();
    global $AccountSid, $AuthToken, $ApiVersion;
    $pool_id			= 	$params['data']['pool_id'];
    $pool_details		= 	$db->getPoolDetail($pool_id);
    $client 			= 	new Services_Twilio($AccountSid, $AuthToken);
    $i					=	1;
    $n					=	0;
    foreach ($client->account->incoming_phone_numbers as $n) {
        if(substr($n->phone_number,0,2)=="+1")
            $twilio_Sid[$i]['number'] = substr($n->phone_number, 2);
        else
            $twilio_Sid[$i]['number'] = substr($n->phone_number, 1);
        $twilio_Sid[$i]['Sid'] = $n->sid;
        $i++;
    }

    foreach($pool_details['numbers'] as $num){
        if(substr($num,0,2)=="+1")
            $num2 = substr($num, 2);
        else
            $num2 = substr($num, 1);

        foreach($twilio_Sid as $sid){
            if($num == $sid['number'] || $num2 == $sid['number']){
                $Sid = $sid['Sid'];
                $number = $num;
            }
        }

        $client 			= 	new TwilioRestClient($AccountSid, $AuthToken);
        $response = @$client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers/".$Sid , "DELETE");

        /*ifif($response->IsError){
            die(json_encode(array("result"=>"error", "msg"=>'Error in deleting the pool numbers from Twilio',"details"=>$sid)));
        }*/
        $db->deleteNumber($number);
        $db->deleteNumber($num);
        $n++;
    }

    if($db->deletePool($pool_id)){
        die(json_encode(array("result"=>"success", "msg"=>'Pool deleted')));
    }else{
        die(json_encode(array("result"=>"error", "msg"=>'Error in deleting the pool from database')));
    }
}
function SYSTEM_TWILIO_DELETE_POOL_NUMBER($params=''){
    $db = new DB();
    global $AccountSid, $AuthToken, $ApiVersion;
    $pool_id			= 	$params['data']['pool_id'];
    $numbers			=	$params['data']['checked_numbers'];
    $pool_details		= 	$db->getPoolDetail($pool_id);
    $client 			= 	new Services_Twilio($AccountSid, $AuthToken);
    $i					=	1;
    $n					=	0;
    foreach ($client->account->incoming_phone_numbers as $n) {
        if(substr($n->phone_number,0,2)=="+1")
            $twilio_Sid[$i]['number'] = substr($n->phone_number, 2);
        else
            $twilio_Sid[$i]['number'] = substr($n->phone_number, 1);
        $twilio_Sid[$i]['Sid'] = $n->sid;
        $i++;
    }
    foreach($numbers as $num){
        if(substr($num,0,2)=="+1")
            $num2 = substr($num, 2);
        else
            $num2 = substr($num, 1);

        foreach($twilio_Sid as $sid){
            if($num == $sid['number'] || $num2 == $sid['number']){
                $Sid = $sid['Sid'];
                $number = $num;
            }
        }
        $client 			= 	new TwilioRestClient($AccountSid, $AuthToken);
        $response = @$client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers/".$Sid , "DELETE");

        /*if($response->IsError){
            die(json_encode(array("result"=>"error", "msg"=>'Error in deleting the pool numbers from Twilio')));
        }*/
        $db->deleteNumber($number);
        $db->deleteNumber($num);
        $n++;
    }
    $numbers = $db->getAllPoolNumbersByPoolId($pool_id);
    $pool_details = $db->getPoolDetail($pool_id);
    $quantity = count($numbers);
    if($quantity == 0){
        // now delete the pool
        if($db->deletePool($pool_id)){
            die(json_encode(array("result"=>"success", "msg"=>'Pool as well as all pool numbers are deleted')));
        }else{
            die(json_encode(array("result"=>"error", "msg"=>'Error in deleting the pool from database')));
        }
    }else{
        // update the quantity of the pool
        $pool_size = $pool_details['quantity'] - $quantity;
        if($db->updatePoolQuantity($pool_size, $pool_id)){
            die(json_encode(array("result"=>"success", "msg"=>'Number(s) Deleted.')));
        }else{
            die(json_encode(array("result"=>"error", "msg"=>'Error deleting number(s) form database.')));
        }
    }
    $numbers = $db->getAllPoolNumbersByPoolId($pool_id);
    $pool_details = $db->getPoolDetail($pool_id);
    $quantity = count($numbers);
    if($quantity == 0){
        // now delete the pool
        if($db->deletePool($pool_id)){
            die(json_encode(array("result"=>"success", "msg"=>'Pool as well as all pool numbers are deleted')));
        }else{
            die(json_encode(array("result"=>"error", "msg"=>'Error in deleting the pool from database')));
        }
    }else{
        // update the quantity of the pool
        $pool_size = $pool_details['quantity'] - $quantity;
        if($db->updatePoolQuantity($pool_size, $pool_id)){
            die(json_encode(array("result"=>"success", "msg"=>'Number(s) Deleted.')));
        }else{
            die(json_encode(array("result"=>"error", "msg"=>'Error deleting number(s) form database.')));
        }
    }
}
function SYSTEM_TWILIO_GET_ADD_NUMBER_POOL($params=''){
    $db = new DB();
    global $AccountSid, $AuthToken, $ApiVersion;
    $area_code			= 	$params['data']['area_code'];
    $pool_size 			=	$params['data']['poolsize'];
    $company_id         =   $params['data']['company'];
    $country            =   $params['data']['country'];
    $company_name       =   $db->getCompanyName($company_id);
    $url 				=	'pool_number.php';
    $pool_field			=	"";
//this function runs if we are editing a pool
    if(isset($params['data']['edit'])){
        $pool_id 			=	$params['data']['pool_id'];
        $url				=	'pool_edit.php?pool_id='.$pool_id;
        $pool_field			=	'<input type="hidden" value="'.$pool_id .'" id="edit_pool_id"/>';
    }
    $client 			= 	new TwilioRestClient($AccountSid, $AuthToken);
    try {
        if (isset($params['data']['toll_free']))
            $response = $client->request("/$ApiVersion/Accounts/$AccountSid/AvailablePhoneNumbers/".$country."/TollFree?AreaCode=".$area_code, "GET");
        else
            $response = $client->request("/$ApiVersion/Accounts/$AccountSid/AvailablePhoneNumbers/".$country."/Local?AreaCode=".$area_code, "GET");

        if(empty($response)) {
            die(json_encode(array("result"=>"none")));
        }
        $available_numbers = array();
        if(!$response->IsError){
            $i = 1;
            $currentPoolSize = count($response->ResponseXml->AvailablePhoneNumbers->AvailablePhoneNumber);
            foreach($response->ResponseXml->AvailablePhoneNumbers->AvailablePhoneNumber as $number){
                if($pool_size >= $i ){ $checked = 'checked = checked'; $selected_numbers = $i; }else{ $checked = '';}
                $available_numbers[] = '<div style="float: left; padding-right: 27px; padding-bottom: 5px;"><span style="font-size:14;" class="dusty"><input type="checkbox" name="checkbox[]" '.$checked.' class="num_select" value="'.(string)$number->PhoneNumber.'" style="display: inline-block !important;" /> '.(string)$number->PhoneNumber.'</span></div>';
                $i++;
            }
            $available_numbers = implode('', $available_numbers);
            if (empty($selected_numbers)) $selected_numbers = 0;
            $available_numbers = '<div><span><label>Company Name</label></span> : <span style="margin-left:10px;">'.stripslashes($company_name).'</span></div><div><span><label>Total Numbers Selected</label></span> : <span style="margin-left:10px;" id="select_count">'.$selected_numbers.'</span></div><span class="act_margin" style="padding-top: 10px;"><br /><input type="checkbox" name="checkAll" id="checkAll" style="display: inline-block !important;" /><span class="act_margin" /> Select all</span></span><form action="#" method="POST" id="buy_numbers" style="padding-top: 10px;">'.$available_numbers.$pool_field.'<div class="act_submit" style="clear: both; padding-top: 10px;"><input type="hidden" id="company_name" name="company_name" value="'.$company_id.'" /><input type="submit" class="submit mid" value="Add Numbers" style="float: left;" /><a href="'.$url.'" style="float: right;"><input type="button" id="cancel" class="submit mid" value="Cancel"/></a></div><br/></form>';
            die(json_encode(array("result"=>"success", "details"=>$available_numbers, "count" =>$currentPoolSize)));
        }else{
            die(json_encode(array("result"=>"error", "details"=>$response, "err_msg"=>$response->ErrorMessage)));
        }
    }catch (Exception $e) {
        die(json_encode(array("result"=>"error")));
    }
}
function SYSTEM_TWILIO_ADDING_SELECTED_NUMBER_POOL($params=''){
    $db = new DB();
    global $AccountSid, $AuthToken, $ApiVersion;
    $pool_id				= 	$params['data']['pool_id'];
    $add_numbers			=  	$params['data']['add_numbers'];
    $company_id			    =	$params['data']['company_id'];
    //$voiceURL				=	'http://'.$_SERVER['HTTP_HOST'].'/handle_incoming_call.php';
    $voiceURL               =   $params['data']['call_handler_url'];
    $country                =   $params['data']['country'];
    $numberCount			= 	count($add_numbers);
    $data 					=	array();
    $data['pool_id'] 		= 	$pool_id;
    $data['international'] 	= 	0;
    $data['company_id'] 	= 	$company_id;
    $client 			= 	new TwilioRestClient($AccountSid, $AuthToken);
    foreach($add_numbers as $number){
        $numberdata = array(
            "FriendlyName" => 'callTracking',
            "VoiceUrl" => $voiceURL,
            "PhoneNumber" => $number
        );
        try {
            $response = $client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers", "POST", $numberdata);
            if(!$response->IsError){
                // add number to company_num table
                $data['number'] 	= $number;
                if($country != "US" && $country != "CA")
                    $data['international'] = 1;
                $db->poolNumberInsert($data);
            }
        }catch (Exception $e) {
            die(json_encode(array("result"=>"error")));
        }
    }
//adding numbers quantity to pool table also
    $pool_details 	= 	$db->getPoolDetail($pool_id);
    $pool_size = $pool_details['quantity'] + $numberCount;
    if($db->updatePoolQuantity($pool_size, $pool_id)){
        die(json_encode(array("result"=>"success", "msg"=>'Number(s) added to the pool.')));
    }else{
        die(json_encode(array("result"=>"error", "msg"=>'Error adding number(s) to database.')));
    }
}
function SYSTEM_TWILIO_ADDING_SELECTED_NUMBER_NEW_POOL($params=''){
    $db = new DB();
    global $AccountSid, $AuthToken, $ApiVersion;
    $add_numbers			=  	$params['data']['add_numbers'];
    $company_id			=	$params['data']['company_name'];
    //$voiceURL				=	'http://'.$_SERVER['HTTP_HOST'].'/handle_incoming_call.php';
    $voiceURL               =   $params['data']['call_handler_url'];
    $company_name 			= 	$db->getCompanyName($company_id);
    $country                =   $params['data']['country'];
    $pool_size				= 	count($add_numbers);
    $pool_id = $db->createPool($company_id, $pool_size);
    $data 					=	array();
    $data['pool_id'] 		= 	$pool_id;
    $data['international'] 	= 	0;
    $data['company_id'] 	= 	$company_id;
    $client 			= 	new TwilioRestClient($AccountSid, $AuthToken);
    foreach($add_numbers as $number){
        $numberdata = array(
            "FriendlyName" => 'callTracking',
            "VoiceUrl" => $voiceURL,
            "PhoneNumber" => $number
        );
        try {
            $response = $client->request("/$ApiVersion/Accounts/$AccountSid/IncomingPhoneNumbers", "POST", $numberdata);
            if(!$response->IsError){
                // add number to company_num table
                $data['number'] 	= $number;
                if($country != "US" && $country != "CA")
                    $data['international'] = 1;
                $db->poolNumberInsert($data);
            }
        }catch (Exception $e) {
            die(json_encode(array("result"=>"error")));
        }
    }
    die(json_encode(array("result"=>"success", 'msg'=>'Pool is created.')));
}

function SYSTEM_SET_RESET_CALL_TIME($params='')
{
    $company_id = $params['data']['company_id'];
    $mint = (int)$params['data']['update_value'];
    if($mint == 0){
        die("FAILURE: As we need numeric value only");
    }else if($mint < 0){
        die("FAILURE: Value must be above 0");
    }

    $value = $mint * 3600;
    $db = new DB();
    $result = $db->resetCallTime($company_id, $value);
    if($result)
    {
        die($mint);
    }else{
        die('error');
    }
}

function SYSTEM_SAVE_SMS_FORWARD_NUMBER($params='') {
    $db = new DB();
    $db->save_sms_forward($params['data']['from'], $params['data']['to']);
    exit();
}

function SYSTEM_SAVE_TRACKING_SETTINGS($params='') {
    $db = new DB();
    $tracking_settings = $db->getTrackingSettingsForCompany($params['data']['company_id']);
    $company_emails = json_decode(Util::escapeString($tracking_settings->emails));

    if ($company_emails) {
        foreach ($company_emails as $email) {
            $jsonurl = "http://api.trackingyourleads.com/disableEmail.php?email=".$email;
            $json = $db->curlGetData($jsonurl);
        }
    }

    $db->saveTrackingSettings($params['data']['company_id'], Util::escapeString($params['data']['clients']), $params['data']['distribution'], Util::escapeString($params['data']['emails']),  $params['data']['billing_tag'],  Util::escapeString($params['data']['client_email_tags']));

    $emails = json_decode(Util::escapeString($params['data']['emails']));

    foreach ($emails as $email) {
        $jsonurl = "http://api.trackingyourleads.com/enableEmail.php?email=".$email;
        $json = $db->curlGetData($jsonurl);
    }
    echo "1";
    exit();
}

function SYSTEM_SAVE_TRACKING_EMAIL_SAVE_TAGS($params='') {
    $db = new DB();
    $stmt = $db->customExecute("UPDATE email_tracking_log SET billing_tags = ? WHERE id = ?");
    $stmt->execute(array($params['data']['tags'], $params['data']['id']));
    exit();
}

function SYSTEM_SAVE_GLOBAL_OPT_OUT_SETTINGS($params='') {
    $db = new DB();

    foreach ($params['data'] as $key => $value) {
        $db->setVar($key, $value);
    }

    exit();
}

function GET_RECORDING($params='')
{
    $recordingId = $params['recordingId'];

    $db = new DB();
    
    $recording = $db->getRecording($recordingId);


    if ($db->getVar("mask_recordings") == "true") {
        $carecording = Util::maskRecordingURL($carecording);
    }

    if ($recording) {
        ob_start();
        Util::generateFlashAudioPlayer($recording->value,"sm");
        $playable = ob_get_clean();
        ob_end_clean();

        die(json_encode(array(
            "url" => $recording->value,
            "playable" => $playable
        )));
    }

    exit();
}

function SYSTEM_DELETE_VOICEMAIL($params='')
{
    $message_id = $params['data']['message_id'];

    $db = new DB();
    
    $query = $db->customExecute("DELETE FROM messages WHERE message_id = ?");
    $query->execute(array($message_id));

    exit();
}

function SYSTEM_READ_VOICEMAIL($params='')
{
    $message_id = $params['data']['message_id'];

    $db = new DB();
    
    $db->updateMessageFlag($message_id,1);

    exit();
}
//
// Utility
//
function genericFail($msg = null){
    $res = array("result"=>"fail");

    if($msg!=null)
        $res = array("result"=>"fail","reason"=>$msg);

    die(json_encode($res));
}
function genericSuccess($data = null){
    if($data==null)
        die(json_encode(array("result"=>"success")));
    else{
        $arr = array("result"=>"success");
        $arr = array_merge($arr,$data);
        die(json_encode($arr));
    }
}
function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}
function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    $start  = $length * -1; //negative
    return (substr($haystack, $start) === $needle);
}

?>