<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_contactlist";

//Including necessary files
require_once 'include/util.php';
require_once 'include/Pagination.php';
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10008)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

//Redirecting to login page if not logged in
    header("Location: login.php");

//Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

//If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

//Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

//Retrieving contact index
$contact_idx = $_GET['contact_idx'];

//Retrieving contact details
$contact_details = ad_advb_cl_get_contact_details($contact_idx);

//Initializing the response variable here
$action_response = '';
$action_response_type = '';

//If edit contact form is submitted
if (isset($_POST['ad_ad_process_settings_form'])):

    //Retrieving the form data
    $first_name = $_POST['ad_ad_dd_firstname'];
    $last_name = $_POST['ad_ad_dd_lastname'];
    $phone = $_POST['ad_ad_dd_pnone'];
    $email = $_POST['ad_ad_dd_email'];
    $address = $_POST['ad_ad_dd_address'];
    $city = $_POST['ad_ad_dd_city'];
    $state = $_POST['ad_ad_dd_state'];
    $zip = $_POST['ad_ad_dd_zip'];
    $business = $_POST['ad_ad_dd_business'];
    $website = $_POST['ad_ad_dd_website'];

    //Updating the contact details and retrieving update status
    $update_status = ad_advb_cl_update_contact_details($contact_idx, array(
        1 => $business,
        2 => $first_name,
        3 => $last_name,
        4 => $email,
        5 => $address,
        6 => $city,
        7 => $state,
        8 => $zip,
        9 => $website
    ),false);

    //If data successfully updated
    if ($update_status):

        //Setting the response message and type
        $action_response = 'Successfully updated contacts data.';
        $action_response_type = 'success';

        //Retrieving update contact details
        $contact_details = ad_advb_cl_get_contact_details($contact_idx);

    else:

        //Assigning the failure message to response variables
        $action_response = 'Nothing updated';
        $action_response_type = 'failed';

    endif;

endif;

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

        <!--//Setting the style of map-->
        <style>
            #map-canvas {
                margin: 0;
                padding: 0;
                height: 100%;
            }
        </style>

        <!--//Loading google maps JS library-->
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script>
<?php
        $ny_cordinates = array("longitude"=>"-74.006605","latitude"=>"40.714623");
?>
            //Defining variable that will contain google map object
            var map;

            //When document finishes loading further data
            $(window).load(function() {

                //Setting map options
                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng('<?php echo $ny_cordinates['latitude']; ?>', '<?php echo $ny_cordinates['longitude']; ?>'),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                //Displaying map on screen and storing the google map object 
                //in our custom varibale defined at first line
                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            });

            /**
             * This function will reset the map to new york as its center location.
             * @returns {undefined}             
             **/
            function cl_reset_map() {
                map.setCenter(new google.maps.LatLng('<?php echo $ny_cordinates['latitude']; ?>', '<?php echo $ny_cordinates['longitude']; ?>'));
            }

        </script>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                <div class="clear"></div>
                <div class="ad_ad_bulk_dial_status_text">

                </div>
                <div class="clear"></div>

                <div class="block" style="background: none;">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <ul style="float: right;">
                            <li><a href="ad_contactlist_log.php">Manage Contact List</a></li>
                            <li><a href="ad_contactlist_add.php">ADD Contact List</a></li>
                        </ul>
                        <h2>Edit Contact Details</h2>
                    </div>
                </div>
                <div class="clear"></div>

                <!--//Starting the Twilio Settings form-->
                <form action="" method="post" id="auto_dialer_dialling_details_form">

                    <!--Initializing the left hand portion of settings page-->
                    <div class="block small left" >
                        <div class="block_head">
                            <div class="bheadl"></div>
                            <div class="bheadr"></div>
                            <h2>Edit Contact:</h2>
                        </div>
                        <!-- .block_head ends -->

                        <div class="block_content">


                            <?php
                            //Checkin if action message is set or not
                            if (!s8_is_str_blank($action_response)):

                                //If action message is set, switching to type of action message
                                switch (strtolower($action_response_type)):

                                    //If action message is of success type
                                    case 'success':
                                        //Displaying success message
                                        ?>
                                        <div class="message success">
                                            <p><?php echo $action_response; ?></p>
                                        </div>
                                        <?php
                                        break;

                                    //If action message is of failure type
                                    case 'failed':
                                        //Displaying failure message
                                        ?>
                                        <div class="message errormsg">
                                            <p><?php echo $action_response; ?></p>
                                        </div>
                                        <?php
                                        break;

                                    //If action message is of failure type
                                    case 'warning':
                                        //Displaying failure message
                                        ?>
                                        <div class="message warning">
                                            <p><?php echo $action_response; ?></p>
                                        </div>
                                        <?php
                                        break;

                                    //If action message is of failure type
                                    case 'info':
                                        //Displaying failure message
                                        ?>
                                        <div class="message info">
                                            <p><?php echo $action_response; ?></p>
                                        </div>
                                        <?php
                                        break;

                                endswitch;

                            endif;
                            ?>

                            <p>
                                <label>First Name: </label><br/>
                                <input tabindex="1" type="text" class="text" name="ad_ad_dd_firstname" value="<?php echo isset($contact_details['first_name']) ? $contact_details['first_name'] : ' - '; ?>" />
                            </p>

                            <p>
                                <label>Last Name: </label>
                                <input tabindex="2" type="text" class="text" name="ad_ad_dd_lastname" value="<?php echo isset($contact_details['last_name']) ? $contact_details['last_name'] : ' - '; ?>" />
                            </p>

                            <p>
                                <label>Phone Number: </label>
                                <input tabindex="3" type="text" class="text" name="ad_ad_dd_pnone" value="<?php echo isset($contact_details['phone_number']) ? $contact_details['phone_number'] : ' - '; ?>" />
                            </p>

                            <p>
                                <label>Email Address: </label><br/>
                                <input tabindex="4" type="text" class="text" name="ad_ad_dd_email" value="<?php echo isset($contact_details['email']) ? $contact_details['email'] : ' - '; ?>" />
                            </p>

                            <p>
                                <label>Address: </label>
                                <input tabindex="5" type="text" class="text" name="ad_ad_dd_address" value="<?php echo isset($contact_details['address']) ? $contact_details['address'] : ' - '; ?>" />
                            </p>

                            <p>
                                <label>City: </label>
                                <input tabindex="6" type="text" class="text" name="ad_ad_dd_city" value="<?php echo isset($contact_details['city']) ? $contact_details['city'] : ' - '; ?>" />
                            </p>

                            <p>
                                <label>State: </label>
                                <input tabindex="7" type="text" class="text" name="ad_ad_dd_state" value="<?php echo isset($contact_details['state']) ? $contact_details['state'] : ' - '; ?>" />
                            </p>

                            <p>
                                <label>Zip Code: </label>
                                <input tabindex="8" type="text" class="text" name="ad_ad_dd_zip" value="<?php echo isset($contact_details['zip']) ? $contact_details['zip'] : ' - '; ?>" />
                            </p>

                            <p>
                                <label>Business: </label>
                                <input tabindex="9" type="text" class="text" name="ad_ad_dd_business" value="<?php echo isset($contact_details['business_name']) ? Util::escapeString($contact_details['business_name']) : ' - '; ?>" />
                            </p>

                            <p>
                                <label>Website: </label>
                                <input tabindex="10" type="text" class="text" name="ad_ad_dd_website" value="<?php echo isset($contact_details['website']) ? $contact_details['website'] : ' - '; ?>" />
                            </p>

                            <p style="width: 200px;margin: 0 auto;">                              
                                <input style="float:left;" type="submit" class="submit small" name="ad_ad_process_settings_form" value="Save" />
                                <input style="float:right;" type="button"  class="submit small" value="Cancel" onclick="window.document.location = 'ad_contactlist_add.php?list_idx=<?php echo $contact_details['list_idx']; ?>'" />
                            </p>
                            <div style="height: 25px;"></div>
                            <!-- .block_content ends -->
                        </div>

                        <div class="bendl"></div>
                        <div class="bendr"></div>
                        <!-- .block.small.right ends -->

                    </div>


                    <!--//Twilio setting form ends here-->
                </form>

                <div class="block small right" style="height:759px;">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Location - 
                            <a href="javascript:void(0);" onclick="cl_reset_map();">
                                Reset
                            </a>
                        </h2>
                    </div>		<!-- .block_head ends -->
                    <div class="block_content" style="height:690px;">
                        <script type="text/javascript">
            if ($ === undefined) {
                $ = jQuery;
            }

            function load_map() {
                var full_address_arr = [$('[name=ad_ad_dd_address]').val(), $('[name=ad_ad_dd_city]').val(), $('[name=ad_ad_dd_state]').val(), $('[name=ad_ad_dd_zip]').val()];
                var full_address = encodeURIComponent(full_address_arr.join(', '));
                $.get('ad_ajax.php?action=getLatLong&address=' + full_address, function(ajax_response) {
                    var latLong = $.parseJSON(ajax_response);
                    map.setCenter(new google.maps.LatLng(latLong.lat, latLong.long));
                });
            }

            $(document).ready(function() {
                $('[name=ad_ad_dd_address],[name=ad_ad_dd_city],[name=ad_ad_dd_state],[name=ad_ad_dd_zip]').change(function() {
                    load_map();
                });
            });

            $(window).load(function() {
                load_map();
            });
                        </script>
                        <div id="map-canvas"></div>
                        <!-- .block_content ends -->
                        <div class="bendl"></div>
                        <div class="bendr"></div>
                    </div>
                </div>


                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Notes</h2>
                    </div>		<!-- .block_head ends -->
                    <div class="block_content">
                        <div id="cl_note_container">
                            <?php
                            //Retrieving the contact notes
                            $contact_notes = ad_advb_cl_get_notes($contact_idx);

                            //If number of notes count is greater than 0
                            if (count($contact_notes) > 0):

                                //Building the response variable
                                $output = '<ul>';
                                foreach ($contact_notes as $single_note_data):
                                    $output .= '<li data-id="' . $single_note_data['idx'] . '">' . $single_note_data['note'] . '<span style="float:right;"><span style="color: #999;">[' . date('m/d/Y H:i e', strtotime($single_note_data['date'])) . ']</span>&nbsp;&nbsp;<a href="#" class="CL_DELETE_NOTE" data-params="' . $single_note_data['idx'] . '" title="Delete Note"><img align="top" src="images/delete.gif"></a></span></li>';
                                endforeach;
                                $output .= '</ul>';

                                //Printing the notes html
                                echo $output;

                            endif;
                            ?>
                        </div>
                        <form id="ad_advb_cl_add_note">
                            <p>
                                <input type="text" class="text left ad_cle_note" style="width:715px !important;" />
                                <input type="submit" class="submit long left marginleft10" value="Add Note" />
                            </p>
                        </form>
                        <script type="text/javascript">
                            if ($ == undefined) {
                                $ = jQuery;
                            }

                            /**
                             * This function will update the current webpage 
                             * to load updated notes of current contact.
                             * @returns {undefined}
                             */
                            function update_contact_notes() {

                                //Firing AJAX request to load updated notes
                                $.get('ad_ajax.php?action=ad_advb_cl_get_notes&contact_idx=<?php echo $contact_idx; ?>', function(ajax_response) {

                                    //Updating the current webpage with new notes
                                    $('#cl_note_container').html(ajax_response);

                                });
                            }

                            //When document is ready for performing JS operations
                            $(document).ready(function() {

                                //When form is submitted
                                $('#ad_advb_cl_add_note').submit(function(e) {

                                    //Retrieving the note from input field
                                    var note = $('.ad_cle_note').val();

                                    //Firing the ajax request to add the note
                                    $.post('ad_ajax.php?action=ad_advb_cl_add_note', {contact_idx: '<?php echo $contact_idx; ?>', note: note}, function(ajax_response) {

                                        //If note is added successfully into the system
                                        if (ajax_response.match('SUCCESS') === null) {

                                            //If error message
                                            //Alerting the user
                                            alert(ajax_response);

                                        } else {

                                            //Clearing up the input field from old data
                                            $('.ad_cle_note').val('');

                                        }

                                        //Updating the notes section on webpage
                                        update_contact_notes();

                                    });

                                    //Preventing the default form submission
                                    e.preventDefault();

                                    //Ending jquery cl note form handler
                                });

                                //Attaching a jquery handler to delete button of notes
                                $('body').delegate('.CL_DELETE_NOTE', 'click', function(e) {

                                    //Retrieving the note ID
                                    var cl_note_idx = $(this).attr('data-params');

                                    //Firing the ajax request to delete the specified note
                                    $.get('ad_ajax.php?action=ad_advb_cl_delete_note&cl_note_idx=' + cl_note_idx, function(ajax_response) {

                                        //If delete operation was not successfull
                                        if (ajax_response.match('SUCCESS') === null) {

                                            //Alerting the user with 
                                            //Error message
                                            alert(ajax_response);

                                        }

                                        //Updating the notes section on webpage
                                        update_contact_notes();

                                    });

                                    e.preventDefault();
                                });

                            });
                        </script>

                        <!-- .block_content ends -->
                    </div>
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>

                <!--                <div style="margin:auto auto;padding-bottom: 75px;width: 200px;background: none;clear: both;" class="block">
                                </div>-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
    </body>
</html>