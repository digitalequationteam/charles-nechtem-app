<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_voice_broadcast";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken, $title, $TIMEZONE;

if(!isset($_GET['campaign_id']) || $_GET['campaign_id']==""){
    ?>
<script type="text/javascript">
    alert('Invalid campaign!');
    window.location = "ad_vb_campaigns.php";
</script>
<?php
}

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10007)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system,
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

$campaign_data = ad_vb_get_campaign_data($_GET['campaign_id']);

if($campaign_data==""){
    ?>
<script type="text/javascript">
    alert('Invalid campaign!');
    window.location = "ad_vb_campaigns.php";
</script>
<?php
}

$response = '';
$response_type = '';

$live_answer_content = '';
$live_answer_type = '';
$live_answer_voice = '';
$live_answer_language = '';
$voicemail_message_content = '';
$voicemail_message_type = '';
$voicemail_message_voice = '';
$voicemail_message_language = '';

if(isset($_GET['campaign_id']) && isset($_GET['id']) && $_GET['id']!=""){
    $type = "EDIT";
    $stmt = $db->customExecute("SELECT * FROM ad_vb_sequences WHERE id = ?");
    $stmt->execute(array($_GET['id']));
    $sequence = $stmt->fetch(PDO::FETCH_ASSOC);
    $separated = explode(" ",$sequence['time']);
    $ex = explode(":",$separated[0]);
    $s_time = array($ex[0],$ex[1],$separated[1]);

    $live_answer_content = $sequence['live_answer_content'];
    $live_answer_type = $sequence['live_answer_type'];
    $live_answer_voice = $sequence['live_answer_voice'];
    $live_answer_language = $sequence['live_answer_language'];
    $voicemail_message_content = $sequence['voicemail_message_content'];
    $voicemail_message_type = $sequence['voicemail_message_type'];
    $voicemail_message_voice = $sequence['voicemail_message_voice'];
    $voicemail_message_language = $sequence['voicemail_message_language'];
}else{
    $type = "CREATE";
}

if (isset($_POST['ad_vb_process_settings_form'])){
    $live_answer_content = $_POST['live_answer_content'];
    $live_answer_type = $_POST['live_answer_type'];
    $live_answer_voice = $_POST['live_answer_voice'];
    $live_answer_language = $_POST['live_answer_language'];
    $voicemail_message_content = $_POST['voicemail_message_content'];
    $voicemail_message_type = $_POST['voicemail_message_type'];
    $voicemail_message_voice = $_POST['voicemail_message_voice'];
    $voicemail_message_language = $_POST['voicemail_message_language'];

    if(!is_numeric($_POST['seq_days_after']) || $_POST['seq_days_after']<0){
        $response .= "Please enter a valid number for 'Days After Contact Subscribed'.<br/>";
        $response_type = "failure";
    }
    if($_POST['seq_hour']=="" || $_POST['seq_minute']==""){
        $response .= "Please enter the time to send the sequence.<br/>";
        $response_type = "failure";
    }
    if($_POST['seq_name']==""){
        $response .= "Please enter a name for the sequence.<br/>";
        $response_type = "failure";
    }

    if (s8_is_str_blank($live_answer_content) && $_POST['ad_vb_broadcast_type'] == "VoiceBroadcast") :
        $response .= 'Please add Live Answer.<br/>';
        $response_type = 'failure';
        $live_answer_content = '';
    endif;

    if (s8_is_str_blank($voicemail_message_content) && $_POST['ad_vb_broadcast_type'] == "VoiceBroadcast"):
        $response .= 'Please add Voicemail Message.';
        $response_type = 'failure';
        $voicemail_message_content = '';
    endif;

    if(s8_is_str_blank($_POST['sms_message']) && $_POST['ad_vb_broadcast_type'] == "SMSBroadcast")
        $response_type = 'failure';

    if($response_type != "failure"){

        if($type=="CREATE"){

            $time = $_POST['seq_hour'].":".$_POST['seq_minute']." ".$_POST['seq_period'];
            $main_time = $time;
            $this_date = new DateTime("now", new DateTimeZone($TIMEZONE));
            $this_date->modify($time);
            $this_date->setTimezone(new DateTimeZone("UTC"));
            $time = $this_date->format("g:i A");

            $date_now = new DateTime("now");
            $stmt = $db->customExecute("INSERT INTO ad_vb_sequences (campaign_id,type,name,days_after,time,sms_message,live_answer_content,live_answer_type,live_answer_voice,live_answer_language,voicemail_message_content,voicemail_message_type,voicemail_message_voice,voicemail_message_language) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            if($stmt->execute(array(
                $_GET['campaign_id'],
                $_POST['ad_vb_broadcast_type'],
                $_POST['seq_name'],
                $_POST['seq_days_after'],
                $main_time,
                $_POST['sms_message'],
                $live_answer_content,
                $live_answer_type,
                $live_answer_voice,
                $live_answer_language,
                $voicemail_message_content,
                $voicemail_message_type,
                $voicemail_message_voice,
                $voicemail_message_language
            ))){
                $sequence_id = $db->lastInsertId();
                $_GET['id'] = $sequence_id;
                $type="CREATED";

                $stmt = $db->customExecute("SELECT contact_idx,date_added FROM ad_advb_cl_link WHERE list_idx = ?");
                $stmt->execute(array($campaign_data['list_id']));
                $contact_ids = $stmt->fetchAll(PDO::FETCH_OBJ);
                if(is_array($contact_ids))
                    foreach($contact_ids as $contact_id){
                        $stmt = $db->customExecute("SELECT * FROM ad_advb_cl_contacts WHERE idx = ?");
                        $stmt->execute(array($contact_id->contact_idx));
                        $contact = $stmt->fetch(PDO::FETCH_OBJ);
                        if(is_object($contact)){

                            $date_added = new DateTime($contact_id->date_added,new DateTimeZone($TIMEZONE));
                            $date_added->modify("+".$_POST['seq_days_after']." days");
                            $date_added->modify($_POST['seq_hour'].":".$_POST['seq_minute']." ".$_POST['seq_period']);
                            $date_added->setTimezone(new DateTimeZone("UTC"));

                            if($date_added->format("U")>$date_now->format("U")){
                                $scheduled_date = new DateTime("+".$_POST['seq_days_after']." days",new DateTimeZone($TIMEZONE));
                                $scheduled_date->modify($time);
                                $scheduled_date->setTimezone(new DateTimeZone("UTC"));

                                $stmt = $db->customExecute("INSERT INTO ad_vb_sequence_schedule (sequence_id,cron_id,contact_id,`date`) VALUES (?,'',?,?)");
                                $stmt->execute(array($sequence_id,$contact->idx,$scheduled_date->format(Util::STANDARD_MYSQL_DATE_FORMAT)));
                                $schedule_id = $db->lastInsertId();

                                $retn = @ad_add_cron_task($scheduled_date->format(Util::STANDARD_MYSQL_DATE_FORMAT),dirname(s8_get_current_webpage_uri()) . '/ad_cron.php?action=process_sequence&schedule_id='.
                                    $schedule_id);

                                if(@$retn->result == "success"){
                                    $cb = $retn->data;
                                    $stmt = $db->customExecute("UPDATE ad_vb_sequence_schedule SET cron_id = ? WHERE id = ?");
                                    $stmt->execute(array($cb->id,$schedule_id));
                                }else{
                                    die("AR Cron Error: ".@$retn->error);
                                }
                            }
                        }
                    }

                $response .= 'Successfully created sequence. <a href="ad_vb_sequences_edit.php?campaign_id='.$_GET['campaign_id'].'">Click here</a> to proceed.';
                $response_type = 'success';
            }else{
                $response .= "Failed to create sequence.";
                $response_type = 'failure';
            }
        }else{
            $date_now = new DateTime("now",new DateTimeZone($TIMEZONE));
            $sequence_data = array(
                'type'=>$_POST['ad_vb_broadcast_type'],
                'name'=>$_POST['seq_name'],
                'days_after'=>$_POST['seq_days_after'],
                'time'=>$_POST['seq_hour'].":".$_POST['seq_minute']." ".$_POST['seq_period'],
                'sms_message'=>$_POST['sms_message'],
                'live_answer_content'=>$live_answer_content,
                'live_answer_type'=>$live_answer_type,
                'live_answer_voice'=>$live_answer_voice,
                'live_answer_language'=>$live_answer_language,
                'voicemail_message_content'=>$voicemail_message_content,
                'voicemail_message_type'=>$voicemail_message_type,
                'voicemail_message_voice'=>$voicemail_message_voice,
                'voicemail_message_language'=>$voicemail_message_language
            );
            $status = ad_vb_update_sequence($_GET['id'],$sequence_data);
            if($status){
                $sequence_id = $_GET['id'];
                $stmt = $db->customExecute("SELECT id,cron_id FROM ad_vb_sequence_schedule WHERE sequence_id = ?");
                $stmt->execute(array($sequence_id));
                foreach($stmt->fetchAll(PDO::FETCH_OBJ) as $sequence){
                    @ad_del_cron_task($sequence->cron_id);
                    $db->customExecute("DELETE FROM ad_vb_sequence_schedule WHERE id = ?")->execute(array($sequence->id));
                }

                $stmt = $db->customExecute("SELECT contact_idx,date_added FROM ad_advb_cl_link WHERE list_idx = ?");
                $stmt->execute(array($campaign_data['list_id']));
                $contact_ids = $stmt->fetchAll(PDO::FETCH_OBJ);
                if(is_array($contact_ids))
                    foreach($contact_ids as $contact_id){
                        $stmt = $db->customExecute("SELECT * FROM ad_advb_cl_contacts WHERE idx = ?");
                        $stmt->execute(array($contact_id->contact_idx));
                        $contact = $stmt->fetch(PDO::FETCH_OBJ);
                        if(is_object($contact)){
                            $date_added = new DateTime($contact_id->date_added,new DateTimeZone("UTC"));
                            $date_added->setTimezone(new DateTimeZone($TIMEZONE));
                            $date_added->modify("+".$_POST['seq_days_after']." days");
                            $date_added->modify($_POST['seq_hour'].":".$_POST['seq_minute']." ".$_POST['seq_period']);
                            //echo $date_added->format(Util::STANDARD_LOG_DATE_FORMAT)." - ".$date_now->format(Util::STANDARD_LOG_DATE_FORMAT)."\n";

                            if($date_added>$date_now){
                                $scheduled_date = $date_added;

                                $stmt = $db->customExecute("INSERT INTO ad_vb_sequence_schedule (sequence_id,cron_id,contact_id,`date`) VALUES (?,'',?,?)");
                                $stmt->execute(array($sequence_id,$contact->idx,$scheduled_date->format(Util::STANDARD_MYSQL_DATE_FORMAT)));
                                $schedule_id = $db->lastInsertId();

                                $retn = @ad_add_cron_task($scheduled_date->format(Util::STANDARD_MYSQL_DATE_FORMAT),dirname(s8_get_current_webpage_uri()) . '/ad_cron.php?action=process_sequence&schedule_id='.
                                    $schedule_id);

                                if(@$retn->result == "success"){
                                    $cb = $retn->data;
                                    $stmt = $db->customExecute("UPDATE ad_vb_sequence_schedule SET cron_id = ? WHERE id = ?");
                                    $stmt->execute(array($cb->id,$schedule_id));
                                }else{
                                    die("AR Cron Error: ".@$retn->error);
                                }
                            }
                        }
                    }
                $response .= 'Successfully updated sequence. <a href="ad_vb_sequences_edit.php?campaign_id='.$_GET['campaign_id'].'">Click here</a> to proceed.';
                $response_type = 'success';
                $type = "EDIT";
                $stmt = $db->customExecute("SELECT * FROM ad_vb_sequences WHERE id = ?");
                $stmt->execute(array($_GET['id']));
                $sequence = $stmt->fetch(PDO::FETCH_ASSOC);
                $separated = explode(" ",$sequence['time']);
                $ex = explode(":",$separated[0]);
                $s_time = array($ex[0],$ex[1],$separated[1]);
            }else{
                $response .= "Failed to update sequence.";
                $response_type = 'failure';
            }
        }

    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $title; ?><?php echo $campaign_data['campaign_name'];?>: Sequence Edit</title>
    <?php include "include/css.php"; ?>
    <style type="text/css">
        #token_list {
            height: 152px;
            margin-left: 8px;
            width: 21%;
            margin-bottom: 7px;
            padding: 5px;
            background: #fefefe;
            border-radius: 3px;
            border: 1px solid #bbb;
            font-family: Helvetica,"Lucida Grande", Verdana, sans-serif;
            font-size: 14px;
            color: #333;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            outline: none;
        }
        .ui-widget-content {border-radius: 5px;}

        .ui-widget-content a {    position: absolute;
            right: 10px;
            top: 5px;}
    </style>
</head>
<body>
<div id="hld">
    <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
        <?php
        //Displaying the navigation menu on page
        include('include/nav.php');
        ?>
        <!--Initializing the html part that will display the campaign list-->
        <div class="block">
            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>
                <h2><?php echo $campaign_data['campaign_name'];?>: <?php echo $type=="EDIT"?"Edit":"Create"; ?> Sequence</h2>
                <ul>
                    <li><a href="ad_vb_sequences_edit.php?campaign_id=<?php echo $_GET['campaign_id'];?>">Back To Sequences</a></li>
                </ul>
            </div>
            <div class="block_content">
                <?php
                if ($response_type == 'success'):
                    ?>
                    <div class="message success"><p><?php echo $response; ?></p></div>
                    <?php
                elseif ($response_type == 'failure'):
                    ?>
                    <div class="message errormsg"><p><?php echo $response; ?></p></div>
                    <?php
                elseif ($response_type == 'warning'):
                    ?>
                    <div class="message warning"><p><?php echo $response; ?></p></div>
                    <?php
                elseif ($response_type == 'info'):
                    ?>
                    <div class="message info"><p><?php echo $response; ?></p></div>
                    <?php
                endif;
                if($type!="CREATED"){
                ?>
                <div>
                    <form enctype="multipart/form-data" action="" method="POST">
                    <table>
                        <tr>
                            <td><label for="seq_name">Sequence Name: </label></td>
                            <td><input type="text" value="<?php echo @$sequence['name']; ?>" class="text small" style="display: inline-block !important; width: 250px;" id="seq_name" name="seq_name" /></td>
                        </tr>
                        <tr>
                            <td><label for="seq_days_after">Days After Contact Subscribed: </label></td>
                            <td><input type="text" value="<?php echo @$sequence['days_after']; ?>" class="text small" style="display: inline-block !important; width: 55px; text-align: center;" id="seq_days_after" name="seq_days_after" /></td>
                        </tr>
                    </table>

                        <p style="">
                            <label>Broadcast Type:</label><br />
                            <input type="radio" style="margin-left: 3px;" name="ad_vb_broadcast_type" value="VoiceBroadcast" id="ad_vb_broadcast_type_voice"<?php if(@$sequence['type']=="VoiceBroadcast"||$type=="CREATE") echo " checked"; ?> class="checkbox left" /> <label class="marginleft10 left lh0" for="ad_vb_broadcast_type_voice">Voice Broadcast</label>
                            <br/>
                            <input type="radio" style="margin-left: 3px;" name="ad_vb_broadcast_type" value="SMSBroadcast" id="ad_vb_broadcast_type_sms"<?php if(@$sequence['type']=="SMSBroadcast") echo " checked"; ?> class="checkbox left" /> <label class="marginleft10 left lh0" for="ad_vb_broadcast_type_sms">SMS Broadcast</label>
                        </p>

                        <div class="fileupload">
                            <label>Live Answer:</label><br />
                            <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                <input type="hidden" class="content" name="live_answer_content" value="<?php echo $live_answer_content; ?>" />
                                <input type="hidden" class="type" name="live_answer_type" value="<?php echo $live_answer_type; ?>" />
                                <input type="hidden" class="voice" name="live_answer_voice" value="<?php echo $live_answer_voice; ?>" />
                                <input type="hidden" class="language" name="live_answer_language" value="<?php echo $live_answer_language; ?>" />
                                <div class="ivr-Menu-selector" style="display: block">
                                    <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                      <?php if ($live_answer_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                      <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($live_answer_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                    </div>
                                    <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                      <?php if ($live_answer_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                      <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($live_answer_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                    </div>
                                    <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                      <?php if ($live_answer_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                      <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($live_answer_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                    </div>
                                    <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                      <?php if ($live_answer_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                      <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($live_answer_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                    </div>
                                  </div>
                                  <div class="ivr-Menu-editor ">
                                    <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                      <div class="ivr-Menu-read-text" style="display: none;">
                                        <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                        <br>
                                        <div>
                                          <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                            <label class="field-label">
                                                <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($live_answer_type == 'Text')? $live_answer_content:''); ?></textarea>

                                                <?php
                                                $voice = (($live_answer_type == 'Text')? $live_answer_voice:'');
                                                $language = (($live_answer_type == 'Text')? $live_answer_language:'');
                                                ?>
                                                <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                  <?php
                                                  echo Util::getTwilioVoices($voice);
                                                  ?>
                                                </select>

                                                <br clear="all" />

                                                <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                  <?php
                                                  echo Util::getTwilioLanguages($voice, $language);
                                                  ?>
                                                </select>

                                                <br clear="all" /><br />

                                                <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                <script type="text/javascript">
                                                  $(document).ready(function() {
                                                    $("#voice").trigger("change");
                                                  });
                                                </script>

                                                <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                            </label>
                                          </fieldset>
                                        </div>
                                        <br>
                                        <br>
                                      </div>
                                      <div class="ivr-audio-upload" style="display: none;">
                                        <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                        <div class="swfupload-container">
                                          <div class="explanation"> <br>
                                            
                                            <span class="title" <?php if ( $live_answer_type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                            <?php 
                                      echo (($live_answer_type == 'Audio')? $live_answer_content:''); ?>
                                            </strong></span> <br>
                                            
                                            
                                            <span class="title">Click to select a file: </span>
                                            <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                            <span class="title" id="statusUpload">&nbsp;</span>
                                          </div>                                                   
                                        
                                        </div>
                                      </div>
                                      <div class="ivr-mp3-url" style="display: none;">
                                        <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                        <div class="swfupload-container">
                                          <div class="explanation"> <br>           
                                            
                                            <span class="title">
                                              <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($live_answer_type == 'MP3_URL')? $live_answer_content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                              <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                            </span>

                                            <br /><br />

                                            <span class="title" <?php if ( $live_answer_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                            <?php 
                                      echo (($live_answer_type == 'MP3_URL')? $live_answer_content:''); ?>
                                            </strong></span>

                                          </div>
                                        
                                        
                                        </div>
                                      </div>
                                      <div class="ivr-record-audio" style="display: none;">
                                        <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                        <div class="swfupload-container">
                                          <div class="explanation"> <br>           
                                            
                                            Caller ID:

                                            <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                              <option value="">Select number</option>
                                              <?php
                                              $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                              foreach ($numbers as $number) {
                                                ?>
                                                  <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                <?php
                                              }
                                              ?>
                                            </select>

                                            Your phone number:

                                            <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                            <br /><br />

                                            <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                            <br /><br />

                                            <span class="title" <?php if ( $live_answer_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                                <?php if ($live_answer_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($live_answer_content, 'sm'); } ?>
                                            </span>

                                          </div>
                                        
                                        
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </fieldset>
                        </div>

                        <div class="fileupload">
                            <label>Voicemail Message:</label><br />
                            <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                <input type="hidden" class="content" name="voicemail_message_content" value="<?php echo $voicemail_message_content; ?>" />
                                <input type="hidden" class="type" name="voicemail_message_type" value="<?php echo $voicemail_message_type; ?>" />
                                <input type="hidden" class="voice" name="voicemail_message_voice" value="<?php echo $voicemail_message_voice; ?>" />
                                <input type="hidden" class="language" name="voicemail_message_language" value="<?php echo $voicemail_message_language; ?>" />
                                <div class="ivr-Menu-selector" style="display: block">
                                    <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                      <?php if ($voicemail_message_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                      <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($voicemail_message_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                    </div>
                                    <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                      <?php if ($voicemail_message_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                      <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($voicemail_message_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                    </div>
                                    <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                      <?php if ($voicemail_message_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                      <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($voicemail_message_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                    </div>
                                    <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                      <?php if ($voicemail_message_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                      <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($voicemail_message_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                    </div>
                                  </div>
                                  <div class="ivr-Menu-editor ">
                                    <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                      <div class="ivr-Menu-read-text" style="display: none;">
                                        <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                        <br>
                                        <div>
                                          <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                            <label class="field-label">
                                                <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($voicemail_message_type == 'Text')? $voicemail_message_content:''); ?></textarea>

                                                <?php
                                                $voice = (($voicemail_message_type == 'Text')? $voicemail_message_voice:'');
                                                $language = (($voicemail_message_type == 'Text')? $voicemail_message_language:'');
                                                ?>
                                                <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                  <?php
                                                  echo Util::getTwilioVoices($voice);
                                                  ?>
                                                </select>

                                                <br clear="all" />

                                                <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                  <?php
                                                  echo Util::getTwilioLanguages($voice, $language);
                                                  ?>
                                                </select>

                                                <br clear="all" /><br />

                                                <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                <script type="text/javascript">
                                                  $(document).ready(function() {
                                                    $("#voice").trigger("change");
                                                  });
                                                </script>

                                                <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                            </label>
                                          </fieldset>
                                        </div>
                                        <br>
                                        <br>
                                      </div>
                                      <div class="ivr-audio-upload" style="display: none;">
                                        <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                        <div class="swfupload-container">
                                          <div class="explanation"> <br>
                                            
                                            <span class="title" <?php if ( $voicemail_message_type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                            <?php 
                                      echo (($voicemail_message_type == 'Audio')? $voicemail_message_content:''); ?>
                                            </strong></span> <br>
                                            
                                            
                                            <span class="title">Click to select a file: </span>
                                            <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                            <span class="title" id="statusUpload">&nbsp;</span>
                                          </div>                                                   
                                        
                                        </div>
                                      </div>
                                      <div class="ivr-mp3-url" style="display: none;">
                                        <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                        <div class="swfupload-container">
                                          <div class="explanation"> <br>           
                                            
                                            <span class="title">
                                              <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($voicemail_message_type == 'MP3_URL')? $voicemail_message_content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                              <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                            </span>

                                            <br /><br />

                                            <span class="title" <?php if ( $voicemail_message_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                            <?php 
                                      echo (($voicemail_message_type == 'MP3_URL')? $voicemail_message_content:''); ?>
                                            </strong></span>

                                          </div>
                                        
                                        
                                        </div>
                                      </div>
                                      <div class="ivr-record-audio" style="display: none;">
                                        <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                        <div class="swfupload-container">
                                          <div class="explanation"> <br>           
                                            
                                            Caller ID:

                                            <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                              <option value="">Select number</option>
                                              <?php
                                              $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                              foreach ($numbers as $number) {
                                                ?>
                                                  <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                <?php
                                              }
                                              ?>
                                            </select>

                                            Your phone number:

                                            <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                            <br /><br />

                                            <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                            <br /><br />

                                            <span class="title" <?php if ( $voicemail_message_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                                <?php if ($voicemail_message_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($voicemail_message_content, 'sm'); } ?>
                                            </span>

                                          </div>
                                        
                                        
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </fieldset>
                            <br />
                        </div>

                        <p class="ad_vb_ac_sms_message" style="display:none;">
                            <label>Message:</label><br />
                            <textarea name="sms_message" maxlength="1600" style="width: 700px; max-width: 700px;"><?php if($type=="CREATE") echo htmlentities(@$_POST['sms_message']); else echo htmlentities(@$sequence['sms_message']); ?></textarea>
                            <select id="token_list" name="ad_ad_tokens" size="10">
                                <option value="[FirstName]">[FirstName]</option>
                                <option value="[LastName]">[LastName]</option>
                                <option value="[Email]">[Email]</option>
                                <option value="[Phone]">[Phone]</option>
                                <option value="[Address]">[Address]</option>
                                <option value="[City]">[City]</option>
                                <option value="[State]">[State]</option>
                                <option value="[Zip]">[Zip]</option>
                                <option value="[Website]">[Website]</option>
                                <option value="[Business]">[Business]</option>
                            </select>
                        </p>

                        <script type="text/javascript">
                            $('[name="ad_vb_broadcast_type"], #ad_vb_broadcast_type_sms, #ad_vb_broadcast_type_voice').click(function() {
                                if ($('#ad_vb_broadcast_type_sms').is(':checked')) {
                                    $('.ad_vb_ac_sms_message').slideDown();
                                    $('.fileupload').slideUp();
                                } else {
                                    $('.ad_vb_ac_sms_message').slideUp();
                                    $('.fileupload').slideDown();
                                }
                            });
                            $(document).ready(function(){
                                if ($('#ad_vb_broadcast_type_sms').is(':checked')) {
                                    $('.ad_vb_ac_sms_message').slideDown();
                                    $('.fileupload').slideUp();
                                } else {
                                    $('.ad_vb_ac_sms_message').slideUp();
                                    $('.fileupload').slideDown();
                                }
                            });
                        </script>

                    <label style="display:block">Send At:</label>
                    <select name="seq_hour" style="display:inline-block">
                        <option value="">Hour</option>
                        <?php for($i=1; $i < 13; $i++) {echo '<option'. ($s_time[0]==$i?" selected":"") .' value="'.$i.'">'.$i.'</option>';} ?>
                    </select>
                    :
                    <select name="seq_minute" style="display:inline-block">
                        <option value="">Minute</option>
                        <?php for($i=0; $i < 60; $i++) { $s = ($i<10 ? "0".$i : $i); echo '<option'.($s_time[1]==$i?" selected":"").' value="'.$s.'">'.$s.'</option>';} ?>
                    </select>
                    &nbsp;
                    <select name="seq_period" style="display:inline-block">
                        <option<?php if(@$s_time[2]=="AM") echo " selected";?> value="AM">AM</option>
                        <option<?php if(@$s_time[2]=="PM") echo " selected";?> value="PM">PM</option>
                    </select>
                        <br><br>
                        <input name="ad_vb_process_settings_form" style="float:left;" type="submit" class="submit small" value="Save" />
                        <input style="float:left;" type="button" class="submit small" value="Cancel" onclick="window.document.location = 'ad_vb_sequences_edit.php?campaign_id=<?php echo $_GET['campaign_id'];?>';" />
                    </form>
                    <br/><br/>
                </div><?php } ?>
            </div>		<!-- .block_content ends -->
            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>
        <!--Exiting the HTML code that will display the campaign list-->

        <!-- #header ends -->
        <?php include "include/footer.php"; ?>
    </div>
</div>

<!--//Notification bar html-->
<div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
<script type="text/javascript" language="javascript">
    $.fn.insertAtCaret = function (tagName) {
        return this.each(function(){
            if (document.selection) {
                //IE support
                this.focus();
                sel = document.selection.createRange();
                sel.text = tagName;
                this.focus();
            }else if (this.selectionStart || this.selectionStart == '0') {
                //MOZILLA/NETSCAPE support
                startPos = this.selectionStart;
                endPos = this.selectionEnd;
                scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos) + tagName + this.value.substring(endPos,this.value.length);
                this.focus();
                this.selectionStart = startPos + tagName.length;
                this.selectionEnd = startPos + tagName.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += tagName;
                this.focus();
            }
        });
    };
    $("#token_list").find("option").on('dblclick', function(){
        insertToken(this.value);
    });

    function insertToken(token){
        $("textarea[name='sms_message']").insertAtCaret(token);
    }
    if ($ === undefined) {
        $ = jQuery;
    }

    /**
     * This function will display the notification message on screen.
     * @param {string} message The message to be displayed in notification bar
     * @returns {undefined}
     */
    function ad_display_message(message) {
        $('.ad_notification').html(message);
        $('.ad_notification').slideDown();
        setTimeout(function() {
            $('.ad_notification').fadeOut(3000);
        }, 3000);

    }

    /**
     * This function will check whether string is blank or not
     * @param {string} str The value to be checked
     * @returns {Boolean} returns boolean TRUE or FALSE based on check performed.
     */
    function ad_is_str_blank(str) {
        if (str === '' || str === ' ' || str === null || str === undefined) {
            return true;
        } else {
            return false;
        }
    }

    //When document is ready to attache events to its elements
    $(document).ready(function() {
        $(".tt").tooltipster({
            position:'bottom',
            theme:'.tooltipster-shadow'
        });
    });

    function showAudioText(obj)
    {
        
        
        var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
        audioChoice.hide();
        audioChoice.parent().children('.ivr-Menu-editor').show();
        var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');
        
        if ( obj.id  == 'txt' ) 
        {       
            
            subDiv.children('.ivr-Menu-read-text').show();
            
            //////////////// only to avoid  file button clickable in text area      
            $('[name="uploadfile"]').css('z-index','-1');       
        
        }
        else if ( obj.id  == 'upload_mp3' ) {
            $('[name="uploadfile"]').css('z-index','2147483583');
            
            subDiv.children('.ivr-audio-upload').show();    
            SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');        
            UploadFile(SubObj); 
        }
        else if ( obj.id  == 'mp3_url' ) {
            subDiv.children('.ivr-mp3-url').show();
        }
        else if ( obj.id  == 'record_audio' ) {
            subDiv.children('.ivr-record-audio').show();
        }
        
    }

    function CloseButton(obj)
    {
        var audioChoice = $(obj).closest('.ivr-Menu');
        
        var audioChoiceEditor   = audioChoice.children('.ivr-Menu-editor');
        var audioChoiceSelector = audioChoice.children('.ivr-Menu-selector');
        
        var subDiv  = audioChoiceEditor.children('.ivr-Menu-editor-padding');
        
        audioChoiceSelector.show();
        audioChoiceEditor.hide();
        subDiv.children('.ivr-audio-upload').hide();    
        subDiv.children('.ivr-Menu-read-text').hide();
        subDiv.children('.ivr-mp3-url').hide();
        subDiv.children('.ivr-record-audio').hide();
    }

    function testVoice(voice, language, text) {
        if ($("#test_voice_iframe_wrapper").length == 0) {
            $("body").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
                                '<form id="test_voice_form" action="test_voice.php" method="post" target="test_voice_iframe">' +
                                    '<input type="hidden" name="voice" id="voice" />' +
                                    '<input type="hidden" name="language" id="language" />' +
                                    '<input type="hidden" name="text" id="text" />' +
                                    '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                '</form>' +
                                '<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
                            '</form>'
            );
        }

        $("#test_voice_iframe_wrapper #voice").val(voice);
        $("#test_voice_iframe_wrapper #language").val(language);
        $("#test_voice_iframe_wrapper #text").val(text);
        $('#test_voice_iframe_wrapper #submit').click()
     }

     var closeBtnHtml = '<a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a>';

     function SaveContent(obj, content_type) {
        var thisBlock = $(obj).closest('.ivr-Menu');
        switch (content_type) {
            case "Text_mail":
            {
                var voice_text = $(thisBlock).find('#readtxt_mail').val();
                var voice = $(thisBlock).find('#voice').val();
                var language = $(thisBlock).find('#language').val();
                $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('.ivr-Menu-close-button').click();

                $(thisBlock).find('.content').val(voice_text);
                $(thisBlock).find('.type').val("Text");
                $(thisBlock).find('.voice').val(voice);
                $(thisBlock).find('.language').val(language);

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#txt').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                break;
            }
            case "MP3_URL":
            {

                var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('.ivr-Menu-close-button').click();

                $(thisBlock).find('.content').val(mp3_url);
                $(thisBlock).find('.type').val("MP3_URL");

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#mp3_url').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                break;
            }
        }
    }

    function removeSelectedOption(obj) {
        promptMsg('Are you sure ?', function() {
          var thisBlock = $(obj).closest('.ivr-Menu');
          $(thisBlock).find(".ttsMwCloseBtn").remove();

          $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

          $(thisBlock).find('.content').val('');
          $(thisBlock).find('.type').val('');
          $(thisBlock).find('.voice').val('');
          $(thisBlock).find('.language').val('');
        });
    }

    function UploadFile(obj)
    {
        var status = $(obj).closest('.explanation').find('#statusUpload');
        var fileNameStatus = $(obj).closest('.explanation').find('#voicefilename');
        var fileNameStatusWrapper = $(obj).closest('.explanation').find('#voicefilenameWrapper');
        
        new AjaxUpload(obj, {
            
            
            action: 'ad_vb_add_campaign.php?act=uploadMp3',
            name: 'uploadfile',
            onSubmit: function(file, ext){
                 if (! (ext && /^(mp3|wma)$/.test(ext))){ 
                    // extension is not allowed 
                    status.text('Only MP3 files are allowed');
                    return false;
                }
                (fileNameStatus).html('Uploading...');
            },
            onComplete: function(file, response){
                //On completion clear the status
                
                //Add uploaded file to list
                
                if(response!="error"){
                    $(fileNameStatusWrapper).css('display' ,'block');
                    $(fileNameStatus).html(response);
                    var thisBlock = $(obj).closest('.ivr-Menu');

                    $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                    $(thisBlock).find('#upload_mp3').addClass('ivr-Menu-Selected');
                    $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                    $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                    $(thisBlock).find('.content').val(response);
                    $(thisBlock).find('.type').val("Audio");
                    $(thisBlock).find('.ivr-Menu-close-button').click();

                    $(thisBlock).find(".ttsMwCloseBtn").remove();
                    $(thisBlock).find('#upload_mp3').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                } else{
                }
            }
        });
            
    }

    var recordingId = "";
    function recordAudio(obj) {
        if ($(obj).val() == "Call Me") {
            recordingId = createUUID();
            var record_from = $(obj).parents('.ivr-Menu').find('#record_from').val();
            var record_to = $(obj).parents('.ivr-Menu').find('#record_to').val();

            if (record_from == "") {
                errMsgDialog("Please select Caller ID.");
                return false;
            }

            if (record_to == "") {
                errMsgDialog("Please enter your phone number.")
                return false;
            }

            $(obj).val('Stop');

            if ($("#record_audio_iframe_wrapper").length == 0) {
                $("body").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
                                    '<form id="record_audio_form" action="../record_audio.php" method="post" target="record_audio_iframe">' +
                                        '<input type="hidden" name="recordingId" id="recordingId" />' +
                                        '<input type="hidden" name="record_from" id="record_from" />' +
                                        '<input type="hidden" name="record_to" id="record_to" />' +
                                        '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                    '</form>' +
                                    '<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
                                '</form>'
                );
            }

            $("#record_audio_iframe_wrapper #recordingId").val(recordingId);
            $("#record_audio_iframe_wrapper #record_from").val(record_from);
            $("#record_audio_iframe_wrapper #record_to").val(record_to);
            $('#record_audio_iframe_wrapper #submit').click()
        }
        else {
            $(obj).val('Please wait...');
            $("#record_audio_iframe").contents().find("#disconnectBtn").click();

            setTimeout(function() {
                $.post("admin_ajax_handle.php", { func: "GET_RECORDING", recordingId: recordingId },  function(response) {
                    var thisBlock = $(obj).closest('.ivr-Menu');

                    $(thisBlock).find('#recordedAudioSavedWrapper').css('display' ,'block');
                    if (response) {
                        $(thisBlock).find('#recordedAudioSavedWrapper').html(response.playable);
                        
                        $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                        $(thisBlock).find('.content').val(response.url);
                        $(thisBlock).find('.type').val("RECORD_AUDIO");
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                        $(thisBlock).find('#record_audio').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                    }
                    else {
                        errMsgDialog('Audio was not recorded, please try again.');
                        $(thisBlock).find('#recordedAudioSavedWrapper').html('');
                    }
                    $(obj).val('Call Me');
                }, "json");
            }, 2000);
        }
     }

     function createUUID() {
        // http://www.ietf.org/rfc/rfc4122.txt
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";

        var uuid = s.join("");
        return uuid;
    }

</script>
</body>
</html>