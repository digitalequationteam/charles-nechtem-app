<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_contactlist";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object 
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10008)){
    header("Location: index.php");
    exit;
}

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

$act = (isset($_GET['act']) ? $_GET['act'] : "");

if (  $act == 'uploadMp3' )
{   
    $uploaddir = 'audio/opt_out_files/'.$_SESSION['user_id'].'/'; 
    
    @mkdir ('audio/opt_out_files/',0777,true);
    @chmod ('audio/opt_out_files/',0777);
    
    @mkdir ($uploaddir,0777,true);
    @chmod($uploaddir,0777);
    
    $file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name'])); 
     
    if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
    { 
        $filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
    
        echo "$filename"; 
    
    } else {
        echo "error";
    }

    exit();
}

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

//By default setting null values to action responses
$action_response = '';
$action_type = '';

//If current request is to perform some action on current page
if (isset($_GET['action'])):

    //Switching to appropriate action code
    switch ($_GET['action']):

    endswitch;

//Enditing condition checking action request
endif;

$sms_keyword  = "";
$sms_number   = "";
$sms_response = "";

$vb_opt_out_content = "";
$vb_opt_out_type = "";
$vb_opt_out_voice = "";
$vb_opt_out_language = "";
$vb_opt_out_key = "";
$sms_opt_out_message = "";
$sms_opt_out_trigger = "";

if (isset($_POST['ad_ad_process_settings_form'])):

    //Retrieving form data here
    $ad_contact_list_name = $_POST['ad_contact_list_name'];

    //If contact list name supplied is not blank
    if (!s8_is_str_blank($ad_contact_list_name)):

        //If upload csv status check
        $ad_ad_ac_upload_csv = isset($_POST['ad_ad_ac_upload_csv']) ? TRUE : FALSE;
        $ad_ad_ac_upload_csv_file = FALSE;

        //Assuming that file is not uploaded and if uploaded no contact number exist in it.
        //Seeting the null value assuming both case if no other
        $ad_ad_csv_file_contacts = array();

        //If CSV file is being uploaded
        if ($ad_ad_ac_upload_csv):

            //Checking if really there is some file uploaded
            if (isset($_FILES['ad_ad_ac_upload_csv_file'])):

                //Creating uploads directory if not already exists
                if (!file_exists(dirname(__FILE__) . '/uploads'))
                    mkdir(dirname(__FILE__) . '/uploads');

                //If there is no error during upload of file
                if ($_FILES['ad_ad_ac_upload_csv_file']['error'] == 0):

                    //Moving it from temporary location to stable one
                    if (move_uploaded_file($_FILES['ad_ad_ac_upload_csv_file']['tmp_name'], dirname(__FILE__) . '/uploads/' . $_FILES['ad_ad_ac_upload_csv_file']['name'])):
                        //Storing the web accessible URI in variable defined for it dynamically
                        $ad_ad_ac_upload_csv_file = dirname(__FILE__) . '/uploads/' . $_FILES['ad_ad_ac_upload_csv_file']['name'];
                    endif;

                endif;

            endif;

        endif;

        //Retrieving manually added contacts
        $ad_ad_ac_contacts = explode("\n", $_POST['ad_ad_ac_contacts']);

        //If Contacts is an array and there exist atleast one entry
        if (is_array($ad_ad_ac_contacts) && count($ad_ad_ac_contacts) > 0):

            //Looping over contacts
            foreach ($ad_ad_ac_contacts as $key => $value):

                //Building contacts data
                $ad_ad_ac_contacts[$key] = explode(',', $value);

            endforeach;

        endif;

        //If csv file is uploaded
        if ($ad_ad_ac_upload_csv === TRUE && $ad_ad_ac_upload_csv_file != ''):
            $fp = fopen($ad_ad_ac_upload_csv_file, 'r+');
            while ($row = fgetcsv($fp)):
                $ad_ad_csv_file_contacts[] = $row;
            endwhile;
            fclose($fp);
        endif;

        //Deleting the labels row from CSV file contacts data retrieved
        unset($ad_ad_csv_file_contacts[0]);

        //Merging the contacts into single array
        $single_set_contacts = array_merge($ad_ad_ac_contacts, $ad_ad_csv_file_contacts);

        $continue = true;

        if($_POST['ad_ad_sms_optin']==false || $_POST['ad_ad_sms_optin']==""){
            $sms_keyword  = "";
            $sms_number   = "";
            $sms_response = "";
        }else{
            $sms_keyword  = @$_POST['sms_keyword'];
            $sms_number   = @$_POST['sms_number'];
            $sms_response = @$_POST['sms_response'];

            if (s8_is_str_blank($sms_keyword)) {
                $action_type = 'failed';
                $action_response = 'Please enter Opt-In SMS keyword.';
                $continue = false;
            }
            elseif (s8_is_str_blank($sms_response)) {
                $action_type = 'failed';
                $action_response = 'Please enter Opt-In SMS response.';
                $continue = false;
            }
            elseif (s8_is_str_blank($sms_number)) {
                $action_type = 'failed';
                $action_response = 'Please enter Opt-In SMS number.';
                $continue = false;
            }
        }

        $vb_opt_out_content = trim($_POST['vb_opt_out_content']);
        $vb_opt_out_type = $_POST['vb_opt_out_type'];
        $vb_opt_out_voice = $_POST['vb_opt_out_voice'];
        $vb_opt_out_language = $_POST['vb_opt_out_language'];
        $global_opt_out_key = ($db->getVar("global_opt_out_key")) ? $db->getVar("global_opt_out_key") : "1";
        $vb_opt_out_key = $_POST['vb_opt_out_key'];
        $sms_opt_out_message = trim($_POST['sms_opt_out_message']);
        $sms_opt_out_trigger = $_POST['sms_opt_out_trigger'];

        if ($vb_opt_out_key == $global_opt_out_key) {
            $action_type = 'failed';
            $action_response = 'Opt-Out key cannot be the same as Global Opt-Out key.';
            $continue = false;
        }

        if (empty($vb_opt_out_content) || empty($sms_opt_out_message)) {
            $action_type = 'failed';
            $action_response = 'Please enter Opt-Out settings.';
            $continue = false;
        }

        if ($continue) {
            //Adding the contact list into the system
            $cl_idx = ad_advb_cl_add_cl(
                $ad_contact_list_name, 
                $sms_keyword, 
                $sms_number, 
                $sms_response, 
                isset($_REQUEST['ad_cl_shared'])? 1:0,
                $vb_opt_out_content,
                $vb_opt_out_type,
                $vb_opt_out_voice,
                $vb_opt_out_language,
                $vb_opt_out_key,
                $sms_opt_out_message,
                $sms_opt_out_trigger
            );

            //If contact list addedd successfully
            if ($cl_idx !== FALSE){

                //Adding the contacts of list
                ad_advb_cl_add_contacts($cl_idx, $single_set_contacts);

                //Building up the response variables
                $action_response = 'Your request has been processed.';
                $action_type = 'Success';

                //Redirecting the user to contacts lists listing page.
                header('location: ad_contactlist_log.php?m=' . urlencode($action_response) . '&mt=' . $action_type);

                //Exiting the PHP code from furthur execution
                exit;

            }else{
                $action_type = 'failed';
                $action_response = 'Unable to save SMS Opt-In details. SMS number is already used in a different contact list.';
            }
        }
        else {

        }

    else:

        $action_response = 'Contact list name provided is blank.';
        $action_type = 'failed';

    endif;

endif;

//Assuming that this page is not an edit campaign page
$edit_campaign = FALSE;

//Checking if this is a edit campaign page
if (isset($_GET['list_idx'])):

    //Setting the edit campaign variable to TRUE
    $edit_campaign = TRUE;

    //Retrieving the unique contact list ID from request URL.
    $edit_list_idx = $_GET['list_idx'];

    //Retrieving the list name
    $edit_cl_name = ad_advb_cl_get_cl_name($edit_list_idx);

    //If ist do not exist in system db
    if ($edit_cl_name === FALSE):

        //Redirecting the user to contact lists listing page
        header('location: ad_contactlist_log.php');

        //Exiting the PHP code to stop furthur execution
        exit;

    //Ending condition checking list existence
    endif;

    //Retrieving the campaign details
    $edit_campaign_details = ad_advb_cl_get_contact_list_details($edit_list_idx);

    if (empty($_POST)) {
        $sms_keyword = $edit_campaign_details['optin_keyword'];
        $sms_response = $edit_campaign_details['optin_response'];
        $sms_number = $edit_campaign_details['optin_number'];

        $vb_opt_out_content = $edit_campaign_details['vb_opt_out_content'];
        $vb_opt_out_type = $edit_campaign_details['vb_opt_out_type'];
        $vb_opt_out_voice = $edit_campaign_details['vb_opt_out_voice'];
        $vb_opt_out_language = $edit_campaign_details['vb_opt_out_language'];
        $vb_opt_out_key = $edit_campaign_details['vb_opt_out_key'];
        $sms_opt_out_message = $edit_campaign_details['sms_opt_out_message'];
        $sms_opt_out_trigger = $edit_campaign_details['sms_opt_out_trigger'];
    }

endif;

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
        <style type="text/css">
            .cmf-skinned-select {
                display:inline-block !important;
            }
        </style>
    </head>

    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
//Displaying the navigation menu on page
                include('include/nav.php');
                ?>

            <?php if(@$_GET['list_idx'] != "") { ?>
            <div class="block">
                <div class="block_head">
                    <div class="bheadl"></div>
                    <div class="bheadr"></div>
                    <h2>API URL to add contacts remotely:</h2>
                </div>
                <div class="block_content">
                    <form>
                        <input id="api_url" type="text" class="text big" style="font-weight:normal;margin-bottom: 6px;" value="" />
                    </form>
                    <script type="text/javascript">
                        $("#api_url").on('mouseup',function(e) { e.preventDefault(); $(this).select(); } );
                        document.getElementById("api_url").value=document.location.href.split("ad_contactlist_add.php")[0]+'ad_ajax.php?action=AddContact&k=<?php echo urlencode(base64_encode($_GET['list_idx'])); ?>&phone=&firstname=&lastname=&email=';
                    </script>
                </div>
                <!-- .block_content ends -->
                <div class="bendl"></div>
                <div class="bendr"></div>
            </div>
            <div class="clear"></div>
            <?php } ?>


                <div class="block bulk_dial_status" style="display: none;background: none;">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2 style="color:red;" class="ad_ad_bulk_dial_status_text"></h2>
                    </div>
                </div>
                <div class="clear"></div>

                <!--Auto dialer campaigns listing section starts here-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <ul style="float: right;">
                            <li><a href="ad_contactlist_log.php">Manage Contact List</a></li>
                            <li><a href="ad_contactlist_add.php">ADD Contact List</a></li>
                        </ul>
                        <h2>
                            <?php
                            if ($edit_campaign):
                                echo 'Edit Contact List';
                            else:
                                echo 'Create Contact List';
                            endif;
                            ?>
                        </h2>
                    </div>
                    <!-- .block_head ends -->
                    <div class="block_content">
                        <?php
//Checkin if action message is set or not
                        if (!s8_is_str_blank($action_response)):

                            //If action message is set, switching to type of action message
                            switch (strtolower($action_type)):

                                //If action message is of success type
                                case 'success':
                                    //Displaying success message
                                    ?>
                                    <div class="message success">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                                //If action message is of failure type
                                case 'failed':
                                    //Displaying failure message
                                    ?>
                                    <div class="message errormsg">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                                //If action message is of failure type
                                case 'warning':
                                    //Displaying failure message
                                    ?>
                                    <div class="message warning">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                                //If action message is of failure type
                                case 'info':
                                    //Displaying failure message
                                    ?>
                                    <div class="message info">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                            endswitch;

                        endif;

//If form processed successfully
                        if (strtolower($action_type) != 'success'):
                            ?>

                            <form enctype="multipart/form-data" action="<?php echo s8_get_current_webpage_uri(); ?>" method="post" id="add_cl_form">
                                <p>
                                    <label>Contact List Name:</label><br />
                                    <input type="text" class="text big" name="ad_contact_list_name" value="<?php echo isset($_POST['ad_contact_list_name']) ? Util::escapeString($_POST['ad_contact_list_name']) : ($edit_campaign === TRUE ? Util::escapeString($edit_campaign_details['list_name']) : ''); ?>" <?php echo $edit_campaign === TRUE ? ' readonly="readonly" ' : ''; ?> />
                                </p>
                                <?php if(@$edit_campaign_details['user']=="" || @$edit_campaign_details['user']==@$_SESSION['user_id']) { ?>
                                <p>
                                    <input style="display: inline-block !important;" type="checkbox" name="ad_cl_shared" id="ad_cl_shared"<?php if(@$edit_campaign_details['shared']==1 || @$_POST['ad_cl_shared'] && $edit_campaign_details['user']) { echo " checked";} ?>/> <label for="ad_cl_shared">Share this list with my companies.</label>
                                </p>
                                <?php } ?>
                                <br/>
                                <script type="text/javascript">

                                    //If somehow, $ is not defined
                                    //Tryinh the alternate variable of jQuery too
                                    if ($ == undefined) {
                                        $ = jQuery;
                                    }

                                    //When document is fully loaded
                                    $(window).load(function() {

                                        if($("#ad_ad_sms_optin").is(":checked")){
                                            $("#ad_ad_sms_optin_box").slideDown('fast');
                                        }else{
                                            $("#ad_ad_sms_optin_box").slideUp('fast');
                                        }

                                        //Displaying the checked section on page load
                                        if ($('#cbdemo1').is(':checked')) {
                                            $('.ad_ad_ac_contacts').slideDown('fast');
                                        } else {
                                            $('.ad_ad_ac_contacts').slideUp('fast');
                                        }

                                        if ($('#cbdemo2').is(':checked')) {
                                            $('.ad_ad_ac_upload_csv_file').slideDown('fast');
                                        } else {
                                            $('.ad_ad_ac_upload_csv_file').slideUp('fast');
                                        }

                                    });

                                    //When current document is ready to perform operations
                                    $(document).ready(function() {

                                        //Attaching handler to checkbox to perform relative action
                                        //on contacts section
                                        $('#ad_ad_sms_optin').click(function() {
                                            if ($(this).is(':checked')) {
                                                $('#ad_ad_sms_optin_box').slideDown('fast');
                                            } else {
                                                $('#ad_ad_sms_optin_box').slideUp('fast');
                                            }
                                        });
                                        $('#cbdemo1').click(function() {
                                            if ($(this).is(':checked')) {
                                                $('.ad_ad_ac_contacts').slideDown('fast');
                                            } else {
                                                $('.ad_ad_ac_contacts').slideUp('fast');
                                            }
                                        });
                                        $('#cbdemo2').click(function() {
                                            if ($(this).is(':checked')) {
                                                $('.ad_ad_ac_upload_csv_file').slideDown('fast');
                                            } else {
                                                $('.ad_ad_ac_upload_csv_file').slideUp('fast');
                                            }
                                        });
                                    });
                                </script>

                                <p>
                                    <b>Contacts data Format & Order:</b> Phone Number, Business Name, First Name, Last Name, Email Address, Address, City, State, Zip, Web site
                                    <br/><sup style="color:black;font-style: italic;">In CSV file, skip the 1st row and start putting data from 2nd row. You can use the first row for labeling the columns. First row of CSV file will be ignored by system.</sup>
                                </p>

                                <p>
                                    <input style="float:left;margin-top: 2px !important;" name="ad_ad_ac_upload_csv" value="true" type="checkbox" class="checkbox" id="cbdemo2" <?php echo isset($_POST['ad_ad_ac_upload_csv']) ? ' checked="checked" ' : '' ?> />
                                    <label class="left marginleft10 lh0" for="cbdemo2">Upload CSV?</label>
                                    <a href="<?php echo dirname(s8_get_current_webpage_uri()) . '/ad_contactlist_download_template.php'; ?>"><input type="button" class="submit long left marginleft10" style="clear: none;" value="Download Template" /></a>
                                </p>

                                <p class="fileupload ad_ad_ac_upload_csv_file" style="display: none;">
                                    <label>Upload CSV File:</label><br />
                                    <input type="file" id="fileupload1" name="ad_ad_ac_upload_csv_file" style="float: left;" /><input type="button" value="Preview" class="cl_preview_csv submit small" />
                                </p>

                                <script type="text/javascript">

                                    //Coding the jquery plugin to center the 
                                    //elements covered by jquery selector
                                    $.fn.center = function() {
                                        this.css("position", "absolute");
                                        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
                                        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
                                        return this;
                                    }

                                    function new_cl_update_ui() {

                                        //Retrieving all contacts 
                                        //and their total
                                        var all_contacts = ($('.ad_cl_add_contacts_manually').val()).split("\n");
                                        var all_contacts_count = all_contacts.length;
                                        $('.new_campaign_contact_list tbody').html('');

                                        for (var i = 0; i < all_contacts_count; i++) {
                                            var current_contact_data = all_contacts[i].split(',');
                                            if (current_contact_data[0] !== null && current_contact_data[0] !== undefined && current_contact_data[0] !== '') {
                                                var contact_html = '\
                                                                      <tr>\n\
                                                                        <td>' + decodeURIComponent(current_contact_data[0]) + '</td>\n\
                                                                        <td>' + decodeURIComponent(current_contact_data[2]) + '</td>\n\
                                                                        <td>' + decodeURIComponent(current_contact_data[3]) + '</td>\n\
                                                                        <td>' + decodeURIComponent(current_contact_data[4]) + '</td>\n\
                                                                        <td>\n\
                                                                        <a href="#" class="new_cl_del_contact" rel="' + i + '">Delete</a>\n\
                                                                        | <a href="#" class="new_cl_edit_contact" rel="' + i + '">Edit</a></td>\n\
                                                                                                  ';
                                                $('.new_campaign_contact_list tbody').append(contact_html);
                                            }
                                        }

                                    }

                                    //WHen document is ready to perform operations
                                    $(document).ready(function() {

                                        //When preview button is clicked
                                        $('.cl_preview_csv').click(function(e) {

                                            //Creating a new form data
                                            var fd = new FormData();

                                            //Appending the file data to FD
                                            fd.append('csv_file', document.getElementById('fileupload1').files[0]);

                                            //Firing the ajax request to upload the file
                                            $.ajax({
                                                url: 'ad_ajax.php?action=handle_csv_upload',
                                                data: fd,
                                                processData: false,
                                                contentType: false,
                                                type: 'POST',
                                                beforeSend: function() {
                                                    //Blocking the UI
                                                    $.blockUI({message: 'Uploading file...', css: {padding: '5px'}});
                                                },
                                                success: function(pdata) {
                                                    var data = {};
                                                    var ok_btn_text = 'Add to List';
                                                    var ok_btn_size = 'mid';
                                                    var btn_area_length = '220px';
                                                    try {
                                                        data = $.parseJSON(pdata);
                                                    } catch (e) {
                                                        data.html = pdata;
                                                        data.json = false;
                                                        ok_btn_text = 'Ok';
                                                        ok_btn_size = 'small';
                                                        btn_area_length = '200px';
                                                    }
                                                    //Unblocking it to show processed data
                                                    // $.unblockUI();

                                                    //Displaying the file contents
                                                    $.blockUI({
                                                        message: data.html + '<div class="block" style="clear: both;padding: 10px;margin: 0px auto;width: ' + btn_area_length + ';margin-bottom: 15px;background:none;"><form><input style="float:left;" type="button" id="data_ok" class="submit ' + ok_btn_size + '" value="' + ok_btn_text + '" /><input style="float:left;" type="button" id="data_cancel" class="submit small" value="Cancel" /></form></div>',
                                                        css: {
                                                            cursor: 'normal',
                                                            width: '960px',
                                                            padding: '15px',
                                                            overflow: 'auto',
                                                            maxHeight: '90%'
                                                        },
                                                        // disable horz centering 
                                                        centerX: true,
                                                        // disable vertical centering 
                                                        centerY: true
                                                    });

                                                    //When OK and CANCEL buttons are clicked
                                                    $('#data_ok, #data_cancel').click(function() {
                                                        $.unblockUI();
                                                        $('[name=ad_ad_ac_upload_csv_file]').val('');
                                                        return false;
                                                    });

                                                    //When OK button is clicked
                                                    //Setting the file form field to null
                                                    $('#data_ok').click(function(e) {

                                                        if (data.json !== false) {

                                                            //Retrieving all contacts 
                                                            //and their total
                                                            var all_contacts = ($('.ad_cl_add_contacts_manually').val()).split("\n");
                                                            var all_contacts_count = all_contacts.length;
                                                            $.each(data.json, function(i, cdata) {
                                                                if (($('.ad_cl_add_contacts_manually').val()).match(cdata[0]) === null && cdata[0] !== '' && cdata[0] !== undefined && cdata[0] !== null) {
                                                                    all_contacts[all_contacts_count] = cdata.join(',');
                                                                    all_contacts_count++;
                                                                }
                                                            });
                                                            $('.ad_cl_add_contacts_manually').val(all_contacts.join("\n"));

    <?php
    if (!$edit_campaign):
        ?>
                                                                new_cl_update_ui();

        <?php
    else:
        ?>

                                                                $.post('ad_ajax.php?action=ad_advb_cl_add_contacts', {ad_ad_ac_contacts: $('[name=ad_ad_ac_contacts]').val(), ad_contact_list_name: $('[name=ad_contact_list_name]').val()}, function(ajax_response) {

                                                                    //When ajax request completes
                                                                    //Alerting the user about its status
                                                                    alert(ajax_response);
                                                                    $.unblockUI();
                                                                    $.blockUI({message: '<h1 style="padding:25px 10px;">Retrieving the updated contact list</h1>'});

                                                                    //Firing ajax request to load modified contact list
                                                                    $.get('ad_ajax.php?action=ad_advb_cl_load_contacts&list_idx=<?php echo $edit_list_idx; ?>', function(ajax_response) {

                                                                        //Updating the section of webpage 
                                                                        //which is displaying the 
                                                                        //contacts of current list that is being edited
                                                                        $('.campaign_contact_list tbody').html(ajax_response);
                                                                        $('[name=ad_ad_ac_contacts]').val('');

                                                                        $.unblockUI();

                                                                    });

                                                                });

    <?php
    endif;
    ?>
                                                        }
                                                        e.preventDefault();
                                                    });

                                                    //WHen overlay is clicked
                                                    //Hiding the popup
                                                    $('.blockOverlay').click(function() {
                                                        $.unblockUI();
                                                    });

                                                    //Centering the popup on screen
                                                    $('.blockUI.blockMsg').center();
                                                    $('.blockUI.blockMsg').animate({scrollTop: 0}, 0);
                                                }
                                            });

                                            //Blocking the default action of preview button
                                            e.preventDefault();
                                        });
                                    });
                                </script>

                                <p>
                                    <input style="float:left;margin-top: 2px !important;" name="ad_ad_ac_add_contacts_1_by_1" value="true" type="checkbox" class="checkbox" id="cbdemo1" <?php echo isset($_POST['ad_ad_ac_add_contacts_1_by_1']) ? ' checked="checked" ' : ''; ?> /> <label class="left marginleft10 lh0" for="cbdemo1">Add Contacts 1 By 1</label>
                                </p>

                                <p class="ad_ad_ac_contacts" style="display: none;">
                                    <!--<label>Contacts (<a href="#" class="ad_cl_add_contacts_manually_btn">Click here</a> to Add New):</label>-->
                                    <textarea name="ad_ad_ac_contacts" class="ad_cl_add_contacts_manually" style="display: none;"><?php echo isset($_POST['ad_ad_ac_contacts']) ? $_POST['ad_ad_ac_contacts'] : '' ?></textarea>

                                    <br/>
                                    <?php
                                    //If this is a edit campaign page.
                                    //if ($edit_campaign):
                                    ?>
                                    <input style="float:left;" type="button" class="submit long <?php if (!$edit_campaign) { ?>ad_cl_add_contacts_manually_btn<?php } ?>" <?php if ($edit_campaign) { ?>id="ad_ad_add_contacts_to_list"<?php } ?> value="Add To List" />
                                    <?php
                                    //endif;
                                    ?>
            <!--<input style="float:left;" type="button" class="submit long" value="Clear List" onclick="$('[name=ad_ad_ac_contacts]').val('');" />-->
                                </p>

                                    <?php
                                    $smsoptin = false;
                                    if(@$edit_campaign_details['optin_keyword']!="" && @$edit_campaign_details['optin_number']!="") $smsoptin = true;
                                    ?>
                                <p><input style="float:left;" id="ad_ad_sms_optin" name="ad_ad_sms_optin" value="true" type="checkbox" class="checkbox"<?php  if(isset($_POST['ad_ad_sms_optin']) || $smsoptin){ echo ' checked="checked" '; }else{ echo '';} ?>/><label class="left marginleft10 lh0" for="ad_ad_sms_optin">SMS Opt-In</label></p>
                                <p>
                                    <table style="display: none; padding-left:50px;" id="ad_ad_sms_optin_box">
                                        <tr style="border: none;">
                                            <td style="border: none;"><label for="sms_keyword" style="display:inline-block !important;">Keyword: </label></td>
                                            <td style="border: none;"><input style="display:inline-block !important; width: 320px;" type="text" class="text" name="sms_keyword" autocomplete="off" id="sms_keyword" value="<?php echo @$sms_keyword; ?>"></td>
                                        </tr>
                                        <tr style="border: none;">
                                            <td style="border: none; vertical-align: top;"><label for="sms_response" style="display:inline-block !important;">Response: </label></td>
                                            <td style="border: none;">
                                                <textarea style="display:inline-block !important; width: 320px; height: 100px;" class="text" name="sms_response" id="sms_response"><?php echo @$sms_response; ?></textarea>
                                            </td>
                                        </tr>
                                        <tr style="border: none;">
                                            <td style="border: none;"><label for="sms_number" style="display:inline-block !important;">Select Phone Number: </label></td>
                                            <td style="border: none;"><select name="sms_number" id="sms_number" class="styled" style="width:320px;">
                                                <option value="">Select Number</option>
                                                <?php
                                                $numbers = $db->getCompanyNum($_SESSION['sel_co']);
                                                //echo "<pre>";print_r($numbers);
                                                for ($i = 0; $i <= count($numbers) - 1; $i++) {
                                                    $num = $numbers[$i];
                                                    ?>
                                                    <option <?php echo isset($edit_campaign_details['optin_number']) && $sms_number == $num ? ' selected="selected" ' : ''; ?> value="<?php echo $num; ?>"><?php echo $num; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select></td>
                                        </tr>
                                    </table>
                                </p>

                                <div id="opt_out_key_wrapper">
                                    <div style="float: left;">
                                        <label>VB Opt-out:</label>
                                        <br clear="all" />
                                        
                                        <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                            <input type="hidden" class="content" name="vb_opt_out_content" value="<?php echo $vb_opt_out_content; ?>" />
                                            <input type="hidden" class="type" name="vb_opt_out_type" value="<?php echo $vb_opt_out_type; ?>" />
                                            <input type="hidden" class="voice" name="vb_opt_out_voice" value="<?php echo $vb_opt_out_voice; ?>" />
                                            <input type="hidden" class="language" name="vb_opt_out_language" value="<?php echo $vb_opt_out_language; ?>" />
                                            <div class="ivr-Menu-selector" style="display: block">
                                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                                  <?php if ($vb_opt_out_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                                  <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($vb_opt_out_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                                </div>
                                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                                  <?php if ($vb_opt_out_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                                  <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($vb_opt_out_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                                </div>
                                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                                  <?php if ($vb_opt_out_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                                  <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($vb_opt_out_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                                </div>
                                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                                  <?php if ($vb_opt_out_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                                  <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($vb_opt_out_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                                </div>
                                              </div>
                                              <div class="ivr-Menu-editor ">
                                                <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                                  <div class="ivr-Menu-read-text" style="display: none;">
                                                    <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                    <br>
                                                    <div>
                                                      <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                                        <label class="field-label">
                                                            <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($vb_opt_out_type == 'Text')? $vb_opt_out_content:''); ?></textarea>

                                                            <?php
                                                            $voice = (($vb_opt_out_type == 'Text')? $vb_opt_out_voice:'');
                                                            $language = (($vb_opt_out_type == 'Text')? $vb_opt_out_language:'');
                                                            ?>
                                                            <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                            <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                              <?php
                                                              echo Util::getTwilioVoices($voice);
                                                              ?>
                                                            </select>

                                                            <br clear="all" />

                                                            <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                            <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                              <?php
                                                              echo Util::getTwilioLanguages($voice, $language);
                                                              ?>
                                                            </select>

                                                            <br clear="all" /><br />

                                                            <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                            <script type="text/javascript">
                                                              $(document).ready(function() {
                                                                $("#voice").trigger("change");
                                                              });
                                                            </script>

                                                            <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                                        </label>
                                                      </fieldset>
                                                    </div>
                                                    <br>
                                                    <br>
                                                  </div>
                                                  <div class="ivr-audio-upload" style="display: none;">
                                                    <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                    <div class="swfupload-container">
                                                      <div class="explanation"> <br>
                                                        
                                                        <span class="title" <?php if ( $vb_opt_out_type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                                        <?php 
                                                  echo (($vb_opt_out_type == 'Audio')? $vb_opt_out_content:''); ?>
                                                        </strong></span> <br>
                                                        
                                                        
                                                        <span class="title">Click to select a file: </span>
                                                        <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                                        <span class="title" id="statusUpload">&nbsp;</span>
                                                      </div>                                                   
                                                    
                                                    </div>
                                                  </div>
                                                  <div class="ivr-mp3-url" style="display: none;">
                                                    <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                    <div class="swfupload-container">
                                                      <div class="explanation"> <br>           
                                                        
                                                        <span class="title">
                                                          <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($vb_opt_out_type == 'MP3_URL')? $vb_opt_out_content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                                          <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                                        </span>

                                                        <br /><br />

                                                        <span class="title" <?php if ( $vb_opt_out_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                                        <?php 
                                                  echo (($vb_opt_out_type == 'MP3_URL')? $vb_opt_out_content:''); ?>
                                                        </strong></span>

                                                      </div>
                                                    
                                                    
                                                    </div>
                                                  </div>
                                                  <div class="ivr-record-audio" style="display: none;">
                                                    <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                                    <div class="swfupload-container">
                                                      <div class="explanation"> <br>           
                                                        
                                                        Caller ID:

                                                        <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                          <option value="">Select number</option>
                                                          <?php
                                                          $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                                          foreach ($numbers as $number) {
                                                            ?>
                                                              <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                            <?php
                                                          }
                                                          ?>
                                                        </select>

                                                        Your phone number:

                                                        <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                                        <br /><br />

                                                        <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                                        <br /><br />

                                                        <span class="title" <?php if ( $vb_opt_out_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                                            <?php if ($vb_opt_out_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($vb_opt_out_content, 'sm'); } ?>
                                                        </span>

                                                      </div>
                                                    
                                                    
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </fieldset>
                                    </div>

                                    <div style="float: left; padding-left: 10px;">
                                        <label>Key:</label><br />
                                        <select class="styled" name="vb_opt_out_key" style="width: 50px;">
                                            <option value="1" <?php if ($vb_opt_out_key == "1") { ?>selected="selected"<?php } ?>>1</option>
                                            <option value="2" <?php if ($vb_opt_out_key == "2") { ?>selected="selected"<?php } ?>>2</option>
                                            <option value="3" <?php if ($vb_opt_out_key == "3") { ?>selected="selected"<?php } ?>>3</option>
                                            <option value="4" <?php if ($vb_opt_out_key == "4") { ?>selected="selected"<?php } ?>>4</option>
                                            <option value="5" <?php if ($vb_opt_out_key == "5") { ?>selected="selected"<?php } ?>>5</option>
                                            <option value="6" <?php if ($vb_opt_out_key == "6") { ?>selected="selected"<?php } ?>>6</option>
                                            <option value="7" <?php if ($vb_opt_out_key == "7") { ?>selected="selected"<?php } ?>>7</option>
                                            <option value="8" <?php if ($vb_opt_out_key == "8") { ?>selected="selected"<?php } ?>>8</option>
                                            <option value="9" <?php if ($vb_opt_out_key == "9") { ?>selected="selected"<?php } ?>>9</option>
                                            <option value="0" <?php if ($vb_opt_out_key == "0") { ?>selected="selected"<?php } ?>>0</option>
                                            <option value="*" <?php if ($vb_opt_out_key == "*") { ?>selected="selected"<?php } ?>>*</option>
                                        </select>
                                    </div>
                                </div>

                                <br clear="all" /><br clear="all" />

                                <div id="opt_out_trigger_wrapper">
                                    <label style="display: inline-block;">SMS Opt-out Message:</label> <input type="text" class="text big" name="sms_opt_out_message" value="<?php echo isset($sms_opt_out_message) ? $sms_opt_out_message : ''; ?>" style="display: inline-block !important; width: 236px;" />

                                    <label style="display: inline-block;">Trigger:</label>
                                    <select class="styled" name="sms_opt_out_trigger" style="width: 150px; display: inline-block;">
                                        <option value="QUIT" <?php if ($sms_opt_out_trigger == "QUIT") { ?>selected="selected"<?php } ?>>QUIT</option>
                                        <option value="CANCEL" <?php if ($sms_opt_out_trigger == "CANCEL") { ?>selected="selected"<?php } ?>>CANCEL</option>
                                        <option value="UNSUBSCRIBE" <?php if ($sms_opt_out_trigger == "UNSUBSCRIBE") { ?>selected="selected"<?php } ?>>UNSUBSCRIBE</option>
                                    </select>
                                </div>
                                <br clear="all" />

                                <?php
                                if (!$edit_campaign):
                                    ?>
                                    <div>
                                        <h2>Contact List</h2>
                                        <table class="new_campaign_contact_list sortable" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Phone Number</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Email Address</th>
                                                    <!--<th>Date Added</th>
                                                    <th>Status</th>-->
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>No records found.</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <script type="text/javascript">
                                        if ($ === undefined) {
                                            $ = jQuery;
                                        }

                                        $(document).ready(function() {

                                            //Updating the Ui on page load
                                            new_cl_update_ui();

                                            $('body').delegate('.new_cl_edit_contact', 'click', function(e) {
                                                e.preventDefault();
                                                //Retrieving contact idx
                                                var ct_idx = $(this).attr('rel');

                                                //Retrieving all contacts 
                                                //and their total
                                                var all_contacts = ($('.ad_cl_add_contacts_manually').val()).split("\n");

                                                //Retrieving contact details
                                                var ct_details = all_contacts[ct_idx].split(',');

                                                var newPCTemplate = '\
                                                    <div style="width:340px;margin:2px;float:left;"><label style="display: inline-block !important;" for="name">Phone Number</label><font color="red"> *Required</font>\
                <input type="text" name="phone_number" id="ta_phone_number" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[0]) + '" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Business Name</label>\
                <input type="text" name="business_name" id="ta_business_name" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[1]) + '" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">First Name</label>\
                <input type="text" name="first_name" id="ta_first_name" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[2]) + '" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Last Name</label>\
                <input type="text" name="last_name" id="ta_last_name" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[3]) + '" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">Email Address</label>\
                <input type="text" name="email" id="ta_email" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[4]) + '" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Address</label>\
                <input type="text" name="address" id="ta_address" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[5]) + '" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">City</label>\
                <input type="text" name="city" id="ta_city" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[6]) + '" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">State</label>\
                <input type="text" name="state" id="ta_state" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[7]) + '" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">Zip</label>\
                <input type="text" name="zip" id="ta_zip" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[8]) + '" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Web Site</label>\
                <input type="text" name="website" id="ta_website" class="text ui-widget-content ui-corner-all" value="' + decodeURIComponent(ct_details[9]) + '" /></div>';
                                                var $dialog = $('<div></div>').html(newPCTemplate).dialog({
                                                    modal: true,
                                                    autoOpen: true,
                                                    minHeight: 177,
                                                    maxWidth: 720,
                                                    minWidth: 720,
                                                    height: 'auto',
                                                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\
                Add Contact',
                                                    buttons: [{
                                                            text: "Update",
                                                            click: function() {
                                                                if ($("#ta_phone_number").val() === "") {
                                                                    msgDialog("Please fill in the phone number.");
                                                                } else {

                                                                    //Updating contacts data
                                                                    var contact_data = [];
                                                                    contact_data[0] = encodeURIComponent($("#ta_phone_number").val());
                                                                    contact_data[1] = encodeURIComponent($("#ta_business_name").val());
                                                                    contact_data[2] = encodeURIComponent($("#ta_first_name").val());
                                                                    contact_data[3] = encodeURIComponent($("#ta_last_name").val());
                                                                    contact_data[4] = encodeURIComponent($("#ta_email").val());
                                                                    contact_data[5] = encodeURIComponent($("#ta_address").val());
                                                                    contact_data[6] = encodeURIComponent($("#ta_city").val());
                                                                    contact_data[7] = encodeURIComponent($("#ta_state").val());
                                                                    contact_data[8] = encodeURIComponent($("#ta_zip").val());
                                                                    contact_data[9] = encodeURIComponent($("#ta_website").val());
                                                                    contact_data[10] = '<?php echo Util::convertToLocalTZ(date("Y-m-d H:i:s"))->format(Util::STANDARD_LOG_DATE_FORMAT); ?>';
                                                                    contact_data[11] = 'Added Manually';
                                                                    all_contacts[ct_idx] = contact_data.join(',');
                                                                    $('.ad_cl_add_contacts_manually').val(all_contacts.join("\n"));

                                                                    //Updating the UI
                                                                    new_cl_update_ui();

                                                                    $($dialog).dialog("close");
                                                                    $($dialog).dialog('destroy').remove();

                                                                }
                                                            }
                                                        }, {
                                                            text: "Cancel",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                $(this).dialog('destroy').remove();
                                                            }
                                                        }]
                                                });
                                            });

                                            $('body').delegate('.new_cl_del_contact', 'click', function(e) {
                                                var ct_idx = $(this).attr('rel');

                                                //Retrieving all contacts 
                                                //and their total
                                                var all_contacts = ($('.ad_cl_add_contacts_manually').val()).split("\n");

                                                //Confirm from user about its action
                                                if (confirm('Are you sure? You are deleting a contact.')) {

                                                    //Deleting the clicked one
                                                    all_contacts.splice(ct_idx, 1);

                                                    //Updating the hidden textarea
                                                    $('.ad_cl_add_contacts_manually').val(all_contacts.join("\n"));

                                                    //Updating the UI
                                                    new_cl_update_ui();

                                                }

                                                //Prevent the broweser to perform default action of
                                                //button
                                                e.preventDefault();
                                            });

                                            //When text area is clicked
                                            $('.ad_cl_add_contacts_manually_btn').click(function(e) {


                                                var newPCTemplate = '\
                                                    <div style="width:340px;margin:2px;float:left;"><label style="display: inline-block !important;" for="name">Phone Number</label><font color="red"> *Required</font>\
                <input type="text" name="phone_number" id="ta_phone_number" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Business Name</label>\
                <input type="text" name="business_name" id="ta_business_name" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">First Name</label>\
                <input type="text" name="first_name" id="ta_first_name" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Last Name</label>\
                <input type="text" name="last_name" id="ta_last_name" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">Email Address</label>\
                <input type="text" name="email" id="ta_email" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Address</label>\
                <input type="text" name="address" id="ta_address" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">City</label>\
                <input type="text" name="city" id="ta_city" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">State</label>\
                <input type="text" name="state" id="ta_state" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">Zip</label>\
                <input type="text" name="zip" id="ta_zip" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Web Site</label>\
                <input type="text" name="website" id="ta_website" class="text ui-widget-content ui-corner-all" /></div>';
                                                var $dialog = $('<div></div>').html(newPCTemplate).dialog({
                                                    modal: true,
                                                    autoOpen: true,
                                                    minHeight: 177,
                                                    maxWidth: 720,
                                                    minWidth: 720,
                                                    height: 'auto',
                                                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\
                Add Contact',
                                                    buttons: [{
                                                            text: "Add",
                                                            click: function() {
                                                                if ($("#ta_phone_number").val() === "") {
                                                                    msgDialog("Please fill in the phone number.");
                                                                } else {

                                                                    //building & Appending 
                                                                    //the contact to textarea
                                                                    var contact_data = [];
                                                                    contact_data[0] = encodeURIComponent($("#ta_phone_number").val());
                                                                    contact_data[1] = encodeURIComponent($("#ta_business_name").val());
                                                                    contact_data[2] = encodeURIComponent($("#ta_first_name").val());
                                                                    contact_data[3] = encodeURIComponent($("#ta_last_name").val());
                                                                    contact_data[4] = encodeURIComponent($("#ta_email").val());
                                                                    contact_data[5] = encodeURIComponent($("#ta_address").val());
                                                                    contact_data[6] = encodeURIComponent($("#ta_city").val());
                                                                    contact_data[7] = encodeURIComponent($("#ta_state").val());
                                                                    contact_data[8] = encodeURIComponent($("#ta_zip").val());
                                                                    contact_data[9] = encodeURIComponent($("#ta_website").val());
                                                                    contact_data[10] = '<?php echo Util::convertToLocalTZ(date("Y-m-d H:i:s"))->format(Util::STANDARD_LOG_DATE_FORMAT); ?>';
                                                                    contact_data[11] = 'Added Manually';
                                                                    $('.ad_cl_add_contacts_manually').val($('.ad_cl_add_contacts_manually').val() + contact_data.join(',') + "\n");

                                                                    new_cl_update_ui();

                                                                    $($dialog).dialog("close");
                                                                    $($dialog).dialog('destroy').remove();

                                                                }
                                                            }
                                                        }, {
                                                            text: "Cancel",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                $(this).dialog('destroy').remove();
                                                            }
                                                        }]
                                                });

                                                e.preventDefault();
                                            });

                                        });
                                    </script>
                                    <?php
                                endif;
                                ?>

                                <?php
                                //If this is a edit campaign page.
                                if ($edit_campaign):
                                    ?>
                                    <div>
                                        <h2>Contact List</h2>
                                        <table class="campaign_contact_list sortable" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Phone Number</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Email Address</th>
                                                    <th>Date Added</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                //Intializing contacts count variable
                                                $total_contacts = 0;

                                                //Checking if this is a edit form
                                                if (isset($_GET['list_idx'])):

                                                    //Retriving unique ID of contact list 
                                                    //that is currently being edited
                                                    $list_idx = $_GET['list_idx'];

                                                    //Retrieving List contacts
                                                    $ad_contacts = ad_advb_cl_get_list_contacts($list_idx);

                                                    //Retrieving total count of list contacts
                                                    $total_contacts = count($ad_contacts);

                                                    if ($total_contacts > 0):

                                                        //Looping over contacts to display their data on screen
                                                        foreach ($ad_contacts as $single_contact_data):
                                                            $opted_out = $db->checkNumberOptedOut($list_idx, $single_contact_data['phone_number']);
                                                            if ($opted_out) {
                                                                $single_contact_data['opt_out'] = 1;
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><?php echo!s8_is_str_blank($single_contact_data['phone_number']) ? $single_contact_data['phone_number'] : ' - '; ?></td>
                                                                <td><?php echo!s8_is_str_blank($single_contact_data['first_name']) ? urldecode($single_contact_data['first_name']) : ' - '; ?></td>
                                                                <td><?php echo!s8_is_str_blank($single_contact_data['last_name']) ? urldecode($single_contact_data['last_name']) : ' - '; ?></td>
                                                                <td><?php echo!s8_is_str_blank($single_contact_data['email']) ? urldecode($single_contact_data['email']) : ' - '; ?></td>
                                                                <td><?php echo Util::convertToLocalTZ($single_contact_data['date_added'])->format(Util::STANDARD_LOG_DATE_FORMAT); ?></td>
                                                                <td><?php echo (($single_contact_data['opt_out'] == 0) ? $single_contact_data["add_type"] : "Opted-out"); ?></td>
                                                                <td>
                                                                    <a href="#" class="delete_contact" rel="<?php echo $single_contact_data['phone_number']; ?>">Delete</a> |
                                                                    <a href="ad_contactlist_edit.php?contact_idx=<?php echo $single_contact_data['idx']; ?>">Edit</a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        endforeach;
                                                    endif;

                                                endif;

                                                if ($total_contacts == 0):
                                                    ?>
                                                    <tr>
                                                        <td>No records found.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <?php
                                                endif;
                                                ?>
                                            </tbody>
                                        </table>
                                        <script type="text/javascript">
                                            if ($ == undefined) {
                                                $ = jQuery;
                                            }

                                            //When document is ready to perform any operations
                                            $(document).ready(function() {

                                                //When add to list button is clicked
                                                $('#ad_ad_add_contacts_to_list').click(function(e) {


                                                    var newPCTemplate = '\
                                                    <div style="width:340px;margin:2px;float:left;"><label style="display: inline-block !important;" for="name">Phone Number</label><font color="red"> *Required</font>\
                <input type="text" name="phone_number" id="ta_phone_number" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Business Name</label>\
                <input type="text" name="business_name" id="ta_business_name" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">First Name</label>\
                <input type="text" name="first_name" id="ta_first_name" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Last Name</label>\
                <input type="text" name="last_name" id="ta_last_name" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">Email Address</label>\
                <input type="text" name="email" id="ta_email" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Address</label>\
                <input type="text" name="address" id="ta_address" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">City</label>\
                <input type="text" name="city" id="ta_city" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">State</label>\
                <input type="text" name="state" id="ta_state" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:left;"><label style="display:inline-block !important;" for="outgoing">Zip</label>\
                <input type="text" name="zip" id="ta_zip" class="text ui-widget-content ui-corner-all" /></div>\
                <div style="width:340px;margin:2px;float:right;"><label style="display:inline-block !important;" for="outgoing">Web Site</label>\
                <input type="text" name="website" id="ta_website" class="text ui-widget-content ui-corner-all" /></div>';
                                                    var $dialog = $('<div></div>').html(newPCTemplate).dialog({
                                                        modal: true,
                                                        autoOpen: true,
                                                        minHeight: 177,
                                                        maxWidth: 720,
                                                        minWidth: 720,
                                                        height: 'auto',
                                                        title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\
                Add Contact',
                                                        buttons: [{
                                                                text: "Add",
                                                                click: function() {
                                                                    if ($("#ta_phone_number").val() === "") {
                                                                        msgDialog("Please fill in the phone number.");
                                                                    } else {

                                                                        //building & Appending 
                                                                        //the contact to textarea
                                                                        var contact_data = [];
                                                                        contact_data[0] = encodeURIComponent($("#ta_phone_number").val());
                                                                        contact_data[1] = encodeURIComponent($("#ta_business_name").val());
                                                                        contact_data[2] = encodeURIComponent($("#ta_first_name").val());
                                                                        contact_data[3] = encodeURIComponent($("#ta_last_name").val());
                                                                        contact_data[4] = encodeURIComponent($("#ta_email").val());
                                                                        contact_data[5] = encodeURIComponent($("#ta_address").val());
                                                                        contact_data[6] = encodeURIComponent($("#ta_city").val());
                                                                        contact_data[7] = encodeURIComponent($("#ta_state").val());
                                                                        contact_data[8] = encodeURIComponent($("#ta_zip").val());
                                                                        contact_data[9] = encodeURIComponent($("#ta_website").val());
                                                                        $('.ad_cl_add_contacts_manually').val($('.ad_cl_add_contacts_manually').val() + contact_data.join(',') + "\n");

                                                                        $.blockUI({message: '<h1 style="padding:25px 10px;">System is processing your request.</h1>'});

                                                                        $.post('ad_ajax.php?action=ad_advb_cl_add_contacts', {ad_ad_ac_contacts: $('[name=ad_ad_ac_contacts]').val(), ad_contact_list_name: $('[name=ad_contact_list_name]').val()}, function(ajax_response) {

                                                                            //When ajax request completes
                                                                            //Alerting the user about its status
                                                                            alert(ajax_response);
                                                                            $.unblockUI();
                                                                            $.blockUI({message: '<h1 style="padding:25px 10px;">Retrieving the updated contact list</h1>'});

                                                                            //Firing ajax request to load modified contact list
                                                                            $.get('ad_ajax.php?action=ad_advb_cl_load_contacts&list_idx=<?php echo $list_idx; ?>', function(ajax_response) {

                                                                                //Updating the section of webpage 
                                                                                //which is displaying the 
                                                                                //contacts of current list that is being edited
                                                                                $('.campaign_contact_list tbody').html(ajax_response);
                                                                                $('[name=ad_ad_ac_contacts]').val('');

                                                                                $.unblockUI();

                                                                            });

                                                                        });

                                                                        $($dialog).dialog("close");
                                                                        $($dialog).dialog('destroy').remove();

                                                                    }
                                                                }
                                                            }, {
                                                                text: "Cancel",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    $(this).dialog('destroy').remove();
                                                                }
                                                            }]
                                                    });

                                                    e.preventDefault();

                                                });

                                                //WHen delete contact button is pressed
                                                $('.delete_contact').live('click', function(e) {

                                                    //Confirm the user about its action
                                                    if (confirm('Are you sure? You are trying to delete a contact.')) {

                                                        //Retrieving the contact number 
                                                        //which is going to be deleted from clicked element
                                                        var contact_number = $(this).attr('rel');

                                                        //Firring the ajax request
                                                        //to perform the delete operation
                                                        $.get('ad_ajax.php?action=ad_advb_cl_delete_contact&contact_number=' + encodeURIComponent(contact_number) + '&list_idx=<?php echo $list_idx; ?>', function(ajax_response) {

                                                            //When ajax request completes
                                                            //Alerting the user about its status
                                                            alert(ajax_response);

                                                            //Firing ajax request to load modified contact list
                                                            $.get('ad_ajax.php?action=ad_advb_cl_load_contacts&list_idx=<?php echo $list_idx; ?>', function(ajax_response) {

                                                                //Updating the section of webpage 
                                                                //which is displaying the 
                                                                //contacts of current list that is being edited
                                                                $('.campaign_contact_list tbody').html(ajax_response);

                                                            });

                                                        });

                                                        //Deleting action 
                                                        //confirmation DIALOG condition ends here
                                                    }

                                                    //Preventing the default action of link
                                                    e.preventDefault();

                                                });

                                                //When delete button is clicked.
                                                $('.delete_cl').click(function(e) {

                                                    //Confirming from user that user really trying to delete 
                                                    //a contact list or mistakenly pressed this button.
                                                    if (confirm('Are you sure? You are trying to delete a contact list.')) {

                                                        //Firing ajax request to load modified contact list
                                                        $.get('ad_ajax.php?action=ad_advb_cl_delete_list&list_idx=<?php echo $list_idx; ?>', function(ajax_response) {

                                                            //Printing the response message.
                                                            alert(ajax_response);

                                                            //If delete operation is successsfull
                                                            if (ajax_response.match('SUCCESS') !== null) {

                                                                //Redirecting the user
                                                                window.document.location = 'ad_contactlist_log.php';

                                                                //Ending condition checking successfull 
                                                                //execution of delete operation via parsing 
                                                                //ajax response.
                                                            }

                                                            //Ending ajax handling jquery code
                                                        });

                                                        //Ending condition confirming user about delete action
                                                    }

                                                    //Preventing the default action of delete button
                                                    e.preventDefault();

                                                    //Ending jquery click event checker on delete CL button
                                                });

                                                //Ending jquery statement 
                                                //checking load of all htl section of current webpage.
                                            });
                                        </script>
                                    </div>

                                    <?php
                                endif;
                                ?>
                            <br>
                            <center>
                                <input style="display: inline-block !important;" type="submit" class="submit small" name="ad_ad_process_settings_form" value="Save" />
                                <input style="display: inline-block !important;" type='button' class="submit small" value="Cancel" onclick="window.document.location = 'ad_contactlist_log.php'" />
                                <?php
                                if ($edit_campaign):
                                    ?>
                                    <input style="float:left;" type="button" class="delete_cl submit small" value="Delete" />
                                    <?php
                                endif;
                                ?>
                            </center>
                            <br/>
                            </form>
                            <?php
//Closing IF statement checking success response
                        endif;
                        ?>

                    </div>
                    <!-- .block_content ends -->

                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Auto dialer campaigns listing ends here-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                setInterval("formatNumbers()",1000);
            });
            function formatNumbers(){
                var numbers = $(".campaign_contact_list tr td:first-child:not('.formatted-number')");
                if(numbers.length > 0){
                    $(numbers).each(function(i,v){
                        var formatted = "";
                        var number = $(v).text().trim();
                        var country = countryForE164Number("+"+number);

                        if (number.length == 10)
                            country = "";

                        if (number != "" && number != "No records found.") {
                            if(country!="" && country!="FR"){
                                formatted = formatLocal(country,"+" + number);
                            }else{
                                if(number[0]=="1")
                                    formatted = formatLocal("US",number);
                                else
                                    formatted = formatLocal("US", "+1" + number);
                            }

                            if(formatted!="" && typeof formatted != 'undefined')
                                $(v).text(formatted).addClass("formatted-number");
                        }
                    });

                }
            }

            function showAudioText(obj)
            {
                
                
                var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
                audioChoice.hide();
                audioChoice.parent().children('.ivr-Menu-editor').show();
                var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');
                
                if ( obj.id  == 'txt' ) 
                {       
                    
                    subDiv.children('.ivr-Menu-read-text').show();
                    
                    //////////////// only to avoid  file button clickable in text area      
                    $('[name="uploadfile"]').css('z-index','-1');       
                
                }
                else if ( obj.id  == 'upload_mp3' ) {
                    $('[name="uploadfile"]').css('z-index','2147483583');
                    
                    subDiv.children('.ivr-audio-upload').show();    
                    SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');        
                    UploadFile(SubObj); 
                }
                else if ( obj.id  == 'mp3_url' ) {
                    subDiv.children('.ivr-mp3-url').show();
                }
                else if ( obj.id  == 'record_audio' ) {
                    subDiv.children('.ivr-record-audio').show();
                }
                
            }

            function CloseButton(obj)
            {
                var audioChoice = $(obj).closest('.ivr-Menu');
                
                var audioChoiceEditor   = audioChoice.children('.ivr-Menu-editor');
                var audioChoiceSelector = audioChoice.children('.ivr-Menu-selector');
                
                var subDiv  = audioChoiceEditor.children('.ivr-Menu-editor-padding');
                
                audioChoiceSelector.show();
                audioChoiceEditor.hide();
                subDiv.children('.ivr-audio-upload').hide();    
                subDiv.children('.ivr-Menu-read-text').hide();
                subDiv.children('.ivr-mp3-url').hide();
                subDiv.children('.ivr-record-audio').hide();
            }

            function testVoice(voice, language, text) {
                if ($("#test_voice_iframe_wrapper").length == 0) {
                    $("body").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
                                        '<form id="test_voice_form" action="test_voice.php" method="post" target="test_voice_iframe">' +
                                            '<input type="hidden" name="voice" id="voice" />' +
                                            '<input type="hidden" name="language" id="language" />' +
                                            '<input type="hidden" name="text" id="text" />' +
                                            '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                        '</form>' +
                                        '<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
                                    '</form>'
                    );
                }

                $("#test_voice_iframe_wrapper #voice").val(voice);
                $("#test_voice_iframe_wrapper #language").val(language);
                $("#test_voice_iframe_wrapper #text").val(text);
                $('#test_voice_iframe_wrapper #submit').click()
             }
             
             var closeBtnHtml = '<a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a>';

             function SaveContent(obj, content_type) {
                var thisBlock = $(obj).closest('.ivr-Menu');
                switch (content_type) {
                    case "Text_mail":
                    {
                        var voice_text = $(thisBlock).find('#readtxt_mail').val();
                        var voice = $(thisBlock).find('#voice').val();
                        var language = $(thisBlock).find('#language').val();
                        $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find('.content').val(voice_text);
                        $(thisBlock).find('.type').val("Text");
                        $(thisBlock).find('.voice').val(voice);
                        $(thisBlock).find('.language').val(language);

                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                        $(thisBlock).find('#txt').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        break;
                    }
                    case "MP3_URL":
                    {

                        var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                        $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find('.content').val(mp3_url);
                        $(thisBlock).find('.type').val("MP3_URL");

                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                        $(thisBlock).find('#mp3_url').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        break;
                    }
                }
            }

            function removeSelectedOption(obj) {
                promptMsg('Are you sure ?', function() {
                  var thisBlock = $(obj).closest('.ivr-Menu');
                  $(thisBlock).find(".ttsMwCloseBtn").remove();

                  $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                  $(thisBlock).find('.content').val('');
                  $(thisBlock).find('.type').val('');
                  $(thisBlock).find('.voice').val('');
                  $(thisBlock).find('.language').val('');
                });
            }

            function UploadFile(obj)
            {
                var status = $(obj).closest('.explanation').find('#statusUpload');
                var fileNameStatus = $(obj).closest('.explanation').find('#voicefilename');
                var fileNameStatusWrapper = $(obj).closest('.explanation').find('#voicefilenameWrapper');
                
                new AjaxUpload(obj, {
                    
                    
                    action: 'ad_contactlist_add.php?act=uploadMp3',
                    name: 'uploadfile',
                    onSubmit: function(file, ext){
                         if (! (ext && /^(mp3|wma)$/.test(ext))){ 
                            // extension is not allowed 
                            status.text('Only MP3 files are allowed');
                            return false;
                        }
                        (fileNameStatus).html('Uploading...');
                    },
                    onComplete: function(file, response){
                        //On completion clear the status
                        
                        //Add uploaded file to list
                        
                        if(response!="error"){
                            $(fileNameStatusWrapper).css('display' ,'block');
                            $(fileNameStatus).html(response);
                            var thisBlock = $(obj).closest('.ivr-Menu');

                            $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                            $(thisBlock).find('#upload_mp3').addClass('ivr-Menu-Selected');
                            $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                            $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                            $(thisBlock).find('.content').val(response);
                            $(thisBlock).find('.type').val("Audio");
                            $(thisBlock).find('.ivr-Menu-close-button').click();

                            $(thisBlock).find(".ttsMwCloseBtn").remove();
                            $(thisBlock).find('#upload_mp3').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        } else{
                        }
                    }
                });
                    
            }

            var recordingId = "";
            function recordAudio(obj) {
                if ($(obj).val() == "Call Me") {
                    recordingId = createUUID();
                    var record_from = $(obj).parents('.ivr-Menu').find('#record_from').val();
                    var record_to = $(obj).parents('.ivr-Menu').find('#record_to').val();

                    if (record_from == "") {
                        errMsgDialog("Please select Caller ID.");
                        return false;
                    }

                    if (record_to == "") {
                        errMsgDialog("Please enter your phone number.")
                        return false;
                    }

                    $(obj).val('Stop');

                    if ($("#record_audio_iframe_wrapper").length == 0) {
                        $("body").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
                                            '<form id="record_audio_form" action="record_audio.php" method="post" target="record_audio_iframe">' +
                                                '<input type="hidden" name="recordingId" id="recordingId" />' +
                                                '<input type="hidden" name="record_from" id="record_from" />' +
                                                '<input type="hidden" name="record_to" id="record_to" />' +
                                                '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                            '</form>' +
                                            '<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
                                        '</form>'
                        );
                    }

                    $("#record_audio_iframe_wrapper #recordingId").val(recordingId);
                    $("#record_audio_iframe_wrapper #record_from").val(record_from);
                    $("#record_audio_iframe_wrapper #record_to").val(record_to);
                    $('#record_audio_iframe_wrapper #submit').click()
                }
                else {
                    $(obj).val('Please wait...');
                    $("#record_audio_iframe").contents().find("#disconnectBtn").click();

                    setTimeout(function() {
                        $.post("admin_ajax_handle.php", { func: "GET_RECORDING", recordingId: recordingId },  function(response) {
                            var thisBlock = $(obj).closest('.ivr-Menu');

                            $(thisBlock).find('#recordedAudioSavedWrapper').css('display' ,'block');
                            if (response) {
                                $(thisBlock).find('#recordedAudioSavedWrapper').html(response.playable);
                                
                                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                                $(thisBlock).find('.content').val(response.url);
                                $(thisBlock).find('.type').val("RECORD_AUDIO");
                                $(thisBlock).find('.ivr-Menu-close-button').click();

                                $(thisBlock).find(".ttsMwCloseBtn").remove();
                                $(thisBlock).find('#record_audio').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                            }
                            else {
                                errMsgDialog('Audio was not recorded, please try again.');
                                $(thisBlock).find('#recordedAudioSavedWrapper').html('');
                            }
                            $(obj).val('Call Me');
                        }, "json");
                    }, 2000);
                }
             }

            function createUUID() {
                // http://www.ietf.org/rfc/rfc4122.txt
                var s = [];
                var hexDigits = "0123456789abcdef";
                for (var i = 0; i < 36; i++) {
                    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
                }
                s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
                s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
                s[8] = s[13] = s[18] = s[23] = "-";

                var uuid = s.join("");
                return uuid;
            }
        </script>
    </body>
</html>