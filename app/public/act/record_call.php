<?php


require_once('include/config.php');
require_once('include/util.php');
require_once('include/db.php');
require_once('include/Services/Twilio.php');
require_once('include/Services/km.php');
require_once('include/twilio_header.php');

$db = new DB();

global $AccountSid, $AuthToken;
$client = new Services_Twilio($AccountSid, $AuthToken);

$_REQUEST['From'] = str_replace(" ","+",urldecode($_REQUEST['From']));
$_REQUEST['To']   = str_replace(" ","+",urldecode($_REQUEST['To']));
$_REQUEST['DialCallTo'] = str_replace(" ","+",urldecode($_REQUEST['DialCallTo']));


if ( @$_REQUEST['exit_from_conference'] =='1') {
    $this_user_is_agent = false;
    $new_participants = array();
    $conference_name = trim($_REQUEST['conference_name']);
    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array($conference_name));
    $conference = $sth->fetch(PDO::FETCH_OBJ);

    $conference->participants = json_decode($conference->participants, true);

    $host_is_removed = false;
    $host = array();

	if(is_array($conference->participants) || is_object($conference->participants)){
		foreach ($conference->participants as $key => $participant) {
			if ($participant['call_sid'] == $_REQUEST['CallSid'] && $participant['type'] == "agent") {
				$this_user_is_agent = true;
			}
			if ($participant['call_sid'] == $_REQUEST['CallSid'] && ((@$participant['on_hold'] != 1 && @$participant['forced_hold'] != 1) || isset($_REQUEST['ignore_hold'])) && @$participant['bringing_them_back'] != 1) {
				unset($conference->participants[$key]);

				if ($participant['is_host']) {
					$host_is_removed = true;
					$host = $participant;
				}
			}
			else {
				$new_participants[] = $participant;
			}
		}
	}

    $new_host_found = false;
    if ($host_is_removed) {
        foreach ($new_participants as $key => $participant) {
            if ($participant['type'] == "agent") {
                $new_participants[$key]['is_host'] = true;
                $new_host_found = true;
                break;
            }
        }
    }

    $remove_all = false;

    if (count($new_participants) <= 1) $remove_all = true;
    if ($host_is_removed && !$new_host_found) $remove_all = true;

    if ($remove_all) {
        try {
            foreach ($new_participants as $key => $participant) {
                $call = $client->account->calls->get($participant['call_sid']);

                $call->update(array(
                    "Url" => dirname(s8_get_current_webpage_uri()) . "/ivrmenu.php?company_id=".$_REQUEST['company_id']."&action=remove_from_call",
                    "Method" => "POST"
                ));
            }
        }
        catch (Exception $e) {}

        $sth = $db->customExecute("DELETE FROM call_conferences WHERE id = ? AND moved_to_conference = ''");
        $sth->execute(array($conference->id));

        $sth = $db->customExecute("DELETE FROM call_conferences WHERE moved_to_conference = ?");
        $sth->execute(array($conference->name));
    }
    else {
        $sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
        $sth->execute(array(json_encode($new_participants), $conference->id));
    }

    if ($this_user_is_agent) exit();
}

if ( @$_REQUEST['voicemail'] =='1' &&
    ( $_REQUEST['DialCallStatus'] == 'busy' || $_REQUEST['DialCallStatus'] == 'no-answer'  || $_REQUEST['DialCallStatus']=='failed'))
{
    $company_id = $_REQUEST['company_id'];
    $savedcall = $db->save_dialed_call();
    header("location: leave_a_message.php?exten=$company_id");
    exit;
}

if (substr($_REQUEST['To'], 0, 2) == "+1")
    $company_id = $db->getCompanyOfNumber(substr($_REQUEST['To'], 2, strlen($_REQUEST['To']) - 1));
else
    $company_id = $db->getCompanyOfNumber(substr($_REQUEST['To'], 1, strlen($_REQUEST['To']) - 1));

$main_call_sid = $_REQUEST['DialCallSid'];

if(@$_REQUEST['test']=="c2")
{
    $_REQUEST['DialCallSid'] = $_REQUEST['CallSid'];
    if (!empty($_REQUEST['RecordingDuration'])) {
        $_REQUEST['DialCallDuration'] = @$_REQUEST['RecordingDuration'];
        $_REQUEST['DialCallStatus'] = "completed";
    }
}

if ($_REQUEST['CallStatus'] == "completed") {
    $stmt = $db->customExecute("SELECT CallSid FROM calls WHERE OriginalCallSid = ?");
    $stmt->execute(array($_REQUEST['CallSid']));
    $res = $stmt->fetch();

    if (!empty($res['CallSid'])) {
        try {
            $call = $client->account->calls->get($res['CallSid']);

            $call->update(array(
                "Status" => "completed"
            ));
        }
        catch (Exception $e) {}
    }

    if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id)) {

    }
    else {
        if (!empty($_REQUEST['RecordingDuration'])) {
            $_REQUEST['DialCallStatus'] = "completed";
        }
        else {
            $_REQUEST['DialCallStatus'] = "no-answer";
        }
    }
}

if (empty($_REQUEST['DialCallStatus'])) {
    $_REQUEST['DialCallStatus'] = $_REQUEST['CallStatus'];
}

if(@$_REQUEST['RecordingUrl']=="" && 0)
{
    $rec_uri = "";
    foreach($client->account->recordings->getIterator(0, 50, array('CallSid' => $_REQUEST['CallSid'])) as $recording) {
        $rec_uri = $recording->uri;
    }
    $_REQUEST['RecordingUrl']     = "http://api.twilio.com".$rec_uri;
}

if(($_REQUEST['DialCallTo']=="" || $_REQUEST['DialCallTo']=="+") && @$_REQUEST['test']!="c3")
{
    $call = $client->account->calls->get($main_call_sid);
    $_REQUEST['DialCallTo'] = $call->to;
}
if(@$_REQUEST['test']=="c3")
    $db->save_call();

$savedcall = $db->save_dialed_call();

smsin();

if($company_id!=FALSE){

    //
    // SMS Delayed Send
    //
    

    //
    // Zapier
    //
    $stmt = $db->customExecute("SELECT * FROM zapier_subscriptions WHERE company_id = ?");
    $stmt->execute(array($company_id));
    $subs = $stmt->fetchAll(PDO::FETCH_OBJ);
    if(is_array($subs)||is_object($subs))
        foreach($subs as $sub){
            $check1 = false;
            $check2 = false;
            $company_check = false;
            if(!$db->isUserAdmin($sub->user_id)){
                $access_num = $db->getUserAccessNumbers($sub->user_id);
                if(count($access_num)==0)
                    $check1 = true;
                else{
                    foreach($access_num as $num){
                        if($num->number == $savedcall->CallTo)
                            $check1 = true;
                    }
                }
                $outgoing_access = $db->getUserOutgoingAccessNumbers($sub->user_id);
                if(count($outgoing_access)==0)
                    $check2 = true;
                else{
                    foreach($outgoing_access as $num){
                        if($num->number == $savedcall->DialCallTo)
                            $check2 = true;
                    }
                }
                $company_check = $db->isUserInCompany($company_id, $sub->user_id);
            }else{
                $check1 = true;
                $check2 = true;
                $company_check = true;
            }

            if($check1 == true && $check2 == true && $company_check == true){
                $_obj = new stdClass();
                $_obj->CallSid = $savedcall->CallSid;
                $_date = new DateTime(date('Y-m-d H:i:s',time()),new DateTimeZone("UTC"));
                $_date->setTimezone(new DateTimeZone($TIMEZONE));
                $_obj->DateCreated = $_date->format("D n\/d Y g\:iA T");
                $_obj->CallerID = $savedcall->CallerID;
                $_obj->Country = $savedcall->FromCountry;
                $_obj->State = $savedcall->FromState;
                $_obj->Zip = $savedcall->FromZip;
                $_obj->City = $savedcall->FromCity;
                $_obj->Status = ucfirst($savedcall->DialCallStatus);
                $_obj->Duration = $savedcall->DialCallDuration != null ? $savedcall->DialCallDuration : "0";
                $_obj->From = $savedcall->CallFrom;
                $_obj->To = $savedcall->DialCallTo;
                $_obj->TrackingNumber = $savedcall->CallTo;
                $_obj->Recording = $savedcall->RecordingUrl;

                $data_string = json_encode($_obj);
                $ch = curl_init($sub->subscription_url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );

                $result = curl_exec($ch);
                $responseInfo = curl_getinfo($ch);
                if($responseInfo['http_code']!=200){
                    $stmt = $db->customExecute("DELETE FROM zapier_subscriptions WHERE id = ?");
                    $stmt->execute(array($sub->id));
                }
            }
        }

    //
    // Google Analytics and Kissmetrics
    //
    $ga_id      = $db->getVar($company_id."[ga_id]");
    $ga_domain  = $db->getVar($company_id."[ga_domain]");
    $km_api_key = $db->getVar($company_id."[kiss_metrics_api_key]");

    if($km_api_key != "" || $km_api_key != false){
        $kmid = $db->getKmIdByCallSid($_REQUEST['CallSid']);
        KM::init($km_api_key);
        KM::identify($kmid);
        $CallerID = "";

        if($savedcall->CallerID != "")
            $CallerID = $savedcall->CallerID;

        $args = array(
            'Customer ID'=>$_REQUEST['From'],
            'Customer Phone Number'=>$_REQUEST['From'],
            'Tracking Number'=>$_REQUEST['To'],
            'Rang Number'=>$_REQUEST['DialCallTo'],
            'Status'=>$_REQUEST['DialCallStatus']
        );

        if ($db->getVar("mask_recordings") == "true") {
            $_REQUEST['RecordingUrl'] = Util::maskRecordingURL($_REQUEST['RecordingUrl']);
        }

        if($_REQUEST['RecordingUrl']!="")
            $args['Call Recording'] = $_REQUEST['RecordingUrl'];
        if($_REQUEST['DialCallStatus']=="completed")
            $args['Call Duration'] = $_REQUEST['DialCallDuration'];
        if($_REQUEST['FromState']!="")
            $args['State'] = $_REQUEST['FromState'];
        if($_REQUEST['FromCity']!="")
            $args['City'] = $_REQUEST['FromCity'];
        if($_REQUEST['FromCountry']!="")
            $args['Country'] = $_REQUEST['FromCountry'];
        if($_REQUEST['FromZip']!="")
            $args['FromZip'] = $_REQUEST['FromZip'];
        if($CallerID!="")
            $args['Caller ID'] = $CallerID;
        $stmt = $db->customExecute("SELECT * FROM calls WHERE CallFrom = ?");
        $stmt->execute(array($_REQUEST['From']));
        $res = $stmt->rowCount();
        if($res>1)
            $args['Repeat Caller'] = 'yes';
        else
            $args['Repeat Caller'] = 'no';
        KM::record("Completed a Phone Call",$args);
    }

    if($ga_id!="" || $ga_id!=false || $ga_domain!="" || $ga_domain!=false)
    {
        $spid = $db->getSpIdByCallSid($_REQUEST['CallSid']);
        //error_log("SpId: ".$spid);
        $data = array(
            'v'=>'1',
            'tid'=>$ga_id,
            'cid'=>$_REQUEST['CallSid'],
            't'=>'event',
            'ec'=>'Calls',
            'el'=>$_REQUEST['To'].", ".$_REQUEST['DialCallTo'].", ".$_REQUEST['DialCallStatus'],
            'ev'=>@$_REQUEST['DialCallDuration']
        );

        if($spid!=NULL || $spid!=""){
            $stmt = $db->customExecute("SELECT * FROM keyword_detail WHERE sp_id = ?");
            $stmt->execute(array($spid));
            $res = $stmt->fetch(PDO::FETCH_OBJ);

            $data['dr'] = $res->referrer;
            $data['cn'] = $res->campaign;
            $data['cs'] = $res->source;
            $data['cm'] = $res->medium;
            $data['ck'] = $res->keywords;
            $data['cc'] = $res->content;
            $data['dl'] = $res->url;

            if($res->glcid != "")
                $data['gclid'] = $res->glcid;

            if($res->medium!=""||$res->medium!="-"){
                if($res->source == "(direct)")
                    $data['ea'] = "Direct";
                else
                    $data['ea']=ucfirst(str_replace(")","",str_replace("(","",$res->sourceh)))." ".ucfirst(str_replace(")","",str_replace("(","",$res->medium)));
            }else
                $data['ea']="Direct";

            if($res->campaign != "" || $res->campaign !="-")
                $data['el'] = $res->campaign.", ".$data['el'];
            else
                $data['el'] = "(not set), ".$data['el'];

            $stmt = $db->customExecute("SELECT * FROM calls WHERE CallFrom = ?");
            $stmt->execute(array($_REQUEST['From']));
            $res = $stmt->rowCount();
            if($res > 1)
                $data['el'] = $data['el'].", repeat-call";
            else
                $data['el'] = $data['el'].", first-call";
        }

        //error_log(print_r($data,true));

        $db->gaFireHit($data);
    }
}

//$file=fopen("callback_log.txt","a+");
//fwrite($file, "[RECV CALL DETAILS]\n");
//fwrite($file,print_r($_REQUEST, true));
//fwrite($file, "[/RECV CALL DETAILS]\n\n");
//fclose($file);

header('Content-type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8"?><Response>';
$company_id = @$_REQUEST['company_id'];
if( isset($_REQUEST['rr'] ) ) /// Round Robin
{

    if($_REQUEST['nextId'] != '' )
    {
        $wId = $_REQUEST['nextId'];
        $company_id = $_REQUEST['company_id'];
        echo '<Redirect>ivrmenu.php?company_id='.$company_id.'&amp;wId='.$wId.'</Redirect>';
    }


}elseif( isset($_REQUEST['f_number'] ) )
{

    if (  $_REQUEST['DialCallStatus'] == "busy"  || $_REQUEST['DialCallStatus'] =="no-answer" || $_REQUEST['DialCallStatus'] =="failed")
    {
        $record= $_REQUEST['rc'];
        $timeout= $_REQUEST['to'];
        $number= $_REQUEST['f_number'];

        echo '<Dial action="record_call.php?DialCallTo='.urlencode($number).'"  timeout="'.$timeout.'" record="'.$record.'">'.$number.'</Dial>';


    }



}elseif ( isset($_REQUEST['wId'])){

    $wId = $_REQUEST['wId'];
    $company_id = $_REQUEST['company_id'];
    echo '<Redirect>ivrmenu.php?company_id='.$company_id.'&amp;wId='.$wId.'</Redirect>';
}

echo '</Response>';






function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}
?>
