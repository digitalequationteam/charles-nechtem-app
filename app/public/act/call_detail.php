<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$db = new DB();
global $RECORDINGS, $TIMEZONE, $title;
$call_details = $db->getCallDetails($_GET['id']);
$call_notes = $db->getCallNotes($_GET['id']);

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}

if($call_details==FALSE)
{
    ?>
<script type="text/javascript">
    alert('Call not found!');
    window.location = "index.php";
</script>
<?php
    die();
}

if($_SESSION['permission'] == 0)
{
    if(!$db->isUserInCompany($db->getCompanyofCall($call_details['CallSid']),$_SESSION['user_id']))
    {
        ?>
    <script type="text/javascript">
        alert('You are not in that company!');
        window.location = "index.php";
    </script>
    <?php
        die();
    }
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        #note_container ul li
        {
            margin-bottom: 10px;
        }
        #USR_ADD_NOTE {
            width: 165px;
            background: url(images/btnb.gif) top center no-repeat;
            height: 30px;
            line-height: 30px;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            text-transform: uppercase;
            color: white;
            text-shadow: 1px 1px 0 #0A5482;
            cursor: pointer;
            margin-right: 10px;
            vertical-align: middle;
        }
        .big-font {
            font-family: "Titillium999", "Trebuchet MS", Arial, sans-serif;
            font-size: 15px;
            font-weight: normal;
            text-transform: uppercase;
            color: #555;
            text-shadow: 1px 1px 0 white;
        }
    </style>

</head>





<body>

<div id="hld">

<div class="wrapper">		<!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->

    <?php
    $tz = new DateTimeZone($TIMEZONE);
    $d = new DateTime($call_details['DateCreated']);
    $d->setTimezone($tz);
    ?>

    <input type="hidden" id="CallSid" value="<?php echo $call_details['CallSid']; ?>" />

<div class="block" style="width: 960px;margin: 0 auto 25px;">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>
            <?php  if($call_details['DialCallStatus']=='voicemail') echo "Voicemail"; else echo "Incoming call"; ?> on <?php echo $d->format("D n\/j Y \a\\t g\:iA T");?>
        </h2>

    </div>		<!-- .block_head ends -->

    <div class="block_content" style="padding:0;">

        <table style="width:960px !important; margin:0 auto;">
            <thead>
            <tr>
                <th colspan="1"><div style="text-align: center;"><span class="big-font">Company:</span> <span style="font-weight: normal;"><?php echo $db->getCompanyName($db->getCompanyofCall($call_details['CallSid'])); ?></span></div></th>
                <th style="text-align: center;" class="big-font">Location</th>
            </tr>
            </thead>
            <tr>
                <td style="width: 53%;">
                    <table id="nested-tbl" width="100%">
                        <tbody>
                        <?php
                        $twilio_numbers=Util::get_all_twilio_numbers();
                        if (array_key_exists(format_phone($call_details['CallTo']), $twilio_numbers))
                            $campaign=$twilio_numbers[format_phone($call_details['CallTo'])];
                        else
                            $campaign="";

                        if($call_details['DialCallStatus']=='voicemail'){
                            $stmt = $db->customExecute("SELECT * FROM messages WHERE CallSid = ?");
                            $stmt->execute(array($call_details['CallSid']));
                            $message = $stmt->fetch(PDO::FETCH_OBJ);
                            $call_details['RecordingUrl'] = $message->message_audio_url;

                            //$stmt = $db->customExecute("UPDATE messages SET message_flag = 0 WHERE message_id = ?");
                            //$stmt->execute(array($message->message_id));
                        }

                        if ($db->getVar("mask_recordings") == "true") {
                            $call_details['RecordingUrl'] = Util::maskRecordingURL($call_details['RecordingUrl']);
                        }
                        ?>
                        <tr>
                            <td class="big-font">Campaign:</td><td style="width: 250px;"><?php echo $campaign == "callTracking"? "Pooled Call" : $campaign; ?></td>
                        </tr>
                        <tr>
                            <td class="big-font">To:</td><td><?php echo format_phone($db->format_phone_db($call_details['CallTo'])); ?></td>
                        </tr>
                        <tr>
                            <td class="big-font">From:</td><td<?php if($db->isNumberBlocked($db->getCompanyofCall($call_details['CallSid']),$call_details['CallFrom'])) echo " style=\"text-decoration: line-through;\" title=\"This number has been blacklisted.\""; ?>><?php echo format_phone($db->format_phone_db($call_details['CallFrom'])); ?></td>
                        </tr>
                        <?php if($call_details['CallerID']!=""){ ?>
                        <tr>
                            <td class="big-font">CallerID:</td><td><?php echo $call_details['CallerID']; ?></td>
                        </tr>
                        <?php } ?>
                        <tr style="<?php if($call_details['DialCallStatus']=="voicemail") echo "display: none"; ?>">
                            <td class="big-font">Rang:</td><td><?php echo format_phone($db->format_phone_db($call_details['DialCallTo'])); ?></td>
                        </tr>
                        <?php if ($call_details['participants'] != "") { ?>
                        <tr>
                            <td class="big-font">Participants:</td><td><?php echo implode(", ", json_decode($call_details['participants'], true)); ?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td class="big-font">Status:</td><td class="message_flag_<?php echo @$message->message_id; ?>"><?php if($call_details['DialCallStatus'] != 'voicemail'){ echo ucwords($call_details['DialCallStatus']);}else{ echo $message->message_flag == 0 ? "New":"Read"; }?></td>
                        </tr>
                        <?php
                        $total_duration = (int)($call_details['DialCallStatus']=='voicemail' ? $message->RecordingDuration : $call_details['DialCallDuration']);
                        $otherRecordings = $db->getCallRecordings($call_details['CallSid']);
                        if ($otherRecordings) {
                            foreach ($otherRecordings as $otherRecording) {
                                $total_duration += $otherRecording['DialCallDuration'];
                            }
                        }
                        ?>
                        <tr>
                            <td class="big-font">Duration:</td><td><?php echo Util::formatTime($total_duration); ?></td>
                        </tr>
                        <tr style="<?php if($call_details['DialCallStatus']=="voicemail") echo "display: none"; ?>">
                            <td class="big-font">Phone Code:</td>
                            <td>
                                <?php if($_SESSION['permission'] > 0 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])) { ?>
                                <select id="phone_code_select" class="styled" onchange="$(this).attr('disabled','true'); changePhoneCode('<?php echo $call_details['CallSid']; ?>',this.options[this.selectedIndex].value,(function(){$('#phone_code_select').removeAttr('disabled');}))">
                                    <?php
                                    $phone_codes = $db->getPhoneCodes($db->getCompanyofCall($call_details['CallSid']));
                                    if($call_details['PhoneCode']==0)
                                        echo "<option value='0' selected>None</option>";
                                    else
                                        echo "<option value='0'>None</option>";
                                    if(is_array($phone_codes) || is_object($phone_codes))
                                        foreach($phone_codes as $code)
                                        {
                                            if($call_details['PhoneCode']==$code->idx)
                                                echo "<option value='$code->idx' selected>$code->name</option>";
                                            else
                                                echo "<option value='$code->idx'>$code->name</option>";
                                        }
                                    ?>
                                </select>
                                <?php
                                    }else{
                                        if($call_details['PhoneCode']==0)
                                            echo "None";
                                        else{
                                            $phone_codes = $db->getPhoneCodes($db->getCompanyofCall($call_details['CallSid']));
                                            foreach($phone_codes as $phone_code){
                                                if($call_details['PhoneCode']==$phone_code->idx)
                                                    echo $phone_code->name;
                                            }
                                        }
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php if($RECORDINGS && $db->isCompanyRecordingDisabled($db->getCompanyofCall($call_details['CallSid'])) == false) {
                        if($call_details['RecordingUrl']=="")
                        {
                            $send_link = '<a href="#" onmousedown="errMsgDialog(\'No recording to send!\');"><img src="images/send_inactive.png" style="float:right;" alt="Send Link" TITLE="Send Link" /></a>';
                        }else{
                            $send_link = '<a href="#" id="USR_SEND_RECORDING_LINK" data-params="'.$call_details["RecordingUrl"].'.mp3"><img src="images/send.png" style="float:right;" alt="Send Link" TITLE="Send Link" /></a>';
                        }
                        ?>
                        <tr>
                            <td colspan="2"><div style="float:left;width: 400px;"><?php 

                                $callback = (!isset($message->message_id)) ? "" : "readVoicemail(".$message->message_id.")";

                                echo Util::generateFlashAudioPlayer($call_details['RecordingUrl'],'lg', $callback); 

                            ?></div>
                            <?php echo $send_link; ?>
                              <!--  <div style="display: inline-block !important;"><a href="#" data-companyid="<?php echo $db->getCompanyofCall($call_details['CallSid']); ?>" data-params="<?php echo $call_details['CallFrom']; ?>" id="USR_ADD_NUM_TO_BLACKLIST"><img style="vertical-align: text-top;" src="images/blacklist_btn.png" alt="Add to blacklist" title="Add number to Blacklist"></a></div> -->
                            </td>
                        </tr>


                        <?php
                        if ($otherRecordings) {
                            foreach ($otherRecordings as $otherRecording) {
                                if ($db->getVar("mask_recordings") == "true") {
                                    $otherRecording['RecordingUrl'] = Util::maskRecordingURL($otherRecording['RecordingUrl']);
                                }
                                ?>
                                <tr>
                                    <td colspan="2">
                                        <div style="float:left;width: 400px;">
                                            <?php echo Util::generateFlashAudioPlayer($otherRecording['RecordingUrl'],'lg'); ?>
                                        </div>
                                        <?php
                                        $send_link = '<a href="#" id="USR_SEND_RECORDING_LINK" data-params="'.$otherRecording["RecordingUrl"].'.mp3"><img src="images/send.png" style="float:right;" alt="Send Link" TITLE="Send Link" /></a>';
                                        echo $send_link;
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        <?php } ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <?php //print_r();
                    //print_r($call_notes);

                    if($call_details['FromCity']!=NULL)
                    {
                        $geo = Util::yahoo_geo($call_details['FromCity'].", ".$call_details['FromState']);
                        ?>

                        <span><center><?php echo ucwords($call_details['FromCity']).", ".$call_details['FromState'];?></center></span>
                        <div id="map" style="width: 426px; height: 265px; border: 1px solid #000055; font-family: verdana; font-size: 8pt;">
                            <noscript>
                                You need javascript to view this map.
                            </noscript>
                        </div>
                        <script src="//maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
                        <script type="text/javascript">
                            var myLatlng = new google.maps.LatLng(<?php echo $geo['latitude'];?>,<?php echo $geo['longitude'];?>);
                            var myOptions = {
                                zoom: 12,
                                center: myLatlng,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };
                            var map = new google.maps.Map(document.getElementById("map"), myOptions);

                            var marker = new google.maps.Marker({
                                position: myLatlng
                            });

                            // To add the marker to the map, call setMap();
                            marker.setMap(map);
                        </script>
                        <?php }else{ ?>
                        <div id="map" style="width: 426px; height: 172px; border: 1px solid #000; font-family: verdana; font-size: 8pt;margin: 0 auto;"><img src="images/no_map.png" /></div>
                        <?php } ?>
                </td>
            </tr>
            <tr style="text-shadow: 1px 1px 0 #fff; background: url(images/bhead.gif) 0 -8px repeat-x; height:46px;">
                <th colspan="2" class="big-font">Notes</th>
            </tr>
            <tr>
                <td colspan="2" style="">
                    <div id="note_container">
                        <ul>
                            <?php
                            foreach($call_notes as $note)
                            {
                                $tz = new DateTimeZone($TIMEZONE);
                                $date = new DateTime($note['DateAdded']);
                                $date->setTimezone($tz);
                                ?><li data-id="<?php echo $note['idx'];?>"><?php echo $note['NoteContents'];?><span style="float:right;"><span style="color: #999;">[<?php echo $note['User'];?> @ <?php echo $date->format("n\/j\/y");?>]</span>&nbsp;&nbsp;<a href="#" id="USR_DELETE_NOTE" data-params="<?php echo $note['idx'];?>" title="Delete Note"><img align="top" src="images/delete.gif"></a></span></li><?php
                            }
                            ?>
                        </ul>
                    </div>

                    <div style="width: 100%; float:right;"><input id="note" name="note" type="text" style="display: inline-block !important; width: 750px;height: 26px;" onKeyDown="return keyDownEvent(event)" /><input id="USR_ADD_NOTE" style="margin-left: 5px;margin-bottom: 4px;display: inline-block !important;" class="button gray" type="submit" value="Add Note"></div></td>
            </tr>

            <tr style="text-shadow: 1px 1px 0 #fff; background: url(images/bhead.gif) 0 -8px repeat-x; height:46px;">
                <th colspan="2" class="big-font">Call History for <?php echo format_phone($db->format_phone_db($call_details['CallFrom'])); ?> </th>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>To</th>
                            <th>Status</th>
                            <th>Duration</th>
                            <?php if($RECORDINGS && $db->isCompanyRecordingDisabled($_SESSION['sel_co'])==false) { ?>
                            <th>Recording</th>
                            <?php } ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $call_history = $db->getAllCallsFromNumber($db->getCompanyofCall($call_details['CallSid']),$call_details['CallFrom']);
                            foreach($call_history[0] as $call){
                                if($call_details['CallSid']==$call['CallSid'])
                                    continue;
                                $tz = new DateTimeZone($TIMEZONE);
                                $d = new DateTime($call['DateCreated']);
                                $d->setTimezone($tz);
                                if($call['DialCallStatus']=='voicemail'){
                                    $stmt = $db->customExecute("SELECT * FROM messages WHERE CallSid = ?");
                                    $stmt->execute(array($call['CallSid']));
                                    $message = $stmt->fetch(PDO::FETCH_OBJ);
                                    $call['RecordingUrl'] = $message->message_audio_url;
                                    $call['DialCallDuration'] = $message->RecordingDuration;
                                    if($message->message_flag == 0){
                                        $stmt = $db->customExecute("UPDATE messages SET message_flag = 1 WHERE message_id = ?");
                                        $stmt->execute(array($message->message_id));
                                    }
                                }
                        ?>
                        <tr>
                            <td><a title="Click for Call Details" href="call_detail.php?id=<?php echo $call['CallSid']; ?>"><?php echo $d->format("D n\/j Y \a\\t g\:iA T"); ?></a></td>
                            <td><?php echo format_phone($db->format_phone_db($call['CallTo'])); ?></td>
                            <td><?php echo ucwords($call['DialCallStatus']);?></td>
                            <td><?php echo Util::formatTime($call['DialCallDuration']);?></td>
                            <?php if($RECORDINGS && $db->isCompanyRecordingDisabled($_SESSION['sel_co'])==false) { ?>
                            <td width="160"><?php Util::generateFlashAudioPlayer($call['RecordingUrl'],"sm"); ?>
                            </td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </td>
            </tr>

            <?php
            if (!empty($call_details['SpId'])) {
                $urls_visited = $db->getVisitedUrls($call_details['SpId']);
            }
            if (!empty($urls_visited)) {
            ?>
            <tr style="text-shadow: 1px 1px 0 #fff; background: url(images/bhead.gif) 0 -8px repeat-x; height:46px;">
                <th colspan="2" class="big-font">Visitor Flow</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <thead>
                        <tr>
                            <th>URL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $i=1;
                            foreach($urls_visited as $url) {
                        ?>
                        <tr>
                            <td>
                                <strong><?php echo $i; ?>.</strong>
                                <?php echo $url['url']; ?>

                                <?php if ($url['made_call']) { ?>
                                    <img src="images/icons/answer.png" style="width: 16px; margin-right: 5px;" />
                                <?php } ?>
                            </td>
                        </tr>
                        <?php $i++; } ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <?php } ?>

        </table>

    </div>		<!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>
</div>		<!-- .block ends -->


<?php include "include/footer.php"; ?>
    <script type="text/javascript">
        function keyDownEvent(e)
        {
            if (e.keyCode == 13) {
                $("#USR_ADD_NOTE").trigger('click');
            }
        }

        function readVoicemail(message_id) {
            var response = JSONajaxCall({
                func: 'SYSTEM_READ_VOICEMAIL',
                data: {
                    message_id: message_id
                } 
            });

            response.done(function() {
                $(".message_flag_" + message_id).text('Read');
            });
        }
    </script>
</body>
</html>
