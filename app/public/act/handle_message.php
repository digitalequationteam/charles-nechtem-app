<?php

require_once('include/config.php');
require_once('include/util.php');
require_once('include/Services/Twilio.php');
require_once "include/class.phpmailer.php";
require_once("include/class.emailtemplates.php");
$db = new DB();

$exten = $_REQUEST['exten'];
$url = $_REQUEST['RecordingUrl'];

if ($db->getVar("mask_recordings") == "true") {
    $url = Util::maskRecordingURL($url);
}

$caller_id = $_REQUEST['Caller'];

$smtp_username = $db->getVar("smtp_username");
$smtp_password = $db->getVar("smtp_password");

global $SITE_NAME;

if($SITE_NAME!="")
    $site_name = $SITE_NAME;
else
    $site_name = "Call Tracking";

if (strlen($exten) && strlen($url)) {
	
	//save recording url and callerid as a message for that mailbox extension

    $company_name = $db->getCompanyName($exten);

    $template = new EmailTemplate();
    $email_template = $template->getEmailTemplate(3);
    $msg = str_replace("[from]",$_REQUEST['From'],$email_template->content);
    $msg = str_replace("[to]",$_REQUEST['To'],$msg);
    $msg = str_replace("[company]",$company_name,$msg);
    $msg = str_replace("[duration]",$_REQUEST['RecordingDuration'],$msg);
    $msg = str_replace("[recordingurl]",$url,$msg);

    $subject = $email_template->subject;

    $users = $db->customQuery("SELECT * FROM users WHERE email IS NOT NULL");

    foreach($users as $user)
    {
        $notification_type = $user['notification_type'];

        if ($notification_type == 1 || $notification_type == 9) {
        	if($user['access_lvl']>0)
            {
                $email_params = array(
                    "subject" => $subject,
                    "msg" => $msg,
                    "emails" => array($user['email'])
                );
                if (isset($user['extra_notification_emails'])) {
                    $extra_emails = explode(",", $user['extra_notification_emails']);
                    foreach ($extra_emails as $email_address) {
                        if (!empty($email_address)) $email_params['emails'][] = $email_address;
                    }
                }

                Util::sendEmail($email_params);
            }else{
                if($db->isUserInCompany($exten,$user['idx']))
                {
                    $send = true;
                    $disabled_numbers = $db->getUserAccessNumbers($user['idx']);
                    foreach($disabled_numbers as $disabled_number)
                    {
                        if($disabled_number->number==$CallTo)
                            $send = false;
                    }

                    if($send)
                    {
                        $send = false;
                        $filter_numbers = $db->getUserOutgoingAccessNumbers($user['idx']);

                        if(count($filter_numbers)>0){
                            if( is_array($filter_numbers) || is_object($filter_numbers) )
                            {
                                foreach($filter_numbers as $filter_number)
                                {
                                    if($filter_number->number == $DialCallTo)
                                        $send = true;
                                }
                            }else{
                                $send = true;
                            }
                        }else{
                            $send = true;
                        }
                        if($send)
                        {
                        	$email_params = array(
                                "subject" => $subject,
                                "msg" => $msg,
                                "emails" => array($user['email'])
                            );
                            if (isset($user['extra_notification_emails'])) {
                                $extra_emails = explode(",", $user['extra_notification_emails']);
                                foreach ($extra_emails as $email_address) {
                                    if (!empty($email_address)) $email_params['emails'][] = $email_address;
                                }
                            }

                            Util::sendEmail($email_params);
                        }
                    }
                }
            }
        }
    }

	$db->addMessage($exten, $caller_id, $url, $_REQUEST['CallSid'], $_REQUEST['RecordingDuration']);

    $stmt = $db->customExecute("UPDATE calls SET DialCallStatus = 'voicemail', DialCallDuration = ?, RecordingUrl = ? WHERE CallSid = ?");
    $stmt->execute(array($_REQUEST['RecordingDuration'], $url, $_REQUEST['CallSid']));

	$response = new Services_Twilio_Twiml();
	if ( isset($_REQUEST['wId']) ) {
		$response->redirect('ivrmenu.php?company_id='.$exten.'&wId='. $_REQUEST['wId']);
	}else{
		//$response->say('Thank you, good bye');
	}
	print $response;
}
?>