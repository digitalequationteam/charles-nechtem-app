<?php
/*
 * This file will handle the tasks that would be done via cron job
 */
//Sending 200 OK response
header('HTTP/1.1 200 OK');

//Initiaizing the session
session_start();

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';


//If cron action param is set in url
if (isset($_REQUEST['action'])):

    //Switching to action type
    switch ($_REQUEST['action']):

        case 'ad_set_pc':
            if(isset($_SESSION['AD_CALLSID']) && isset($_POST['pc'])){
                $db = new DB();
                $stmt = $db->customExecute("UPDATE ad_ad_call_log SET code = ? WHERE CallSid = ?");
                $stmt->execute(array($_POST['pc'],  $_SESSION['AD_CALLSID']));
            }
            break;

        //If ajax action is set to call script retrieval
        case 'ad_ad_dd_open_script':

            //If campaign ID is set in request URI and user logged in
            //Priting the call script to ajax response
            if (isset($_REQUEST['ad_ad_dd_campaign_idx']) && isset($_SESSION['user_id']))
                echo ad_ad_get_call_script($_REQUEST['ad_ad_dd_campaign_idx']);

            break;


        case 'ad_get_timezone':{
            if(isset($_REQUEST['lat']) && isset($_REQUEST['lon'])){
                echo $db->curlGetData("http://cronapi.web1syndication.com/8237275212/getTimeFromLatLon?lat=".$_REQUEST['lat']."&lon=".$_REQUEST['lon']);
            }
            break;
        }

        //If ajax action is set to call script retrieval
        case 'ad_ad_dd_get_call_details':

            //If campaign IDX and contact number is set in request URI
            //and user is logged in
            if (isset($_REQUEST['ad_ad_dd_campaign_idx']) && isset($_REQUEST['ad_ad_dd_contact_number']) && isset($_SESSION['user_id'])):

                //Storing the data in relative variables
                $campaign_idx = $_REQUEST['ad_ad_dd_campaign_idx'];
                $contact_number = $_REQUEST['ad_ad_dd_contact_number'];

                //Retrieving the contact_idx
                $contact_idx = ad_advb_get_contact_idx($contact_number);

                //Retrieving the contact details based on contact index
                $contact_details = ad_advb_get_contact_details($contact_idx);

                $ext_idx = ad_advb_get_extended_contact_idx($campaign_idx,$contact_number);

                //Setting the keys of which data has to be printing
                $data_keys = array(
                    'first_name',
                    'last_name',
                    'email',
                    'business_name',
                    'address',
                    'city',
                    'state',
                    'zip',
                    'website'
                );

                //Looping over contact details
                if (!empty($contact_details)) {
                    foreach ($contact_details as $key => $value):
                        //if currently iterated key does not exist in earlier defined ones
                        if (!in_array($key, $data_keys)):
                            //Deleting the associted data from array variable
                            unset($contact_details[$key]);
                        endif;
                    endforeach;
                }

                $contact_details['idx'] = $contact_idx;
                $contact_details['ext_idx'] = $ext_idx;
                $contact_details['business_name'] = Util::escapeString($contact_details['business_name']);

                if (!empty($contact_details)) {
                    foreach ($contact_details as $key => $value) {
                        if (isset($contact_details[$key]['business_name']))
                            $contact_details[$key]['business_name'] = Util::escapeString($contact_details[$key]['business_name']);
                    }
                }

                //Printing the required contact info as ajax response
                echo json_encode($contact_details);

            endif;

            break;

        case 'store_current_call_sid':

            //Adding the urrent callsid to a file
            $CurrentCallSid = $_REQUEST['CallSid'];
            $fp = fopen(dirname(__FILE__) . '/include/ad_auto_dialer_files/CurrentCallSid.txt', 'w');
            fwrite($fp, $CurrentCallSid);
            fclose($fp);

            ?>
            <Response>
                <Say></Say>
            </Response>
            <?php
            break;

        case 'play_mp3':

            //If user is logged in
            if (isset($_SESSION['user_id'])):

                try {

                    //Storing params in custom variables
                    $file_key = $_REQUEST['rel'];

                    //Retriiving the SID of current call
                    $CurrentCallSid = ad_ad_get_current_call_sid(dirname(__FILE__) . '/include/ad_auto_dialer_files');

                    global $AccountSid, $AuthToken;
                    $client = new Services_Twilio($AccountSid, $AuthToken);

                    // Get an object from its sid. If you do not have a sid,
                    // check out the list resource examples on this page
                    $call = $client->account->calls->get($CurrentCallSid);

                    $call->update(array(
                        "Url" => dirname(s8_get_current_webpage_uri()) . '/ad_ajax.php?action=play_mp3_to_caller&mp3_url=' . base64_encode($file_key),
                        "Method" => "POST"
                    ));

                    echo "You request for playing mp3 file to caller has been processed.\nCall will automatically hang after mp3 play finishes.";
                } catch (Services_Twilio_RestException $e) {
                    echo "Error while processing your request.\nSeems like no call is in progress currently.";
                }

            endif;

            break;

        case 'play_mp3_to_caller':
            ?>
            <Response>
                <Play><?php echo str_replace(" ", "%20", base64_decode($_REQUEST['mp3_url'])); ?></Play>
            </Response>
            <?php
            break;

        case 'play_text':

            //If user is logged in
            if (isset($_SESSION['user_id'])):

                try {

                    //Storing params in custom variables
                    $text = $_REQUEST['text'];
                    $language = $_REQUEST['language'];
                    $voice = $_REQUEST['voice'];

                    //Retriiving the SID of current call
                    $CurrentCallSid = ad_ad_get_current_call_sid(dirname(__FILE__) . '/include/ad_auto_dialer_files');

                    global $AccountSid, $AuthToken;
                    $client = new Services_Twilio($AccountSid, $AuthToken);

                    // Get an object from its sid. If you do not have a sid,
                    // check out the list resource examples on this page
                    $call = $client->account->calls->get($CurrentCallSid);

                    $call->update(array(
                        "Url" => dirname(s8_get_current_webpage_uri()) . '/ad_ajax.php?action=play_text_to_speech_to_caller&text=' . urlencode($text).'&language=' . urlencode($language).'&voice=' . urlencode($voice),
                        "Method" => "POST"
                    ));

                    echo "You request for playing the text to speech to caller has been processed.\nCall will automatically hang after text to speech finishes.";
                } catch (Services_Twilio_RestException $e) {
                    echo "Error while processing your request.\nSeems like no call is in progress currently.";
                }

            endif;

            break;

        case 'play_text_to_speech_to_caller':
            $_REQUEST['language'] = str_replace("|W", "", $_REQUEST['language']);
            $_REQUEST['language'] = str_replace("|M", "", $_REQUEST['language']);
            ?>
            <Response>
                <Say language="<?php echo urldecode($_REQUEST['language']); ?>" voice="<?php echo urldecode($_REQUEST['voice']); ?>"><?php echo urldecode($_REQUEST['text']); ?></Say>
            </Response>
            <?php
            break;

        case 'ad_retrive_progress':

            //If campaign type and campaign index is set in request URL
            //and user is logged in
            if (isset($_REQUEST['camp_type']) && isset($_REQUEST['camp_idx']) && isset($_SESSION['user_id'])):

                //Initializing the response variable
                $output = FALSE;

                //Retrieving the campaign type
                $campaign_type = $_REQUEST['camp_type'];
                //Retrieving the campaign index
                $campaign_idx = $_REQUEST['camp_idx'];

                //Switching over campaign type
                switch (strtolower($campaign_type)):

                    //If camapign type is of auto dialer type
                    case 'ad':
                        //Retrieving the auto dialer campaign progress
                        $output = ad_ad_get_campaign_progress($campaign_idx);
                        break;

                    //If campaign type is of voice broadcast type
                    case 'vb':
                        //Retrieving the voice broadcast campaign progress
                        $output = ad_vb_get_campaign_progress($campaign_idx);
                        break;

                endswitch;

                //Printing the response message
                echo $output;

            endif;

            break;

        case 'ad_ad_ec_display_contacts':

            //If user is logged in
            if (isset($_SESSION['user_id'])):

                //Initializing the variable that will contain campaigns contact
                $campaign_contacts = array();

                //If campaign ID is set in request URI
                //Priting the call script to ajax response
                if (isset($_REQUEST['ad_ad_ec_campaign_idx']))
                    $campaign_contacts = ad_advb_retrieve_all_contacts('ad', $_REQUEST['ad_ad_ec_campaign_idx']);

                //If atlest one contact do exist in campaign
                if (count($campaign_contacts) > 0):

                    //Creating the database instance
                    $db = new DB();

                    //DIsplaying title
                    echo '<h2>"' . ad_ad_get_campaign_name($_REQUEST['ad_ad_ec_campaign_idx']) . '" contacts (' . count($campaign_contacts) . ')</h2>';

                    //Looping over campaign contacts
                    echo '<ol style="list-style:decimal;padding:25px;">';
                    foreach ($campaign_contacts as $single_number):
                        echo '<li>' . format_phone($db->format_phone_db($single_number)) . ' - <a class="delete_contact" href="#" rel="' . $single_number . '">Delete</a></li>';
                    endforeach;
                    echo '</ol>';

                else:

                    //Displaying error message
                    echo 'No contacts found.';

                endif;

            endif;

            break;

        case 'ad_ad_call_change_pc':
            if(isset($_REQUEST['pc']) && isset($_REQUEST['callsid'])){
                ad_advb_update_pc($_REQUEST['callsid'],$_REQUEST['pc']);
            }
            break;

        //If action is set to adding a new note in system
        case 'ad_ad_dd_add_note':

            //If user is logged in
            if (isset($_SESSION['user_id'])):

                //Retrieving the campaign ID
                $contact_idx = $_REQUEST['ad_ad_dd_contact_idx'];

                //Buidling the note data
                $note_details = array(
                    'type' => 'AutoDialer',
                    'contact_idx' => $contact_idx,
                    'note' => $_REQUEST['ad_ad_dd_note']
                );

                //Adding the note into system database
                ad_advb_cl_add_note($note_details);

                //Retrieving the campaign notes
                $contact_notes = ad_advb_cl_get_notes($contact_idx);

                //If campign notes is an array and atleast one note does exist in system
                if (is_array($contact_notes) && count($contact_notes) > 0):
                    //Looping through each note
                    foreach ($contact_notes as $single_note):
                        //Printing the note on screen
                        $date = Util::convertToLocalTZ($single_note['date'])->format(Util::STANDARD_LOG_DATE_FORMAT);
                        echo '<b>' . $date . ': </b>' . $single_note['note'] . '<br/>';
                    endforeach;
                endif;

            endif;

            break;

        //If action is set to displaying notes html
        case 'ad_ad_dd_notes':

            //if user is logged in
            if (isset($_SESSION['user_id'])):

                //Printing the notes html as ajax response
                include 'include/ad_auto_dialer_files/notes_html.php';

            endif;

            break;

        //If action is set to process dialling details form
        case 'ad_ad_dd_process_form':

            //If user is logged in
            if (isset($_SESSION['user_id'])):

                //Initializing the call details array
                $call_details = array(
                    'idx' => @$_REQUEST['ad_ad_dd_idx'],
                    'first_name' => @$_REQUEST['ad_ad_dd_firstname'],
                    'last_name' => @$_REQUEST['ad_ad_dd_lastname'],
                    'email' => @$_REQUEST['ad_ad_dd_email'],
                    'phone_number' => @$_REQUEST['ad_ad_dd_pnone'],
                    'address' => @$_REQUEST['ad_ad_dd_address'],
                    'city' => @$_REQUEST['ad_ad_dd_city'],
                    'state' => @$_REQUEST['ad_ad_dd_state'],
                    'zip' => @$_REQUEST['ad_ad_dd_zip'],
                    'website' => @$_REQUEST['ad_ad_dd_website'],
                    'business_name' => @$_REQUEST['ad_ad_dd_business']
                );

                //Adding call details into system db
                //And retrieving the status of add operation
                $add_dd = ad_ad_dd_add_call_details($call_details);

                if ($add_dd)
                    echo 'Updated the contact record!';
                else
                    echo 'Failed to update the contact record.';

            endif;

            break;

        //If action is set to retrieve the current system time
        case 'get_system_time':
            global $TIMEZONE;
            $currenttime = new DateTime("now",new DateTimeZone($TIMEZONE));

            echo $currenttime->format('m/d Y g:i A T');

            break;

        case 'delete_contact':

            //If user is logged in
            //and contact number and camapign id is set in request URL
            if (isset($_REQUEST['number']) && isset($_REQUEST['campaign_idx']) && isset($_SESSION['user_id'])):

                //Retrieving the contact number and campaign index ID from request URL
                $contact_number = $_REQUEST['number'];
                $campaign_idx = $_REQUEST['campaign_idx'];

                //If contact do exist
                if (ad_advb_is_contact_exist('ad', $campaign_idx, $contact_number)):

                    //Trying to delete the contact
                    $response = ad_advb_delete_contact('ad', $campaign_idx, $contact_number);

                    //if delete operation executes successfully
                    if ($response !== FALSE):
                        echo 'Contact delete successfully!';
                    else:
                        echo 'System failed to delete contact.';
                    endif;

                else:

                    //ELSE printing the error message
                    echo 'Contact no longer exist in system for current campaign.';

                endif;

            endif;

            break;

        case 'ad_ad_dd_phone_code':
            if (isset($_REQUEST['phone_code']) && isset($_REQUEST['contact_id'])) {
                ad_ad_ext_update_pc($_REQUEST['phone_code'],$_REQUEST['contact_id']);
            }
            break;

        case 'show_dd':

            //If user is logged in
            if (isset($_SESSION['user_id'])):

                //If contact index is set in request URL
                if (isset($_REQUEST['contact_idx']) && !s8_is_str_blank($_REQUEST['contact_idx'])):

                    //Retriving the contact index
                    $contact_idx = $_REQUEST['contact_idx'];

                    //Retrieving the details of contact index
                    $contact_dd = ad_ad_dd_get_call_details($contact_idx);

                    //If there are details
                    if ($contact_dd !== FALSE && is_array($contact_dd)):

                        echo '<h2>Call log details of "<b>' . format_phone($db->format_phone_db($contact_idx)) . '</b>"</h2>';
                        echo '<table>';

                        //Printing the details as AJAX response
                        foreach ($contact_dd as $key => $value):

                            //If key is phone
                            //Formating it to US format
                            if ($key == 'phone')
                                $value = format_phone($db->format_phone_db($value));

                            echo '
                            <tr>
                                <td><b>' . ucfirst(str_replace('_', ' ', $key)) . '</b></td>
                                <td></td>
                                <td style="padding:5px 20px;">' . $value . '</td>
                            </tr>
                        ';
                        endforeach;

                        echo '</table>';

                    else:

                        //ELSE printing error message
                        echo 'Log details do not exist.';

                    endif;

                else:

                    //ELSE printing error message
                    echo 'Log details do not exist.';

                endif;

            endif;

            break;

        case 'AddContact':
            $phone = trim(urldecode($_REQUEST['phone']));
            if (isset($_REQUEST['k']) && !empty($phone)){

                $list_idx = base64_decode(urldecode($_REQUEST['k']));
                $number = $_REQUEST['phone'];
                $first_name = isset($_REQUEST['firstname']) ? $_REQUEST['firstname'] : '';
                $last_name = isset($_REQUEST['lastname']) ? $_REQUEST['lastname'] : '';
                $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';

                if(!isset($number)||$number==""){
                    echo "Error: phone number must be provided.";
                }else{
                    $db = new DB();
                    $stmt = $db->customExecute("insert into ad_advb_cl_contacts (`phone_number`,`first_name`,`last_name`,`email`) VALUES (?,?,?,?)");
                    $stmt->execute(array(
                        $number,
                        $first_name,
                        $last_name,
                        $email
                    ));

                    $stmt = $db->customExecute("select * from ad_advb_cl_contacts where phone_number = ?");
                    $stmt->execute(array($number));
                    $data = $stmt->fetch(PDO::FETCH_OBJ);

                    $now = new DateTime("now",new DateTimeZone("UTC"));

                    $stmt = $db->customExecute("insert into ad_advb_cl_link (contact_idx,list_idx,date_added) values (?,?,?)");
                    if($stmt->execute(array($data->idx,$list_idx,$now->format(Util::STANDARD_MYSQL_DATE_FORMAT)))) {
                        // Gotta get the AR campaigns..
                        $stmt = $db->customExecute("SELECT * FROM ad_vb_campaigns WHERE list_id = ? AND when_to_run = 'sequence' AND progress = 'Active'");
                        $stmt->execute(array($list_idx));
                        $campaigns = $stmt->fetchAll(PDO::FETCH_OBJ);

                        // Loop through and set sequence for this contact
                        if(is_array($campaigns) || is_object($campaigns))
                            foreach($campaigns as $campaign){
                                $stmt = $db->customExecute("SELECT * FROM ad_vb_sequences WHERE campaign_id = ?");
                                $stmt->execute(array($campaign->idx));
                                $sequences = $stmt->fetchAll(PDO::FETCH_OBJ);

                                if(is_array($sequences)||is_object($sequences))
                                    foreach($sequences as $sequence){

                                        if (empty($sequence->delay)) {
                                            $time = $sequence->time;

                                            $this_date = new DateTime("now", new DateTimeZone($TIMEZONE));
                                            $this_date->modify($time);
                                            $this_date->setTimezone(new DateTimeZone("UTC"));
                                            $time = $this_date->format("g:i A");

                                            $delay = "";
                                        }
                                        else {
                                            $date = new DateTime("now", new DateTimeZone("UTC"));
                                            $time_return = strtotime($date->format("Y-m-d h:iA"));
                                            $time_return = $time_return + $sequence->delay;

                                            $time = date("g:i A", $time_return);
                                        }

                                        $scheduled_date = new DateTime("+".$sequence->days_after." day", new DateTimeZone("UTC"));
                                        $scheduled_date->modify($time);

                                        $cron_date = $scheduled_date->format(Util::STANDARD_MYSQL_DATE_FORMAT);

                                        $scheduled_date->setTimezone(new DateTimeZone($TIMEZONE));

                                        $stmt = $db->customExecute("INSERT INTO ad_vb_sequence_schedule (sequence_id,cron_id,contact_id,`date`) VALUES (?,'',?,?)");
                                        $stmt->execute(array($sequence->id,$data->idx,$scheduled_date->format(Util::STANDARD_MYSQL_DATE_FORMAT)));
                                        $schedule_id = $db->lastInsertId();

                                        $retn = ad_add_cron_task($cron_date,dirname(s8_get_current_webpage_uri()) . '/ad_cron.php?action=process_sequence&schedule_id='.
                                            $schedule_id);

                                        if(@$retn->result == "success"){
                                            $cb = $retn->data;
                                            $stmt = $db->customExecute("UPDATE ad_vb_sequence_schedule SET cron_id = ? WHERE id = ?");
                                            $stmt->execute(array($cb->id,$schedule_id));
                                        }else{
                                            die("AR Cron Error: ".@$retn->error);
                                        }
                                    }
                            }

                        echo "Contact added successfully! You may now close this window.";
                    }else{
                        echo "Error in adding contact...";
                    }
                }
            }
            break;

        case 'HARVB':

            //IF isset key and number
            if (isset($_REQUEST['k']) && $_REQUEST['phone']):

                //Retrieving the neccessary data from request URL
                $campaign_idx = base64_decode($_REQUEST['k']);
                $number = $_REQUEST['phone'];
                $first_name = isset($_REQUEST['firstname']) ? $_REQUEST['firstname'] : '';
                $last_name = isset($_REQUEST['lastname']) ? $_REQUEST['lastname'] : '';
                $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';

                //Adding the contact into system DB
                ad_advb_add_contacts('vb', $campaign_idx, array($number));

                //Retrieving the unique ID of contact number added
                $contact_idx = ad_advb_get_contact_idx('vb', $campaign_idx, $number);

                //Updating the contact details in system DB
                ad_advb_update_contact_details($contact_idx, array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email
                ));

                //Printing the cureent timestamp to fulfill ajax reqest response
                echo time();

            else:

                //ELSE printing the error message
                echo 'Phone number is missing.';

            endif;

            break;

        case 'HARAD':

            //IF isset key and number
            if (isset($_REQUEST['k']) && $_REQUEST['phone']):

                //Retrieving the neccessary data from request URL
                $campaign_idx = base64_decode($_REQUEST['k']);
                $number = $_REQUEST['phone'];
                $first_name = isset($_REQUEST['firstname']) ? $_REQUEST['firstname'] : '';
                $last_name = isset($_REQUEST['lastname']) ? $_REQUEST['lastname'] : '';
                $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';

                //Adding the contact into system DB
                ad_advb_add_contacts('ad', $campaign_idx, array($number));

                //Retrieving the unique ID of contact number added
                $contact_idx = ad_advb_get_contact_idx('ad', $campaign_idx, $number);

                //Updating the contact details in system DB
                ad_advb_update_contact_details($contact_idx, array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email
                ));

                //Printing the cureent timestamp to fulfill ajax reqest response
                echo time();

            else:

                //ELSE printing the error message
                echo 'Phone number is missing.';

            endif;

            break;

        case 'getLatLong':

            //If address is set in request URL
            if (isset($_REQUEST['address'])):

                //Storing the address in relative variable
                $address = $_REQUEST['address'];

                //Retrieving the latlong and storing it in response variable
                $output = Util::yahoo_geo($address);

                //Printing the ajax response
                echo json_encode(array('lat' => $output['latitude'], 'long' => $output['longitude']));

            endif;

            break;

        case 'ad_advb_cl_add_contacts':

            //If contacts data is provided in request URL
            if (isset($_SESSION['user_id']) && isset($_POST['ad_ad_ac_contacts']) && isset($_POST['ad_contact_list_name'])):

                //Retrieving form data here
                $ad_contact_list_name = $_POST['ad_contact_list_name'];

                //If contact list name supplied is not blank
                if (!s8_is_str_blank($ad_contact_list_name)):

                    //Retrieving manually added contacts
                    $ad_ad_ac_contacts = explode("\n", $_POST['ad_ad_ac_contacts']);

                    //If Contacts is an array and there exist atleast one entry
                    if (is_array($ad_ad_ac_contacts) && count($ad_ad_ac_contacts) > 0):

                        //Looping over contacts
                        foreach ($ad_ad_ac_contacts as $key => $value):

                            //Building contacts data
                            $ad_ad_ac_contacts[$key] = explode(',', $value);

                        endforeach;

                    endif;

                    //Adding the contact list into the system
                    $cl_idx = ad_advb_cl_add_cl($ad_contact_list_name);

                    //If contact list added successfully
                    if ($cl_idx !== FALSE):

                        //Adding the contacts of list
                        ad_advb_cl_add_contacts($cl_idx, $ad_ad_ac_contacts);

                        //Returning the response message
                        echo 'Your request has been processed.';

                    endif;

                else:

                    //Returning failure message
                    echo 'Contact list name provided is blank.';

                endif;

            endif;

            break;

        case 'ad_advb_cl_load_contacts':

            //If user is logged in
            if (isset($_SESSION['user_id'])):

                //Intializing contacts count variable
                $total_contacts = 0;

                //Checking if this is a edit form
                if (isset($_REQUEST['list_idx'])):

                    //Retriving unique ID of contact list 
                    //that is currently being edited
                    $list_idx = $_REQUEST['list_idx'];

                    //Retrieving List contacts
                    $ad_contacts = ad_advb_cl_get_list_contacts($list_idx);

                    //Retrieving total count of list contacts
                    $total_contacts = count($ad_contacts);

                    if ($total_contacts > 0):

                        //Looping over contacts to display their data on screen
                        foreach ($ad_contacts as $single_contact_data):

                            $opted_out = $db->checkNumberOptedOut($list_idx, $single_contact_data['phone_number']);
                            if ($opted_out) {
                                $single_contact_data['opt_out'] = 1;
                            }
                            ?>
                            <tr>
                                <td><?php echo!s8_is_str_blank($single_contact_data['phone_number']) ? $single_contact_data['phone_number'] : ' - '; ?></td>
                                <td><?php echo!s8_is_str_blank($single_contact_data['first_name']) ? $single_contact_data['first_name'] : ' - '; ?></td>
                                <td><?php echo!s8_is_str_blank($single_contact_data['last_name']) ? $single_contact_data['last_name'] : ' - '; ?></td>
                                <td><?php echo!s8_is_str_blank($single_contact_data['email']) ? $single_contact_data['email'] : ' - '; ?></td>
                                <td><?php echo Util::convertToLocalTZ($single_contact_data['date_added'])->format(Util::STANDARD_LOG_DATE_FORMAT); ?></td>
                                <td><?php echo (($single_contact_data['opt_out'] == 0) ? $single_contact_data['add_type'] : "Opted-out"); ?></td>
                                <td>
                                    <a href="#" class="delete_contact" rel="<?php echo $single_contact_data['phone_number']; ?>">Delete</a> |
                                    <a href="ad_contactlist_edit.php?contact_idx=<?php echo $single_contact_data['idx']; ?>">Edit</a>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;

                endif;

                if ($total_contacts == 0):
                    ?>
                    <tr>
                        <td>No records found.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                endif;

            endif;

            break;

        case 'ad_advb_cl_delete_contact':

            //If contact idx and list idx is provided in request URL
            if (isset($_SESSION['user_id']) && isset($_REQUEST['contact_number']) && isset($_REQUEST['list_idx'])):

                //Retrieving the contact IDX
                $contact_number = $_REQUEST['contact_number'];

                //Retrieving the list IDX
                $list_idx = $_REQUEST['list_idx'];

                if (ad_advb_cl_is_contact_exist($list_idx, $contact_number)):

                    //Deleting the contact from system and retrieving its status
                    $status = ad_advb_cl_delete_contact($list_idx, $contact_number);

                    //Returning the appropriate response based on status
                    if ($status)
                        echo 'Contact deleted successfully.';
                    else
                        echo 'System failed to delete contact.';

                else:

                    //Printing the failure message
                    echo 'Number you are trying to delete does not exist in system.';

                endif;

            endif;

            break;

        case 'ad_advb_cl_delete_list':

            //If contact idx and list idx is provided in request URL
            if (isset($_SESSION['user_id']) && isset($_REQUEST['list_idx'])):

                //Retrieving the list IDX
                $list_idx = $_REQUEST['list_idx'];

                //Retrieving the list_name
                $list_name = ad_advb_cl_get_cl_name($list_idx);

                //If list do exist in system DB.
                if ($list_name !== FALSE):

                    //Deleting the contact from system and retrieving its status
                    $status = ad_advb_cl_delete_contact_list($list_idx);

                    //Returning the appropriate response based on status
                    if ($status)
                        echo 'SUCCESS: Contact list deleted successfully.';
                    else
                        echo 'System failed to delete contact list.';

                else:

                    //Printing the failure message
                    echo 'List you are trying to delete does not exist in system.';

                endif;

            else:

                //List do not exist in system.
                echo 'Contact list do not exist.';

            endif;

            break;

        //If action is set to adding a new note in system
        case 'ad_advb_cl_add_note':

            //If user is logged in
            if (isset($_SESSION['user_id'])):

                //Retrieving the campaign ID
                $contact_idx = $_REQUEST['contact_idx'];

                //Buidling the note data
                $note_details = array(
                    'contact_idx' => $contact_idx,
                    'note' => $_REQUEST['note'],
                    'type' => 'Manual'
                );

                //Adding the note into system database
                $insert_status = ad_advb_cl_add_note($note_details);

                //If insert operation is successfull
                //Printing the success message
                //ELSE printing the failure message
                if ($insert_status)
                    echo 'SUCCESS: note added succesfully.';
                else
                    echo 'FAILED: System failed to add note.';


            endif;

            break;

        //If action is set to displaying notes html
        case 'ad_advb_cl_get_notes':

            //if user is logged in
            if (isset($_SESSION['user_id'])):

                //Retrieving the campaign ID
                $contact_idx = $_REQUEST['contact_idx'];

                //Retrieving the contact notes
                $contact_notes = ad_advb_cl_get_notes($contact_idx);

                //If number of notes count is greater than 0
                if (count($contact_notes) > 0):

                    //Building the response variable
                    $output = '<ul>';
                    foreach ($contact_notes as $single_note_data):
                        $output .= '<li data-id="' . $single_note_data['idx'] . '">' . $single_note_data['note'] . '<span style="float:right;"><span style="color: #999;">[' . date('m/d/Y H:i e', strtotime($single_note_data['date'])) . ']</span>&nbsp;&nbsp;<a href="#" class="CL_DELETE_NOTE" data-params="' . $single_note_data['idx'] . '" title="Delete Note"><img align="top" src="images/delete.gif"></a></span></li>';
                    endforeach;
                    $output .= '</ul>';

                    //Printing the CL notes html as ajax response
                    echo $output;

                endif;

            endif;

            break;

        case 'ad_advb_cl_delete_note':

            //if user is logged in
            if (isset($_SESSION['user_id'])):

                //Retrieving the campaign ID
                $note_idx = $_REQUEST['cl_note_idx'];

                //Trying to delete the note from system and retrieving its status
                $delete_status = ad_advb_cl_delete_note($note_idx);

                //If delete operation is successfull
                if ($delete_status):

                    //Printing the success message
                    echo 'SUCCESS: Note deleted successfully';

                else:

                    //ELSE printing the failure message
                    echo 'FAILED: System failed to delete the note.';

                endif;

            endif;

            break;

        case 'handle_csv_upload':

            //Initializing the response variable
            $output = '';

            //Retrieving the temporary path where it is currently stored
            $csv_file_path = $_FILES['csv_file']['tmp_name'];

            if (file_exists($csv_file_path)):

                //Retrieving the data from file
                //And storing it in response variable
                $fp = fopen($csv_file_path, 'r');
                $csv_data = array();
                while ($row = fgetcsv($fp)):
                    $csv_data[] = $row;
                endwhile;

                //CLosing the connection with file
                fclose($fp);

                //Freeing up system storage by deleting the file
                unlink($csv_file_path);

                //Generating the html content and storing it in response variable
                $output['html'] = '
                <div style="width: 940px;">
                    <table style="width: 940px;border-collapse:collapse;" border="1" cellpadding="10">
                        <thead>
                            <tr><th>' . implode('</th><th>', $csv_data[0]) . '</th></tr>
                        </thead>
                        <tbody>';

                //Deleting the first row
                unset($csv_data[0]);

                //Looping over other data
                foreach ($csv_data as $data):
                    $output['html'] .= '<tr><td>' . implode('</td><td>', $data) . '</td></tr>';
                endforeach;

                $output['html'] .= '
                        </tbody>
                    </table>
                </div>';

                //Optimizing the array keys
                $opt_csv_data = array_values($csv_data);
                unset($csv_data);

                //Storing the CSV dat in output variable as JSON encoded
                $output['json'] = $opt_csv_data;

                //Printing the response back to ajax request
                echo json_encode($output);

            else:

                //Printing the error message
                echo 'File not found.';

            endif;

            break;

            case 'getPhoneNumbersRegions':
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');

                $return = array(
                    "regions" => array()
                );

                $numbers = $_POST['phoneNumbers'];

                if (count($numbers) > 0) {

                    $countriesList = Util::$countriesList;

                    foreach ($numbers as $key => $row) {
                        $country[$key]  = $row['country'];
                    }

                    array_multisort($country, SORT_ASC, $numbers);

                    for ($i = 0; $i < count($numbers); $i++) {
                        for ($j = $i; $j < count($numbers); $j++) {
                            if ($numbers[$i]['country'] != "US" && $numbers[$j]['country'] == "US") {
                                $aux = $numbers[$i];
                                $numbers[$i] = $numbers[$j];
                                $numbers[$j] = $aux;
                            }
                        }
                    }

                    foreach ($numbers as $number) {
                        $this_timezone = "";
                        if ($number['country'] == "US") {
                            $stmt = $db->customExecute("SELECT * FROM countries_area_codes WHERE area_code = ? AND country = ? AND type = ? AND timezone != ''");
                            $stmt->execute(array($number['extension'], $number['country'], 'city'));
                            $city_result = $stmt->fetch();

                            $stmt = $db->customExecute("SELECT * FROM countries_area_codes WHERE area_code = ? AND country = ? AND type = ? AND timezone != ''");
                            $stmt->execute(array($number['extension'], $number['country'], 'state'));
                            $state_result = $stmt->fetch();

                            if (!empty($city_result['timezone'])) {
                                $this_timezone = $city_result['timezone'];
                            }
                            if (!empty($state_result['timezone'])) {
                                $this_timezone = $state_result['timezone'];
                            }
                        }
                        else {
                            $stmt = $db->customExecute("SELECT * FROM countries_area_codes WHERE area_code = ? AND country = ? AND type = ?");
                            $stmt->execute(array($number['extension'], $number['country'], 'region'));
                            $region_result = $stmt->fetch();

                            if (!empty($region_result['timezone'])) {
                                $this_timezone = $region_result['timezone'];
                            }
                            else {
                                $stmt = $db->customExecute("SELECT * FROM countries_area_codes WHERE country = ? AND timezone != ''");
                                $stmt->execute(array($number['country']));
                                $region_result = $stmt->fetch();

                                if (!empty($region_result['timezone'])) {
                                    $this_timezone = $region_result['timezone'];
                                }
                            }
                        }

                        if (empty($this_timezone)) {
                            $this_timezone = "Unknown";
                        }
                        else {
                            try {
                                $tz = new DateTimeZone($this_timezone);
                                $date = new DateTime(date("Y-m-d H:i:s"));
                                $date->setTimezone($tz);

                                $this_timezone = $this_timezone." - ".$date->format("h:i A");
                            }
                            catch (Exception $e) {}
                        }

                        if (!isset($return['regions'][$countriesList[$number['country']]])) {
                            $return['regions'][$countriesList[$number['country']]] = array();
                        }

                        if (!isset($return['regions'][$countriesList[$number['country']]][$this_timezone])) {
                            $return['regions'][$countriesList[$number['country']]][$this_timezone] = array();
                        }

                        $return['regions'][$countriesList[$number['country']]][$this_timezone][] = $number['number'];
                    }
                }

                foreach ($return['regions'] as $key => $value) {
                    $sort = array();
                    foreach ($return['regions'][$key] as $key2 => $value2) {
                        $sort[$key2]  = $key2;
                    }

                    array_multisort($sort, SORT_ASC, $return['regions'][$key]);
                }
                

                array_multisort($country, SORT_ASC, $numbers);

                echo json_encode($return);
                exit();
            break;

    endswitch;

endif;
exit;
?>