<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();

require_once('include/util.php');
require_once('include/Pagination.php');
$db = new DB();

// Check if Admin
if($_SESSION['permission'] == 0)
{
    $company_count = $db->getCompanyCountForUser($_SESSION['user_id']);
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);
}else{
    $company_count = $db->getCompanyCount();
    $companies = $db->getAllCompanies();
}

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

$twilio_numbers=Util::get_all_twilio_numbers();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

</head>





<body>

<div id="hld">

<div class="wrapper">		<!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->


<div class="block">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>
            <?php
            if($company_count==1) echo ("1 Company &nbsp; &nbsp; "); else
                echo ($company_count." Companies &nbsp; &nbsp; ");
            ?>
        </h2>

    </div>		<!-- .block_head ends -->

    <div class="block_content">

        <?php if(@$_GET['sel']=="no"){ ?>
        <div class="message warning" style="width: 93%;margin: 0 auto;margin-top: 8px;"><p>Please select a company to view.</p></div>
        <?php } ?>

        <table cellpadding="0" cellspacing="0" width="100%" class="companies">

            <thead>
            <tr>
                <th>Company</th>
                <th>Numbers</th>
                <th>Calls (Last 7 days)</th>
                <th>Calls (Total)</th>
                <th>Calls (Out)</th>
            </tr>
            </thead>

            <tbody>

            <?php

            if($_SESSION['permission']==0)
            {
                $disabled_numbers = $db->getUserAccessNumbers($_SESSION['user_id']);
            }else{
                $disabled_numbers = array();
            }
            //session_write_close();

            if (!isset($_SESSION['last_cached_date'])) {
                $_SESSION['last_cached_date'] = strtotime("now");
            }

            foreach ($companies as $key => $company) {
                global $TIMEZONE;
                $now = new DateTime_52();
                $now->setTimezone(new DateTimeZone($TIMEZONE));

                $sevendaysago = new DateTime_52();
                $sevendaysago->setTimezone(new DateTimeZone($TIMEZONE));
                $sevendaysago->modify("-7 days");
                $sevendaysago->setTime(0,0,0);
                
                if (isset($_SESSION['companies_cached_counts_'.$company['idx']])) {
                    $cached_counts = json_decode($_SESSION['companies_cached_counts_'.$company['idx']], true);
                    $numbers = $cached_counts['numbers'];
                    $last_seven_days_count = $cached_counts['last_seven_days_count'];
                    $more_calls = $db->getCallsInRangeCount($company['idx'], $_SESSION['last_cached_date'], strtotime("now"));

                    $last_seven_days_count = $last_seven_days_count + $more_calls;

                    $total_incoming_calls = $cached_counts['total_incoming_calls'] + $more_calls;
                    $total_outgoing_calls = $cached_counts['total_outgoing_calls'];

                    $cached_counts['last_seven_days_count'] = $last_seven_days_count;
                    $cached_counts['total_incoming_calls'] = $total_incoming_calls;

                    $more_calls = $db->getCallsInRangeCount($company['idx'], $_SESSION['last_cached_date'], strtotime("now"), 0, 'all', false, "", true, "", "");
                    $total_outgoing_calls = $cached_counts['total_outgoing_calls'] + $more_calls;
                    $cached_counts['total_outgoing_calls'] = $total_outgoing_calls;

                    $_SESSION['companies_cached_counts_'.$company['idx']] = json_encode($cached_counts);
                }
                else {
                    $c_nums = $db->getCompanyNumIntl($company['idx']);
                    $numbers = "";
                    $last_seven_days_count = 0;

                    if($c_nums!="")
                    {
                        foreach($c_nums as $number)
                        {
                            $number_disabled = false;
                            foreach($disabled_numbers as $disabled_number)
                            {
                                $number_ = "";
                                if(!$number[1])
                                    $number_ = "+1".$number[0];

                                if($disabled_number->number===$number_)
                                    $number_disabled = true;
                            }

                            if($number_disabled)
                                continue;

                            $numbers .= format_phone($number[0]).", ";
                            $calls_arr = $db->getCallsLastSeven($number[0],$number[1]);
                            for ($i = 0; $i <= 7; $i++)
                            {
                                $last_seven_days_count += $calls_arr[$i];
                            }
                        }
                    }

                    if(strlen($numbers)==0)
                        $numbers = "None";

                    $total_incoming_calls = 0;
                    $total_outgoing_calls = 0;

                    $call_c = $db->cget_calls_count($company['idx']);
                    if(is_array($call_c))
                        foreach($call_c as $num){
                            $total_incoming_calls += $num['cnt'];
                        }

                    $out_call = $db->cget_outgoing_calls_count($company['idx']);
                    if(is_array($out_call))
                        foreach($out_call as $num){
                            $total_outgoing_calls += $num['cnt'];
                        }


                    $_SESSION['companies_cached_counts_'.$company['idx']] = json_encode(array(
                        'numbers' => $numbers,
                        'last_seven_days_count' => $last_seven_days_count,
                        'total_incoming_calls' => $total_incoming_calls,
                        'total_outgoing_calls' => $total_outgoing_calls
                    ));
                }
            ?>
            <tr>
                <td><?php echo "<a href=\"index.php?op=select&co=".$company['idx']."\" title=\"Click to select company.\">" . Util::escapeString($company['company_name']) . "</a>"; ?></td>
                <td><?php echo $numbers; ?></td>
                <td><a href="call_report.php?sd=<?php echo urlencode($sevendaysago->format("m/d/Y")); ?>&ed=<?php echo urlencode($now->format("m/d/Y")); ?>&cm=<?php echo $company['idx']; ?>&cr=all&pc=0&rt=default&page=1" target="_blank" title="Report details"><?php echo $last_seven_days_count; ?></a></td>
                <td><?php echo $total_incoming_calls; ?></td>
                <td><?php echo $total_outgoing_calls; ?></td>
            </tr>
                <?php }
                    $_SESSION['last_cached_date'] = strtotime("now");
                ?>
        </table>

    </div>		<!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>
</div>		<!-- .block ends -->


<?php include "include/footer.php"; ?>
    <script type="text/javascript">
        var lastsevenExtract = function(node){
            return $(node).children[0].innerHTML;
        };
        $("table.companies").tablesorter({
            widgets: ['zebra'],
            textExtraction: { 2:lastsevenExtract }
        });
    </script>
</body>
</html>