function IsEmpty(aTextField) {
    if (typeof(aTextField) == "undefined") {
        return false;
    }

    if ((aTextField.length == 0) ||
        (aTextField == null)) {
        return true;
    } else {
        return false;
    }
}