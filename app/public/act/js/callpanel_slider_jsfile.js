/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var $jj = jQuery;

(function($)
{
    // This script was written by Steve Fenton
    // http://www.stevefenton.co.uk/Content/Jquery-Side-Content/
    // Feel free to use this jQuery Plugin
    // Version: 3.0.2
    // Contributions by: 

    var classModifier = "";
    var sliderCount = 0;
    var sliderWidth = "400px";

    var attachTo = "rightside";

    var totalPullOutHeight = 0;

    function CloseSliders(thisId) {
        // Reset previous sliders
        for (var i = 0; i < sliderCount; i++) {
            var sliderId = classModifier + "_" + i;
            var pulloutId = sliderId + "_pullout";

            // Only reset it if it is shown
            if ($("#" + sliderId).width() > 0) {

                if (sliderId == thisId) {
                    // They have clicked on the open slider, so we'll just close it
                    showSlider = false;
                }

                // Close the slider
                $("#" + sliderId).animate({
                    width: "0px"
                }, 1500);

                // Reset the pullout
                if (attachTo == "leftside") {
                    $("#" + pulloutId).animate({
                        left: "0px"
                    }, 1500);
                } else {
                    $("#" + pulloutId).animate({
                        right: "0px"
                    }, 1500);
                }
            }
        }
    }

    function ToggleSlider() {

        var rel = $(this).attr("rel");

        var thisId = classModifier + "_" + rel;
        var thisPulloutId = thisId + "_pullout";
        var showSlider = true;

        if ($("#" + thisId).width() > 0) {
            showSlider = false;
        }

        CloseSliders(thisId);

        if (showSlider) {
            // Open this slider
            $("#" + thisId).animate({
                width: sliderWidth
            }, 1500);

            // Move the pullout
            if (attachTo == "leftside") {
                $("#" + thisPulloutId).animate({
                    left: sliderWidth
                }, 1500);
            } else {
                $("#" + thisPulloutId).animate({
                    right: sliderWidth
                }, 1500);
            }
        }

        return false;
    }
    ;

    $.fn.sidecontent = function(settings) {

        var config = {
            classmodifier: "sidecontent",
            attachto: "rightside",
            width: "300px",
            opacity: "0.8",
            pulloutpadding: "5",
            textdirection: "vertical",
            clickawayclose: false
        };

        if (settings) {
            $.extend(config, settings);
        }

        return this.each(function() {

            $This = $(this);

            // Hide the content to avoid flickering
            $This.css({
                opacity: 0
            });

            classModifier = config.classmodifier;
            sliderWidth = config.width;
            attachTo = config.attachto;

            var sliderId = classModifier + "_" + sliderCount;
            var sliderTitle = config.title;

            // Get the title for the pullout
            sliderTitle = $This.attr("title");

            // Start the totalPullOutHeight with the configured padding
            if (totalPullOutHeight == 0) {
                totalPullOutHeight += parseInt(config.pulloutpadding);
            }

            if (config.textdirection == "vertical") {
                var newTitle = "";
                var character = "";
                for (var i = 0; i < sliderTitle.length; i++) {
                    character = sliderTitle.charAt(i).toUpperCase();
                    if (character == " ") {
                        character = "&nbsp;";
                    }
                    newTitle = newTitle + "<span>" + character + "</span>";
                }
                sliderTitle = newTitle;
            }

            // Wrap the content in a slider and add a pullout			
            $This.wrap('<div class="' + classModifier + '" id="' + sliderId + '"></div>').wrap('<div style="width: ' + sliderWidth + '"></div>');
            $("#" + sliderId).before('<div class="' + classModifier + 'pullout" id="' + sliderId + '_pullout" rel="' + sliderCount + '">' + sliderTitle + '</div>');

            if (config.textdirection == "vertical") {
                $("#" + sliderId + "_pullout span").css({
                    display: "block",
                    textAlign: "center"
                });
            }

            // Hide the slider
            $("#" + sliderId).css({
                position: "absolute",
                overflow: "hidden",
                top: "0",
                width: "0px",
                zIndex: "1",
                opacity: config.opacity
            });

            // For left-side attachment
            if (attachTo == "leftside") {
                $("#" + sliderId).css({
                    left: "0px"
                });
            } else {
                $("#" + sliderId).css({
                    right: "0px"
                });
            }

            // Set up the pullout
            $("#" + sliderId + "_pullout").css({
                position: "absolute",
                top: totalPullOutHeight + "px",
                zIndex: "1000",
                cursor: "pointer",
                opacity: config.opacity
            })

            $("#" + sliderId + "_pullout").live("click", ToggleSlider);

            var pulloutWidth = $("#" + sliderId + "_pullout").width();

            // For left-side attachment
            if (attachTo == "leftside") {
                $("#" + sliderId + "_pullout").css({
                    left: "0px",
                    width: pulloutWidth + "px"
                });
            } else {
                $("#" + sliderId + "_pullout").css({
                    right: "0px",
                    width: pulloutWidth + "px"
                });
            }

            totalPullOutHeight += parseInt($("#" + sliderId + "_pullout").height());
            totalPullOutHeight += parseInt(config.pulloutpadding);

            var suggestedSliderHeight = totalPullOutHeight + 30;
            if (suggestedSliderHeight > $("#" + sliderId).height()) {
                $("#" + sliderId).css({
                    height: suggestedSliderHeight + "px"
                });
            }

            if (config.clickawayclose) {
                $("body").click(function() {
                    CloseSliders("");
                });
            }

            // Put the content back now it is in position
            $This.css({
                opacity: 1
            });

            sliderCount++;
        });

        return this;
    };
})($jj);


$jj(document).ready(function() {
    $jj(".callmaker").click(function(e)
    {
        var divStatus = $jj("#divstatus").val();
        if (divStatus == "")
        {
            $jj("div#dialer").animate({
                right: '0px'
            }, 700);
            $jj('#divstatus').val('shown');
            $jj("div#client-make-call").css('display', 'block');
            $jj("div#client-ui-status, div#client-ui-actions").css('display', 'none');
        }
        else
        {
            $jj('#divstatus').val('');
            $jj("div#dialer").animate({
                right: '-315px'
            }, 700);
            Twilio.Device.disconnectAll();
        }
        e.preventDefault();
    });

    $jj('#divstatus').val('');
    $jj("div#dialer").css({
        right: '-315px'
    });

    $jj("#dial-input-button").click(function()
    {
//        $jj("div#client-make-call").css('display', 'none');
//        $jj("div#client-ui-status, div#client-ui-actions").css('display', 'block');
        //$jj("button#client-ui-close").css('display', 'block');
//        $jj("button#client-ui-hangup").css('display', 'block');
    });


    $jj("#client-ui-hangup").click(function()
    {
//        $jj('#divstatus').val('');
//        $jj('#divstatus').val('shown');
//        $jj("div#client-make-call").css('display', 'block');
//        $jj("div#client-ui-status, div#client-ui-actions").css('display', 'none');
    });

    $jj("#call-options-summary").click(function()
    {
        var divStatus = $jj("#callOptionsInput").val();
        if (divStatus == '')
        {
            $jj("#summary-call-toggle").addClass('open');
            $jj("div#call-options-inputs").slideDown();
            //$("#call-options-inputs").show();
            //$("#call-options-inputs").slideDown("slow");
            $jj("#summary-call-toggle").text("«");
            $jj('#callOptionsInput').val('shown');
        }
        else
        {
            $jj("#summary-call-toggle").attr('class', '');
            $jj("div#call-options-inputs").slideUp();
            $jj("#summary-call-toggle").text("»");
            $jj('#callOptionsInput').val('');
        }
    });



    $jj("#hide").click(function()
    {
        $jj('#divstatus').val('');
        $jj("div#dialer").animate({
            right: '-315px'
        }, 700);
        Twilio.Device.disconnectAll();
        //$jj("div#client-on-call").css('display', 'none');
        //$jj("div#client-make-call").css('display', 'block');
    });

    //    $('#call-options-summary').click(function(){
    //        $('#call-options-inputs').slideToggle();
    //    });

    $jj('#caller-id-phone-number').change(function() {
        $jj('.device-number').text($jj(this).val());
    });
    $jj('.device-number').text($jj('#caller-id-phone-number').val());

    $jj('#client-ui-pad .client-ui-button').click(function() {
        $jj('#dial-phone-number').val($jj('#dial-phone-number').val() + '' + $jj(this).children('.client-ui-button-number').text());
    });

    $jj('.clear-ui-button-data').click(function() {
        $jj('#dial-phone-number').val('');
    });

});

var mins = 00;
var secs = mins * 60;
function countdown()
{
    document.getElementById("minutes").innerHTML = 00;
    document.getElementById("seconds").innerHTML = 00;

    mins = 00;
    secs = 00;
    document.getElementById('TimeStatus').value = 'start';
    setTimeout('TimeIncrement()', 1000);
}
function TimeIncrement()
{
    if (document.getElementById('TimeStatus').value == 'start')
    {
        minutes = document.getElementById("minutes");
        seconds = document.getElementById("seconds");
        // if less than a minute remaining
        if (seconds < 59)
        {
            //seconds.value = secs;
            seconds.innerHTML = secs;
        }
        else
        {
            minutes.innerHTML = getminutes();
            seconds.innerHTML = getseconds();
        }
        secs++;
        setTimeout('TimeIncrement()', 1000);
    }
}
function getminutes()
{
    mins = Math.floor(secs / 60);
    return mins;
}

function getseconds()
{
    return secs - Math.round(mins * 60);
}
function stop()
{
    try{
        document.getElementById("minutes").innerHTML = 00;
        document.getElementById("seconds").innerHTML = 00;


        mins = 00;
        secs = 00;
        document.getElementById('TimeStatus').value = '';
    }catch(Exception){}
}