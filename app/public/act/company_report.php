<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$page = "adminpage";
$db = new DB();
$companies = $db->getAllCompanies();

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if($_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        input.submit {
            width: 165px;
            background: url(images/btnb.gif) top center no-repeat;
            height: 30px;
            line-height: 30px;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            text-transform: uppercase;
            color: white;
            text-shadow: 1px 1px 0 #0A5482;
            cursor: pointer;
            margin-right: 10px;
            vertical-align: middle;
        }
        .ui-state-highlight {
            background:#eee !important;
        }
    </style>

</head>





<body>

<div id="hld">

    <div class="wrapper">		<!-- wrapper begins -->


        <?php include_once("include/nav.php"); ?>
        <!-- #header ends -->

        <!-- Filter Box -->
        <div class="block" id="filter-box" style="width:350px; margin:0 auto;margin-bottom: 25px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Run Usage Report
                </h2>
            </div>

            <div class="block_content">

                <table class="filter-calls" style="width:305px !important; max-width:305px !important; margin:0 auto;">
                    <thead>
                    <tr>
                        <th colspan="2"><center><b>Report Details</b></center></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="240" style="border:none;">
                            <label for="start_date">Start:</label> <input type="text" id="start_date" name="start_date" style="width:120px" />
                        </td>
                        <td width="240" style="border:none;">
                            <label for="end_date">End:</label> <input type="text" id="end_date" name="end_date" style="width:120px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border:none;">
                            <label for="company_select">Company:</label>
                            <select id="company_select">
                                <option value="-1">All</option>
                                <?php
                                foreach($companies as $company)
                                {
                                    ?>                            <option value="<?php echo $company['idx']?>"><?php echo $company['company_name']?></option><?php echo "\n";
                                }
                                ?>
                            </select>

                        </td>
                    </tr>

                    <tr>
                        <td style="border-spacing:0px; padding:0px; padding-top:10px; border-bottom:none;" class="no_style_td" colspan="2">
                            <table id="rounded-corner" width="100%" height="100%" style="margin-bottom: 0;">
                                <tr>
                                    <td style="border:none; width: 316px;" class="no_style_td button_space"></td>
                                    <td style="padding:0px; border:none;" class="no_style_td"><input id="SYSTEM_GEN_COMPANY_REPORT" class="submit long" type="submit" value="Run Usage Report"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="font-size:10px; border-bottom: none;"><br><center>*Note: This report will take time to generate</center></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>		<!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>		<!-- .block ends -->



        <!-- Result Box -->
        <div class="block" id="results-box" style="display:none !important;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                Company Report &nbsp; &nbsp; <a href="#" id="SYSTEM_RESET_COMPANY_REPORT">RESET FILTER</a>
                </h2>
                <div style="float:right;"><a href="#" id="export_btn"><img src="images/excel_img.png" width="50" alt="Export as CSV" title="Export as CSV" /></a></div>
            </div>		<!-- .block_head ends -->


            <div class="block_content">
                <iframe id="export" src="" style="display:none;"></iframe>
                <table id="dataTable" cellpadding="0" cellspacing="0" width="100%" class="sortable">
                    <thead>
                    <tr>
                        <th>Number</th>
                        <th>Minutes</th>
                        <th>Twilio Charge</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>

            </div>		<!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>		<!-- .block ends -->
        <?php include "include/footer.php"; ?>

        <script type="text/javascript">
            $(document).ready(function(e){
                $("#export_btn").click(function(e){
                    var s_date = $("#start_date").datepicker("getDate").getTime();
                    var e_date = $("#end_date").datepicker("getDate").getTime();
                    var company = $("#company_select").val();
                    $("#export").attr("src","include/excel_export_company_record.php?company="+company+"&s_date="+s_date+"&e_date="+e_date+"&type=company_report");
                });
            });
        </script>
</body>
</html>