<?php
require_once('include/config.php');
require_once('include/util.php');
require_once('include/Services/Twilio.php');
require_once('include/twilio_header.php');

header('Content-type: text/xml');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

$db = new DB();
smsin();
global $RECORDINGS;

$_REQUEST['From'] = str_replace(" ","+",urldecode($_REQUEST['From']));
$_REQUEST['To']   = str_replace(" ","+",urldecode($_REQUEST['To']));
$_REQUEST['From'] = str_replace("*67", "", $_REQUEST['From']);

if(substr($_REQUEST['From'],0,1)=="+"){
    // +1 772 2631634
    $caller_num = substr($_REQUEST['From'],1,strlen($_REQUEST['From'])-1);
    $forward_num = $db->check_call_forward(substr(trim($_REQUEST['To']),1,strlen(trim($_REQUEST['To']))-1),$caller_num);
}
else{
    $caller_num = trim($_REQUEST['From']);
    $forward_num = $db->check_call_forward(substr(trim($_REQUEST['To']),0,strlen(trim($_REQUEST['To']))),$caller_num);
}

// Start new Call Logic
if (substr($_REQUEST['To'], 0, 2) == "+1")
    $company_id = $db->getCompanyOfNumber(substr($_REQUEST['To'], 2, strlen($_REQUEST['To']) - 1));
else
    $company_id = $db->getCompanyOfNumber(substr($_REQUEST['To'], 1, strlen($_REQUEST['To']) - 1));

if(!empty($forward_num[0]['forward_number'])){
    if($company_id!=FALSE)
        $caller_id = $db->getCallerId($company_id);
    else
        $caller_id = "";

    if($caller_id!="")
        $caller_id = ' callerId="'.$caller_id.'"';

    echo '<?xml version="1.0" encoding="UTF-8"?>';
    ?>
<Response>
    <Dial method="POST" action="record_call.php?test=c2&DialCallTo=<?php echo urlencode($forward_num[0]['forward_number']); ?>"<?php echo $caller_id; ?>>
        <Number><?php echo $forward_num[0]['forward_number']; ?></Number>
    </Dial>
</Response>
<?php
    die();
}

$response = new Services_Twilio_Twiml;

if(isset($_REQUEST['SMSTo']))
{
    require_once('include/ad_auto_dialer_files/lib/ad_lib_funcs.php');
    try {
        $client = new Services_Twilio($AccountSid, $AuthToken);
        $client->account->messages->sendMessage($_REQUEST['To'], $_REQUEST['SMSTo'], html_entity_decode($_REQUEST['Body']));
    }
    catch (Exception $e) {
        
    }                
}else{
    if($company_id==FALSE){
        $response->reject(array("reason"=>"busy"));
    }else{
        if ($db->isNumberBlocked($company_id, @$_REQUEST['From']))
        {
            $response->reject(array("reason"=>"busy"));
        }else{
            $company_settings = $db->getCompanySettings($company_id);
            $caller_id = $db->getCallerId($company_id);

            if ($company_settings->call_flow == 4) $_REQUEST['CallStatus'] = "IVR";

            $db->save_call();

            switch($company_settings->call_flow)
            {
                case 0:{
                    $response->reject(array("reason"=>"busy"));
                    break;
                }
                case 1:{
                    $number = $company_settings->assigned_number;

                    //echo date('d-m-y H:i', strtotime('now')); echo "===";
                    //echo $number  .'======'.$company_settings->opt_hours ."=====";

                    $openNumber = $company_settings->assigned_number;
                    if($company_settings->opt_hours =='1' )
                    {
                        $closeNumber = $company_settings->close_number;
                        $number =$db->getNumberToDial($company_id ,$openNumber,$closeNumber);
                    }

                    $ring_count = $company_settings->ring_count;
                    if (empty($ring_count)) $ring_count = 60;

                    $international = ($number == @$openNumber) ? $company_settings->international : $company_settings->international_closed;
                    $voicemail = ($number == @$openNumber) ? $company_settings->voicemail : $company_settings->voicemail_closed;

                    if($international==1)
                        $number = "+".$number;
                    else
                        $number = "+1".$number;

                    $vm_data = "";
                    if($voicemail==1)
                    {
                        $voicemail = true;
                        $vm_data = "&company_id=$company_id&voicemail=1";
                    }

                    if ((empty($number) || $number == "+1" || $number == "+") && ($company_settings->voicemail == 1 || $company_settings->voicemail_closed == 1)) {
                        $response->redirect(dirname(s8_get_current_webpage_uri()).'/leave_a_message.php?exten='.$company_id);
                    }
                    else {
                        $ring_tone = Util::getRingTone();

                        $dial_actions = array(
                            'action' => dirname(s8_get_current_webpage_uri()).'/record_call.php?test=c2&DialCallTo='.urlencode($number). @$vm_data,
                            'method' => 'POST',
                            'record' => ($company_settings->record_from_ring == 1) ? 'record-from-ringing' : 'true',
                            'timeout'=> $ring_count,
                            'ringTone' => $ring_tone
                        );

                        $caller_id = $db->getCallerId($company_id);
                        if($caller_id!="")
                            $dial_actions['callerId'] = $caller_id;

                        if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
                            $dial_actions['record'] = 'false';

                        if($company_settings->recording_notification!="")
                        {
                            if($company_settings->recording_notification != ""){
                                if ($company_settings->recording_notification_type == "Text") {
                                    $language = $company_settings->recording_notification_language;
                                    $voice = $company_settings->recording_notification_voice;
                                    $voice = str_replace("|M", "", str_replace("|W", "", $voice));

                                    if (empty($language))
                                        $response->say($company_settings->recording_notification);
                                    else
                                        $response->say($company_settings->recording_notification, array("language" => $language, "voice" => $voice));
                                }
                                else {
                                    if (substr($company_settings->recording_notification, 0, 4) != "http") {
                                        $company_settings->recording_notification = dirname(s8_get_current_webpage_uri())."/audio/".$company_id."/".$company_settings->recording_notification;
                                    }
                                    $response->play($company_settings->recording_notification);
                                }
                            }
                        }

                        if($company_settings->whisper!=""){
                            $dial = $response->dial(NULL,$dial_actions);
                            $dial->number($number,array('url'=>'whisper.php?num='.urlencode($_REQUEST['To'])));
                        }else{
                            $dial = $response->dial($number,$dial_actions);
                        }
                    }
                    break;
                }
                case 2:{
                    $last_idx = $company_settings->last_roundrobin;

                    $forward_sec = $company_settings->forward_sec;
                    $forward_number = $company_settings->forward_number;
                    if ( !is_numeric($forward_sec)) $forward_sec= '30';

                    $numbers = $db->customExecute("SELECT `number` FROM cf_round_robin WHERE company_id = ? GROUP BY idx ASC");
                    $numbers->execute(array($company_id));
                    $numbers = $numbers->fetchAll(PDO::FETCH_OBJ);

                    if ( count($numbers) == 1 ) $forward_flg= true; else $forward_flg= false;
                    $f_number ='';
                    if ( $forward_flg && trim($forward_number) != '')
                    {
                        $forward_number = "+".$forward_number;
                        $f_number = '&f_number='.urlencode($forward_number).'&to='.$forward_sec;
                        if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
                            $f_number = $f_number . '&rc=false';
                        else
                            $f_number = $f_number . '&rc=true';
                    }

                    if(count($numbers)==0)
                    {
                        $response->reject(array("reason"=>"busy"));
                    }else{
                        if($last_idx==NULL)
                        {
                            $this_idx = 0;
                            $number = $numbers[0];
                            $number = "+".$number->number;
                        }else{
                            $this_idx = $last_idx+1;
                            $number = @$numbers[$this_idx];
                            if(@$number->number!=NULL){
                                $number = "+".$number->number;
                            }else{
                                $this_idx = 0;
                                $number = $numbers[0];
                                $number = "+".$number->number;
                            }
                        }

                        $prev_num = $db->getPreviousOutgoingNumbertoCall($_REQUEST['To'],$_REQUEST['From'], $company_id);

                        if($prev_num != false && $_REQUEST['From']!="+266696687" && $_REQUEST['From']!="266696687")
                            $number = $prev_num;

                        $ring_tone = Util::getRingTone();

                        $dial_actions = array(
                            'action' => dirname(s8_get_current_webpage_uri()).'/record_call.php?test=c2&DialCallTo='.urlencode($number). ($f_number),
                            'method' => 'POST',
                            'record' => ($company_settings->record_from_ring == 1) ? 'record-from-ringing' : 'true',
                            'ringTone' => $ring_tone
                        );

                        if($forward_sec!="0")
                            $dial_actions['timeout'] = $forward_sec;

                        $caller_id = $db->getCallerId($company_id);
                        if($caller_id!="")
                            $dial_actions['callerId'] = $caller_id;

                        if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
                            $dial_actions['record'] = 'false';

                        if($company_settings->recording_notification!="")
                        {
                            if($company_settings->recording_notification != ""){
                            if ($company_settings->recording_notification_type == "Text") {
                                $language = $company_settings->recording_notification_language;
                                $voice = $company_settings->recording_notification_voice;
                                $voice = str_replace("|M", "", str_replace("|W", "", $voice));

                                if (empty($language))
                                    $response->say($company_settings->recording_notification);
                                else
                                    $response->say($company_settings->recording_notification, array("language" => $language, "voice" => $voice));
                            }
                            else {
                                if (substr($company_settings->recording_notification, 0, 4) != "http") {
                                    require_once('include/twilio_header.php');
                                    $company_settings->recording_notification = dirname(s8_get_current_webpage_uri())."/audio/".$company_id."/".$company_settings->recording_notification;
                                }
                                $response->play($company_settings->recording_notification);
                            }
                        }
                        }

                        if($company_settings->whisper!=""){
                            $dial = $response->dial(NULL,$dial_actions);
                            $dial->number($number,array('url'=>'whisper.php?num='.urlencode($_REQUEST['To'])));
                        }else{
                            $dial = $response->dial($number,$dial_actions);
                        }

                        $stmt = $db->customExecute("UPDATE companies SET last_roundrobin = ? WHERE idx = ?");
                        $stmt->execute(array($this_idx,$company_id));
                    }
                    break;
                }
                case 3:{
                    $numbers = $db->customExecute("SELECT `number` FROM cf_multiple_numbers WHERE company_id = ? GROUP BY idx ASC");
                    $numbers->execute(array($company_id));
                    $numbers = $numbers->fetchAll(PDO::FETCH_OBJ);
                    if(count($numbers)==0)
                    {
                        $response->reject(array("reason"=>"busy"));
                    }else{

                        $ring_tone = Util::getRingTone();
                        $dial_actions = array(
                            'action' => dirname(s8_get_current_webpage_uri()).'/record_call.php?test=c2&DialCallTo='.urlencode($number),
                            'method' => 'POST',
                            'record' => ($company_settings->record_from_ring == 1) ? 'record-from-ringing' : 'true',
                            'timeout'=> '60',
                            'ringTone' => $ring_tone
                        );
                        $caller_id = $db->getCallerId($company_id);
                        if($caller_id!="")
                            $dial_actions['callerId'] = $caller_id;

                        if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
                            $dial_actions['record'] = 'false';

                        if($company_settings->recording_notification!="")
                        {
                            if($company_settings->recording_notification != ""){
                                if ($company_settings->recording_notification_type == "Text") {
                                    $language = $company_settings->recording_notification_language;
                                    $voice = $company_settings->recording_notification_voice;
                                    $voice = str_replace("|M", "", str_replace("|W", "", $voice));

                                    if (empty($language))
                                        $response->say($company_settings->recording_notification);
                                    else
                                        $response->say($company_settings->recording_notification, array("language" => $language, "voice" => $voice));
                                }
                                else {
                                    if (substr($company_settings->recording_notification, 0, 4) != "http") {
                                        require_once('include/twilio_header.php');
                                        $company_settings->recording_notification = dirname(s8_get_current_webpage_uri())."/audio/".$company_id."/".$company_settings->recording_notification;
                                    }
                                    $response->play($company_settings->recording_notification);
                                }
                            }
                        }

                        $prev_num = $db->getPreviousOutgoingNumbertoCall($_REQUEST['To'],$_REQUEST['From'], $company_id);
                        
                        $dial = $response->dial(NULL,$dial_actions);

                        if($prev_num != false && $_REQUEST['From']!="+266696687" && $_REQUEST['From']!="266696687")
                        {
                            $dial->number($prev_num,array('url'=>'whisper.php?num='.urlencode($_REQUEST['To'])));
                        }else{
                            foreach($numbers as $number)
                            {
                                $numbertoDial = $number->number;
                                if (substr($numbertoDial, 0, 1) != "+")
                                    $numbertoDial = "+".$numbertoDial;

                                $dial->number($numbertoDial,array('url'=>'whisper.php?num='.urlencode($_REQUEST['To'])));
                            }
                        }
                    }
                    break;
                }
                case 4:{

                    header('location:ivrmenu.php?company_id='.$company_id);
                    exit;
                }
                case 6:{

                    $number = $company_settings->assigned_number;
                    if($company_settings->international==1)
                        $number = "+".$number;
                    else
                        $number = "+1".$number;

                    if($company_settings->voicemail==1)
                    {
                        $voicemail = true;
                        $vm_data = "&company_id=$company_id&voicemail=1";
                    }

                    
                    $dial_actions = array(
                        'action' => dirname(s8_get_current_webpage_uri()).'/record_call.php?test=c2&DialCallTo='.urlencode($number). $vm_data,
                        'method' => 'POST',
                        'record' => 'true'
                    );
                    $caller_id = $db->getCallerId($company_id);
                    if($caller_id!="")
                        $dial_actions['callerId'] = $caller_id;

                    if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
                        $dial_actions['record'] = 'false';

                    if($company_settings->recording_notification!="")
                    {
                        if($company_settings->recording_notification != ""){
                            if ($company_settings->recording_notification_type == "Text") {
                                $language = $company_settings->recording_notification_language;
                                $voice = $company_settings->recording_notification_voice;
                                $voice = str_replace("|M", "", str_replace("|W", "", $voice));

                                if (empty($language))
                                        $response->say($company_settings->recording_notification);
                                    else
                                        $response->say($company_settings->recording_notification_l, array("language" => $language, "voice" => $voice));
                            }
                            else {
                                if (substr($company_settings->recording_notification, 0, 4) != "http") {
                                    require_once('include/twilio_header.php');
                                    $company_settings->recording_notification = dirname(s8_get_current_webpage_uri())."/audio/".$company_id."/".$company_settings->whisper;
                                }
                                $response->play($company_settings->recording_notification);
                            }
                        }
                    }
                    
                    if($company_settings->sip_msg!='')
                    {
                        $response->say($company_settings->sip_msg);
                    }


                    $sip  = $company_settings->sip_endpoint;
                    if($company_settings->sip_header!='')
                    {
                        $sip  = 'sip:' .$sip. '?'.$company_settings->sip_header;
                    }
                    $sipOption = array ();
                    if($company_settings->sip_username!='')
                    {
                        $sipOption['username']= $company_settings->sip_username;
                        $sipOption['password']= $company_settings->sip_password;
                    }
                    $dial = $response->dial(NULL,$dial_actions);

                    $dial->sip($sip, $sipOption);
                    break;
                }
            }
        }
    }
}

print $response;

?>
