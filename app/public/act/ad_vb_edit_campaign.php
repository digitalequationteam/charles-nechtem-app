<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_voice_broadcast";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10007)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

$act = (isset($_GET['act']) ? $_GET['act'] : "");

if (  $act == 'uploadMp3' )
{   
    $uploaddir = 'audio/voice_broadcast_files/'.$user_id.'/'; 
    
    @mkdir ('audio/voice_broadcast_files/',0777,true);
    @chmod ('audio/voice_broadcast_files/',0777);
    
    @mkdir ($uploaddir,0777,true);
    @chmod($uploaddir,0777);

    $file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name']));

    if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
    { 
        $filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
    
        echo "$filename"; 
    
    } else {
        echo "error";
    }

    exit();
}

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

//Setting the default value of campaign index to false
$campaign_idx = FALSE;

//If campaign index is supplied in request URL
//Assigning the campaign index to our custom variable
if (isset($_GET['idx']))
    $campaign_idx = $_GET['idx'];

//If campaign index is not set in request url.
//Redirecting the user to campaign listing page to select one campaign and come back to this page.
if ($campaign_idx === FALSE)
    header('location: ' . dirname(s8_get_current_webpage_uri()) . '/ad_ad_campaigns.php');

//Retrieving the campaign data
$campaign_data = ad_vb_get_campaign_data($campaign_idx);


$response = '';
$response_type = '';

//Checking form submission
if (isset($_POST['ad_vb_update_settings'])):

    //General variables
    //Retrieving the name of campaign
    $ad_vb_campaign_name = $_POST['ad_vb_campaign_name'];
    //Retrieving the twilio caller id from form
    $phone_number = $_POST['ad_vb_ac_phone_number'];

    if($campaign_data['when_to_run'] != "draft") {
        global $TIMEZONE;
        //Retrieving the schedule data if campaign is not going to run now.
        try{
            $ad_vb_ac_scheduled_date = new DateTime(
                $_POST['ad_vb_ac_sd_year'] . '/' .
                    ($_POST['ad_vb_ac_sd_month'] < 10 ? '0' . $_POST['ad_vb_ac_sd_month'] : $_POST['ad_vb_ac_sd_month']) . '/' .
                    ($_POST['ad_vb_ac_sd_day'] < 10 ? '0' . $_POST['ad_vb_ac_sd_day'] : $_POST['ad_vb_ac_sd_day']) . ' ' .
                    ($_POST['ad_vb_ac_sd_hour'] < 10 ? '0' . $_POST['ad_vb_ac_sd_hour'] : $_POST['ad_vb_ac_sd_hour']) . ':' .
                    ($_POST['ad_vb_ac_sd_minute'] < 10 ? '0' . $_POST['ad_vb_ac_sd_minute'] : $_POST['ad_vb_ac_sd_minute']) . ':00' .
                    (' ' . $_POST['ad_vb_ac_sd_period'])
                , new DateTimeZone($TIMEZONE));

            $ad_vb_ac_scheduled_date->setTimezone(new DateTimeZone("UTC"));
            $ad_vb_ac_scheduled_date = $ad_vb_ac_scheduled_date->format("U");
        }catch(Exception $ex){
            $ad_vb_ac_scheduled_date = strtotime("now");
        }
    }

    //File variables
    $ad_vb_ac_live_answer_content = $_POST['live_answer_content'];
    $ad_vb_ac_live_answer_type = $_POST['live_answer_type'];
    $ad_vb_ac_live_answer_voice = $_POST['live_answer_voice'];
    $ad_vb_ac_live_answer_language = $_POST['live_answer_language'];
    $ad_vb_ac_voicemail_message_content = $_POST['voicemail_message_content'];
    $ad_vb_ac_voicemail_message_type = $_POST['voicemail_message_type'];
    $ad_vb_ac_voicemail_message_voice = $_POST['voicemail_message_voice'];
    $ad_vb_ac_voicemail_message_language = $_POST['voicemail_message_language'];

    $voicemail_only = isset($_POST['voicemail_only']);
    $live_answer_only = isset($_POST['live_answer_only']);

    $allow_ivr = !empty($_POST['allow_ivr']);
    $ivr_key = $_POST['ivr_key'];
    $ivr_phone_number = $_POST['ivr_phone_number'];

    $sms_message = @$_POST['sms_message'];

    //if files are uploaded
    if (isset($_FILES)):
        //Looping over each file
        foreach ($_FILES as $file_key => $file_details):
            //If no error in currently iterated file
            if ($file_details['error'] == 0):
                //Moving it from temporary location to stable one
                if (move_uploaded_file($file_details['tmp_name'], dirname(__FILE__) . '/uploads/' . $file_details['name'])):
                    //Storing the web accessible URI in variable defined for it dynamically
                    ${$file_key} = dirname(s8_get_current_webpage_uri()) . '/uploads/' . $file_details['name'];
                endif;
            endif;
        endforeach;
    endif;

    if (s8_is_str_blank($ad_vb_ac_live_answer_content) && $campaign_data['type'] == "VoiceBroadcast") :
        $response .= 'Please add Live Answer.<br/>';
        $response_type = 'failure';
        $ad_vb_ac_live_answer_content = '';
    endif;

    if (s8_is_str_blank($ad_vb_ac_voicemail_message_content) && $campaign_data['type'] == "VoiceBroadcast"):
        $response .= 'Please add Voicemail Message.';
        $response_type = 'failure';
        $ad_vb_ac_voicemail_message_content = '';
    endif;

    if(s8_is_str_blank($sms_message) && $campaign_data['type']=="SMSBroadcast")
        $response_type = 'failure';

    if(s8_is_str_blank($ivr_phone_number) && $allow_ivr) {
        $response .= 'Please enter phone number to be dialed.<br/>';
        $response_type = 'failure';
    }


    if (!s8_is_str_blank($phone_number) && !s8_is_str_blank($ad_vb_campaign_name) && $response_type != 'failure'):

        //Defining the voice bradcast campaigns table column name
        $campaign_columns_data = array(
            'campaign_name' => $ad_vb_campaign_name,
            'phone_number' => $phone_number,
            'live_answer_content' => isset($ad_vb_ac_live_answer_content)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_live_answer_content:"",
            'live_answer_type' => isset($ad_vb_ac_live_answer_type)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_live_answer_type:"",
            'live_answer_voice' => isset($ad_vb_ac_live_answer_voice)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_live_answer_voice:"",
            'live_answer_language' => isset($ad_vb_ac_live_answer_language)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_live_answer_language:"",
            'voicemail_message_content' => isset($ad_vb_ac_voicemail_message_content)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message_content:"",
            'voicemail_message_type' => isset($ad_vb_ac_voicemail_message_type)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message_type:"",
            'voicemail_message_voice' => isset($ad_vb_ac_voicemail_message_voice)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message_voice:"",
            'voicemail_message_language' => isset($ad_vb_ac_voicemail_message_language)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message_language:"",
            'voicemail_message' => isset($ad_vb_ac_voicemail_message)&&$ad_vb_ac_when_to_run!="sequence"?$ad_vb_ac_voicemail_message:"",
            'when_to_run' => isset($ad_vb_ac_scheduled_date)?$ad_vb_ac_scheduled_date:$campaign_data['when_to_run'],
            'user' => $_SESSION['user_id'],
            'shared' => isset($_REQUEST['ad_vb_shared'])? 1:0,
            'sms_message' => isset($sms_message)? $sms_message:"",
            "voicemail_only" => $voicemail_only,
            "live_answer_only" => $live_answer_only,
            "allow_ivr" => $allow_ivr,
            "ivr_key" => $ivr_key,
            "ivr_phone_number" => $ivr_phone_number
        );

        //Setting the proceed variable to TRUE
        $proceed = true;

        if($campaign_data['when_to_run'] != "draft" && $campaign_data['calls_status']!="completed"){
            $cron_response = ad_add_cron_task(date('Y-m-d H:'.($_POST['ad_vb_ac_sd_minute'] < 10 ? '0' . $_POST['ad_vb_ac_sd_minute'] : $_POST['ad_vb_ac_sd_minute']) . ':00', $ad_vb_ac_scheduled_date), dirname(s8_get_current_webpage_uri()) . '/ad_cron.php?action=process_campaigns');
        }else{
            $cron_response = json_decode(json_encode(array("result"=>"success")));
        }

        //If cron is failed to add, setting the proceed variable to fales
        if (@$cron_response->result != 'success')
            $proceed = FALSE;

        //If proceed variable to set to true
        if ($proceed == TRUE):

            //Adding the voice broadcast campaign into the system and retrieving its Unique ID from system
            $update_status = ad_vb_update_campaign($campaign_idx, $campaign_columns_data);

            //If insert operation executed successfully
            if ($update_status != FALSE):

                //Setting the success message to be displayed on screen
                $response .= 'Successfully updated campaign. <a href="ad_vb_campaigns.php">Click here</a> to proceed.';
                $response_type = 'success';
                $campaign_data = ad_vb_get_campaign_data($campaign_idx);

            else:

                //Setting the success message to be displayed on screen
                $response .= 'System Error while updating campaign data.';
                $response_type = 'failure';

            endif;

        else:

            //Setting the success message to be displayed on screen
            $response .= '
                Campaign update failed. System failure occurred
                while trying to schedule the campaign.
                ';
            $response_type = 'failure';

        endif;

    else:

        //Adding failure message that will be displayed on screen
        //If campaign name is not blank
        if (s8_is_str_blank($ad_vb_campaign_name))
            $response .= 'Campaign name not provided.<br/>';
        //If phone number is not blank
        if (s8_is_str_blank($phone_number))
            $response .= 'Phone number is not selected.<br/>';
        //If live answer mp3 file is not blank
        if (s8_is_str_blank($ad_vb_ac_live_answer_content) && $campaign_data['type'] == "VoiceBroadcast")
            $response .= 'Live Answer MP3 is not provided.<br/>';
        //If voicemail message mp3 file is not null
        if (s8_is_str_blank($ad_vb_ac_voicemail_message_content) && $campaign_data['type'] == "VoiceBroadcast")
            $response .= 'Voicemail Message MP3 is not provided.<br/>';
        if (s8_is_str_blank($sms_message) && $campaign_data['type']=="SMSBroadcast")
            $response .= 'Please enter your Message!';

        //Setting the response type to failure as all the above messages are failure ones.
        $response_type = 'failure';

    endif;

endif;

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
        <style type="text/css">
            #token_list {
                height: 152px;
                margin-left: 8px;
                width: 21%;
                margin-bottom: 7px;
                padding: 5px;
                background: #fefefe;
                border-radius: 3px;
                border: 1px solid #bbb;
                font-family: Helvetica,"Lucida Grande", Verdana, sans-serif;
                font-size: 14px;
                color: #333;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                outline: none;
            }
            .ui-widget-content {border-radius: 5px;}

            .ui-widget-content a {    position: absolute;
                right: 10px;
                top: 5px;}
        </style>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>

            <?php if($campaign_data['calls_status']=="sequence"){ ?>
            <div class="block">
                <div class="block_head">
                    <div class="bheadl"></div>
                    <div class="bheadr"></div>
                    <h2>API URL to add contacts remotely:</h2>
                </div>
                <div class="block_content">
                    <form _lpchecked="1">
                        <input id="api_url" type="text" class="text big" style="font-weight:normal;margin-bottom: 6px;" value="">
                    </form>
                    <script type="text/javascript">
                        $("#api_url").on('mouseup',function(e) { e.preventDefault(); $(this).select(); } );
                        document.getElementById("api_url").value=document.location.href.split("ad_vb_edit_campaign.php")[0]+'ad_ajax.php?action=AddContact&k=<?php echo urlencode(base64_encode($campaign_data['list_id'])); ?>&phone=&firstname=&lastname=&email=';
                    </script>
                </div>
                <!-- .block_content ends -->
                <div class="bendl"></div>
                <div class="bendr"></div>
            </div>
            <?php } ?>

                <!--Initializing the HTML part that will display the create new campaign page-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2>Messages: Update Campaign "<?php echo ad_vb_get_campaign_name($campaign_idx) ?>"</h2>
                        <ul>
                            <li><a href="ad_vb_add_campaign.php">Add a Campaign</a></li>
                            <li><a href="ad_vb_campaigns.php">Campaigns List</a></li>
                            <li><a href="ad_contactlist_log.php">Contacts</a></li>
                            <li><a href="ad_vb_logs.php">Logs</a></li>
                        </ul>
                    </div>		<!-- .block_head ends -->
                    <div class="block_content">
                        <?php
                        //If response message is of success type
                        if ($response_type == 'success'):
                            //Printing thesuccess message
                            ?>
                            <div class="message success"><p><?php echo $response; ?></p></div>
                            <?php
                        //ELSE if response message is of failure type
                        elseif ($response_type == 'failure'):
                            //Pringitng hte error message
                            ?>
                            <div class="message errormsg"><p><?php echo $response; ?></p></div>
                            <?php
                        endif;
                        ?>
                        <form enctype="multipart/form-data" action="" method="post">
                            <input type="hidden" name="type" id="type" value="<?php echo $campaign_data['type']; ?>"/>
                            <p>
                                <label>Campaign Name:</label><br />
                                <input type="text" class="text big" name="ad_vb_campaign_name" value="<?php echo $campaign_data['campaign_name']; ?>" />
                            </p>
                            <?php if(@$campaign_data['user']=="" || @$campaign_data['user']==@$_SESSION['user_id']) { ?>
                            <p>
                                <input style="display: inline-block !important;" type="checkbox" name="ad_vb_shared" id="ad_vb_shared"<?php if(isset($campaign_data['shared']) && $campaign_data['shared']==1) { echo " checked";} ?>/> <label for="ad_vb_shared">Share this campaign with my companies.</label>
                            </p>
                            <?php } ?>
                            <p>
                                <label>Phone Number:</label><br />
                                <select name="ad_vb_ac_phone_number" class="styled">
                                    <option value="">Select Number</option>
                                    <?php
                                    $numbers = $db->getCompanyNum($_SESSION['sel_co']);
                                    //echo "<pre>";print_r($numbers);
                                    for ($i = 0; $i <= count($numbers) - 1; $i++) {
                                        $num = $numbers[$i];
                                        ?>
                                        <option <?php echo ($campaign_data['phone_number'] == $num) ? ' selected="selected" ' : ''; ?> value="<?php echo $num; ?>"><?php echo $num; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </p>

                            <?php if($campaign_data['type']=="VoiceBroadcast") { ?>
                            <p class="fileupload">
                                <label>Live Answer:</label><br />

                                <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                    <input type="hidden" class="content" name="live_answer_content" value="<?php echo $campaign_data['live_answer_content']; ?>" />
                                    <input type="hidden" class="type" name="live_answer_type" value="<?php echo $campaign_data['live_answer_type']; ?>" />
                                    <input type="hidden" class="voice" name="live_answer_voice" value="<?php echo $campaign_data['live_answer_voice']; ?>" />
                                    <input type="hidden" class="language" name="live_answer_language" value="<?php echo $campaign_data['live_answer_language']; ?>" />
                                     <div class="ivr-Menu-selector" style="display: block">
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($campaign_data['live_answer_type'] == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($campaign_data['live_answer_type'] == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($campaign_data['live_answer_type'] == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($campaign_data['live_answer_type'] == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($campaign_data['live_answer_type'] == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($campaign_data['live_answer_type'] == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($campaign_data['live_answer_type'] == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($campaign_data['live_answer_type'] == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                        </div>
                                      </div>
                                      <div class="ivr-Menu-editor ">
                                        <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                          <div class="ivr-Menu-read-text" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <br>
                                            <div>
                                              <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                                <label class="field-label">
                                                    <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($campaign_data['live_answer_type'] == 'Text')? $campaign_data['live_answer_content']:''); ?></textarea>

                                                    <?php
                                                    $voice = (($campaign_data['live_answer_type'] == 'Text')? $campaign_data['live_answer_voice']:'');
                                                    $language = (($campaign_data['live_answer_type'] == 'Text')? $campaign_data['live_answer_language']:'');
                                                    ?>
                                                    <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                    <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                      <?php
                                                      echo Util::getTwilioVoices($voice);
                                                      ?>
                                                    </select>

                                                    <br clear="all" />

                                                    <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                    <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                      <?php
                                                      echo Util::getTwilioLanguages($voice, $language);
                                                      ?>
                                                    </select>

                                                    <br clear="all" /><br />

                                                    <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                    <script type="text/javascript">
                                                      $(document).ready(function() {
                                                        $("#voice").trigger("change");
                                                      });
                                                    </script>

                                                    <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                                </label>
                                              </fieldset>
                                            </div>
                                            <br>
                                            <br>
                                          </div>
                                          <div class="ivr-audio-upload" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>
                                                
                                                <span class="title" <?php if ( $campaign_data['live_answer_type'] != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                                <?php
                                          echo (($campaign_data['live_answer_type'] == 'Audio')? $campaign_data['live_answer_content']:''); ?>
                                                </strong></span> <br>


                                                <span class="title">Click to select a file: </span>
                                                <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                                <span class="title" id="statusUpload">&nbsp;</span>
                                              </div>

                                            </div>
                                          </div>
                                          <div class="ivr-mp3-url" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>
                                                
                                                <span class="title">
                                                  <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($campaign_data['live_answer_type'] == 'MP3_URL')? $campaign_data['live_answer_content']:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                                  <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                                </span>

                                                <br /><br />

                                                <span class="title" <?php if ( $campaign_data['live_answer_type'] != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                                <?php
                                          echo (($campaign_data['live_answer_type'] == 'MP3_URL')? $campaign_data['live_answer_content']:''); ?>
                                                </strong></span>

                                              </div>


                                            </div>
                                          </div>
                                          <div class="ivr-record-audio" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>

                                                Caller ID:

                                                <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                  <option value="">Select number</option>
                                                  <?php
                                                  $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                                  foreach ($numbers as $number) {
                                                    ?>
                                                      <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                    <?php
                                                  }
                                                  ?>
                                                </select>

                                                Your phone number:

                                                <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                                <br /><br />

                                                <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                                <br /><br />

                                                <span class="title" <?php if ( $campaign_data['live_answer_type'] != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                                    <?php if ($campaign_data['live_answer_type'] == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($campaign_data['live_answer_content'], 'sm'); } ?>
                                                </span>

                                              </div>


                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </fieldset>
                            </p>

                            <div class="fileupload">
                                <label>Voicemail Message:</label><br />

                                <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                                    <input type="hidden" class="content" name="voicemail_message_content" value="<?php echo $campaign_data['voicemail_message_content']; ?>" />
                                    <input type="hidden" class="type" name="voicemail_message_type" value="<?php echo $campaign_data['voicemail_message_type']; ?>" />
                                    <input type="hidden" class="voice" name="voicemail_message_voice" value="<?php echo $campaign_data['voicemail_message_voice']; ?>" />
                                    <input type="hidden" class="language" name="voicemail_message_language" value="<?php echo $campaign_data['voicemail_message_language']; ?>" />
                                    <div class="ivr-Menu-selector" style="display: block">
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($campaign_data['voicemail_message_type'] == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($campaign_data['voicemail_message_type'] == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($campaign_data['voicemail_message_type'] == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($campaign_data['voicemail_message_type'] == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($campaign_data['voicemail_message_type'] == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($campaign_data['voicemail_message_type'] == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                        </div>
                                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                          <?php if ($campaign_data['voicemail_message_type'] == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                          <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($campaign_data['voicemail_message_type'] == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                        </div>
                                      </div>
                                      <div class="ivr-Menu-editor ">
                                        <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                          <div class="ivr-Menu-read-text" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <br>
                                            <div>
                                              <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                                <label class="field-label">
                                                    <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($campaign_data['voicemail_message_type'] == 'Text')? $campaign_data['voicemail_message_content']:''); ?></textarea>

                                                    <?php
                                                    $voice = (($campaign_data['voicemail_message_type'] == 'Text')? $campaign_data['voicemail_message_voice']:'');
                                                    $language = (($campaign_data['voicemail_message_type'] == 'Text')? $campaign_data['voicemail_message_language']:'');
                                                    ?>
                                                    <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                                    <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                                      <?php
                                                      echo Util::getTwilioVoices($voice);
                                                      ?>
                                                    </select>

                                                    <br clear="all" />

                                                    <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                                    <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                                      <?php
                                                      echo Util::getTwilioLanguages($voice, $language);
                                                      ?>
                                                    </select>

                                                    <br clear="all" /><br />

                                                    <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                                    <script type="text/javascript">
                                                      $(document).ready(function() {
                                                        $("#voice").trigger("change");
                                                      });
                                                    </script>

                                                    <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                                </label>
                                              </fieldset>
                                            </div>
                                            <br>
                                            <br>
                                          </div>
                                          <div class="ivr-audio-upload" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>
                                                
                                                <span class="title" <?php if ( $campaign_data['voicemail_message_type'] != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                                <?php
                                          echo (($campaign_data['voicemail_message_type'] == 'Audio')? $campaign_data['voicemail_message_content']:''); ?>
                                                </strong></span> <br>


                                                <span class="title">Click to select a file: </span>
                                                <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                                <span class="title" id="statusUpload">&nbsp;</span>
                                              </div>

                                            </div>
                                          </div>
                                          <div class="ivr-mp3-url" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>
                                                
                                                <span class="title">
                                                  <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($campaign_data['voicemail_message_type'] == 'MP3_URL')? $campaign_data['voicemail_message_content']:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                                  <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                                </span>

                                                <br /><br />

                                                <span class="title" <?php if ( $campaign_data['voicemail_message_type'] != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                                <?php
                                          echo (($campaign_data['voicemail_message_type'] == 'MP3_URL')? $campaign_data['voicemail_message_content']:''); ?>
                                                </strong></span>

                                              </div>


                                            </div>
                                          </div>
                                          <div class="ivr-record-audio" style="display: none;">
                                            <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                            <div class="swfupload-container">
                                              <div class="explanation"> <br>

                                                Caller ID:

                                                <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                  <option value="">Select number</option>
                                                  <?php
                                                  $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                                  foreach ($numbers as $number) {
                                                    ?>
                                                      <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                    <?php
                                                  }
                                                  ?>
                                                </select>

                                                Your phone number:

                                                <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                                <br /><br />

                                                <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                                <br /><br />

                                                <span class="title" <?php if ( $campaign_data['voicemail_message_type'] != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                                    <?php if ($campaign_data['voicemail_message_type'] == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($campaign_data['voicemail_message_content'], 'sm'); } ?>
                                                </span>

                                              </div>


                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </fieldset>
                            </div>

                            <br />

                            <div class="fileupload">
                                <label><input type="checkbox" name="voicemail_only" style="display: inline-block !important;" <?php if ($campaign_data['voicemail_only']) { ?>checked="checked"<?php } ?> /> Voicemail Only <span style="font-size: 11px; color: #939393; font-weight: normal;">(When this option is selected, your Voice Broadcast message will be delivered when ACT detects that a voicemail.)</span></label><br />
                                <label><input type="checkbox" name="live_answer_only" style="display: inline-block !important;" <?php if ($campaign_data['live_answer_only']) { ?>checked="checked"<?php } ?> /> Live Answer Only <span style="font-size: 11px; color: #939393; font-weight: normal;">(When this option is selected, your message will only play when ACT detects that a live person has answered the call. No voicemail will be left.)</span></label><br />

                                <div style="float: left; padding-top: 3px;">
                                    <label><input type="checkbox" name="allow_ivr" id="allow_ivr" style="display: inline-block !important;" <?php if ($campaign_data['allow_ivr'] == 1) { ?>checked="checked"<?php } ?> /> Allow customer to press </label>
                                </div>

                                <div style="float: left;">
                                    <select class="styled" name="ivr_key" style="width: 50px;" onchange="$('#allow_ivr').prop('checked', true);">
                                        <option value="1" <?php if ($campaign_data['ivr_key'] == "1") { ?>selected="selected"<?php } ?>>1</option>
                                        <option value="2" <?php if ($campaign_data['ivr_key'] == "2") { ?>selected="selected"<?php } ?>>2</option>
                                        <option value="3" <?php if ($campaign_data['ivr_key'] == "3") { ?>selected="selected"<?php } ?>>3</option>
                                        <option value="4" <?php if ($campaign_data['ivr_key'] == "4") { ?>selected="selected"<?php } ?>>4</option>
                                        <option value="5" <?php if ($campaign_data['ivr_key'] == "5") { ?>selected="selected"<?php } ?>>5</option>
                                        <option value="6" <?php if ($campaign_data['ivr_key'] == "6") { ?>selected="selected"<?php } ?>>6</option>
                                        <option value="7" <?php if ($campaign_data['ivr_key'] == "7") { ?>selected="selected"<?php } ?>>7</option>
                                        <option value="8" <?php if ($campaign_data['ivr_key'] == "8") { ?>selected="selected"<?php } ?>>8</option>
                                        <option value="9" <?php if ($campaign_data['ivr_key'] == "9") { ?>selected="selected"<?php } ?>>9</option>
                                        <option value="0" <?php if ($campaign_data['ivr_key'] == "0") { ?>selected="selected"<?php } ?>>0</option>
                                        <option value="*" <?php if ($campaign_data['ivr_key'] == "*") { ?>selected="selected"<?php } ?>>*</option>
                                    </select>
                                </div>

                                <div style="float: left; padding-top: 5px; font-weight: bold; padding-right: 6px;">
                                    to dial
                                </div>

                                <div style="float: left;">
                                    <input type="text" name="ivr_phone_number" value="<?php echo $campaign_data['ivr_phone_number']; ?>" class="text small" style="width: 120px;" placeholder="Phone#" onkeyup="$('#allow_ivr').prop('checked', !(this.value == ''));" />
                                </div>

                                <div style="clear: both;"></div>
                                <br />
                            </div>

                            <br />

                            <?php }elseif($campaign_data['when_to_run']!="sequence"){ ?>
                            <p class="ad_vb_ac_sms_message">
                                <label>Message:</label><br />
                                <textarea name="sms_message" maxlength="1600" style="width: 700px; max-width: 700px;"><?php echo (@$campaign_data['sms_message']); ?></textarea>
                                <select id="token_list" name="ad_ad_tokens" size="10">
                                    <option value="[FirstName]">[FirstName]</option>
                                    <option value="[LastName]">[LastName]</option>
                                    <option value="[Email]">[Email]</option>
                                    <option value="[Phone]">[Phone]</option>
                                    <option value="[Address]">[Address]</option>
                                    <option value="[City]">[City]</option>
                                    <option value="[State]">[State]</option>
                                    <option value="[Zip]">[Zip]</option>
                                    <option value="[Website]">[Website]</option>
                                    <option value="[Business]">[Business]</option>
                                </select>
                            </p>

                            <?php } ?>
                            <?php
                            if (strtolower($campaign_data['calls_status']) == 'scheduled'):
                                ?>
                                <p>
                                    <label>When To Run:</label><br />

                                    <input type="radio" disabled="disabled" class="radio left" id="cbdemo4" />
                                    <label class="marginleft10 left lh0" for="cbdemo3">Now</label>

                                    <input name="ad_vb_ac_when_to_run" type="radio" value="scheduled" class="radio left" id="cbdemo4" checked="checked" />
                                    <label class="marginleft10 left lh0" for="cbdemo4">Schedule</label>

                                    <input type="radio" disabled="disabled" class="radio left" id="cbdemo4" />
                                    <label class="marginleft10 left lh0" for="cbdemo5">Save As Draft</label>
                                </p>
                                <script type="text/javascript">
                                    if ($ === undefined) {
                                        $ = jQuery;
                                    }

                                    /**
                                     * This function manages the display property of WHEN TO RUN
                                     * section of form.
                                     * @param {object} selector The js object referencing to selector
                                     * to perform various JS operations
                                     * @returns {undefined} Returns nothing.
                                     */
                                    function manage_ui_when_to_run(selector) {
                                        if ($(selector).attr('value') === 'scheduled' && $(selector).is(':checked')) {
                                            $('#ad_vb_ac_scheduled_date').show()
                                        } else {
                                            $('#ad_vb_ac_scheduled_date').hide()
                                        }
                                    }

                                    //When window finished loading
                                    $(window).load(function() {

                                        //On document load
                                        //Updating the UI
                                        manage_ui_when_to_run($('[name=ad_vb_ac_when_to_run]'));

                                    });

                                    //When any WHEN to RUN radio is clicked
                                    $('[name=ad_vb_ac_when_to_run]').click(function() {
                                        //Updating the UI
                                        manage_ui_when_to_run(this);
                                    });

                                    setInterval(function() {
                                        $.get("ad_ajax.php?action=get_system_time", function(ajax_response) {
                                            $('.ad_vb_sys_time').text(ajax_response);
                                        });
                                    }, 4000);
                                </script>

                                <?php
                                global $TIMEZONE;
                                $set_date = new DateTime("@".$campaign_data['when_to_run']);
                                $set_date->setTimezone(new DateTimeZone($TIMEZONE));
                                //Retrieving the scheduled time
                                $scheduled_time = explode(':', $set_date->format('Y:m:d:g:i:A'));
                                ?>

                                <p id="ad_vb_ac_scheduled_date" style="<?php echo!isset($_POST['ad_vb_ac_when_to_run']) || $_POST['ad_vb_ac_when_to_run'] != 'scheduled' ? 'display: none;' : ''; ?>">
                                    <label>Schedule date (Current System Time: <span class="ad_vb_sys_time"><?php global $TIMEZONE;
                                        $currenttime = new DateTime("now",new DateTimeZone($TIMEZONE));
                                        echo $currenttime->format('m/d Y g:i A T'); ?></span>):</label><br/>
                                    Select:
                                    <select name="ad_vb_ac_sd_month">
                                        <option value="">Month</option>
                                        <?php
                                        for ($i = 1; $i <= 12; $i++):
                                            echo '<option ' . (isset($_POST['ad_vb_ac_sd_month']) && $_POST['ad_vb_ac_sd_month'] == $i ? ' selected="selected" ' : ($scheduled_time[1] == $i ? ' selected="selected" ' : '')) . '  value="' . $i . '">' . $i . '</option>';
                                        endfor;
                                        ?>
                                    </select>/<select name="ad_vb_ac_sd_day">
                                        <option value="">Day</option>
                                        <?php
                                        for ($i = 1; $i <= 31; $i++):
                                            echo '<option ' . (isset($_POST['ad_vb_ac_sd_day']) && $_POST['ad_vb_ac_sd_day'] == $i ? ' selected="selected" ' : ($scheduled_time[2] == $i ? ' selected="selected" ' : '')) . '  value="' . $i . '">' . $i . '</option>';
                                        endfor;
                                        ?>
                                    </select>
                                    &nbsp;
                                    <select name="ad_vb_ac_sd_year">
                                        <option value="">Year</option>
                                        <?php
                                        for ($i = date('Y'); $i <= (date('Y') + 10); $i++):
                                            echo '<option ' . (isset($_POST['ad_vb_ac_sd_year']) && $_POST['ad_vb_ac_sd_year'] == $i ? ' selected="selected" ' : ($scheduled_time[0] == $i ? ' selected="selected" ' : '')) . ' value="' . $i . '">' . $i . '</option>';
                                        endfor;
                                        ?>
                                    </select>
                                    &nbsp;
                                    Hour<select name="ad_vb_ac_sd_hour">
                                        <option value="">Hour</option>
                                        <?php
                                        for ($i = 1; $i <= 12; $i++):
                                            echo '<option ' . (isset($_POST['ad_vb_ac_sd_hour']) && $_POST['ad_vb_ac_sd_hour'] == $i ? ' selected="selected" ' : ($scheduled_time[3] == $i ? ' selected="selected" ' : '')) . '  value="' . $i . '">' . $i . '</option>';
                                        endfor;
                                        ?>
                                    </select>:<select name="ad_vb_ac_sd_minute">
                                        <option value="">Minute</option>
                                        <?php
                                        for ($i = 0; $i <= 59; $i++):
                                            if (is_int($i / 5)):
                                                echo '<option ' . (isset($_POST['ad_vb_ac_sd_minute']) && $_POST['ad_vb_ac_sd_minute'] == $i ? ' selected="selected" ' : ($scheduled_time[4] == $i ? ' selected="selected" ' : '')) . '  value="' . $i . '">' . $i . '</option>';
                                            endif;
                                        endfor;
                                        ?>
                                    </select>
                                    &nbsp;
                                    <select name="ad_vb_ac_sd_period">
                                        <option value="">Period</option>
                                        <option value="AM" <?php echo (isset($_POST['ad_vb_ac_sd_period']) && $_POST['ad_vb_ac_sd_period'] == 'AM' ? ' selected="selected" ' : ($scheduled_time[5] == 'AM' ? ' selected="selected" ' : '')) ?>>AM</option>
                                        <option value="PM" <?php echo (isset($_POST['ad_vb_ac_sd_period']) && $_POST['ad_vb_ac_sd_period'] == 'PM' ? ' selected="selected" ' : ($scheduled_time[5] == 'PM' ? ' selected="selected" ' : '')) ?>>PM</option>
                                    </select>
                                </p>
                                <?php
                            endif;
                            ?>
                            <hr />

                            <p>
                                <input name="ad_vb_update_settings" style="float:left;" type="submit" class="submit small" value="Save" />
                                <input style="float:left;" type="button" class="submit small" value="Cancel" onclick="window.document.location = 'ad_vb_campaigns.php';" />
                            </p>
                        </form>
                    </div>		<!-- .block_content ends -->
                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Exiting the HTML code that was displaying the create new campaign form-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>

        <!--//Notification bar html-->
        <div class="ad_notification" style="font-weight: bold; font-size: 16px;z-index: 999999999;display:none;position:fixed;top:0px; left:0px;width: 100%;padding: 10px;background-color:black;color:white;text-align: center;"></div>
        <script type="text/javascript" language="javascript">
            if ($ === undefined) {
                $ = jQuery;
            }

            /**
             * This function will display the notification message on screen.
             * @param {string} message The message to be displayed in notification bar
             * @returns {undefined}
             */
            function ad_display_message(message) {
                $('.ad_notification').html(message);
                $('.ad_notification').slideDown();
                setTimeout(function() {
                    $('.ad_notification').fadeOut(3000);
                }, 3000);

            }

            /**
             * This function will check whether string is blank or not
             * @param {string} str The value to be checked
             * @returns {Boolean} returns boolean TRUE or FALSE based on check performed.
             */
            function ad_is_str_blank(str) {
                if (str === '' || str === ' ' || str === null || str === undefined) {
                    return true;
                } else {
                    return false;
                }
            }

            //When document is ready to attache events to its elements
            $(document).ready(function() {

                //Attaching the function to submit event of text dial
                $('#ad_vb_test_dial, #ad_vb_dial_numbers').submit(function(e) {

                    //Retrieving the caller id
                    var caller_id = $('[name=ad_vb_caller_id]').val();
                    //Retrieving the number to dial
                    if ($(this).attr('id') === 'ad_vb_test_dial') {
                        var to_num = $('[name=ad_vb_test_dial_number]').val();
                    } else {
                        var to_num = $('[name=ad_vb_bulk_dial_numbers]').val();
                    }

                    //CHecking for blank values
                    if (!ad_is_str_blank(caller_id) && !ad_is_str_blank(to_num)) {

                        //Preparing the ajax URL
                        var ajax_url = '<?php echo dirname(s8_get_current_webpage_uri()); ?>/include/ad_auto_dialer_files/ad_ajax.php?to=' + to_num + '&caller_id=' + caller_id + '&userid=<?php echo @$_SESSION['user_id']; ?>';
                        //Firing the AJAX request
                        $.get(ajax_url, function(ajax_response) {

                            //Displaying the notification
                            ad_display_message(ajax_response);

                        });
                    } else {

                        //If blank value detected
                        //Throwing the error on screen
                        ad_display_message('Either caller ID not selected or No number entered.');

                    }

                    //Preventing the default action of form submission
                    e.preventDefault();
                });

            });

            function showAudioText(obj)
            {


                var audioChoice = $(obj).closest('.ivr-Menu-selector');
                audioChoice.hide();
                audioChoice.parent().children('.ivr-Menu-editor').show();
                var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');

                if ( obj.id  == 'txt' )
                {

                    subDiv.children('.ivr-Menu-read-text').show();

                    //////////////// only to avoid  file button clickable in text area
                    $('[name="uploadfile"]').css('z-index','-1');

                }
                else if ( obj.id  == 'upload_mp3' ) {
                    $('[name="uploadfile"]').css('z-index','2147483583');

                    subDiv.children('.ivr-audio-upload').show();
                    SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');
                    UploadFile(SubObj);
                }
                else if ( obj.id  == 'mp3_url' ) {
                    subDiv.children('.ivr-mp3-url').show();
                }
                else if ( obj.id  == 'record_audio' ) {
                    subDiv.children('.ivr-record-audio').show();
                }

            }

            function CloseButton(obj)
            {
                var audioChoice = $(obj).closest('.ivr-Menu');

                var audioChoiceEditor   = audioChoice.children('.ivr-Menu-editor');
                var audioChoiceSelector = audioChoice.children('.ivr-Menu-selector');

                var subDiv  = audioChoiceEditor.children('.ivr-Menu-editor-padding');

                audioChoiceSelector.show();
                audioChoiceEditor.hide();
                subDiv.children('.ivr-audio-upload').hide();
                subDiv.children('.ivr-Menu-read-text').hide();
                subDiv.children('.ivr-mp3-url').hide();
                subDiv.children('.ivr-record-audio').hide();
            }

            function testVoice(voice, language, text) {
                if ($("#test_voice_iframe_wrapper").length == 0) {
                    $("body").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
                                        '<form id="test_voice_form" action="test_voice.php" method="post" target="test_voice_iframe">' +
                                            '<input type="hidden" name="voice" id="voice" />' +
                                            '<input type="hidden" name="language" id="language" />' +
                                            '<input type="hidden" name="text" id="text" />' +
                                            '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                        '</form>' +
                                        '<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
                                    '</form>'
                    );
                }

                $("#test_voice_iframe_wrapper #voice").val(voice);
                $("#test_voice_iframe_wrapper #language").val(language);
                $("#test_voice_iframe_wrapper #text").val(text);
                $('#test_voice_iframe_wrapper #submit').click()
             }

             var closeBtnHtml = '<a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a>';

             function SaveContent(obj, content_type) {
                var thisBlock = $(obj).closest('.ivr-Menu');
                switch (content_type) {
                    case "Text_mail":
                    {
                        var voice_text = $(thisBlock).find('#readtxt_mail').val();
                        var voice = $(thisBlock).find('#voice').val();
                        var language = $(thisBlock).find('#language').val();
                        $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find('.content').val(voice_text);
                        $(thisBlock).find('.type').val("Text");
                        $(thisBlock).find('.voice').val(voice);
                        $(thisBlock).find('.language').val(language);

                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                        $(thisBlock).find('#txt').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        break;
                    }
                    case "MP3_URL":
                    {

                        var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                        $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find('.content').val(mp3_url);
                        $(thisBlock).find('.type').val("MP3_URL");

                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                        $(thisBlock).find('#mp3_url').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        break;
                    }
                }
            }

            function removeSelectedOption(obj) {
                promptMsg('Are you sure ?', function() {
                  var thisBlock = $(obj).closest('.ivr-Menu');
                  $(thisBlock).find(".ttsMwCloseBtn").remove();

                  $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                  $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                  $(thisBlock).find('.content').val('');
                  $(thisBlock).find('.type').val('');
                  $(thisBlock).find('.voice').val('');
                  $(thisBlock).find('.language').val('');
                });
            }

            function UploadFile(obj)
            {
                var status = $(obj).closest('.explanation').find('#statusUpload');
                var fileNameStatus = $(obj).closest('.explanation').find('#voicefilename');
                var fileNameStatusWrapper = $(obj).closest('.explanation').find('#voicefilenameWrapper');

                new AjaxUpload(obj, {


                    action: 'ad_vb_add_campaign.php?act=uploadMp3',
                    name: 'uploadfile',
                    onSubmit: function(file, ext){
                         if (! (ext && /^(mp3|wma)$/.test(ext))){
                            // extension is not allowed
                            status.text('Only MP3 files are allowed');
                            return false;
                        }
                        (fileNameStatus).html('Uploading...');
                    },
                    onComplete: function(file, response){
                        //On completion clear the status

                        //Add uploaded file to list

                        if(response!="error"){
                            $(fileNameStatusWrapper).css('display' ,'block');
                            $(fileNameStatus).html(response);
                            var thisBlock = $(obj).closest('.ivr-Menu');

                            $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                            $(thisBlock).find('#upload_mp3').addClass('ivr-Menu-Selected');
                            $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                            $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                            $(thisBlock).find('.content').val(response);
                            $(thisBlock).find('.type').val("Audio");
                            $(thisBlock).find('.ivr-Menu-close-button').click();

                            $(thisBlock).find(".ttsMwCloseBtn").remove();
                            $(thisBlock).find('#upload_mp3').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                        } else{
                        }
                    }
                });

            }

            var recordingId = "";
            function recordAudio(obj) {
                if ($(obj).val() == "Call Me") {
                    recordingId = createUUID();
                    var record_from = $(obj).parents('.ivr-Menu').find('#record_from').val();
                    var record_to = $(obj).parents('.ivr-Menu').find('#record_to').val();

                    if (record_from == "") {
                        errMsgDialog("Please select Caller ID.");
                        return false;
                    }

                    if (record_to == "") {
                        errMsgDialog("Please enter your phone number.")
                        return false;
                    }

                    $(obj).val('Stop');

                    if ($("#record_audio_iframe_wrapper").length == 0) {
                        $("body").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
                                            '<form id="record_audio_form" action="../record_audio.php" method="post" target="record_audio_iframe">' +
                                                '<input type="hidden" name="recordingId" id="recordingId" />' +
                                                '<input type="hidden" name="record_from" id="record_from" />' +
                                                '<input type="hidden" name="record_to" id="record_to" />' +
                                                '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                            '</form>' +
                                            '<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
                                        '</form>'
                        );
                    }

                    $("#record_audio_iframe_wrapper #recordingId").val(recordingId);
                    $("#record_audio_iframe_wrapper #record_from").val(record_from);
                    $("#record_audio_iframe_wrapper #record_to").val(record_to);
                    $('#record_audio_iframe_wrapper #submit').click()
                }
                else {
                    $(obj).val('Please wait...');
                    $("#record_audio_iframe").contents().find("#disconnectBtn").click();

                    setTimeout(function() {
                        $.post("admin_ajax_handle.php", { func: "GET_RECORDING", recordingId: recordingId },  function(response) {
                            var thisBlock = $(obj).closest('.ivr-Menu');

                            $(thisBlock).find('#recordedAudioSavedWrapper').css('display' ,'block');
                            if (response) {
                                $(thisBlock).find('#recordedAudioSavedWrapper').html(response.playable);
                                
                                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                                $(thisBlock).find('.content').val(response.url);
                                $(thisBlock).find('.type').val("RECORD_AUDIO");
                                $(thisBlock).find('.ivr-Menu-close-button').click();

                                $(thisBlock).find(".ttsMwCloseBtn").remove();
                                $(thisBlock).find('#record_audio').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                            }
                            else {
                                errMsgDialog('Audio was not recorded, please try again.');
                                $(thisBlock).find('#recordedAudioSavedWrapper').html('');
                            }
                            $(obj).val('Call Me');
                        }, "json");
                    }, 2000);
                }
             }

             function createUUID() {
                // http://www.ietf.org/rfc/rfc4122.txt
                var s = [];
                var hexDigits = "0123456789abcdef";
                for (var i = 0; i < 36; i++) {
                    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
                }
                s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
                s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
                s[8] = s[13] = s[18] = s[23] = "-";

                var uuid = s.join("");
                return uuid;
            }

        </script>
    </body>
</html>
