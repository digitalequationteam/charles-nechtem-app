<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "dial_number";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;
$phone = $_GET['from'];
$phone = trim(str_replace("+1", "", $phone));
$company_id = $db->getCompanyOfNumber($phone);

if(@$_SESSION['sel_co']==NULL)
{
    if($db->isUserInCompany($company_id,$_SESSION['user_id']) or $_SESSION['permission'] >= 1)
    {
        $_SESSION['sel_co'] = $company_id;
        $_SESSION['sel_co_name'] = $db->getCompanyName($company_id);
    }
}elseif($db->isUserInCompany($company_id, $_SESSION['user_id']) or $_SESSION['permission'] >= 1)
{
    $_SESSION['sel_co'] = $company_id;
    $_SESSION['sel_co_name'] = $db->getCompanyName($company_id);
}

//Disabling access if currently browsing user is not ADMIN
if (@$_SESSION['permission'] < 1) {
    die("You are not allowed to do that.");
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == "get_phone_codes") {
        $number_selected = $_REQUEST['number_selected'];
        $company_id = $db->getCompanyOfNumber($number_selected);
        $phone_codes = $db->getPhoneCodes($company_id, 2);

        foreach ($phone_codes as $phone_code) {
            ?>
                <option value="<?php echo $phone_code->idx;?>"><?php echo $phone_code->name;?></option>
            <?php
        }
        exit();
    }
}

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

$response = '';
$response_type = '';

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
        <style type="text/css">
            body {
                overflow: hidden;
            }
            #token_list {
                height: 152px;
                margin-left: 8px;
                width: 21%;
                margin-bottom: 7px;
                padding: 5px;
                background: #fefefe;
                border-radius: 3px;
                border: 1px solid #bbb;
                font-family: Helvetica,"Lucida Grande", Verdana, sans-serif;
                font-size: 14px;
                color: #333;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                outline: none;
            }
            .ui-widget-content {border-radius: 5px;}

            .ui-widget-content a {    position: absolute;
                right: 10px;
                top: 5px;}

            #dialer {
                display: none;
                z-index: 0;
                width: 414px;
                margin: auto;
            }

            .client-ui-tab {
                display: none;
            }

            .client-ui-content {
                width: 414px !important;
            }

            .client-ui-bg-overlay {
                background-color: #1c1e1f !important;
            }

            .client-ui-inset {
                width: 404px !important;
                margin: auto !important;
                background-color: #272728 !important;
                padding-bottom: 5px !important;
                -webkit-border-bottom-right-radius: 3px !important;
                -webkit-border-bottom-left-radius: 3px !important;
                -moz-border-radius-bottomright: 3px !important;
                -moz-border-radius-bottomleft: 3px !important;
                border-bottom-right-radius: 3px !important;
                border-bottom-left-radius: 3px !important;
            }

            #client-make-call {
                height: 138px;
            }

            #call-options {
                border: 0px !important;
                background: #2e2f31 !important;
                padding-bottom: 7px;
            }

            #call-options-summary #summary-call-using {
                background: url(../images/caller_id_icon.png) no-repeat !important;
                background-position: 0 0 !important;
                width: 21px !important;
                height: 19px !important;
            }

            #summary-call-toggle {
                transform: rotate(0deg) !important;
                -webkit-transform: rotate(0deg) !important;
                -moz-transform: rotate(0deg) !important;
                padding-top: 6px !important;
                right: 15px !important;
            }

            #call-options-summary {
                padding-left: 15px !important;
                padding-right: 35px !important;
            }

            #dial-phone-number {
                float: left !important;
                background: url(images/dial_phone_number.png) no-repeat !important;
                width: 393px !important;
                height: 52px !important;
                padding: 0px !important;
                border: 0px !important;
                margin: 0px !important;
                text-align: center !important;
                color: #FFFFFF !important;
                font-size: 20px !important;
                font-weight: normal !important;
                background-size: 393px 52px !important;
            }

            #client-ui-pad {
                width: 398px !important;
                margin-top: 0px;
            }

            .client-ui-button {
                background: url(images/cd_button_bg.png) no-repeat !important;
                width: 129px !important;
                height: 40px !important;
                margin: 1px !important;
                padding: 0px !important;
                padding-top: 14px !important;
                border: 0px !important;
                background-size: 129px 57px !important;
                font-size: 24px !important;
            }

            .client-ui-button:hover {
                background: url(images/cd_button_bg_h.png) no-repeat !important;
                color: #FFFFFF !important;
            }

            #client-ui-status {
                padding: 34px 8px 0px 8px !important;
                background: url(../images/cd_cip_bg.png) no-repeat !important;
                background-position: 15px 32px !important;
                height: 104px !important;
            }

            #client-ui-message {
                padding-left: 91px !important;
                text-align: left !important;
            }

            .client-ui-timer {
                margin-top: 16px !important;
                font-size: 35px !important;
                font-weight: normal !important;
            }

            .client-ui-button .client-ui-button-letters {
                margin-top: 0px;
            }
        </style>

        <script type="text/javascript">
        function JSONuserajaxCall(data) {
            return $.ajax({
                url: "ajax_handle.php",
                type: "POST",
                data: data,
                dataType:"json",
                cache: false
            });
        }
        </script>
    </head>
    <body style="background: #272728 !important;">
        <div id="dialer2" style="width: 414px; margin: auto; top: 0px;">
            <input type='hidden' id='divstatus' value="">
            <input type='hidden' id='callOptionsInput' value="">

            <div class="client-ui-tab open">

                <div class="client-ui-bg-overlay"><!-- leave me alone! --></div>
                <div class="client-ui-inset">
                    <div id="client-ui-tab-status">
                        <div class="client-ui-tab-wedge">
                            <img src="images/callmaker-phone-ico.png" class="callmaker-phone-ico" />
                            <a class="callmaker" href="#dialer"><span class="symbol"></span> Hide</a>
                        </div>
                        <div class="client-ui-tab-status-inner">
                            <div class="mic"></div>
                            <h3 class="client-ui-timer">00:00</h3>
                        </div><!-- .client-ui-tab-status-inner -->
                    </div><!-- #client-ui-tab-status -->
                </div><!-- #client-ui-tab-inset -->

            </div><!-- .client-ui-tab .open -->

            <div class="client-ui-content">
                <div class="client-ui-bg-overlay"><!-- leave me alone! --></div>
                <div class="client-ui-inset">

                    <div id="client-make-call">
                        <form id="make-call-form" action="" method="post" style="padding: 5px;">

                            <div style="height: 80px;">
                                <div id="call-options">
                                    <div id="call-options-summary">
                                        <span id="summary-call-using" class="browser"></span>
                                        <span id="summary-caller-id">Caller ID: <span class="device-number">None</span></span>
                                        <span id="summary-call-toggle">&raquo;</span>
                                    </div>
                                    <div id="call-options-inputs" style="display: none; color: #FFFFFF;">
                                        <label class="field-label" style="display: inline-block;">Caller ID
                                            <?php
                                            $numbers = explode(",", $_REQUEST['numbers']);
                                            ?>
                                            <select name="browserphone_caller_id" id="caller-id-phone-number" style="width: 129px;" onchange="changeCallerId(this);">
                                                <?php
                                                //$numbers = $db->getCompanyNumIntl($_SESSION['sel_co']);
                                                //echo "<pre>";print_r($numbers);
                                                for ($i = 0; $i <= count($numbers) - 1; $i++) {
                                                    $number = $numbers[$i];
                                                    $number = trim(str_replace("+1", "", $number));
                                                    ?>
                                                    <option value="<?php echo $number; ?>" <?php if ($phone == $number) { ?>selected="selected"<?php } ?>><?php echo "+" . $number; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </label>

                                        <label class="field-label" style="display: inline-block;">Using
                                            <select name="browserphone_call_using" id="client-mode-status" style="width: 129px;">
                                                <option value="browser" selected="selected" data-device="[]">Your Computer</option>
                                                <!--<option value="device:1" data-device="{&quot;number&quot;:&quot;(561) 310-5929&quot;,&quot;name&quot;:&quot;Primary Device&quot;}">Device: Primary Device</option>-->
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <fieldset>
                                <input style="float: left;" type="text" name="dial_phone_number" value="" id="dial-phone-number" />
                            </fieldset>
                        </form><!-- #make-call-form -->

                    </div><!-- #client-make-call -->

                    <div id="client-on-call" style="display: block;">
                        <div id="client-ui-status" class="clearfix" style="display:none;">
                            <h2 id="client-ui-message">Ready</h2>
                            <h3 class="client-ui-timer" style="float:left;margin-left:87px;width:25px;">
                                <div id="minutes"></div>
                                <h3>
                                <h3 class="client-ui-timer" style="float:left;margin-left:10px;width:10px;">:</h3>
                                <h3 class="client-ui-timer" style="float:left;width:50px;"><div id="seconds"></div><h3>
                                <input type=hidden id="TimeStatus" value="">

                                <?php
                                if($_SESSION['permission']>=1 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])){
                                ?>
                                <div id="client-ui-phonecode" class="clearfix" style="display:none; margin-top: 5px;margin-left: 0px; clear: both; text-align: center; padding-top: 19px">
                                    <select id="client-phonecode" style="font-size: 12px;">
                                        <option value="0">Select a Phone Code</option>
                                        <?php
                                        if(is_array($_SESSION['sel_co_pc']['codes']) || is_object($_SESSION['sel_co_pc']['codes']))
                                            foreach(@$_SESSION['sel_co_pc']['codes'] as $phone_code){
                                                ?><option value="<?php echo $phone_code->idx;?>"><?php echo $phone_code->name;?></option><?php
                                            }
                                        ?>
                                    </select>
                                </div>
                                <?php
                                }
                                ?>

                        </div>
                        <!-- #client-ui-status -->
                       
                        <div id="client-ui-pad" class="clearfix">
                            <div class="client-ui-button-row">
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">1</div>
                                    <div class="client-ui-button-letters"></div>
                                </div>
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">2</div>
                                    <div class="client-ui-button-letters">abc</div>
                                </div>
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">3</div>
                                    <div class="client-ui-button-letters">def</div>
                                </div>
                            </div>
                            <div class="client-ui-button-row">
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">4</div>
                                    <div class="client-ui-button-letters">ghi</div>
                                </div>
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">5</div>
                                    <div class="client-ui-button-letters">jkl</div>
                                </div>
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">6</div>
                                    <div class="client-ui-button-letters">mno</div>
                                </div>
                            </div>
                            <div class="client-ui-button-row">
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">7</div>
                                    <div class="client-ui-button-letters">pqrs</div>
                                </div>
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">8</div>
                                    <div class="client-ui-button-letters">tuv</div>
                                </div>
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number">9</div>
                                    <div class="client-ui-button-letters">wxyz</div>
                                </div>
                            </div>
                            <div class="client-ui-button-row">
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number asterisk" style="margin-top: 8px;">*</div>
                                    <!--<div class="client-ui-button-letters"></div>-->
                                </div>
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number" style="margin-top: 4px;">0</div>
                                    <!--<div class="client-ui-button-letters"></div>-->
                                </div>
                                <div class="client-ui-button">
                                    <div class="client-ui-button-number" style="margin-top: 4px;">#</div>
                                    <!--<div class="client-ui-button-letters"></div>-->
                                </div>
                            </div>
                            <div class="client-ui-button-row">
                                <div class="client-ui-button"></div>
                                <div class="client-ui-button clear-ui-button-data">
                                    <div class="client-ui-button-number" style="font-size: 20px;">C</div>
                                    <div class="client-ui-button-letters" style="margin-left:-4px;">Clear</div>
                                </div>
                                <div class="client-ui-button"></div>
                            </div>
                        </div><!-- /client-ui-pad -->
                        <div id="client-make-call" style="text-align: center; padding-top: 4px;">
                            <input type="image" src="images/cd_dial_btn.png" id="dial-input-button" name="dial_input_button" style="display: inline-block !important;" />
                        </div>
                        <div id="client-ui-actions" style="text-align: center; padding-top: 4px; display:none;">
                            <input type="image" src="images/cd_hangup_btn.png" id="client-ui-hangup" style="display: inline-block !important;" />
                        </div><!-- #client-ui-actions -->           </div><!-- #client-on-call -->
                </div>
            </div>

        </div>

        <?php
        $to_number = $_GET['to'];
        $to_number = str_replace(array(" ", ")", "(", "-"), array("", "", "", ""), $to_number);

        if (substr($to_number, 0, 1) == "1")
            $to_number = "+".$to_number;
        elseif (substr($to_number, 0, 2) != "+1")
            $to_number = "+1".$to_number;
        ?>

        <script type="text/javascript">
            jQuery(document).ready(function() {
                $("#summary-call-toggle").html('<img src="images/caller_id_dd.png" width="12" height="11" />');
                $("#call-options-summary").click(function() {
                    if ($("#call-options").height() > 50) {
                        $("#summary-call-toggle").html('<img src="images/caller_id_dd.png" width="12" height="11" />');
                    }else {
                        $("#summary-call-toggle").html('<img src="images/caller_id_dd_h.png" width="12" height="11" />');
                    }
                });
                jQuery("#report_logo").hide();
                jQuery("#header").hide();
                $('.callmaker').click();
                $("#dial-phone-number").val('<?php echo str_replace("-", "", $to_number); ?>');
                //$("#dial-input-button").click();

                $("#client-ui-hangup").click(function() {
                    setTimeout(function() {
                        $("#dial-phone-number").val('<?php echo str_replace("-", "", $to_number); ?>');
                    }, 100);
                });
            });
        </script>

        <script type="text/javascript">
            function changeCallerId(el) {
                $('#client-phonecode').html('<option value="0">Select a Phone Code</option>');
                $.get(window.location + "&action=get_phone_codes&number_selected=" + el.value, function(data) {
                    $('#client-phonecode').append(data);
                });                                            
            }
            function setCookie(c_name,value,exdays)
            {
                var exdate=new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
                document.cookie=c_name + "=" + c_value;
            }
            function getCookie2(c_name)
            {
                var i,x,y,ARRcookies=document.cookie.split(";");
                for (i=0;i<ARRcookies.length;i++)
                {
                    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
                    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
                    x=x.replace(/^\s+|\s+$/g,"");
                    if (x==c_name)
                    {
                        return unescape(y);
                    }
                }
            }
        </script>

    </body>
</html>