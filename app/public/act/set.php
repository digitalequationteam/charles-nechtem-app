<?php

header("HTTP/1.0 200 OK");
header("Content-Type: application/json");
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found.</b>");
}

session_start();
require_once('include/util.php');
require_once('include/Services/km.php');
$db = new DB();

if( $_REQUEST['f'] == "key" ){
	if( $_REQUEST['p'] && $_REQUEST['i'] ){
	}
}

if( $_REQUEST['f'] == "sa" ){
	if( $_REQUEST['i'] ){
		$spId = urldecode($_REQUEST['i']);
		$sessionData = array();
		$sessionData['inuse'] = 1;
		$sessionData['lastseen'] = time();
		$ex = $db->insertKeywordPool($sessionData,$spId);
		echo $_REQUEST['callback']. '('.json_encode($ex). ')';
	}
}

if( $_REQUEST['f'] == "get" ){
	$company_id = $_REQUEST['cid'];
	$k = urldecode($_REQUEST['k']);
	$s = urldecode($_REQUEST['s']);
	$m = urldecode($_REQUEST['m']);
	$c = urldecode($_REQUEST['c']);
	$g = urldecode($_REQUEST['g']);

	$referrer = base64_decode($_REQUEST['referrer']);
	$url = base64_decode($_REQUEST['url']);
	$content = urldecode($_REQUEST['content']);

	$curPage = base64_decode($_REQUEST['url']);
	$curNumb = urldecode($_REQUEST['curNumb']);
    $is_good = urldecode($_REQUEST['good']);
    $refid   = urldecode($_REQUEST['refid']);
    $kmid    = ($_REQUEST['kmid']);

    /*$km_api_key = $db->getVar($company_id."[kiss_metrics_api_key]");

    if($km_api_key != "" || $km_api_key != false && $kmid!=""){
        KM::init($km_api_key);
        KM::identify($kmid);
        KM::record("Logged to ACT");
    }*/

	$cadw = $_REQUEST['cadw'];

	if( $s == "google" && $cadw == 1 ){
		$s = "Google Adwords";
		$m = "CPC";
	}elseif( $s == "google" && $g != "" && $g != "undefined" && $g != "-" ){
		$s = "Google Adwords";
		$m = "PPC";
	}elseif( $s == "google" ){
		$s = "Google";
	}elseif( $s == "yahoo" ){
		$s = "Yahoo";
	}elseif( $s == "bing" ){
		$s = "Bing";
	}elseif( $s == "facebook" ){
		$s = "Facebook";
	}elseif( $s == "facebook.com" ){
		$s = "Facebook";
	}elseif( $s == "youtube" ){
		$s = "Youtube";
	}elseif( $s == "youtube.com" ){
		$s = "Youtube";
	}else{
		$s = "Default";
	}

	if($k =='-' || $k == ''){
		$k = '(not provided)';
	}

	$spId = 0;

	if(isset($_COOKIE["_kspid"])){
		$usidContent = explode("|", $_COOKIE['_kspid']);

		if(isset($usidContent[1]))
			$spId 				= $usidContent[1]; // Sp_id saved in cookie
			$cookieSessionId	= $usidContent[2]; // Session Id saved in cookie
	}

	if(isset($_COOKIE['_ncps']) && !empty($_COOKIE['_ncps'])){
		$numbercompany = explode("|", $_COOKIE['_ncps']);
		if( $company_id == $numbercompany[1] ){
			
			$number = $numbercompany[0];
			
			//check if number exsits or not
			if(!$db->checkNumberExsist($number, $company_id)){
				$number = $db->getRandomNumbers($company_id);
			}
			
			$poolSession['number'] = $number;
			setcookie("_ncps", $number."|".$company_id."|".$curPage."|".$s, time()+60*60*24*90);
			//edit cookie page if its different and set the bounce rate cookie or update it accordingly
			if($curPage != $numbercompany[2]){
				setcookie("_ncps", $number."|".$company_id."|".$curPage."|".$s, time()+60*60*24*90);

				$db->updateUrlVisited($spId, $url);
				$db->updateBounceRate($spId);
			}
		}else{
			$number = $db->getRandomNumbers($company_id);
			setcookie("_ncps", $number."|".$company_id."|".$curPage."|".$s, time()+60*60*24*90);
			$poolSession['number'] = $number;
		}
	}else{
		//here we get the source and get the numbers through source and company id
		$number = $db->getRandomNumbers($company_id);
		setcookie("_ncps", $number."|".$company_id."|".$curPage."|".$s, time()+60*60*24*90);
		$poolSession['number'] = $number;
	}

	$sessionData = array();
	$keywordData = array();

	$sessionData['session_id'] = session_id();
	$sessionData['company_id'] = $company_id;
	$sessionData['number'] = $poolSession['number'];
	$sessionData['timestamp'] = time();
	$sessionData['lastseen'] = time()+5;

	$sessionData['is_default_number'] = 0;

	$result  = $db->checkKeywordSpid($k, $spId, $sessionData['session_id']);

	$firstvisit = 0;
	if(empty($number)){
		$number 							= $curNumb;
		$sessionData['number'] 				= $curNumb;
		$default 							= 1;
		$sessionData['is_default_number']	= 1;
		
	}
	
	if( $result==0 && $number){
		$keywordData = array();
		if( empty($k) || $k == '-' ){
			$keywordData['keywords'] 	= '(not provided)';
		}else{
			$keywordData['keywords'] 	= $k;
		}
		$firstvisit = 1;
		$sessionID = NULL;
		
		$spId = $db->insertKeywordPool($sessionData, $sessionID);
		
		//set the usid cookie with spId
		setcookie("_kspid",$k.'|'.$spId.'|'.$sessionData['session_id'], time()+60*60*24*90);

		//inserting url in url_visted table
		
		$db->insertUrls($spId, $url);
		
		$userData['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$userData['host'] = $_SERVER['HTTP_HOST'];
		$userData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$userData['referer'] = $_SERVER['HTTP_REFERER'];

		if(empty($userData['referer'])){
			$userData['referer'] = '--';
		}
        $userData['km_id'] = $kmid;

		$userData['date'] = date("Y-m-d H:i:s");
		$userData['firstvisit'] = 1;
		$userData['sp_id'] = $spId;
		$uresult = $db->InsertUserInfo($userData);
		$keywords = $k;
		$remote_addr = $_SERVER['REMOTE_ADDR'];
		$keywordData['sp_id'] = $spId;
        if(urldecode($_REQUEST['s'])=='-'){
            if($refid=="direct"){
                $m = '(none)';
                $_REQUEST['s'] = "(direct)";
            }else{
                $ref = explode("_",$refid);
                switch($ref[0]){
                    case "g":
                        $_REQUEST['s'] = "google";
                        break;
                    case "y":
                        $_REQUEST['s'] = "yahoo";
                        break;
                    case "b":
                        $_REQUEST['s'] = "bing";
                        break;
                }
                switch($ref[1]){
                    case "paid":
                        $m = 'paid';
                        break;
                    case "local":
                        $m = 'local';
                        break;
                    case "organic":
                        $m = 'organic';
                        break;
                }
            }
        }
		$keywordData['medium'] = $m;
		$keywordData['source'] = urldecode($_REQUEST['s']);
        if($c == '-')
            $c = '(not set)';
		$keywordData['campaign'] = $c;
		$keywordData['gclid'] = $g;
		$keywordData['remote_addr'] = $_SERVER['REMOTE_ADDR'];

		if($db->checkKeywordInfoEntry($keywords, $remote_addr))
		{
			$keywordData['firsthit'] = 0;
		}else{
			$keywordData['firsthit'] = 1;
		}

		$keywordData['referrer'] = $referrer;
		$keywordData['url'] = $url;
		$keywordData['content'] = $content;

		$kresult = $db->insertKeywordInfo($keywordData);
	}

	//preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', $number,  $matches );
	//$number = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
	if(!empty($number) && !isset($default)){
		$arr_num = str_split($number);
		//$number = $number;
	}else{
		$number = '--';
	}

	$result = $number.':'.$spId;

	echo $_REQUEST['callback']. '('.json_encode($result). ')';

}

?>