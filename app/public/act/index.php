<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
//Check for config..
if(file_exists("include/config.php") == false)
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
require_once('include/Pagination.php');
$db = new DB();
global $RECORDINGS;

//Method calls
if(@$_SESSION['permission'] < 1)
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);
else
    $companies = $db->getAllCompanies();

// Operations
if(@$_GET['op'] == "logout")
{
    $db->userLogout($_SESSION['user_id']);
    session_destroy();
    header("Location: login.php");
    exit;
}

if(@$_GET['op'] == "select")
{
    if(@$_SESSION['sel_co']==NULL)
    {
        if($db->isUserInCompany($_GET['co'],$_SESSION['user_id']) or $_SESSION['permission'] >= 1)
        {
            $_SESSION['sel_co'] = $_GET['co'];
            $_SESSION['sel_co_name'] = $db->getCompanyName($_GET['co']);
            header("Location: index.php");
            exit;
        }else{
        ?>
        <script type="text/javascript">
            alert('You don\'t have access to that company. Try again.');
            window.location = "index.php";
        </script>
        <?php
        }
    }elseif($db->isUserInCompany($_GET['co'], $_SESSION['user_id']) or $_SESSION['permission'] >= 1)
    {
        $_SESSION['sel_co'] = $_GET['co'];
        $_SESSION['sel_co_name'] = $db->getCompanyName($_GET['co']);
        header("Location: index.php");
    }
}


//Login Check
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}

if(!isset($_SESSION['sel_co']))
{
    header("Location: companies.php?sel=no");
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

$twilio_numbers=Util::get_all_twilio_numbers();

$outgoing = false;
if(isset($_GET['out']) && $_GET['out']==1)
{
    $outgoing = true;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <style type="text/css">
        #header #nav, #header #nav *, #header #nav * * { z-index: 10000; }
    </style>
	
	<!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <?php if(!$outgoing) { ?>
    <script type="text/javascript">
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            $.each(Web1Graphs.graphTypes, function(i,v){
                if(v.dashboard){
                    console.log("Adding " + v.friendly );
                    $('#graph_types')
                        .append($("<option></option>")
                        .attr("value",v.graph_name)
                        .text(v.friendly));
                }
            });
            if(getCookie2("dashboardGraphIndex")!=null)
            {
                $("#graph_types")[0].selectedIndex = $("#graph_types > option[value='"+getCookie2("dashboardGraphIndex")+"']").index();
            }
            else
                $("#graph_types")[0].selectedIndex = Web1Graphs.dashboardIndex;


            $("#graph_types").change(function(){
                $("#charts").block({
                    message: '<h1 style="color:#fff">Loading...</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .7,
                        color: '#fff !important',
                        'z-index':3
                    }
                });
                var selectedVal = $('#graph_types option:selected').attr('value');
                Web1Graphs.graphTypes[selectedVal].draw_graph("graph",$("#company_id").val());
                setCookie("dashboardGraphIndex",selectedVal,"1");
            });
            $("#graph_types").trigger("change");
        }
    </script>
       <?php }else{ ?>
    <script type="text/javascript">
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            $.each(Web1Graphs.graphTypes, function(i,v){
                if(v.dashboard){
                    console.log("Adding " + v.friendly );
                    $('#graph_types')
                            .append($("<option></option>")
                            .attr("value",v.graph_name)
                            .text(v.friendly));
                }
            });
            if(getCookie2("dashboardGraphIndex")!=null)
            {
                $("#graph_types")[0].selectedIndex = $("#graph_types > option[value='"+getCookie2("dashboardGraphIndex")+"']").index();
            }
            else
                $("#graph_types")[0].selectedIndex = Web1Graphs.dashboardIndex;


            $("#graph_types").change(function(){
                $("#charts").block({
                    message: '<h1 style="color:#fff">Loading...</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .7,
                        color: '#fff !important'
                    }
                });
                var selectedVal = $('#graph_types option:selected').attr('value');
                Web1Graphs.graphTypes[selectedVal].draw_graph("graph",$("#company_id").val(),true);
                setCookie("dashboardGraphIndex",selectedVal,"1");
            });
            $("#graph_types").trigger("change");
        }
    </script>
       <?php } ?>
</head>


<body>
<input type="hidden" id="company_id" value="<?php echo $_SESSION['sel_co']; ?>" />
	
	<div id="hld">
	
		<div class="wrapper">		<!-- wrapper begins -->
	
	
	    <?php include_once("include/nav.php"); ?>
			<!-- #header ends -->
			<div class="block" id="charts">
			
				<div class="block_head">
					<div class="bheadl"></div>
					<div class="bheadr"></div>
					
					<h2>Call stats for:  <span style="color:#777; text-transform: none;"><?php echo $db->getCompanyName($_SESSION['sel_co']); ?></span></h2>

                    <form style="padding:12px 0;">
                        <select class="styled" id="graph_types">

                        </select>
                    </form>
				</div>		<!-- .block_head ends -->
				
				
				
				<div class="block_content" id="days" style="padding: 10px 5px 0;">

                    <div id="graph" style="width: 960px; height: 300px; position: relative; "></div>
					
				</div>		<!-- .block_content ends -->
				
				<div class="bendl"></div>
				<div class="bendr"></div>
			</div>		<!-- .block ends -->
			

			<div class="block">
			
				<div class="block_head">
					<div class="bheadl"></div>
					<div class="bheadr"></div>

                    <h2 id="call_stats" style="width: 750px; cursor: pointer;" title="">
                    <?php
                        $call_stats = "";
                        if(!$outgoing){
                            // create the pagination class
                            $pagination = new Pagination();
                            $page_n = 1;
                            if (isset($_GET['page'])){
                                $page_n = (int) $_GET['page'];
                            }
                            if(strpos($_SERVER['REQUEST_URI'],"page") === false)
                            {
                                $url="//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                $pagination->setLink($url."?page=%s");
                            }else{
                                $url="//".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['page']));
                                $pagination->setLink($url."%s");
                            }
                            $pagination->setPage($page_n);
                            $pagination->setSize(10);

                            $calls = $db->cget_calls($_SESSION['sel_co'],$pagination);

                            $pagination->setTotalRecords($calls[1]);
                            $calls = $calls[0];
                            $calls_count = $db->cget_calls_count($_SESSION['sel_co']);

                            $pooled_calls = 0;

                            foreach ($calls_count as $number) {
                                if (array_key_exists(format_phone($number['CallTo']), $twilio_numbers))
                                    $campaign=$twilio_numbers[format_phone($number['CallTo'])];
                                else
                                    $campaign=$number['CallTo'];

                                $number_details = $db->getOutgoingNumberDetails($db->format_phone_db($number['CallTo']));

                                if ($number_details['pool_id'] == 0) {
                                    $label="call";
                                    if ($number['cnt']>1)
                                        $label="calls";
                                    $call_stats .= "<b>".$number['cnt'] . "</b> $label to <b>".Util::format_phone_us($number['CallTo'])."</b> (<b> $campaign </b>)<br/>";
                                    echo ($number['cnt'] . " $label to <span style=\"color:#777;\">" . Util::escapeString($campaign) . "</span> &nbsp; &nbsp; ");
                                }
                                else {
                                    $pooled_calls = $pooled_calls + $number['cnt'];
                                }
                            }

                            $label="call";
                                if ($pooled_calls>1)
                                    $label="calls";

                            echo ($pooled_calls . " <span style=\"color:#777;\">Pooled ".ucfirst($label)."</span> &nbsp; &nbsp; ");

                        }else{
                            // create the pagination class
                            $pagination = new Pagination();
                            $page_n = 1;
                            if (isset($_GET['page'])){
                                $page_n = (int) $_GET['page'];
                            }
                            if(strpos($_SERVER['REQUEST_URI'],"page") === false)
                            {
                                $url="//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                $pagination->setLink($url."?out=1&page=%s");
                            }else{
                                $url="//".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['page']));
                                $pagination->setLink($url."%s");
                            }
                            $pagination->setPage($page_n);
                            $pagination->setSize(10);

                            $calls = $db->cget_outgoing_calls($_SESSION['sel_co'],$pagination);
                            $pagination->setTotalRecords($calls[1]);
                            $calls = $calls[0];
                            $calls_count = $db->cget_outgoing_calls_count($_SESSION['sel_co']);

                            foreach ($calls_count as $number) {
                                if (array_key_exists(format_phone($number['CallFrom']), $twilio_numbers))
                                    $campaign=$twilio_numbers[format_phone($number['CallFrom'])];
                                else
                                    $campaign=$number['CallFrom'];
                                $label="call";
                                if ($number['cnt']>1)
                                    $label="calls";
                                $call_stats .= "<b>".$number['cnt'] . "</b> outgoing $label from <b>".Util::format_phone_us($number['CallFrom'])."</b> (<b> $campaign </b>)<br/>";
                                echo ($number['cnt'] . " outgoing $label from <span style=\"color:#777;\">" . Util::escapeString($campaign) . "</span> &nbsp; &nbsp; ");
                            }
                        }
                        ?>
                    </h2>
                    <?php
                        if($outgoing)
                            echo '<div style="display:inline-block; float: right;"><h2 style="font-size: 16px;"><a href="index.php">Go To Incoming Calls</a></h2></div>';
                        else{
                            if($_SESSION['permission']>=1 || !$db->isUserOutboundLinksDisabled($_SESSION['user_id'])){
                                echo '<div style="display:inline-block; float: right;"><h2 style="font-size: 16px;"><a href="index.php?out=1">Go To Outgoing Calls</a></h2></div>';
                            }
                        }
                    ?>
				</div>		<!-- .block_head ends -->
				
				
				
				<div class="block_content">
					
						<table cellpadding="0" cellspacing="0" width="100%" class="sortable" style="font:13px 'Helvetica' !important;">
						
							<thead>
								<tr>
									<th>Date</th>
									<th>Campaign</th>
									<th>From</th>
									<th>To</th>
									<th>City, State</th>
                                    <th>Duration</th>
                                    <th>Status</th>
                                    <?php if($outgoing){ ?>
                                    <th>User</th>
                                    <?php } ?>
                                    <?php if($RECORDINGS && $db->isCompanyRecordingDisabled($_SESSION['sel_co'])==false) { ?>
                                    <th>Recording</th>
                                    <?php } ?>
								</tr>
							</thead>
							
							<tbody>

                            <?php
                            if(isset($calls)){
                                if(!$outgoing){
                                    foreach (@$calls as $call) {
                                        if (array_key_exists(format_phone($call['CallTo']), $twilio_numbers))
                                            $campaign=$twilio_numbers[format_phone($call['CallTo'])];
                                        else
                                            $campaign="";

                                        $tz = new DateTimeZone($TIMEZONE);
                                        $date = new DateTime($call['DateCreated']);
                                        $date->setTimezone($tz);

                                        if($call['DialCallStatus']=="")
                                            $call['DialCallStatus'] = "in progress";

                                        if ($call['RecordingUrl']!="")
                                            $recording = $call['RecordingUrl'];
                                        else
                                            $recording = "";

                                        if ($db->getVar("mask_recordings") == "true") {
                                            $recording = Util::maskRecordingURL($recording);
                                        }
                                    ?>
                                    <tr>
                                        <td><span style="display:none;"><?php echo $date->format("U"); ?></span><?php echo "<a href=\"call_detail.php?id=".$call['CallSid']."\" title='Click for call details.'>" . $date->format("D n\/j Y g\:iA")."</a>"; ?></td>
                                        <td><?php echo $campaign == "callTracking"? "Pooled Call" : Util::escapeString($campaign); ?></td>
                                        <td<?php if($db->isNumberBlocked($_SESSION['sel_co'],$call['CallFrom'])) echo " style=\"text-decoration: line-through;\" title=\"This number has been blacklisted.\""; ?>><?php echo format_phone($db->format_phone_db($call['CallFrom'])); ?></td>
                                        <td style="white-space: nowrap;"><?php echo format_phone($db->format_phone_db($call['CallTo'])); ?></td>
                                        <td>
                                            <?php
                                                if($call['FromCity'] == "" && $call['FromState'] == "")
                                                    echo "N/A";
                                                elseif($call['FromCity']=="")
                                                    echo $call['FromState'];
                                                else
                                                    echo $call['FromCity'].", ".$call['FromState'];
                                            ?>
                                        </td>
                                        <td><?php echo Util::formatTime($call['DialCallDuration']); ?></td>
                                        <td><?php echo $call['DialCallStatus']; ?></td>
                                        <?php if($RECORDINGS && $db->isCompanyRecordingDisabled($_SESSION['sel_co'])==false) { ?>
                                        <td style="width: 175px;">
                                            <?php 
                                            echo Util::generateFlashAudioPlayer($recording,"sm");
                                            $otherRecordings = $db->getCallRecordings($call['CallSid']);

                                            if ($otherRecordings) {
                                                ?>
                                                <div style="padding-top: 4px; padding-bottom: 3px;">
                                                    <a href="call_detail.php?id=<?php echo $call['CallSid']; ?>">(<?php echo count($otherRecordings); ?> more recording<?php if (count($otherRecordings) > 1) { ?>s<?php } ?>)</a>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php if($_SESSION['permission'] > 0 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])) { ?>
                                            <select title="Select a Phone Code" style="margin-top: 3px;width: 135px;" class="sid_<?php echo $call['CallSid']; ?>" onChange="$(this).attr('disabled','true'); changePhoneCode('<?php echo $call['CallSid']; ?>',this.options[this.selectedIndex].value,(function(){$('.sid_<?php echo $call['CallSid']; ?>').removeAttr('disabled');}))">
                                                <?php
                                                $phone_codes = $db->getPhoneCodes($db->getCompanyofCall($call['CallSid']));
                                                if($call['PhoneCode']==0)
                                                    echo "<option value='0' selected>None</option>";
                                                else
                                                    echo "<option value='0'>None</option>";

                                                if($phone_codes!=false){
                                                    foreach($phone_codes as $code)
                                                    {
                                                        if($call['PhoneCode']==$code->idx)
                                                            echo "<option value='$code->idx' selected>$code->name</option>";
                                                        else
                                                            echo "<option value='$code->idx'>$code->name</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <?php }else{ ?>
                                                <strong>
                                                    <?php
                                                    if($call['PhoneCode']==0)
                                                        echo "None";
                                                    else{
                                                        $phone_codes = $db->getPhoneCodes($db->getCompanyofCall($call['CallSid']));
                                                        foreach($phone_codes as $phone_code){
                                                            if($call['PhoneCode']==$phone_code->idx)
                                                                echo $phone_code->name;
                                                        }
                                                    }
                                                    ?>
                                                </strong>
                                            <?php } ?>
                                            <div style="display: inline-block !important;"><a href="#" data-companyid="<?php echo $_SESSION['sel_co']; ?>" data-params="<?php echo $call['CallFrom']; ?>" id="USR_ADD_NUM_TO_BLACKLIST"><img style="vertical-align: text-top;" src="images/blacklist_btn.png" alt="Add to blacklist" title="Add number to Blacklist"></a>
                                                <a href="#" id="USR_ADD_NOTE_FROM_LIST" title="Add Note" data-callsid="<?php echo $call['CallSid']; ?>"><img src="images/comment-edit-icon.png" style="width:16px; vertical-align: text-top;"></a></div>
                                        </td>
                                        <?php } ?>
                                    </tr>
                                    <?php }
                                }else{
                                    //Outgoing calls
                                    foreach (@$calls as $call) {
                                        if (array_key_exists(format_phone($call['CallFrom']), $twilio_numbers))
                                            $campaign=$twilio_numbers[format_phone($call['CallFrom'])];
                                        else
                                            $campaign="";

                                        $tz = new DateTimeZone($TIMEZONE);
                                        $date = new DateTime($call['DateCreated']);
                                        $date->setTimezone($tz);

                                        if($call['DialCallStatus']=="")
                                            $call['DialCallStatus'] = "in progress";

                                        if ($call['RecordingUrl']!="")
                                            $recording = $call['RecordingUrl'];
                                        else
                                            $recording = "";

                                        if ($db->getVar("mask_recordings") == "true") {
                                            $recording = Util::maskRecordingURL($recording);
                                        }
                                        ?>
                                    <tr>
                                        <td><span style="display:none;"><?php echo $date->format("U"); ?></span><?php echo "<a href=\"call_detail_outgoing.php?id=".$call['CallSid']."\" title='Click for call details.'>" . $date->format("D n\/j Y g\:iA")."</a>"; ?></td>
                                        <td><?php echo Util::escapeString($campaign); ?></td>
                                        <td style="white-space: nowrap;" <?php if($db->isNumberBlocked($_SESSION['sel_co'],$call['CallFrom'])) echo " style=\"text-decoration: line-through;\" title=\"This number has been blacklisted.\""; ?>><?php echo format_phone($db->format_phone_db($call['CallFrom'])); ?></td>
                                        <td style="white-space: nowrap;"><?php echo format_phone($db->format_phone_db($call['CallTo'])); ?></td>
                                        <td>
                                            <?php
                                            if($call['ToCity'] == "" && $call['ToState'] == "")
                                                echo "N/A";
                                            elseif($call['ToCity']=="")
                                                echo $call['ToState'];
                                            else
                                                echo $call['ToCity'].", ".$call['ToState'];
                                            ?>
                                        </td>
                                        <td><?php echo Util::formatTime($call['CallDuration']); ?></td>
                                        <td><?php echo $call['DialCallStatus']; ?></td>
                                        <td><?php echo $db->getUserName($call['UserSource']); ?></td>
                                        <?php if($RECORDINGS && $db->isCompanyRecordingDisabled($_SESSION['sel_co'])==false) { ?>
                                        <td width="160">
                                            <?php
                                            echo Util::generateFlashAudioPlayer($recording, "sm");
                                            $otherRecordings = $db->getCallRecordings($call['CallSid']);

                                            if ($otherRecordings) {
                                                ?>
                                                <div style="padding-top: 4px; padding-bottom: 3px;">
                                                    <a href="call_detail_outgoing.php?id=<?php echo $call['CallSid']; ?>">(<?php echo count($otherRecordings); ?> more recording<?php if (count($otherRecordings) > 1) { ?>s<?php } ?>)</a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <?php if($_SESSION['permission'] > 0 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])) { ?>
                                            <select title="Select a Phone Code" style="margin-top: 3px;width: 135px;" class="sid_<?php echo $call['CallSid']; ?>" onChange="$(this).attr('disabled','true'); changePhoneCode('<?php echo $call['CallSid']; ?>',this.options[this.selectedIndex].value,(function(){$('.sid_<?php echo $call['CallSid']; ?>').removeAttr('disabled');}),2)">
                                                <?php
                                                $phone_codes = $db->getPhoneCodes($_SESSION['sel_co'],2);
                                                if($call['PhoneCode']==0)
                                                    echo "<option value='0' selected>None</option>";
                                                else
                                                    echo "<option value='0'>None</option>";

                                                if($phone_codes!=false){
                                                    foreach($phone_codes as $code)
                                                    {
                                                        if($call['PhoneCode']==$code->idx)
                                                            echo "<option value='$code->idx' selected>$code->name</option>";
                                                        else
                                                            echo "<option value='$code->idx'>$code->name</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <?php }else{ ?>
                                            <strong>
                                                <?php
                                                if($call['PhoneCode']==0)
                                                    echo "None";
                                                else{
                                                    $phone_codes = $db->getPhoneCodes($db->getCompanyofCall($call['CallSid']));
                                                    foreach($phone_codes as $phone_code){
                                                        if($call['PhoneCode']==$phone_code->idx)
                                                            echo $phone_code->name;
                                                    }
                                                }
                                                ?>
                                            </strong>
                                            <?php } ?>
                                        </td>
                                        <?php } ?>
                                    </tr>
                                        <?php }
                                }
                            } ?>
						</table>



                    <?php $navigation = $pagination->create_links();
                    echo $navigation; ?>
					
				</div>		<!-- .block_content ends -->
				
				<div class="bendl"></div>
				<div class="bendr"></div>
			</div>		
            
            
            <?php
                        
                        
                            // create the pagination class
                            $vm_pagination = new Pagination();
                            $vm_page_n = 1;
                            if (isset($_GET['page_vm'])){
                                $vm_page_n = (int) $_GET['page_vm'];
                            }
                            if(strpos($_SERVER['REQUEST_URI'],"page_vm") === false)
                            {
                                $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                $vm_pagination->setLink($url."?page_vm=%s");
                            }else{
                                $url="http://".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['page_vm']));
                                $vm_pagination->setLink($url."%s");
                            }
                            $vm_pagination->setPage($vm_page_n);
                            $vm_pagination->setSize(10);

                            $voicemails = $db->get_voicemail($_SESSION['sel_co'],$vm_pagination);
                            $vm_pagination->setTotalRecords($voicemails[1]);
							
                            $voicemails = $voicemails[0];
                            $label=" Voicemails";


            if ($voicemails)
			{  ?>
            <div class="block">
			
				<div class="block_head">
					<div class="bheadl"></div>
					<div class="bheadr"></div>

                    <h2 id="call_stats" style="width: 750px; cursor: pointer;" title="">
                    <?php echo $label ?>
                    </h2>
                   <div style="display:inline-block; float: right;"></div>
                   
				</div>		<!-- .block_head ends -->
				
				
				
				<div class="block_content">
					
						<table cellpadding="0" cellspacing="0" width="100%" class="sortable" style="font:13px 'Helvetica' !important;">
						
							<thead>
								<tr>
									<th>Date</th>
                                    <th>Campaign</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>City, State</th>
                                    <th>Duration</th>
                                    <th>Status</th>
                                    <th>Play</th>
                                    <th>&nbsp;</th>
								</tr>
							</thead>
							
							<tbody>

                            <?php
                            if(isset($voicemails)){
                                
								
                                    foreach (@$voicemails as $call) {
                                        

                                        $tz = new DateTimeZone($TIMEZONE);
                                        $date = new DateTime($call['message_date']);
                                        $date->setTimezone($tz);

                                        $campaign="";
                                        $call_details = array();
                                        if($call['CallSid']!="" || isset($call['CallSid'])){
                                            $call_details = $db->getCallDetails($call['CallSid']);

                                            if (array_key_exists(format_phone($call_details['CallTo']), $twilio_numbers))
                                                $campaign=$twilio_numbers[format_phone($call_details['CallTo'])];
                                        }
                                       
                                    ?>
                                    <tr>
                                        <td><?php if($call['CallSid']!=""){ ?>
                                            <a href="call_detail.php?id=<?php echo $call['CallSid']; ?>">
                                            <?php } ?>
                                            <span style="display:none;"><?php echo $date->format("U"); ?></span><?php echo $date->format("D n\/j Y g\:iA") ?>
                                            <?php if($call['CallSid']!=""){ ?>
                                            </a>
                                            <?php } ?>
                                        </td>

                                        <td><?php echo $campaign == "callTracking"? "Pooled Call" : Util::escapeString($campaign); ?></td>

                                        <td><?php echo format_phone($db->format_phone_db($call['message_from'])); ?></td>

                                        <td><?php if(isset($call_details)) echo format_phone($db->format_phone_db($call_details['CallTo']));; ?></td>
                                        <td>
                                            <?php
                                            if(isset($call_details)){
                                                if($call_details['FromCity'] == "" && $call_details['FromState'] == "")
                                                    echo "N/A";
                                                elseif($call_details['FromCity']=="")
                                                    echo $call_details['FromState'];
                                                else
                                                    echo $call_details['FromCity'].", ".$call_details['FromState'];
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo Util::formatTime($call['RecordingDuration']); ?></td>

                                        <td class="message_flag_<?php echo $call['message_id']; ?>"><?php echo  ( $call['message_flag']=='0') ? 'New':'Read'; ?></td>
                                        <td style="width: 175px;">
                                        <?php
                                        $callback = "readVoicemail(".$call['message_id'].")";
                                        echo Util::generateFlashAudioPlayer($call['message_audio_url'],"sm", $callback);
                                        ?>
                                        </td>

                                        <td>
                                            <a href="javascript: void(0);" onclick="confirmVoicemailDeletion(<?php echo $call['message_id']; ?>, this);"><img class="tt" title="Delete Voicemail" width="16" src="images/delete.gif"></a>
                                        </td>
                                        
                                    </tr>
                                    <?php }
                                
                            } ?>
						</table>



                    <?php $navigation = $vm_pagination->create_links();
                    echo $navigation; ?>
					
				</div>		<!-- .block_content ends -->
				
				<div class="bendl"></div>
				<div class="bendr"></div>
			</div>
            
            <!-- .block ends -->


	<?php }
		
		include "include/footer.php"; ?>
        <script type="text/javascript">
            $("#charts").block({
                message: '<h1 style="color:#fff">Loading...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .7,
                    color: '#fff !important',
                    'z-index': 3
                }
            });
            $("#call_stats").tooltipster({
                position:'top',
                theme:'.tooltipster-shadow'
            });
            $("#call_stats").tooltipster("update","<?php echo $call_stats; ?>");

            function confirmVoicemailDeletion(message_id, el) {
                promptMsg('Are you sure you want to delete the voicemail ?', function() {
                    $.blockUI({message: 'Deleting voicemail...', css: {padding: '5px'}});

                    var response = JSONajaxCall({
                        func: 'SYSTEM_DELETE_VOICEMAIL',
                        data: {
                            message_id: message_id
                        } 
                    });
                    
                    $.blockUI({message: 'Deleting voicemail...', css: {padding: '5px'}});
                    response.done(function() {
                        $.unblockUI();
                        $(el).parents('tr').remove();
                    });
                });
            }

            function readVoicemail(message_id) {
                var response = JSONajaxCall({
                    func: 'SYSTEM_READ_VOICEMAIL',
                    data: {
                        message_id: message_id
                    } 
                });

                response.done(function() {
                    $(".message_flag_" + message_id).text('Read');
                });
            }
        </script>
</body>
</html>