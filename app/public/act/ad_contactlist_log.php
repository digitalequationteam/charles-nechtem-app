<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "ad_contactlist";

//Including necessary files
require_once('include/util.php');
require_once('include/Pagination.php');
require_once 'include/twilio_header.php';

if(@$lcl<2){
    header("Location: index.php");
    exit;
}

//Including the library Auto Dialer and Voice Broadcast files
require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

//Initializing the DB object 
$db = new DB();

//Initializing other global variables that are required.
Global $AccountSid, $AuthToken;

if(@$_SESSION['permission']<1 && !$db->checkAddonAccess($_SESSION['user_id'],10008)){
    header("Location: index.php");
    exit;
}

//Checking if currently browsing user is ADMIN or normal USER
if (@$_SESSION['permission'] < 1):

    //If logged in person is a simple user of system, 
    //then, retrieving only the comanies associated with it
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);

else:

    //If logged in person is admin
    //Retrieving all companies
    $companies = $db->getAllCompanies();

//Enditing condition checking logged in user permissions
endif;

//loading the user ID in relative custom variable
$user_id = $_SESSION['user_id'];

//Pre-load Checks
//Checking if user not logged in
if (!isset($_SESSION['user_id'])):

    //Redirecting to login page if not logged in
    header("Location: login.php");

    //Exiting the code as no furthur processing requires
    exit;

//Exiting the condition checking logged in state of user in session
endif;

//Checking if company is set in session cloud
if (!isset($_SESSION['sel_co'])):

    //If not set, then, redirecting the user to compnies page to select one
    header("Location: companies.php?sel=no");

    //Exiting the code as no furhthur processing requires.
    exit;

//Exiting the codition checking company in session cloud
endif;

//Calling the function that will hadle the table creation part if not already created.
ad_db_handle_data_tables();

//By default setting null values to action responses
$action_response = '';
$action_type = '';

//If current request is to perform some action on current page
if (isset($_GET['action'])):

    //Switching to appropriate action code
    switch ($_GET['action']):

    endswitch;

//Enditing condition checking action request
endif;

$order_by = (empty($_REQUEST['order_by']) ? "last_updated" : $_REQUEST['order_by']);
$order_type = (empty($_REQUEST['order_type']) ? "DESC" : $_REQUEST['order_type']);

//Retriving the lists data
$ad_contact_lists = ad_advb_cl_get_contact_lists(isset($_GET['page']) ? $_GET['page'] : '1', 20, false, $order_by, $order_type);

//Calculating the number of lists
$total_list = count($ad_contact_lists);

//Starting the html buffering on screen from here onwards
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <?php include "include/css.php"; ?>
    </head>
    <body>
        <div id="hld">
            <div class="wrapper"<?php if (isset($report_type)) echo " style=\"width:960px\""; ?>>		<!-- wrapper begins -->
                <?php
                //Displaying the navigation menu on page
                include('include/nav.php');
                ?>
                <div class="block bulk_dial_status" style="display: none;background: none;">
                    <div class="block_head"> 
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <h2 style="color:red;" class="ad_ad_bulk_dial_status_text"></h2>
                    </div>
                </div>
                <div class="clear"></div>

                <!--Auto dialer campaigns listing section starts here-->
                <div class="block">
                    <div class="block_head">
                        <div class="bheadl"></div>
                        <div class="bheadr"></div>
                        <ul style="float: right;">
                            <li><a href="ad_contactlist_log.php">Manage Contact List</a></li>
                            <li><a href="ad_contactlist_add.php">ADD Contact List</a></li>
                        </ul>
                        <h2>
                            <?php if($total_list==1) echo("1 Contact List"); else
                    echo ($total_list." Contact Lists"); ?>
                        </h2>
                    </div>
                    <!-- .block_head ends -->
                    <div class="block_content">
                        <?php
                        //Checkin if action message is set or not
                        if (!s8_is_str_blank($action_response) || isset($_GET['m'])):

                            //If action type is not set and message type is set in request URL
                            if (s8_is_str_blank($action_type) && isset($_GET['m']) && isset($_GET['mt'])):
                                $action_response = $_GET['m'];
                                $action_type = $_GET['mt'];
                            endif;

                            //If action message is set, switching to type of action message
                            switch (strtolower($action_type)):

                                //If action message is of success type
                                case 'success':
                                    //Displaying success message
                                    ?>
                                    <div class="message success">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                                //If action message is of failure type
                                case 'failed':
                                    //Displaying failure message
                                    ?>
                                    <div class="message errormsg">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                                //If action message is of failure type
                                case 'warning':
                                    //Displaying failure message
                                    ?>
                                    <div class="message warning">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                                //If action message is of failure type
                                case 'info':
                                    //Displaying failure message
                                    ?>
                                    <div class="message info">
                                        <p><?php echo $action_response; ?></p>
                                    </div>
                                    <?php
                                    break;

                            endswitch;

                        endif;
                        ?>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <?php
                                    $columns = array(
                                        "list_name" => "Group Name",
                                        "contacts_nr" => "Contacts",
                                        "last_updated" => "Last Updated",
                                        "user" => "User"
                                    );

                                    foreach ($columns as $key => $value) {
                                        if ($order_by == $key) { if ($order_type == "ASC") { $next_sort_type = "DESC"; $class = "headerSortDown"; } else { $next_sort_type = "ASC"; $class = "headerSortUp"; } } else { $next_sort_type = "ASC"; $class = ""; }

                                        $url = Util::curPageURL();
                                        $parsed_url = parse_url($url);
                                        parse_str($parsed_url["query"], $output);
                                        $output["order_by"] = $key;
                                        $output["order_type"] = $next_sort_type;

                                        $url = $parsed_url['scheme']."://".$parsed_url["host"].$parsed_url['path']."?".http_build_query($output);
                                        if ($key == "Campaign") {
                                            ?>
                                                <th><?php echo $value; ?></div></td>
                                            <?php
                                        }
                                        else {
                                            ?>
                                                <th style="cursor: pointer;" onclick="window.location = '<?php echo $url;?>';" class="header <?php echo $class; ?>"><?php echo $value; ?></div></td>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //If there exist atleast one contact list in system
                                if ($total_list > 0):

                                    //Looping over contact list to display them
                                    foreach ($ad_contact_lists as $single_list_data):
                                        ?>
                                        <tr>
                                            <td><?php echo Util::escapeString($single_list_data['list_name']); ?></td>
                                            <td><?php echo ad_advb_cl_get_list_contacts_count($single_list_data['idx']); ?></td>
                                            <td><?php echo Util::convertToLocalTZ($single_list_data['last_updated'])->format(Util::STANDARD_LOG_DATE_FORMAT); ?></td>
                                            <td><?php echo ad_get_user($single_list_data['user']); ?></td>
                                            <td>
                                                <a href="ad_contactlist_add.php?list_idx=<?php echo $single_list_data['idx']; ?>"><img src="images/cog_edit.png" width="16" class="tt" title="Edit Contact List"></a>&nbsp;
                                                <a href="#" class="delete_cl" onclick="deleteContactList('<?php echo $single_list_data['idx']; ?>')"><img src="images/delete.gif" class="tt" title="Delete Contact List"></a>
                                            </td>
                                        </tr>
                                        <?php
                                    endforeach;

                                else:
                                    ?>
                                    <tr>
                                        <td>No records found.</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php
                                endif;
                                ?>
                            </tbody>
                        </table>
                        <?php

                        $pagination = new Pagination();
                        $page_n = 1;
                        if (isset($_GET['page'])){
                            $page_n = (int) $_GET['page'];
                        }
                        if(strpos($_SERVER['REQUEST_URI'],"page") === false)
                        {
                            $url="//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                            if (empty($_SERVER['QUERY_STRING']))
                                $pagination->setLink($url."?page=%s");
                            else
                                $pagination->setLink($url."&page=%s");
                        }else{
                            $url="//".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['page']));
                            $pagination->setLink($url."%s");
                        }
                        $pagination->setPage($page_n);
                        $pagination->setSize(20);
                        $pagination->setTotalRecords(ad_advb_cl_get_contact_lists_count());

                        echo $pagination->create_links();
                        ?>
                    </div>		
                    <!-- .block_content ends -->

                    <div class="bendl"></div>
                    <div class="bendr"></div>
                </div>
                <!--Auto dialer campaigns listing ends here-->

                <!-- #header ends -->
                <?php include "include/footer.php"; ?>
            </div>
        </div>
    <script type="text/javascript">
        function deleteContactList(id){
            if (confirm('Are you sure? You are trying to delete a contact list.')) {
                $.get('ad_ajax.php?action=ad_advb_cl_delete_list&list_idx='+id, function(ajax_response) {
                    alert(ajax_response);
                    if (ajax_response.match('SUCCESS') !== null) {
                        window.document.location = 'ad_contactlist_log.php';
                    }
                });
            }
        }
        $(".tt").tooltipster({
            position:'bottom',
            theme:'.tooltipster-shadow'
        });
    </script>
    </body>
</html>