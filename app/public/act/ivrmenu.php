<?php
	$ignore_db_check = true;
	require_once('include/config.php');
	require_once('include/util.php');
	require_once('include/db.php');
	require_once('include/Services/Twilio.php');
	header('Content-type: text/xml');
	$db = new DB();

	$con = mysqli_connect($DB_HOST,$DB_USER,$DB_PASS);
	mysqli_select_db($con, $DB_NAME );
	
	$response = new Services_Twilio_Twiml;
	
	if ( isset ( $_REQUEST['company_id']) && trim ($_REQUEST['company_id'] )!= '')
 		
		$company_id = $_REQUEST['company_id'];
	
	if($company_id==FALSE)
	{
        $response->reject(array("reason"=>"busy"));
    }
	
	$company_settings = $db->getCompanySettings($company_id);
	
    $caller_id = $db->getCallerId($company_id);

    if (isset($_REQUEST['action'])) {
    	if (in_array($_REQUEST['action'], array('put_on_hold', 'decline', 'activate_answer', 'play_hold_music', 'play_hold_music_without_prompt', 'default', 'remove_from_call'))) {
    		$sth = $db->customExecute("SELECT * FROM call_ivr_widget WHERE companyId = ? AND flowtype = 'AgentIVR'");
			$sth->execute(array($company_id));
			$widget = $sth->fetch();
			$_REQUEST['wId'] = $widget['wId'];

			if (empty($widget)) {
				$widget = array(
					"flowtype" => "AgentIVR",
					"content" => json_encode(array(
						"max_queue_size" => 1000,
						"flow_type" => "first_available",
						"hold_music" => array(
							"type" => "MP3_URL",
							"content" => dirname(s8_get_current_webpage_uri())."/mp3/default-hold.mp3"
						),
						"agents" => array(
							array(
								"id" => $_REQUEST['forced_agent_id']
							)
						)
					))
				);
			}
    	}
    }

	if( isset( $_REQUEST['wId']) )
	{
		$getStarted  =	$_REQUEST['wId'];
	}else{
		$getStarted  = $db->getStarted($company_id); 
	}

	if ( $getStarted == '' && empty($widget))
	{
		 $response->reject(array("reason"=>"busy"));
	}
	
	if ( $getStarted )
	{
		$kp ='-';
		if ( @$_REQUEST['kp'] !='' )		$kp  =	$_REQUEST['kp'];
	 	$widget= $db->getWidget($getStarted,$kp);
	}

	switch ($widget['flowtype'])
	{
		case "Hangup":{
			$response->hangup();
			$stmt = $db->customExecute("UPDATE calls set DialCallStatus='completed' WHERE CallSid=?");
			$stmt->execute(array(@$_REQUEST['CallSid']));
			break;
			}
		case "RoundRobin":{
			
			
					
                    $last_idx = $db->customExecute("SELECT `meta_data` FROM call_ivr_data WHERE  meta_key = 'last_idx'  and wId = ? GROUP BY id ASC");
                    $last_idx->execute(array( $widget['wId']));
                    $last_idx = $last_idx->fetch(PDO::FETCH_ASSOC);
					
					if ( !empty($last_idx) ) $last_idx =$last_idx['meta_data']; else $last_idx=NULL;
				
					
					
					
					$timeout = getMetaDetail($widget['nId'], "ring_count");
					if (empty($timeout))
						$timeout= '60';
					  

                    $numbers = $db->customExecute("SELECT `number` FROM call_ivr_round_robin WHERE wId = ? GROUP BY idx ASC");
                    $numbers->execute(array( $widget['wId']));
                    $numbers = $numbers->fetchAll(PDO::FETCH_OBJ);
					
				
					if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
                              $URL_data =  '&rc=false';
						else
						  $URL_data =  '&rc=true';
					
					$URL_data .='&rr=1&company_id='.$company_id.'&wId='. $widget['wId'].'&to='.$timeout;
					if ( $widget['nId'] != '0' && $widget['nId'] != '' )
					{
						$URL_data .='&nextId='. $widget['nId'];
						
					}
					
					
					
						
                    if(count($numbers)==0)
                    {
                        $response->reject(array("reason"=>"busy"));
                    }else{
						
                        if($last_idx==NULL)
                        {
                            $this_idx = 0;
                            $number = $numbers[0];
                            $number = $number->number;
                        }else{
                            $this_idx = $last_idx+1;
                            $number = $numbers[$this_idx];
                            if($number->number!=NULL){
                                $number = $number->number;
                            }else{
                                $this_idx = 0;
                                $number = $numbers[0];
                                $number = $number->number;
                            }
                        }

                        $prev_num = $db->getPreviousOutgoingNumbertoCall($_REQUEST['To'],$_REQUEST['From'], $company_id);

                        if($prev_num != false && $_REQUEST['From']!="+266696687" && $_REQUEST['From']!="266696687")
                            $number = $prev_num;

                        $ring_tone = Util::getRingTone();

                        $dial_actions = array(
                            'action' => dirname(s8_get_current_webpage_uri()).'/record_call.php?DialCallTo='.urlencode($number). $URL_data,
                            'method' => 'POST',
                            'timeout' => $timeout,
							'record' => ($company_settings->record_from_ring == 1) ? 'record-from-ringing' : 'true',
							'ringTone' => $ring_tone
							
                        );
                        $caller_id = $db->getCallerId($company_id);
                        if($caller_id!="")
                            $dial_actions['callerId'] = $caller_id;

                        if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
                            $dial_actions['record'] = 'false';

                        if($company_settings->recording_warning_url!="")
                        {
                            $response->play($company_settings->recording_warning_url);
                        }

                        if($company_settings->whisper!=""){
                            $dial = $response->dial(NULL,$dial_actions);
                            $dial->number($number,array('url'=>'whisper.php?num='.urlencode($_REQUEST['To'])));
                        }else{
                            $dial = $response->dial($number,$dial_actions);
                        }
						
						$stmt = $db->customExecute("delete from call_ivr_data  WHERE wId = ?");
						$stmt->execute(array( $widget['wId']));
						
                        $stmt = $db->customExecute("insert call_ivr_data SET meta_key = 'last_idx', meta_data = '$this_idx', wId = ?");
                        $stmt->execute(array( $widget['wId']));
					
                    }

                    if ( $widget['nId'] != '0'){
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['nId'].'&tcx='.strtotime('now'));
					}

                    break;
                
		}
		case "MultipleNumbers":
		{
					$numbers = $db->customExecute("SELECT `number` FROM call_ivr_multiple_numbers WHERE wId = ? GROUP BY idx ASC");
                    $numbers->execute(array($widget['wId']));
                    $numbers = $numbers->fetchAll(PDO::FETCH_OBJ);
                    if(count($numbers)==0)
                    {
                        $response->reject(array("reason"=>"busy"));
                    }else{

                    	$timeout = getMetaDetail($widget['nId'], "ring_count");
						if (empty($timeout))
							$timeout= '60';
						
						$nextURL ='';
						if ( $widget['nId'] != '0' && $widget['nId'] != '' ){
							$nxtURL ='&company_id='.$company_id.'&wId='. $widget['nId'];
						}
				
                        $ring_tone = Util::getRingTone();
                        $dial_actions = array(
                            'action' => dirname(s8_get_current_webpage_uri()).'/record_call.php?DialCallTo='.urlencode($number).$nxtURL,
                            'method' => 'POST',
                            'record' => ($company_settings->record_from_ring == 1) ? 'record-from-ringing' : 'true',
                            'timeout' => $timeout,
							'ringTone' => $ring_tone
                        );
                        $caller_id = $db->getCallerId($company_id);
                        if($caller_id!="")
                            $dial_actions['callerId'] = $caller_id;

                        if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
                            $dial_actions['record'] = 'false';

                        if($company_settings->recording_warning_url!="")
                        {
                            $response->play($company_settings->recording_warning_url);
                        }

                        $dial = $response->dial(NULL,$dial_actions);

                        $prev_num = $db->getPreviousOutgoingNumbertoCall($_REQUEST['To'],$_REQUEST['From'], $company_id);

                        if($prev_num != false && $_REQUEST['From']!="+266696687" && $_REQUEST['From']!="266696687")
                        {
                            $dial->number($prev_num,array('url'=>'whisper.php?num='.urlencode($_REQUEST['To'])));
                        }else{
                            foreach($numbers as $number)
                            {
								$numbertoDial = $number->number;
                                if (substr($numbertoDial, 0, 1) != "+")
                                    $numbertoDial = "+".$numbertoDial;
									
                                $dial->number($numbertoDial,array('url'=>'whisper.php?num='.urlencode($_REQUEST['To'])));
                            }
                        }
                    }

                    if ( $widget['nId'] != '0'){
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['nId'].'&tcx='.strtotime('now'));
					}
			
			break;
		}
		
		case "SMS":{
            require_once('include/ad_auto_dialer_files/lib/ad_lib_funcs.php');
            $sms_delay = getMetaDetail($widget['wId'], "sms_delay");
            if($sms_delay=="" || $sms_delay == 0) {
            	//$response->message(html_entity_decode($widget['content']));

            	try {
	            	$client = new Services_Twilio($AccountSid, $AuthToken);
	                $client->account->messages->sendMessage($_REQUEST['To'], $_REQUEST['From'], html_entity_decode($widget['content']));
                }
                catch (Exception $e) {
                	
                }
            }
			else{
                $scheduled_date = new DateTime("+".$sms_delay." minutes");
                $retn = ad_add_cron_task($scheduled_date->format("Y-m-d H:i:s"),dirname(s8_get_current_webpage_uri()) . '/ad_cron.php?action=process_call_flow_sms&wId='.
                    $widget['wId']."&To=".urlencode($_REQUEST['From'])."&From=".urlencode($_REQUEST['To']));
            }

			if ( $widget['nId'] == '0'){
				$response->hangup();
				$stmt = $db->customExecute("UPDATE calls set DialCallStatus='completed' WHERE CallSid=?");
				$stmt->execute(array(@$_REQUEST['CallSid']));
			}else{
				$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['nId'].'&tcx='.strtotime('now'));
		}
				
				
				break;
		}
		case "Greetings":{
			
				if ( $widget['content_type']  == 'Text' ) {
					$language = getMetaDetail($widget['wId'], 'language');
					$voice = getMetaDetail($widget['wId'], 'voice');
					$language = str_replace("|M", "", str_replace("|W", "", $language));

					if (empty($language))
						$response->say($widget['content']);
					else
						$response->say($widget['content'], array("language" => $language, "voice" => $voice));
				}				
				elseif ($widget['content_type']  == 'Audio' ) {
					$response->play('audio/'.$company_id.'/'.$widget['content'], array("loop" => "1"));
				}				
				elseif ($widget['content_type']  == 'MP3_URL' || $widget['content_type']  == 'RECORD_AUDIO') {
						$response->play($widget['content'], array("loop" => "1"));
				}

				if ( $widget['nId'] == '0'){
					$response->hangup();
					$stmt = $db->customExecute("UPDATE calls set DialCallStatus='completed' WHERE CallSid=?");
					$stmt->execute(array(@$_REQUEST['CallSid']));
				}else{
					$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['nId'].'&tcx='.strtotime('now'));
				}
				
				
				break;
			}
			case "Voicemail":{

				if ( $widget['content_type']  == 'Text' ) {
					$language = getMetaDetail($widget['wId'], 'language');
					$voice = getMetaDetail($widget['wId'], 'voice');
					$language = str_replace("|M", "", str_replace("|W", "", $language));

					if (empty($language))
						$response->say($widget['content']);
					else
						$response->say($widget['content'], array("language" => $language, "voice" => $voice));
				}				
				elseif ($widget['content_type']  == 'Audio' )	
					$response->play('audio/'.$company_id.'/'.urlencode($widget['content']), array("loop" => "1"));
				elseif ($widget['content_type']  == 'MP3_URL' || $widget['content_type']  == 'RECORD_AUDIO') {
						$response->play($widget['content'], array("loop" => "1"));
				}
				else
					$response->say('Leave a message for at the beep');
						
				$vmURL = "handle_message.php?exten=$company_id";
				
				if ( $widget['nId'] != '0') $vmURL =$vmURL .'wId='.$widget['nId'];

				$params = array(
                    'action' => $vmURL,
                    'maxLength' => '120',
                    'playBeep' => 'true'
                );

                $send_transcript = getMetaDetail($widget['wId'], 'send_transcript');
                $TranscriptEmail = getMetaDetail($widget['wId'], 'TranscriptEmail');

                if ($send_transcript == 1) {
                    $params['transcribe'] = 'true';
                    $params['transcribeCallback'] = 'handle_transcription.php?email='.urlencode($TranscriptEmail);
                }
				
				$response->record($params);			
				
				break;
			}
			case "VoicemailWithoutRing":{

				if ( $widget['content_type']  == 'Text' ) {
					$language = getMetaDetail($widget['wId'], 'language');
					$voice = getMetaDetail($widget['wId'], 'voice');
					$language = str_replace("|M", "", str_replace("|W", "", $language));

					if (empty($language))
						$response->say($widget['content']);
					else
						$response->say($widget['content'], array("language" => $language, "voice" => $voice));
				}				
				elseif ($widget['content_type']  == 'Audio' )	
					$response->play('audio/'.$company_id.'/'.urlencode($widget['content']), array("loop" => "1"));
				elseif ($widget['content_type']  == 'MP3_URL' || $widget['content_type']  == 'RECORD_AUDIO') {
						$response->play($widget['content'], array("loop" => "1"));
				}
				else
					$response->say('Leave a message for at the beep');
						
				$vmURL = "handle_message.php?exten=$company_id";
				
				if ( $widget['nId'] != '0') $vmURL =$vmURL .'wId='.$widget['nId'];

				$params = array(
                    'action' => $vmURL,
                    'maxLength' => '120',
                    'playBeep' => 'true'
                );

                $send_transcript = getMetaDetail($widget['wId'], 'send_transcript');
                $TranscriptEmail = getMetaDetail($widget['wId'], 'TranscriptEmail');

                if ($send_transcript == 1) {
                    $params['transcribe'] = 'true';
                    $params['transcribeCallback'] = 'handle_transcription.php?email='.urlencode($TranscriptEmail);
                }
				
				$response->record($params);			
				
				break;
			}
			case "Menu":{
				
				$rdir=0;
				if (isset ($_REQUEST['rdir'] ) )
					{
						$rdir = $_REQUEST['rdir'];
					}
				
				if (strlen($_REQUEST['Digits']))
				{
					$digits = $_REQUEST['Digits'];
					$ivr_detail = $db->getMenuItems($widget['wId']);
					
					if ( array_key_exists($digits,	$ivr_detail))
					{
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $ivr_detail[$_REQUEST['Digits']]['wId'].'&kp='.$_REQUEST['Digits'] .'&tcx='.strtotime('now'));
							
					}else{
						
						
						$response->say('please enter valid digit');
						$response->pause(NULL, array ('length'=>'2'));
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['wId'].'&rdir='. $rdir .'&tcx='.strtotime('now'));
					}
				}else{
					
				
					$menu_repeat =  intval($widget['meta_data']);
					if ( $menu_repeat == 0) $menu_repeat =1;
				
					$gather = $response->gather(array("numDigits" => "1"));
					
					if ( $widget['content_type']  == 'Text' ) {
						$language = getMetaDetail($widget['wId'], 'language');
						$voice = getMetaDetail($widget['wId'], 'voice');
						$language = str_replace("|M", "", str_replace("|W", "", $language));

						if (empty($language))
							$gather->say($widget['content']);
						else
							$gather->say($widget['content'], array("language" => $language, "voice" => $voice));
					}
					
					else {
						if (!empty($widget['content'])) {
							$play_url = $widget['content'];
							if (substr($play_url, 0, 4) != "http") {
	                            $play_url = dirname(s8_get_current_webpage_uri()).'/audio/'.$company_id.'/'.$widget['content'];
	                        }
	                        	$gather->play($play_url);
                        }
					}
					
					
					
					
					if ( $rdir <  $menu_repeat )
					{
						if ($rdir != 0 && !empty($rdir)) {
							$menu_delay = getMetaDetail($widget['wId'], 'menu_delay');
							if (!empty($menu_delay)) {
								if (is_numeric($menu_delay)) {
									$menu_delay = $menu_delay - 5;
									if ($menu_delay < 0) $menu_delay = 0;
									sleep($menu_delay);
								}
							}
						}

						$rdir++;
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['wId'].'&rdir='. $rdir.'&tcx='.strtotime('now'));
					
					}elseif ( $widget['nId'] == '0'){
						
						$response->hangup();

						$stmt = $db->customExecute("UPDATE calls set DialCallStatus='completed' WHERE CallSid=?");
						$stmt->execute(array(@$_REQUEST['CallSid']));
					
					}else{
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['nId'].'&tcx='.strtotime('now'));
					}
				
				}
				
				break;
			}
			
			case "Dial":{

				$dial_details = $widget['content'];

				if (!empty($dial_details)) {
				 	$dial_details = json_decode($dial_details, true);

					$number = $dial_details['open_number'];

                    $openNumber = $dial_details['open_number'];
                    $closeNumber = $dial_details['close_number'];

					if($dial_details['specify_hours'] == '1')
                    {
                        $tz = new DateTimeZone($TIMEZONE);
			            $date = new DateTime();
			            $date->setTimezone($tz);
						
						
			            $today = strtolower($date->format("N") - 1); 
						$todayDate = $date->format("Y-m-d");
						$curTime = strtotime($date->format("Y-m-d H:i"));

			            $openTime =  strtotime($todayDate.' '.$dial_details['call_hours'][$today]['open_time']);
						$closeTime =  strtotime($todayDate.' '.$dial_details['call_hours'][$today]['close_time']);

						if ($curTime >= $openTime &&  $closeTime > $curTime) {
							if($dial_details['call_hours'][$today]['status']  == '1')
								$number = $closeNumber;
							elseif ( !$dial_details['forward_number_enabled'] ) 
								$number = $openNumber;
						}
						else {
							$number = $closeNumber;
						}
					}

					$international = ($number == $openNumber) ? $dial_details['open_number_int'] : $dial_details['close_number_int'];

                    $search_for = ($number == $openNumber) ? "open" : "closed";

                    $cvw_details = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM call_ivr_widget WHERE pId = '".$widget['wId']."' AND data = '".$search_for."'"));

			        if (!empty($cvw_details['wId'])) {
			         	$nextWidgetURL ='&company_id='.$company_id.'&wId='.$cvw_details['wId'];
			        }
			        elseif ( $widget['nId'] != '0') $nextWidgetURL ='&company_id='.$company_id.'&wId='.$widget['nId'];

			        $ring_count = getMetaDetail($cvw_details['wId'], "ring_count");
			        if (empty($ring_count)) $ring_count = 60;

                	if($international==1)
                        $number = "+".$number;
                    else
                        $number = "+1".$number;

					if ((empty($number) || $number == "+1") && !empty($cvw_details['wId'])) {
                        $response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='.$cvw_details['wId']);
                    }
                    else {
					 
                        $ring_tone = Util::getRingTone();

                        $dial_actions = array(
                            'action' => dirname(s8_get_current_webpage_uri()).'/record_call.php?DialCallTo='.urlencode($number).$nextWidgetURL,
                            'method' => 'POST',
							'timeout'=>	$ring_count,
                            'record' => ($company_settings->record_from_ring == 1) ? 'record-from-ringing' : 'true',
                            'ringTone' => $ring_tone
                        );
	                 
					 	$caller_id = $db->getCallerId($company_id);
	                    if($caller_id!="") $dial_actions['callerId'] = $caller_id;

	                    if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
	                            $dial_actions['record'] = 'false';

						if($company_settings->recording_warning_url!="")
						{
							$response->play($company_settings->recording_warning_url);
						}

	                     $number_actions = array();
	                     if($company_settings->whisper!="")
	                         $number_actions['url'] = 'whisper.php?num='.urlencode($_REQUEST['To']);

	                     //array('url'=>'whisper.php?num='.urlencode($_REQUEST['To']))
	                     $dial = $response->dial(NULL,$dial_actions);
	                     $dial->number($number,$number_actions);
	                 }
					}

					if ( $widget['nId'] != '0'){
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['nId'].'&tcx='.strtotime('now'));
					}
				
				break;
			}

			case "OptIn":{
			
				require_once 'include/ad_auto_dialer_files/lib/ad_lib_funcs.php';
				$list_name =  $widget['content'];

				$number = $_REQUEST['From'];

				if (Util::isNumberIntl($number)) {
					if (strlen($number) == 10)
						$number = "1".$number;
				}

				$single_contacts = array(
					array(
		                0 => $number,
		                1 => "",
		                2 => "",
		                3 => "",
		                4 => "",
		                5 => "",
		                6 => "",
		                7 => "",
		                8 => "",
		                9 => ""
		            )
		        );

		        $cl_idx = ad_advb_cl_add_cl($list_name);

		        ad_advb_cl_add_contacts($cl_idx, $single_contacts);

		        if ( $widget['nId'] == '0'){
					$response->hangup();

					$stmt = $db->customExecute("UPDATE calls set DialCallStatus='completed' WHERE CallSid=?");
					$stmt->execute(array(@$_REQUEST['CallSid']));
				}else{
					$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['nId'].'&tcx='.strtotime('now'));
				}
				
				break;
			}


			case "AgentIVR":{

				$content = json_decode($widget['content'], true);
				$greeting = $content['greeting'];
				$hold_music = $content['hold_music'];

				$hold_music_none = false;
				if (empty($hold_music['content'])) {
					$hold_music = array(
						"type" => "MP3_URL",
						"content" => dirname(s8_get_current_webpage_uri())."/mp3/default-hold.mp3"
					);
					$content['repeat'] = 2;
				}

				$agents = $content['agents'];
				$addons_list = Addons::getAddonList();

				$action = (isset($_GET['action'])) ? $_GET['action'] : "default";
				
				$client = new Services_Twilio($AccountSid, $AuthToken);

				switch ($action) {
					case 'default':

						$went_to_agent_voicemail = false;
						$specific_agent = false;
						if (strlen($_REQUEST['Digits']))
						{
							$digits = $_REQUEST['Digits'];
							foreach ($agents as $agent) {
								if ($agent['extension'] == $digits) {
									$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time,access_lvl, (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') as has_browser_softphone FROM users u WHERE u.idx = ?");
									$stmt->execute(array($agent['user_id']));
									$user = $stmt->fetch(PDO::FETCH_OBJ);

									$diff = strtotime("now") - $user->status_update_time;

									if (($user->status == "ONLINE" || $user->status == "BUSY" || $user->status == "AWAY") && $diff <= 20 && ($user->has_browser_softphone > 0 || $user->access_lvl >=1)) {
										$specific_agent = true;
										$queue_name = "user_".$user->idx;
										break;
									}
									else {
										//Go to agent voicemail
										if ($agent['voicemail']['type']  == 'Text' ) {
											$language = $agent['voicemail']['language'];
											$voice = $agent['voicemail']['voice'];
											$language = str_replace("|M", "", str_replace("|W", "", $language));

											if (empty($language))
												$response->say($agent['voicemail']['content']);
											else
												$response->say($agent['voicemail']['content'], array("language" => $language, "voice" => $voice));
										}				
										elseif ($agent['voicemail']['type']  == 'Audio' )	
											$response->play('audio/'.$company_id.'/'.$agent['voicemail']['content'], array("loop" => "1"));
										elseif ($agent['voicemail']['type']  == 'MP3_URL' || $agent['voicemail']['type']  == 'RECORD_AUDIO') {
											$response->play($agent['voicemail']['content'], array("loop" => "1"));
										}
										else
											$response->say('Leave a message for at the beep');
												
										$vmURL = "handle_message.php?exten=$company_id";

										$params = array(
						                    'action' => $vmURL,
						                    'maxLength' => '120',
						                    'playBeep' => 'true'
						                );

						                $send_transcript = $agent['send_transcript'];
						                $TranscriptEmail = $agent['transcript_email'];

						                if ($send_transcript == 1) {
						                    $params['transcribe'] = 'true';
						                    $params['transcribeCallback'] = 'handle_transcription.php?email='.urlencode($TranscriptEmail);
						                }
										$response->record($params);
										
										$went_to_agent_voicemail = true;
									}
								}
							}
						}
						else {
							$available_agents = array();
							if (isset($_REQUEST['forced_agent_id'])) {
								if ($_REQUEST['forced_agent_id'] == 2) {
									$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time FROM users u WHERE u.idx = ?");
									$stmt->execute(array($_REQUEST['forced_agent_id']));
									$user = $stmt->fetch(PDO::FETCH_OBJ);

									$diff = strtotime("now") - $user->status_update_time;
									if (($user->status == "ONLINE" || $user->status == "BUSY" || $user->status == "AWAY") && $diff <= 20) {
										$specific_agent = true;
										$queue_name = "user_".$user->idx;
									}
								}
								else {
									$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time FROM users u WHERE u.idx = ?");
									$stmt->execute(array($_REQUEST['forced_agent_id']));
									$user = $stmt->fetch(PDO::FETCH_OBJ);

									$diff = strtotime("now") - $user->status_update_time;
									if (($user->status == "ONLINE" || $user->status == "BUSY" || $user->status == "AWAY") && $diff <= 20) {
										$specific_agent = true;
										$queue_name = "user_".$user->idx;
									}

									if (empty($queue_name)) {
										foreach ($agents as $agent) {
											if ($agent['user_id'] == $_REQUEST['forced_agent_id']) {
												//Go to agent voicemail
												if ($agent['voicemail']['type']  == 'Text' ) {
													$language = $agent['voicemail']['language'];
													$voice = $agent['voicemail']['voice'];
													$language = str_replace("|M", "", str_replace("|W", "", $language));

													if (empty($language))
														$response->say($agent['voicemail']['content']);
													else
														$response->say($agent['voicemail']['content'], array("language" => $language, "voice" => $voice));
												}				
												elseif ($agent['voicemail']['type']  == 'Audio' )	
													$response->play('audio/'.$company_id.'//'.$agent['voicemail']['content'], array("loop" => "1"));
												elseif ($agent['voicemail']['type']  == 'MP3_URL' || $agent['voicemail']['type']  == 'RECORD_AUDIO') {
													$response->play($agent['voicemail']['content'], array("loop" => "1"));
												}
												else
													$response->say('Leave a message for at the beep');
														
												$vmURL = "handle_message.php?exten=$company_id";

												$params = array(
								                    'action' => $vmURL,
								                    'maxLength' => '120',
								                    'playBeep' => 'true'
								                );

								                $send_transcript = $agent['send_transcript'];
								                $TranscriptEmail = $agent['transcript_email'];

								                if ($send_transcript == 1) {
								                    $params['transcribe'] = 'true';
								                    $params['transcribeCallback'] = 'handle_transcription.php?email='.urlencode($TranscriptEmail);
								                }
												$response->record($params);
												
												$went_to_agent_voicemail = true;
											}
										}
									}
								}
							}
							elseif ($content['flow_type'] == "multiple_agents") {
								foreach ($agents as $agent) {
									$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time,access_lvl, (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') as has_browser_softphone FROM users u WHERE u.idx = ?");
									$stmt->execute(array($agent['user_id']));
									$user = $stmt->fetch(PDO::FETCH_OBJ);

									$diff = strtotime("now") - $user->status_update_time;

									if (($user->status == "ONLINE" || $user->status == "BUSY") && $diff <= 20 && ($user->has_browser_softphone > 0 || $user->access_lvl >=1)) {
										$available_agents[] = $agent;
									}
								}
							}
							elseif ($content['flow_type'] == "agent_round_robin") {
								$last_used = $content['last_used'];
								$get_next = false;
								$found_agent = false;
								$found_agent_id = 0;
								foreach ($agents as $agent) {
									if ($get_next) {
										$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time,access_lvl, (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') as has_browser_softphone FROM users u WHERE u.idx = ?");
										$stmt->execute(array($agent['user_id']));
										$user = $stmt->fetch(PDO::FETCH_OBJ);

										$diff = strtotime("now") - $user->status_update_time;

										if ($user->status == "ONLINE" && $diff <= 20 && ($user->has_browser_softphone > 0 || $user->access_lvl >=1)) {
											$queue_name = "user_".$user->idx;
											$found_agent = true;
											$found_agent_id = $user->idx;
											break;
										}
									}

									if ($last_used == $agent['idx']) $get_next = true;
								}
								
								if (!$found_agent) {
									foreach ($agents as $agent) {
										$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time,access_lvl, (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') as has_browser_softphone FROM users u WHERE u.idx = ?");
										$stmt->execute(array($agent['user_id']));
										$user = $stmt->fetch(PDO::FETCH_OBJ);

										$diff = strtotime("now") - $user->status_update_time;

										if ($user->status == "ONLINE" && $diff <= 20 && ($user->has_browser_softphone > 0 || $user->access_lvl >=1)) {
											$queue_name = "user_".$user->idx;
											$found_agent = true;
											$found_agent_id = $user->idx;
											break;
										}
									}
								}
								
								if (!$found_agent) {
									foreach ($agents as $agent) {
										$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time,access_lvl, (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') as has_browser_softphone FROM users u WHERE u.idx = ?");
										$stmt->execute(array($agent['user_id']));
										$user = $stmt->fetch(PDO::FETCH_OBJ);

										$diff = strtotime("now") - $user->status_update_time;

										if ($user->status == "BUSY" && $diff <= 20 && ($user->has_browser_softphone > 0 || $user->access_lvl >=1)) {
											$queue_name = "user_".$user->idx;
											$found_agent = true;
											$found_agent_id = $user->idx;
											break;
										}
									}
								}
							}
							else {
								foreach ($agents as $agent) {
									$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time,access_lvl, (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') as has_browser_softphone FROM users u WHERE u.idx = ?");
									$stmt->execute(array($agent['user_id']));
									$user = $stmt->fetch(PDO::FETCH_OBJ);

									$diff = strtotime("now") - $user->status_update_time;

									if ($user->status == "ONLINE" && $diff <= 20 && ($user->has_browser_softphone > 0 || $user->access_lvl >=1)) {
										$queue_name = "user_".$user->idx;
										break;
									}
								}

								if (empty($queue_name)) {
									foreach ($agents as $agent) {
										$stmt = $db->customExecute("SELECT idx,full_name,username,status,status_update_time,access_lvl, (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') as has_browser_softphone FROM users u WHERE u.idx = ?");
										$stmt->execute(array($agent['user_id']));
										$user = $stmt->fetch(PDO::FETCH_OBJ);

										$diff = strtotime("now") - $user->status_update_time;

										if ($user->status == "BUSY" && $diff <= 20 && ($user->has_browser_softphone > 0 || $user->access_lvl >=1)) {
											$queue_name = "user_".$user->idx;
											break;
										}
									}
								}
							}
						}


						if ($content['flow_type'] == "multiple_agents" && count($available_agents) > 0) {
							$queue_name = "company_".$company_id;
						}

						if (!empty($queue_name)) {
							$qsid = "";
						    foreach ($client->account->queues as $queue) {
						        if ($queue->friendly_name == $queue_name) {
						            $qsid = $queue->sid;
						            break;
						        }
						    }

						    if (empty($qsid))
						    	$queue = $client->account->queues->create($queue_name, array("MaxSize" => $content['max_queue_size']));
						    else
						    	$queue = $client->account->queues->get($qsid);

						    if (count($queue->members) >= $content['max_queue_size']) {
						    	$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $content['master_voicemail_wId'].'&tcx='.strtotime('now'));
						    }
						    else {
						    	if (!$specific_agent) {
						    		if ($content['agents_use_extensions'] == 1) {
								    	$gather = $response->gather(array("numDigits" => $content['agents_extension_length']));
										if ($greeting['type']  == 'Text' ) {
											$greeting['language'] = str_replace("|M", "", str_replace("|W", "", $greeting['language']));

											if (empty($greeting['language']))
												$gather->say($greeting['content']);
											else
												$gather->say($greeting['content'], array("language" => $greeting['language'], "voice" => $greeting['voice']));
										}				
										elseif ($greeting['type']  == 'Audio' ) {
											$gather->play('audio/'.$company_id.'/'.$greeting['content'], array("loop" => "1"));
										}				
										elseif ($greeting['type']  == 'MP3_URL' || $greeting['type']  == 'RECORD_AUDIO') {
											$gather->play($greeting['content'], array("loop" => "1"));
										}
									}
									else {
										if ($greeting['type']  == 'Text' ) {
											$greeting['language'] = str_replace("|M", "", str_replace("|W", "", $greeting['language']));

											if (empty($greeting['language']))
												$response->say($greeting['content']);
											else
												$response->say($greeting['content'], array("language" => $greeting['language'], "voice" => $greeting['voice']));
										}				
										elseif ($greeting['type']  == 'Audio' ) {
											$response->play('audio/'.$company_id.'/'.$greeting['content'], array("loop" => "1"));
										}				
										elseif ($greeting['type']  == 'MP3_URL' || $greeting['type']  == 'RECORD_AUDIO') {
											$response->play($greeting['content'], array("loop" => "1"));
										}
									}
								}
								$wait_url = 'ivrmenu.php?company_id='.$company_id.'&wId='. $widget['wId'].'&tcx='.strtotime('now')."&action=play_hold_music";
								if (isset($_REQUEST['forced_agent_id']))
									$wait_url = $wait_url."&forced_agent_id=".$_REQUEST['forced_agent_id'];

								if (!empty($content['applet_name'])) {
									$stmt = $db->customExecute("UPDATE calls SET Department = ? WHERE CallSid = ?");
									$stmt->execute(array($content['applet_name'], $_REQUEST['CallSid']));
									$user = $stmt->fetch(PDO::FETCH_OBJ);
								}

								$response->enqueue($queue_name, array(
										"waitUrl" => $wait_url
									)
								);
							}
						}
						else {
							if (!$went_to_agent_voicemail)
								$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $content['master_voicemail_wId'].'&tcx='.strtotime('now'));
						}

					break;

					case 'play_hold_music_without_prompt':
						if ($hold_music_none) {
							$response->pause(10);
						}
						else {
							if ($hold_music['type']  == 'Text' ) {
								$hold_music['language'] = str_replace("|M", "", str_replace("|W", "", $hold_music['language']));

								if (empty($hold_music['language']))
									$response->say($hold_music['content']);
								else
									$response->say($hold_music['content'], array("language" => $hold_music['language'], "voice" => $hold_music['voice']));
							}				
							elseif ($hold_music['type']  == 'Audio' ) {
								$response->play('audio/'.$company_id.'/'.$hold_music['content'], array("loop" => "1"));
							}				
							elseif ($hold_music['type']  == 'MP3_URL' || $hold_music['type']  == 'RECORD_AUDIO') {
								$response->play($hold_music['content'], array("loop" => "1"));
							}
						}
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['wId'].'&tcx='.strtotime('now')."&action=play_hold_music_without_prompt");
					break;

					case 'play_hold_music':
						ob_start();

						if (isset($_REQUEST['forced_agent_id'])) {
							$response->play('mp3/telephone-dial.mp3');
							$response->hangup();
						}
						else {
							$repeat = (int)$content['repeat'];
							$this_count = (isset($_REQUEST['counter']) ? (int)$_REQUEST['counter'] : 1);

							if ($this_count <= $repeat) {
								if ($hold_music['type']  == 'Text' ) {
									$hold_music['language'] = str_replace("|M", "", str_replace("|W", "", $hold_music['language']));

									if (empty($hold_music['language']))
										$response->say($hold_music['content']);
									else
										$response->say($hold_music['content'], array("language" => $hold_music['language'], "voice" => $hold_music['voice']));
								}				
								elseif ($hold_music['type']  == 'Audio' ) {
									$response->play('audio/'.$company_id.'/'.$hold_music['content'], array("loop" => "1"));
								}				
								elseif ($hold_music['type']  == 'MP3_URL' || $hold_music['type']  == 'RECORD_AUDIO') {
									$response->play($hold_music['content'], array("loop" => "1"));
								}

								if ($content['prompt_message_type'] != "none" && !$hold_music_none) {
									$prompt_messages = $content['prompt_messages'];
									$prompt_message = $prompt_messages[rand(0, (count($prompt_messages) - 1))];

									if ($prompt_message['type']  == 'Text' ) {
										$prompt_message['language'] = str_replace("|M", "", str_replace("|W", "", $prompt_message['language']));

										if (strpos($prompt_message['content'], "[NumberInQueue]") !== false) {
											$prompt_message['content'] = str_replace('[NumberInQueue]', $_REQUEST['QueuePosition'], $prompt_message['content']);
										}

										if (empty($prompt_message['language']))
											$response->say($prompt_message['content']);
										else
											$response->say($prompt_message['content'], array("language" => $prompt_message['language'], "voice" => $prompt_message['voice']));
									}				
									elseif ($prompt_message['type']  == 'Audio' ) {
										$response->play('audio/'.$company_id.'/'.$prompt_message['content'], array("loop" => "1"));
									}				
									elseif ($prompt_message['type']  == 'MP3_URL' || $prompt_message['type']  == 'RECORD_AUDIO') {
										$response->play($prompt_message['content'], array("loop" => "1"));
									}
								}
								$next_count = $this_count + 1;
								$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $widget['wId'].'&tcx='.strtotime('now')."&action=play_hold_music&counter=".$next_count);
							}
							else {
								$call = $client->account->calls->get($_REQUEST['CallSid']);

							    $exploded = explode("include", dirname(s8_get_current_webpage_uri()));

							    $call->update(array(
							        "Url" => dirname(s8_get_current_webpage_uri()).'/ivrmenu.php?company_id=' . $company_id . '&action=decline',
							        "Method" => "POST"
							    ));
							}
						}
					break;

					case 'activate_answer':
						$conference_name = trim($_REQUEST['conference_name']);

						$_REQUEST['From'] = trim(urldecode($_REQUEST['From']));
						$sth = $db->customExecute("SELECT * FROM call_conferences WHERE `from` = ?");
						$sth->execute(array($_REQUEST['From']));
						$conference = $sth->fetch();

						if ($conference) {
							$sth = $db->customExecute("UPDATE call_conferences SET moved_to_conference = ? WHERE `from` = ?");
							$sth->execute(array($conference_name, $_REQUEST['From']));
						}

						if ($_REQUEST['caller_is_agent'] != 1) {
							$sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
							$sth->execute(array($conference_name));
							$conference = $sth->fetch();

							$participants = json_decode($conference['participants'], true);

							$handle = $_REQUEST['From'];
							$contact = $db->getContactByPhoneNumberIfInAList($_REQUEST['From']);

					        if ($contact) {
					            $handle = trim(Util::escapeString($contact['first_name']." ".$contact['last_name']));
					            if (empty($handle)) $handle = trim(Util::escapeString($contact['business_name']));

								$participants[] = array(
									"type" => "contact",
									"contact_id" => $contact['idx'],
									"handle" => $handle,
									"phone_number" => $_REQUEST['From'],
									"call_sid" => $_REQUEST['CallSid'],
									"is_host" => false,
									"is_caller" => true
								);
					        }
					        else {
								$participants[] = array(
									"type" => "phone_number",
									"user_id" => 0,
									"handle" => $handle,
									"call_sid" => $_REQUEST['CallSid'],
									"is_host" => false,
									"is_caller" => true
								);
					        }

							$sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
							$sth->execute(array(json_encode($participants), $conference['id']));
						}

						if ($_REQUEST['bringing_back'] == 1) {
							$sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
							$sth->execute(array($conference_name));
							$conference = $sth->fetch();

							$new_participants = array();
							$participants = json_decode($conference['participants'], true);

							foreach ($participants as $participant) {
								if ($_REQUEST['call_sid'] == $participant['call_sid']) {
									$participant['bringing_them_back'] = '0';
								}
								$new_participants[] = $participant;
							}

							$sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
							$sth->execute(array(json_encode($new_participants), $conference['id']));
						}

						$action_url = dirname(s8_get_current_webpage_uri()).'/record_call.php?test=c2&DialCallTo='.urlencode($_REQUEST['From']).'&company_id='.$company_id.'&exit_from_conference=1&conference_name='.$conference_name;

						$dial_actions = array(
                            'action' => $action_url,
                            'record' => 'true'
                        );

                        $dial = $response->dial(NULL, $dial_actions);
  						$dial->conference($conference_name, 
  							array(
                                "record" => "true",
                                "beep" => "false"
                            )
						);
					break;

					case 'decline':
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $content['master_voicemail_wId'].'&tcx='.strtotime('now'));
					break;

					case 'remove_from_call':
						$response->hangup();
					break;

					case 'put_on_hold':
						$queue_name = $_REQUEST['queue_name'];
						$conference_name = $_REQUEST['conference_name'];
						
						$qsid = "";
					    foreach ($client->account->queues as $queue) {
					        if ($queue->friendly_name == $queue_name) {
					            $qsid = $queue->sid;
					            break;
					        }
					    }

					    if (empty($qsid))
					    	$queue = $client->account->queues->create($queue_name, array("MaxSize" => 1000));
					    else
					    	$queue = $client->account->queues->get($qsid);

						$response->enqueue($queue_name, array(
								"waitUrl" => 'ivrmenu.php?company_id='.$company_id.'&wId='. $widget['wId'].'&tcx='.strtotime('now')."&action=play_hold_music_without_prompt",
								"action" => $action_url = dirname(s8_get_current_webpage_uri()).'/record_call.php?&company_id='.$company_id.'&exit_from_conference=1&conference_name='.$conference_name."&ignore_hold=1"
							)
						);
					break;
				}

				break;
			}

				case "BusinessHours":{

					$dial_details = $widget['content'];
				 	$dial_details = json_decode($dial_details, true);

				 	$tz = new DateTimeZone($TIMEZONE);
				 	$date = new DateTime();
		            $date->setTimezone($tz);

		            $today = strtolower($date->format("N") - 1); 
					$todayDate = $date->format("Y-m-d");
					$curTime = strtotime($date->format("Y-m-d H:i"));

					if (empty($dial_details['call_hours'][$today]['open_time']))
						$dial_details['call_hours'][$today]['open_time'] = "8:00:00";

					if (empty($dial_details['call_hours'][$today]['close_time']))
						$dial_details['call_hours'][$today]['close_time'] = "17:00:00";
		            
		            $openTime =  strtotime($todayDate.' '.$dial_details['call_hours'][$today]['open_time']);
					$closeTime =  strtotime($todayDate.' '.$dial_details['call_hours'][$today]['close_time']);

					$nextWidget = "open";
					if ($curTime >= $openTime &&  $closeTime > $curTime) {
						if($dial_details['call_hours'][$today]['status']  == '1')
							$nextWidget = "closed";
					}
					else {
						$nextWidget = "closed";
					}

					$sth = $db->customExecute("SELECT * FROM call_ivr_widget WHERE pId = ? AND data = ?");

					$sth->execute(array($widget['wId'], $nextWidget));
					$nextWidget = $sth->fetch();

					if ($nextWidget['wId'] != '0'){
						$response->redirect('ivrmenu.php?company_id='.$company_id.'&wId='. $nextWidget['wId'].'&tcx='.strtotime('now'));
					}
				
				break;
			}
	}
		/* 
		
		
		
		
		$widget['wId'] = $rs_nxt['wId'];
					$widget['pId'] = $rs_nxt['pId'];
					$widget['flowtype'] = $rs_nxt['flowtype'];
					$widget['content_type'] = $rs_nxt['content_type'];
					$widget['content'] = $rs_nxt['content'];
					$widget['nId'] = $rs_nxt['nId'];
					$widget['data'] = $rs_nxt['data']
					$widget['keypress'] = $rs_nxt['keypress'];
	
		

	
}*/
	print $response;


function getMetaDetail($wId, $meta_key)
{
	global $con;
	$row= array();
	$sql_dt = "SELECT * FROM call_ivr_data WHERE wId = '$wId' AND meta_key = '".$meta_key."'";
	$res = mysqli_query($con, $sql_dt);
	if (mysqli_num_rows($res) >0 )
	{   
	  $row = mysqli_fetch_assoc($res);
	  return $row['meta_data'];
	}
	else {
	  return "";
	}
}
function s8_get_current_webpage_uri() {
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}
	
	?>
