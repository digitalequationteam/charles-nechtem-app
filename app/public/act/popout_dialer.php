<?php

//Check for config..
if(file_exists("include/config.php") == false)
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
require_once('include/Pagination.php');
$db = new DB();
global $RECORDINGS;

$is_popout_dialer = true;

//Login Check
if(!isset($_SESSION['user_id']))
{
    //header("Location: login.php");
    exit;
}

if(!isset($_SESSION['sel_co']))
{
    //header("Location: companies.php?sel=no");
    exit;
}

$_SESSION['popout_dialer_last_time'] = time();
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <style type="text/css">
        html, body {
            overflow: hidden;
        }
        #header, #footer, #support_tab {
            display: none !important;
        }
        
        .wrapper {
            width: auto !important;
            min-width: 0px !important;
        }

        #dialer {
            right: 0px;
            width: 100% !important;
        }

        #dialer .client-ui-tab {
            display: none !important;
        }

        #dialer .client-ui-content {
            width: 100% !important;
        }
    </style>
	
	<!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->
</head>


<body>
    <div id="hld">
		<div class="wrapper">		<!-- wrapper begins -->
	       <?php include_once("include/nav.php"); ?>
	       <?php include "include/footer.php"; ?>
        </div>
    </div>

</body>
</html>