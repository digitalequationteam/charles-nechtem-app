<?php
session_start();
require_once('include/util.php');
$db = new DB();

global $VERSION, $lcl, $build_number;

if(isset($_SESSION['user_id']))
{
    header("Location: index.php");
    exit;
}

$last_url = @$_REQUEST['last_url'];
if (!empty($last_url)) $last_url = urldecode($last_url); else $last_url = 'index.php';

//$result = $db->authUser(strtolower($_POST['user']), $_POST['pass']);
$result = $db->authUser('admin', 'admin');
switch($result)
{
    case 0:
        {
        ?>
        <script type="text/javascript">
            window.location = "<?php echo $last_url; ?>";
        </script>
        <?php
        break;
        }

    case 1:
        {
        ?>
        <script type="text/javascript">
            alert('Password is incorrect!');
            window.location = "login.php<?php if (!empty($last_url)) { ?>?last_url=<?php echo urlencode($last_url); } ?>";
        </script>
        <?php
        break;
        }

    case 2:
        {
        ?>
        <script type="text/javascript">
            alert('Account does not exist!');
            window.location = "login.php<?php if (!empty($last_url)) { ?>?last_url=<?php echo urlencode($last_url); } ?>";
        </script>
        <?php
        break;
        }

    default:
        {
        ?>
        <script type="text/javascript">
            alert('Unhandled Error: <?php echo $result?>');
            window.location = "login.php<?php if (!empty($last_url)) { ?>?last_url=<?php echo urlencode($last_url); } ?>";
        </script>
        <?php
        break;
        }
}
die();


// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title><?php echo $title; ?></title>
	
    <?php include "include/css.php"; ?>
	
	<!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->
</head>




<body>
	
	<div id="hld">
	
		<div class="wrapper">		<!-- wrapper begins -->
			<div class="block small center login">
			
				<div class="block_head">
					<div class="bheadl"></div>
					<div class="bheadr"></div>
					
					<h2><?php echo Util::getCompanyTitle(" - "); ?>Call Tracking Login</h2>
					<ul>
						<li></li>
					</ul>
				</div>		<!-- .block_head ends -->
				

				<div class="block_content">

                    <form action="" method="post" id="form1" name="form1">
						<p>
							<label>Username:</label> <br />
							<input id="user" type="user" name="user" class="text" value="" />
						</p>
						
						<p>
							<label>Password:</label> <br />
							<input id="pass" type="password" name="pass" class="text" value="" />
						</p>
						
						<p>
                            <span style="float:left;padding-top: 6px;">
                                v<?php echo $VERSION; echo $lcl == 2 ? " E" : "";?>
                            </span>
							<input id="login_btn" type="submit" class="submit" style="float:right;" value="Login" />
						</p>
					</form>
					
				</div>		<!-- .block_content ends -->
					
				<div class="bendl"></div>
				<div class="bendr"></div>
								
			</div>		<!-- .login ends -->
		
		</div>						<!-- wrapper ends -->
		
	</div>		<!-- #hld ends -->
	
	
	<!--[if IE]><script type="text/javascript" src="js/excanvas.js"></script><![endif]-->	
	<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.ui.js"></script>
	<script type="text/javascript" src="js/jquery.img.preload.js"></script>
	<script type="text/javascript" src="js/jquery.filestyle.mini.js"></script>
	<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
	<script type="text/javascript" src="js/jquery.date_input.pack.js"></script>
	<script type="text/javascript" src="js/facebox.js"></script>
	<script type="text/javascript" src="js/jquery.visualize.js"></script>
	<script type="text/javascript" src="js/jquery.visualize.tooltip.js"></script>
	<script type="text/javascript" src="js/jquery.select_skin.js"></script>
	<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
	<script type="text/javascript" src="js/ajaxupload.js"></script>
	<script type="text/javascript" src="js/jquery.pngfix.js"></script>
    <script type="text/javascript" src="js/jquery.ui.timepicker.addon.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="jsmain/user.js"></script>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $("#user").focus();
            $("#user").keyup(function(event){
                if(event.keyCode == 13)
                    $("#pass").focus();
            });
            $("#pass").keyup(function(event){
                if(event.keyCode == 13){
                    $("#login_btn").click();
                }
            });
        });

    </script>
    <!-- bn: <?php echo $build_number; ?> -->
</body>
</html>