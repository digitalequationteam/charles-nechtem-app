<?php
require_once('include/util.php');
header('Content-type: text/xml');
// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$db = new DB();

$conf_id = $_REQUEST['conf_id'];

$mp3url = $db->getVar("intl_dialtone_url");

$company_id = $_REQUEST['company'];

if($company_id!="")
{
    $settings = $db->getCompanySettings($company_id);
}

if($_REQUEST['op']=="1"){

    if (substr($_REQUEST['T'], 0, 2) == "+1")
        $number = $db->getOutgoingNumber(substr($_REQUEST['T'], 2, strlen($_REQUEST['T']) - 2));
    else
        $number = $db->getOutgoingNumber(substr($_REQUEST['T'], 1, strlen($_REQUEST['T']) - 1));

    if($_REQUEST['DialCallTo'] != "")
        $number = $_REQUEST['DialCallTo'];

    global $AccountSid, $AuthToken;

    $client = new Services_Twilio($AccountSid, $AuthToken);
    $url = "http://" . $_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']) . "/".'intl_tone.php?op=2&T='.urlencode(str_replace(" ", "", $_REQUEST['To']))."&company=".$_REQUEST['company']."&DialCallTo=".urlencode(str_replace(" ","", $_REQUEST['DialCallTo'])).'&conf_id='.$conf_id;

    echo '<?xml version="1.0" encoding="UTF-8"?>';
    echo "<Response><Play loop=\"0\">{$mp3url}</Play></Response>";

    $ring_count = $settings->ring_count;
    if (empty($ring_count)) $ring_count = 60;

    $record = 'true';

    if($RECORDINGS==false || $db->isCompanyRecordingDisabled($company_id))
        $record = 'false';

    $call = $client->account->calls->create($_REQUEST['T'], str_replace(" ", "", $number), $url, array(
        'SendDigits'=>@$_REQUEST['sendDigits'],
        "StatusCallback" => "http://" . $_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']) . "/"."record_call.php?test=c2&DialCallTo=".$_REQUEST['T'],
        "StatusCallbackMethod" => "POST",
        "Timeout" => $ring_count,
        "Record" => $record
    ));

    $sth = $db->customExecute("UPDATE calls SET CallSid = ?, OriginalCallSid = ? WHERE CallSid = ?");
    $sth->execute(array($call->sid, $_REQUEST['CallSid'], $_REQUEST['CallSid']));

}else{
    $whisp_url = "";
    ?>
<Response>
    <?php if($settings->whisper != ""){

        if ($settings->whisper_type == "Text") {
            $settings->whisper_language = str_replace("|W", "", $settings->whisper_language);
            $settings->whisper_language = str_replace("|M", "", $settings->whisper_language);
            ?>
                <Say voice="<?php echo $settings->whisper_voice; ?>" language="<?php echo $settings->whisper_language; ?>"><?php echo $settings->whisper ?></Say>
            <?php
        }
        else {
            if (substr($settings->whisper, 0, 4) != "http") {
                require_once('include/twilio_header.php');
                $settings->whisper = dirname(s8_get_current_webpage_uri())."/audio/".$company_id."/".$settings->whisper;
            }
            ?>
                <Play><?php echo str_replace(" ", "%20", $settings->whisper); ?></Play>
            <?php
        }

         ?>
    <?php } ?>
    <Dial>
        <Conference beep="false" endConferenceOnExit="true"><?php echo $conf_id; ?></Conference>
    </Dial>
</Response>
    <?php
}
?>