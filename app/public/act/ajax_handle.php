<?php
session_start();
require_once('include/util.php');
ignore_user_abort();

if(!isset($_SESSION['user_id']) && @$_POST['func']=="")
{
    header('HTTP/1.0 403 Forbidden');
    exit;
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

// Function Table
$valid_functions = array(
    'USR_ADD_NOTE'                 => true,
    'USR_DELETE_NOTE'              => true,
    'USR_CHANGE_PASSWORD'          => true,
    'USR_SEND_EMAIL'               => true,
    'USR_CHANGE_PHONE_CODE'        => true,
    'USR_GET_PHONE_CODE_REPORT'    => true,
    'USR_GET_PHONE_CODE_REPORT_2'  => true,
    'USR_GET_OUTGOING_PHONE_CODE_REPORT' => true,
    'USR_GET_COMPANY_CAMPAIGNS'    => true,
    'USR_ADD_NUM_TO_BLACKLIST'     => true,
    'USR_GET_NOTES'                => true,
    'USR_AD_CSLIST'                => true,
    'USR_AD_CSSAVE'                => true,
    'USR_AD_CSDEL'                 => true,
    'USR_SEQ_DEL'                  => true,
    'USR_BROWSERPHONE_PC_SET'      => true,
    'USR_BROWSERPHONE_INCOMING_PC_SET' => true,
    'USR_BROWSERPHONE_TOGGLE_MUTE' => true,
    'USER_PUT_CALL_ON_HOLD'        => true,
    'USER_UPDATE_STATUS'           => true,
    'USR_DECLINE_INCOMING_CALL'    => true,
    'USR_DELETE_MISSED_CALL'       => true,
    'USR_REFRESH_TWILIO_TOKEN'     => true,
    'USR_REMOVE_PERSON_FROM_CALL'  => true,
    'USR_ADD_ANOTHER_AGENT_TO_CALL'=> true,
    'USR_ADD_PHONE_NUMBER_TO_CALL' => true,
    'USR_ADD_INCOMING_CALL_TO_CONFERENCE' => true,
    'USR_ADD_ONGOING_CALL_TO_CONFERENCE'  => true
);

if(in_array($_POST['func'],$valid_functions) && isset($_POST['func']))
{
	$params = $_POST;
	unset($params['func']);
	if($valid_functions[$_POST['func']]){
        $params['user_id'] = $_SESSION['user_id'];
        $_POST['func']($params);
    }else
		die("FUNCTION_NOT_EXIST:::".$_POST['func']);
}else{
		die("MISSING_FUNCTION_CALL:::");
}

function USR_BROWSERPHONE_PC_SET($params=''){
    $db = new DB();
    $db->setVar("browserphone_".$_SESSION['user_id'],$params['pc']);
    genericSuccess(array("browserphone_".$_SESSION['user_id'],$params['pc']));
}
function USR_BROWSERPHONE_INCOMING_PC_SET($params=''){
    $db = new DB();
    $db->setVar("browserphone_".$params['call_sid'],$params['pc']);
    genericSuccess(array("browserphone_".$params['call_sid'],$params['pc']));
}

function USR_SEQ_DEL($params=''){
    require('include/ad_auto_dialer_files/lib/ad_lib_funcs.php');
    $db = new DB();
    $seqid = $params['data'];

    $stmt = $db->customExecute("SELECT t1.*,t2.shared,t2.user from ad_vb_sequences t1 JOIN ad_vb_campaigns t2 ON t1.campaign_id=t2.idx WHERE t1.id = ?");
    $stmt->execute(array($seqid));
    $data = $stmt->fetch(PDO::FETCH_OBJ);

    if($_SESSION['user_id']>1){
        if($data!=""){
            $stmt = $db->customExecute("DELETE FROM ad_vb_sequences WHERE id = ?");
            if($stmt->execute(array($seqid))){
                $stmt = $db->customExecute("SELECT id,cron_id FROM ad_vb_sequence_schedule WHERE sequence_id = ?");
                $stmt->execute(array($seqid));
                foreach($stmt->fetchAll(PDO::FETCH_OBJ) as $sequence){
                    ad_del_cron_task($sequence->cron_id);
                    $db->customExecute("DELETE FROM ad_vb_sequence_schedule WHERE id = ?")->execute(array($sequence->id));
                }
                genericSuccess();
            }else{
                genericFail(2);
            }
        }else{
            genericFail(1);
        }
    }else{
        if($data->user!=$_SESSION['user']&&$data->shared==1){
            genericFail(3);
        }else{
            if($data->shared==1){
                $result = $db->customQuery("SELECT COUNT(*) as total FROM user_company WHERE user_id = '".$data->user."' AND company_id IN (SELECT company_id FROM user_company WHERE user_id = '".$_SESSION['user_id']."')");
                $count = $result->fetchAll(PDO::FETCH_ASSOC);
                if($count[0]['total']>0){
                    $stmt = $db->customExecute("DELETE FROM ad_vb_sequences WHERE id = ?");
                    if($stmt->execute(array($seqid))){
                        $stmt = $db->customExecute("SELECT id,cron_id FROM ad_vb_sequence_schedule WHERE sequence_id = ?");
                        $stmt->execute(array($seqid));
                        foreach($stmt->fetchAll(PDO::FETCH_OBJ) as $sequence){
                            ad_del_cron_task($sequence->cron_id);
                            $db->customExecute("DELETE FROM ad_vb_sequence_schedule WHERE id = ?")->execute(array($sequence->id));
                        }
                        genericSuccess();
                    }else{
                        genericFail(5);
                    }
                }else{
                    genericFail(6);
                }
            }else{
                $stmt = $db->customExecute("DELETE FROM ad_vb_sequences WHERE id = ?");
                if($stmt->execute(array($seqid))){
                    $stmt = $db->customExecute("SELECT id,cron_id FROM ad_vb_sequence_schedule WHERE sequence_id = ?");
                    $stmt->execute(array($seqid));
                    foreach($stmt->fetchAll(PDO::FETCH_OBJ) as $sequence){
                        ad_del_cron_task($sequence->cron_id);
                        $db->customExecute("DELETE FROM ad_vb_sequence_schedule WHERE id = ?")->execute(array($sequence->id));
                    }
                    genericSuccess();
                }else{
                    genericFail(4);
                }
            }
        }
    }
}
function USR_AD_CSDEL($params=''){
    $db = new DB();
    if(!isset($params['user_id']))
        genericFail(1);
    else{
        if($params['data']['id']!=""){
            $userid = $_SESSION['user_id'];
            $stmt = $db->customExecute("SELECT * FROM ad_callscripts WHERE user_id = ? AND id = ?");
            if($stmt->execute(array($userid,$params['data']['id']))){
                $data = $stmt->fetchAll(PDO::FETCH_OBJ);
                if(count($data)==0)
                    genericFail(2);
                else{
                    $stmt2 = $db->customExecute("DELETE FROM ad_callscripts WHERE id = ?");
                    $stmt2->execute(array($params['data']['id']));
                    genericSuccess();
                }
            }else{
                genericFail(4);
            }
        }else{
            genericFail(3);
        }
    }
}
function USR_AD_CSSAVE($params=''){
    $db = new DB();
    if(!isset($params['user_id']))
        genericFail(1);
    else{
        if($params['data']['contents']=="")
            genericFail(2);
        $userid = $_SESSION['user_id'];
        $stmt = $db->customExecute("INSERT INTO ad_callscripts (user_id, contents) VALUES (?,?)");
        if($stmt->execute(array($userid,$params['data']['contents']))){
            genericSuccess();
        }else{
            genericFail(3);
        }
    }
}
function USR_AD_CSLIST($params=''){
    $db = new DB();
    if(!isset($params['user_id']))
        genericFail(1);
    else{
        $userid = $_SESSION['user_id'];
        $stmt = $db->customExecute("SELECT * FROM ad_callscripts WHERE user_id = ?");
        if($stmt->execute(array($userid))){
            $data = $stmt->fetchAll(PDO::FETCH_OBJ);
            die(json_encode($data));
        }else{
            genericFail(2);
        }
    }
}
function USR_GET_NOTES($params='')
{
    $db = new DB();
    $CallSid = $params['data']['CallSid'];
    $list_html = "";

    foreach($db->getCallNotes($CallSid) as $notes){
        $date = new DateTime_52($notes['DateAdded']);
        $list_html .= '<li style="padding-left: 15px; background: url(images/li.gif) 0 5px no-repeat;">'.$notes["NoteContents"].'<span style="float:right;"><span style="color: #999;">['.$notes["User"].' @ '.$date->format("n\/j\/y").']</span></span></li>';
    }

    die(json_encode(array("data"=>$list_html)));
}
function USR_ADD_NUM_TO_BLACKLIST($params='')
{
    $db = new DB();
    $number = $params['data']['number'];
    $company_id = addslashes($params['data']['company_id']);

    if($db->isNumberBlocked($company_id,$number))
        die(json_encode(array("result"=>"exists")));

    $sql  = "SELECT `blacklist_id` FROM companies WHERE `idx`=$company_id";
    $qres = $db->customQuery($sql);

    $id = 0;

    if($qres!=false)
    {
        $res = $qres->fetch();
        $id = $res['blacklist_id'];
    }

    if($id==0)
        die(json_encode(array("result"=>"not_assigned")));

    $blacklist = $db->getBlacklist($id);
    if(substr($blacklist, -1, 1)==",")
        $blacklist = $blacklist . $number;
    else
        $blacklist = $blacklist . "," . $number;

    if($db->editBlacklist($id,$blacklist))
        die(json_encode(array("result"=>"success")));
    else
        genericFail();
}

function USR_GET_PHONE_CODE_REPORT_2($params=''){
    $db        = new DB();
    $companyid = $params['data']['companyid'];
    $codes = $db->getPhoneCodes($companyid,2);
    $html = "<option value='0'>All / None</option> ";
    if($codes!="")
    {
        foreach($codes as $code)
        {
            $html .= "<option value='$code->idx'>$code->name</option> ";
        }
    }
    die(json_encode(array("data"=>$html)));
}

function USR_GET_PHONE_CODE_REPORT($params='')
{
    $db        = new DB();
    $companyid = $params['data']['companyid'];
    $codes = $db->getPhoneCodes($companyid);
    $html = "<option value='0'>All / None</option> ";
    if($codes!="")
    {
        foreach($codes as $code)
        {
            $html .= "<option value='$code->idx'>$code->name</option> ";
        }
    }
    die(json_encode(array("data"=>$html)));
}

function USR_GET_OUTGOING_PHONE_CODE_REPORT($params='')
{
    $db        = new DB();
    $companyid = $params['data']['companyid'];
    $numbers = $db->getNumbersOfCompany($companyid);
    $html = "<option value=''>All / None</option> ";
    if($numbers!="")
    {
        foreach($numbers as $number)
        {
            $html .= "<option value='$number->number'>$number->number</option> ";
        }
    }
    die(json_encode(array("data"=>$html)));
}

function USR_GET_COMPANY_CAMPAIGNS($params='')
{
    global $AccountSid, $AuthToken, $ApiVersion;
    $client = new Services_Twilio($AccountSid,$AuthToken);
    $db        = new DB();
    $companyid = $params['data']['companyid'];
    $numbers = $db->getNumbersOfCompany($companyid);

    $campaigns = array();
    foreach ($client->account->incoming_phone_numbers as $number) {
        $this_phone_number = $db->format_phone_db($number->phone_number);
        foreach ($numbers as $number2) {
            if ($number2->number == $this_phone_number) {
                if (!isset($campaigns[$number->friendly_name]))
                    $campaigns[$number->friendly_name] = array();

                $campaigns[$number->friendly_name][] = $this_phone_number;
                break;
            }
        }
    }

    $html = "<option value=''>All / None</option> ";
    if($numbers!="")
    {
        foreach($campaigns as $name=>$campaign)
        {
            $html .= "<option value='$name'>$name</option> ";
        }
    }
    die(json_encode(array("data"=>$html)));
}

function USR_CHANGE_PHONE_CODE($params='')
{
    $db        = new DB();
    $call_sid  = $params['data']['callsid'];
    $phonecode = $params['data']['phonecode'];
    $type      = $params['data']['type'];

    $db->setPhoneCode($call_sid,$phonecode,$type);
}
function USR_SEND_EMAIL($params='')
{
    $email_to = $params['data']['email'];
    $subject  = $params['data']['subject'];
    $contents = $params['data']['contents'];
    if(Util::isValidEmail($email_to))
    {
        if(mail($email_to,$subject,$contents))
            die(json_encode(array("result"=>"success","response"=>array($email_to,$subject,$contents))));
        else
            die(json_encode(array("result"=>"failed","response"=>"Mail failed to deliver.")));
    }else{
        die(json_encode(array("result"=>"failed","response"=>"Invalid email!")));
    }
}
function USR_ADD_NOTE($params='')
{
    $db = new DB();
    $result = $db->addNote($params['data']['CallSid'],$params['data']['Contents'],$_SESSION['user_id']);
    if($result[0])
        die("SUCCESS:::<li data-id='$result[1]'>".$params['data']['Contents']."<span style='float:right;'><span style='color: #999;'>[".$result[3]." @ ".date("n\/j\/y",strtotime($result[2]))."]</span>&nbsp;&nbsp;<a href=\"#\" id=\"USR_DELETE_NOTE\" data-params=\"".$result[1]."\" title=\"Delete Note\"><img align=\"top\" src=\"images/delete.gif\"></a></span></li>:::$result[1]");
    else
        die("FAILURE:::$result[1]");
}
function USR_CHANGE_PASSWORD($params='')
{
    $db = new DB();
    $result = $db->changePassword($_SESSION['user_id'],$params['data']['password']);
    if($result)
        die("SUCCESS:::");
    else
        die("FAILURE:::$result[1]");
}
function USR_DELETE_NOTE($params='')
{
	$db = new DB();
	if($_SESSION['permission']>=1)
		$result = $db->deleteNote($params['data']['CallSid'],$params['data']['noteid'],$_SESSION['user_id'],true);
	else
		$result = $db->deleteNote($params['data']['CallSid'],$params['data']['noteid'],$_SESSION['user_id']);
	
	if($result[0])
		die("SUCCESS:::");
	else{
		if($result[1]==2)
			die("FAILURE:::USER_NOT_IN_COMPANY");
		else
			die("FAILURE:::NOTE_NOT_IN_DB");
	}
}

function USR_BROWSERPHONE_TOGGLE_MUTE($params='') {
    if ($params['action'] == "mute") {
        $_SESSION['browsersoftphone_muted'] = true;
    }
    else {
        $_SESSION['browsersoftphone_muted'] = false;
    }

    exit();
}

function USER_PUT_CALL_ON_HOLD($params='') {
    global $db,$AccountSid,$AuthToken,$ApiVersion;

    $i_am_host = $_REQUEST['i_am_host'];
    $conference_name = $_REQUEST['conference_name'];

    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array(trim($conference_name)));
    $conference = $sth->fetch(PDO::FETCH_OBJ);
    $conference->participants = json_decode($conference->participants, true);

    $new_participants = array();
    foreach ($conference->participants as $key => $participant) {
        if ($participant['type'] == "agent" && $participant['user_id'] == $_SESSION['user_id']) {
            $participant['on_hold'] = 1;
        }
        else {
            if ($i_am_host == 1 && ((@$_REQUEST['ignore_hold_everybody'] != 1) || (@$_REQUEST['ignore_hold_everybody'] == 1 && count($conference->participants) <= 2))) { $participant['forced_hold'] = 1; }
        }
        $new_participants[] = $participant;
    }

    $sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
    $sth->execute(array(json_encode($new_participants), $conference->id));

    if ($i_am_host == 1 && ((@$_REQUEST['ignore_hold_everybody'] != 1) || (@$_REQUEST['ignore_hold_everybody'] == 1 && count($conference->participants) <= 2))) {
        $client = new Services_Twilio($AccountSid,$AuthToken);
        foreach ($conference->participants as $participant) {
            if (@$participant['pending'] != 1 && !empty($participant['call_sid']) && $participant['user_id'] != $_SESSION['user_id']) {
                $call = $client->account->calls->get($participant['call_sid']);

                $sth = $db->customExecute("UPDATE call_conferences SET on_hold = 1 WHERE id = ?");
                $sth->execute(array($conference->id));
                $url = dirname(s8_get_current_webpage_uri()) . "/ivrmenu.php?company_id=".$_SESSION['sel_co']."&action=put_on_hold&queue_name="."user_ongoing_".$_SESSION['user_id']."&conference_name=".$conference_name;
                try {
                    $call->update(array(
                        "Url" => $url,
                        "Method" => "POST"
                    ));
                }
                catch (Exception $e) {}

                $sth = $db->customExecute("UPDATE call_conferences SET on_hold = 0 WHERE id = ?");
                $sth->execute(array($conference->id));
            }
        }
    }

    exit();
}

function USER_UPDATE_STATUS($params='') {
    global $db;

    $db->setUserStatus($_SESSION['user_id'], $params['status']);

    exit();
}

function USR_DECLINE_INCOMING_CALL($params='') {
    global $db,$AccountSid,$AuthToken,$ApiVersion;
    $client = new Services_Twilio($AccountSid,$AuthToken);

    $call = $client->account->calls->get($params['call_sid']);

    $sth = $db->customExecute("DELETE FROM calls_missed WHERE call_sid = ?");
    $sth->execute(array($params['call_sid']));

    $call->update(array(
        "Url" => dirname(s8_get_current_webpage_uri()) . "/ivrmenu.php?company_id=".$_SESSION['sel_co']."&action=decline",
        "Method" => "POST"
    ));

    exit();
}

function USR_DELETE_MISSED_CALL($params='') {
    global $db,$AccountSid,$AuthToken,$ApiVersion;
    $client = new Services_Twilio($AccountSid,$AuthToken);

    $call = $client->account->calls->get($params['call_sid']);

    $sth = $db->customExecute("DELETE FROM calls_missed WHERE call_sid = ?");
    $sth->execute(array($params['call_sid']));

    exit();
}

function USR_REMOVE_PERSON_FROM_CALL($params='') {
    global $db;

    global $AccountSid,$AuthToken,$ApiVersion;
    $client = new Services_Twilio($AccountSid,$AuthToken);

    $conference_name = trim($_REQUEST['conference_name']);
    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array($conference_name));
    $conference = $sth->fetch(PDO::FETCH_OBJ);

    $conference->participants = json_decode($conference->participants, true);

    foreach ($conference->participants as $key => $participant) {
        if ($participant['call_sid'] == $params['call_sid']) {

        }
        else {
            $new_participants[] = $participant;
        }
    }

    $sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
    $sth->execute(array(json_encode($new_participants), $conference->id));

    $call = $client->account->calls->get($params['call_sid']);

    $call->update(array(
        "Url" => dirname(s8_get_current_webpage_uri()) . "/ivrmenu.php?company_id=".$_SESSION['sel_co']."&action=remove_from_call",
        "Method" => "POST"
    ));

    exit();
}

function USR_ADD_ANOTHER_AGENT_TO_CALL($params='') {
    global $db;

    $conference_name = $params['conference_name'];
    $agent = $params['agent'];

    $conference_name = trim($_REQUEST['conference_name']);

    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array($conference_name));
    $conference = $sth->fetch();

    $participants = json_decode($conference['participants'], true);

    $handle = $_REQUEST['From'];

    $participants[$key] = array(
        "type" => "agent",
        "user_id" => $agent['user_id'],
        "handle" => $agent['handle'],
        "call_sid" => "",
        "is_host" => false,
        "pending" => 1,
        "time_added" => strtotime("now")
    );

    $sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
    $sth->execute(array(json_encode($participants), $conference['id']));
}

function USR_ADD_PHONE_NUMBER_TO_CALL($params='') {
    global $db;
    global $AccountSid,$AuthToken,$ApiVersion;
    $client = new Services_Twilio($AccountSid, $AuthToken);

    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array($params['conference_name']));
    $conference = $sth->fetch();

    $participants = json_decode($conference['participants'], true);

    $user_id = rand(1, 99999999);
    $participants[] = array(
        "type" => "phone_number",
        "user_id" => $user_id,
        "handle" => $params['phone_number'],
        "call_sid" => "",
        "is_host" => false,
        "pending" => 1
    );

    $sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
    $sth->execute(array(json_encode($participants), $conference['id']));

    $action_url = dirname(s8_get_current_webpage_uri()) . "/include/twilio_dial_number.php?company_id=".$params['company_id']."&action=answer_incoming_call&conference_name=".$params['conference_name']."&caller_is_agent=1&phone_number=".$params['phone_number']."&user_id=".$user_id."&call_sid=".$params['call_sid']."&company_id=".$_SESSION['sel_co'];

    $call = $client->account->calls->create($params['caller_id'], $params['phone_number'], $action_url, array(
        "method" => "post"));
}

function USR_ADD_INCOMING_CALL_TO_CONFERENCE($params='') {
    global $db;
    global $AccountSid,$AuthToken,$ApiVersion;
    $client = new Services_Twilio($AccountSid, $AuthToken);

    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array($params['conference_name']));
    $conference = $sth->fetch();

    $participants = json_decode($conference['participants'], true);

    if ($_REQUEST['redo_current_call_sid']) {
        foreach ($participants as $participant) {
            if ($participant['user_id'] != $_SESSION['user_id']) {
                 try {
                    $call = $client->account->calls->get($participant['call_sid']);
                    $call->update(array(
                        "Url" => dirname(s8_get_current_webpage_uri()).'/ivrmenu.php?company_id=' . @$_SESSION['sel_co'] . '&action=activate_answer&conference_name='.$conference['name'].'&caller_is_agent=1',
                        "Method" => "POST"
                    ));
                }
                catch (Exception $e) {}
            }
        }

        foreach ($participants as $participant) {
            if ($participant['user_id'] == $_SESSION['user_id']) {
                 try {
                    $call = $client->account->calls->get($participant['call_sid']);
                    $call->update(array(
                        "Url" => dirname(s8_get_current_webpage_uri()).'/ivrmenu.php?company_id=' . @$_SESSION['sel_co'] . '&action=activate_answer&conference_name='.$conference['name'].'&caller_is_agent=1',
                        "Method" => "POST"
                    ));
                }
                catch (Exception $e) {}
            }
        }
    }

    $participants[] = $params['member_to_add'];

    $sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
    $sth->execute(array(json_encode($participants), $conference['id']));

    $call_sid = $params['member_to_add']['call_sid'];

    $sth = $db->customExecute("DELETE FROM calls_missed WHERE call_sid = ?");
    $sth->execute(array($params['member_to_add']['call_sid']));

    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE `from` = ?");
    $sth->execute(array($params['member_to_add']['handle']));
    $prev_conference = $sth->fetch();

    if ($conference) {
        $sth = $db->customExecute("UPDATE call_conferences SET moved_to_conference = ? WHERE `from` = ?");
        $sth->execute(array($params['conference_name'], $params['member_to_add']['handle']));
    }

    try {
        $call = $client->account->calls->get($call_sid);
        $call->update(array(
            "Url" => dirname(s8_get_current_webpage_uri()).'/ivrmenu.php?company_id=' . @$_SESSION['sel_co'] . '&action=activate_answer&conference_name='.$params['conference_name'].'&caller_is_agent=1',
            "Method" => "POST"
        ));
    }
    catch (Exception $e) {}
}

function USR_ADD_ONGOING_CALL_TO_CONFERENCE($params='') {
    global $db;
    global $AccountSid,$AuthToken,$ApiVersion;
    $client = new Services_Twilio($AccountSid, $AuthToken);

    $current_conference = $_REQUEST['current_conference'];
    $ongoing_conference = $_REQUEST['ongoing_conference'];

    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array($current_conference));
    $current_conference = $sth->fetch();

    $sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
    $sth->execute(array($ongoing_conference));
    $ongoing_conference = $sth->fetch();

    $current_participants = json_decode($current_conference['participants'], true);
    $ongoing_participants = json_decode($ongoing_conference['participants'], true);

    $new_participants = array();
    foreach ($current_participants as $participant) {
        $new_participants[] = $participant;

        if ($_REQUEST['redo_current_call_sid'] == 1 && $participant['user_id'] != $_SESSION['user_id']) {
             try {
                $call = $client->account->calls->get($participant['call_sid']);
                $call->update(array(
                    "Url" => dirname(s8_get_current_webpage_uri()).'/ivrmenu.php?company_id=' . @$_SESSION['sel_co'] . '&action=activate_answer&conference_name='.$current_conference['name'].'&caller_is_agent=1',
                    "Method" => "POST"
                ));
            }
            catch (Exception $e) {}
        }
    }

    foreach ($current_participants as $participant) {
        if ($_REQUEST['redo_current_call_sid'] == 1 && $participant['user_id'] == $_SESSION['user_id']) {
             try {
                $call = $client->account->calls->get($participant['call_sid']);
                $call->update(array(
                    "Url" => dirname(s8_get_current_webpage_uri()).'/ivrmenu.php?company_id=' . @$_SESSION['sel_co'] . '&action=activate_answer&conference_name='.$current_conference['name'].'&caller_is_agent=1',
                    "Method" => "POST"
                ));
            }
            catch (Exception $e) {}
        }
    }

    $incoming_call_sid = "";
    foreach ($ongoing_participants as $participant) {
        if ($participant['user_id'] != $_SESSION['user_id']) {
            $participant['is_host'] = false;
            $participant['forced_hold'] = 0;
            $new_participants[] = $participant;

            try {
                $call = $client->account->calls->get($participant['call_sid']);
                $call->update(array(
                    "Url" => dirname(s8_get_current_webpage_uri()).'/ivrmenu.php?company_id=' . @$_SESSION['sel_co'] . '&action=activate_answer&conference_name='.$current_conference['name'].'&caller_is_agent=1',
                    "Method" => "POST"
                ));
            }
            catch (Exception $e) {}
        }

        if (@$participant['is_caller']) {
            $incoming_call_sid = $participant['call_sid'];
        }
    }

    if (!empty($incoming_call_sid)) {
        $call = $db->getCallDetails($incoming_call_sid);
        $call_participants = (empty($call['participants'])) ? array() : json_decode($call['participant']);

        foreach ($new_participants as $new_participant) {
            if (!in_array($new_participant['handle'], $call_participants))
                $call_participants[] = $new_participant['handle'];
        }

        $sth = $db->customExecute("UPDATE calls SET participants = ? WHERE CallSid = ?");
        $sth->execute(array(json_encode($call_participants), $incoming_call_sid));
    }

    $sth = $db->customExecute("UPDATE call_conferences set participants = ? WHERE id = ?");
    $sth->execute(array(json_encode($new_participants), $current_conference['id']));

    $sth = $db->customExecute("UPDATE call_conferences SET moved_to_conference = ?, participants = '' WHERE id = ?");
    $sth->execute(array($current_conference["name"], $ongoing_conference['id']));
}

function USR_REFRESH_TWILIO_TOKEN($params='') {
    include('include/twilio_header.php');
    echo generate_twilio_auth_token();
    exit();
}


//
// Utility
//
function genericFail($msg = null){
    $res = array("result"=>"fail");

    if($msg!=null)
        $res = array("result"=>"fail","reason"=>$msg);

    die(json_encode($res));
}
function genericSuccess($data = null){
    if($data==null)
        die(json_encode(array("result"=>"success")));
    else{
        $arr = array("result"=>"success");
        $arr = array_merge($arr,$data);
        die(json_encode($arr));
    }
}
