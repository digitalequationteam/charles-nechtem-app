/*!
 * Web1 CTM Client JS
 *
 * Copyright 2012, Web1Syndication
 */
$(document).ready(function(e) {
	$("#submit_btn").click(function(e) {
		generateReport();
	});
    $(document).keydown(function(e){
        if (e.keyCode == 13 && e.srcElement.nodeName != "TEXTAREA") {
            if($('button:contains("Send")')[0]){
                return true;
            }else{
                e.preventDefault();
                $('button:contains("Ok"),button:contains("Save"),button:contains("Yes")').click();
            }
        }
    });
	var dates = $( "#start_date, #end_date" ).datepicker({
				changeMonth: true,
				numberOfMonths: 2,
				onSelect: function( selectedDate ) {
					var option = this.id == "start_date" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" ),
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
});

function generateReport()
{
    var report_page = "call_report.php";
    if(typeof $("#outgoing_report") != "undefined"){
        if($("#outgoing_report").val()=="true"){
            report_page = "call_report_outgoing.php";
        }
    }

    if($("#report_type").val()=="phone_code_rep")
    {
        $("#phone_code").val("0");
    }
    
    if($("#report_type").val()=="phone_code_rep" && $("#company_select").val()=="-1")
    {
        errMsgDialog("You can not generate a Phone Code Report for All companies.");
        return false;
    }
    
    if($("#report_type").val()=="campaign_rep" && $("#company_select").val()=="-1")
    {
        errMsgDialog("You can not generate a Campaign Report for All companies.");
        return false;
    }

	if($("#start_date").datepicker("getDate")==null || $("#end_date").datepicker("getDate")==null)
	{
		errMsgDialog("Please select a date range.");
		return false;
	}
	if($("#company_select").val()=="none")
	{
		errMsgDialog("Please select a company.");
		return false;
	}
	console.log("[Debug] Start Date:[%s], End Date:[%s], Company:[%s : %s], Call Result:[%s : %s]",
				$("#start_date").datepicker("getDate").toString(),$("#end_date").datepicker("getDate").toString(),
				$("#company_select").val(),$("#company_select").find(":selected").text(),$("#call_result").val(),$("#call_result").find(":selected").text());
	$(".filter-calls").block({
	message: '<h1 style="color:#fff">Loading...</h1>',
	css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .7, 
            color: '#fff !important' 
        } }); 

    var ca = encodeURIComponent($("#campaign").val());
    if (ca == "false") ca = "";

    var pc = encodeURIComponent($("#phone_code").val());
    if (pc == "false") pc = "";

    var on = encodeURIComponent($("#outgoing_number").val());
    if (on == "false") on = "";

    if($("#report_type").val()=="default")
    {
        var user = "";
        if(typeof $("#user").val() !== "undefined" && $("#user").val() != "0")
            user = "&user="+encodeURIComponent($("#user").val());
        window.location = report_page+"?sd="+encodeURIComponent($("#start_date").val())
                          +"&ed="+encodeURIComponent($("#end_date").val())
                          +"&cm="+encodeURIComponent($("#company_select").val())
                          +"&cr="+encodeURIComponent($("#call_result").val())
                          +"&pc="+pc
                          +"&rt="+encodeURIComponent($("#report_type").val())
                          +"&on="+on
                          +"&ca="+ca
                          +user
                          +"&page=1";
    }else{
        window.open(report_page+"?sd="+encodeURIComponent($("#start_date").val())
            +"&ed="+encodeURIComponent($("#end_date").val())
            +"&cm="+encodeURIComponent($("#company_select").val())
            +"&cr="+encodeURIComponent($("#call_result").val())
            +"&pc="+pc
            +"&rt="+encodeURIComponent($("#report_type").val())
            +"&on="+on
            +"&ca="+ca
            +"&page=1");
        $(".filter-calls").unblock();
    }
}

function funcs(elm)
{
	//alert($(this).attr('id'));
	var params = $(elm).attr("data-params");
	
	console.log("[DEBUG] Executing: [" + $(elm).attr('id') + "] with params [" + params + "]");
	
	switch ($(elm).attr('id')) {
        case "USR_ADD_NOTE_FROM_LIST":
        {
            var addNoteHTML = '<p>\
            <ul id="note_list" style="height: 150px; overflow: auto"></ul>\
            </p><label for="note_contents">Contents:</label>\
<input id="note_contents" type="text" style="width: 455px; height: 25px; padding-left:10px;">';
            var $dialog = $('<div></div>').html(addNoteHTML).dialog({
                modal:     true,
                autoOpen:  true,
                width:     490,
                minWidth:  490,
                maxWidth:  490,
                height:    350,
                minHeight: 350,
                maxHeight: 350,
                title:     '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Add Note',
                buttons:   [{
                    text: 'Add',
                    id:   'addbtn',
                    click: function(){
                        var note_contents = $("#note_contents").val();
                        if(note_contents.length==0)
                        {
                            msgDialog("Please enter a note!");
                            return false;
                        }else{
                            $("#addbtn").attr("disabled","disabled");
                            var params = {
                                Contents: note_contents,
                                CallSid: $(elm).attr("data-callsid")
                            };
                            var response = userAjaxCall({
                                func: "USR_ADD_NOTE",
                                data: params
                            });
                            var dialog = this;
                            response.done(function (msg) {
                                var msg = msg.split(":::");
                                console.log(msg);
                                if (msg[0] == "SUCCESS") {
                                    notifyWithCheckmark("Note Added!");
                                    $(dialog).dialog("close");
                                    $(dialog).dialog('destroy').remove();
                                } else {
                                    errMsgDialog("There was an error in the request.");
                                    $(dialog).dialog("close");
                                    $(dialog).dialog('destroy').remove();
                                }
                            });
                        }
                    }
                }, {
                    text: "Cancel",
                    click: function () {
                        $(this).dialog("close");
                        $(this).dialog('destroy').remove()
                    }
                }]
            });
            var response = JSONuserajaxCall({
                func: "USR_GET_NOTES",
                data: {CallSid:$(elm).attr("data-callsid")}
            });
            response.done(function(res){
                $("#note_list").html(res.data);
            });

            break;
        }
        case "USR_DELETE_NOTE":
        {
            //infoMsgDialog("Under Development.");
            console.log("[DEBUG] noteID: ["+params+"]");
            var params = {
                noteid: params,
                CallSid: $("#CallSid").val()
            }
            var response = userAjaxCall({
                func: "USR_DELETE_NOTE",
                data: params
            });
            response.done(function (msg) {
                var msg = msg.split(":::");
                console.log(msg);
                if (msg[0] == "SUCCESS") {
                    notifyWithCheckmark("Note Deleted");
                    $("li[data-id='"+params['noteid']+"']").remove();
                }else if(msg[0]=="FAILURE"){
                    switch(msg[1])
                    {
                        case "USER_NOT_IN_COMPANY":
                            errMsgDialog("You are not in this company.");
                            break;
                        case "NOTE_NOT_IN_DB":
                            errMsgDialog("The note was not found in the DB.");
                            break;
                    }
                }
            });
            break;
        }
        case "USR_CHANGE_PASSWORD":
        {
            var editPasswordHTML = '<p style="font-size:15px;">\
<form>\
<fieldset>\
<label for="pass1">New Password:</label>\
<input type="password" name="pass1" id="pass1" class="text ui-widget-content ui-corner-all" value="" /><br>\
<label for="pass2">New Password (Confirm):</label>\
<input type="password" name="pass2" id="pass2" class="text ui-widget-content ui-corner-all" value="" />\
</fieldset>\
</form></p>';
            var $dialog = $('<div></div>').html(editPasswordHTML).dialog({
                modal: true,
                autoOpen: true,
                minHeight: 177,
                maxWidth: 587,
                minWidth: 250,
                height: 'auto',
                title: '<span class="ui-button-icon-primary ui-icon ui-icon-key" style="float:left; margin-right:5px;"></span>\
Change Password',
                buttons: [{
                    text: "Ok",
                    click: function () {
                        if ($("#pass1").val() == "" || $("#pass2").val() == "") {
                            msgDialog("Please fill in all fields.");
                            return false;
                        }
                        if($("#pass1").val() != $("#pass2").val())
                        {
                            msgDialog("Passwords do not match.");
                            return false;
                        }
                        if ($("#pass1").val().length < 5) {
                            errMsgDialog("Please enter more than 5 characters.");
                            return false;
                        } else {
                            var aparams = {
                                password: $("#pass1").val()
                            }
                            var response = userAjaxCall({
                                func: "USR_CHANGE_PASSWORD",
                                data: aparams
                            });
                            response.done(function (msg) {
                                var msg = msg.split(":::");
                                console.log(msg);
                                if (msg[0] == "SUCCESS") {
                                    $($dialog).dialog("close");
                                    $($dialog).dialog('destroy').remove();
                                    notifyWithCheckmark("Password changed.");

                                } else errMsgDialog("There was an error in the request.");
                            });
                        }
                    }
                }, {
                    text: "Cancel",
                    click: function () {
                        $(this).dialog("close");
                        $(this).dialog('destroy').remove()
                    }
                }]
            });
            break;
        }
        case "USR_ADD_NUM_TO_BLACKLIST":
        {
            promptMsg("Are you sure you want to blacklist this number?",function(){
                var response = JSONuserajaxCall({
                    func: "USR_ADD_NUM_TO_BLACKLIST",
                    data: {number:params,company_id:$("#USR_ADD_NUM_TO_BLACKLIST").attr("data-companyid")}
                });
                response.done(function (msg) {
                    if(msg.result=="success")
                    {
                        infoMsgDialog("Number was added to the blacklist.");
                        return;
                    }
                    if(msg.result=="exists")
                    {
                        infoMsgDialog("That number is already on the blacklist.");
                        return;
                    }
                    if(msg.result=="not_assigned")
                    {
                        errMsgDialog("No blacklist is assigned to this company.");
                    }else{
                        errMsgDialog("There was an error in the request.");
                    }
                });
            });
            break;
        }
        case "USR_ADD_NOTE":
        {
            var note_contents = $("#note").val();
            if(note_contents.length==0)
            {
                errMsgDialog("Please enter a note.");
                return false;
            }
            var params = {
                Contents: note_contents,
                CallSid: $("#CallSid").val()
            }
            var response = userAjaxCall({
                func: "USR_ADD_NOTE",
                data: params
            });
            response.done(function (msg) {
                var msg = msg.split(":::");
                console.log(msg);
                if (msg[0] == "SUCCESS") {
                    notifyWithCheckmark("Note Added");
                    $("#note_container ul").append(msg[1]);
                    $("a[data-params="+msg[2]+"]").click(function(e){funcs(this)});
                    $("#note").val("");
                } else errMsgDialog("There was an error in the request.");
            });
            break;
        }

        case "USR_SEND_RECORDING_LINK":
        {
            var sendRecordingLinkHTML = '<p style="font-size:15px;">\
<form>\
<fieldset>\
<label for="email_to">To:</label>\
<input type="text" name="email_to" id="email_to" class="text ui-widget-content ui-corner-all" value="" style="width:270px;" /><br>\
<label for="contents">Contents:</label>\
<textarea name="contents" id="contents" class="text ui-widget-content ui-corner-all" style="width:100%; height: 104px;"></textarea>\
</fieldset>\
</form></p>';
            var $dialog = $('<div></div>').html(sendRecordingLinkHTML).dialog({
                modal: true,
                autoOpen: true,
                minHeight: 177,
                maxWidth: 587,
                minWidth: 587,
                height: 'auto',
                title: '<span class="ui-button-icon-primary ui-icon ui-icon-mail-closed" style="float:left; margin-right:5px;"></span>\
                Send Recording Link',
                buttons: [{
                    text: "Send",
                    click: function () {
                        if ($("#email_to").val() == "") {
                            errMsgDialog("Please enter email.");
                            return false;
                        }
                        var params = {
                            email: $("#email_to").val(),
                            contents: $("#contents").val(),
                            subject: $.trim($(".block_head > h2").text())
                        };
                        var response = JSONuserajaxCall({
                            func: "USR_SEND_EMAIL",
                            data: params
                        });
                        response.done(function (msg) {
                            switch(msg.result)
                            {
                                case "failed":
                                    errMsgDialog(msg.response);
                                    break;

                                case "success":
                                    msgDialogCB("Email Sent!",function(e){
                                        $($dialog).dialog("close");
                                        $($dialog).dialog('destroy').remove();
                                    });
                                    break;

                                default:
                                    console.log(msg);
                                    errMsgDialog("An Unknown Error Has Occurred..");
                                    break;
                            }
                        });
                    }
                }, {
                    text: "Cancel",
                    click: function () {
                        $(this).dialog("close");
                        $(this).dialog('destroy').remove()
                    }
                }]
            });
            $("#contents").text($.trim($(".block_head > h2").text())+"\n\n"+"Recording Link: \n"+params);
            break;
        }
        case "USR_SEQ_DELETE":{
            promptMsg("Are you sure you want to delete this sequence?",function(){
                var response = JSONuserajaxCall({
                    func: "USR_SEQ_DEL",
                    data: params
                });
                response.done(function(data){
                    if(data.result=="success"){
                        infoMsgDialog("Sequence deleted!");
                        $("tr[data-bind='"+params+"']").remove();
                    }else{
                        errMsgDialog("An error has occurred.");
                    }
                });
            });
            break;
        }
        case "USR_AD_CSLOAD":{
            var $dialog = $('<div></div>').html('<center><span style="color: #AAA;width: 245px;margin-left: 15px;font: 10px Helvetica;">* Double-Click to select call script *</span></center><ol id="callscript"></ol>').dialog({modal: true,
                autoOpen: true,
                minWidth: 587,
                maxWidth: 587,
                height: 'auto',
                maxHeight: 560,
                close: function() {$($dialog).dialog('destroy').remove();},
                title: '<span class="ui-button-icon-primary ui-icon ui-icon-folder-open" style="float:left; margin-right:5px;"></span>Load Call Script',
                buttons: []});

            var response = JSONuserajaxCall({
                func: "USR_AD_CSLIST",
                data:{}
            });

            response.done(function(data){
                for(i=0; i < data.length; i++){
                    var contents = data[i].contents.replaceAll('"','&quot;');
                    $("#callscript").append("<li class='ui-widget-content' data-key='cs_"+data[i].id+"' title=\""+contents+"\" data-contents=\""+contents+"\">"+Truncate(data[i].contents,60,'...')+"<a class='callscript-delete' data-key='cs_"+data[i].id+"' href='#'><img src='images/delete.gif'></a></li>");
                }

                $("#callscript").selectable({selected: function(event, ui) {
                    $(ui.selected).siblings().removeClass("ui-selected");
                }});

                $("li[data-key^='cs_']").unbind().bind("dblclick",function(e){
                    var id = $(this).attr("data-key").replace("cs_","");
                    $("textarea[name='ad_ad_call_script']").text($("li[data-key='cs_"+id+"']").attr("data-contents"));
                    $("textarea[name='ad_ad_call_script']").val($("li[data-key='cs_"+id+"']").attr("data-contents"));
                    $($dialog).dialog("close");
                    $($dialog).dialog("destroy").remove();
                });

                $("a[data-key^='cs_']").unbind().bind("click",function(e){
                    e.preventDefault();
                    var id = $(this).attr("data-key").replace("cs_","");
                    promptMsg("Are you sure you want to delete this Call Script?",function(){
                        blockUI("body");
                        var response = JSONuserajaxCall({func:"USR_AD_CSDEL",data:{id:id}});
                        response.done(function(data){
                            $.unblockUI();
                            if(data.result=="success"){
                                infoMsgDialog("Template deleted!");
                                $("li[data-key='cs_"+id+"']").remove();
                            }else{
                                if(data.reason==2)
                                    errMsgDialog("You don't own that template!");
                                else
                                    errMsgDialog("An unknown error has occurred.");
                            }
                        });
                    });
                });

                $("li[data-key^='cs_']").tooltipster({
                    position: 'right',
                    theme:'.tooltipster-shadow'
                });
            });

            break;
        }
        case "USR_AD_CSSAVE":{
            blockUI("body");

            var response = JSONuserajaxCall({
                func: "USR_AD_CSSAVE",
                data: {contents:$("textarea[name='ad_ad_call_script']").val()}
            });

            response.done(function(data){
                $.unblockUI();
                if(data.result=="success"){
                    infoMsgDialog("Template saved successfully!");
                }else{
                    if(data.reason==1)
                        errMsgDialogCB("Not signed in!", function(){window.location = "login.php";});
                    else if(data.reason==2)
                        errMsgDialog("Please enter the Call Script.");
                    else
                        errMsgDialog("An unknown error has occurred.");
                }
            });
            break;
        }

        default:
            errMsgDialog("Unknown Function.");
            break;
    }

    //getAjax($(this).attr('id'),params);
    return false;
}

//function that converts colors from rgb to hex.
function hexc(colorval) {

	var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	
	delete(parts[0]);
	
	for (var i = 1; i <= 3; ++i) {
	
		parts[i] = parseInt(parts[i]).toString(16);
		
		if (parts[i].length == 1) {
		
			parts[i] = '0' + parts[i];
		}
	}
	
	return ('#' + parts.join(''));
}

$(document).ready(function () {
	$("*[id*=USR_]").click(function(e){
        e.preventDefault();
        funcs(this)
    });
});

function blockUI(selector){
    $(selector).block({
        message: '<h1 style="color:#fff">Loading</h1>',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .7,
            color: '#fff !important'
        }
    });
}
function changePhoneCode(callsid, phonecode, func, type)
{
    if(!type)
        var type = 1;

    var params = {
        callsid: callsid,
        phonecode: phonecode,
        type: type
    };
    var response = JSONuserajaxCall({
        func: "USR_CHANGE_PHONE_CODE",
        data: params
    });
    response.done(function(d){
       func();
    });
}
function companySelected(companyid,cb)
{
    if(companyid=="none")
    {
        $("#phone_code").attr("disabled","true");
        $("#outgoing_number").attr("disabled","true");
        $("#campaign").attr("disabled","true");
    }else{
        var params = {
            companyid:companyid
        };
        var response = JSONuserajaxCall({
            func: "USR_GET_PHONE_CODE_REPORT",
            data: params
        });
        response.done(function(d){
            $("#phone_code").removeAttr("disabled");
            $("#phone_code").html(d.data);
            if(cb)
                cb();
        });


        var response = JSONuserajaxCall({
            func: "USR_GET_OUTGOING_PHONE_CODE_REPORT",
            data: params
        });
        response.done(function(d){
            $("#outgoing_number").removeAttr("disabled");
            $("#outgoing_number").html(d.data);
            if(cb)
                cb();
        });


        var response = JSONuserajaxCall({
            func: "USR_GET_COMPANY_CAMPAIGNS",
            data: params
        });
        response.done(function(d){
            $("#campaign").removeAttr("disabled");
            $("#campaign").html(d.data);
            if(cb)
                cb();
        });
    }
}
function companySelectedOutgoing(companyid,cb)
{
    if(companyid=="none")
    {
        $("#phone_code").attr("disabled","true");
    }else{
        var params = {
            companyid:companyid
        };
        var response = JSONuserajaxCall({
            func: "USR_GET_PHONE_CODE_REPORT_2",
            data: params
        });
        response.done(function(d){
            $("#phone_code").removeAttr("disabled");
            $("#phone_code").html(d.data);
            if(cb)
                cb();
        });
        
        var response = JSONuserajaxCall({
            func: "USR_GET_COMPANY_CAMPAIGNS",
            data: params
        });
        response.done(function(d){
            $("#campaign").removeAttr("disabled");
            $("#campaign").html(d.data);
            if(cb)
                cb();
        });
    }
}

function userAjaxCall(data) {
    return $.ajax({
        url:"ajax_handle.php",
        type:"POST",
        data:data,
        cache:false
    });
}
function JSONuserajaxCall(data) {
    return $.ajax({
        url: "ajax_handle.php",
        type: "POST",
        data: data,
        dataType:"json",
        cache: false
    });
}

//Dialogs
function errMsgDialog(msgtext) {
    msgtext = '<div class="ui-widget">\
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; margin-top: 0;"> \
<p style="font-size:15px;"><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .7em; margin-top: 2px;"></span> \
<strong>Error:</strong> ' + msgtext + ' </p>\
</div>\
</div>';
    var $dialog = $('<div></div>').html(msgtext).dialog({
        modal: true,
        autoOpen: true,
        minHeight: 177,
        maxWidth: 587,
        height: 'auto',
        width: 'auto',
        title: '<span class="ui-button-icon-primary ui-icon ui-icon-alert" style="float:left; margin-right:5px;"></span>\
Error',
        buttons: [{
            text: "Ok",
            click: function () {
                $(this).dialog("close");
                $(this).dialog('destroy').remove()
            }
        }]
    });
}

function errMsgDialogCB(msgtext,callback) {
    msgtext = '<div class="ui-widget">\
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; margin-top: 0;"> \
<p style="font-size:15px;"><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .7em; margin-top: 2px;"></span> \
<strong>Error:</strong> ' + msgtext + ' </p>\
</div>\
</div>';
    var $dialog = $('<div></div>').html(msgtext).dialog({
        modal: true,
        autoOpen: true,
        minHeight: 177,
        maxWidth: 587,
        height: 'auto',
        width: 'auto',
        closeOnEscape: false,
        title: '<span class="ui-button-icon-primary ui-icon ui-icon-alert" style="float:left; margin-right:5px;"></span>\
Error',
        buttons: [{
            text: "Ok",
            click: function () {
                $(this).dialog("close");
                $(this).dialog('destroy').remove()
                callback();
            }
        }]
    });
    $(".ui-icon-closethick:visible").click(function() { callback(); });
}

function infoMsgDialog(msgtext) {
	msgtext = '<div class="ui-widget">\
<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> \
<p style="font-size:15px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .7em; margin-top: 2px;"></span>\
' + msgtext + '</p>\
</div>\
</div>';
	var $dialog = $('<div></div>').html(msgtext).dialog({
		modal: true,
		autoOpen: true,
		minHeight: 177,
		maxWidth: 587,
		height: 'auto',
		width: 'auto',
		title: '<span class="ui-button-icon-primary ui-icon ui-icon-alert" style="float:left; margin-right:5px;"></span>\
Info',
		buttons: [{
			text: "Ok",
			click: function () {
				$(this).dialog("close");
				$(this).dialog('destroy').remove()
			}
		}]
	});
}

function msgDialog(msgtext) {
	msgtext = '<p style="font-size:15px;">' + msgtext + '</p>';
	var $dialog = $('<div></div>').html(msgtext).dialog({
		modal: true,
		autoOpen: true,
		minHeight: 177,
		maxWidth: 587,
		height: 'auto',
		width: 'auto',
		title: '<span class="ui-button-icon-primary ui-icon ui-icon-info" style="float:left; margin-right:5px;"></span>\
Notice',
		buttons: [{
			text: "Ok",
			click: function () {
				$(this).dialog("close");
				$(this).dialog('destroy').remove()
			}
		}]
	});
}

function msgDialogCB(msgtext, cb) {
	msgtext = '<p style="font-size:15px;">' + msgtext + '</p>';
	var $dialog = $('<div></div>').html(msgtext).dialog({
		modal: true,
		autoOpen: true,
		minHeight: 177,
		maxWidth: 587,
		height: 'auto',
		width: 'auto',
		title: '<span class="ui-button-icon-primary ui-icon ui-icon-info" style="float:left; margin-right:5px;"></span>\
Notice',
		buttons: [{
			text: "Ok",
			click: function () {
				$(this).dialog("close");
				$(this).dialog('destroy').remove();
				cb();
			}
		}]
	});

    $(".ui-icon-closethick").click(function() {
        cb();
    });
}

function notifyWithCheckmark(msg)
{
	var $m = $('<div class="growlUI"></div>');
	$m.append('<h1>'+msg+'</h1>');
	var timeout = 3000;
	$.blockUI({ 
    	message: $m, fadeIn: 700, fadeOut: 1000, centerY: false,
    	timeout: timeout, showOverlay: false,
        css: $.blockUI.defaults.growlCSS
    }); 

}

function setCookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}
function getCookie2(c_name)
{
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name)
        {
            return unescape(y);
        }
    }
}

function promptMsg2(question, callbackyes,callbackno) {
    msgtext = '<p style="font-size:15px;">' + question + '</p>';
    var $dialog = $('<div></div>').html(msgtext).dialog({
        modal: true,
        autoOpen: true,
        minHeight: 177,
        maxWidth: 587,
        height: 'auto',
        width: 'auto',
        title: '<span class="ui-button-icon-primary ui-icon ui-icon-check" style="float:left; margin-right:5px;"></span>\
Question',
        buttons: [{
            text: "Yes",
            click: function () {
                $(this).dialog("close");
                $(this).dialog('destroy').remove()
                callbackyes();
            }
        }, {
            text: "No",
            click: function () {
                $(this).dialog("close");
                $(this).dialog('destroy').remove()
                callbackno();
            }
        }]
    });
}
function promptMsg(question, callback) {
    msgtext = '<p style="font-size:15px;">' + question + '</p>';
    var $dialog = $('<div></div>').html(msgtext).dialog({
        modal: true,
        autoOpen: true,
        minHeight: 177,
        maxWidth: 587,
        height: 'auto',
        width: 'auto',
        title: '<span class="ui-button-icon-primary ui-icon ui-icon-check" style="float:left; margin-right:5px;"></span>\
Question',
        buttons: [{
            text: "Yes",
            click: function () {
                $(this).dialog("close");
                $(this).dialog('destroy').remove()
                callback();
            }
        }, {
            text: "No",
            click: function () {
                $(this).dialog("close");
                $(this).dialog('destroy').remove()
            }
        }]
    });
}
String.prototype.wordWrap = function(m, b, c){
    var i, j, s, r = this.split("\n");
    if(m > 0) for(i in r){
        for(s = r[i], r[i] = ""; s.length > m;
            j = c ? m : (j = s.substr(0, m).match(/\S*$/)).input.length - j[0].length
                || m, r[i] += s.substr(0, j) + ((s = s.substr(j)).length ? b : ""));
        r[i] += s;}
    return r.join("\n");};
String.prototype.replaceAll = function(search, replace, ignoreCase) {
    if (ignoreCase) {
        var result = [];
        var _string = this.toLowerCase();
        var _search = search.toLowerCase();
        var start = 0, match, length = _search.length;
        while ((match = _string.indexOf(_search, start)) >= 0) {
            result.push(this.slice(start, match));
            start = match + length;
        }
        result.push(this.slice(start));
    } else {
        result = this.split(search);
    }
    return result.join(replace);
}
function preloadSound(src,cb) {
    var sound = document.createElement("audio");
    if ("src" in sound) {
        sound.autoPlay = false;
    }
    else {
        sound = document.createElement("bgsound");
        sound.volume = -10000;
    }
    sound.src = src;
    sound.onload = function (){cb();};
    document.body.appendChild(sound);
    return sound;
}
function setCookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}
function getCookie2(c_name)
{
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name)
        {
            return unescape(y);
        }
    }
}

function formatNumber(number){
    number = number.replace(/[^\d]/g, '');
    var country = countryForE164Number("+"+number);

    if (number.length == 10) country = "";

    if (number != "" && number != "No records found.") {
        if(country!="" && country!="FR"){
            formatted = formatLocal(country,"+" + number);
        }else{
            if(number[0]=="1")
                formatted = formatLocal("US",number);
            else
                formatted = formatLocal("US", "+1" + number);
        }

        if(formatted!="" && typeof formatted != 'undefined')
            number = formatted;
    }

    return number;
}

function checkIfCompanySelected(el, company_selected) {
    if (company_selected) {
        window.location = el.href;
    }
    else {
        errMsgDialogCB("You need to select a company.", function(){
            var url2 = window.location.href.replace("/companies.php");
            if (url2 == window.location.href) {
                window.location = 'companies.php';

            }
        });
        
    }
}

window.infoMsgDialog = infoMsgDialog;
window.errMsgDialog = errMsgDialog;
window.errMsgDialogCB = errMsgDialogCB;