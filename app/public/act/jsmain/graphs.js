// Web1 Graph Settings

var Web1Graphs = {

    graphOptions: {
        index_graph_options: {
            vAxis: {
                format: '#',
                viewWindowMode: 'explicit',
                viewWindow: {
                    min: 0
                }
            },
            hAxis: {
                textStyle: {
                    fontSize: 11
                }
            },
            legend: {
                position: 'top',
                textStyle: {
                    fontSize: 15
                }
            },
            chartArea: {
                width: '95%',
                height: '82%'
            },
            lineWidth: 4,
            pointSize: 9
        },
        bar_graph_options: {
            vAxis: {
                format: '#',
                viewWindow: {
                    min: 0
                }
            },
            legend: {
                position: 'top',
                textStyle: {
                    fontSize: 15
                }
            },
            chartArea: {
                width: '93%',
                height: '82%'
            },
            lineWidth: 4,
            pointSize: 9
        },
        calls_by_hour: {
            isStacked: true,
            vAxis: {
                format: '#',
                textStyle: {
                    fontSize: 9
                },
                viewWindow: {
                    min: 0
                }
            },
            hAxis: {
                textStyle: {
                    fontSize: 10
                }
            },
            legend: {
                position: 'top',
                textStyle: {
                    fontSize: 15
                }
            },
            chartArea: {
                width: '93%',
                height: '82%'
            },
            lineWidth: 4,
            pointSize: 9
        }
    },

    // What graph to display by default on index
    dashboardIndex: 0,

    // Actual data for the graphs.
    graphTypes: {
        callsToday: {
            graph_name: 'callsToday',
            friendly: "Today",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_TODAY",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        var options = Web1Graphs.graphOptions.index_graph_options;
                        options.hAxis.textStyle.fontSize = 9;
                        line_chart.draw(line_data, new Object(options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        callsYesterday: {
            graph_name: 'callsYesterday',
            friendly: "Yesterday",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_YESTERDAY",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        var options = Web1Graphs.graphOptions.index_graph_options;
                        options.hAxis.textStyle.fontSize = 9;
                        line_chart.draw(line_data, new Object(options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        callsThisWeek: {
            graph_name: 'callsThisWeek',
            friendly: "Last 7 Days",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_THIS_WEEK",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        line_chart.draw(line_data, new Object(Web1Graphs.graphOptions.index_graph_options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        calls14Days: {
            graph_name: 'calls14Days',
            friendly: "Last 14 Days",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_14_DAYS",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        line_chart.draw(line_data, new Object(Web1Graphs.graphOptions.index_graph_options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        calls30Days: {
            graph_name: 'calls30Days',
            friendly: "Last 30 Days",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_30_DAYS",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        line_chart.draw(line_data, new Object(Web1Graphs.graphOptions.index_graph_options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        calls60Days: {
            graph_name: 'calls60Days',
            friendly: "Last 60 Days",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_60_DAYS",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        line_chart.draw(line_data, new Object(Web1Graphs.graphOptions.index_graph_options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        calls90Days: {
            graph_name: 'calls90Days',
            friendly: "Last 90 Days",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_90_DAYS",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        line_chart.draw(line_data, new Object(Web1Graphs.graphOptions.index_graph_options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        callsThisMonth: {
            graph_name: 'callsThisMonth',
            friendly: "This Month",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_THIS_MONTH",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        line_chart.draw(line_data, new Object(Web1Graphs.graphOptions.index_graph_options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        callsLastMonth: {
            graph_name: 'callsLastMonth',
            friendly: "Last Month",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_LAST_MONTH",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        line_chart.draw(line_data, new Object(Web1Graphs.graphOptions.index_graph_options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        callsLastYear: {
            graph_name: 'callsLastYear',
            friendly: "Last Year",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_LAST_YEAR",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.arrayToDataTable(msg.data);

                        var chart = new google.visualization.ColumnChart(document.getElementById(element_id));
                        chart.draw(data, Web1Graphs.graphOptions.bar_graph_options);
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        callsThisYear: {
            graph_name: 'callsThisYear',
            friendly: "This Year",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_THIS_YEAR",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.arrayToDataTable(msg.data);

                        var chart = new google.visualization.ColumnChart(document.getElementById(element_id));
                        chart.draw(data, Web1Graphs.graphOptions.bar_graph_options);
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        //        callsLastYear:{
        //            graph_name: 'callsLastYear',
        //            friendly:   "Last Year",
        //            dashboard:  true,
        //            draw_graph: function(element_id,company_id,outgoing){
        //                if(!outgoing)
        //                    var outgoing = false;
        //                console.log("[Web1Graphs] draw called.");
        //                var response = Web1Graphs.getGraphData({
        //                    func: "CALLS_LAST_YEAR",
        //                    data: {company_id: company_id, outgoing:outgoing}
        //                });
        //                response.done(function (msg) {
        //                    if(msg.result=="success")
        //                    {
        //                        //console.log(msg.data);
        //                        var data = new google.visualization.arrayToDataTable(msg.data);
        //
        //                        var chart = new google.visualization.ColumnChart(document.getElementById(element_id));
        //                        chart.draw(data, Web1Graphs.graphOptions.bar_graph_options);
        //                        $("#charts").unblock();
        //                    }else{
        //                        console.error("[Web1Graphs] Failed to pull data.");
        //                        $("#charts").unblock();
        //                    }
        //                });
        //            }
        //        },
        callsAllTime: {
            graph_name: 'callsAllTime',
            friendly: "All Time",
            dashboard: true,
            draw_graph: function(element_id, company_id, outgoing) {
                if (!outgoing)
                    var outgoing = false;
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_ALL_TIME",
                    data: {
                        company_id: company_id,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.arrayToDataTable(msg.data);

                        var chart = new google.visualization.ColumnChart(document.getElementById(element_id));
                        var options = Web1Graphs.graphOptions.bar_graph_options;
                        options.hAxis = {
                            textStyle: {
                                fontSize: 10
                            }
                        };
                        chart.draw(data, options);
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        answeredToUnanswered: { //Pie
            graph_name: 'answeredToUnanswered',
            friendly: 'Answered vs Unanswered',
            draw_graph: function(element_id, company_id, start_date, end_date, phone_code, call_result, outgoing_number, outgoing, user, campaign, agent) {
                console.log("[Web1Graphs] draw called.");
                if (!phone_code)
                    var phone_code = 0;
                if (!call_result)
                    var call_result = 'all';
                if (!outgoing_number)
                    var outgoing_number = '';
                if (!outgoing)
                    var outgoing = false;
                if (!user)
                    var user = "";
                if (!campaign)
                    var campaign = "";
                if (!agent)
                    var agent = "";
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_ANSWERED_PIE",
                    data: {
                        company_id: company_id,
                        start_date: start_date,
                        end_date: end_date,
                        phone_code: phone_code,
                        call_result: call_result,
                        outgoing_number: outgoing_number,
                        outgoing: outgoing,
                        user: user,
                        campaign: campaign,
                        agent: agent
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);

                        var chart = new google.visualization.PieChart(document.getElementById(element_id));
                        chart.draw(data, {
                            is3D: true,
                            vAxis: {
                                format: '#',
                                viewWindow: {
                                    min: 0
                                }
                            },
                            legend: {
                                position: 'top',
                                textStyle: {
                                    fontSize: 15
                                }
                            },
                            chartArea: {
                                width: '93%',
                                height: '82%'
                            },
                            lineWidth: 4,
                            pointSize: 9
                        });
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        campaignPhoneCodes: { //Pie
            graph_name: 'campaignPhoneCodes',
            friendly: 'Campaign Phone Pie',
            draw_graph: function(element_id, company_id, start_date, end_date, phone_code, call_result, outgoing_number, campaign, outgoing) {
                console.log("[Web1Graphs] draw called.");
                if (!phone_code)
                    var phone_code = 0;
                if (!call_result)
                    var call_result = 'all';
                if (!outgoing_number)
                    var outgoing_number = '';
                if (!outgoing)
                    var outgoing = false;
                var response = Web1Graphs.getGraphData({
                    func: "CAMPAIGN_PHONE_CODES_PIE",
                    data: {
                        company_id: company_id,
                        start_date: start_date,
                        end_date: end_date,
                        phone_code: phone_code,
                        call_result: call_result,
                        outgoing_number: outgoing_number,
                        campaign: campaign,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);

                        var chart = new google.visualization.PieChart(document.getElementById(element_id));
                        chart.draw(data, {
                            is3D: true,
                            vAxis: {
                                format: '#',
                                viewWindow: {
                                    min: 0
                                }
                            },
                            legend: {
                                position: 'top',
                                textStyle: {
                                    fontSize: 15
                                }
                            },
                            chartArea: {
                                width: '93%',
                                height: '82%'
                            },
                            lineWidth: 4,
                            pointSize: 9
                        });
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        callByTheHour: { //Column
            graph_name: 'callByTheHour',
            friendly: 'Call By Hour',
            draw_graph: function(element_id, company_id, start_date, end_date, phone_code, call_result, day, outgoing_number, outgoing, user, campaign, agent) {
                console.log("[Web1Graphs] draw called.");
                if (!phone_code)
                    var phone_code = 0;
                if (!call_result)
                    var call_result = 'all';
                if (!day)
                    var day = 'all';
                if (!outgoing_number)
                    var outgoing_number = '';
                if (!outgoing)
                    var outgoing = false;
                if (!user)
                    var user = "";
                if (!campaign)
                    var campaign = "";
                if (!agent)
                    var agent = "";
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_BY_HOUR",
                    data: {
                        company_id: company_id,
                        start_date: start_date,
                        end_date: end_date,
                        phone_code: phone_code,
                        call_result: call_result,
                        day: day,
                        outgoing_number: outgoing_number,
                        outgoing: outgoing,
                        user: user,
                        campaign: campaign,
                        agent: agent
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);

                        var chart = new google.visualization.ColumnChart(document.getElementById(element_id));
                        chart.draw(data, Web1Graphs.graphOptions.calls_by_hour);
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        campaignDailyTrends: { //Column
            graph_name: 'campaignDailyTrends',
            friendly: 'Campaign Daily Trends',
            draw_graph: function(element_id, company_id, start_date, end_date, phone_code, call_result, outgoing_number, outgoing) {
                console.log("[Web1Graphs] draw called.");
                if (!phone_code)
                    var phone_code = 0;
                if (!call_result)
                    var call_result = 'all';
                if (!outgoing_number)
                    var outgoing_number = '';
                if (!outgoing)
                    outgoing = false;

                var response = Web1Graphs.getGraphData({
                    func: "CAMPAIGN_DAILY_TRENDS",
                    data: {
                        company_id: company_id,
                        start_date: start_date,
                        end_date: end_date,
                        phone_code: phone_code,
                        call_result: call_result,
                        outgoing_number: outgoing_number,
                        outgoing: outgoing
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);

                        var chart = new google.visualization.ColumnChart(document.getElementById(element_id));
                        chart.draw(data, Web1Graphs.graphOptions.calls_by_hour);
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        callByTheWeek: { //Column
            graph_name: 'callByTheWeek',
            friendly: 'Call By Week',
            draw_graph: function(element_id, company_id, start_date, end_date, phone_code, call_result, outgoing_number, outgoing, campaign) {
                console.log("[Web1Graphs] draw called.");
                if (!phone_code)
                    var phone_code = 0;
                if (!call_result)
                    var call_result = 'all';
                if (!outgoing_number)
                    var outgoing_number = '';
                if (!outgoing)
                    var outgoing = false;
                if (!campaign)
                    var campaign = '';
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_BY_WEEK",
                    data: {
                        company_id: company_id,
                        start_date: start_date,
                        end_date: end_date,
                        phone_code: phone_code,
                        call_result: call_result,
                        outgoing_number: outgoing_number,
                        outgoing: outgoing,
                        campaign: campaign
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);

                        var chart = new google.visualization.ColumnChart(document.getElementById(element_id));
                        chart.draw(data, Web1Graphs.graphOptions.calls_by_hour);
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        customDateI: { // Index page version of customDate
            graph_name: 'customDateI',
            friendly: "Date Range",
            draw_graph: function(element_id, company_id, start_date, end_date) {
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_DATE_RANGE",
                    data: {
                        company_id: company_id,
                        start_date: start_date,
                        end_date: end_date
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);

                        var chart = new google.visualization.LineChart(document.getElementById(element_id));
                        chart.draw(data, Web1Graphs.graphOptions.bar_graph_options);
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        customDate: {
            graph_name: 'customDate',
            friendly: "Date Range",
            draw_graph: function(element_id, report_type, company_id, start_date, end_date, phone_code, call_result, outgoing_number, outgoing, user, campaign, agent) {
                console.log("[Web1Graphs] draw called.");
                if (!phone_code)
                    var phone_code = 0;
                if (!call_result)
                    var call_result = 'all';
                if (!report_type)
                    var report_type = 'default';
                if (!outgoing_number)
                    var outgoing_number = '';
                if (!outgoing)
                    var outgoing = false;
                if (!user)
                    var user = "";
                if (!campaign)
                    var campaign = "";
                if (!agent)
                    var agent = "";
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_DATE_RANGE_ALL_NUMBERS",
                    data: {
                        company_id: company_id,
                        start_date: start_date,
                        end_date: end_date,
                        phone_code: phone_code,
                        call_result: call_result,
                        report_type: report_type,
                        outgoing_number: outgoing_number,
                        outgoing: outgoing,
                        user: user,
                        campaign: campaign,
                        agent: agent
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);
                        var chart;
                        var options;
                        switch (report_type) {
                            case "default":
                                chart = new google.visualization.LineChart(document.getElementById(element_id));
                                options = {
                                    curveType: "function",
                                    vAxis: {
                                        format: '#',
                                        viewWindowMode: 'explicit',
                                        viewWindow: {
                                            min: 0
                                        },
                                        textStyle: {
                                            fontSize: 9
                                        },
                                        maxValue: 4
                                    },
                                    legend: {
                                        position: 'top',
                                        textStyle: {
                                            fontSize: 15
                                        }
                                    },
                                    chartArea: {
                                        width: '95%',
                                        height: '82%'
                                    },
                                    hAxis: {
                                        textStyle: {
                                            fontSize: 10
                                        }
                                    },
                                    lineWidth: 2,
                                    pointSize: 3
                                };
                                break;
                            case "ans_unans":
                                chart = new google.visualization.ColumnChart(document.getElementById(element_id));
                                options = {
                                    isStacked: true,
                                    curveType: "function",
                                    vAxis: {
                                        format: '#',
                                        viewWindowMode: 'explicit',
                                        viewWindow: {
                                            min: 0
                                        },
                                        textStyle: {
                                            fontSize: 9
                                        }
                                    },
                                    legend: {
                                        position: 'top',
                                        textStyle: {
                                            fontSize: 15
                                        }
                                    },
                                    hAxis: {
                                        textStyle: {
                                            fontSize: 10
                                        }
                                    },
                                    chartArea: {
                                        width: '95%',
                                        height: '82%'
                                    },
                                    lineWidth: 2,
                                    pointSize: 3
                                };
                                break;
                        }

                        chart.draw(data, options);
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        },
        generatedCalls: { //Pie
            graph_name: 'generatedCalls',
            friendly: 'Generated Calls',
            draw_graph: function(element_id, start_date, end_date, company_id) {
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_GEN_CALLS_PIE",
                    data: {
                        start_date: start_date,
                        end_date: end_date,
                        company_id: company_id
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);
                        var chart = new google.visualization.PieChart(document.getElementById(element_id));
                        chart.draw(data, {
                            is3D: true,
                            vAxis: {
                                format: '#',
                                viewWindow: {
                                    min: 0
                                }
                            },
                            legend: {
                                position: 'right',
                                textStyle: {
                                    fontSize: 15
                                }
                            },
                            chartArea: {
                                width: '93%',
                                height: '82%'
                            },
                            lineWidth: 4,
                            pointSize: 9,
                            title: 'Keywords Generated Calls'
                        });
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                    }
                });
            }
        },
        allMatureHits: { //Pie
            graph_name: 'allMatureHits',
            friendly: 'Matured Hits',
            draw_graph: function(element_id, start_date, end_date, company_id) {
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_All_HITS_PIE",
                    data: {
                        start_date: start_date,
                        end_date: end_date,
                        company_id: company_id
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);

                        var chart = new google.visualization.PieChart(document.getElementById(element_id));
                        chart.draw(data, {
                            is3D: true,
                            vAxis: {
                                format: '#',
                                viewWindow: {
                                    min: 0
                                }
                            },
                            legend: {
                                position: 'right',
                                textStyle: {
                                    fontSize: 15
                                }
                            },
                            chartArea: {
                                width: '93%',
                                height: '82%'
                            },
                            lineWidth: 4,
                            pointSize: 9,
                            title: 'All Keywords'
                        });
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                    }
                });
            }
        },
        totalHitsGraph: { //Pie
            graph_name: 'totalHitsGraph',
            friendly: 'Total Hits Graph',
            draw_graph: function(element_id, start_date, end_date, company_id) {
                console.log("[Web1Graphs] draw called.");
                var response = Web1Graphs.getGraphData({
                    func: "CALLS_NONGEN_CALL",
                    data: {
                        start_date: start_date,
                        end_date: end_date,
                        company_id: company_id
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var data = new google.visualization.DataTable(msg.data);
                        var chart = new google.visualization.PieChart(document.getElementById(element_id));
                        chart.draw(data, {
                            is3D: true,
                            vAxis: {
                                format: '#',
                                viewWindow: {
                                    min: 0
                                }
                            },
                            legend: {
                                position: 'right',
                                textStyle: {
                                    fontSize: 15
                                }
                            },
                            chartArea: {
                                width: '93%',
                                height: '82%'
                            },
                            lineWidth: 4,
                            pointSize: 9,
                            title: 'All Keywords'
                        });
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                    }
                });
            }
        },
        emailTrackingClients: {
            graph_name: 'emailTrackingClients',
            friendly: "Email Tracking Clients",
            dashboard: true,
            draw_graph: function(element_id, company_id, start_date, end_date) {
                var response = Web1Graphs.getGraphData({
                    func: "EMAIL_TRACKING_CLIENTS",
                    data: {
                        company_id: company_id,
                        start_date: start_date,
                        end_date: end_date
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        //console.log(msg.data);
                        var line_data = new google.visualization.arrayToDataTable(msg.data);
                        var line_chart = new google.visualization.LineChart(document.getElementById(element_id));
                        line_chart.draw(line_data, new Object(Web1Graphs.graphOptions.index_graph_options));
                        $("#charts").unblock();
                    } else {
                        console.error("[Web1Graphs] Failed to pull data.");
                        $("#charts").unblock();
                    }
                });
            }
        }
    },

    getGraphData: function(data) {
        return $.ajax({
            url: "graph_handler.php",
            type: "POST",
            data: data,
            dataType: "json",
            cache: false
        });
    }
};