$(document).ready(function() {

    var addClientButton = $("#add-client-button");
    var invoiceTemplateButton = $("#invoice-settings-button");

    //clicking the Invoice Setting button.
    $("#invoice-settings-button").click(function() {

        var dialog = $("#invoice-settings-dialog").dialog({
            modal: true,
            resizable: false,
            draggable: false,
            autoOpen: true,
            width: 630,
            height: 'auto',
            minHeight: 0,
            dialogClass: "invoice-settings-dialog"
        });
    });

    //init color wheels in the Invoice Settings dialog.
    $("#invoice-settings-dialog .color-wheel").each(function() {

        $.farbtastic(this, $(this).parents(".color-option:eq(0)").find(".color-preview"));
    });

    //clicking the color preview div.
    $("#invoice-settings-dialog .color-preview").click(function(event) {

        //the color preview div.
        var colorPreviewDiv = $(this);

        //the color wheel wrapper.
        var colorWheelWrapperDiv = colorPreviewDiv.siblings(".color-wheel-wrapper");

        //the color wheel.
        var colorWheelDiv = colorWheelWrapperDiv.find(".color-wheel");

        //get the color wheel object.
        var colorWheel = $.farbtastic(colorWheelDiv);

        //toggle is not working for some reason.

        if (colorWheelWrapperDiv.css("display") == "block") {

            colorWheelWrapperDiv.hide();
        } else if (colorWheelWrapperDiv.css("display") == "none") {

            colorWheelWrapperDiv.show();

            colorWheel.setColor(hexc(colorPreviewDiv.css("background-color")));
        }
    });

    //clicking the Cancel button on the Invoice Settings dialog.
    $("#invoice-settings-dialog #cancel-invoice-settings-button").click(function() {

        $("#invoice-settings-dialog").dialog("close");
    });

    var clientsTableHtml = "";

    //loop through clients array.
    for (var i = 0; i < clientsArray.length; i++) {

        var clientId = clientsArray[i]["id"];
        var clientName = clientsArray[i]["client_name"] || "";
        var frequency = clientsArray[i]["frequency"] || "";
        var cronId = clientsArray[i]["cron_id"];

        var billForName = "";
        var bill_for = clientsArray[i]["bill_for"];

        switch (bill_for) {
            case "call-tracking-service":
                billForName = "Call Tracking";
                break;
            case "lead-gen":
                billForName = "Lead Gen";
                break;
        }

        switch (frequency) {

            case "weekly":
                frequencyName = "Weekly";
                break;
            case "monthly":
                frequencyName = "Monthly";
                break;
            case "bimonthly":
                frequencyName = "Bi Monthly";
                break;
        }

        if (typeof clientsArray[i]["last_invoice"] === "undefined") {
            var lastInvoice = "None";
            var status = "None";
        } else {
            var lastInvoice = new Date(clientsArray[i]["last_invoice"]["last_invoice_date"]);
            lastInvoice = lastInvoice.getTime() / 1000 | 0;
            lastInvoice = new Date((lastInvoice + timezone) * 1000).format("mmm d, yyyy h:MMtt");
            if (clientsArray[i]["last_invoice"]["last_invoice_status"] == 1) {
                var status = "<span class='invoice-status paid'>Paid</span>";
            } else if (clientsArray[i]["last_invoice"]["last_invoice_status"] == 2) {
                var status = "<span class='invoice-status pending' style='cursor: pointer;' onclick=\"window.location= 'call_report_review.php?sd=" + clientsArray[i]["last_invoice"]["last_invoice_from"] + "&ed=" + clientsArray[i]["last_invoice"]["last_invoice_to"] + "&client_id=" + clientId + "';\">Pending REVIEW</span>"
            } else if (clientsArray[i]["last_invoice"]["last_invoice_status"] == 3) {
                var status = "<span class='invoice-status declined'>Declined</span>"
            } else if (clientsArray[i]["last_invoice"]["last_invoice_status"] == 4) {
                var status = "<span class='invoice-status paid'>Paid Offline</span>";
            } else {
                var status = "<span class='invoice-status unpaid'>Not Paid</span>";
            }
        }

        clientsTableHtml += "<tr clientid='" + clientId + "' cronid='" + cronId + "'>";
        clientsTableHtml += "<td><a href='#' class='client-name'>" + clientName + "</a></td>";
        clientsTableHtml += "<td>" + billForName + "</td>";
        clientsTableHtml += "<td>" + frequencyName + "</td>";
        clientsTableHtml += "<td>" + lastInvoice + "</td>";
        clientsTableHtml += "<td>" + status + "</td>";
        clientsTableHtml += "<td><a href='billing_log.php?client_id=" + clientId + "' class='billing-log-button' title='Billing Log'><img width='16' src='images/magnifying_glass.png' /></a> <a href='#' class='edit-client-button' title='Edit Client'><img width='16' src='images/cog_edit.png'/></a> <a href='#' class='delete-client-button' title='Delete Client'><img src='images/delete.gif'></a></td>";
        clientsTableHtml += "</tr>";
    }

    $("#clients-table tbody").html(clientsTableHtml);

    $(".billing-log-button, .edit-client-button, .delete-client-button").tooltipster({
        position: 'bottom',
        theme: '.tooltipster-shadow'
    });

    if (paypalIntegrationInfo[0] === false) {
        addClientButton.attr("style", "color:#cecece;");
        addClientButton.click(function() {
            alert("Please setup paypal before adding a client.");
        });
    } else {
        addClientButton.click(addclientClick);
    }

    function addclientClick() {
        $("#add-client-dialog .block h2").text("Add Client");

        var addoredit = $("#add-client-dialog .block h2").text();

        var usersHTML = "";
        $(users).each(function(i, v) {
            var this_client_exists = false;

            for (var i = 0; i < clientsArray.length; i++) {
                if (addoredit != "Edit Client") {
                    if (v.email === clientsArray[i].user_to_send_bill_to)
                        this_client_exists = true;
                }
            }

            if (!this_client_exists)
                usersHTML = usersHTML + "<option value='" + v.email + "'>" + v.full_name + " (" + v.email + ")</option>";
        });
        $("#user-to-send-bill-to").html(usersHTML);

        //reset form.
        $("#add-client-dialog :text").val("");
        $("select option:eq(0)").prop("selected", true).change();
        $("#add-client-dialog :radio, #add-client-dialog :checkbox").prop("checked", false);

        $("[showforleadgen],[showforcalltracking]").hide();

        $("#frequency").find("option[value='weekly'],option[value='bimonthly']").prop("disabled", false);

        $("#add-client-dialog #save-add-client-button").attr("mode", "add");
        $("#add-client-dialog #save-add-client-button").attr("editclientid", "");
        $("#add-client-dialog #save-add-client-button").attr("editcronid", "");

        var todaysDate = new Date();

        var todaysDateString = ("00" + (todaysDate.getMonth() + 1)).slice(-2) + "/" + ("00" + todaysDate.getDate()).slice(-2) + "/" + todaysDate.getFullYear();

        $("#add-client-start-date").val(todaysDateString);
        $("#add-client-start-date").prop('disabled', false);

        var dialog = $("#add-client-dialog").dialog({
            modal: true,
            resizable: false,
            draggable: false,
            autoOpen: true,
            width: 630,
            height: 'auto',
            minHeight: 0,
            dialogClass: "add-client-dialog"
        });
    }

    $("#add-client-start-date").datepicker();

    var addoredit = $("#add-client-dialog .block h2").text();

    var usersHTML = "";
    $(users).each(function(i, v) {
        var this_client_exists = false;

        for (var i = 0; i < clientsArray.length; i++) {
            if (addoredit != "Edit Client") {
                if (v.email === clientsArray[i].user_to_send_bill_to)
                    this_client_exists = true;
            }
        }

        if (!this_client_exists)
            usersHTML = usersHTML + "<option value='" + v.email + "'>" + v.full_name + " (" + v.email + ")</option>";
    });

    $("#user-to-send-bill-to").html(usersHTML);

    var addClientCompaniesSelectHtml = "<option value='0'>-- Select Company --</option>";

    //loop through all companies.
    for (var i = 0; i < companiesArray.length; i++) {

        var companyId = companiesArray[i]["idx"];
        var companyName = companiesArray[i]["company_name"];

        addClientCompaniesSelectHtml += "<option value='" + companyId + "'>" + stripslashes(companyName) + "</option>";
    }

    //fill the companies drop down.
    $("#add-client-companies-select").html(addClientCompaniesSelectHtml).change(function() {

        var companiesSelect = $(this);

        var selectedCompanyId = companiesSelect.val();

        //populate company outgoing numbers.

        var outgoingNumbersCheckboxesHtml = "";

        //loop through outgoing numbers.
        for (var i = 0; i < outgoingNumbersArray.length; i++) {

            var outgoingNumberArray = outgoingNumbersArray[i];

            if (selectedCompanyId == outgoingNumberArray["company_id"]) {

                if (outgoingNumberArray["international"] == "0")
                    if (outgoingNumberArray["number"].substr(0, 1) != "1")
                        outgoingNumberArray["number"] = "1" + outgoingNumberArray["number"];

                outgoingNumbersCheckboxesHtml += "<div><input type='checkbox' class='checkbox outgoing-number-checkbox' id='outgoing-number-" + outgoingNumberArray["number"] + "' value='" + outgoingNumberArray["number"] + "' /> <label for='outgoing-number-" + outgoingNumberArray["number"] + "'>" + formatLocal(countryForE164Number("+" + outgoingNumberArray["number"]), "+" + outgoingNumberArray["number"]) + "</label></div>";
            }
        }

        $("#outgoing-numbers-div").html(outgoingNumbersCheckboxesHtml);

        //populate company phone codes.

        var billByPhoneCodesCheckboxesButtonsHtml = "<div id='lead-gen-bill-by-phone-code-checkboxes-div'><a href='#' id='lead-gen-bill-by-phone-code-select-all-button'>Select All</a> &#124; <a href='#' id='lead-gen-bill-by-phone-code-select-none-button'>Select None</a></div>";
        var billByPhoneCodesLeftCheckboxesHtml = "<div class='left-col'>";
        var billByPhoneCodesRightCheckboxesHtml = "<div class='right-col'>";

        var counter = 0;

        //loop through phone codes.
        for (var i = 0; i < phoneCodesArray.length; i++) {

            var phoneCodeArray = phoneCodesArray[i];

            if (selectedCompanyId == phoneCodeArray["company_id"]) {

                var billByCheckboxHtml = "<div><input type='checkbox' class='checkbox lead-gen-bill-by-phone-code-checkbox' id='lead-gen-bill-by-phone-code-" + phoneCodeArray["idx"] + "' value='" + phoneCodeArray["idx"] + "' checked='checked' /> <label for='lead-gen-bill-by-phone-code-" + phoneCodeArray["idx"] + "'>" + phoneCodeArray["name"] + "</label></div>";

                if ((counter % 2) == 0) {

                    billByPhoneCodesLeftCheckboxesHtml += billByCheckboxHtml;
                } else {

                    billByPhoneCodesRightCheckboxesHtml += billByCheckboxHtml;
                }

                counter++;
            }
        }

        var phoneCallsWithNoPhoneCodeCheckboxHtml = "<div><input type='checkbox' class='checkbox lead-gen-bill-by-phone-code-checkbox' id='lead-gen-bill-by-phone-code-unassigned' value='unassigned' checked='checked' /> <label for='lead-gen-bill-by-phone-code-unassigned'>Calls with No Phone Code Assigned</label></div>";

        if ((counter % 2) == 0) {

            billByPhoneCodesLeftCheckboxesHtml += phoneCallsWithNoPhoneCodeCheckboxHtml;
        } else {

            billByPhoneCodesRightCheckboxesHtml += phoneCallsWithNoPhoneCodeCheckboxHtml;
        }

        billByPhoneCodesLeftCheckboxesHtml += "</div>";
        billByPhoneCodesRightCheckboxesHtml += "</div>";

        $("#bill-by-phone-code-div").html(billByPhoneCodesCheckboxesButtonsHtml + billByPhoneCodesLeftCheckboxesHtml + billByPhoneCodesRightCheckboxesHtml);

    $('.lead-gen-bill-by-phone-code-checkbox').click(function() {
        if ($('#bill-by-phone-code-div input[type="checkbox"]:checked').length > 0) {
            //$("[showForPhoneCodes]").show();
            $("#lead_gen_delay_period").val(7);
        }
        else {
            $("[showForPhoneCodes]").hide();
            $("#lead_gen_delay_period").val(1);
            $("#lead_gen_delay_email").val('');
        }
    });

        $("#bill-by-phone-code-div #lead-gen-bill-by-phone-code-select-all-button").click(function() {

            $("#bill-by-phone-code-div :checkbox").prop("checked", true);
            //$("[showForPhoneCodes]").show();
            $("#lead_gen_delay_period").val(7);
        });

        $("#bill-by-phone-code-div #lead-gen-bill-by-phone-code-select-none-button").click(function() {

            $("#bill-by-phone-code-div :checkbox").prop("checked", false);
            $("[showForPhoneCodes]").hide();
            $("#lead_gen_delay_period").val(1);
            $("#lead_gen_delay_email").val('');
        });
    });

    //clicking Lead Gen or Call Tracking Service.
    $("[name='bill-for-radiogroup']").click(function() {

        var radio = $(this);

        if (radio.attr("id") == "lead-gen") {

            $("[showForLeadGen]").show();
            $("[showForPhoneCodes]").hide();
            $("[showForCallTracking]").hide();

            if ($('#bill-by-phone-code-div input[type="checkbox"]:checked').length > 0) {
                //$("[showForPhoneCodes]").show();
            $("#lead_gen_delay_period").val(7);
            }
            else {
                $("[showForPhoneCodes]").hide();
                $("#lead_gen_delay_period").val(1);
                $("#lead_gen_delay_email").val('');
            }

            $("#frequency").find("option[value='weekly'],option[value='bimonthly']").prop("disabled", false);
        } else if (radio.attr("id") == "call-tracking-service") {

            $("[showForLeadGen]").hide();
            $("[showForPhoneCodes]").hide();
            $("[showForCallTracking]").show();

            $("#frequency").val("monthly").find("option[value='weekly'],option[value='bimonthly']").prop("disabled", true);
        }
    });

    //clicking the Cancel button on the Add Client dialog.
    $("#add-client-dialog #cancel-add-client-button").click(function() {

        $("#add-client-dialog").dialog("close");
    });

    //clicking the Save button on the Invoice Settings dialog.
    $("#add-client-dialog #save-add-client-button").click(function() {

        var addoredit = $("#add-client-dialog .block h2").text();

        var saveButton = $(this);

        var companyId = $("#add-client-companies-select").val();

        var outgoingNumbersArray = [];

        $("#outgoing-numbers-div .outgoing-number-checkbox:checked").each(function() {

            outgoingNumbersArray.push($(this).val());
        });

        var billFor = $("[name='bill-for-radiogroup']:checked").val() || "";

        var leadGenBillFor = $("[name='lead-gen-bill-for-radiogroup']:checked").val() || "";

        var billByPhoneCodeArray = [];

        $("#bill-by-phone-code-div .lead-gen-bill-by-phone-code-checkbox:checked").each(function() {

            billByPhoneCodeArray.push($(this).val());
        });

        var lead_gen_delay_period = $("#lead_gen_delay_period").val();
        var lead_gen_delay_email = $("#lead_gen_delay_email").val();
        var freeLeadsNumber = $("#free-leads-number").val();

        var amountPerQualifiedLead = $("#amount-per-qualified-lead").val();
        var amountPerQualifiedLeadCurrency = $("#amount-per-qualified-lead-currency-select").val();

        var flatRatePerMonth = $("#flat-rate-per-month").val();
        var perNumberFee = $("#per-number-fee").val();
        var perMinuteFee = $("#per-minute-fee").val();
        var callTrackingCurrency = $("#flat-rate-per-month-currency-select").val();

        var userToSendBillTo = $("#user-to-send-bill-to").val();

        var frequency = $("#frequency").val();

        var billNotPaidPastDueDays = $("#bill-not-paid-past-due-days").val();

        var startDate = $("#add-client-start-date").val();

        var invoiceEmailTemplate = $("#invoice-email-template").val();

        var clientName = $("#client-name").val();
        var address = $("#address").val();
        var city = $("#city").val();
        var state = $("#state").val();
        var zipCode = $("#zip-code").val();

        if (clientName == "" || address == "" || city == "" || state == "" ||
            zipCode == "" || billFor == "" || companyId == "" || companyName == "") {
            alert('Please enter all the information.');
            return false;
        }

        for (var i = 0; i < clientsArray.length; i++) {
            if (addoredit != "Edit Client") {
                if (clientName === clientsArray[i].client_name) {
                    alert('Client name already exists! Please change the Client Name.');
                    return false;
                } 
                if (userToSendBillTo === clientsArray[i].user_to_send_bill_to) {
                    alert('Client already exists! Please choose another User.');
                    return false;
                }
            }
        }

        var data = {
            ajaxRequest: "saveAddClient",
            companyId: companyId,
            outgoingNumbers: outgoingNumbersArray.toString(),
            billFor: billFor,
            leadGenBillFor: leadGenBillFor,
            billByPhoneCode: billByPhoneCodeArray.toString(),
            freeLeadsNumber: freeLeadsNumber,
            lead_gen_delay_period: lead_gen_delay_period,
            lead_gen_delay_email: lead_gen_delay_email,
            amountPerQualifiedLead: amountPerQualifiedLead,
            amountPerQualifiedLeadCurrency: amountPerQualifiedLeadCurrency,
            flatRatePerMonth: flatRatePerMonth,
            perNumberFee: perNumberFee,
            perMinuteFee: perMinuteFee,
            callTrackingCurrency: callTrackingCurrency,
            userToSendBillTo: userToSendBillTo,
            frequency: frequency,
            billNotPaidPastDueDays: billNotPaidPastDueDays,
            startDate: startDate,
            invoiceEmailTemplate: invoiceEmailTemplate,
            address: address,
            clientName: clientName,
            city: city,
            state: state,
            zipCode: zipCode
        };

        if (saveButton.attr("mode") == "edit") {

            data.ajaxRequest = "saveEditClient";
            data.clientId = saveButton.attr("editclientid");
            data.cronId = saveButton.attr("editcronid");
        }

        //send request to save the client.
        $.ajax({
            url: "manage_billing.php",
            type: "POST",
            dataType: "json",
            data: data,
            success: function() {

                window.location = "manage_billing.php";
            }
        });

        $("#add-client-dialog").dialog("close");
        $("#add-client-dialog").dialog('destroy').remove();
    });

    $("#invoice-template-save").click(function() {
        var template_ = [];
        $(".color-preview").each(function(i, v) {
            var color = $(v).css('background-color');
            template_.push(color);
        });
        var data = {
            ajaxRequest: 'saveTemplate',
            template: template_
        };
        $.ajax({
            url: "manage_billing.php",
            type: "POST",
            dataType: "json",
            data: data,
            success: function() {
                $("#cancel-invoice-settings-button").trigger("click");
                alert('Saved template!');
            }
        });
    });

    //clicking the client name button.
    $(".client-name").click(function() {

        var clientNameButton = $(this);

        var clientId = clientNameButton.parents("tr:eq(0)").attr("clientid");

        var clientArray = getClientArrayFromClientsArray(clientId);

        var companyId = clientArray["company_id"];
        var companyName = "";

        if (companyId) {

            var companyArray = getCompanyArrayFromCompaniesArray(clientArray["company_id"]);

            companyName = companyArray["company_name"];
        }

        var outgoingNumbers = clientArray["outgoing_numbers"];
        var outgoingNumbersString = "";

        if (outgoingNumbers) {

            for (var i = 0; i < outgoingNumbers.length; i++) {

                outgoingNumbersString += "<div>" + formatLocal(countryForE164Number("+" + outgoingNumbers[i]), "+" + outgoingNumbers[i]) + "</div>";
            }
        }

        var billFor = clientArray["bill_for"];
        var billForString = "";

        if (billFor == "lead-gen") {

            billForString = "Lead Gen";
        } else if (billFor == "call-tracking-service") {

            billForString = "Call Tracking";
        }

        var leadGenBillFor = clientArray["lead_gen_bill_for"];
        var leadGenBillForString = "";

        if (leadGenBillFor) {

            switch (leadGenBillFor) {

                case "all":
                    leadGenBillForString = "All";
                    break;
                case "30":
                    leadGenBillForString = "Calls Over 30 seconds";
                    break;
                case "60":
                    leadGenBillForString = "Calls Over 60 seconds";
                    break;
                case "90":
                    leadGenBillForString = "Calls Over 90 seconds";
                    break;
                case "120":
                    leadGenBillForString = "Calls Over 120 seconds";
                    break;
                case "missed":
                    leadGenBillForString = "Missed Calls";
                    break;
                case "answered":
                    leadGenBillForString = "Answered Calls";
                    break;
            }
        }

        var leadGenBillByPhoneCodes = clientArray["lead_gen_bill_by_phone_codes"];
        var leadGenBillByPhoneCodesString = "";

        if (leadGenBillByPhoneCodes) {

            for (var i = 0; i < leadGenBillByPhoneCodes.length; i++) {

                if (leadGenBillByPhoneCodes[i] == "unassigned") {

                    leadGenBillByPhoneCodesString += "<div>Calls with No Phone Code Assigned</div>";
                } else {
                    var phoneCodeArray = getPhoneCodeArrayFromPhoneCodesArray(leadGenBillByPhoneCodes[i]);

                    leadGenBillByPhoneCodesString += "<div>" + phoneCodeArray["name"] + "</div>";
                }
            }
        }

        var leadGenFreeLeadsNumber = clientArray["lead_gen_free_leads_number"];

        if (leadGenFreeLeadsNumber == "0") {

            leadGenFreeLeadsNumber = "";
        }

        var lead_gen_delay_period = clientArray["lead_gen_delay_period"];
        var lead_gen_delay_email = clientArray["lead_gen_delay_email"];

        var leadGenAmountPerQualifiedLead = clientArray["lead_gen_amount_per_qualified_lead"];
        var leadGenAmountPerQualifiedLeadCurrency = clientArray["lead_gen_amount_per_qualified_lead_currency"];

        if (leadGenAmountPerQualifiedLead == "0") {

            leadGenAmountPerQualifiedLead = "";
        } else {

            leadGenAmountPerQualifiedLead = leadGenAmountPerQualifiedLead + " " + leadGenAmountPerQualifiedLeadCurrency;
        }


        var callTrackingCurrency = clientArray["call_tracking_currency"];
        var callTrackingFlatRatePerMonth = clientArray["call_tracking_flat_rate_per_month"];

        if (callTrackingFlatRatePerMonth == "0") {

            callTrackingFlatRatePerMonth = "";
        } else {

            callTrackingFlatRatePerMonth = callTrackingFlatRatePerMonth + " " + callTrackingCurrency;
        }

        var callTrackingPerNumberFee = clientArray["call_tracking_per_number_fee"];

        if (callTrackingPerNumberFee == "0") {

            callTrackingPerNumberFee = "";
        } else {

            callTrackingPerNumberFee = callTrackingPerNumberFee + " " + callTrackingCurrency;
        }

        var callTrackingPerMinuteFee = clientArray["call_tracking_per_minute_fee"];

        if (callTrackingPerMinuteFee == "0") {

            callTrackingPerMinuteFee = "";
        } else {

            callTrackingPerMinuteFee = callTrackingPerMinuteFee + " " + callTrackingCurrency;
        }

        var userToSendBillTo = clientArray["user_to_send_bill_to"];
        var userToSendBillToString = "";

        if (userToSendBillTo) {

            userToSendBillToString = userToSendBillTo;
        }

        var frequency = clientArray["frequency"];
        var frequencyString = "";

        if (frequency) {

            switch (frequency) {

                case "weekly":
                    frequencyString = "Weekly";
                    break;
                case "monthly":
                    frequencyString = "Monthly";
                    break;
                case "bimonthly":
                    frequencyString = "Bi Monthly";
                    break;
            }
        }

        var billNotPaidPastDueDays = clientArray["bill_not_paid_past_due_days"];

        if (billNotPaidPastDueDays == "0") {

            billNotPaidPastDueDays = "";
        }

        var startDate = clientArray["startdate"];
        var startDateString = "";

        if (startDate) {

            var startDateTimeArray = startDate.split(" ");

            var startDateArray = startDateTimeArray[0].split("-");

            startDateString = startDateArray[1] + "/" + startDateArray[2] + "/" + startDateArray[0];
        }

        var invoiceEmailTemplate = clientArray["invoice_email_template"];
        var invoiceEmailTemplateString = "";

        if (invoiceEmailTemplate) {

            switch (invoiceEmailTemplate) {

                case "template1":
                    invoiceEmailTemplateString = "Template 1";
                    break;
                case "template2":
                    invoiceEmailTemplateString = "Template 2";
                    break;
                case "template3":
                    invoiceEmailTemplateString = "Template 3";
                    break;
            }
        }

        var clientName = clientArray["client_name"];
        var address = clientArray["address"];
        var city = clientArray["city"];
        var state = clientArray["state"];
        var zipCode = clientArray["zip_code"];


        var clientInfoHtml = "";

        clientInfoHtml += "<div class='form-row'><label>Company:</label><div class='field-value'>" + companyName + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>Outgoing Numbers:</label><div class='field-value'>" + outgoingNumbersString + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>Bill For:</label><div class='field-value'>" + billForString + "</div><div class='clear-float'></div></div>";

        if (billFor == "lead-gen") {
            clientInfoHtml += "<div class='form-row'><label>Lead Gen - Bill For:</label><div class='field-value'>" + leadGenBillForString + "</div><div class='clear-float'></div></div>";
            clientInfoHtml += "<div class='form-row'><label>Bill By Phone Codes:</label><div class='field-value'>" + leadGenBillByPhoneCodesString + "</div><div class='clear-float'></div></div>";
            clientInfoHtml += "<div class='form-row'><label>Number Of Free Leads:</label><div class='field-value'>" + leadGenFreeLeadsNumber + "</div><div class='clear-float'></div></div>";
            clientInfoHtml += "<div class='form-row'><label>Days after invoice is created:</label><div class='field-value'>" + lead_gen_delay_period + "</div><div class='clear-float'></div></div>";
            clientInfoHtml += "<div class='form-row'><label>Email for notification:</label><div class='field-value'>" + lead_gen_delay_email + "</div><div class='clear-float'></div></div>";
            clientInfoHtml += "<div class='form-row'><label>Amount Per Qualified Lead:</label><div class='field-value'>" + leadGenAmountPerQualifiedLead + "</div><div class='clear-float'></div></div>";
        } else if (billFor == "call-tracking-service") {
            clientInfoHtml += "<div class='form-row'><label>Flat Rate Per Month:</label><div class='field-value'>" + callTrackingFlatRatePerMonth + "</div><div class='clear-float'></div></div>";
            clientInfoHtml += "<div class='form-row'><label>Per Number Fee:</label><div class='field-value'>" + callTrackingPerNumberFee + "</div><div class='clear-float'></div></div>";
            clientInfoHtml += "<div class='form-row'><label>Per Minute Usage Fee:</label><div class='field-value'>" + callTrackingPerMinuteFee + "</div><div class='clear-float'></div></div>";
        }

        clientInfoHtml += "<div class='form-row'><label>User To Send Bill To:</label><div class='field-value'>" + userToSendBillToString + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>Frequency:</label><div class='field-value'>" + frequencyString + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>Days Till Bill Is Past Due If Not Paid:</label><div class='field-value'>" + billNotPaidPastDueDays + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>Start Date:</label><div class='field-value'>" + startDateString + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row' style='display:none;'><label>Email Template To Use When Sending Invoice/Auto Bill:</label><div class='field-value'>" + invoiceEmailTemplateString + "</div><div class='clear-float'></div></div>";

        clientInfoHtml += "<div class='form-row'><label>Company Info:</label><br /><div class='clear-float'></div></div>";

        clientInfoHtml += "<div class='form-row'><label>Client Name:</label><div class='field-value'>" + clientName + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>Address:</label><div class='field-value'>" + address + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>City:</label><div class='field-value'>" + city + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>State/Region:</label><div class='field-value'>" + state + "</div><div class='clear-float'></div></div>";
        clientInfoHtml += "<div class='form-row'><label>Zip Code/Postal Code:</label><div class='field-value'>" + zipCode + "</div><div class='clear-float'></div></div>";

        $("#show-client-info-div").html(clientInfoHtml);

        var dialog = $("#show-client-info-dialog").dialog({
            modal: true,
            resizable: false,
            draggable: false,
            autoOpen: true,
            width: 630,
            height: 'auto',
            minHeight: 0,
            dialogClass: "show-client-info-dialog"
        });
    });

    //clicking the Close button on the show client info dialog.
    $("#show-client-info-dialog #close-client-info-button").click(function() {

        $("#show-client-info-dialog").dialog("close");
        $("#show-client-info-dialog").dialog("destroy");
    });

    //clicking the edit client button.
    $(".edit-client-button").click(function() {

        var addoredit = $("#add-client-dialog .block h2").text();

        var usersHTML = "";
        $(users).each(function(i, v) {
            usersHTML = usersHTML + "<option value='" + v.email + "'>" + v.full_name + " (" + v.email + ")</option>";
        });

        $("#user-to-send-bill-to").html(usersHTML);

        $("#add-client-dialog .block h2").text("Edit Client");

        var editClientButton = $(this);

        var clientId = editClientButton.parents("tr:eq(0)").attr("clientid");
        var cronId = editClientButton.parents("tr:eq(0)").attr("cronid");

        var clientArray = getClientArrayFromClientsArray(clientId);

        var companyId = clientArray["company_id"];
        var companyIdSelectVal = "0";

        if (companyId) {

            companyIdSelectVal = companyId;
        }

        $("#add-client-companies-select").val(companyIdSelectVal).change();

        var outgoingNumbers = clientArray["outgoing_numbers"];

        if (outgoingNumbers) {

            $("#outgoing-numbers-div .outgoing-number-checkbox").each(function() {

                var outgoingNumberCheckbox = $(this);

                var outgoingNumber = outgoingNumberCheckbox.val();

                var outgoingNumberFound = false;

                for (var i = 0; i < outgoingNumbers.length; i++) {

                    if (outgoingNumber == outgoingNumbers[i]) {

                        outgoingNumberCheckbox.prop("checked", true);
                        outgoingNumberFound = true;
                        break;
                    }
                }

                if (!outgoingNumberFound) {

                    outgoingNumberCheckbox.prop("checked", false);
                }
            });
        } else {

            $("#outgoing-numbers-div .outgoing-number-checkbox").each(function() {

                var outgoingNumberCheckbox = $(this);

                outgoingNumberCheckbox.prop("checked", false);
            });
        }

        var billFor = clientArray["bill_for"];

        if (billFor) {

            $("[name='bill-for-radiogroup']#" + billFor).prop("checked", true).click();
        } else {

            $("[name='bill-for-radiogroup']").prop("checked", false);

            $("[showforleadgen],[showforcalltracking]").hide();
        }

        var leadGenBillFor = clientArray["lead_gen_bill_for"];

        if (leadGenBillFor) {

            $("[name='lead-gen-bill-for-radiogroup'][value='" + leadGenBillFor + "']").prop("checked", true);
        } else {

            $("[name='lead-gen-bill-for-radiogroup']").prop("checked", false);
        }

        var leadGenBillByPhoneCodes = clientArray["lead_gen_bill_by_phone_codes"];

        if (leadGenBillByPhoneCodes) {

            $("#bill-by-phone-code-div .lead-gen-bill-by-phone-code-checkbox").each(function() {

                var billByPhoneCodeCheckbox = $(this);

                var phoneCodeId = billByPhoneCodeCheckbox.val();

                var phoneCodeFound = false;

                for (var i = 0; i < leadGenBillByPhoneCodes.length; i++) {

                    if (phoneCodeId == leadGenBillByPhoneCodes[i]) {

                        billByPhoneCodeCheckbox.prop("checked", true);
                        phoneCodeFound = true;
                        break;
                    }
                }

                if (!phoneCodeFound) {

                    billByPhoneCodeCheckbox.prop("checked", false);
                }
            });
        } else {

            $("#bill-by-phone-code-div .lead-gen-bill-by-phone-code-checkbox").each(function() {

                var billByPhoneCodeCheckbox = $(this);

                billByPhoneCodeCheckbox.prop("checked", false);
            });
        }

        var leadGenFreeLeadsNumber = clientArray["lead_gen_free_leads_number"];

        if (leadGenFreeLeadsNumber == "0") {

            leadGenFreeLeadsNumber = "";
        }

        $("#free-leads-number").val(leadGenFreeLeadsNumber);

        var lead_gen_delay_period = clientArray["lead_gen_delay_period"];

        $("#lead_gen_delay_period").val(lead_gen_delay_period);        

        var lead_gen_delay_email = clientArray["lead_gen_delay_email"];

        $("#lead_gen_delay_email").val(lead_gen_delay_email);        

        var leadGenAmountPerQualifiedLead = clientArray["lead_gen_amount_per_qualified_lead"];

        if (leadGenAmountPerQualifiedLead == "0") {

            leadGenAmountPerQualifiedLead = "";
        }

        $("#amount-per-qualified-lead").val(leadGenAmountPerQualifiedLead);

        var leadGenAmountPerQualifiedLeadCurrency = clientArray["lead_gen_amount_per_qualified_lead_currency"];

        if (leadGenAmountPerQualifiedLeadCurrency) {

            $("#amount-per-qualified-lead-currency-select").val(leadGenAmountPerQualifiedLeadCurrency);
        } else {

            $("#amount-per-qualified-lead-currency-select option:eq(0)").prop("selected", true);
        }

        var callTrackingCurrency = clientArray["call_tracking_currency"];

        if (callTrackingCurrency) {

            $("#flat-rate-per-month-currency-select,#per-number-fee-currency-select,#per-minute-fee-currency-select").val(callTrackingCurrency);
        } else {

            $("#flat-rate-per-month-currency-select,#per-number-fee-currency-select,#per-minute-fee-currency-select").find("option:eq(0)").prop("selected", true);
        }

        var callTrackingFlatRatePerMonth = clientArray["call_tracking_flat_rate_per_month"];

        if (callTrackingFlatRatePerMonth == "0") {

            callTrackingFlatRatePerMonth = "";
        }

        $("#flat-rate-per-month").val(callTrackingFlatRatePerMonth);

        var callTrackingPerNumberFee = clientArray["call_tracking_per_number_fee"];

        if (callTrackingPerNumberFee == "0") {

            callTrackingPerNumberFee = "";
        }

        $("#per-number-fee").val(callTrackingPerNumberFee);

        var callTrackingPerMinuteFee = clientArray["call_tracking_per_minute_fee"];

        if (callTrackingPerMinuteFee == "0") {

            callTrackingPerMinuteFee = "";
        }

        $("#per-minute-fee").val(callTrackingPerMinuteFee);

        var userToSendBillTo = clientArray["user_to_send_bill_to"];
        var userToSendBillToString = "";

        if (userToSendBillTo) {

            userToSendBillToString = userToSendBillTo;
        }

        $("#user-to-send-bill-to").val(userToSendBillToString);

        var frequency = clientArray["frequency"];

        if (frequency) {

            $("#frequency").val(frequency);
        } else {

            $("#frequency option:eq(0)").prop("selected", true);
        }

        var billNotPaidPastDueDays = clientArray["bill_not_paid_past_due_days"];

        if (billNotPaidPastDueDays == "0") {

            billNotPaidPastDueDays = "";
        }

        $("#bill-not-paid-past-due-days").val(billNotPaidPastDueDays);

        var startDate = clientArray["startdate"];
        var startDateString = "";

        if (startDate) {

            var startDateTimeArray = startDate.split(" ");

            var startDateArray = startDateTimeArray[0].split("-");

            startDateString = startDateArray[1] + "/" + startDateArray[2] + "/" + startDateArray[0];
        }

        $("#add-client-start-date").val(startDateString);
        $("#add-client-start-date").prop('disabled', true);

        var invoiceEmailTemplate = clientArray["invoice_email_template"];

        if (invoiceEmailTemplate) {

            $("#invoice-email-template").val(invoiceEmailTemplate);
        } else {

            $("#invoice-email-template option:eq(0)").prop("selected", true);
        }

        var clientName = clientArray["client_name"];

        $("#client-name").val(clientName);

        var address = clientArray["address"];

        $("#address").val(address);

        var city = clientArray["city"];

        $("#city").val(city);

        var state = clientArray["state"];

        $("#state").val(state);

        var zipCode = clientArray["zip_code"];

        $("#zip-code").val(zipCode);

        $("#add-client-dialog #save-add-client-button").attr("mode", "edit");
        $("#add-client-dialog #save-add-client-button").attr("editclientid", clientId);
        $("#add-client-dialog #save-add-client-button").attr("editcronid", cronId);

        var dialog = $("#add-client-dialog").dialog({
            modal: true,
            resizable: false,
            draggable: false,
            autoOpen: true,
            width: 630,
            height: 'auto',
            minHeight: 0,
            dialogClass: "add-client-dialog"
        });
    });

    //sync call tracking currency drop downs.
    $("#flat-rate-per-month-currency-select,#per-number-fee-currency-select,#per-minute-fee-currency-select").change(function() {

        var theSelect = $(this);

        $("#flat-rate-per-month-currency-select,#per-number-fee-currency-select,#per-minute-fee-currency-select").val(theSelect.val());
    });

    //clicking the delete client button.
    $(".delete-client-button").click(function() {

        if (confirm("Are you sure you want to delete the client?")) {

            var deleteClientButton = $(this);

            var clientId = deleteClientButton.parents("tr:eq(0)").attr("clientid");
            var cronId = deleteClientButton.parents("tr:eq(0)").attr("cronid");

            //send request to delete the client.
            $.ajax({
                url: "manage_billing.php",
                type: "POST",
                dataType: "json",
                data: {
                    ajaxRequest: "deleteClient",
                    clientId: clientId,
                    cronId: cronId
                },
                success: function() {

                    window.location = "manage_billing.php";
                }
            });
        }
    });


    //clicking the paypal integration radio options.
    //    $("[name='paypal-option-radiogroup']").click(function() {
    //
    //	    var radio = $(this);
    //
    //	    if (radio.attr("id") == "paypal-merchant-id") {
    //
    //		    $("#paypal-merchant-id-text").parent().show();
    //		    $("#paypal-email-text").val("").parent().hide();
    //	    }
    //	    else if (radio.attr("id") == "paypal-email") {
    //
    //		    $("#paypal-merchant-id-text").val("").parent().hide();
    //		    $("#paypal-email-text").parent().show();
    //	    }
    //    });

    //clicking the Integrate Paypal button.
    $("#integrate-paypal-button").click(function() {

        var dialog = $("#integrate-paypal-dialog").dialog({
            modal: true,
            resizable: false,
            draggable: false,
            autoOpen: true,
            width: 630,
            height: 'auto',
            minHeight: 0,
            dialogClass: "integrate-paypal-dialog"
        });
    });

    var paypalIntegrationOption = paypalIntegrationInfo[0];
    var paypalIntegrationId = paypalIntegrationInfo[1];

    //$("[name='paypal-option-radiogroup']#" + paypalIntegrationOption).prop("checked", true).click();
    //$("#" + paypalIntegrationOption + "-text").val(paypalIntegrationId);


    //clicking the Save button on the Integrate Paypal dialog.
    $("#integrate-paypal-dialog #save-integrate-paypal-button").click(function() {

        var paypalIntegrationOption = $("[name='paypal-option-radiogroup']:checked").val();
        var paypalIntegrationId;

        if (paypalIntegrationOption == "paypal-merchant-id") {

            paypalIntegrationId = $("#paypal-merchant-id-text").val();
        } else if (paypalIntegrationOption == "paypal-email") {

            paypalIntegrationId = $("#paypal-email-text").val();
        }

        //send request to save the paypal integration info.
        $.ajax({
            url: "manage_billing.php",
            type: "POST",
            dataType: "json",
            data: {
                ajaxRequest: "saveIntegratePaypalData",
                paypalIntegrationOption: paypalIntegrationOption,
                paypalIntegrationId: paypalIntegrationId
            },
            success: function() {

                window.location = "manage_billing.php";
            }
        });

        $("#integrate-paypal-dialog").dialog("close");
        $("#integrate-paypal-dialog").dialog("destroy").remove();
    });

    //clicking the Cancel button on the Integrate Paypal dialog.
    $("#integrate-paypal-dialog #cancel-integrate-paypal-button").click(function() {

        $("#integrate-paypal-dialog").dialog("close");
    });
});

function getClientArrayFromClientsArray(clientId) {

    var clientArray;

    for (var i = 0; i < clientsArray.length; i++) {

        if (clientId == clientsArray[i]["id"]) {

            clientArray = clientsArray[i];
            break;
        }
    }

    return (clientArray);
}

function getCompanyArrayFromCompaniesArray(companyId) {

    var companyArray;

    for (var i = 0; i < companiesArray.length; i++) {

        if (companyId == companiesArray[i]["idx"]) {

            companyArray = companiesArray[i];
            break;
        }
    }

    return (companyArray);
}

function getPhoneCodeArrayFromPhoneCodesArray(phoneCodeId) {

    var phoneCodeArray;

    for (var i = 0; i < phoneCodesArray.length; i++) {

        if (phoneCodeId == phoneCodesArray[i]["idx"]) {

            phoneCodeArray = phoneCodesArray[i];
            break;
        }
    }

    return (phoneCodeArray);
}