$(document).ready(function () {
	
	var invoicesTableHtml = "";
	
	//loop through invoices array.
	for (var i = 0; i < invoicesArray.length; i++) {
	
		var invoiceId = invoicesArray[i]["id"];
		var invoiceDate = invoicesArray[i]["date"];
		var invoiceFromDate = invoicesArray[i]["from_date"];
		var invoiceToDate = invoicesArray[i]["to_date"];
		var invoiceTotalAmount = invoicesArray[i]["totalAmount"];
		var invoiceAmountCurrency = invoicesArray[i]["amount_currency"];
		//var invoiceAmountPaid = invoicesArray[i]["amount_paid"];
        var invoicestatus = invoicesArray[i]["is_paid"];
        var pending = invoicesArray[i]["is_pending"];
        var declined = invoicesArray[i]["is_declined"];
        var is_offline = invoicesArray[i]["is_offline"];
		var dueDate = invoicesArray[i]["dueDate"];
		var invoiceActive = invoicesArray[i]["active"];

        var status = "";

        if(invoicestatus == 1){
            if(is_offline!=1)
                status = "<span class='invoice-status paid'>Paid</span>";
            else
                status = "<span class='invoice-status paid'>Paid Offline</span>";
        }else
            status = "<span class='invoice-status unpaid'>Not Paid</span>";

        if(pending==1)
            status = "<span class='invoice-status pending'>Pending</span>";

        if(declined==1)
            status = "<span class='invoice-status declined'>Declined</span>";

		
		var invoiceName = "INV-" + invoicesArray[i]["date_mysql_format"].replace(/-/g, "") + invoiceId;
		
		invoicesTableHtml += "<tr invoiceid='" + invoiceId + "' startdate='"+invoicesArray[i]["startdate"]+"' enddate='"+invoicesArray[i]["enddate"]+"' clientid='"+ invoicesArray[i]["client_id"] +"'>";
		invoicesTableHtml += "<td><a class='view-invoice' title='View Invoice' target='_blank' href='billing_log_invoice.php?invoice_id=" + invoicesArray[i]["date_mysql_format"].replace(/-/g, "")+invoiceId + "&rkey="+invoicesArray[i]["sess_key"]+"' target='_blank'>" + invoiceName + "</a></td>";
		invoicesTableHtml += "<td>" + invoiceDate + "</td>";
		invoicesTableHtml += "<td>" + invoiceFromDate + "</td>";
		invoicesTableHtml += "<td>" + invoiceToDate + "</td>";
		invoicesTableHtml += "<td>" + invoiceTotalAmount + " " + invoiceAmountCurrency + "</td>";
		invoicesTableHtml += "<td>" + status + "</td>";
		invoicesTableHtml += "<td>" + dueDate + "</td>";
		invoicesTableHtml += "<td>";
		
		if (invoiceActive == "1") {
		
			invoicesTableHtml += "<a href='#' class='email-invoice-button' title='Email Invoice to Client'><img width='16' src='images/email-icon.gif' /></a> ";
            if(invoicesArray[i]["is_paid"]!=1 && invoicesArray[i]["is_pending"]!=1){
                invoicesTableHtml += "<a href='#' class='regenerate-invoice-button' title='Regenerate Invoice'><img width='16' src='images/regenerate.png' /></a> ";
                invoicesTableHtml += "<a href='#' class='offline-payment-button' title='Click here to mark as Paid Offline'><img width='16' src='images/money_icon.png'/></a> "
            }
            if(invoicesArray[i]["is_offline"]==1){
                invoicesTableHtml += "<a href='#' class='offline-payment-reverse-button' title='Set as not paid'><img width='16' src='images/money_icon_x.png' /></a> ";
            }
			invoicesTableHtml += "<a href='#' class='delete-invoice-button' title='Delete Invoice'><img src='images/delete.gif' /></a>";
		}
		
		invoicesTableHtml += "</td>";
		invoicesTableHtml += "</tr>";
	}
	
	$("#invoices-table tbody").html(invoicesTableHtml);

    $(".delete-invoice-button, .email-invoice-button, .view-invoice, .regenerate-invoice-button, .offline-payment-button, .offline-payment-reverse-button").tooltipster({
        position:'bottom',
        theme:'.tooltipster-shadow'
    });

    $('.offline-payment-reverse-button').click(function(){
        if(confirm("Are you sure you want to mark as \"Not Paid\"?")){
            var button = $(this);

            var invoiceId = button.parents("tr:eq(0)").attr("invoiceid");

            $.ajax({
                url: "billing_log.php",
                type: "POST",
                dataType: "json",
                data: {
                    ajaxRequest: "markInvoicePaidOfflineReverse",
                    invoiceId: invoiceId
                },
                success: function() {

                    window.location = "billing_log.php";
                }
            });
        }
    });

    $('.offline-payment-button').click(function(){

        if (confirm("Are you sure you want to mark the invoice as \"Paid Offline\"?")) {

            var button = $(this);

            var invoiceId = button.parents("tr:eq(0)").attr("invoiceid");

            //send request to delete the client.
            $.ajax({
                url: "billing_log.php",
                type: "POST",
                dataType: "json",
                data: {
                    ajaxRequest: "markInvoicePaidOffline",
                    invoiceId: invoiceId
                },
                success: function() {

                    window.location = "billing_log.php";
                }
            });
        }

    });

	//clicking the delete invoice button.
    $(".delete-invoice-button").click(function() {
	    
		if (confirm("Are you sure you want to delete the invoice?")) {
		    
		    var deleteInvoiceButton = $(this);
		    
		    var invoiceId = deleteInvoiceButton.parents("tr:eq(0)").attr("invoiceid");
		    
		    //send request to delete the client.
		    $.ajax({
			    url: "billing_log.php",
			    type: "POST",
			    dataType: "json",
			    data: {
				    ajaxRequest: "deleteInvoice",
				    invoiceId: invoiceId
			    },
			    success: function() {
				    
					window.location = "billing_log.php";
			    }
		    });
		}
    });

    var __regenerating = false;

    //clicking the regenerate invoice button.
    $(".regenerate-invoice-button").click(function() {

        if(__regenerating === true)
            alert("Please wait while the other process finishes.");
        else{
            if (confirm("Are you sure you want to regenerate the invoice?")) {

                var regenerateInvoiceButton = $(this);

                var invoiceId = regenerateInvoiceButton.parents("tr:eq(0)").attr("invoiceid");
                var clientId = regenerateInvoiceButton.parents("tr:eq(0)").attr("clientid");
                var startDate= regenerateInvoiceButton.parents("tr:eq(0)").attr("startdate");
                var endDate = regenerateInvoiceButton.parents("tr:eq(0)").attr("enddate");

                //send request to delete the client.
                $.ajax({
                    url: "invoice.php",
                    type: "GET",
                    dataType: "json",
                    data: {
                        client_id: clientId,
                        start_date: startDate,
                        end_date: endDate,
                        invoice_id: invoiceId
                    },
                    success: function() {
                        __regenerating = false;
                        alert('Finished regenerating invoice!');
                        window.location = "billing_log.php";
                    }
                });
                __regenerating = true;
            }
        }
    });
    
    //clicking the email invoice button.
    $(".email-invoice-button").click(function() {
	    
		if (confirm("When you click Ok, the invoice will be sent to the client with a button to pay using Paypal.")) {
		    
		    var emailInvoiceButton = $(this);
		    
		    var invoiceId = emailInvoiceButton.parents("tr:eq(0)").attr("invoiceid");
		    
		    //send request to delete the client.
		    $.ajax({
			    url: "billing_log.php",
			    type: "POST",
			    dataType: "json",
			    data: {
				    ajaxRequest: "emailInvoice",
				    invoiceId: invoiceId
			    },
			    success: function() {
				    
					alert("Email sent successfully!");
			    }
		    });
		}
    });
    
    //the CSV Export button.
    $("#exportCsvButton").click(function() {
	    
	    var clientId = $(this).attr("clientid");
	    
	    //send request to export invoice data.
	    $.ajax({
		    url: "billing_log.php",
		    type: "POST",
		    dataType: "json",
		    data: {
			    ajaxRequest: "exportCsv",
			    clientId: clientId
		    },
		    success: function() {
			    
				
		    }
	    });
    });
});