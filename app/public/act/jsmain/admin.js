/*!
 * Web1 CTM Admin JS
 *
 * Copyright 2012, Web1Syndication
 */
function systemClickEvt(e) {
    //alert($(this).attr('id'));
    e.preventDefault();
    var params = $(this).attr("data-params");

    console.log("[DEBUG] Executing: [" + $(this).attr('id') + "] with params [" + params + "]");

    switch ($(this).attr('id')) {
        case "SYSTEM_ADD_COMPANY":
            {
                var newCompany = '<p style="font-size:15px;">\
<form>\
<fieldset>\
<label for="name">Company Name</label>\
<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />\
</fieldset>\
</form></p>';
                var $dialog = $('<div></div>').html(newCompany).dialog({
                    modal: true,
                    autoOpen: true,
                    minHeight: 177,
                    maxWidth: 587,
                    minWidth: 520,
                    height: 'auto',
                    minWidth: 520,
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\
Add Company',
                    buttons: [{
                        text: "Ok",
                        click: function() {
                            if ($("#name").val() == "") {
                                msgDialog("Please fill in a Company name.");
                                return false;
                            } else {
                                var params = {
                                    name: $("#name").val()
                                }
                                var response = ajaxCall({
                                    func: "SYSTEM_ADD_COMPANY",
                                    data: params
                                });
                                response.done(function(msg) {
                                    var msg = msg.split(":::");
                                    console.log(msg);
                                    if (msg[0] == "SUCCESS") {
                                        msgDialogCB("Company Created.", (function() {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                            document.location.reload();
                                        }));

                                    } else errMsgDialog("There was an error in the request. (Duplicate Company Name?)");
                                });
                            }
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove();
                        }
                    }]
                });

                break;
            }
        case "SYSTEM_SET_COMPANY_INFO":
            {
                var response = JSONajaxCall({
                    func: "SYSTEM_SAVE_DB_VAR",
                    data: {
                        dbvar: "company_info",
                        dbval: $("#company_info").val()
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success")
                        infoMsgDialog("Saved company info.");
                    else
                        errMsgDialog("An error has occurred.");
                });
                break;
            }
        case "SYSTEM_ENABLE_SUPPORTLINK":
            {
                $("#filter-box").block({
                    message: '<h1 style="color:#fff">Loading</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .7,
                        color: '#fff !important'
                    }
                });

                var dbval = "no";
                if (params == "no") {
                    dbval = "yes";
                }
                var response = JSONajaxCall({
                    func: "SYSTEM_SAVE_DB_VAR",
                    data: {
                        dbvar: "enable_support_link",
                        dbval: dbval
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        $("#filter-box").unblock();
                        if ($('#SYSTEM_ENABLE_SUPPORTLINK').is(':checked')) {
                            $("#SYSTEM_ENABLE_SUPPORTLINK").removeAttr("checked");
                        } else {
                            $("#SYSTEM_ENABLE_SUPPORTLINK").attr("checked", "checked");
                        }
                        return true;
                    } else {
                        errMsgDialog("An error has occurred.");
                        $("#filter-box").unblock();
                    }
                });
                break;
            }
        case "SYSTEM_DISABLE_FOOTER":
            {
                $("#filter-box").block({
                    message: '<h1 style="color:#fff">Loading</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .7,
                        color: '#fff !important'
                    }
                });

                var dbval = "no";
                if (params == "no") {
                    dbval = "yes";
                }
                var response = JSONajaxCall({
                    func: "SYSTEM_SAVE_DB_VAR",
                    data: {
                        dbvar: "disable_footer",
                        dbval: dbval
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        $("#filter-box").unblock();
                        if ($('#SYSTEM_DISABLE_FOOTER').is(':checked')) {
                            $("#SYSTEM_DISABLE_FOOTER").removeAttr("checked");
                        } else {
                            $("#SYSTEM_DISABLE_FOOTER").attr("checked", "checked");
                        }
                        return true;
                    } else {
                        errMsgDialog("An error has occurred.");
                        $("#filter-box").unblock();
                    }
                });
                break;
            }
        case "SYSTEM_SET_DIALTONE_URL":
            {
                var response = JSONajaxCall({
                    func: "SYSTEM_SAVE_DB_VAR",
                    data: {
                        dbvar: "intl_dialtone_url",
                        dbval: $("#dial_tone_url").val()
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success")
                        infoMsgDialog("Saved dial tone URL.");
                    else
                        errMsgDialog("An error has occurred.");
                });
                break;
            }
        case "SYSTEM_SET_LOGO_URL":
            {
                var response = JSONajaxCall({
                    func: "SYSTEM_SAVE_DB_VAR",
                    data: {
                        dbvar: "company_logo",
                        dbval: $("#company_logo_url").val()
                    }
                });
                response.done(function(msg) {
                    if (msg.result == "success")
                        infoMsgDialog("Saved company logo.");
                    else
                        errMsgDialog("An error has occurred.");
                });
                break;
            }
        case "SYSTEM_ADD_BLACKLIST":
            {
                var newBlacklist = '<p style="font-size:15px;">\
<form>\
<fieldset>\
<label for="name">Blacklist Name</label>\
<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />\
</fieldset>\
</form></p>';
                var $dialog = $('<div></div>').html(newBlacklist).dialog({
                    modal: true,
                    autoOpen: true,
                    minHeight: 177,
                    maxWidth: 587,
                    minWidth: 520,
                    height: 'auto',
                    minWidth: 520,
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\
Add Blacklist',
                    buttons: [{
                        text: "Ok",
                        click: function() {
                            if ($("#name").val() == "") {
                                msgDialog("Please fill in the blacklist's name.");
                                return false;
                            } else {
                                var params = {
                                    name: $("#name").val()
                                }
                                var response = ajaxCall({
                                    func: "SYSTEM_ADD_BLACKLIST",
                                    data: params
                                });
                                response.done(function(msg) {
                                    var msg = msg.split(":::");
                                    console.log(msg);
                                    if (msg[0] == "SUCCESS") {
                                        msgDialogCB("Blacklist Created.", (function() {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                            document.location.reload();
                                        }));

                                    } else errMsgDialog("There was an error in the request. (Duplicate Blacklist Name?)");
                                });
                            }
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove();
                        }
                    }]
                });

                break;
            }
        case "SYSTEM_EDIT_PC_TEMPLATE":
            {
                var editPhoneCodeHTML = '<!--<p class="validateTips"></p>-->\
<form id="c_frm">\
<fieldset style="margin-top:5px !important;">\
<label for="phone_code_name">Phone Code:</label>\
<input type="text" id="phone_code_name" class="ui-widget-content ui-corner-all" disabled="true" style="display:inline-block !important;height: 20px;width: 55%; margin-bottom:5px;" />\
<button style="width:100px; margin-left:35px;" id="add_phone_code">Add</button>\
<br /><br />\
<div style="width:98%; margin:0 auto; background:#AAA; height:1px;"></div>\
</fieldset>\
</form>\
<table style="width: 100%;" id="phone_code_t">\
<thead><tr><th align="left">Phone Code</th><th align="left"></th><th align="left">Delete</th></tr></thead>\
<tbody>\
{{phone_code_rows}}\
</tbody></table>\
<span style="position: absolute; bottom:0px; color: #AAA;width: 245px;margin-left: 15px;font: 10px Helvetica;">* Will be applied when you click "Save" *</span>';

                var response = JSONajaxCall({
                    func: "SYSTEM_GET_PHONE_CODES_FROM_TEMPLATE",
                    data: params
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        editPhoneCodeHTML = editPhoneCodeHTML.replace("{{phone_code_rows}}", msg.table_data);
                        var $dialog = $('<div></div>').html(editPhoneCodeHTML).dialog({
                            modal: true,
                            autoOpen: true,
                            minHeight: 489,
                            minWidth: 380,
                            maxWidth: 587,
                            height: 'auto',
                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Edit Phone Codes',
                            buttons: [{
                                text: "Save",
                                icons: {
                                    primary: "ui-icon-pencil"
                                },
                                click: function() {
                                    var phone_codes = [];
                                    $("#phone_code_t > tbody > tr").each(function(i, v) {
                                        phone_codes.push($(v).text());
                                    });

                                    var response = JSONajaxCall({
                                        func: "SYSTEM_SAVE_PHONE_CODE_TO_TEMPLATE",
                                        data: {
                                            phone_codes: phone_codes,
                                            id: params
                                        }
                                    });
                                    response.done(function(data) {
                                        if (data.result == "success") {
                                            msgDialogCB("Phone Codes updated.", (function() {
                                                $($dialog).dialog("close");
                                                $($dialog).dialog('destroy').remove();
                                                document.location.reload();
                                            }));
                                        } else {
                                            errMsgDialog("There was an error in the request.");
                                        }
                                    });
                                }
                            }, {
                                text: "Cancel",
                                click: function() {
                                    $(this).dialog("close");
                                    $(this).dialog('destroy').remove();
                                }
                            }]
                        });

                        $("#add_phone_code").button();
                        $("*[id*=SUB_DELETE_PHONE_CODE]").click(function(e) {
                            e.preventDefault();
                            $(deleted).each(function(i, v) {
                                if (v == $(this).attr("data-params"))
                                    return false;
                            });
                            deleted.push($(this).attr("data-params"));
                            $(this).parent().parent().remove();
                            return false;
                        });
                        $("#add_phone_code").click(function(e) {
                            e.preventDefault();

                            var phone_code_name = $("#phone_code_name").val();

                            if (phone_code_name == "") {
                                errMsgDialog("Can not add empty values!");
                            } else {
                                $("#phone_code_t > tbody:last").append("<tr class='new_pc'><td>" + phone_code_name + "</td><td></td><td><a href='#' onclick='$(this).parent().parent().remove()' =><img src=\"images/delete.gif\"></td></tr>");
                                $("#phone_code_name").val("");
                                $("#dial_code").val("");
                            }
                            return false;
                        });

                        $("#phone_code_name").removeAttr("disabled");
                        $("#phone_code_name").focus();

                    } else errMsgDialog("There was an error in the request.");
                });

                break;
            }

        case "SYSTEM_ADD_PC_TEMPLATE":
            {
                var newPCTemplate = '<label for="name">Template Name</label>\
<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />\
<input type="checkbox" name="outgoing" id="outgoing" style="display:inline-block !important;" />&nbsp;&nbsp;<label style="display:inline-block !important;" for="outgoing">Outgoing</label> ';
                var $dialog = $('<div></div>').html(newPCTemplate).dialog({
                    modal: true,
                    autoOpen: true,
                    minHeight: 177,
                    maxWidth: 340,
                    minWidth: 340,
                    height: 'auto',
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-plus" style="float:left; margin-right:5px;"></span>\
Add Template',
                    buttons: [{
                        text: "Ok",
                        click: function() {
                            if ($("#name").val() == "") {
                                msgDialog("Please fill in the template's name.");
                                return false;
                            } else {
                                if ($("#outgoing").is(":checked"))
                                    var outgoing = 2;
                                else
                                    var outgoing = 1;
                                var params = {
                                    name: $("#name").val(),
                                    outgoing: outgoing
                                };
                                var response = JSONajaxCall({
                                    func: "SYSTEM_ADD_PC_TEMPLATE",
                                    data: params
                                });
                                response.done(function(data) {
                                    if (data.result == "success") {
                                        msgDialogCB("Template Created.", (function() {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                            document.location.reload();
                                        }));

                                    } else errMsgDialog("There was an error in the request.");
                                });
                            }
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove();
                        }
                    }]
                });

                break;
            }

        case "SYSTEM_EDIT_CALL_FLOW_SETTINGS":
            {
                var title = $("#label_" + $(this).attr("data-params")).text();
                var company_id = $("#company_id").val();
                var callflowHTML = "";
                var width = 0;
                var height = 0;
                var flow_id = $(this).attr("data-params");
                switch (flow_id) {
                    case "1":
                        { //Ring 1 number

                            width = 250;
                            height = 177;
                            callflowHTML = '<p style="font-size:15px;">\
<form>\
<fieldset>\
<input type="checkbox" name="international_number" id="international_number" style="display: inline !important;" />&nbsp;\
<label for="international_number" style="display: inline !important;">International Number</label><br /><br />\
<label for="outgoing">Outgoing Number</label>\
<input type="text" name="outgoing" id="outgoing" class="text ui-widget-content ui-corner-all" />\
</fieldset>\
</form></p>';
                            break;
                        }
                    case "2": // Round robin
                    case "3":
                        { //Multiple numbers
                            width = 380;
                            height = 489;
                            callflowHTML = '<br><input type="checkbox" name="international_number" id="international_number" style="display: inline !important;" />&nbsp;\
<label for="international_number" style="display: inline !important;">International Number</label><br><br />\
<label for="phone_number">Phone Number:</label>\
<input type="text" id="phone_number" class="ui-widget-content ui-corner-all" style="display:inline-block !important;height: 20px;width: 50%;" />\
<button style="width:100px; margin-left:35px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" id="add_phone_number"><span class="ui-button-text">Add</span></button>\
<br /><br />\
<div style="width:98%; margin:0 auto; background:#AAA; height:1px;"></div>\
<table style="width: 100%;" id="number_t">\
<thead><tr><th align="left">Phone Number</th><th align="left">Delete</th></tr></thead><tbody>\
</tbody></table>\
<span style="position: absolute; bottom:0px; color: #AAA;width: 245px;margin-left: 15px;font: 10px Helvetica;">Number Example: 15552461128</span>';
                            break;
                        }
                }

                var dialog_div_id = "ecf_" + flow_id;

                if ($("#" + dialog_div_id).length == 0) {
                    $("body").append('<div id="' + dialog_div_id + '"></div>');
                }



                var $dialog = $('#' + dialog_div_id).html(callflowHTML).dialog({
                    modal: true,
                    autoOpen: true,
                    minHeight: height,
                    maxHeight: height,
                    minWidth: width,
                    maxWidth: width,
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>' + title + ' Settings',
                    buttons: [{
                        text: "Save",
                        click: function() {
                            switch (flow_id) {
                                case "1": // Ring 1 Number
                                    {
                                        if ($("#outgoing").val().length != 10 && !$("#international_number").is(":checked") && $("#outgoing").val() != "") {
                                            errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
                                            return false;
                                        } else {
                                            var intl = 0;
                                            if ($("#international_number").is(":checked")) {
                                                intl = 1;
                                            }
                                            var aparams = {
                                                outgoing: $("#outgoing").val(),
                                                company_id: company_id,
                                                intl: intl
                                            };
                                            var response = ajaxCall({
                                                func: "SYSTEM_EDIT_OUTGOING_NUMBER",
                                                data: aparams
                                            });
                                            response.done(function(msg) {
                                                var msg = msg.split(":::");
                                                console.log(msg);
                                                if (msg[0] == "SUCCESS") {
                                                    $($dialog).dialog("close");
                                                    $($dialog).dialog('destroy').remove();
                                                    msgDialogCB("Edited Outgoing Number.", (function() {
                                                        $($dialog).dialog("close");
                                                        $($dialog).dialog('destroy').remove();
                                                    }));
                                                } else errMsgDialog("There was an error in the request.");
                                            });
                                        }
                                        break;
                                    }
                                case "2":
                                case "3":
                                    {
                                        var func = "";
                                        if (flow_id == "2")
                                            func = "SYSTEM_EDIT_ROUND_ROBIN";
                                        else
                                            func = "SYSTEM_EDIT_MN";
                                        var numbers = new Array();
                                        $("#number_t > tbody").children().each(function(i, v) {
                                            numbers.push($(v).text());
                                        });
                                        var response = JSONajaxCall({
                                            func: func,
                                            company_id: company_id,
                                            numbers: numbers
                                        });
                                        response.done(function() {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                            msgDialogCB("Saved numbers.", (function() {
                                                $($dialog).dialog("close");
                                                $($dialog).dialog('destroy').remove();
                                            }));
                                        });
                                        break;
                                    }
                            }
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove()
                        }
                    }]
                });
                switch (flow_id) {
                    case "1":
                        {
                            var response = JSONajaxCall({
                                func: "SYSTEM_GET_OUTGOING_NUMBER",
                                data: company_id
                            });
                            response.done(function(data) {
                                $("#outgoing").val(data.number);
                                if (data.intl == 1)
                                    $("#international_number").attr("checked", "checked");
                            });
                            break;
                        }
                    case "2":
                    case "3":
                        {
                            var func = "";
                            if (flow_id == "2")
                                func = "SYSTEM_GET_ROUNDROBIN";
                            else
                                func = "SYSTEM_GET_MN";
                            var response = JSONajaxCall({
                                func: func,
                                data: company_id
                            });
                            response.done(function(data) {
                                $.each(data, function(i, v) {
                                    console.log(v);
                                    $('<tr><td>' + v.number + '</td><td><a href="#" onclick="$(this).parent().parent().remove()" =><img src="images/delete.gif"></td></tr>').appendTo("#number_t > tbody");
                                });
                            });
                            $("button#add_phone_number").click(function() {
                                var number = $("#phone_number").val();
                                if (number.length != 10 && !$("#international_number").is(":checked") && number != "" && $.isNumeric(number)) {
                                    errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
                                } else {
                                    if ($.isNumeric(number)) {
                                        if (!$("#international_number").is(":checked")) {
                                            $('<tr><td>' + "1" + number + '</td><td><a href="#" onclick="$(this).parent().parent().remove()" =><img src="images/delete.gif"></td></tr>').appendTo("#number_t > tbody");
                                            $("#phone_number").val("");
                                        } else {
                                            $('<tr><td>' + number + '</td><td><a href="#" onclick="$(this).parent().parent().remove()" =><img src="images/delete.gif"></td></tr>').appendTo("#number_t > tbody");
                                            $("#phone_number").val("");
                                        }
                                    } else {
                                        errMsgDialog("Invalid number.");
                                    }
                                }
                            });
                        }
                }
                break;
            }
        case "SYSTEM_EDIT_OUTGOING_NUMBER":
            {
                var outgoingNumMK = '<p style="font-size:15px;">\
<form>\
<fieldset>\
<input type="checkbox" name="international_number" id="international_number" style="display: inline !important;" />&nbsp;\
<label for="international_number" style="display: inline !important;">International Number</label><br /><br />\
<label for="outgoing">Outgoing Number</label>\
<input type="text" name="outgoing" id="outgoing" class="text ui-widget-content ui-corner-all" value="' + $(this).attr("data-number") + '" />\
</fieldset>\
</form></p>';
                outgoingNumMK = outgoingNumMK.replace("undefined", "");

                var $dialog = $('<div></div>').html(outgoingNumMK).dialog({
                    modal: true,
                    autoOpen: true,
                    minHeight: 177,
                    maxWidth: 587,
                    minWidth: 250,
                    height: 'auto',
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>\
Edit Outgoing Number',
                    buttons: [{
                        text: "Ok",
                        click: function() {
                            if ($("#outgoing").val() == "") {
                                //msgDialog("Please fill in the Outgoing Number.");
                                //return false;
                            }
                            if ($("#outgoing").val().length != 10 && !$("#international_number").is(":checked") && $("#outgoing").val() != "") {
                                errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number, please check the box.");
                                return false;
                            } else {
                                var intl = 0;
                                if ($("#international_number").is(":checked")) {
                                    intl = 1;
                                }
                                var aparams = {
                                    outgoing: $("#outgoing").val(),
                                    company_id: params,
                                    intl: intl
                                };
                                var response = ajaxCall({
                                    func: "SYSTEM_EDIT_OUTGOING_NUMBER",
                                    data: aparams
                                });
                                response.done(function(msg) {
                                    var msg = msg.split(":::");
                                    console.log(msg);
                                    if (msg[0] == "SUCCESS") {
                                        $($dialog).dialog("close");
                                        $($dialog).dialog('destroy').remove();
                                        msgDialogCB("Edited Outgoing Number.", (function() {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                            document.location.reload();
                                        }));

                                    } else errMsgDialog("There was an error in the request.");
                                });
                            }
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove()
                        }
                    }]
                });
                break;
            }
        case "SYSTEM_ADD_USER":
            {
                var editUserCompanyHTML = '<label>Companies</label><div id="company_list" style="max-height: 250px; overflow-y: scroll; border: solid 1px #CCCCCC; margin-bottom: 5px; padding: 5px;">{{company_details}}</div>';

                //Ajax, Get companies.
                var response = ajaxCall({
                    func: "SYSTEM_GET_COMPANY_LIST",
                    data: params
                });
                response.done(function(msg) {
                    var msg = msg.split(":::");
                    //console.log(msg);
                    if (msg[0] == "SUCCESS") {
                        var addonHTML = "";
                        var companies = msg[1].split("::");
                        companies.pop();
                        $.each(companies, (function(index, value) {
                            var checked = "";
                            var isIn = 'id="not"';
                            var company_detail = value.split(":");
                            if (company_detail[2] == 1) {
                                checked = "checked";
                                isIn = 'id="is"';
                            }
                            addonHTML += '<div><input style="display:inline !important;" type="checkbox" ' + isIn + ' ' + checked + ' value="' + company_detail[0] + '" />&nbsp;' + company_detail[1] + '</div>';
                        }));
                        editUserCompanyHTML = editUserCompanyHTML.replace("{{company_details}}", addonHTML);

                        var editUserAddonPrivHTML = '<div><label>Permissions</label><input type="radio" id="cr_0" name="call_restriction" style="display:inline-block !important;" value="0"><label for="cr_0" style="display:inline-block !important; padding-left: 5px;">All Calls</label><br>                         <input type="radio" id="cr_2" name="call_restriction" style="display:inline-block !important;" value="2"><label style="display:inline-block !important; padding-left: 5px;" for="cr_2">Only allow access to calls before this date: </label> <br /> <input type="radio" id="cr_1" name="call_restriction" style="display:inline-block !important;" value="1"><label style="display:inline-block !important; padding-left: 5px;" for="cr_1">Only allow access to calls after this date: </label>                         <input disabled="" type="text" id="restrict_call_date" value="" style="    display: inline-block !important;">                         <br><input type="checkbox" style="display:inline-block !important;" id="disable_outgoing" name="disable_outgoing"><label for="disable_outgoing" style="display: inline !important; padding-left: 5px; font-weight: normal;">Remove Outbound Call Links</label><br>                          <input type="checkbox" style="display:inline-block !important;" id="enable_phone_code" name="enable_phone_code"><label for="enable_phone_code" style="display: inline !important; padding-left: 5px; font-weight: normal;">Allow user to set Phone Codes</label>                         <br><br><label id="pa">Plugin Access:</label>                        <div id="addon_list"><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10001" id="ACT_MOBILE_APP"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_MOBILE_APP">ACT iPhone App</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10004" id="ACT_ANDROID_APP"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_ANDROID_APP">ACT Android App</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10002" id="ACT_CLICK2CALL"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_CLICK2CALL">ACT Click 2 Call</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10003" id="ACT_BROWSER_SOFTPHONE"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_BROWSER_SOFTPHONE">Browser Softphone</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10005" id="ACT_BROWSER_APPLET"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_BROWSER_APPLET">ACT Browser App</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10006" id="ACT_AUTO_DIALER"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_AUTO_DIALER">Auto Dialer</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10007" id="ACT_VOICE_BROADCAST"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_VOICE_BROADCAST">Voice Broadcast</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10008" id="ACT_CONTACTS"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_CONTACTS">Manage Contacts</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10009" id="ACT_PURCHASE_NUMBERS"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_PURCHASE_NUMBERS">Purchase Numbers</label></div><div style="float: left; width: 145px;"><input style="display:inline !important;" type="checkbox" value="10010" id="ACT_CUSTOMIZE_CALL_FLOW"><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="ACT_CUSTOMIZE_CALL_FLOW">Customize Call Flow</label></div></div><br></div>';

                        var newUserMK = '<p style="font-size:15px;"><span class="validateTips">All form fields are required.</span>\
        <form>\
        <fieldset>\
        <div style="float: left; width: 215px;">\
            <label for="name">Username</label>\
            <input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />\
            <label for="full_name">Email</label>\
            <input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" />\
        </div>\
        <div style="float: left; padding-left: 15px; width: 215px;">\
            <label for="full_name">Full Name</label>\
            <input type="text" name="full_name" id="full_name" value="" class="text ui-widget-content ui-corner-all" />\
            <label for="password">Password</label>\
            <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all" />\
        </div>\
        <div style="clear: both;"></div>' + editUserCompanyHTML + '' + editUserAddonPrivHTML + '\
        </fieldset>\
            </form></p>';
                            var $dialog = $('<div></div>').html(newUserMK).dialog({
                                modal: true,
                                autoOpen: true,
                                minHeight: 177,
                                maxWidth: 587,
                                minWidth: 520,
                                height: 'auto',
                                minWidth: 520,
                                title: '<span class="ui-button-icon-primary ui-icon ui-icon-person" style="float:left; margin-right:5px;"></span>\
            Add New User',
                                buttons: [{
                                    text: "Ok",
                                    click: function() {
                                        if ($("#name").val() == "") {
                                            msgDialog("Please fill in a username.");
                                            return false;
                                        }
                                        if ($("#email").val() == "") {
                                            msgDialog("Please fill in a email.");
                                            return false;
                                        }
                                        if ($("#full_name").val() == "") {
                                            msgDialog('Please enter the full name.');
                                            return false;
                                        }
                                        if ($("#password").val() == "") {
                                            msgDialog("Please enter a password.");
                                            return false;
                                        } else {
                                            var _companies_in = new Array();

                                            $("input[id$=not]").each(function(index, element) {
                                                if ($(element).is(':checked')) {
                                                    _companies_in.push($(element).val());
                                                }
                                            });

                                            var call_restrict_enabled = $("input[name='call_restriction']:checked").val();
                                            if ((call_restrict_enabled == 1 || call_restrict_enabled == 2) && $("#restrict_call_date").val() == "") {
                                                errMsgDialog("Please enter a date!");
                                                return false;
                                            }

                                            if (call_restrict_enabled == 0)
                                                $("#restrict_call_date").val("");

                                            var checked = [];
                                            $("#addon_list").children("div").children("input:checked").each(function(i, v) {
                                                checked.push(v.value);
                                            });

                                            var disable_outgoing = 0;
                                            var enable_phone_code = 0;

                                            if ($("#disable_outgoing").is(':checked'))
                                                disable_outgoing = 1;
                                            if ($("#enable_phone_code").is(':checked'))
                                                enable_phone_code = 1;

                                            var permissions = {
                                                checked: checked,
                                                disable_outgoing: disable_outgoing,
                                                enable_phone_code: enable_phone_code,
                                                call_restriction_enabled: call_restrict_enabled,
                                                call_restriction_date: $("#restrict_call_date").val()
                                            };

                                            var params = {
                                                username: $("#name").val(),
                                                full_name: $("#full_name").val(),
                                                password: $("#password").val(),
                                                email: $("#email").val(),
                                                companies: _companies_in,
                                                permissions: permissions
                                            };
                                            var response = ajaxCall({
                                                func: "SYSTEM_ADD_USER",
                                                data: params
                                            });
                                            response.done(function(msg) {
                                                var msg = msg.split(":::");
                                                console.log(msg);
                                                if (msg[0] == "SUCCESS") {
                                                    msgDialogCB("User Created.", (function() {
                                                        $("#users tbody").append("<tr data-key='" + msg[1] + "'>" +
                                                            "<td style=\"white-space: nowrap;\">" + msg[2] + "</td>" +
                                                            "<td style=\"white-space: nowrap;\">" + msg[3] + "</td>" +
                                                            "<td>" + msg[4] + "</td>" +
                                                            '<td><a href="#" id="SYSTEM_EDIT_USER_COMPANY" data-params="' + msg[1] + '" title="Edit Companies" style="cursor:pointer">' + _companies_in.length + '</a></td>' +
                                                            '<td style="min-width: 115px;"><a href="#" id="SYSTEM_DELETE_USER" data-params="' + msg[1] + '" title="Delete User"><img src="images/delete.gif"></a>&nbsp;&nbsp;<a href="#" id="SYSTEM_PROMOTE_USER" data-params="' + msg[1] + '" title="Promote User to Admin"><img src="images/up_arrow_16.png"></a>&nbsp;&nbsp;<a href="#" id="SYSTEM_USER_PRIV" data-params="' + msg[1] + '" title="Change the permissions for this user"><img src="images/1355869412_key.png"></a>&nbsp;&nbsp;<a href="#" id="SYSTEM_USER_PHONE_PRIV" data-params="' + msg[1] + '" title="Change allowed phone numbers."><img src="images/phone-dg-32.png" width="16"></a>&nbsp;&nbsp;<a href="#" id="SYSTEM_RESET_USER" data-params="' + msg[1] + '" title="Reset Password and Email it to the user"><img width="16" src="images/regenerate.png"></a>                    &nbsp;&nbsp;<a href="#" id="SYSTEM_SET_FORWARDING_EMAIL" data-params="' + msg[1] + '" title="Setup email forwarding"><img src="images/email-icon.gif" style="width:16px;"></a></td>' +
                                                            "</tr>");

                                                        $("*[id*=SYSTEM_]").unbind("click").click(systemClickEvt);
                                                        $($dialog).dialog("close");
                                                        $($dialog).dialog('destroy').remove();
                                                    }));
                                                } else if (msg[0] == "EXISTS") {
                                                    errMsgDialog("That username already exists.");
                                                } else errMsgDialog("There was an error in the request.");
                                            });
                                        }
                                    }
                                }, {
                                    text: "Cancel",
                                    click: function() {
                                        $(this).dialog("close");
                                        $(this).dialog('destroy').remove()
                                    }
                                }]
                            });
                    }


                    var lcl = $("#lcl").val();
                    if (lcl < 2) {
                        $("#disable_outgoing").attr("style", "display:none !important");
                        $("label[for='disable_outgoing']").attr("style", "display:none !important");
                        $("#pa").attr("style", "display:none !important");
                        $("#addon_list").attr("style", "display:none !important");
                        $dialog.minHeight = 100;
                        $dialog.maxnHeight = 100;
                    }
                    $("input[id^='cr_']").click(function(e) {
                        var call_restrict_enabled = e.currentTarget.id;
                        if (call_restrict_enabled == 'cr_1' || call_restrict_enabled == 'cr_2') {
                            $("input#restrict_call_date").removeAttr("disabled");
                        } else {
                            $("input#restrict_call_date").attr("disabled", "disabled");
                        }
                    });
                    $("#restrict_call_date").datetimepicker({
                        timeFormat: "hh:mm tt"
                    });
                });
            
            break;
            }
        case "SYSTEM_EDIT_COMPANY_SETTINGS":
            {
                $(".ui-dialog").remove();
                $("#edit_company_form").remove();

                var editCompanyMK = '<p class="validateTips">Loading...';

                $.get('manage_companies.php?act=company_settings_form&cId=' + params, function(data) {
                    editCompanyMK = data;

                    var $dialog = $('<div></div>').html(editCompanyMK).dialog({
                        modal: true,
                        autoOpen: true,
                        minHeight: 177,
                        width: 520,
                        maxWidth: 587,
                        height: 'auto',
                        title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>\
Edit Company Settings',
                        buttons: [{
                            text: "Save",
                            icons: {
                                primary: "ui-icon-pencil"
                            },
                            click: function() {
                                var data_params = {
                                    whisper: $('input[name="whisper"]').val(),
                                    whisper_type: $('input[name="whisper_type"]').val(),
                                    whisper_voice: $('input[name="whisper_voice"]').val(),
                                    whisper_language: $('input[name="whisper_language"]').val(),
                                    recording_notification: $('input[name="recording_notification"]').val(),
                                    recording_notification_type: $('input[name="recording_notification_type"]').val(),
                                    recording_notification_voice: $('input[name="recording_notification_voice"]').val(),
                                    recording_notification_language: $('input[name="recording_notification_language"]').val(),
                                    rec_disable: $('input[name="rec_disable"]').is(":checked"),
                                    record_from_ring: $('input[name="record_from_ring"]').is(":checked"),
                                    id: params,
                                    ga_id: $("#ga_id").val(),
                                    ga_domain: $("#ga_domain").val(),
                                    km_api_key: $("#km_api_key").val()
                                };
                                console.log(data_params);
                                $($dialog).dialog("close");
                                $($dialog).dialog('destroy').remove();
                                var response = ajaxCall({
                                    func: "SYSTEM_EDIT_COMPANY_SETTINGS",
                                    data: data_params
                                });
                                response.done(function(data) {
                                    var msg = data.split(":::");
                                    if (msg[0] == "FAILURE") errMsgDialog("Unknown error.");
                                    else {
                                        msgDialogCB("Saved Settings.", (function() {
                                            $(this).dialog("close");
                                            $(this).dialog('destroy').remove();
                                            //document.location.reload();
                                        }));
                                    }
                                    //console.log(msg);
                                });
                            }
                        }, {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                                $(this).dialog('destroy').remove()
                            }
                        }]
                    });
                    $($dialog).find("input[name='ga_enable']").click(function() {
                        if ($(this).is(":checked")) {
                            $("#ga_id").prop("disabled", false);
                            $("#ga_domain").prop("disabled", false);
                            $("#ga_table").show();
                        } else {
                            $("#ga_id").prop("disabled", true).val("");
                            $("#ga_domain").prop("disabled", true).val("");
                            $("#ga_table").hide();
                        }
                    });
                    $($dialog).find("input[name='kissmetrics_enable']").click(function() {
                        if ($(this).is(":checked")) {
                            $("#km_api_key").prop("disabled", false);
                            $("#kw_table").show();
                        } else {
                            $("#km_api_key").prop("disabled", true).val("");
                            $("#kw_table").hide();
                        }
                    });
                });
                break;
            }
        case "SYSTEM_EDIT_COMPANY_NUMBERS":
            {
                if ($('a[data-params^="' + params + '"][id^="SYSTEM_EDIT_OUTGOING_NUMBER"]').text() != "None") {
                    var editNumberCompanyHTML = '<p style="font-size:15px;">\
<form>\
<fieldset>\
<div id="number_list">{{number_details}}</div>\
</fieldset>\
</form></p>';
                    //Ajax, Get company numbers.
                    var response = ajaxCall({
                        func: "SYSTEM_GET_PHONE_NUMBER_LIST",
                        data: params
                    });
                    response.done(function(msg) {
                        var msg = msg.split(":::");
                        //console.log(msg);
                        if (msg[0] == "SUCCESS") {
                            var addonHTML = "";
                            var numbers = msg[1].split("::");
                            numbers.pop();
                            $.each(numbers, (function(index, value) {
                                var checked = "";
                                var isIn = 'id="not"';
                                var number_detail = value.split(":");
                                if (number_detail[1] == 1) {
                                    checked = "checked";
                                    isIn = 'id="is"';
                                }

                                addonHTML += '<div><input style="display:inline !important;" type="checkbox" ' + isIn + ' ' + checked + ' value="' + number_detail[0] + '" international="' + number_detail[2] + '" />&nbsp;' + number_detail[0] + '&nbsp;<strong>( ' + number_detail[3] + ' )</strong></div><br>';
                            }));
                            editNumberCompanyHTML = editNumberCompanyHTML.replace("{{number_details}}", addonHTML);
                            var $dialog = $('<div></div>').html(editNumberCompanyHTML).dialog({
                                modal: true,
                                autoOpen: true,
                                minHeight: 177,
                                maxWidth: 620,
                                height: 'auto',
                                title: '<span class="ui-button-icon-primary ui-icon ui-icon-suitcase" style="float:left; margin-right:5px;"></span>\
Edit Company Numbers',
                                buttons: [{
                                    text: "Save",
                                    icons: {
                                        primary: "ui-icon-pencil"
                                    },
                                    click: function() {
                                        var _numbers_in = [];
                                        var _numbers_out = [];

                                        $("input[id$=not]").each(function(index, element) {
                                            if ($(element).is(':checked')) {
                                                console.log("out: " + $(element).val());
                                                _numbers_in.push(new Array($(element).val(), $(element).attr("international")));
                                            }
                                        });
                                        $("input[id$=is]").each(function(index, element) {
                                            if (!$(element).is(':checked')) {
                                                console.log("in: " + $(element).val());
                                                _numbers_out.push($(element).val());
                                            }
                                        });

                                        var data_params = {
                                            numbers_out: _numbers_out,
                                            numbers_in: _numbers_in,
                                            id: params
                                        };
                                        console.log(data_params);
                                        $($dialog).dialog("close");
                                        $($dialog).dialog('destroy').remove();
                                        var response = ajaxCall({
                                            func: "SYSTEM_EDIT_COMPANY_NUMBERS",
                                            data: data_params
                                        });
                                        response.done(function(msg) {
                                            var msg = msg.split(":::");
                                            if (msg[0] == "FAILURE") infoMsgDialog("Nothing changed.");
                                            else {
                                                msgDialogCB("Saved numbers for company.", (function() {
                                                    $(this).dialog("close");
                                                    $(this).dialog('destroy').remove();
                                                    document.location.reload();
                                                }));
                                            }
                                            console.log(msg);
                                        });
                                    }
                                }, {
                                    text: "Cancel",
                                    click: function() {
                                        $(this).dialog("close");
                                        $(this).dialog('destroy').remove()
                                    }
                                }]
                            });
                        } else errMsgDialog("There were no free numbers to assign. Please add some.");
                    });
                } else {
                    errMsgDialog("Please add an Outgoing Number.");
                }
                break;
            }
        case "SYSTEM_EDIT_BLACKLIST_COMPANY":
            {
                var editBlacklistHTML = '<form>\
<fieldset>\
<label for="blacklists">Blacklist</label>\
<select id="blacklists">\
<option value="0">None</option>\
{{OPTIONS}}\
</select>\
</fieldset>\
</form>';
                //Ajax, Get companies.
                var response = ajaxCall({
                    func: "SYSTEM_GET_BLACKLISTS",
                    data: params
                });
                response.done(function(msg) {
                    var msg = msg.split(":::");
                    //console.log(msg);
                    if (msg[0] == "SUCCESS") {
                        editBlacklistHTML = editBlacklistHTML.replace("{{OPTIONS}}", msg[1]);
                        var $dialog = $('<div></div>').html(editBlacklistHTML).dialog({
                            modal: true,
                            autoOpen: true,
                            minHeight: 177,
                            maxWidth: 587,
                            height: 'auto',
                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-suitcase" style="float:left; margin-right:5px;"></span>\
Edit Blacklist for Company',
                            buttons: [{
                                text: "Save",
                                icons: {
                                    primary: "ui-icon-pencil"
                                },
                                click: function() {
                                    var data_params = {
                                        blacklist_id: $("#blacklists").val(),
                                        company_id: params
                                    };
                                    //console.log(data_params);
                                    $($dialog).dialog("close");
                                    $($dialog).dialog('destroy').remove();
                                    var response = ajaxCall({
                                        func: "SYSTEM_EDIT_BLACKLIST_FOR_COMPANY",
                                        data: data_params
                                    });
                                    response.done(function(msg) {
                                        var msg = msg.split(":::");
                                        if (msg[0] == "FAILURE") infoMsgDialog("Nothing changed.");
                                        else {
                                            msgDialogCB("Saved blacklist!", (function() {
                                                $(this).dialog("close");
                                                $(this).dialog('destroy').remove();
                                                document.location.reload();
                                            }));
                                        }
                                        console.log(msg);
                                    });
                                }
                            }, {
                                text: "Cancel",
                                click: function() {
                                    $(this).dialog("close");
                                    $(this).dialog('destroy').remove()
                                }
                            }]
                        });
                    } else errMsgDialog("Please add a blacklist.");
                });
                break;
            }
        case "SYSTEM_EDIT_BLACKLIST":
            {
                var editBlacklistHTML = '<p class="validateTips">Comma separated numbers. Include the "+". Example: +17775556666, +14446662222</p>\
<form>\
<fieldset>\
<label for="numbers">Numbers</label>\
<textarea name="numbers" id="numbers" class="text ui-widget-content ui-corner-all" style="height: 96px; width: 100%;" >{{numbers}}</textarea>\
</fieldset>\
</form>';


                //Ajax, Get companies.
                var response = ajaxCall({
                    func: "SYSTEM_GET_BLACKLIST",
                    data: params
                });
                response.done(function(msg) {
                    var msg = msg.split(":::");
                    //console.log(msg);
                    if (msg[0] == "SUCCESS") {
                        editBlacklistHTML = editBlacklistHTML.replace("{{numbers}}", msg[1]);
                        var $dialog = $('<div></div>').html(editBlacklistHTML).dialog({
                            modal: true,
                            autoOpen: true,
                            minHeight: 177,
                            maxWidth: 587,
                            height: 'auto',
                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-suitcase" style="float:left; margin-right:5px;"></span>\
Edit Blacklist Numbers',
                            buttons: [{
                                text: "Save",
                                icons: {
                                    primary: "ui-icon-pencil"
                                },
                                click: function() {
                                    var data_params = {
                                        numbers: $("#numbers").val(),
                                        id: params
                                    };
                                    //console.log(data_params);
                                    $($dialog).dialog("close");
                                    $($dialog).dialog('destroy').remove();
                                    var response = ajaxCall({
                                        func: "SYSTEM_EDIT_BLACKLIST",
                                        data: data_params
                                    });
                                    response.done(function(msg) {
                                        var msg = msg.split(":::");
                                        if (msg[0] == "FAILURE") infoMsgDialog("Nothing changed.");
                                        else {
                                            msgDialogCB("Saved blacklist!", (function() {
                                                $(this).dialog("close");
                                                $(this).dialog('destroy').remove();
                                                document.location.reload();
                                            }));
                                        }
                                        console.log(msg);
                                    });
                                }
                            }, {
                                text: "Cancel",
                                click: function() {
                                    $(this).dialog("close");
                                    $(this).dialog('destroy').remove()
                                }
                            }]
                        });
                    } else errMsgDialog("There was an error in the request.");
                });
                break;
            }
        case "SYSTEM_USER_PHONE_PRIV":
            {
                var editPhonePrivHTML = "<div>{{phone_html}}</div><div style='width:100%; height: 1px; background: #7A7A7A;'></div><br>" +
                    "<label for='outgoing_number_only'>Show calls to this Outgoing Number ONLY:</label>" +
                    "<input type='text' id='outgoing_number_only' /><br>";
                // TODO: better outgoing number filtering
                var response = JSONajaxCall({
                    func: "SYSTEM_USER_GET_PHONES",
                    data: params
                });
                response.done(function(data) {
                    var addonHTML = "";
                    $.each(data.companies, function(i, v) {
                        addonHTML += "<h3>" + v.name + "</h3>";
                        $.each(v.phones, function(i, v2) {
                            v2.number = v2[0];
                            if (v2.noaccess)
                                addonHTML += "<input class='company_number' style='display:inline-block !important;' type='checkbox' value='" + v2.number + "' />&nbsp;" + v2.number + "<br>";
                            else
                                addonHTML += "<input class='company_number' style='display:inline-block !important;' type='checkbox' checked value='" + v2.number + "' />&nbsp;" + v2.number + "<br>";
                        });
                        addonHTML += "<br>";
                    });
                    //console.log(addonHTML);
                    editPhonePrivHTML = editPhonePrivHTML.replace("{{phone_html}}", addonHTML);
                    var $dialog = $('<div></div>').html(editPhonePrivHTML).dialog({
                        modal: true,
                        autoOpen: true,
                        height: 520,
                        minHeight: 520,
                        width: 321,
                        minWidth: 321,
                        maxWidth: 321,
                        maxHeight: 520,
                        title: '<span class="ui-button-icon-primary ui-icon ui-icon-key" style="float:left; margin-right:5px;"></span>\
User Phone Number Access',
                        buttons: [{
                            text: "Save",
                            icons: {
                                primary: "ui-icon-key"
                            },
                            click: function() {
                                var outgoing_number_filter = $("#outgoing_number_only").val();
                                var disabled_phones = [];
                                $(".company_number").not(":checked").each(function(i, v) {
                                    disabled_phones.push(v.value);
                                });
                                var response = JSONajaxCall({
                                    func: "SYSTEM_USER_SAVE_PHONES",
                                    data: {
                                        id: params,
                                        disabled: disabled_phones,
                                        outgoing_number_filter: outgoing_number_filter
                                    }
                                });
                                response.done(function(data) {
                                    msgDialogCB("Saved phone number access for user.", (function() {
                                        $($dialog).dialog("close");
                                        $($dialog).dialog('destroy').remove();
                                    }));
                                });
                            }
                        }, {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                                $(this).dialog('destroy').remove()
                            }
                        }]
                    });
                    $("#outgoing_number_only").val(data.outgoing_number);
                });

                break;
            }
        case "SYSTEM_USER_PRIV":
            {
                var editUserAddonPrivHTML = '\
                <div><br>\
                <input type="radio" id="cr_0" name="call_restriction" style="display:inline-block !important;" value="0" /><label for="cr_0" style="display:inline-block !important; padding-left: 5px;">All Calls</label><br> \
                <input type="radio" id="cr_2" name="call_restriction" style="display:inline-block !important;" value="2" /><label style="display:inline-block !important; padding-left: 5px;" for="cr_2">Only allow access to calls before this date: </label>\
                <input type="radio" id="cr_1" name="call_restriction" style="display:inline-block !important;" value="1" /><label style="display:inline-block !important; padding-left: 5px;" for="cr_1">Only allow access to calls after this date: </label> \
                <input {{disabled}} type="text" id="restrict_call_date" value="{{access_from_date}}" /> \
                <br><input type="checkbox" style="display:inline-block !important;" id="disable_outgoing" name="disable_outgoing"{{outbound_links}}/><label for="disable_outgoing" style="display: inline !important; padding-left: 5px; font-weight: normal;">Remove Outbound Call Links</label><br>  \
                <input type="checkbox" style="display:inline-block !important;" id="enable_phone_code" name="enable_phone_code"{{enable_phone_code}}/><label for="enable_phone_code" style="display: inline !important; padding-left: 5px; font-weight: normal;">Allow user to set Phone Codes</label> \
                <br><br><label id="pa">Plugin Access:</label>\
                <div id="addon_list">{{addon_list}}</div><br></div>';

                var response = JSONajaxCall({
                    func: "SYSTEM_USER_PRIV",
                    data: params
                });
                response.done(function(data) {
                    var addonHTML = "";
                    $.each(data.products, function(i, v) {
                        var checked = "";
                        if (v.has_access)
                            checked = "checked";

                        addonHTML += '<div><input style="display:inline !important;" type="checkbox" ' + checked + ' value="' + v.id + '" id="' + i + '" /><label style="display: inline !important; padding-left: 5px; font-weight: normal;" for="' + i + '">' + v.name + '</label></div>';
                    });
                    if (data.access_from != false) {
                        editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{access_from_date}}", data.access_from);
                        editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{disabled}}", "");
                    } else {
                        editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{access_from_date}}", "");
                        editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{disabled}}", "disabled");
                    }
                    if (data.outbound_links == 1)
                        editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{outbound_links}}", " checked");
                    else
                        editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{outbound_links}}", "");
                    if (data.allow_phone_code == 1)
                        editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{enable_phone_code}}", "checked");
                    else
                        editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{enable_phone_code}}", "");

                    editUserAddonPrivHTML = editUserAddonPrivHTML.replace("{{addon_list}}", addonHTML);

                    var $dialog = $('<div></div>').html(editUserAddonPrivHTML).dialog({
                        modal: true,
                        autoOpen: true,
                        minHeight: 510,
                        maxHeight: 510,
                        maxWidth: 315,
                        minWidth: 315,
                        height: 'auto',
                        title: '<span class="ui-button-icon-primary ui-icon ui-icon-key" style="float:left; margin-right:5px;"></span>User Permissions',
                        buttons: [{
                            text: "Save",
                            icons: {
                                primary: "ui-icon-key"
                            },
                            click: function() {
                                var call_restrict_enabled = $("input[name='call_restriction']:checked").val();
                                if ((call_restrict_enabled == 1 || call_restrict_enabled == 2) && $("#restrict_call_date").val() == "") {
                                    errMsgDialog("Please enter a date!");
                                    return false;
                                }

                                if (call_restrict_enabled == 0)
                                    $("#restrict_call_date").val("");

                                var checked = [];
                                $("#addon_list").children("div").children("input:checked").each(function(i, v) {
                                    checked.push(v.value);
                                });
                                var not_checked = [];
                                $("#addon_list").children("div").children("input:not(:checked)").each(function(i, v) {
                                    not_checked.push(v.value);
                                });

                                var disable_outgoing = 0;
                                var enable_phone_code = 0;

                                if ($("#disable_outgoing").is(':checked'))
                                    disable_outgoing = 1;
                                if ($("#enable_phone_code").is(':checked'))
                                    enable_phone_code = 1;


                                var response = JSONajaxCall({
                                    func: "SYSTEM_UPDATE_USER_PRIV",
                                    data: {
                                        id: params,
                                        checked: checked,
                                        disable_outgoing: disable_outgoing,
                                        enable_phone_code: enable_phone_code,
                                        not_checked: not_checked,
                                        call_restriction_enabled: call_restrict_enabled,
                                        call_restriction_date: $("#restrict_call_date").val()
                                    }
                                });
                                response.done(function(data) {
                                    if (data.result == "success") {
                                        msgDialogCB("Saved permissions for this user.", (function() {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                        }));
                                    } else {
                                        $($dialog).dialog("close");
                                        $($dialog).dialog('destroy').remove();
                                        errMsgDialog("Error while saving permissions.");
                                    }
                                });
                            }
                        }, {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                                $(this).dialog('destroy').remove()
                            }
                        }]
                    });
                    var lcl = $("#lcl").val();
                    if (lcl < 2) {
                        $("#disable_outgoing").attr("style", "display:none !important");
                        $("label[for='disable_outgoing']").attr("style", "display:none !important");
                        $("#pa").attr("style", "display:none !important");
                        $("#addon_list").attr("style", "display:none !important");
                        $dialog.minHeight = 100;
                        $dialog.maxnHeight = 100;
                    }
                    $("input[id^='cr_']").click(function(e) {
                        var call_restrict_enabled = e.currentTarget.id;
                        if (call_restrict_enabled == 'cr_1' || call_restrict_enabled == 'cr_2') {
                            $("input#restrict_call_date").removeAttr("disabled");
                        } else {
                            $("input#restrict_call_date").attr("disabled", "disabled");
                        }
                    });
                    $("#restrict_call_date").datetimepicker({
                        timeFormat: "hh:mm tt"
                    });
                    $("#cr_" + data.access_type).trigger("click");
                });
                break;
            }
        case "SYSTEM_SET_FORWARDING_EMAIL":
            {
                var editUserEmailForwardingHTML = '<label>Edit Email(s):</label>' +
                    
                    '<div id="notification_emails">' +
                        '<div class="notification_email">' + 
                            '<div style="float: left; padding-bottom: 5px;"><input type="text" id="forward_email" style="padding: 2px; width: 170px;" /></div>' +
                            '<div style="float: left; padding-bottom: 5px; padding-left: 5px;"><a class="add action btnAddMore" href="javascript:void(0)" id="btnAddMore" onClick="addMoreEmails(this)"><span class="replace">Add</span></a></div>' +
                            '<div style="clear: both;"></div>' +
                        '</div>' +
                    '</div><br />' +

                    '<label>Send Notification On Calls:</label>' +
                    '<select id="notification_type">' +
                    '<option value="0">None</option> ' +
                    '<option value="1">All</option>' +
                    '<option value="2">Missed</option>' +
                    '<option value="3">Answered</option> ' +
                    '<option value="4">Over 30 Seconds</option> ' +
                    '<option value="5">Over 45 Seconds</option>' +
                    '<option value="6">Over 60 Seconds</option>' +
                    '<option value="7">Over 90 Seconds</option>' +
                    '<option value="8">Over 2 Minutes</option>' +
                    '<option value="9">Voicemails</option>' +
                    '</select> ';
                var $dialog = $('<div></div>').html(editUserEmailForwardingHTML).dialog({
                    modal: true,
                    autoOpen: true,
                    height: 'auto',
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-mail-closed" style="float:left;margin-right:5px"></span>' +
                        'Email &amp; Notification Settings ',
                    buttons: [{
                        text: "Save",
                        icons: {
                            primary: "ui-icon-pencil"
                        },
                        click: function() {
                            var emails = [];
                            $('div.ui-dialog:visible').find('input#forward_email').each(function() {
                                if ($(this).val().trim() != "") emails.push($(this).val());
                            });
                            var notification_type = $("div.ui-dialog:visible").find("select#notification_type").val();

                            var response = JSONajaxCall({
                                func: "SYSTEM_SAVE_USER_EMAIL_SETTINGS",
                                data: {
                                    user_id: params,
                                    emails: emails,
                                    notification_type: notification_type
                                }
                            });
                            response.done(function(data) {
                                msgDialog("Saved Notification Settings!");
                                var email = (typeof(emails[0]) == "undefined") ? "" : emails[0];

                                $("tr[data-key='" + params + "'] td:eq(2)").text(email);
                                $($dialog).dialog("close");
                                $($dialog).dialog("destroy").remove();
                            });
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove()
                        }
                    }]
                });
                var response = JSONajaxCall({
                    func: "SYSTEM_GET_USER_EMAIL_SETTINGS",
                    data: params
                });
                response.done(function(data) {
                    $('div.ui-dialog:visible').find('input#forward_email').val(data.email);

                    if (data.extra_notification_emails != "") {
                        var emails = data.extra_notification_emails.split(",");

                        for (var i in emails) {
                            addMoreEmails($('div.ui-dialog:visible').find("#btnAddMore"), emails[i]);
                        }
                    }

                    $("div.ui-dialog:visible").find("select#notification_type").find("option[value='" + data.notification + "']").attr("selected", "selected");
                });
                break;
            }
        case "SYSTEM_EDIT_USER_COMPANY":
            {
                var editUserCompanyHTML = '<br><div id="company_list" style="max-height: 500px;">{{company_details}}</div>';

                //Ajax, Get companies.
                var response = ajaxCall({
                    func: "SYSTEM_GET_COMPANY_LIST",
                    data: params
                });
                response.done(function(msg) {
                    var msg = msg.split(":::");
                    //console.log(msg);
                    if (msg[0] == "SUCCESS") {
                        var addonHTML = "";
                        var companies = msg[1].split("::");
                        companies.pop();
                        $.each(companies, (function(index, value) {
                            var checked = "";
                            var isIn = 'id="not"';
                            var company_detail = value.split(":");
                            if (company_detail[2] == 1) {
                                checked = "checked";
                                isIn = 'id="is"';
                            }
                            addonHTML += '<div><input style="display:inline !important;" type="checkbox" ' + isIn + ' ' + checked + ' value="' + company_detail[0] + '" />&nbsp;' + company_detail[1] + '</div><br>';
                        }));
                        editUserCompanyHTML = editUserCompanyHTML.replace("{{company_details}}", addonHTML);
                        var $dialog = $('<div></div>').html(editUserCompanyHTML).dialog({
                            modal: true,
                            autoOpen: true,
                            minHeight: 177,
                            maxHeight: 300,
                            height: 'auto',
                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-suitcase" style="float:left; margin-right:5px;"></span>\
Edit Users\' Companies',
                            buttons: [{
                                text: "Save",
                                icons: {
                                    primary: "ui-icon-pencil"
                                },
                                click: function() {
                                    var _companies_in = new Array();
                                    var _companies_out = new Array();

                                    $("input[id$=not]").each(function(index, element) {
                                        if ($(element).is(':checked')) {
                                            console.log("out: " + $(element).val());
                                            _companies_in.push($(element).val());
                                        }
                                    });
                                    $("input[id$=is]").each(function(index, element) {
                                        if (!$(element).is(':checked')) {
                                            console.log("in: " + $(element).val());
                                            _companies_out.push($(element).val());
                                        }
                                    });

                                    var data_params = {
                                        companies_out: _companies_out,
                                        companies_in: _companies_in,
                                        id: params
                                    };
                                    console.log(data_params);
                                    $($dialog).dialog("close");
                                    $($dialog).dialog('destroy').remove();
                                    var response = ajaxCall({
                                        func: "SYSTEM_EDIT_USER_COMPANY",
                                        data: data_params
                                    });
                                    response.done(function(msg) {
                                        var msg = msg.split(":::");
                                        if (msg[0] == "FAILURE") infoMsgDialog("Nothing changed.");
                                        else {
                                            msgDialogCB("Saved this user's company.", (function() {
                                                $(this).dialog("close");
                                                $(this).dialog('destroy').remove();
                                                document.location.reload();
                                            }));
                                        }
                                        console.log(msg);
                                    });
                                }
                            }, {
                                text: "Cancel",
                                click: function() {
                                    $(this).dialog("close");
                                    $(this).dialog('destroy').remove()
                                }
                            }]
                        });
                    } else errMsgDialog("There was an error in the request.");
                });

                break;
            }
        case "SYSTEM_DELETE_COMPANY":
            {
                promptMsg("Are you sure you want to delete this company?", (function() {
                    var response = ajaxCall({
                        func: "SYSTEM_DELETE_COMPANY",
                        data: params
                    });
                    response.done(function(msg) {
                        var msg = msg.split(":::");
                        console.log(msg);
                        if (msg[0] == "SUCCESS") {
                            msgDialogCB("Company Deleted.", (function() {
                                document.location.reload();
                            }));

                        } else errMsgDialog("There was an error in the request.");
                    });
                }));
                break;
            }
        case "SYSTEM_DELETE_PC_TEMPLATE":
            {
                promptMsg("Are you sure you want to delete this template?", function() {
                    var response = JSONajaxCall({
                        func: "SYSTEM_DELETE_PC_TEMPLATE",
                        data: params
                    });
                    response.done(function(data) {
                        if (data.result == "success") {
                            msgDialogCB("Template Deleted.", (function() {
                                document.location.reload();
                            }));
                        } else {
                            errMsgDialog("There was an error in the request.");
                        }
                    });
                });
                break;
            }
        case "SYSTEM_DELETE_BLACKLIST":
            {
                promptMsg("Are you sure you want to delete this blacklist?", (function() {
                    var response = ajaxCall({
                        func: "SYSTEM_DELETE_BLACKLIST",
                        data: params
                    });
                    response.done(function(msg) {
                        var msg = msg.split(":::");
                        console.log(msg);
                        if (msg[0] == "SUCCESS") {
                            msgDialogCB("Blacklist Deleted.", (function() {
                                document.location.reload();
                            }));

                        } else errMsgDialog("There was an error in the request.");
                    });
                }));
                break;
            }
        case "SYSTEM_DELETE_USER":
            {
                promptMsg("Are you sure you want to delete this user?", (function() {
                    var response = ajaxCall({
                        func: "SYSTEM_DELETE_USER",
                        data: params
                    });
                    response.done(function(msg) {
                        var msg = msg.split(":::");
                        console.log(msg);
                        if (msg[0] == "SUCCESS") {
                            msgDialogCB("User Deleted.", (function() {
                                $("tr[data-key='" + params + "']").remove();
                            }));

                        } else errMsgDialog("There was an error in the request.");
                    });
                }));
                break;
            }
        case "SYSTEM_PROMOTE_USER":
            {
                promptMsg("Are you sure you want to promote this user to an Administrator?", (function() {
                    var response = ajaxCall({
                        func: "SYSTEM_PROMOTE_USER",
                        data: params
                    });
                    response.done(function(msg) {
                        var msg = msg.split(":::");
                        console.log(msg);
                        if (msg[0] == "SUCCESS") {
                            msgDialogCB("The user was promoted to admin.", (function() {
                                document.location.reload();
                            }));

                        } else errMsgDialog("There was an error in the request.");
                    });
                }));

                break;
            }
        case "SYSTEM_RESET_USER":
            {
                promptMsg("Are you sure you want to reset this user's password?", (function() {
                    var response = JSONajaxCall({
                        func: "SYSTEM_RESET_USER",
                        data: params
                    });
                    response.done(function(data) {
                        infoMsgDialog("Done!");
                    })
                }));
                break;
            }
        case "SYSTEM_GEN_COMPANY_REPORT":
            {
                if ($("#start_date").datepicker("getDate") == null || $("#end_date").datepicker("getDate") == null) {
                    errMsgDialog("Please select a date range.");
                    return false;
                }
                if ($("#company_select").val() == "none") {
                    errMsgDialog("Please select a company.");
                    return false;
                }

                $("#filter-box").block({
                    message: '<h1 style="color:#fff">Loading</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .7,
                        color: '#fff !important'
                    }
                });

                var ajaxparams = {
                    start_date: $("#start_date").datepicker("getDate").getTime(),
                    end_date: $("#end_date").datepicker("getDate").getTime(),
                    company_id: $("#company_select").val()
                };

                var response = JSONajaxCall({
                    func: "SYSTEM_GET_COMPANY_REPORT",
                    data: ajaxparams
                });

                response.done(function(msg) {
                    var data = msg[0];
                    $.each(data, function(i, v) {
                        if (v.price == 0) {
                            v.price = '0.00';
                        }

                        $('#dataTable > tbody:last').append($("<tr>").append("<td>" + i + "</td><td><span style='display: none;'>"+ v.seconds+"</span>" + secondstotime(v.seconds) + "</td><td>$" + v.price + "</td>"));
                    });
                    $('#dataTable').append("<tfoot>");
                    $('#dataTable > tfoot:last').append($("<tr>").append("<th></th><th></th><th></th>"));

                    if (msg[1] == 0) {
                        msg[1] = '0.00';
                    }

                    $('#dataTable > tfoot:last').append($("<tr>").append("<th>Total</th><td><span style='display:none;'>"+msg[2]+"</span>" + secondstotime(msg[2]) + "</td><td>$" + msg[1] + "</td>"));

                    $("#filter-box").unblock();
                    $("#filter-box").fadeOut(300, function(e) {
                        $("#results-box").fadeIn(300);
                        $("#dataTable").trigger("update");
                        $("#dataTable").tablesorter({
                            widgets: ['zebra'],
                            textExtraction: {
                                1:function(node){
                                    return $(node).children[0].innerHTML;
                                },
                                2:function(node){
                                    return $(node).val().replace('$','').replace(/,/g,'');
                                }
                            }
                        });
                    });
                });

                break;
            }
        case "SYSTEM_RESET_COMPANY_REPORT":
            {
                $("#dataTable > tbody").html("");
                $("#dataTable > tfoot").remove();
                $("#results-box").fadeOut(300, function(e) {
                    $("#filter-box").fadeIn(300);
                });
                break;
            }
        case "SYSTEM_DEMOTE_USER":
            {
                promptMsg("Are you sure you want to demote this user?", (function() {
                    var response = ajaxCall({
                        func: "SYSTEM_DEMOTE_USER",
                        data: params
                    });
                    response.done(function(msg) {
                        var msg = msg.split(":::");
                        console.log(msg);
                        if (msg[0] == "SUCCESS") {
                            msgDialogCB("The user was demoted.", (function() {
                                document.location.reload();
                            }));

                        } else errMsgDialog("There was an error in the request.");
                    });
                }));

                break;
            }
        case "SYSTEM_EDIT_CALLERID":
            {
                var editCallerIDHTML = '<p class="validateTips">You can only set one number as the Caller ID. Include the "+" and country code. Example:  +14446662222<br>Be sure to validate number with Twilio first!<br>Leave blank to disable.</p>\
<form>\
<fieldset>\
<label for="number">Number</label>\
<input type="text" name="number" id="number" class="text ui-widget-content ui-corner-all" value="{{number}}" />\
</fieldset>\
</form>';
                var response = ajaxCall({
                    func: "SYSTEM_GET_CALLERID",
                    data: params
                });
                response.done(function(msg) {
                    var msg = msg.split(":::");
                    //console.log(msg);
                    if (msg[0] == "SUCCESS") {
                        editCallerIDHTML = editCallerIDHTML.replace("{{number}}", msg[1]);
                        var $dialog = $('<div></div>').html(editCallerIDHTML).dialog({
                            modal: true,
                            autoOpen: true,
                            minHeight: 177,
                            maxWidth: 587,
                            height: 'auto',
                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-contact" style="float:left; margin-right:5px;"></span>Edit Caller ID',
                            buttons: [{
                                text: "Save",
                                icons: {
                                    primary: "ui-icon-pencil"
                                },
                                click: function() {
                                    var data_params = {
                                        number: $("#number").val(),
                                        id: params
                                    };
                                    //console.log(data_params);
                                    $($dialog).dialog("close");
                                    $($dialog).dialog('destroy').remove();
                                    var response = ajaxCall({
                                        func: "SYSTEM_EDIT_CALLERID",
                                        data: data_params
                                    });
                                    response.done(function(msg) {
                                        var msg = msg.split(":::");
                                        msgDialogCB("Saved Caller ID!", (function() {
                                            $(this).dialog("close");
                                            $(this).dialog('destroy').remove();
                                            document.location.reload();
                                        }));
                                        console.log(msg);
                                    });
                                }
                            }, {
                                text: "Cancel",
                                click: function() {
                                    $(this).dialog("close");
                                    $(this).dialog('destroy').remove()
                                }
                            }]
                        });
                    } else errMsgDialog("There was an error in the request.");
                });
                break;
            }
        case "SYSTEM_EDIT_NUMBER_CAMPAIGN":
            {
                var editCampaignHTML = '<!--<p class="validateTips"></p>-->\
<form id="c_frm">\
<fieldset style="margin-top:5px !important;">\
<input type="radio" name="campaign_sel" class="ui-widget-content ui-corner-all" style="display:inline-block !important;" value="Google Organic" />&nbsp;<span>Google Organic</span><br />\
<input type="radio" name="campaign_sel" class="ui-widget-content ui-corner-all" style="display:inline-block !important;" value="Google Adwords" />&nbsp;<span>Google Adwords</span><br />\
<input type="radio" name="campaign_sel" class="ui-widget-content ui-corner-all" style="display:inline-block !important;" value="Youtube" />&nbsp;<span>Youtube</span><br />\
<input type="radio" name="campaign_sel" class="ui-widget-content ui-corner-all" style="display:inline-block !important;" value="Facebook" />&nbsp;<span>Facebook</span><br />\
<input type="radio" name="campaign_sel" class="ui-widget-content ui-corner-all" style="display:inline-block !important;" value="Yahoo" />&nbsp;<span>Yahoo!</span><br />\
<input type="radio" name="campaign_sel" class="ui-widget-content ui-corner-all" style="display:inline-block !important;" value="Bing" />&nbsp;<span>Bing</span><br />\
<input type="radio" name="campaign_sel" class="ui-widget-content ui-corner-all" style="display:inline-block !important;" value="Default" />&nbsp;<span>Default</span><br />\
<input type="radio" name="campaign_sel" class="ui-widget-content ui-corner-all" style="display:inline-block !important;" value="custom" />&nbsp;<span>Custom</span>&nbsp&nbsp<input type="text" value="{{custom_value}}" disabled style="display:inline-block !important;" /><br />\
</fieldset>\
</form>';
                var this_campaign = $("#" + params + " [name='friendly_name']").val();

                console.log("Campaign: " + this_campaign);
                var $dialog = $('<div></div>').html(editCampaignHTML).dialog({
                    modal: true,
                    autoOpen: true,
                    minHeight: 177,
                    maxWidth: 587,
                    height: 'auto',
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Edit Campaign',
                    buttons: [{
                        text: "Save",
                        icons: {
                            primary: "ui-icon-pencil"
                        },
                        click: function() {
                            var selected_campaign = $("#c_frm :checked").attr("value");
                            if (selected_campaign == "custom") {
                                selected_campaign = $("#c_frm [type='text']").attr("value");
                            }
                            if (selected_campaign == "Default") {
                                selected_campaign = $("#" + params + " [name='number']").val();
                            }
                            var ajaxparams = {
                                number_sid: params,
                                campaign: selected_campaign
                            };

                            var response = JSONajaxCall({
                                func: "SYSTEM_SET_CAMPAIGN",
                                data: ajaxparams
                            });

                            response.done(function(msg) {
                                if (msg.result == "error")
                                    errMsgDialog("An error has occured.");
                                else {
                                    $("#" + params + " [name='friendly_name']").val(selected_campaign);
                                    $("#" + params + " [name='friendly_name_static']").val(selected_campaign);
                                    $($dialog).dialog("close");
                                    $($dialog).dialog('destroy').remove()
                                }
                            });
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove()
                        }
                    }]
                });
                $("[name='campaign_sel']").click(function(e) {
                    if (e.currentTarget.defaultValue == "custom") {
                        $("#c_frm [type='text']").removeAttr("disabled");
                    } else {
                        $("#c_frm [type='text']").attr("disabled", true);
                    }
                });
                var custom = true;
                $("input[name='campaign_sel']").each(function(i, v) {
                    if ($(v).val() === this_campaign) {
                        custom = false;
                    }
                    console.log("Radio Select: i:" + i + " v:" + v.value);
                });

                if (custom) {
                    $("#c_frm [value='custom']").attr('checked', 'checked');
                    $("#c_frm [type='text']").removeAttr("disabled");
                    $("#c_frm [type='text']").val($("#" + params + " [name='friendly_name']").val());
                } else {
                    $("#c_frm [value='" + this_campaign + "']").attr('checked', 'checked');
                    $("#c_frm [type='text']").val("");
                }
                break;
            }
        case "SYSTEM_VOICE_MAIL":
            {
                var response = JSONajaxCall({
                    func: "SYSTEM_VOICE_MAIL",
                    data: params,
                    type: type
                });
                break;
            }
        case "SYSTEM_EDIT_RESET_CALL_TIME":
            {
                var editCampaignHTML = '<!--<p class="validateTips"></p>-->\
<form id="c_frm">\
<fieldset style="margin-top:5px !important;">\
<label for="update_reset_value">Enter Reset Call Time: </label>\
<input type="text" class="ui-widget-content ui-corner-all" style="margin-top: 7px; display: inline-block !important; padding: 5px;" value="{{custom_value}}" id="update_reset_value" /> hour(s)<br />\
</fieldset>\
</form>';
                var this_campaign = $("[name='reset_call_time_" + params + "']").val();
                editCampaignHTML = editCampaignHTML.replace("{{custom_value}}", this_campaign);
                var $dialog = $('<div></div>').html(editCampaignHTML).dialog({
                    modal: true,
                    autoOpen: true,
                    minHeight: 177,
                    maxWidth: 587,
                    height: 'auto',
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Edit Reset Call Time',
                    buttons: [{
                        text: "Save",
                        icons: {
                            primary: "ui-icon-pencil"
                        },
                        click: function() {
                            var reset_time_value = $("#c_frm [type='text']").attr("value");
                            if (reset_time_value == '') {
                                errMsgDialog("Field Can not be empty");
                                return false;
                            }
                            if (!$.isNumeric(reset_time_value)) {
                                errMsgDialog("Please enter numeric value");
                                return false;
                            }
                            if (reset_time_value < 1) {
                                errMsgDialog("Value can not be less than 1");
                                return false;
                            }

                            var ajaxparams = {
                                company_id: params,
                                update_value: reset_time_value
                            };

                            var response = JSONajaxCall({
                                func: "SYSTEM_SET_RESET_CALL_TIME",
                                data: ajaxparams
                            });

                            response.done(function(msg) {
                                if (msg == "error")
                                    errMsgDialog("An error has occured.");
                                else {
                                    $("[name='reset_call_time_" + params + "']").val(reset_time_value);
                                    //$("#"+params+" [name='friendly_name_static']").val(selected_campaign);
                                    $($dialog).dialog("close");
                                    $($dialog).dialog('destroy').remove()

                                }
                            });
                        }
                    }, {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove()
                        }
                    }]
                });
                break;
            }
        case "SYSTEM_EDIT_PHONE_CODE":
            {
                var selectPCHTML = '<button style="width: 90%; margin-left: 15px; margin-top: 5px;" id="SYSTEM_EDIT_PHONE_CODE_IN">Incoming Phone Codes</button><br/><br/>' +
                    '<button style="width:90%; margin-left: 15px;" id="SYSTEM_EDIT_PHONE_CODE_OUT">Outgoing Phone Codes</button> ';
                var $dialog = $('<div></div>').html(selectPCHTML).dialog({
                    modal: true,
                    autoOpen: true,
                    title: 'Select',
                    buttons: [{
                        text: "Close",
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog("destroy").remove();
                        }
                    }]
                });
                $("#SYSTEM_EDIT_PHONE_CODE_IN").click(function() {
                    phoneCodeWindow(params, 1)
                });
                $("#SYSTEM_EDIT_PHONE_CODE_IN").button();
                $("#SYSTEM_EDIT_PHONE_CODE_OUT").click(function() {
                    phoneCodeWindow(params, 2)
                });
                $("#SYSTEM_EDIT_PHONE_CODE_OUT").button();

                function phoneCodeWindow(companyid, type) {
                    var params = companyid;
                    var editPhoneCodeHTML = '<!--<p class="validateTips"></p>-->\
<form id="c_frm">\
<fieldset style="margin-top:5px !important;">\
<label for="phone_code_name">Phone Code:</label>\
<input type="text" id="phone_code_name" class="ui-widget-content ui-corner-all" disabled="true" style="display:inline-block !important;height: 20px;width: 100%; margin-bottom:5px;" />\
<label for="dial_code">Order:</label>\
<input type="text" id="dial_code" class="ui-widget-content ui-corner-all" disabled="true" style="display:inline-block !important;height: 20px;width: 50%;" />\
<button style="width:100px; margin-left:35px;" id="add_phone_code">Add</button>\
<br /><br />\
<div style="width:98%; margin:0 auto; background:#AAA; height:1px;"></div>\
</fieldset>\
</form>\
<table style="width: 100%;" id="phone_code_t">\
<tbody><tr><th align="left">Phone Code</th><th align="left">Order</th><th align="left">Delete</th></tr>\
{{phone_code_rows}}\
</tbody></table>\
<span style="position: absolute; bottom:0px; color: #AAA;width: 245px;margin-left: 15px;font: 10px Helvetica;">* Will be applied when you click "Save" *</span>';

                    var deleted = new Array();

                    if (type == 1)
                        var type_string = "Incoming";
                    else
                        var type_string = "Outgoing";

                    var response = JSONajaxCall({
                        func: "SYSTEM_GET_PHONE_CODES",
                        data: params,
                        type: type
                    });
                    response.done(function(msg) {
                        if (msg.result == "success") {
                            editPhoneCodeHTML = editPhoneCodeHTML.replace("{{phone_code_rows}}", msg.table_data);
                            var $dialog = $('<div></div>').html(editPhoneCodeHTML).dialog({
                                modal: true,
                                autoOpen: true,
                                minHeight: 489,
                                minWidth: 380,
                                maxWidth: 587,
                                height: 'auto',
                                title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Edit ' + type_string + ' Phone Codes',
                                buttons: [{
                                    style: 'margin-right: 70px !important',
                                    text: "Load Template",
                                    click: function() {
                                        var loadFromTemplateHTML = '<p><span style="font-weight:bold;color: rgb(255, 68, 68);">Warning</span>: This <b>WILL</b> replace any phone codes already assigned to this company</p>' +
                                            '<br><label for="pc_templates" style="display:inline-block !important;">Phone Code Template: </label>&nbsp;' +
                                            '<select id="pc_templates" style="display:inline-block !important;"></select> ';
                                        var response2 = JSONajaxCall({
                                            func: "SYSTEM_GET_PC_TEMPLATES",
                                            data: type
                                        });
                                        response2.done(function(data) {
                                            var $dialog2 = $('<div></div>').html(loadFromTemplateHTML).dialog({
                                                modal: true,
                                                autoOpen: true,
                                                minHeight: 200,
                                                maxHeight: 200,
                                                minWidth: 200,
                                                maxWidth: 200,
                                                height: 'auto',
                                                title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Load From Template',
                                                buttons: [{
                                                    text: "Insert",
                                                    click: function() {
                                                        promptMsg("Are you sure? This will replace current " + type_string + " Phone Codes.", function() {
                                                            var response3 = JSONajaxCall({
                                                                func: "SYSTEM_REPLACE_PC",
                                                                data: {
                                                                    templateid: $("#pc_templates").val(),
                                                                    company_id: params,
                                                                    type: type
                                                                }
                                                            });
                                                            response3.done(function(data) {
                                                                if (data.result == "success") {
                                                                    msgDialogCB("Saved " + type_string + " Phone Codes!", function() {
                                                                        document.location.reload();
                                                                    });
                                                                } else if (data.result == 'no_pc_in_template') {
                                                                    errMsgDialog("There are no " + type_string + " Phone Codes in this Template. Aborting.");
                                                                } else {
                                                                    errMsgDialog("An error has occurred while saving.")
                                                                }
                                                            });
                                                        });
                                                    }
                                                }, {
                                                    text: "Cancel",
                                                    click: function() {
                                                        $($dialog2).dialog("close");
                                                        $($dialog2).dialog("destroy").remove();
                                                    }
                                                }]
                                            });
                                            if (data.templates != null) {
                                                $(data.templates).each(function(i, v) {
                                                    $("#pc_templates").append("<option value='" + v.id + "'>" + v.name + "</option>")
                                                });
                                            }
                                        });
                                    }
                                }, {
                                    text: "Save",
                                    icons: {
                                        primary: "ui-icon-pencil"
                                    },
                                    click: function() {
                                        var to_add = new Array();
                                        $(".new_pc").each(function(i, v) {
                                            var new_d = {
                                                phone_code_name: $($(v).children(0)[0]).text(),
                                                dial_code: $($(v).children(0)[1]).text()
                                            };
                                            to_add.push(new_d);
                                        });

                                        if (to_add.length == 0 && deleted.length == 0) {
                                            msgDialogCB("Nothing changed.", function() {
                                                $($dialog).dialog("close");
                                                $($dialog).dialog('destroy').remove();
                                            });
                                        } else {
                                            var ajaxparams = {
                                                type: type,
                                                company_id: params,
                                                added: to_add,
                                                deleted: deleted
                                            };

                                            var response = JSONajaxCall({
                                                func: "SYSTEM_SAVE_PHONE_CODE",
                                                data: ajaxparams
                                            });

                                            response.done(function(msg) {
                                                if (msg.result == "success") {
                                                    msgDialogCB("Saved", function() {
                                                        $($dialog).dialog("close");
                                                        $($dialog).dialog('destroy').remove();
                                                    });
                                                }
                                            });
                                        }
                                    }
                                }, {
                                    text: "Cancel",
                                    click: function() {
                                        $(this).dialog("close");
                                        $(this).dialog('destroy').remove();
                                    }
                                }]
                            });

                            $("#add_phone_code").button();
                            $("*[id*=SUB_DELETE_PHONE_CODE]").click(function(e) {
                                e.preventDefault();
                                $(deleted).each(function(i, v) {
                                    if (v == $(this).attr("data-params"))
                                        return false;
                                });
                                deleted.push($(this).attr("data-params"));
                                $(this).parent().parent().remove();
                                return false;
                            });
                            $("#add_phone_code").click(function(e) {
                                e.preventDefault();

                                var phone_code_name = $("#phone_code_name").val();
                                var dial_code = $("#dial_code").val();

                                if (!IsNumeric(dial_code) && dial_code != "") {
                                    errMsgDialog("Order must be numeric.");
                                    return false;
                                }

                                if (phone_code_name == "" || dial_code == "") {
                                    errMsgDialog("Can not add empty values!");
                                } else {
                                    $("#phone_code_t > tbody:last").append("<tr class='new_pc'><td>" + phone_code_name + "</td><td>" + dial_code + "</td><td><a href='#' onclick='$(this).parent().parent().remove()' =><img src=\"images/delete.gif\"></td></tr>");
                                    $("#phone_code_name").val("");
                                    $("#dial_code").val("");
                                }
                                return false;
                            });

                            $("#phone_code_name").removeAttr("disabled");
                            $("#dial_code").removeAttr("disabled");

                        } else errMsgDialog("There was an error in the request.");
                    });
                }
                break;
            }
        case "SYSTEM_EDIT_PHONE_CODE_OUT":
            {


                break;
            }
        case "SYSTEM_EDIT_PHONE_CODE_INC":
            {
                var editPhoneCodeHTML = '<!--<p class="validateTips"></p>-->\
<form id="c_frm">\
<fieldset style="margin-top:5px !important;">\
<label for="phone_code_name">Phone Code:</label>\
<input type="text" id="phone_code_name" class="ui-widget-content ui-corner-all" disabled="true" style="display:inline-block !important;height: 20px;width: 100%; margin-bottom:5px;" />\
<label for="dial_code">Order:</label>\
<input type="text" id="dial_code" class="ui-widget-content ui-corner-all" disabled="true" style="display:inline-block !important;height: 20px;width: 50%;" />\
<button style="width:100px; margin-left:35px;" id="add_phone_code">Add</button>\
<br /><br />\
<div style="width:98%; margin:0 auto; background:#AAA; height:1px;"></div>\
</fieldset>\
</form>\
<table style="width: 100%;" id="phone_code_t">\
<tbody><tr><th align="left">Phone Code</th><th align="left">Order</th><th align="left">Delete</th></tr>\
{{phone_code_rows}}\
</tbody></table>\
<span style="position: absolute; bottom:0px; color: #AAA;width: 245px;margin-left: 15px;font: 10px Helvetica;">* Will be applied when you click "Save" *</span>';

                var deleted = new Array();

                var response = JSONajaxCall({
                    func: "SYSTEM_GET_PHONE_CODES",
                    data: params
                });
                response.done(function(msg) {
                    if (msg.result == "success") {
                        editPhoneCodeHTML = editPhoneCodeHTML.replace("{{phone_code_rows}}", msg.table_data);
                        var $dialog = $('<div></div>').html(editPhoneCodeHTML).dialog({
                            modal: true,
                            autoOpen: true,
                            minHeight: 489,
                            minWidth: 380,
                            maxWidth: 587,
                            height: 'auto',
                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Edit Phone Codes',
                            buttons: [{
                                style: 'margin-right: 70px !important',
                                text: "Load Template",
                                click: function() {
                                    var loadFromTemplateHTML = '<p><span style="font-weight:bold;color: rgb(255, 68, 68);">Warning</span>: This <b>WILL</b> replace any phone codes already assigned to this company</p>' +
                                        '<br><label for="pc_templates" style="display:inline-block !important;">Phone Code Template: </label>&nbsp;' +
                                        '<select id="pc_templates" style="display:inline-block !important;"></select> ';
                                    var response2 = JSONajaxCall({
                                        func: "SYSTEM_GET_PC_TEMPLATES",
                                        data: 1
                                    });
                                    response2.done(function(data) {
                                        var $dialog2 = $('<div></div>').html(loadFromTemplateHTML).dialog({
                                            modal: true,
                                            autoOpen: true,
                                            minHeight: 200,
                                            maxHeight: 200,
                                            minWidth: 200,
                                            maxWidth: 200,
                                            height: 'auto',
                                            title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Load From Template',
                                            buttons: [{
                                                text: "Insert",
                                                click: function() {
                                                    promptMsg("Are you sure? This will replace current phone codes.", function() {
                                                        var response3 = JSONajaxCall({
                                                            func: "SYSTEM_REPLACE_PC",
                                                            data: {
                                                                templateid: $("#pc_templates").val(),
                                                                company_id: params
                                                            }
                                                        });
                                                        response3.done(function(data) {
                                                            if (data.result == "success") {
                                                                msgDialogCB("Saved Phone Codes!", function() {
                                                                    document.location.reload();
                                                                });
                                                            } else if (data.result == 'no_pc_in_template') {
                                                                errMsgDialog("There are no Phone Codes in this Template. Aborting.");
                                                            } else {
                                                                errMsgDialog("An error has occurred while saving.")
                                                            }
                                                        });
                                                    });
                                                }
                                            }, {
                                                text: "Cancel",
                                                click: function() {
                                                    $($dialog2).dialog("close");
                                                    $($dialog2).dialog("destroy").remove();
                                                }
                                            }]
                                        });
                                        if (data.templates != null) {
                                            $(data.templates).each(function(i, v) {
                                                $("#pc_templates").append("<option value='" + v.id + "'>" + v.name + "</option>")
                                            });
                                        }
                                    });
                                }
                            }, {
                                text: "Save",
                                icons: {
                                    primary: "ui-icon-pencil"
                                },
                                click: function() {
                                    var to_add = new Array();
                                    $(".new_pc").each(function(i, v) {
                                        var new_d = {
                                            phone_code_name: $($(v).children(0)[0]).text(),
                                            dial_code: $($(v).children(0)[1]).text()
                                        };
                                        to_add.push(new_d);
                                    });

                                    if (to_add.length == 0 && deleted.length == 0) {
                                        msgDialogCB("Nothing changed.", function() {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                        });
                                    } else {
                                        var ajaxparams = {
                                            company_id: params,
                                            added: to_add,
                                            deleted: deleted
                                        };

                                        var response = JSONajaxCall({
                                            func: "SYSTEM_SAVE_PHONE_CODE",
                                            data: ajaxparams
                                        });

                                        response.done(function(msg) {
                                            if (msg.result == "success") {
                                                msgDialogCB("Saved", function() {
                                                    $($dialog).dialog("close");
                                                    $($dialog).dialog('destroy').remove();
                                                });
                                            }
                                        });
                                    }
                                }
                            }, {
                                text: "Cancel",
                                click: function() {
                                    $(this).dialog("close");
                                    $(this).dialog('destroy').remove();
                                }
                            }]
                        });

                        $("#add_phone_code").button();
                        $("*[id*=SUB_DELETE_PHONE_CODE]").click(function(e) {
                            e.preventDefault();
                            $(deleted).each(function(i, v) {
                                if (v == $(this).attr("data-params"))
                                    return false;
                            });
                            deleted.push($(this).attr("data-params"));
                            $(this).parent().parent().remove();
                            return false;
                        });
                        $("#add_phone_code").click(function(e) {
                            e.preventDefault();

                            var phone_code_name = $("#phone_code_name").val();
                            var dial_code = $("#dial_code").val();

                            if (!IsNumeric(dial_code) && dial_code != "") {
                                errMsgDialog("Order must be numeric.");
                                return false;
                            }

                            if (phone_code_name == "" || dial_code == "") {
                                errMsgDialog("Can not add empty values!");
                            } else {
                                $("#phone_code_t > tbody:last").append("<tr class='new_pc'><td>" + phone_code_name + "</td><td>" + dial_code + "</td><td><a href='#' onclick='$(this).parent().parent().remove()' =><img src=\"images/delete.gif\"></td></tr>");
                                $("#phone_code_name").val("");
                                $("#dial_code").val("");
                            }
                            return false;
                        });

                        $("#phone_code_name").removeAttr("disabled");
                        $("#dial_code").removeAttr("disabled");

                    } else errMsgDialog("There was an error in the request.");
                });

                break;
            }
        case "SYSTEM_SAVE_SMTP_INFO":
            {
                var smtp_name = $("#smtp_name").val();
                var smtp_email = $("#smtp_email").val();
                var smtp_host = $("#smtp_host").val();
                var smtp_port = $("#smtp_port").val();
                var smtp_username = $("#smtp_username").val();
                var smtp_password = $("#smtp_password").val();
                var smtp_ssl_tls = $("#smtp_ssl_tls").is(":checked") ? 1 : 0;

                $("#SYSTEM_SAVE_SMTP_INFO").val('Saving...').prop('disabled', true);
                var response = JSONajaxCall({
                    func: "SYSTEM_SAVE_SMTP_INFO",
                    data: {
                        smtp_name: smtp_name,
                        smtp_email: smtp_email,
                        smtp_host: smtp_host,
                        smtp_port: smtp_port,
                        smtp_username: smtp_username,
                        smtp_password: smtp_password,
                        smtp_ssl_tls: smtp_ssl_tls
                    }
                });
                response.done(function(data) {
                    if (data.result == 'success')
                        infoMsgDialog("Saved account info!");
                    else
                        errMsgDialog("SMTP settings not valid, could not save.");

                    $("#SYSTEM_SAVE_SMTP_INFO").val('Save Settings').prop('disabled', false);
                });
                break;
            }
        default:
            errMsgDialog("Unknown Function.");
            break;
    }

    //getAjax($(this).attr('id'),params);
    return false;
}

function generateKWCode(company_id) {
    var g_country = 'default';
    var dialog_html = '<p>\
        <input type="radio" checked="" name="tracktype" value="all" id="tracktype1"> <label for="tracktype1"> Track all visitors</label><br/>\
        <input type="radio" name="tracktype" value="select" id="tracktype2"> <label for="tracktype2"> Track visitors from selected sources:</label>\
        <table id="tracktype" style="margin-left: 30px; width: 460px;">\
        <tr id="google" style="width: 105px;"><td>Google</td><td><input type="checkbox" name="paid" id="g_paid"/> <label for="g_paid"> Paid</label></td><td><input type="checkbox" name="organic" id="g_organic"/> <label for="g_organic"> Organic</label></td><td><input type="checkbox" name="local" id="g_local"/> <label for="g_local"> Local</label></td></tr>\
        <tr id="yahoo"><td>Yahoo</td><td><input type="checkbox" name="paid" id="y_paid"/> <label for="y_paid"> Paid</label></td><td><input type="checkbox" name="organic" id="y_organic"/> <label for="y_organic"> Organic</label></td><td><input type="checkbox" name="local" id="y_local"/> <label for="y_local"> Local</label></td></tr>\
        <tr id="bing"><td>Bing</td><td><input type="checkbox" name="paid" id="b_paid"/> <label for="b_paid"> Paid</label></td><td><input type="checkbox" name="organic" id="b_organic"/> <label for="b_organic"> Organic</label></td><td><input type="checkbox" name="local" id="b_local"/> <label for="b_local"> Local</label></td></tr>\
        </table>\
        <br><br>\
        <input type="checkbox" id="specific_number" /> <label for="specific_number"> Replace Number</label>&nbsp;&nbsp; <input id="the_specific_num" style="width: 160px !important;" type="text" disabled /><br/><br/> \
        <input type="checkbox" id="numbers_clickable" /> <label for="numbers_clickable"> Make numbers clickable on mobile devices</label>&nbsp;&nbsp;<br/> \
        <br/>\
        <label for="country">Local Number Formatting: &nbsp;&nbsp;&nbsp;</label> \
        '+$("#country")[0].outerHTML.replace("display:none;","")+'\
        <br/><br/>\
        When using the Replace Number feature, please enter the <b>Exact</b> number to replace. If the number appears on the site as "(784) 233-1122", then enter that exact number into the box. By default, the script looks for the first phone number on the page.\
        </p>\
        Place before &lt;/head&gt;<br/>\
        <textarea id="copypastebox" style="position: absolute;bottom:5px;width: 550px;height:100px;"></textarea>';
    var template_html = '<scr' + 'ipt type="text/javascript">' + ' var kwcid="{{company_id}}";{{options}} var kwurl = "' + document.location.href.split("act_settings.php")[0] + '";' + '</scr' + 'ipt>' + '<scr' + 'ipt type="text/javascript">\n\
    setTimeout(function(){\n\
        var d = document, f = d.getElementsByTagName(\'script\')[0],\n\
        s = d.createElement(\'script\');\n\
        s.type = \'text/javascript\'; s.async = true; s.src = "' + document.location.href.split("act_settings.php")[0] + 'keyword-tracking.js' + '";\n\
        f.parentNode.insertBefore(s, f);\n\
    }, 1);\n\
    </scr' + 'ipt>';
    var specific_number = '';

    function generateOptions() {
        var add_to_template = "";
        if ($("input[value='select']").is(":checked")) {
            var options = [$("#g_paid"), $("#g_organic"), $("#g_local"), $("#y_paid"), $("#y_organic"), $("#y_local"),
                $("#b_paid"), $("#b_organic"), $("#b_local")
            ];
            for (var i in options) {
                add_to_template = add_to_template + " var " + options[i].attr("id") + " = " + options[i].is(":checked") + ";";
            }
        }
        if ($("#specific_number").is(":checked"))
            add_to_template = add_to_template + " var specific_number=\"" + $("#the_specific_num").val() + "\";";

        if ($("#numbers_clickable").is(":checked"))
            add_to_template = add_to_template + " var numbers_clickable=true;";

        var prescript = "";
        if(g_country!='default'){
            prescript = '<script type="text/javascript" src="'+document.location.href.split("act_settings.php")[0] + 'js/PhoneFormat.js' +'"></script>'+"\n";
            add_to_template = add_to_template + " var format = '"+g_country+"';";
        }
        $("#copypastebox").text(prescript+template_html.replace("{{company_id}}", company_id).replace("{{options}}", add_to_template));
    }

    var $dialog = $('<div></div>').html(dialog_html).dialog({
        modal: true,
        autoOpen: true,
        width: 587,
        height: 560,
        resizable: false,
        title: '<span class="ui-button-icon-primary ui-icon ui-icon-scissors" style="float:left; margin-right:5px;"></span>Generate Tracking Code',
        buttons: [{
            text: "Close",
            click: function() {
                $(this).dialog("close");
                $(this).dialog('destroy').remove();
            }
        }],
        beforeClose: function() {
            $(this).dialog('destroy').remove();
        },
        open: function() {
            var textarea = $(this).find("textarea");
            $(textarea).on("click", function() {
                this.focus();
                this.select();
            });
            $(textarea).text(template_html.replace("{{company_id}}", company_id).replace("{{options}}", ""));
            $(this).find("input[name='tracktype']").click(function() {
                if ($("input[value='select']").is(":checked")) {
                    $($dialog).find("#tracktype").find("input").prop("disabled", false);
                    generateOptions();
                } else {
                    $($dialog).find("#tracktype").find("input").prop("disabled", true).prop("checked", false);
                }
            });
            $(this).find("input[value='all']").click(function() {
                if ($("input[value='select']").not(":checked")) {
                    generateOptions();
                }
            });
            $(this).find("#country").change(function(){g_country = this.value; generateOptions();});
            $(this).find("#tracktype").find("input").prop("disabled", true);
            $(this).find("#tracktype").find("input,label").click(generateOptions);
            $(this).find("#specific_number").click(function() {
                if ($(this).is(":checked"))
                    $("#the_specific_num").prop("disabled", false);
                else
                    $("#the_specific_num").prop("disabled", true);
                generateOptions();
            });
            $(this).find("#numbers_clickable").click(function() {
                generateOptions();
            });

            $(this).find("#the_specific_num").keyup(function() {
                generateOptions();
            });
        }
    });
}

function update_flow(flow_id, title) {


    var company_id = $("#company_id").val();
    var callflowHTML = "";
    var width = 0;
    var height = 0;

    switch (flow_id) {
        case "1":
            { //Ring 1 number

                var width = 600;
                var height = 700;

                var outputHolder = $('<div><iframe id="ring1numberIframe" scrolling="no" src="ivr/opt_hours.php?cId=' + company_id + '" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen height="' + height + '"  width="580"></iframe></div>');

                //$("body").append(outputHolder);

                outputHolder.dialog({

                    resizable: false,
                    show: 'slow',
                    modal: true,
                    autoOpen: true,
                    minHeight: "auto",
                    maxHeight: "auto",
                    minWidth: width,
                    maxWidth: width,
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Ring 1 Number Options',
                    close: function(){
                        setTimeout(function(){
                            $("iframe[id='ring1numberIframe']").attr("src","about:blank;");
                            $("iframe[id='ring1numberIframe']").parent().remove();
                            $(".ui-dialog").remove();
                        },500);
                    }
                });
                break;
            }
        case "2": // Round robin
        case "3":
            { //Multiple numbers
                width = 380;
                height = 489;
                callflowHTML = '<br><input type="checkbox" name="international_number" id="international_number" style="display: inline !important;" />&nbsp;\
<label for="international_number" style="display: inline !important;">International Number</label><br><br />';
                callflowHTML = callflowHTML + '<label for="phone_number">Phone Number:</label>\
<input type="text" id="phone_number" class="text ui-widget-content ui-corner-all" style="display:inline-block !important;height: 20px;width: 189px !important;" />\
<button style="width:100px; margin-left:35px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" id="add_phone_number"><span class="ui-button-text">Add</span></button>\
<br /><br />\
<div style="width:98%; margin:0 auto; background:#AAA; height:1px;"></div>\
<table style="width: 100%;" id="number_t">\
<thead><tr><th align="left">Phone Number</th><th align="left">Delete</th></tr></thead><tbody>\
</tbody></table>\
<span style="position: absolute; bottom:0px; color: #AAA;width: 245px;margin-left: 15px;font: 10px Helvetica;">Number Example: 15552461128</span>';
                break;
            }
        case "4":
            {
                $("html, body").css('overflow', 'hidden');
                $(".ui-dialog").remove();
                $("#call_flow_iframe_holder").remove();

                var width = 900;
                var height = $(window).height();
                height = height - 60;

                var outputHolder = $('<div id="call_flow_iframe_holder"><iframe id="call_flow_iframe" src="ivr/?cId=' + company_id + '" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen height="' + height + '"  width="' + (width - 30) + '"></iframe></div>');

                //$("body").append(outputHolder);

                var doClose = false;

                var $dialog = outputHolder.dialog({
                    resizable: false,
                    draggable: false,
                    show: 'slow',
                    modal: true,
                    autoOpen: true,
                    minHeight: "auto",
                    maxHeight: "auto",
                    minWidth: width,
                    maxWidth: width,
                    title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Advanced Settings',
                    beforeClose: function() {
                        if (!$("#save_call_flow_btn").prop('disabled')) {
                            promptMsg('Do you want to save the changes?', function() {
                                document.getElementById('call_flow_iframe').contentWindow.SaveCallFlow(company_id);
                                $('.ui-widget-overlay').remove();
                                $(".ui-dialog").hide();
                            });

                            setTimeout(function() {
                                $(".ui-dialog-buttonset button:eq(1)").click(function() {
                                    $(".ui-dialog").remove();
                                });
                            }, 500);
                            return false;
                        }else{
                            doClose = true;
                            return true;
                        }
                    },
                    close: function(event, ui) {
                        if(doClose == true){
                            setTimeout(function(){
                                $("#call_flow_iframe").attr("src","about:blank;");
                                $("#call_flow_iframe_holder").remove();
                                $(".ui-dialog").remove();
                            },500);
                            doClose = false;
                            $("html, body").css('overflow', 'auto');
                            return true;
                        }else{
                            event.preventDefault();
                            return false;
                        }
                    }
                });

                $(window).resize(function() {
                    var height = $(window).height();
                    height = height - 60;
                    $("#call_flow_iframe").attr("height", height);
                    $dialog.dialog("option", "position", "center");
                });

                $(".ui-dialog-titlebar").prepend('<div style="float: right; padding-right: 19px; width: 314px;">' +
                    '<input id="copy_from_another_company_btn" type="button" class="submit mid" value="Copy from Company" style="width: 205px; height: 30px; line-height: 30px; background: url(images/btnvb.png) top center no-repeat; border: 0; font-size: 14px; font-weight: normal; text-transform: uppercase; text-shadow: 1px 1px 0 #0a5482; cursor: pointer; margin-right: 10px; vertical-align: middle; color: #FFFFFF; display: inline-block !important;font-family: \'Titillium800\', \'Trebuchet MS\', Arial, sans-serif;" /> ' +
                    '<input id="save_call_flow_btn" type="button" class="submit mid" value="Save" onclick="document.getElementById(\'call_flow_iframe\').contentWindow.SaveCallFlow(' + company_id + ');" style="width: 85px; height: 30px; line-height: 30px; background: url(images/btns.gif) top center no-repeat; border: 0; font-size: 14px; font-weight: normal; text-transform: uppercase; text-shadow: 1px 1px 0 #0a5482; cursor: pointer; margin-right: 10px; vertical-align: middle; color: #FFFFFF; display: inline-block !important; font-family: \'Titillium800\', \'Trebuchet MS\', Arial, sans-serif;" />' +
                    '</div>'
                );

                $("#save_call_flow_btn").fadeTo(0, 0.6);
                $("#save_call_flow_btn").prop('disabled', true);

                $("#copy_from_another_company_btn").click(function() {
                    var companies_txt = '<option value="0">Select Company</option>';

                    if (companies.length == 0) {
                        companies_txt += '<option value="" disabled="disabled">None of your other companies are using the Advanced Call Flow.</option>';
                    } else {
                        for (var i in companies) {
                            companies_txt += '<option value="' + companies[i].idx + '">' + companies[i].company_name + '</option>';
                        }
                    }

                    var copyCompanyOutputHolder = $('<div id="copy_from_company_div"><br />' +
                        '<select id="copy_company_id" style="width: 390px; height: 28px; vertical-align: middle;" class="styled">' +
                        companies_txt +
                        '</select>');

                    $("body").append(copyCompanyOutputHolder);

                    $("#copy_from_company_div").dialog({
                        resizable: false,
                        modal: true,
                        autoOpen: true,
                        width: 440,
                        title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>Copy Advanced Call Flow from Another Company',
                        close: function(event, ui) {
                            $(this).dialog("destroy");
                            $(this).remove();
                        },
                        buttons: [
                            {text: "Copy",
                            click: function(){
                                var from_cId = $("#copy_company_id").val();
                                if (from_cId == 0) {
                                    errMsgDialog("Please select a company first!");
                                    return false;
                                }

                                $("#call_flow_iframe").attr("src", "ivr/?cId=" + company_id + "&from_cId=" + from_cId);

                                $("#copy_from_company_div").dialog("close");
                                $("#copy_from_company_div").dialog('destroy').remove();
                            }}
                        ],
                        open: function(event, ui) {
                            $("#copy_company_id").select_skin();
                            $("#copy_company_id").css("z-index",10000);
                        }
                    });
                });

                break;
            }
        case "6":
            {

                width = 250;
                height = 177;
                callflowHTML = '<p style="font-size:15px;">\
<form>\
<fieldset>\
<label for="sip_endpoint">SIP End Point</label>\
<input type="text" name="sip_endpoint" id="sip_endpoint" class="text ui-widget-content ui-corner-all" />\
<label for="sip_msg">Message</label>\
<input type="text" name="sip_msg" id="sip_msg" class="text ui-widget-content ui-corner-all" />\
<label for="sip_username">SIP Username</label>\
<input type="text" name="sip_username" id="sip_username" class="text ui-widget-content ui-corner-all" />\
<label for="sip_password">SIP Password</label>\
<input type="text" name="sip_password" id="sip_password" class="text ui-widget-content ui-corner-all" />\
<label for="sip_header">Custom Header Number</label>\
<input type="text" name="sip_header" id="sip_header" class="text ui-widget-content ui-corner-all" />\
</fieldset>\
</form></p>';
                break;

            }
    }
    if (flow_id != '4' && flow_id != '1') {

        var dialog_div_id = "ecf_" + flow_id;

        if ($("#" + dialog_div_id).length == 0) {
            $("body").append('<div id="' + dialog_div_id + '"></div>');
        }


        var $dialog = $('#' + dialog_div_id).html(callflowHTML).dialog({
            modal: true,
            autoOpen: true,
            minHeight: height,
            maxHeight: height,
            minWidth: width,
            maxWidth: width,
            resizable: false,
            title: '<span class="ui-button-icon-primary ui-icon ui-icon-pencil" style="float:left; margin-right:5px;"></span>' + title + ' Settings',
            buttons: [{
                text: "Save",
                click: function() {
                    switch (flow_id) {
                        case "1": // Ring 1 Number
                            {
                                if ($("#outgoing").val().length != 10 && !$("#international_number").is(":checked") && $("#outgoing").val() != "") {
                                    errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
                                    return false;
                                } else {
                                    var intl = 0;
                                    if ($("#international_number").is(":checked")) {
                                        intl = 1;
                                    }
                                    var voicemail = 0;
                                    if ($("#voicemail").is(":checked")) {
                                        voicemail = 1;
                                    }
                                    var aparams = {
                                        outgoing: $("#outgoing").val(),
                                        company_id: company_id,
                                        intl: intl,
                                        voicemail: voicemail
                                    };
                                    var response = ajaxCall({
                                        func: "SYSTEM_EDIT_OUTGOING_NUMBER",
                                        data: aparams
                                    });
                                    response.done(function(msg) {
                                        var msg = msg.split(":::");
                                        console.log(msg);
                                        if (msg[0] == "SUCCESS") {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                            msgDialogCB("Edited Outgoing Number.", (function() {
                                                $($dialog).dialog("close");
                                                $($dialog).dialog('destroy').remove();
                                            }));
                                        } else errMsgDialog("There was an error in the request.");
                                    });
                                }
                                break;
                            }
                        case "2":
                        case "3":
                            {
                                var func = "";
                                if (flow_id == "2")
                                    func = "SYSTEM_EDIT_ROUND_ROBIN";
                                else
                                    func = "SYSTEM_EDIT_MN";
                                var numbers = new Array();
                                $("#number_t > tbody").children().each(function(i, v) {
                                    numbers.push($(v).text());
                                });
                                if (flow_id == "2") {
                                    var international_number = 0;
                                    if ($("#international_number").is(":checked")) {
                                        international_number = 1;
                                    }
                                    var forward_sec = $('#forward_sec').val();
                                    var forward_number = $('#forward_number').val();
                                    var response = JSONajaxCall({
                                        func: func,
                                        company_id: company_id,
                                        numbers: numbers,
                                        international_number: international_number,
                                        forward_sec: forward_sec,
                                        forward_number: forward_number

                                    });
                                } else
                                    var response = JSONajaxCall({
                                        func: func,
                                        company_id: company_id,
                                        numbers: numbers
                                    });
                                response.done(function() {
                                    $($dialog).dialog("close");
                                    $($dialog).dialog('destroy').remove();
                                    msgDialogCB("Saved numbers.", (function() {
                                        $($dialog).dialog("close");
                                        $($dialog).dialog('destroy').remove();
                                    }));
                                });
                                break;
                            }
                        case "6":
                            {

                                if ($("#sip_endpoint").val() == '') {
                                    errMsgDialog("Please provide SIP end point.");
                                    return false;
                                } else {


                                    var aparams = {
                                        sip_endpoint: $("#sip_endpoint").val(),
                                        sip_msg: $("#sip_msg").val(),
                                        sip_username: $("#sip_username").val(),
                                        sip_password: $("#sip_password").val(),
                                        sip_header: $("#sip_header").val(),
                                        company_id: company_id
                                    };
                                    var response = ajaxCall({
                                        func: "SYSTEM_EDIT_SIP",
                                        data: aparams
                                    });
                                    response.done(function(msg) {

                                        var msg = msg.split(":::");
                                        console.log(msg);
                                        if (msg[0] == "SUCCESS") {
                                            $($dialog).dialog("close");
                                            $($dialog).dialog('destroy').remove();
                                            msgDialogCB("Edited SIP End Point.", (function() {
                                                $($dialog).dialog("close");
                                                $($dialog).dialog('destroy').remove();
                                            }));
                                        } else errMsgDialog("There was an error in the request.");
                                    });
                                }
                                break;


                            }
                    }
                }
            }, {
                text: "Cancel",
                click: function() {
                    $(this).dialog("close");
                    $(this).dialog('destroy').remove()
                }
            }]
        });
        switch (flow_id) {
            case "1":
                {
                    /* var response = JSONajaxCall({
                 func: "SYSTEM_GET_OUTGOING_NUMBER",
                 data: company_id
                 });
                 response.done(function(data){
                 $("#outgoing").val(data.number);
                 if(data.intl==1)
                 $("#international_number").attr("checked","checked");
                 if(data.voicemail==1)
                 $("#voicemail").attr("checked","checked");
                 });*/
                    break;
                }
            case "2":
            case "3":
                {
                    var func = "";
                    if (flow_id == "2")
                        func = "SYSTEM_GET_ROUNDROBIN";
                    else
                        func = "SYSTEM_GET_MN";
                    var response = JSONajaxCall({
                        func: func,
                        data: company_id
                    });
                    response.done(function(data) {

                        $('#forward_number').val(data['forward_number']);
                        $('#forward_sec').val(data['forward_sec']);
                        if (data['international'] == 1) $('#international_number').prop('checked', true);
                        if (data['international'] == 0) $('#international_number').prop('checked', false);



                        $.each(data['numbers'], function(i, v) {
                            console.log(v);
                            $('<tr><td>' + v.number + '</td><td><a href="#" onclick="$(this).parent().parent().remove()" =><img src="images/delete.gif"></td></tr>').appendTo("#number_t > tbody");
                        });
                    });
                    $("#add_phone_number").click(function() {
                        var number = $("#phone_number").val();
                        if (number.length != 10 && !$("#international_number").is(":checked") && number != "" && $.isNumeric(number)) {
                            errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
                        } else {
                            if ($.isNumeric(number)) {
                                if (!$("#international_number").is(":checked")) {
                                    $('<tr><td>' + "1" + number + '</td><td><a href="#" onclick="$(this).parent().parent().remove()" =><img src="images/delete.gif"></td></tr>').appendTo("#number_t > tbody");
                                    $("#phone_number").val("");
                                } else {
                                    $('<tr><td>' + number + '</td><td><a href="#" onclick="$(this).parent().parent().remove()" =><img src="images/delete.gif"></td></tr>').appendTo("#number_t > tbody");
                                    $("#phone_number").val("");
                                }
                            } else {
                                errMsgDialog("Invalid number.");
                            }
                        }
                    });

                    break;
                }
            case "6":
                {
                    var response = JSONajaxCall({
                        func: "SYSTEM_GET_SIP",
                        data: company_id
                    });
                    response.done(function(data) {

                        $("#sip_endpoint").val(data.sip_endpoint);
                        $("#sip_msg").val(data.sip_msg);
                        $("#sip_username").val(data.sip_username);
                        $("#sip_password").val(data.sip_password);
                        $("#sip_header").val(data.sip_header);


                    });
                    break;
                }

        }

    }
}

$(document).ready(function() {
    $("*[id*=SYSTEM_]").click(systemClickEvt);
});

function ajaxCall(data) {
    return $.ajax({
        url: "admin_ajax_handle.php",
        type: "POST",
        data: data,
        cache: false
    });
}

function JSONajaxCall(data) {
    return $.ajax({
        url: "admin_ajax_handle.php",
        type: "POST",
        data: data,
        dataType: "json",
        cache: false
    });
}

function secondstotime(secs) {
    var t = new Date(1970, 0, 1);
    t.setSeconds(secs);
    var s = t.toTimeString().substr(0, 8);
    if (secs > 86399)
        s = Math.floor((t - Date.parse("1/1/70")) / 3600000) + s.substr(2);
    return s;
}

function IsNumeric(strString) {
    var strValidChars = "0123456789.";
    var strChar;
    var blnResult = true;

    if (strString.length == 0) return false;

    //check if strString consists of valid characters listed above
    for (i = 0; i < strString.length && blnResult == true; i++) {
        strChar = strString.charAt(i);
        if (strValidChars.indexOf(strChar) == -1) {
            blnResult = false;
        }
    }
    return blnResult;
}

function blockUI(selector) {
    $(selector).block({
        message: '<h1 style="color:#fff">Loading</h1>',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .7,
            color: '#fff !important'
        }
    });
}

function Truncate(str, maxLength, suffix) {
    if (str.length > maxLength) {
        str = str.substring(0, maxLength + 1);
        str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
        str = str + suffix;
    }
    return str;
}

function addslashes(str) {
    str = str.replace(/\\/g, '\\\\');
    str = str.replace(/\'/g, '\\\'');
    str = str.replace(/\"/g, '\\"');
    str = str.replace(/\0/g, '\\0');
    return str;
}
function stripslashes(str) {
    str=str.replace(/\\'/g,'\'');
    str=str.replace(/\\"/g,'"');
    str=str.replace(/\\0/g,'\0');
    str=str.replace(/\\\\/g,'\\');
    return str;
}

function addMoreEmails(el, value) {
    if (typeof(value) == "undefined") value = "";
    $(el).parents('div#notification_emails').append('<div class="notification_email">' + 
                            '<div style="float: left; padding-bottom: 5px;"><input type="text" id="forward_email" style="padding: 2px; width: 170px;" value="' + value + '" /></div>' +
                            '<div style="float: left; padding-bottom: 5px; padding-left: 5px;"><a class="remove action deleteThisRow" href="javascript:void(0)" onclick="$(this).parents(\'.notification_email\').remove(); $(\'.ui-dialog:visible\').css(\'top\', (parseInt($(\'.ui-dialog:visible\').css(\'top\').replace(\'px\', \'\')) + 12) + \'px\');"> <span class="replace">Remove</span></a></div>' +
                            '<div style="clear: both;"></div>' +
                        '</div>');

    $(".ui-dialog:visible").css('top', (parseInt($(".ui-dialog:visible").css('top').replace("px", "")) - 12) + 'px');
}
