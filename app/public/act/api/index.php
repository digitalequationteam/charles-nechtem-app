<?php
/*
 * Web1 - ACT API
 * 2.0.1 - Switch to Limonade for better compatibility
 */
require_once('limonade.php');
require_once('../include/util.php');
require_once('../include/Pagination.php');
ignore_user_abort();

// Routes ////////////////////////////////////////////////////////

// Index
dispatch_get('/','ACTAPI::index');

// API Version
dispatch_get('/:authtoken/version','ACTAPI::version');

// Companies
dispatch_get('/:authtoken/companies','ACTAPI::companies');

// Calls
dispatch_get('/:authtoken/calls/:company_id','ACTAPI::calls');

//TWILIO AUTH TOKEN
dispatch_get('/:authtoken/auth_token','ACTAPI::auth_token');

// Reports
dispatch_get('/:authtoken/report/:company_id/:start_date/:end_date','ACTAPI::report');

dispatch_get('/:authtoken/report_outgoing/:company_id/:start_date/:end_date','ACTAPI::report_outgoing');

// Outgoing Calls
dispatch_get('/:authtoken/outgoing_calls/:company_id','ACTAPI::outgoing_calls');

dispatch_get('/:authtoken/get_phone_codes/:number','ACTAPI::get_phone_codes');

// Save Softphone Details
dispatch_post('/:authtoken/save_call','ACTAPI::save_call');

// Save Phone Code for Call
dispatch_post('/:authtoken/set_phone_code','ACTAPI::set_phone_code');

// Save Outgoing Call details
dispatch_post('/:authtoken/save_outgoing_call','ACTAPI::save_outgoing_call');

// Get User's Contacts List
dispatch_get('/:authtoken/get_contacts_list','ACTAPI::get_contacts_list');

// Save a User's Contact List
dispatch_post('/:authtoken/save_contacts','ACTAPI::save_contacts');

// Save incoming tracking emails
dispatch_post('/:authtoken/handle_incoming_email','ACTAPI::handle_incoming_email');

// INTERNAL APPS ONLY
dispatch_get('/:authtoken/get_campaign_report_app','ACTAPI::get_campaign_report_app');
dispatch_get('/:authtoken/get_dailytrend_report_app','ACTAPI::get_dailytrend_report_app');

// Classes ///////////////////////////////////////////////////////

header("Content-Type: application/json");

class ACTAPI {

    static function index()
    {
        echo json_encode(array("error"=>"NO_METHOD_SPECIFIED"));
    }

    static function version()
    {
        $userid = APIUtil::generalAuth(params('authtoken'));

        global $VERSION, $build_number, $AccountSid, $AuthToken;
        $db = new DB();
        echo json_encode(array("act"=>$VERSION,"db"=>$db->getDatabaseVersion(),"build"=>$build_number,"AccountSid"=>$AccountSid,"AuthToken"=>$AuthToken));
    }

    static function companies()
    {
        $userid = APIUtil::generalAuth(params('authtoken'));

        $db = new DB();

        $ret = array();

        $_SESSION['user_id'] = $userid;

        if($db->isUserAdmin($userid))
            $companies = $db->getAllCompanies();
        else
            $companies = $db->getAllCompaniesForUser($userid);

        foreach($companies as $company)
        {
            $company["numbers"] = array();
            foreach($db->getCompanyNumIntl($company["idx"]) as $number)
            {
                if($number[1]==true)
                    $new_number = array("number"=>$number[0],"international"=>true);
                else{
                    $new_number = array("number"=>"+1".$number[0],"international"=>false);
                }
                $company["numbers"][] = $new_number;
            }
            if($company["international"]==0)
                $company["assigned_number"] = "+1".$company["assigned_number"];

            $phone_codes_array = array();
            $phone_codes_outgoing_array = array();

            $phone_codes = $db->getPhoneCodes($company["idx"]);
            $phone_codes_outgoing = $db->getPhoneCodes($company["idx"],2);

            if($phone_codes)
            {
                foreach ($phone_codes as $phone_code)
                {
                    $phone_codes_array[] = array("id"=>$phone_code->idx,"name"=>$phone_code->name);
                }
            }
            if($phone_codes_outgoing)
            {
                foreach($phone_codes_outgoing as $phone_code_outgoing){
                    $phone_codes_outgoing_array[] = array("id"=>$phone_code_outgoing->idx,"name"=>$phone_code_outgoing->name);
                }
            }

            $company["phone_codes"] = $phone_codes_array;
            $company["phone_codes_outgoing"] = $phone_codes_outgoing_array;
            $company["call_count"] = $db->cget_calls_count($company["idx"]);
            $ret[] = $company;
        }

        echo json_encode($ret);
    }

    static function auth_token()
    {
        global $DB_NAME;

        $userid = APIUtil::generalAuth(params('authtoken'));

        global $AccountSid, $AuthToken;

        //By default, setting the live credentials to use in generating the JS token
        $accountSid = $AccountSid;
        $auth_token = $AuthToken;

        try {
            //Initializing the twilio API
            $token = new Services_Twilio_Capability($accountSid, $auth_token);

            $twilio_app_name = 'ACT_V_THREE_' . md5($DB_NAME) . "_" . $userid;

            $token->allowClientOutgoing(get_twilio_application_sid($accountSid, $auth_token, $twilio_app_name, $userid));

            // @end snippet
            $output = $token->generateToken();
        } catch (Services_Twilio_RestException $e) {
            //Nothing to do here
            $output = '';
        }

        echo $output;
    }

    static function report()
    {
        $userid     = APIUtil::generalAuth(params('authtoken'));
        $company_id = params("company_id");

        $start_date = params("start_date");
        $end_date   = params("end_date");

        $db = new DB();

        $_SESSION['user_id'] = $userid;

        if($db->isUserInCompany($company_id,$userid) || $db->isUserAdmin($userid))
            $calls = $db->getCallsInRange($company_id,$start_date,$end_date);
        else
            die(json_encode(array("error"=>"USER_DOES_NOT_HAVE_ACCESS")));

        echo json_encode(array($calls, count($calls)));
    }

    static function report_outgoing()
    {
        $userid     = APIUtil::generalAuth(params('authtoken'));
        $company_id = params("company_id");

        $start_date = params("start_date");
        $end_date   = params("end_date");

        $db = new DB();

        $_SESSION['user_id'] = $userid;

        if($db->isUserInCompany($company_id,$userid) || $db->isUserAdmin($userid))
            $calls = $db->getCallsInRange($company_id,$start_date,$end_date,0,'all',false,'',true);
        else
            die(json_encode(array("error"=>"USER_DOES_NOT_HAVE_ACCESS")));

        echo json_encode(array($calls, count($calls)));
    }

    static function calls()
    {
        $userid     = APIUtil::generalAuth(params('authtoken'));
        $company_id = params("company_id");

        $db = new DB();

        $_SESSION['user_id'] = $userid;

        if($db->isUserInCompany($company_id,$userid) || $db->isUserAdmin($userid))
            $calls = $db->cget_all_calls($company_id);
        else
            die(json_encode(array("error"=>"USER_DOES_NOT_HAVE_ACCESS")));

        echo json_encode($calls);
    }

    static function save_outgoing_call()
    {
        $db = new DB();
        $params = $_POST['call_details'];
        $userid = @APIUtil::generalAuth(params('authtoken'));

        $token = @explode("_|_",base64_decode(params('authtoken')));

        $app = @$token[2];

        if($params=="")
        {
            die(json_encode(array("error"=>"INVALID_CALL_DETAILS")));
        }else{

            // Call
            $CallSid          = isset($params['CallSid']) ? $params['CallSid'] : "";
            $AccountSid       = isset($params['AccountSid']) ? $params['AccountSid'] : "";
            $CallStatus       = isset($params['CallStatus']) ? $params['CallStatus'] : "";
            $To               = isset($params['To']) ? $params['To'] : "";
            $From             = isset($params['From']) ? $params['From'] : "";
            $RecordingUrl     = isset($params['RecordingUrl']) ? $params['RecordingUrl'] : "";
            $Direction        = isset($params['Direction']) ? $params['Direction'] : "";
            $ApiVersion       = isset($params['ApiVersion']) ? $params['ApiVersion'] : "";

            // Dialed Call
            $DialCallSid      = isset($params['DialCallSid']) ? $params['DialCallSid'] : "";
            $CallDuration     = isset($params['CallDuration']) ? $params['CallDuration'] : (isset($params['DialCallDuration']) ? $params['DialCallDuration'] : "");
            $AnsweredBy       = isset($params['AnsweredBy']) ? $params['AnsweredBy'] : "";
            $MadeUsing        = isset($app) ? $app : "";
            $DialCallStatus   = isset($params['DialCallStatus']) ? $params['DialCallStatus'] : "";
            $userid           = isset($userid) ? $userid : "";
            $DateCreated      = date('Y-m-d H:i:s',time());

            $result = $db->save_outgoing_call_api(array(
                $CallSid,
                $AccountSid,
                $CallStatus,
                $To,
                $From,
                $RecordingUrl,
                $Direction,
                $ApiVersion,
                $DialCallSid,
                $CallDuration,
                $DialCallStatus,
                $DateCreated,
                $AnsweredBy,
                $MadeUsing,
                $userid
            ));

            if($result)
                die(json_encode(array("success")));
            else
                die(json_encode(array("error"=>"DB_ERROR")));
        }
    }

    static function get_phone_codes(){
        $userid = @APIUtil::generalAuth(params('authtoken'));
        $db = new DB();
        $phone_number = params("number");

        if(!$db->isUserAbleToSetPhoneCodes($userid) && !$db->isUserAdmin($userid)){
            die(json_encode(array("error"=>"PHONECODE_NOT_ALLOWED")));
        }

        if(!isset($phone_number) || $phone_number == "")
            die(json_encode(array("error"=>"PHONE_NUMBER_NOT_VALID")));
        else{
            $company = $db->getCompanyOfNumber($db->format_phone_db($phone_number));
            die(json_encode($db->getPhoneCodes($company,1)));
        }
    }

    static function set_phone_code(){
        $userid = @APIUtil::generalAuth(params('authtoken'));
        $db = new DB();
        $CallSid = $_POST['callsid'];
        $phonecode_id = $_POST['phonecode_id'];

        if(!$db->isUserAbleToSetPhoneCodes($userid) && !$db->isUserAdmin($userid)){
            die(json_encode(array("error"=>"PHONECODE_NOT_ALLOWED")));
        }

        if(!isset($_POST['callsid']) || !isset($_POST['phonecode_id']) ||
            $_POST['callsid']=="" || $_POST['phonecode_id']=="" || !is_numeric($_POST['phonecode_id']))
            die(json_encode(array("error"=>"INVALID_VARIABLES")));
        else{
            $company = $db->getCompanyofCall($CallSid);
            $phone_codes = $db->getPhoneCodes($company);
            $found = false;
            foreach($phone_codes as $phone_code){
                if($phone_code->idx == $phonecode_id)
                    $found = true;
            }
            if($phonecode_id==0)
                $found=true;
            if($found==false)
                die(json_encode(array("error"=>"NO_SUCH_PHONECODE")));
            else{
                $db->setPhoneCode($CallSid,$phonecode_id,1);
                die(json_encode(array("success")));
            }
        }
    }

    static function save_call()
    {
        $userid = @APIUtil::generalAuth(params('authtoken'));
        $db = new DB();
        $params = $_POST['call_details'];

        if($params=="")
        {
            die(json_encode(array("error"=>"INVALID_CALL_DETAILS")));
        }else{
            // Call
            $CallSid          = isset($params['CallSid']) ? $params['CallSid'] : "";
            $AccountSid       = isset($params['AccountSid']) ? $params['AccountSid'] : "";
            $CallStatus       = isset($params['CallStatus']) ? $params['CallStatus'] : "";
            $To               = isset($params['To']) ? $params['To'] : "";
            $From             = isset($params['From']) ? $params['From'] : "";
            $RecordingUrl     = isset($params['RecordingUrl']) ? $params['RecordingUrl'] : "";
            $Direction        = isset($params['Direction']) ? $params['Direction'] : "";
            $ApiVersion       = isset($params['ApiVersion']) ? $params['ApiVersion'] : "";

            // Dialed Call
            $DialCallSid      = isset($params['DialCallSid']) ? $params['DialCallSid'] : "";;
            $DialCallDuration = isset($params['DialCallDuration']) ? $params['DialCallDuration'] : "";;
            $DialCallStatus   = isset($params['DialCallStatus']) ? $params['DialCallStatus'] : "";;
            $DialCallTo       = isset($params['DialCallTo']) ? $params['DialCallTo'] : "";
            $DateCreated      = date('Y-m-d H:i:s',time());

            $result = $db->save_call_api(array(
                $CallSid,
                $AccountSid,
                $CallStatus,
                $To,
                $From,
                $RecordingUrl,
                $Direction,
                $ApiVersion,
                $DialCallSid,
                $DialCallDuration,
                $DialCallStatus,
                $DateCreated,
                $DialCallTo
            ));

            if($result)
                die(json_encode(array("success")));
            else
                die(json_encode(array("error"=>"DB_ERROR")));
        }
    }

    static function outgoing_calls()
    {
        $userid     = APIUtil::generalAuth(params('authtoken'));
        $company_id = params("company_id");

        $db = new DB();

        $_SESSION['user_id'] = $userid;

        if($db->isUserInCompany($company_id,$userid) || $db->isUserAdmin($userid))
            $calls = $db->cget_all_outgoing_calls($company_id);
        else
            die(json_encode(array("error"=>"USER_DOES_NOT_HAVE_ACCESS")));

        echo json_encode($calls);
    }

    static function get_contacts_list()
    {
        $userid     = APIUtil::generalAuth(params('authtoken'));

        require_once '../include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

        $contacts = ad_advb_cl_get_contact_lists(0, 0, true);

        echo json_encode($contacts);
    }

    static function save_contacts()
    {
        $userid     = APIUtil::generalAuth(params('authtoken'));

        require_once '../include/ad_auto_dialer_files/lib/ad_lib_funcs.php';
        require_once '../include/twilio_header.php';

        $contacts_list = $_POST["contacts_list"];
        $act_new_contacts_list = $_POST["act_new_contacts_list"];
        $contacts = json_decode($_POST["contacts"]);

        $single_contacts = array();
        foreach ($contacts as $contact) {
            $single_contacts[] = array(
                0 => $contact->phone,
                1 => $contact->name,
                2 => $contact->fname,
                3 => $contact->lname,
                4 => $contact->email,
                5 => $contact->address,
                6 => $contact->city,
                7 => $contact->state,
                8 => $contact->zip,
                9 => $contact->website
            );
        }

        $cl_idx = ($contacts_list == "new_contacts_list") ? ad_advb_cl_add_cl($act_new_contacts_list) : ad_advb_cl_add_cl($contacts_list);

        ad_advb_cl_add_contacts($cl_idx, $single_contacts);

        exit();

    }

    static function get_dailytrend_report_app(){
        header('Access-Control-Allow-Origin: *');
        global $AccountSid, $AuthToken, $ApiVersion, $TIMEZONE;
        $client = new Services_Twilio($AccountSid,$AuthToken);

        $db         = new DB();
        $userid     = APIUtil::generalAuth(params('authtoken'));

        $company_id   = $_GET['company_id'];
        $start_date   = $_GET['start_date'];
        $end_date     = $_GET['end_date'];
        $phone_code   = $_GET['phone_code'];
        $call_result  = $_GET['call_result'];
        $outgoing     = false;
        $outgoing_num = $_GET['outgoing_number'];

        if(isset($_GET['outgoing']) && $_GET['outgoing']==1)
            $outgoing = true;

        // First Graph
        $day = array();
        $data = new GDataView();
        $data->addCol("string","Day");
        $data->addCol("number","Answered");
        $data->addCol("number","Unanswered");
        $data->addCol("number","Busy");
        $err_added = false;

        $calls = $db->getCallsInRange($company_id,$start_date,$end_date,$phone_code,$call_result,false,$outgoing_num,$outgoing,"",'');

        for($i=0; $i < 6; $i++)
        {
            $day[$i] = array("ans"=>0,"unans"=>0,"busy"=>0,"err"=>0);
        }

        if( is_array($calls) || is_object($calls) )
            foreach($calls as $call)
            {
                $dateObj = new DateTime("@".strtotime($call['DateCreated']));
                $date = $dateObj->format("N")-1;
                //$date = date("N",strtotime($call['DateCreated']))-1;
                $arr = &$day[$date];

                if($call['DialCallStatus']=="no-answer")
                    $arr["unans"]=@$arr["unans"]+1;
                elseif($call['DialCallStatus']=="busy")
                    $arr["busy"]=@$arr["busy"]+1;
                elseif($call['DialCallStatus']=="completed")
                    $arr["ans"]=@$arr["ans"]+1;
                else{
                    $arr["err"]=@$arr["err"]+1;
                    if(!$err_added)
                    {
                        $data->addCol("number","Error");
                        $err_added = true;
                    }
                }
            }

        foreach($day as $d=>$count)
        {
            //$diff = floor(($end_date - $start_date)/(60*60*24));

            $data->addRow(array("c"=>array(
                array("f"=>jddayofweek($d,1)),
                array("v"=>@$count['ans']),
                array("v"=>@$count['unans']),
                array("v"=>@$count['busy']),
                array("v"=>@$count['err']),
                //array("v"=>($count/$diff))
            )));
        }
        $days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');

        $all_data = array("big"=>$data->getData());

        foreach($days as $day){
            $hours = array();
            $data = new GDataView();
            $data->addCol("string","Hours");
            $data->addCol("number","Answered");
            $data->addCol("number","Unanswered");
            $data->addCol("number","Busy");
            $err_added = false;

            for($i=0; $i < 24; $i++)
            {
                $hours[$i] = array("ans"=>0,"unans"=>0,"busy"=>0,"err"=>0);
            }


            if( is_array($calls) || is_object($calls) )
                foreach($calls as $call)
                {
                    $dateObj = new DateTime("@".strtotime($call['DateCreated']));
                    $dateObj->setTimezone(new DateTimeZone($TIMEZONE));
                    $date = $dateObj->format("G");
                    $arr = &$hours[$date];

                    if($day!='all' && $day!=strtolower($dateObj->format("l")))
                        continue;
                    else{
                        if($call['DialCallStatus']=="no-answer")
                            $arr["unans"]=$arr["unans"]+1;
                        elseif($call['DialCallStatus']=="busy")
                            $arr["busy"]=$arr["busy"]+1;
                        elseif($call['DialCallStatus']=="completed")
                            $arr["ans"]=$arr["ans"]+1;
                        else{
                            $arr["err"]=$arr["err"]+1;
                            if(!$err_added){
                                $data->addCol("number","Error");
                                $err_added = true;
                            }
                        }
                    }
                }

            foreach($hours as $hour=>$count)
            {
                //$diff = floor(($end_date - $start_date)/(60*60*24));

                $data->addRow(array("c"=>array(
                    array("f"=>date("ga",strtotime($hour.":00"))),
                    array("v"=>$count['ans']),
                    array("v"=>$count['unans']),
                    array("v"=>$count['busy']),
                    array("v"=>$count['err']),
                    //array("v"=>($count/$diff))
                )));
            }
            $all_data[$day] = $data->getData();
        }
        die(json_encode(array("result"=>"success","data"=>$all_data)));
    }

    static function get_campaign_report_app(){
        header('Access-Control-Allow-Origin: *');
        global $AccountSid, $AuthToken, $ApiVersion;
        $client = new Services_Twilio($AccountSid,$AuthToken);

        $db         = new DB();
        $userid     = APIUtil::generalAuth(params('authtoken'));

        $company_id   = $_GET['company_id'];
        $start_date   = $_GET['start_date'];
        $end_date     = $_GET['end_date'];
        $phone_code   = $_GET['phone_code'];
        $call_result  = $_GET['call_result'];
        $outgoing_num = @$_GET['outgoing_number'];
        $outgoing     = false;

        if(isset($_GET['outgoing']) && $_GET['outgoing']==1){
            $outgoing = true;
        }

        $numbers = $db->getNumbersOfCompany($company_id, true);

        $campaigns = array();
        foreach ($client->account->incoming_phone_numbers as $number) {
            $this_phone_number = $db->format_phone_db($number->phone_number);
            foreach ($numbers as $number2) {
                if ($number2->number == $this_phone_number) {
                    if (!isset($campaigns[$number->friendly_name]))
                        $campaigns[$number->friendly_name] = array();

                    $campaigns[$number->friendly_name][] = $this_phone_number;
                    break;
                }
            }
        }

        $day = array();
        $data2 = new GDataView();

        $data2->addCol("string", "Day");
        foreach ($campaigns as $name=>$numbers) {
            $data2->addCol("number", $name);
        }

        $err_added = false;

        $calls = $db->getCallsInRange($company_id,$start_date,$end_date,$phone_code,$call_result,false,$outgoing_num,$outgoing);

        $overview = array();

        //var_dump($calls);

        for($i=0; $i < 6; $i++)
        {
            $this_day = array();
            foreach ($campaigns as $name=>$numbers) {
                $this_day[$name] = 0;
            }
            $day[$i] = $this_day;
        }

        if( is_array($calls) || is_object($calls) )
            foreach($calls as $call)
            {
                $dateObj = new DateTime("@".strtotime($call['DateCreated']));
                $date = $dateObj->format("N")-1;
                //$date = date("N",strtotime($call['DateCreated']))-1;
                $arr = &$day[$date];

                $callTo = $outgoing ? $db->format_phone_db($call['CallFrom']) : $db->format_phone_db($call['CallTo']);

                foreach ($campaigns as $name=>$numbers) {
                    if (in_array($callTo, $numbers)) {
                        if(@$overview[$name]=="")
                            $overview[$name] = 1;
                        else
                            $overview[$name] = $overview[$name]+1;

                        @$arr[$name] = @$arr[$name] + 1;
                    }
                }
            }

        foreach($day as $d=>$count)
        {
            //$diff = floor(($end_date - $start_date)/(60*60*24));

            $data_array = array(
                "c"=>array(
                    array("f"=>jddayofweek($d,1))
                )
            );

            foreach ($campaigns as $name=>$numbers) {
                @$data_array["c"][] = array("v"=>$count[$name]);
            }

            $data2->addRow($data_array);
        }

        $overview_ = array();
        $total = 0;
        // Overview, create table.
        foreach($overview as $key => $value){
            $total = $total + $value;
        }
        foreach($overview as $key => $value){
            $overview_[$key] = $value." (". round(100 * $value/$total) ."%)";
        }

        $campaign_graphs = array();

        $numbers = $db->getNumbersOfCompany($company_id, true);

        foreach($campaigns as $name=>$numbers11){
            $answered   = 0;
            $unanswered = 0;
            $busy       = 0;
            $error      = 0;

            $data = new GDataView();
            $data->addCol("string","callstatus");

            $campaign_data = array("phone_codes" => array(), "numbers" => array());
            foreach ($client->account->incoming_phone_numbers as $number) {
                if ($number->friendly_name == $name) {
                    $this_phone_number = $db->format_phone_db($number->phone_number);
                    foreach ($numbers as $number2) {
                        if (@$number2->number == $this_phone_number) {
                            $campaign_data["numbers"][] = $this_phone_number;
                            break;
                        }
                    }
                }
            }

            $data->addcol("number", "Calls");

            $phone_codes_set = array();
            if( is_array($calls) || is_object($calls) )
                foreach($calls as $call)
                {
                    $callTo = $outgoing ? $db->format_phone_db($call['CallFrom']) : $db->format_phone_db($call['CallTo']);

                    if (in_array($callTo, $campaign_data["numbers"])) {
                        if ($call["PhoneCode"] != 0) {
                            $phone_code = $db->getPhoneCodeName($call["PhoneCode"], $outgoing ? 2 : 1 );

                            if (!isset($phone_codes_set[$phone_code])) {
                                $data->addcol("number", $phone_code);
                                $phone_codes_set[$phone_code] = 1;
                            }
                            else {
                                $phone_codes_set[$phone_code] = $phone_codes_set[$phone_code] + 1;
                            }
                        }
                    }
                }
            $campaign_name = $name;
            foreach ($phone_codes_set as $name=>$count) {
                if ($count > 0) {
                    $data->addRow(array("c"=>array(
                        array("f"=>$name),array("v"=>$count)
                    )));
                }
            }
            $campaign_graphs[] = array("name"=>$campaign_name, "data"=>$data->getData());
        }

        die(json_encode(array("result"=>"success","data"=>$data2->getData(),"data2"=>$overview_,"data3"=>$campaign_graphs)));

    }

    static function handle_incoming_email()
    {
        $db = new DB();
        global $TIMEZONE;

        foreach ($_REQUEST["emails"] as $sent_email) {
            $stmt = $db->customExecute("SELECT * FROM email_tracking_settings WHERE emails LIKE ?");
            $stmt->execute(array('%"'.$sent_email['to_email'].'"%'));
            $tracking_settings = $stmt->fetch(PDO::FETCH_OBJ);
            $clients = json_decode($tracking_settings->clients);
            $emails = json_decode($tracking_settings->emails);
            $client_email_tags = json_decode($tracking_settings->client_email_tags);

            $clients_to_add_to = array();
            if ($tracking_settings->distribution == "all" || count($clients) == 1) {
                foreach ($clients as $client) {
                    $clients_to_add_to[] = $client->id;
                }
            }
            elseif ($tracking_settings->distribution == "even") {
                $emails_clients_tree = array();

                foreach ($emails as $email) {
                    $emails_clients_tree[$email] = array();
                }

                foreach ($clients as $client) {
                    if (isset($client_email_tags->{$client->id})) {
                        foreach ($emails as $email) {
                            if (isset($client_email_tags->{$client->id}->{$email})) {
                                if (count($client_email_tags->{$client->id}->{$email}) > 0)
                                    $emails_clients_tree[$email][] = $client;
                            }
                        }
                    }
                }

                $stmt2 = $db->customExecute("SELECT client_id FROM email_tracking_log WHERE to_email = ? AND invoiced = 0 ORDER BY id DESC LIMIT 1");
                $stmt2->execute(array($sent_email['to_email']));
                $last_email_sent = $stmt2->fetch();

                if ($last_email_sent) {
                    $last_email_client_id = $last_email_sent['client_id'];

                    $next_client_id = null;
                    $grab_the_next_one = false;

                    foreach ($emails_clients_tree[$sent_email['to_email']] as $client) {
                        if ($grab_the_next_one) {
                            $next_client_id = $client->id;
                            break;
                        }

                        if ($client->id == $last_email_client_id)
                            $grab_the_next_one = true;
                    }

                    if ($next_client_id == null)
                        $next_client_id = $emails_clients_tree[$sent_email['to_email']][0]->id;
                }
                else {
                    $next_client_id = $emails_clients_tree[$sent_email['to_email']][0]->id;
                }

                $clients_to_add_to[] = $next_client_id;
            }

            foreach ($clients_to_add_to as $client_id) {
                $set_date = new DateTime($sent_email["email_date"]);
                $set_date->setTimezone(new DateTimeZone($TIMEZONE));
                $sent_email["email_date"] = $set_date->format('Y:m:d H:i:s');

                $billing_tags = array();

                if ($tracking_settings->billing_tag == "auto") {
                    $client_tags = $client_email_tags->{$client_id}->{$sent_email["to_email"]};

                    $found_but_not = false;
                    foreach ($client_tags as $tag) {
                        if ($tag->rule == "None") {
                            if (strpos(strtolower($sent_email["body"]), strtolower($tag->keyword)) !== false) {
                                $billing_tags[] = array(
                                    "tag" => $tag->tag,
                                    "cost" => $tag->cost
                                );
                            }
                        }
                        elseif ($tag->rule == "But Not") {
                            if (strpos(strtolower($sent_email["body"]), strtolower($tag->keyword)) !== false) {
                                $found_but_not = true;
                            }
                        }
                    }

                    if ($found_but_not)
                        $billing_tags = array();
                }

                $stmt3 = $db->customExecute("INSERT INTO email_tracking_log(company_id, client_id, to_email, from_email, message_id, email_date, subject, body, billing_tags) 
                                VALUES(?,?,?,?,?,?,?,?,?)");
                $stmt3->execute(array($tracking_settings->company_id, $client_id, $sent_email["to_email"], $sent_email["from_email"], $sent_email["message_id"], $sent_email["email_date"], $sent_email["subject"], $sent_email["body"], json_encode($billing_tags)));
            }
        }

        exit();
    }
}


class APIUtil {
    static function generalAuth($token)
    {
        $db = new DB();

        global $lcl;

        if($lcl<2)
            die(json_encode(array("error"=>"NO_ACCESS_TO_APP")));

        $token = explode("_|_",base64_decode($token));

        if(!isset($token[0]) || !isset($token[1]))
            die(json_encode(array("error"=>"LOGIN_FAILED")));

        $username     = $token[0];
        $password_md5 = $token[1];
        $app          = @$token[2];

        $status = $db->authUserAPI($username,$password_md5);

        if(!$status[0])
        {
            die(json_encode(array("error"=>"LOGIN_FAILED")));
        }else{
            $access = false;
            switch($app)
            {
                default: // Assumed iPhone App
                    $access = $db->checkAddonAccess($status[1],10001);
                    break;
                case "android":
                    $access = $db->checkAddonAccess($status[1],10004);
                    break;
                case "click2call":
                    $access = $db->checkAddonAccess($status[1],10002);
                    break;
                case "browserapp":
                    $access = $db->checkAddonAccess($status[1],10005);
                    break;
                case "browsersoftphone":
                    $access = $db->checkAddonAccess($status[1],10003);
                    break;
                case "LGP":
                    $access = $db->checkAddonAccess($status[1],10008);
                    break;
            }

            if($access)
                return $status[1];
            else
                die(json_encode(array("error"=>"NO_ACCESS_TO_APP")));
        }
    }
}

if (!function_exists('get_twilio_application_sid')):

    /**
     * Retrieving the APPSID by either creating a new application in twilio
     * OR by retrieving it from app created earlier
     * @param string $accountSid Twilio account sid
     * @param string $auth_token Twilio auth token
     * @return string
     */
    function get_twilio_application_sid($accountSid, $auth_token, $twilio_app_name, $userid) {
        // Your Account Sid and Auth Token from twilio.com/user/account
        $client = new Services_Twilio($accountSid, $auth_token);

        // Loop over the list of apps and echo a property for each one
        //Setting the appsid to null by default
        $appsid = '';

        $mainapp = "";

        //Checking and retrieving the APP side from twilio account
        //If APP is already created
        foreach ($client->account->applications->getIterator(0, 50, array(
            "FriendlyName" => $twilio_app_name
        )) as $app
        ) {
            //Retrieving the APP sid
            $mainapp = $app;
            $voiceurl = $app->voice_url;
            $appsid = $app->sid;
        }

        $exploded = explode("/api", dirname(s8_get_current_webpage_uri()));
        $site_url = trim($exploded[0]);

        //If APPSID is not set indicating that app is not already
        if (!isset($appsid) || empty($appsid)) {

            $callbackurl = $site_url . "/include/twilio_dial_number.php?userid=" . $userid . "&app=browsersoftphone";

            $callbackurl = str_replace("/i/", "/", $site_url . "/include/twilio_dial_number.php?userid=" . $userid . "&app=browsersoftphone");

            //Creating a new APP in twilio account with voice URL and VOICE method
            $app = $client->account->applications->create($twilio_app_name, array(
                "VoiceUrl" => $callbackurl,
                "VoiceMethod" => "POST"
            ));

            //Retrieving the APP sid.
            $appsid = $app->sid;
        } else {
            $callbackurl = $site_url . "/include/twilio_dial_number.php?userid=" . $userid . "&app=browsersoftphone";
            $callbackurl = str_replace("/i/", "/", $site_url . "/include/twilio_dial_number.php?userid=" . $userid . "&app=browsersoftphone");

            if ($voiceurl != $callbackurl)
                $mainapp->update(array(
                    'VoiceUrl' => $callbackurl
                ));
        }

        //Returning the APP sid to calle rof this function.
        return $appsid;
    }

endif;

class GDataView
{
    private $columns;
    private $rows;

    public function __construct()
    {
        $this->columns = array();
        $this->rows    = array();
    }

    public function addCol($type, $label="", $pattern = "", $id = "")
    {
        $col = (object)array();

        if($id!="")
            $col->id      = $id;
        if($label!="")
            $col->label   = $label;
        if($pattern!="")
            $col->pattern = $pattern;
        $col->type    = $type;

        $this->columns[] = $col;
    }

    public function addRow($cells)
    {
        $this->rows[] = $cells;
    }

    public function getData()
    {
        $data = (object)array();

        $data->cols = $this->columns;
        $data->rows = $this->rows;

        return $data;
    }
}


run();