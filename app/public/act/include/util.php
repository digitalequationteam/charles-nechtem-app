<?php
	require_once "twilio.php";
    require_once "Services/Twilio.php";
	require_once "db.php";
	require_once "config.php";


    $product_version = 16;
    $product_db_version = 51;

    $version_str = "2.6.4";
    $build_number="[[build_number]]";

    if(!isset($RECORDINGS))
        $RECORDINGS = true;

    // Adding script execution timeout. To prevent deadlock.
    // 300 seconds (5 minutes)
    @set_time_limit(300);

    if(isset($_SERVER['REAL_DOCUMENT_ROOT']))
        $_SERVER['DOCUMENT_ROOT'] = $_SERVER["REAL_DOCUMENT_ROOT"];

    $url_folder = substr(substr($_SERVER["REQUEST_URI"],1), 0,
        strpos(substr($_SERVER["REQUEST_URI"],1), "/"));

    if(stristr($_SERVER['DOCUMENT_ROOT'], $url_folder."/api")!==FALSE)
        $_SERVER['DOCUMENT_ROOT'] = str_replace("/".$url_folder."/api","",$_SERVER['DOCUMENT_ROOT']);

    if(!file_exists($_SERVER['DOCUMENT_ROOT']."/".$url_folder."/api/.htaccess"))
    {
        if($url_folder!=""){
            $folder = "/".$url_folder;
        }else{
            $folder = "";
        }

        $data = <<<TEXT
<IfModule mod_rewrite.c>
  Options +FollowSymlinks
  Options +Indexes
  RewriteEngine on

  # if your app is in a subfolder
  RewriteBase $folder/api/

  # test string is a valid files
  RewriteCond %{SCRIPT_FILENAME} !-f
  # test string is a valid directory
  RewriteCond %{SCRIPT_FILENAME} !-d

  RewriteRule ^(.*)$   index.php?uri=/$1    [NC,L,QSA]
  # with QSA flag (query string append),
  # forces the rewrite engine to append a query string part of the
  # substitution string to the existing string, instead of replacing it.
</IfModule>
TEXT;


        @file_put_contents($_SERVER['DOCUMENT_ROOT']."/".$url_folder."/api/.htaccess", $data);
    }

    if(!file_exists($_SERVER['DOCUMENT_ROOT']."/".$url_folder."/recording/.htaccess"))
    {
        if($url_folder!=""){
            $folder = "/".$url_folder;
        }else{
            $folder = "";
        }

        $data = <<<TEXT
<IfModule mod_rewrite.c>
  Options +FollowSymlinks
  Options +Indexes
  RewriteEngine on

  # if your app is in a subfolder
  RewriteBase $folder/recording/

  # test string is a valid files
  RewriteCond %{SCRIPT_FILENAME} !-f
  # test string is a valid directory
  RewriteCond %{SCRIPT_FILENAME} !-d

  RewriteRule ^(.*)$   http://api.twilio.com/$1    [L]
  # with QSA flag (query string append),
  # forces the rewrite engine to append a query string part of the
  # substitution string to the existing string, instead of replacing it.
</IfModule>
TEXT;


        @file_put_contents($_SERVER['DOCUMENT_ROOT']."/".$url_folder."/recording/.htaccess", $data);
    }

	// API Version for Twilio
	$ApiVersion = "2010-04-01";

    //Check Database Version
    $db = new DB();
    $update_needed = $db->getVar("update_needed");
    $db_ver = $db->getDatabaseVersion();
    if($db->getVar("product_version")!=$product_version)
        $db->setVar("product_version",$product_version);
    $lcl = $db_ver[1];
    $db_ver = $db_ver[0];

    if (!isset($ignore_db_check)) $ignore_db_check = false;

    if(!$db_ver || $db_ver < $product_db_version && !$ignore_db_check)
    {
        require("static/db-needs-upd.php");
        die();
    }
    global $AccountSid, $AuthToken;
    if($AccountSid=="" || $AuthToken=="")
    {
        require("static/twilio-keys-missing.php");
        die();
    }
    unset($RECORDINGS);
    $VERSION = $db->getVar("act_version");
    if($VERSION!=$version_str)
    {
        $VERSION=$version_str;
        $db->setVar("act_version",$version_str);
    }
    $RECORDINGS = $db->getVar("global_recordings")=='true'? true: false;

    $page = @reset(explode(".",substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1)));
    $act_prefix = "Call Tracking - ";
    switch($page)
    {
        case "login":
            $title=$act_prefix."Login";
            break;
        case "index":
            $title =$act_prefix."Dashboard";
            break;
        case "companies":
            $title =$act_prefix."Companies";
            break;
        case "manage_companies":
            $title =$act_prefix."Manage Companies";
            break;
        case "manage_users":
            $title =$act_prefix."Manage Users";
            break;
        case "manage_blacklists":
            $title =$act_prefix."Manage Blacklists";
            break;
        case "setup_phone_numbers":
            $title =$act_prefix."Manage Phone Numbers";
            break;
        case "manage_pc_templates":
            $title =$act_prefix."Manage Phone Code Templates";
            break;
        case "call_report":
            $title=$act_prefix."Call Report";
            break;
        case "call_detail":
            $title=$act_prefix."Call Details";
            break;
        case "company_report":
            $title=$act_prefix."Company Report";
            break;
        case "act_settings":
            $title=$act_prefix."ACT Settings";
            break;
        case "manage_exceptions":
            $title=$act_prefix."Manage Exceptions";
            break;
        case "edit_call_flow":
            $title=$act_prefix."Edit Call Flow";
            break;
        case "checkversion":
            $title=$act_prefix."Authenticate ACT";
            break;
        case "manage_email":
            $title=$act_prefix."Manage Email";
            break;
        case "update-core":
            $title=$act_prefix."Update ACT";
            break;
        case "ad_ad_add_campaign":
            $title=$act_prefix."Auto Dialer Add Campaign";
            break;
        case "ad_ad_campaigns":
            $title=$act_prefix."Auto Dialer Campaigns";
            break;
        case "ad_ad_dialing_details":
            $title=$act_prefix."Auto Dialer";
            break;
        case "ad_ad_edit_campaign":
            $title=$act_prefix."Auto Dialer Edit Campaign";
            break;
        case "ad_ad_logs":
            $title=$act_prefix."Auto Dialer Call Logs";
            break;
        case "ad_vb_add_campaign":
            $title=$act_prefix."Messages: Add Campaign";
            break;
        case "ad_vb_campaigns":
            $title=$act_prefix."Messages: Campaigns";
            break;
        case "ad_vb_edit_campaign":
            $title=$act_prefix."Messages: Edit Campaign";
            break;
        case "ad_vb_logs":
            $title=$act_prefix."Messages: Call Logs";
            break;
        case "ad_contactlist_add":
            $title=$act_prefix."Messages: Add List";
            break;
        case "ad_contactlist_edit":
            $title=$act_prefix."Messages: Edit List";
            break;
        case "ad_contactlist_log":
            $title=$act_prefix."Contact Manager";
            break;
        case "call_report_outgoing":
            $title=$act_prefix."Outgoing Call Report";
            break;
        case "ad_vb_sequences_edit":
            $title=$act_prefix;
            break;
        case "keyword_default_locked_number":
            $title=$act_prefix."Keyword Locked Number";
            break;
        case "keyword_generated_call_report":
            $title=$act_prefix."Keyword Generated Call Report";
            break;
        case "keyword_locked_number":
            $title=$act_prefix."Keyword Locked Number";
            break;
        case "keyword_not_generated_call_report":
            $title=$act_prefix."Keyword Not Generated Call Report";
            break;
        case "keyword_report":
            $title=$act_prefix."Keyword Report";
            break;
        case "pool_detail":
            $title=$act_prefix."Pool Details";
            break;
        case "pool_edit":
            $title=$act_prefix."Edit Pool";
            break;
        case "pool_number":
            $title=$act_prefix."Pooling Numbers";
            break;
        case "email_tracking_setup":
            $title=$act_prefix."Email Lead Tracking";
            break;
        case "email_tracking_stats":
            $title=$act_prefix."Email Tracking Stats";
            break;
    }

    if (!function_exists('s8_get_current_webpage_uri')) {
        function s8_get_current_webpage_uri() {
            $pageURL = 'http';
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            return $pageURL;
        }
    }
    
	function format_phone($phone, $twilio_resp=false)
	{
        if(substr($phone,0,3)=="+44" && $twilio_resp==false)
        {
            $phone = substr($phone,3,(strlen($phone)-3));
            return "44".preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
        }else{
            $phone = preg_replace("/[^0-9]/", "", $phone);

            if(strlen($phone) == 7)
                return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
            elseif(strlen($phone) == 10)
                return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
            else
                return $phone;
        }
	}

    class Addons {
        private static $addons = array(
            "ACT_MOBILE_APP"          => array("id"=>10001,"name"=>"ACT iPhone App"),
            "ACT_ANDROID_APP"         => array("id"=>10004,"name"=>"ACT Android App"),
            "ACT_CLICK2CALL"          => array("id"=>10002,"name"=>"ACT Click 2 Call"),
            "ACT_BROWSER_SOFTPHONE"   => array("id"=>10003,"name"=>"Browser Softphone"),
            "ACT_BROWSER_APPLET"      => array("id"=>10005,"name"=>"ACT Browser App"),
            "ACT_AUTO_DIALER"         => array("id"=>10006,"name"=>"Auto Dialer"),
            "ACT_VOICE_BROADCAST"     => array("id"=>10007,"name"=>"Voice Broadcast"),
            "ACT_CONTACTS"            => array("id"=>10008,"name"=>"Manage Contacts"),
            "ACT_PURCHASE_NUMBERS"    => array("id"=>10009,"name"=>"Purchase Numbers"),
            "ACT_CUSTOMIZE_CALL_FLOW" => array("id"=>10010,"name"=>"Customize Call Flow")
        );


        public static function getAddonList()
        {
            return Addons::$addons;
        }
    }

    class CallFlow {
        private  static $call_flows = array(
            array("name"=>"None",             "desc"=>"Use no call flow. This will Reject calls."),
            array("name"=>"Ring 1 Number",    "desc"=>"Ring directly to one number."),
            array("name"=>"Round Robin",      "desc"=>"Calls will rotate to multiple ring-to numbers based on the last number called. The first call goes to the first number listed, then the next call will be sent to the second number on the list."),
            array("name"=>"Multiple Numbers", "desc"=>"This will ring to all the numbers in the list. And whoever answers first takes the call."),
            array("name"=>"Advanced",         "desc"=>"Build your own advanced call flow using: Greetings, Menus, Ring 1 Number, Round Robin, Multiple Numbers, SMS, Timeouts, Voicemail, Specify Open & Close Hours And More."),
            array("name"=>"Voicemail",        "desc"=>"Voicemail systems are designed to convey a caller's recorded audio message to a recipient.", "disabled"=>true),
            array("name"=>"SIP",              "desc"=>"Teach an old box new tricks: receive calls from Twilio at your SIP-enabled endpoint.", "disabled"=>true)
        );

        public static function getCallFlows()
        {
            return CallFlow::$call_flows;
        }
        public static function getCallFlow($id)
        {
            return CallFlow::$call_flows[$id];
        }
        public static function getCallFlowName($id)
        {
            $call_flow = CallFlow::$call_flows[$id];
            return $call_flow['name'];
        }
    }

	class Util {
        const STANDARD_LOG_DATE_FORMAT = "D n\/d Y g\:iA";
        const STANDARD_MYSQL_DATE_FORMAT = "Y-m-d H:i:s";
        public static function convertToLocalTZ($formatteddate){
            global $TIMEZONE;
            $date = new DateTime($formatteddate,new DateTimeZone("UTC"));
            $date->setTimezone(new DateTimeZone($TIMEZONE));
            return $date;
        }
        public static function rand_passwd( $length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ) {
            return substr( str_shuffle( $chars ), 0, $length );
        }
        public static function get_all_twilio_numbers() {
            global $AccountSid, $AuthToken;
            if(isset($_SESSION)){
                if(isset($_SESSION['twilio_numbers_last_upd']) && ($_SESSION['twilio_numbers_last_upd']+3600) >= strtotime("now") ){
                    return $_SESSION['twilio_numbers'];
                }else{
                    $twilio_numbers=array();
                    $client = new Services_Twilio($AccountSid,$AuthToken);
                    foreach ($client->account->incoming_phone_numbers->getIterator(0, 50, array()) as $number)
                    {
                        $twilio_numbers[format_phone($number->phone_number,true)]=$number->friendly_name;
                    }
                    $_SESSION['twilio_numbers'] = $twilio_numbers;
                    $_SESSION['twilio_numbers_last_upd'] = strtotime("now");
                    return $_SESSION['twilio_numbers'];
                }
            }else{
                $twilio_numbers=array();
                $client = new Services_Twilio($AccountSid,$AuthToken);
                foreach ($client->account->incoming_phone_numbers->getIterator(0, 50, array()) as $number)
                {
                    $twilio_numbers[format_phone($number->phone_number,true)]=$number->friendly_name;
                }
                return $twilio_numbers;
            }
        }

        public static function get_all_calls($start_date,$end_date){
            global $AccountSid, $AuthToken;
            $calls = array();
            $client = new Services_Twilio($AccountSid, $AuthToken);
            $start = new DateTime("@".$start_date);
            $end = new DateTime("@".$end_date);
            //die($start->format("Y-m-d")." - ".$end->format("Y-m-d"));
            foreach ($client->account->calls->getIterator(0, 50, array(
                'StartTime>' => $start->format("Y-m-d"),
                'StartTime<' => $end->format("Y-m-d")
            )) as $call) {
                //echo $call->date_created;
                $obj = new ArrayObject();
                $obj->Sid = $call->sid;
                $obj->Status = $call->status;
                $obj->ParentCallSid = $call->parent_call_sid;
                $obj->To = $call->to;
                $obj->From = $call->from;
                $obj->StartTime = $call->start_time;
                $obj->Price = $call->price;
                $obj->Duration = $call->duration;
                $calls[] = $obj;
            }

            return $calls;
        }

        public static function limit_text($string, $limit, $pad="...")
        {
            $string = strip_tags($string);

            if (strlen($string) > $limit) {
                $string = substr($string, 0, $limit).$pad;
            }
            return $string;
        }

        public static function generateFlashAudioPlayer($url, $size='sm', $callback = "")
        {
            $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
            $ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
            $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
            $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
            $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
            $palm = strpos($_SERVER['HTTP_USER_AGENT'],"PalmOS");

            $show_mobile = ($iphone || $ipod || $android || $berry || $ipad || $palmpre || $palm) ? true : false;
            //$show_mobile = false;
            if ($show_mobile)
            {
                switch($size)
                {
                    case "sm":
                        $width=60;
                        break;
                    case "lg":
                        $width=400;
                        break;
                }
                ?><audio src="<?php echo $url; ?>" controls preload="none" style="width:<?php echo $width; ?>px;"></audio><?php
            }else{
                $id = uniqid("",true);
                $id = str_replace(".","",$id);
                ?>
            <div id="jquery_jplayer_<?php echo $id; ?>" class="jp-jplayer"></div>

            <div class="jp-container_<?php echo $id; ?>"<?php if($size=="lg") echo " style='display:inline-block; width:360px;'"; ?>>
                <div class="jp-audio"<?php if($size=="sm") echo " style='width:160px;'"; ?>>
                    <div class="jp-type-single">
                        <div id="jp_interface_1" class="jp-interface">
                            <ul class="jp-controls">
                                <li style="background:none;"><a href="#" class="jp-play" tabindex="1">play</a></li>
                                <li style="background:none;"><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                                <li style="background:none;"><a href="#" class="jp-mute"<?php if($size=="sm") echo " style='left:133px;'"; ?> tabindex="1">mute</a></li>
                                <li style="background:none;"><a href="#" class="jp-unmute"<?php if($size=="sm") echo " style='left:133px;'"; ?> tabindex="1">unmute</a></li>
                            </ul>
                            <div class="jp-progress-container"<?php if($size=="sm") echo " style='width:65px;'"; ?>>
                                <div class="jp-progress"<?php if($size=="sm") echo " style='width:60px;'"; ?>>
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-volume-bar-container"<?php if($size=="sm") echo " style='display:none;'"; ?>>
                                <div class="jp-volume-bar">
                                    <div class="jp-volume-bar-value"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                $firefox = strpos($_SERVER['HTTP_USER_AGENT'],"Firefox");
                $ie = strpos($_SERVER['HTTP_USER_AGENT'],"Trident");
                if ($ie)
                    $extension = "mp3";
                else
                    $extension = "wav";
                ?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        setTimeout(function() {
                            $("#jquery_jplayer_<?php echo $id; ?>").jPlayer({
                                ready: function () {
                                    $(this).jPlayer("setMedia", {
                                        //mp3: "<?php echo $url.".wav"; ?>",
                                        <?php echo $extension; ?>: "<?php echo $url.".".$extension; ?>"
                                    });
                                },
                                play: function() { // To avoid both jPlayers playing together.
                                    $(this).jPlayer("pauseOthers");
                                    <?php echo $callback; ?>
                                },
                                swfPath: "player",
                                supplied: "<?php echo $extension; ?>",
                                volume: 1,
                                preload: "none",
                                wmode: "window",
                                cssSelectorAncestor: ".jp-container_<?php echo $id; ?>"
                            });
                        }, 1000);
                    });

                </script>
                <?php

            }
        }

		public static function format_phone_us($phone = '', $convert = true, $trim = true)
		{
			if (empty($phone)) {
				return false;
			}

			$phone = preg_replace("/[^0-9A-Za-z]/", "", $phone);
			$OriginalPhone = $phone;

			if ($trim == true && strlen($phone)>11) {
				$phone = substr($phone, 0, 11);
			}

			if ($convert == true && !is_numeric($phone)) {
				$replace = array('2'=>array('a','b','c'),
								 '3'=>array('d','e','f'),
								 '4'=>array('g','h','i'),
								 '5'=>array('j','k','l'),
								 '6'=>array('m','n','o'),
								 '7'=>array('p','q','r','s'),
								 '8'=>array('t','u','v'),
								 '9'=>array('w','x','y','z'));

				foreach($replace as $digit=>$letters) {
					$phone = str_ireplace($letters, $digit, $phone);
				}
			}

			$length = strlen($phone);
			switch ($length) {
				case 7:
					// Format: xxx-xxxx
					return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1-$2", $phone);
				case 10:
					// Format: (xxx) xxx-xxxx
					return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "($1) $2-$3", $phone);
				case 11:
					// Format: x(xxx) xxx-xxxx
					return preg_replace("/([0-9a-zA-Z]{1})([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1($2) $3-$4", $phone);
				default:
					// Return original phone if not 7, 10 or 11 digits long
					return $OriginalPhone;
			}
		}

		public static function yahoo_geo($location) {
			global $yahoo_geo_api_key;
            $db =  new DB();
			$q = 'http://gws2.maps.yahoo.com/findlocation?pf=1&locale=en_US&flags=J&offset=1&gflags=&q='.urlencode(trim($location)).'&start=0&count=1';

            $json_data = $db->curlGetData($q);

            if($json_data!="")
            {
                if($json = json_decode($json_data))
                {
                    $ret = array();
                    if(count($json->ResultSet->Results) > 0)
                    {
                        if(!is_array($json->ResultSet->Results)){
                            $json = $json->ResultSet->Results;
                        }else{
                            $json = $json->ResultSet->Results[0];
                        }

                        $ret['longitude'] = $json->longitude;
                        $ret['latitude']  = $json->latitude;
                    }else{
                        return false;
                    }

                    return $ret;
                }
            }
            return false;
		}

        public static function getCompanyTitle($padding_right = "")
        {
            global $SITE_NAME;
            if($SITE_NAME==""){
                return "";
            }else{
                return $SITE_NAME.$padding_right;
            }
        }

        public static function isNumberIntl($num)
        {
            if(substr($num,0,2)=="+1")
                return false;
            else
                return true;
        }

		public static function formatTime($secs) {
			return str_pad(floor($secs/3600),2,"0",STR_PAD_LEFT).":".
				str_pad(floor(($secs%3600)/60),2,"0",STR_PAD_LEFT).":".
				str_pad($secs%60,2,"0",STR_PAD_LEFT);
		}

        public static function percent($num_amount, $num_total) {
            if($num_amount==0 || $num_total == 0)
                return 0;
            $count1 = $num_amount / $num_total;
            $count2 = $count1 * 100;
            $count = number_format($count2, 0);
            return $count;
        }

        public static function isValidEmail($email){
            return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
        }

        public static function getBrowser( $user_agent ) {

            $browser        =   "Unknown";

            $browser_array  =   array(
                                    '/msie/i'       =>  'Internet Explorer',
                                    '/trident/i'    =>  'Internet Explorer',
                                    '/firefox/i'    =>  'Firefox',
                                    '/safari/i'     =>  'Safari',
                                    '/chrome/i'     =>  'Chrome',
                                    '/opera/i'      =>  'Opera',
                                    '/netscape/i'   =>  'Netscape',
                                    '/maxthon/i'    =>  'Maxthon',
                                    '/konqueror/i'  =>  'Konqueror',
                                    '/mobile/i'     =>  'Handheld Browser'
                                );

            foreach ($browser_array as $regex => $value) {

                if (preg_match($regex, $user_agent)) {
                    $browser    =   $value;
                }

            }

            return $browser;

        }



        public static function getOS( $user_agent ) {

            $os_platform    =   "Unknown";

            $os_array       =   array(
                                    '/windows nt 6.2/i'     =>  'Microsoft Windows 8',
                                    '/windows nt 6.1/i'     =>  'Microsoft Windows 7',
                                    '/windows nt 6.0/i'     =>  'Microsoft Windows Vista',
                                    '/windows nt 5.2/i'     =>  'Microsoft Windows Server 2003/XP x64',
                                    '/windows nt 5.1/i'     =>  'Microsoft Windows XP',
                                    '/windows xp/i'         =>  'Microsoft Windows XP',
                                    '/windows nt 5.0/i'     =>  'Microsoft Windows 2000',
                                    '/windows me/i'         =>  'Microsoft Windows ME',
                                    '/win98/i'              =>  'Microsoft Windows 98',
                                    '/win95/i'              =>  'Microsoft Windows 95',
                                    '/win16/i'              =>  'Microsoft Windows 3.11',
                                    '/macintosh|mac os x/i' =>  'Mac OS X',
                                    '/mac_powerpc/i'        =>  'Mac OS 9',
                                    '/linux/i'              =>  'Linux',
                                    '/ubuntu/i'             =>  'Ubuntu',
                                    '/iphone/i'             =>  'iPhone',
                                    '/ipod/i'               =>  'iPod',
                                    '/ipad/i'               =>  'iPad',
                                    '/android/i'            =>  'Android',
                                    '/blackberry/i'         =>  'BlackBerry',
                                    '/webos/i'              =>  'Mobile'
                                );

            foreach ($os_array as $regex => $value) {

                if (preg_match($regex, $user_agent)) {
                    $os_platform    =   $value;
                }

            }

            return $os_platform;

        }

        public static function getTwilioVoices($selected = "man") {
            $voices = array(
                "man" => "Man",
                "woman" => "Woman",
                "alice" => "Alice"
            );

            $voices_txt = "";

            foreach ($voices as $key => $value) {
                $voices_txt .= '<option value="'.$key.'"'.(($selected == $key) ? ' selected="selected"' : "").'>'.$value.'</option>';
            }

            return $voices_txt;
        }

        public static function getTwilioLanguages($voice = "", $selected = "") {

            if (empty($selected)) {
                if ($voice == "man" || $voice == "") {
                    $selected = "en|M";
                }
                elseif ($voice == "woman") {
                    $selected = "en|W";
                }
                elseif ($voice == "alice") {
                    $selected = "en-US";
                }
            }

            $languages = array(
                "man" => array(
                    "en|M" => "English",
                    "en-GB|M" => "English, UK",
                    "es|M" => "Spanish",
                    "fr|M" => "French",
                    "de|M" => "German",
                    "it|M" => "Italian"
                ),
                "woman" => array(
                    "en|W" => "English",
                    "en-GB|W" => "English, UK",
                    "es|W" => "Spanish",
                    "fr|W" => "French",
                    "de|W" => "German",
                    "it|W" => "Italian"
                ),
                "alice" => array(
                    "en-US" => "English, United States",
                    "da-DK" => "Danish, Denmark",
                    "de-DE" => "German, Germany",
                    "en-AU" => "English, Australia",
                    "en-CA" => "English, Canada",
                    "en-GB" => "English, UK",
                    "en-IN" => "English, India",
                    "ca-ES" => "Catalan, Spain",
                    "es-ES" => "Spanish, Spain",
                    "es-MX" => "Spanish, Mexico",
                    "fi-FI" => "Finnish, Finland",
                    "fr-CA" => "French, Canada",
                    "fr-FR" => "French, France",
                    "it-IT" => "Italian, Italy",
                    "ja-JP" => "Japanese, Japan",
                    "ko-KR" => "Korean, Korea",
                    "nb-NO" => "Norwegian, Norway",
                    "nl-NL" => "Dutch, Netherlands",
                    "pl-PL" => "Polish-Poland",
                    "pt-BR" => "Portuguese, Brazil",
                    "pt-PT" => "Portuguese, Portugal",
                    "ru-RU" => "Russian, Russia",
                    "sv-SE" => "Swedish, Sweden",
                    "zh-CN" => "Chinese (Mandarin)",
                    "zh-HK" => "Chinese (Cantonese)",
                    "zh-TW" => "Chinese (Taiwanese Mandarin)"
                ),
            );
    
            $languages_txt = "";
            
            foreach ($languages as $voice => $languages_list) {
                foreach ($languages_list as $key => $value) {
                    $languages_txt .= '<option value="'.$key.'"'.(($selected == $key) ? ' selected="selected"' : "").' data-voice="'.$voice.'">'.$value.'</option>';
                }
            }

            return $languages_txt;

        }
        public static function print_r($var,$return = false){
            global $INDEV;
            if($INDEV===true)
                return print_r($var,$return);
            else
                return "";
        }
        public static function error_log($msg){
            global $INDEV;
            if($INDEV===true)
                return error_log($msg);
            else
                return "";
        }

        public static function escapeString($value){
            return stripslashes(str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array("", '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $value));
        }

        public static function timeAgo($time)
        {
           $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
           $lengths = array("60","60","24","7","4.35","12","10");

           $now = strtotime("now");

           $difference     = $now - $time;
           $tense         = "ago";

           for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
               $difference /= $lengths[$j];
           }

           $difference = round($difference);

           if($difference != 1) {
               $periods[$j].= "s";
           }

           return "$difference $periods[$j] ago ";
        }

        public static function genTTSAudioPlugin($params = array("name" => "", "content" => "", "content_type" => "", "voice" => "", "language" => "", "multiple" => "", "company_id" => 0)) {
            global $db;
            if (empty($params['multiple'])) $params['multiple'] = "";
            ?>
            <div class="settings-panel ivrMenu">
              <div class="ivrMenu">
                <div class="menu-prompt">
                  <fieldset class="ivr-Menu ivr2-input-container">
                    <input type="hidden" class="location" name="location" value="<?php echo $params['location']; ?>" />

                    <input type="hidden" class="content" name="<?php echo $params['name']; ?>content<?php echo $params['multiple']; ?>" value="<?php echo $params['content']; ?>" />
                    <input type="hidden" class="type" name="<?php echo $params['name']; ?>type<?php echo $params['multiple']; ?>" value="<?php echo $params['content_type']; ?>" />
                    <input type="hidden" class="voice" name="<?php echo $params['name']; ?>voice<?php echo $params['multiple']; ?>" value="<?php echo $params['voice']; ?>" />
                    <input type="hidden" class="language" name="<?php echo $params['name']; ?>language<?php echo $params['multiple']; ?>" value="<?php echo $params['language']; ?>" />
                    <div class="ivr-Menu-selector" style="display: block">
                      <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                        <?php if ($params['content_type'] == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="TTSAP.removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                        <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($params['content_type'] == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="TTSAP.openEditor(this);"> <span class="title">Text To Speech</span></a> </div>
                      </div>
                      <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                        <?php if ($params['content_type'] == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="TTSAP.removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                        <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($params['content_type'] == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="TTSAP.openEditor(this);" > <span class="title">Upload MP3</span></a></div>
                      </div>
                      <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                        <?php if ($params['content_type'] == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="TTSAP.removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                        <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($params['content_type'] == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="TTSAP.openEditor(this);" > <span class="title">Enter MP3 URL</span></a></div>
                      </div>
                      <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                        <?php if ($params['content_type'] == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="TTSAP.removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                        <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($params['content_type'] == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="TTSAP.openEditor(this);" > <span class="title">Record Audio</span></a></div>
                      </div>
                    </div>
                    <div class="ivr-Menu-editor ">
                      <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                        <div class="ivr-Menu-read-text" style="display: none;">
                          <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="TTSAP.close(this);"> <span class="replace">close</span> </a> </div>
                          <br>
                          <div>
                            <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                              <label class="field-label">
                                <form action="" method="post" onsubmit="return false;">
                                  <textarea class="voicemail-text" name="text" id="text" style="width: 97%;"><?php echo (($params['content_type'] == 'Text')? $params['content']:''); ?></textarea>
                                  <label class="field-label-left" style="width: 55px;">Voice: </label>
                                  <select class="styled" id="voice" onchange="TTSAP.selectVoice(this);" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                    <?php
                                    echo Util::getTwilioVoices($params['voice']);
                                    ?>
                                  </select>

                                  <br clear="all" />

                                  <label class="field-label-left" style="width: 55px;">Dialect: </label>
                                  <select class="styled" id="language" style="display: inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                    <?php
                                    echo Util::getTwilioLanguages($params['voice'], $params['language']);
                                    ?>
                                  </select>

                                  <br clear="all" /><br />

                                  <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="TTSAP.testVoice(this);" style="margin-left: 0px; display: inline;" />

                                  <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="TTSAP.saveContent(this, 'TEXT');" style="float: right; margin-left: 0px; margin-bottom: 5px;" />
                                </form>
                              </label>
                            </fieldset>
                          </div>
                          <br>
                          <br>
                        </div>
                        <div class="ivr-audio-upload" style="display: none;">
                          <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="TTSAP.close(this);"> <span class="replace">close</span> </a> </div>
                          <div class="swfupload-container">
                            <div class="explanation"> <br>
                              <span class="title" <?php if ( $params['content_type'] != 'Audio' ) echo ' style="display:none" ' ?>  id="voicefilename">Voice to play: <strong class='uploaded_file_name'><?php echo (($params['content_type'] == 'Audio')? $params['content']:''); ?></strong></span><br />
                              <span class="title">Click to select a file:  <input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></span>
                            <span class="title" id="statusUpload">&nbsp;</span>
                            </div>
                          </div>
                        </div>
                        <div class="ivr-mp3-url" style="display: none;">
                          <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="TTSAP.close(this);"> <span class="replace">close</span> </a> </div>
                          <div class="swfupload-container">
                            <div class="explanation"> <br>           
                              
                              <span class="title">
                                <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($params['content_type'] == 'MP3_URL')? $params['content']:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 97%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="TTSAP.saveContent(this, 'MP3_URL');" />
                              </span>

                              <br /><br />

                              <span class="title" <?php if ( $params['content_type'] != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong> <?php echo (($params['content_type'] == 'MP3_URL')? $params['content']:''); ?></strong></span>
                            </div>
                          </div>
                        </div>
                        <div class="ivr-record-audio" style="display: none;">
                          <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="TTSAP.close(this);"> <span class="replace">close</span> </a> </div>
                          <div class="swfupload-container">
                            <div class="explanation"> <br>           
                              
                              Caller ID:

                              <select id="record_from" style="display: inline; width: 24%; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                <option value="">Select number</option>
                                <?php
                                $numbers = $db->getNumbersOfCompany($params['company_id']);
                                foreach ($numbers as $number) {
                                  ?>
                                    <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                  <?php
                                }
                                ?>
                              </select>

                              Your phone number:

                              <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 24%;" />

                              <br />
                              <br />

                              <div style="width: 115px; margin: auto;">
                                <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="TTSAP.recordAudio(this);" style="width: 115px; margin: 0px;" />
                              </div>

                              <br />

                              <span class="title" <?php if ( $params['content_type'] != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSaved" >
                                <?php if ($params['content_type'] == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($params['content'], 'sm'); } ?>
                              </span>

                            </div>
                          
                          
                          </div>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <?php
        }

        public static function maskRecordingURL($url) {
            $main_url = s8_get_current_webpage_uri();
            $exploded = explode("?", $main_url);
            $main_url = $exploded[0];

            if (dirname($main_url) != "http:") {
                $main_url = dirname($main_url);
            }
            $exploded = explode("/include", $main_url);
            $main_url = $exploded[0];

            $new_url = (empty($url) ? "" : str_replace("http://api.twilio.com/", $main_url."/recording/", $url));

            if ($new_url == "http://api.twilio.com.mp3" || $new_url == "http://api.twilio.com.wav") $new_url = "";

            return $new_url;
        }

        public static function sendEmail($params) {
            global $db, $SITE_NAME;

            $sent = false;

            $smtp_settings = $db->getVar("smtp_settings");

            if (empty($smtp_settings)) {
                $smtp_username = $db->getVar("smtp_username");
                $smtp_password = $db->getVar("smtp_password");

                if (!empty($smtp_username) && !empty($smtp_password)) {
                    $smtp_settings = json_encode(array(
                        "email" => $smtp_username,
                        "username" => $smtp_username,
                        "password" => $smtp_password,
                        "host" => "smtp.gmail.com",
                        "port" => 465,
                        "ssl_tls" => "SSL"
                    ));
                }
            }

            if (!empty($smtp_settings)) {
                $smtp_settings = json_decode($smtp_settings, true);

                require_once("include/class.phpmailer.php");

                try {
                    $mail = new PHPMailer(true);
                    $mail->IsSMTP();
                    $mail->SMTPDebug = 0;
                    $mail->SMTPAuth = true;
                    if (!empty($smtp_settings['ssl_tls'])) $mail->SMTPSecure = $smtp_settings['ssl_tls'];
                    $mail->Host = $smtp_settings['host'];
                    $mail->Port = $smtp_settings['port'];
                    $mail->Username = $smtp_settings['username'];
                    $mail->Password = $smtp_settings['password'];
                    $mail->SetFrom($smtp_settings['email'], $smtp_settings['name']);
                    $mail->AddReplyTo($smtp_settings['email'], $smtp_settings['name']);
                    $mail->Subject = $params['subject'];
                    $mail->Body = $params['msg'];
                    if (!empty($params['emails'])) {
                        foreach ($params['emails'] as $email) {
                            $mail->AddAddress($email);
                        }
                    }
                    $mail->IsHTML(false);

                    $sent = $mail->Send();

                    if (!$sent) {
                        $mail->Host = "ssl://".$smtp_settings['host'];
                        $sent = $mail->Send();
                    }
                }
                catch (Exception $e) {
                    $sent = false;
                }
            }

            if (!$sent) {
                $host = explode(".",$_SERVER['HTTP_HOST']);
                $host = $host[count($host)-2].".".$host[count($host)-1];

                if($SITE_NAME!="")
                    $site_name = $SITE_NAME;
                else
                    $site_name = "Call Tracking";

                $headers = 'From: '.$SITE_NAME.' <act@' . $host . ">";
                if (!empty($params['emails'])) {
                    foreach ($params['emails'] as $email) {
                        mail($email,$params['subject'],$params['msg'],$headers);
                    }
                }
            }
        }

        public static function curPageURL() {
            $pageURL = 'http';
            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}

            $pageURL .= "://";

            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
            }
            return $pageURL;
        }

        public static function getRingTone() {
            global $db;

            $option_ringtone = $db->getVar("option_ringtone");
            $intl_dialtone_url = $db->getVar("intl_dialtone_url");
           
            $intl_dialtone = false;
            if ($db->getVar("intl_dialtone") == "yes")
                $intl_dialtone = true;

            if (empty($option_ringtone) && $intl_dialtone && !empty($intl_dialtone_url)) {
                $exploded = explode("/", $intl_dialtone_url);
                $new_or = substr($exploded[count($exploded) - 1], 0, 2);

                if (!in_array($new_or, array_keys(Util::$supported_countries))) {
                    $option_ringtone = $new_or;
                }
            }

            if (empty($option_ringtone))
                $option_ringtone = "auto";

            return $option_ringtone;
        }

        //
        // parsed from https://www.twilio.com/resources/rates/international-phone-number-rates.csv
        // https://www.twilio.com/docs/api/rest/available-phone-numbers#countries
        //
        public static $supported_countries = array(
            'CA'=>array('Canada','1',1),
            'GB'=>array('United Kingdom','44',1),
            'AU'=>array('Australia','61'),
            'AT'=>array('Austria','43'),
            'BH'=>array('Bahrain','973'),
            'BE'=>array('Belgium','32'),
            'BR'=>array('Brazil','55'),
            'BG'=>array('Bulgaria','359'),
            'CY'=>array('Cyprus','357'),
            'CZ'=>array('Czech Republic','420'),
            'DK'=>array('Denmark','45'),
            'DO'=>array('Dominican Republic','1809'),
            'SV'=>array('El Salvador','503'),
            'EE'=>array('Estonia','372'),
            'FI'=>array('Finland','358'),
            'FR'=>array('France','33'),
            'GR'=>array('Greece','30'),
            'HK'=>array('Hong Kong','852'),
            'IE'=>array('Ireland','353'),
            'IL'=>array('Israel','972'),
            'IT'=>array('Italy','39'),
            'JP'=>array('Japan','81'),
            'LV'=>array('Latvia','371'),
            'LT'=>array('Lithuania','370'),
            'LU'=>array('Luxembourg','352'),
            'MT'=>array('Malta','356'),
            'MX'=>array('Mexico','52'),
            'NZ'=>array('New Zealand','64'),
            'NO'=>array('Norway','47'),
            'PE'=>array('Peru','51'),
            'PL'=>array('Poland','48'),
            'PT'=>array('Portugal','351'),
            'PR'=>array('Puerto Rico','1787'),
            'RO'=>array('Romania','40'),
            'SK'=>array('Slovakia','421'),
            'ZA'=>array('South Africa','27'),
            'ES'=>array('Spain','34'),
            'SE'=>array('Sweden','46'),
            'CH'=>array('Switzerland','41'),
            'NL'=>array('The Netherlands','31'),
            //'US'=>array('United States','1'),
        );

        public static $countriesList = array(
            'AF' => "Afghanistan",
            'AL' => "Albania",
            'DZ' => "Algeria",
            'AS' => "American Samoa",
            'AD' => "Andorra",
            'AO' => "Angola",
            'AI' => "Anguilla",
            'AQ' => "Antarctica",
            'AG' => "Antigua And Barbuda",
            'AR' => "Argentina",
            'AM' => "Armenia",
            'AW' => "Aruba",
            'AU' => "Australia",
            'AT' => "Austria",
            'AZ' => "Azerbaijan",
            'BS' => "Bahamas",
            'BH' => "Bahrain",
            'BD' => "Bangladesh",
            'BB' => "Barbados",
            'BY' => "Belarus",
            'BE' => "Belgium",
            'BZ' => "Belize",
            'BJ' => "Benin",
            'BM' => "Bermuda",
            'BT' => "Bhutan",
            'BO' => "Bolivia",
            'BA' => "Bosnia And Herzegovina",
            'BW' => "Botswana",
            'BV' => "Bouvet Island",
            'BR' => "Brazil",
            'IO' => "British Indian Ocean Territory",
            'BN' => "Brunei",
            'BG' => "Bulgaria",
            'BF' => "Burkina Faso",
            'BI' => "Burundi",
            'KH' => "Cambodia",
            'CM' => "Cameroon",
            'CA' => "Canada",
            'CV' => "Cape Verde",
            'KY' => "Cayman Islands",
            'CF' => "Central African Republic",
            'TD' => "Chad",
            'CL' => "Chile",
            'CN' => "China",
            'CX' => "Christmas Island",
            'CC' => "Cocos (Keeling) Islands",
            'CO' => "Columbia",
            'KM' => "Comoros",
            'CG' => "Congo",
            'CK' => "Cook Islands",
            'CR' => "Costa Rica",
            'CI' => "Cote D'Ivorie (Ivory Coast)",
            'HR' => "Croatia (Hrvatska)",
            'CU' => "Cuba",
            'CY' => "Cyprus",
            'CZ' => "Czech Republic",
            'CD' => "Democratic Republic Of Congo (Zaire)",
            'DK' => "Denmark",
            'DJ' => "Djibouti",
            'DM' => "Dominica",
            'DO' => "Dominican Republic",
            'TP' => "East Timor",
            'EC' => "Ecuador",
            'EG' => "Egypt",
            'SV' => "El Salvador",
            'GQ' => "Equatorial Guinea",
            'ER' => "Eritrea",
            'EE' => "Estonia",
            'ET' => "Ethiopia",
            'FK' => "Falkland Islands (Malvinas)",
            'FO' => "Faroe Islands",
            'FJ' => "Fiji",
            'FI' => "Finland",
            'FR' => "France",
            'FX' => "France, Metropolitan",
            'GF' => "French Guinea",
            'PF' => "French Polynesia",
            'TF' => "French Southern Territories",
            'GA' => "Gabon",
            'GM' => "Gambia",
            'GE' => "Georgia",
            'DE' => "Germany",
            'GH' => "Ghana",
            'GI' => "Gibraltar",
            'GR' => "Greece",
            'GL' => "Greenland",
            'GD' => "Grenada",
            'GP' => "Guadeloupe",
            'GU' => "Guam",
            'GT' => "Guatemala",
            'GN' => "Guinea",
            'GW' => "Guinea-Bissau",
            'GY' => "Guyana",
            'HT' => "Haiti",
            'HM' => "Heard And McDonald Islands",
            'HN' => "Honduras",
            'HK' => "Hong Kong",
            'HU' => "Hungary",
            'IS' => "Iceland",
            'IN' => "India",
            'ID' => "Indonesia",
            'IR' => "Iran",
            'IQ' => "Iraq",
            'IE' => "Ireland",
            'IM' => "Isle of Man",
            'IL' => "Israel",
            'IT' => "Italy",
            'JM' => "Jamaica",
            'JP' => "Japan",
            'JO' => "Jordan",
            'KZ' => "Kazakhstan",
            'KE' => "Kenya",
            'KI' => "Kiribati",
            'KW' => "Kuwait",
            'KG' => "Kyrgyzstan",
            'LA' => "Laos",
            'LV' => "Latvia",
            'LB' => "Lebanon",
            'LS' => "Lesotho",
            'LR' => "Liberia",
            'LY' => "Libya",
            'LI' => "Liechtenstein",
            'LT' => "Lithuania",
            'LU' => "Luxembourg",
            'MO' => "Macau",
            'MK' => "Macedonia",
            'MG' => "Madagascar",
            'MW' => "Malawi",
            'MY' => "Malaysia",
            'MV' => "Maldives",
            'ML' => "Mali",
            'MT' => "Malta",
            'MH' => "Marshall Islands",
            'MQ' => "Martinique",
            'MR' => "Mauritania",
            'MU' => "Mauritius",
            'YT' => "Mayotte",
            'MX' => "Mexico",
            'FM' => "Micronesia",
            'MD' => "Moldova",
            'MC' => "Monaco",
            'MN' => "Mongolia",
            'MS' => "Montserrat",
            'MA' => "Morocco",
            'MZ' => "Mozambique",
            'MM' => "Myanmar (Burma)",
            'NA' => "Namibia",
            'NR' => "Nauru",
            'NP' => "Nepal",
            'NL' => "Netherlands",
            'AN' => "Netherlands Antilles",
            'NC' => "New Caledonia",
            'NZ' => "New Zealand",
            'NI' => "Nicaragua",
            'NE' => "Niger",
            'NG' => "Nigeria",
            'NU' => "Niue",
            'NF' => "Norfolk Island",
            'KP' => "North Korea",
            'MP' => "Northern Mariana Islands",
            'NO' => "Norway",
            'OM' => "Oman",
            'PK' => "Pakistan",
            'PW' => "Palau",
            'PA' => "Panama",
            'PG' => "Papua New Guinea",
            'PY' => "Paraguay",
            'PE' => "Peru",
            'PH' => "Philippines",
            'PN' => "Pitcairn",
            'PL' => "Poland",
            'PT' => "Portugal",
            'PR' => "Puerto Rico",
            'QA' => "Qatar",
            'RE' => "Reunion",
            'RO' => "Romania",
            'RU' => "Russia",
            'RW' => "Rwanda",
            'SH' => "Saint Helena",
            'KN' => "Saint Kitts And Nevis",
            'LC' => "Saint Lucia",
            'PM' => "Saint Pierre And Miquelon",
            'VC' => "Saint Vincent And The Grenadines",
            'SM' => "San Marino",
            'ST' => "Sao Tome And Principe",
            'SA' => "Saudi Arabia",
            'SN' => "Senegal",
            'SC' => "Seychelles",
            'SL' => "Sierra Leone",
            'SG' => "Singapore",
            'SK' => "Slovak Republic",
            'SI' => "Slovenia",
            'SB' => "Solomon Islands",
            'SO' => "Somalia",
            'ZA' => "South Africa",
            'GS' => "South Georgia And South Sandwich Islands",
            'KR' => "South Korea",
            'ES' => "Spain",
            'LK' => "Sri Lanka",
            'SD' => "Sudan",
            'SR' => "Suriname",
            'SJ' => "Svalbard And Jan Mayen",
            'SZ' => "Swaziland",
            'SE' => "Sweden",
            'CH' => "Switzerland",
            'SY' => "Syria",
            'TW' => "Taiwan",
            'TJ' => "Tajikistan",
            'TZ' => "Tanzania",
            'TH' => "Thailand",
            'TG' => "Togo",
            'TK' => "Tokelau",
            'TO' => "Tonga",
            'TT' => "Trinidad And Tobago",
            'TN' => "Tunisia",
            'TR' => "Turkey",
            'TM' => "Turkmenistan",
            'TC' => "Turks And Caicos Islands",
            'TV' => "Tuvalu",
            'UG' => "Uganda",
            'UA' => "Ukraine",
            'AE' => "United Arab Emirates",
            'GB' => "United Kingdom",
            'US' => "United States",
            'UM' => "United States Minor Outlying Islands",
            'UY' => "Uruguay",
            'UZ' => "Uzbekistan",
            'VU' => "Vanuatu",
            'VA' => "Vatican City (Holy See)",
            'VE' => "Venezuela",
            'VN' => "Vietnam",
            'VG' => "Virgin Islands (British)",
            'VI' => "Virgin Islands (US)",
            'WF' => "Wallis And Futuna Islands",
            'EH' => "Western Sahara",
            'WS' => "Western Samoa",
            'YE' => "Yemen",
            'YU' => "Yugoslavia",
            'ZM' => "Zambia",
            'ZW' => "Zimbabwe"
        );
	}
?>
