<?php
session_start();
?>
<link href="css/callpanel_cssfile.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="//static.twilio.com/libs/twiliojs/1.2/twilio.min.js"></script>

<?php  ?>
<script type="text/javascript" language="javascript">
    var connection = null;
    var company_id = "<?php echo $_SESSION['sel_co']; ?>";
    var call_type = "outgoing"; // or "incoming"
    var received_call_sid = "";
    var last_status = "";
    var current_conference = { 
        "name": "", 
        i_am_host: false,
        members: []
    };

    $(document).ready(function() {
        Twilio.Device.setup("<?php echo generate_twilio_auth_token(); ?>");

        $("#dial-input-button").click(function() {
            if (!$('#dial-phone-number').val()) {
                errMsgDialog('Please enter a phone number.');
                return false;
            }

            var phone_number = $('#dial-phone-number').val();
            var number_to_add = { phone_number: phone_number, handle: phone_number, pending: 1};

            current_conference.members = [number_to_add];
            current_conference.i_am_host = 1;
            call_type = "outgoing";
            current_conference["name"] = '<?php echo $_SESSION['handle']; ?>_' + phone_number;
            current_conference["name"] = current_conference["name"].replace(/\(/g, "").replace(/\)/g, "").replace(/\+/g, "").replace(/ /g, "").replace(/\-/g, "");

            var outgoing_country =  countryForE164Number("+" + phone_number) || "US";

            var params = {
                "action": "call_another_number",
                "conference_name": current_conference["name"],
                "user_id": <?php echo $_SESSION['user_id']; ?>,
                "handle": '<?php echo str_replace("Rand", "RRRRaandddd", $_SESSION['handle']); ?>',
                "company_id": company_id,
                "tocall": phone_number,
                "Fromcall": $("#caller-id-phone-number").val(),
                "outgoing_country": outgoing_country
            };

            connection = Twilio.Device.connect(params);

            $("#client-participants-wrapper").hide();
            $("#client-from-wrapper, #client-to-wrapper").show();
            $(".call-from").text(formatNumber($("#caller-id-phone-number").val()));
            $(".call-to").text(formatNumber($("#dial-phone-number").val()));
        });
        $("#client-ui-hangup").click(function() {
            Twilio.Device.disconnectAll();
        });

        Twilio.Device.ready(function(device) {
            currentCallSid = "";
            $('#client-ui-message').text('Ready to start call');
            stopTimer();
        });

        Twilio.Device.offline(function(device) {
            currentCallSid = "";
            connection = null;
            $('#client-ui-message').text('Offline');
            stopTimer();
        });

        Twilio.Device.error(function(error) {
            $('#client-ui-message').text(error);
            Twilio.Device.disconnectAll();
            stopTimer();
        });

        Twilio.Device.connect(function(conn) {
            $(".dialer_popout_icon").css('visibility', 'hidden');

            var call_type_text = "Outbound Call";
            if (call_type == "incoming") call_type_text = "Connected";
            if (call_type == "ongoing") call_type_text = "Ongoing Call";

            $('#client-ui-message').text(call_type_text);
            $("div#client-make-call").hide();
            $("div#client-ui-status, div#client-ui-actions").show();
            if (call_type == "outgoing") {
                $("div#client-ui-phonecode").show();
                $("div#client-ui-phonecode-incoming").hide();
            }
            else {
                $("div#client-ui-phonecode").hide();
                $("div#client-ui-phonecode-incoming").show();
            }

            $(".hold-btn>div").show();

            toggleCallStatus();
            startTimer();
            connection = conn;

            connection.mute((($('.mute-btn').attr('data-mutted') == "yes") ? true : false));

            last_status = $("#user-status").val();
            $("#user-status").val('BUSY');
            $("#user-status").trigger('change');
            
            checkIncomingLiveCalls(true);

            if (mc_to_delete != "") {
                deleteMisedCall(mc_to_delete);
            }
        });

        Twilio.Device.disconnect(function(conn) {
            $("#user-status").val(last_status);
            $("#user-status").trigger('change');
            $(".dialer_popout_icon").css('visibility', 'visible');
            $('#client-ui-message').text('Call ended');
            $('#divstatus').val('shown');
            $("div#client-make-call").show();
            $("div#client-ui-status, div#client-ui-actions, div#client-ui-phonecode, client-ui-phonecode-incoming").hide();
            connection = null;
            currentCallSid = "";
            $("#dial-phone-number").val('');
            toggleCallStatus();
            stopTimer();
            $('#client-phonecode, #client-phonecode-incoming').val(0);

            $(".hold-btn>div").hide();
            
            if (incXhr) {
                incXhr.abort();
                incXhr = null;
                setTimeout(checkIncomingLiveCalls, 7000);
            }
        });

        Twilio.Device.incoming(function(conn) {
            console.log(conn);
            //alert(JSON.stringify(conn));
            // accept the incoming connection and start two-way audio
            //conn.accept();
        });

        setInterval(function() {
            if (!connection) {
                var response = JSONuserajaxCall({func:'USR_REFRESH_TWILIO_TOKEN'});
                response.done(function(token){
                    Twilio.Device.setup(token);
                });
            }
        }, 17000000);

        function toggleCallStatus() {
            $('#hangup').toggle();
            $('#dialpad').toggle();

            toggleAgentsTransfer();
        }

        $(".client-ui-button:not(.mute-btn):not(.hold-btn)").click(function(){
            if (connection) {
                var value = $(this).find(".client-ui-button-number").text();
                if (value != 'C' || value != "") connection.sendDigits(value);
            }
        });

        $("#dial-phone-number").keyup(function(event){
            if(event.keyCode == 13)
                $("#dial-input-button").click();
        });

        $(".callmaker").click(function(e)
        {
            if (connection) return false;

            if ($(this).hasClass('disabled'))
                return false;

            if ($("#divstatus").val() == "")
            {
                $("div#dialer").animate({ right: '0px' }, 700);
                $('#divstatus').val('shown');
                $("div#client-make-call").show();
                $("div#client-ui-status, div#client-ui-actions").hide();
                $("#support_tab").removeClass('SupportTabRight');
                $("#support_tab").addClass('SupportTabLeft');
                $("#client-calls-queue").hide();
            }
            else
            {
                $('#divstatus').val('');
                <?php if (!isset($is_popout_dialer)) { ?>$("div#dialer").animate({ right: '-405px' }, 700);<?php } ?>
                Twilio.Device.disconnectAll();
                $("#support_tab").removeClass('SupportTabLeft');
                $("#support_tab").addClass('SupportTabRight');
                if ($("#client-calls-queue .incoming_call").length > 0)
                    $("#client-calls-queue").show();
            }
            e.preventDefault();
        });

        $('#divstatus').val('');
        <?php if (!isset($is_popout_dialer)) { ?>$("div#dialer").css({ right: '-405px' });<?php } ?>

        $("#call-options-summary").click(function() {
            if ($("#call-options").height() > 50) {
                $("#summary-call-toggle").html('<img src="images/caller_id_dd.png" width="12" height="11" />');
            }else {
                $("#summary-call-toggle").html('<img src="images/caller_id_dd_h.png" width="12" height="11" />');
            }

            if ($("#callOptionsInput").val() == '')
            {
                $("#summary-call-toggle").addClass('open');
                $("div#call-options-inputs").slideDown();
                $('#callOptionsInput').val('shown');
            }
            else
            {
                $("#summary-call-toggle").removeClass('open');
                $("div#call-options-inputs").slideUp();
                $('#callOptionsInput').val('');
            }
        });

        $('#caller-id-phone-number').change(function() {
            $('.device-number').text($(this).val());
        });
        $('.device-number').text($('#caller-id-phone-number').val());

        $('#client-ui-pad .client-ui-button:not(.mute-btn):not(.hold-btn)').click(function() {
            $('#dial-phone-number').val($('#dial-phone-number').val() + '' + $(this).children('.client-ui-button-number').text());
        });

        $('.clear-ui-button-data').click(function() {
            $('#dial-phone-number').val('');
        });

        $("#client-phonecode").change(function(){
            if(connection==null)
                errMsgDialog("Error setting phone code. No call is active!");
            else{
                var val = $("#client-phonecode").val();
                $(this).attr('disabled','true');
                var response = JSONuserajaxCall({func:'USR_BROWSERPHONE_PC_SET', pc: val});
                response.done(function(){
                    $('#client-phonecode').removeAttr('disabled');
                });
            }
        });

        $("#client-phonecode-incoming").change(function(){
            if(connection==null)
                errMsgDialog("Error setting phone code. No call is active!");
            else{
                var val = $("#client-phonecode-incoming").val();
                $(this).attr('disabled','true');
                var response = JSONuserajaxCall({func:'USR_BROWSERPHONE_INCOMING_PC_SET', call_sid: received_call_sid, pc: val});
                response.done(function(){
                    $('#client-phonecode-incoming').removeAttr('disabled');
                });
            }
        });

        <?php
        $popout_dialer_time_diff = (isset($_SESSION['popout_dialer_last_time'])) ? time() - $_SESSION['popout_dialer_last_time'] : 99;
        if ($popout_dialer_time_diff <= 5 && !isset($is_popout_dialer)) {
            ?>
            disableSlideOutDialer();
            <?php
        }
        ?>

        $("a:not(.jp-play):not(.jp-pause):not(.callmaker)").on("click", function(e) {
            var link = this;
            var exploded = link.href.split("script:");
            var without_diez = link.href.replace("#", "");

            if (connection && without_diez == link.href && exploded.length == 1) {
                e.preventDefault();

                promptMsg('Are you sure you want to end call?', function() {
                    var clonedEvent = new MouseEvent('click', e);
                    if (!e.srcElement.dispatchEvent(clonedEvent)) {
                        window.location = link.href;
                    }
                });
            }
        });

        if (document.getElementById("phoneRing")) {
            document.getElementById("phoneRing").addEventListener("paused", function(){
                this.currentTime = 0;
            });
        }
    });

    var mins = 0;
    var secs = 0;
    function startTimer()
    {
        $("#minutes, #seconds").html('00');

        mins = 0;
        secs = 0;
        $('#TimeStatus').val('start');
        setTimeout('runTimer()', 1000);
    }
    function runTimer()
    {
        if (document.getElementById('TimeStatus').value == 'start')
        {
            if (secs < 59)
                $("#seconds").html(((secs < 10) ? "0" : "") + secs);
            else
            {
                mins = Math.floor(secs / 60);
                $("#minutes").html(((mins < 10) ? "0" : "") + mins);
                var new_seconds = secs - Math.round(mins * 60);
                $("#seconds").html(((new_seconds < 10) ? "0" : "") + new_seconds);

            }
            secs++;
            setTimeout('runTimer()', 1000);
        }
    }

    function stopTimer()
    {
        mins = 0;
        secs = 0;
        $("#minutes, #seconds").html('00');        
        $("#TimeStatus").val('');
    }

    var popoutDialer;
    var popoutDialerInt;
    var popoutDialerClosed = false;
    function openPopoutDialer(obj) {
        popoutDialerClosed = false;
        obj = $(obj);
        obj.blur();

        var top = ($(window).height() - 650)/2;
        var left = ($(window).width() - 400)/2;
        popoutDialer = window.open("popout_dialer.php", "popoutDialer", "width=400, height=750, top=" + top + ", left=" + left + ", menubar=no, toolbar=no, directories=no, scrollbar=no, resizable=no, status=no");

        disableSlideOutDialer();

        popoutDialerInt = setInterval(function() {
            if (typeof(popoutDialer) != "undefined") {
                if (popoutDialer.window == null) {
                    enableSlideOutDialer();
                    clearTimeout(popoutDialerInt);
                    popoutDialerInt = null;
                    popoutDialerClosed = true;
                    checkIncomingLiveCalls(true);
                }
            }
        }, 100);
    }

    function disableSlideOutDialer() {
        if (popoutDialerClosed) return false;

        $("div#dialer").css({ right: '-405px' });
        $("#dialer .client-ui-tab").fadeTo(0, 0.5);
        $("#dialer .callmaker").addClass('disabled');
        $('#divstatus').val('');
        $("#support_tab").removeClass('SupportTabLeft');
        $("#support_tab").addClass('SupportTabRight');
    }

    function enableSlideOutDialer() {
        if (typeof(popoutDialer) != "undefined")
            if (popoutDialer.window != null)
                return false;

        $("#dialer .client-ui-tab").fadeTo(0, 1);
        $("#dialer .callmaker").removeClass('disabled');
    }

    function toggleDialerMute() {
        var mute_btn = $('.mute-btn');

        var action = (mute_btn.attr('data-mutted') == "no") ? "mute" : "unmute";

        var response = JSONuserajaxCall({
            func: "USR_BROWSERPHONE_TOGGLE_MUTE",
            action: action
        });

        dialerMuteButtonDisplay(action);
    }

    function dialerMuteButtonDisplay(display_type) {
        var mute_btn = $('.mute-btn');
        if (display_type == "mute") {
            mute_btn.attr('data-mutted', 'yes');
            mute_btn.find('.muted').show();
            mute_btn.find('.unmuted').hide();
            mute_btn.find('.client-ui-button-letters').text('Unmute');

            if (connection) connection.mute(true);
        }
        else {
            mute_btn.attr('data-mutted', 'no');
            mute_btn.find('.muted').hide();
            mute_btn.find('.unmuted').show();
            mute_btn.find('.client-ui-button-letters').text('Mute');
            if (connection) connection.mute(false);
        }
    }

    function putCallOnHold() {
        if (connection) {
            var params = {
                func: "USER_PUT_CALL_ON_HOLD",
                conference_name: current_conference["name"],
                i_am_host: current_conference.i_am_host
            };

            var response = JSONuserajaxCall(params);

            response.done(function() {
                $("#client-ui-hangup").click();
            });
        }
    }

    function changeUserStatus(status_el) {
        status_el = $(status_el);

        var response = JSONuserajaxCall({
            func: "USER_UPDATE_STATUS",
            status: status_el.val()
        });
    }

    function answerIncomingCall(call_sid) {
        var call_div = $(".call_" + call_sid);
        if (call_div.length > 0) {
            call_type = "incoming";
            received_call_sid = call_sid;
            previous_handles = "";

            if ($("#divstatus").val() == "") {
                $(".callmaker").click();
            }

            var call_user_id = call_div.find(".call_data_user_id").val();
            var member_call_type = call_div.find(".call_data_type").val();
            var call_from = call_div.find(".call_data_from").val();
            var call_to = call_div.find(".call_data_to").val();
            var call_name = call_div.find(".call_data_name").val();
            var call_from_display = call_div.find(".call_data_display").val();
            var call_data_incoming = call_div.find(".call_data_incoming").val();

            var caller_is_agent = 0;
            var call_to_display = formatNumber(call_to);

            if ($('#client-ui-actions').is(":visible")) {
                caller_is_agent = 1;

                var member_to_add = {
                    type: member_call_type,
                    user_id: call_user_id,
                    handle: call_name,
                    is_host: false,
                    call_sid: call_sid
                }

                var redo_current_call_sid = 0;

                if (!$("#client-participants-wrapper").is(":visible")) {
                    redo_current_call_sid = 1;
                    $("#client-participants-wrapper").show();
                    $("#client-from-wrapper").hide();
                    $("#client-to-wrapper").hide();
                }

                current_conference.members.push(member_to_add);
                var response = JSONuserajaxCall({
                    func: "USR_ADD_INCOMING_CALL_TO_CONFERENCE",
                    conference_name: current_conference["name"],
                    member_to_add: member_to_add,
                    redo_current_call_sid: redo_current_call_sid
                });



                response.done(function() {
                    $("div.call_" + call_sid).remove();
                    checkIncomingLiveCalls(true);
                    if ($("#client-calls-queue .incoming_call").length == 0) {
                        $("div#client-calls-list").html('No calls.');
                    }
                });
            }
            else {
                if (call_from == "-") {
                    caller_is_agent = 1;
                    current_conference["name"] = call_name + "_<?php echo $_SESSION['handle']; ?>";
                    call_to_display = '<?php echo $_SESSION['handle']; ?>';
                }
                else {
                    current_conference["name"] = call_from + "_<?php echo $_SESSION['handle']; ?>";
                }
                current_conference["name"] = current_conference["name"].replace(/\(/g, "").replace(/\)/g, "").replace(/\+/g, "").replace(/ /g, "").replace(/\-/g, "");
                
                var params = {
                    "action": "answer_incoming_call",
                    "call_sid": call_sid,
                    "conference_name": current_conference["name"],
                    "user_id": <?php echo $_SESSION['user_id']; ?>,
                    "handle": '<?php echo str_replace("Rand", "RRRRaandddd", $_SESSION['handle']); ?>',
                    "company_id": company_id,
                    "tocall": call_to,
                    "Fromcall": call_from,
                    "caller_is_agent": caller_is_agent
                };

                $("#client-from-wrapper, #client-to-wrapper").hide();
                $("#client-participants-wrapper").show();
                $(".call-participants").html('<img src="images/loading.gif" style="width: 16px;" />');
                previous_handles = 'start again';

                connection = Twilio.Device.connect(params);
                if (incXhr) {
                    incXhr.abort();
                    incXhr = null;
                    setTimeout(checkIncomingLiveCalls, 7000);
                }
                $("div.call_" + call_sid).remove();
                if ($("#client-calls-queue .incoming_call").length == 0) {
                    $("div#client-calls-list").html('No calls.');
                }
            }
        }
    }

    function rejoinOngoingCall(conference_name) {

        var call_div = $(".call_" + conference_name);
        if (call_div.length > 0) {
            call_type = "incoming";
            previous_handles = "start again";

            if ($("#divstatus").val() == "") {
                $(".callmaker").click();
            }

            if (connection) {
                var redo_current_call_sid = 0;

                if (!$("#client-participants-wrapper").is(":visible")) {
                    redo_current_call_sid = 1;
                    $("#client-participants-wrapper").show();
                    $("#client-from-wrapper").hide();
                    $("#client-to-wrapper").hide();
                }

                var response = JSONuserajaxCall({
                    func: "USR_ADD_ONGOING_CALL_TO_CONFERENCE",
                    current_conference: current_conference["name"],
                    ongoing_conference: conference_name,
                    redo_current_call_sid: redo_current_call_sid
                });

                response.done(function() {});
            }
            else {

                var call_data_i_am_host = call_div.find(".call_data_i_am_host").val();

                current_conference["name"] = conference_name;
                current_conference.i_am_host = call_data_i_am_host;
                
                var params = {
                    "action": "rejoin_ongoing_call",
                    "conference_name": current_conference["name"],
                    "user_id": <?php echo $_SESSION['user_id']; ?>,
                    "handle": '<?php echo str_replace("Rand", "RRRRaandddd", $_SESSION['handle']); ?>',
                    "company_id": company_id,
                    "call_data_i_am_host": call_data_i_am_host
                };

                $("#client-from-wrapper, #client-to-wrapper").hide();
                $("#client-participants-wrapper").show();
                $(".call-participants").html('<img src="images/loading.gif" style="width: 16px;" />');
                previous_handles = 'start again';

                connection = Twilio.Device.connect(params);
            }

            $("div.call_" + conference_name).remove();

            if (incXhr) {
                incXhr.abort();
                incXhr = null;
                setTimeout(checkIncomingLiveCalls, 7000);
            }

            if ($("#client-calls-ongoing-list .incoming_call").length == 0) {
                $("div#client-ongoing-wrapper").hide();
                $("div#client-calls-ongoing-list").html('No calls.');
            }
        }
    }

    var previous_handles = '';
    var cp_tooltip;
    function updateConferenceDetails(conference) {
        current_conference = conference;
        var handles = [];
        var participants_html = '';

        current_conference.members = [];
        current_conference.i_am_host = 0;
        for (var i in conference.participants) {
            if (conference.participants[i].is_host == true && conference.participants[i].user_id == <?php echo $_SESSION['user_id']; ?>) {
                current_conference.i_am_host = 1;
            }
            if (conference.participants[i]["type"] == "agent" || conference.participants[i]["type"] == "phone_number") {
                current_conference.members.push(conference.participants[i]);
            }
        }

        var has_pending = false;
        for (var i in conference.participants) {        
            var participant = conference.participants[i];
            var this_handle = participant.handle;

            if (typeof(participant.pending) != "undefined") {
                this_handle = this_handle;
                previous_handles = "";
            }

            if (typeof(participant.on_hold) != "undefined") {
                if (participant.on_hold == 1) {
                    this_handle += ' (on hold)';
                }
            }

            handles.push(this_handle);

            participants_html += '<div class="incoming_call">' + 
                    '<div class="call_action_buttons" style="display: block;">';

                    if (current_conference.i_am_host && participant.user_id != <?php echo $_SESSION['user_id']; ?>) {
                        participants_html += '<a href="javascript: void(0);"><img src="images/icons/decline.png" onclick="removePersonFromCall(\'' + conference.participants[i].call_sid + '\', \'' + encodeURIComponent(conference.participants[i].handle) + '\');" style="width: 16px;" /></a>';
                    }

                    if (typeof(participant.on_hold) != "undefined") {
                        if (participant.on_hold == '1') {
                            this_handle = '<span style="opacity: 0.6; filter: alpha(opacity=60);">' + this_handle + '</span>';
                        }
                    }

                    participants_html += '</div>' +
                    '<strong>' + this_handle + '</strong>' + 
                    '<div style="clear: both;"></div>' +
                '</div>';
        }
        var handles = handles.join(", ");

        if (previous_handles != handles || $(".call-participants img").length > 0) {
            previous_handles = handles;

            if (handles.length > 20) handles = handles.substr(0, 17) + '...';

            $(".call-participants").html(handles);

            if (cp_tooltip) cp_tooltip.tooltipster('destroy');

            cp_tooltip = $(".call-participants").tooltipster({ content: '<div style="width: 200px;">' + participants_html + '</div>', interactive: true, contentAsHTML: true });
        }

        toggleAgentsTransfer();
    }

    function toggleAgentsTransfer() {
        var transfer_agent_label = $("#client-list-agent-transfer");
        var add_to_call_btn = $("#client-add-external-number-wrapper");

        if(connection !== null && current_conference.i_am_host){
            transfer_agent_label.text("Transfer");
            add_to_call_btn.attr("style","");
        }else{
            transfer_agent_label.text("Agents");
            add_to_call_btn.attr("style","display:none;");
        }
    }

    function removePersonFromCall(call_sid, handle) {
        promptMsg("Are you sure you want to remove " + decodeURIComponent(handle) + " from this call ?", function() {
            if (incXhr) {
                incXhr.abort();
                incXhr = null;
                setTimeout(checkIncomingLiveCalls, 5000);
            }
            var response = JSONuserajaxCall({
                func: "USR_REMOVE_PERSON_FROM_CALL",
                call_sid: call_sid,
                conference_name: current_conference["name"]
            });

            if (previous_handles.split(',').length <= 2) {
                $("#client-ui-hangup").click();
            }

            $("div.call_" + call_sid).remove();
            if (cp_tooltip) { cp_tooltip.tooltipster('destroy'); cp_tooltip = null; }
            $(".call-participants").html('<img src="images/loading.gif" style="width: 16px;" />');
            checkIncomingLiveCalls(true);
        });
    }

    function callAnotherAgent(user_id, user_name) {
        previous_handles = "start again";

        if (connection) {
            var params2 = {
                func: "USER_PUT_CALL_ON_HOLD",
                conference_name: current_conference["name"],
                i_am_host: current_conference.i_am_host,
                ignore_hold_everybody: 1
            };

            var response = JSONuserajaxCall(params2);

            response.done(function() {
                $("#client-ui-hangup").click();

                setTimeout(function() {
                    var agent_to_add = { user_id: user_id, handle: user_name, pending: 1 };
                    user_name = decodeURIComponent(user_name);

                    current_conference.members = [agent_to_add];
                    current_conference.i_am_host = 1;
                    call_type = "to_another_agent";
                    current_conference["name"] = '<?php echo $_SESSION['handle']; ?>_' + user_name;
                    current_conference["name"] = current_conference["name"].replace(/\(/g, "").replace(/\)/g, "").replace(/\+/g, "").replace(/ /g, "").replace(/\-/g, "");

                    var params = {
                        "action": "call_another_agent",
                        "conference_name": current_conference["name"],
                        "user_id": <?php echo $_SESSION['user_id']; ?>,
                        "handle": '<?php echo str_replace("Rand", "RRRRaandddd", $_SESSION['handle']); ?>',
                        "company_id": company_id,
                        "to_user_id": user_id,
                        "to_user_name": user_name
                    };

                    connection = Twilio.Device.connect(params);

                    $("#client-from-wrapper, #client-to-wrapper").hide();
                    $("#client-participants-wrapper").show();
                    $(".call-participants").html('<img src="images/loading.gif" style="width: 16px;" />');
                    checkIncomingLiveCalls(true);
                    previous_handles = 'start again';
                }, 1000);
                
            });
        }
        else {
            var agent_to_add = { user_id: user_id, handle: user_name, pending: 1 };
            user_name = decodeURIComponent(user_name);

            current_conference.members = [agent_to_add];
            current_conference.i_am_host = 1;
            call_type = "to_another_agent";
            current_conference["name"] = '<?php echo $_SESSION['handle']; ?>_' + user_name;
            current_conference["name"] = current_conference["name"].replace(/\(/g, "").replace(/\)/g, "").replace(/\+/g, "").replace(/ /g, "").replace(/\-/g, "");

            var params = {
                "action": "call_another_agent",
                "conference_name": current_conference["name"],
                "user_id": <?php echo $_SESSION['user_id']; ?>,
                "handle": '<?php echo str_replace("Rand", "RRRRaandddd", $_SESSION['handle']); ?>',
                "company_id": company_id,
                "to_user_id": user_id,
                "to_user_name": user_name
            };
            connection = Twilio.Device.connect(params);

            $("#client-from-wrapper, #client-to-wrapper").hide();
            $("#client-participants-wrapper").show();
            $(".call-participants").html('<img src="images/loading.gif" style="width: 16px;" />');
            checkIncomingLiveCalls(true);
            previous_handles = 'start again';
        }
    }

    function addPhoneNumberToCall() {
        promptMsg("Adding a number will put your current call on hold to call the number.<br>Are you sure you want to proceed?",function(){
            var phone_number = $("#client-add-external-number-input").val();

            if (phone_number == '') {
                errMsgDialog('Please enter a phone number first.');
                return false;
            }

            for (var i in current_conference.members) {
                if (current_conference.members[i]["handle"] == phone_number) {
                    var message = (typeof(current_conference.members[i].pending) == "undefined") ? phone_number + " is already on this call!" : "You already added " + phone_number + " on this call!";
                    errMsgDialog(message);
                    return false;
                }
            }

            $("#client-add-external-number-input").val('');

            if (connection) {
                var params2 = {
                    func: "USER_PUT_CALL_ON_HOLD",
                    conference_name: current_conference["name"],
                    i_am_host: current_conference.i_am_host,
                    ignore_hold_everybody: 1
                };

                var response = JSONuserajaxCall(params2);

                response.done(function() {
                    $("#client-ui-hangup").click();

                    setTimeout(function() {
                        $('#dial-phone-number').val(phone_number);
                        $("#dial-input-button").click();
                    }, 1000);
                });
            }
        });
    }

    var mc_to_delete = "";
    function callBackMissedCall(call_sid) {
        if (connection) return false;

        var call_div = $(".call_" + call_sid);
        if (call_div.length > 0) {
            mc_to_delete = call_sid;
            var call_data_type = call_div.find(".call_data_type").val();
            var call_data_from_number = call_div.find(".call_data_from_number").val();
            var call_data_from = call_div.find(".call_data_from").val();

            if (call_data_type == "agent") {
                callAnotherAgent(call_data_from_number, call_data_from);
            }
            else {
                $('#dial-phone-number').val(call_data_from_number);
                $("#dial-input-button").click();
            }
        }
    }

    function deleteMisedCall(call_sid) {
        if (incXhr) {
            incXhr.abort();
            incXhr = null;
            setTimeout(checkIncomingLiveCalls, 5000);
        }
        var response = JSONuserajaxCall({
            func: "USR_DELETE_MISSED_CALL",
            call_sid: call_sid
        });
        $("div.call_" + call_sid).remove();
        if ($("#client-missed-wrapper .incoming_call").length == 0) {
            $("#client-missed-wrapper").hide();
            $("div#client-missed-calls-list").html('No calls.');
        }
    }

    function declineIncomingCall(call_sid) {
        promptMsg("Are you sure you want to decline this call ?", function() {
            if (incXhr) {
                incXhr.abort();
                incXhr = null;
                setTimeout(checkIncomingLiveCalls, 5000);
            }
            var response = JSONuserajaxCall({
                func: "USR_DECLINE_INCOMING_CALL",
                call_sid: call_sid
            });
            $("div.call_" + call_sid).remove();
            if ($("#client-calls-queue .incoming_call").length == 0) {
                $("#client-calls-queue").hide();
                $("div#client-calls-list").html('No calls.');
            }
        });
    }

    var animating = false;
    function clientListToggle(elm){
        if(!animating) {
            $("#client-list-calls").removeClass("on");
            $("#client-list-agent-transfer").removeClass("on");
            $("#"+$(elm).attr("id")).addClass("on");

            var agent_list = $("#client-list-agents");
            var calls_list = $("#client-list-calls-list");

            if(calls_list.css('display')==="none" && elm.innerText == "Calls"){
                animating = true;
                agent_list.fadeOut(100, function(){
                    calls_list.fadeIn(200, function(){
                        animating = false;
                    });
                });
            }else if(calls_list.css('display')!=="none" && elm.innerText != "Calls"){
                animating = true;
                calls_list.fadeOut(100, function(){
                    agent_list.fadeIn(200, function(){
                        animating = false;
                    });
                });
            }
        }
    }

    var incomingAnimationStarted = false;
    var incomingAnimationCounter = 0;
    var incomingRinging = false;
    setInterval(toggleIncomingCallsAnimation, 500);

    function toggleIncomingCallsAnimation() {
        if (!shouldRunOnThisTab()) {
            incomingRinging = false;
            if (incomingRinging) {
                document.getElementById("phoneRing").pause();
                document.getElementById("phoneRing").currentTime = 0;
            }
            return false;
        }

        if ($('div#client-calls-list .incoming_call').length > 0) {
            //if (incomingAnimationStarted) return false;
            if (!$("#client-list-calls").hasClass('orange')) $("#client-list-calls").addClass('orange');
            if (incomingAnimationCounter == 0) {
                incomingAnimationCounter = 1;
                $("#client-list-calls").fadeTo(250, 0.3)
            }
            else {
                incomingAnimationCounter = 0;
                $("#client-list-calls").fadeTo(250, 1.0);
            }

            if (!incomingRinging && !connection) {
                if (!$(".callmaker").hasClass('disabled')) {
                    incomingRinging = true;
                    document.getElementById("phoneRing").play();
                }
            }

            if (incomingRinging && connection) {
                incomingRinging = false;
                document.getElementById("phoneRing").pause();
                document.getElementById("phoneRing").currentTime = 0;
            }
            
            incomingAnimationStarted = true;
        }
        else {
            $("#client-list-calls").removeClass('orange');
            $("#client-list-calls").fadeTo(250, 1.0);
            incomingAnimationStarted = false;
            if (incomingRinging) {
                incomingRinging = false;
                document.getElementById("phoneRing").pause();
                document.getElementById("phoneRing").currentTime = 0;
            }
        }
    }
</script>