<?php
class EmailTemplate{
    private $email_types = array(
        //    ID NAME            SELECT LABEL
        array(1,'Invoice',0,array(
            "[numberofleads]",
            "[invoiceperiod]",
            "[cashperlead]",
            "[cashtotalinvoiceamount]",
            "[companyinfo]",
            "[invoicenumber]",
            "[currentdate]",
            "[fullname]",
            "[useraddress]",
            "[usercity]",
            "[userstate]",
            "[userzip]",
            "[url]"
        )),
        array(2,'User Email',0,array(
            "[useremail]",
            "[fullname]",
            "[username]",
            "[userpass]",
            "[companyinfo]",
            "[currentdate]",
            "[url]"
        )),
//        array(3,'Call Report',0),
        array(5,'All Calls',1,array(
            "[from]",
            "[to]",
            "[duration]",
            "[company]",
            "[recordingurl]"
        )),
        array(6,'Calls Missed',1,array(
            "[from]",
            "[to]",
            "[company]",
        )),
        array(7,'Calls Answered',1,array(
            "[from]",
            "[to]",
            "[duration]",
            "[company]",
            "[recordingurl]"
        )),
        array(8,'Calls Over 30 Seconds',1,array(
            "[from]",
            "[to]",
            "[duration]",
            "[company]",
            "[recordingurl]"
        )),
        array(9,'Calls Over 45 Seconds',1,array(
            "[from]",
            "[to]",
            "[duration]",
            "[company]",
            "[recordingurl]"
        )),
        array(10,'Calls Over 60 Seconds',1,array(
            "[from]",
            "[to]",
            "[duration]",
            "[company]",
            "[recordingurl]"
        )),
        array(11,'Calls Over 90 Seconds',1,array(
            "[from]",
            "[to]",
            "[duration]",
            "[company]",
            "[recordingurl]"
        )),
        array(12,'Calls Over 2 Minutes',1,array(
            "[from]",
            "[to]",
            "[duration]",
            "[company]",
            "[recordingurl]"
        )),
        array(13,'Invoice Pending Review',1,array(
            "[clientname]",
            "[invoiceperiod]",
            "[url]",
            "[system_create_date]"
        ))
    );

    public function getList(){
        return $this->email_types;
    }

    private function dateNow(){
        require_once("config.php");
        global $TIMEZONE;
        $date = new DateTime("now", new DateTimeZone($TIMEZONE));
        return $date->format("F j, Y");
    }

    private $template_examples = array(
        1 => array(
            "[numberofleads]"=>23,
            "[invoiceperiod]"=>"July 12 2013 - August 12 2013",
            "[cashperlead]"=>"2.50",
            "[cashtotalinvoiceamount]"=>"2,301",
            "[companyinfo]"=>"",
            "[invoicenumber]"=>"INV-2013081255",
            "[currentdate]"=>"",
            "[fullname]"=>"Bill",
            "[useraddress]"=>"123 Example St",
            "[usercity]"=>"San Diego",
            "[userstate]"=>"CA",
            "[userzip]"=>"‎91911",
            "[url]"=>""
        ),
        2 => array(
            "[useremail]"=>"example@test.com",
            "[fullname]"=>"Bill Smith",
            "[username]"=>"billy122",
            "[userpass]"=>"s9dj2so",
            "[url]"=>"",
            "[companyinfo]"=>"",
            "[currentdate]"=>""
        ),
        4 => array(
            "[useremail]"=>"example@test.com",
            "[fullname]"=>"Bill Smith",
            "[username]"=>"billy122",
            "[userpass]"=>"s9dj2so",
            "[url]"=>"",
            "[companyinfo]"=>"",
            "[currentdate]"=>""
        ),
        5 => array(
            "[from]"=>"(555) 763-1533",
            "[to]"=>"(555) 883-3325",
            "[duration]"=>"312 Seconds",
            "[company]"=>"Example Co",
            "[recordingurl]"=>"http://api.twilio.com/Cowbell.mp3"
        ),
        6 => array(
            "[from]"=>"(555) 763-1533",
            "[to]"=>"(555) 883-3325",
            "[company]"=>"Example Co",
        ),
        7 => array(
            "[from]"=>"(555) 763-1533",
            "[to]"=>"(555) 883-3325",
            "[duration]"=>"312 Seconds",
            "[company]"=>"Example Co",
            "[recordingurl]"=>"http://api.twilio.com/Cowbell.mp3"
        ),
        8 => array(
            "[from]"=>"(555) 763-1533",
            "[to]"=>"(555) 883-3325",
            "[duration]"=>"312 Seconds",
            "[company]"=>"Example Co",
            "[recordingurl]"=>"http://api.twilio.com/Cowbell.mp3"
        ),
        9 => array(
            "[from]"=>"(555) 763-1533",
            "[to]"=>"(555) 883-3325",
            "[duration]"=>"312 Seconds",
            "[company]"=>"Example Co",
            "[recordingurl]"=>"http://api.twilio.com/Cowbell.mp3"
        ),
        10 => array(
            "[from]"=>"(555) 763-1533",
            "[to]"=>"(555) 883-3325",
            "[duration]"=>"312 Seconds",
            "[company]"=>"Example Co",
            "[recordingurl]"=>"http://api.twilio.com/Cowbell.mp3"
        ),
        11 => array(
            "[from]"=>"(555) 763-1533",
            "[to]"=>"(555) 883-3325",
            "[duration]"=>"312 Seconds",
            "[company]"=>"Example Co",
            "[recordingurl]"=>"http://api.twilio.com/Cowbell.mp3"
        ),
        12 => array(
            "[from]"=>"(555) 763-1533",
            "[to]"=>"(555) 883-3325",
            "[duration]"=>"312 Seconds",
            "[company]"=>"Example Co",
            "[recordingurl]"=>"http://api.twilio.com/Cowbell.mp3"
        ),
        13 => array(
            "[clientname]"=>"Bill Smith",
            "[invoiceperiod]"=>"Mon 18, 2014 - Sun 27, 2014",
            "[url]"=>"",
            "[system_create_date]" => "Mon 29, 2014"
        )
    );

    public function getExample($template_id, $email_text){
        if($this->template_examples[$template_id]!=""){
            $data = $this->template_examples[$template_id];
            if(substr($_SERVER["HTTP_HOST"],0,4)=="www.")
                $host = substr($_SERVER["HTTP_HOST"],4,strlen($_SERVER["HTTP_HOST"]-4));
            else
                $host = $_SERVER["HTTP_HOST"];

            $host = "http://".$host.str_replace("/admin_ajax_handle.php","",$_SERVER['SCRIPT_NAME']);

            $data['[url]'] = $host;

            if($template_id == 1){
                $db = new DB();
                $data['[companyinfo]'] = $db->getVar("company_info");
                $data['[currentdate]'] = $this->dateNow();
            }

            foreach($data as $token => $value){
                $email_text = str_replace($token,$value,$email_text);
            }
            return nl2br($email_text);
        }else{
            return nl2br($email_text);
        }
    }

    public function getShortcodes($id){
        foreach($this->getList() as $template){
            if($template[0]==$id)
                return $template;
        }
        return false;
    }

    public function getEmailTemplate($id){
        require_once("db.php");
        $db = new DB();
        $stmt = $db->customExecute("SELECT `data` FROM email_templates WHERE id = ?");
        $stmt->execute(array($id));
        $data = $stmt->fetch(PDO::FETCH_OBJ);

        $data = @$data->data;
        $data = str_replace("\r", htmlspecialchars("\\r"), $data); 
        $data = str_replace("\n", htmlspecialchars("\\n"), $data);

        return json_decode($data);
    }
}