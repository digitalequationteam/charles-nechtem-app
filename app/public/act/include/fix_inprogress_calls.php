<?php
ignore_user_abort(true);
set_time_limit(0);
require_once('util.php');
session_write_close();
$db = new DB();
$fix_inprogress_run = $db->getVar("fix_inprogress_run");

//
// See if value is in DB. If not, add it.
//
if($fix_inprogress_run == false || $fix_inprogress_run == 0){
    $db->setVar("fix_inprogress_run",-1);
    $fix_inprogress_run = -1;
}

//
// Check how long script has been running. And if it stalled, reset.
//
if($fix_inprogress_run != -1 && (strtotime('now') - $fix_inprogress_run) >= (60*60)){
    Util::error_log("[CallFix] Resetting..");
    $db->setVar("fix_inprogress_run",-1);
    $fix_inprogress_run = -1;
}


if($fix_inprogress_run != -1){
    Util::error_log("[CallFix] Already running...");
    die("running");
}else{
    $db->setVar("fix_inprogress_run",strtotime('now'));

    $two_hours_ago = date(Util::STANDARD_MYSQL_DATE_FORMAT,strtotime("-2 hours"));

    //
    // Get count of in-progress calls.
    //
    $stmt = $db->customExecute("SELECT COUNT(*) AS 'count' FROM `calls` WHERE `DialCallStatus` IS NULL AND `DateCreated` < ?;");
    $stmt->execute(array($two_hours_ago)); $res = $stmt->fetch(PDO::FETCH_ASSOC);
    $count = $res['count'];
    Util::error_log("[CallFix] in-progress count: ".$count);

    if($count > 0){
        $stmt = $db->customQuery("SELECT `CallSid` FROM `calls` WHERE `DialCallStatus` IS NULL AND `DateCreated` < '".$two_hours_ago."'");
        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i = 0;

        global $AccountSid,$AuthToken,$ApiVersion;
        $client = new Services_Twilio($AccountSid,$AuthToken);

        foreach($data as $callsid){
            $i = $i + 1;
            $found = false;
            Util::error_log("[CallFix] Progress: $i/$count");

            foreach ($client->account->calls->getIterator(0, 50, array(
                'ParentCallSid' => $callsid->CallSid
            )) as $call) {
                Util::error_log("[CallFix] Child CallSid: ".$call->sid);
                $found = true;
                unset($_REQUEST);
                $_REQUEST['CallSid']          = $call->parent_call_sid;
                $_REQUEST['DialCallSid']      = $call->sid;
                $_REQUEST['DialCallDuration'] = $call->duration;
                $_REQUEST['DialCallTo']       = $call->to;
                $_REQUEST['DialCallStatus']   = $call->status;
                $rec_uri = "";
                foreach($client->account->recordings->getIterator(0, 50, array('CallSid' => $call->parent_call_sid)) as $recording) {
                    $rec_uri = $recording->uri;
                    Util::error_log(" -Recording: ".$rec_uri);
                }
                $_REQUEST['RecordingUrl']     = "http://api.twilio.com".$rec_uri;
                $_REQUEST['callsync'] = true;
                $db->save_dialed_call();
            }
            if($found===false){
                $stmt = $db->customExecute("UPDATE calls SET DialCallStatus = 'failed' WHERE CallSid = ?");
                $stmt->execute(array($callsid->CallSid));
            }
        }
    }

    $db->setVar("fix_inprogress_run",-1);
}