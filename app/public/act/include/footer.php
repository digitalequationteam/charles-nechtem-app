<?php
$disable_footer = "no";
if($db->getVar("disable_footer")=="yes")
    $disable_footer = "yes";

if($disable_footer=="no") {
?>
<div id="footer">

     <div id="footer-center"><img width="180" src="images/twilio_logo.png" /></div>

</div>
<?php }
    global $build_number;
?>
<?php
$enable_sidebar = "no";
if($db->getVar("enable_support_link")=="yes" && $_SESSION['permission']>=1)
    $enable_sidebar = "yes";

if($enable_sidebar=="yes") {
    ?>
<a href="http://web1support.com" target="_blank"><div id="support_tab" style="display: block; background-image: url(images/tab_support_right.png); background-color: black; border-color: black;" title="Support" class="SupportTabRight" classname="SupportTabRight">Support</div></a>
<?php } ?>

</div>						<!-- wrapper ends -->

</div>		<!-- #hld ends -->


<!--[if IE]><script type="text/javascript" src="js/excanvas.js"></script><![endif]-->
<script type="text/javascript" src="js/jquery.ui.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.img.preload.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.filestyle.mini.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.date_input.pack.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/facebox.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.visualize.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.visualize.tooltip.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.select_skin.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/ajaxupload.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.pngfix.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.blockUI.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.ui.timepicker.addon.js<?php echo "?".$build_number; ?>"></script>
<?php if(@$_SESSION['permission'] >= 1 || ($_SESSION['prems']['purchase_numbers'] || $_SESSION['prems']['customize_call_flow'])) { ?>
<script type="text/javascript" src="jsmain/admin.js<?php echo "?".$build_number; ?>"></script>
<?php } ?>
<script type="text/javascript" src="jsmain/user.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="jsmain/graphs.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/custom.js<?php echo "?".$build_number; ?>"></script>
<iframe src="include/fix_inprogress_calls.php" style="width: 1px; height: 1px; visibility: hidden;"></iframe>
<?php if($update_needed=="1") { ?>
<script type="text/javascript">
    function updateDismiss(e){
        setCookie("update_dismiss","1",1);
    }
    if(getCookie2("update_dismiss")!="1"){
        setTimeout(function(){
            var elm = $("#upd_mes");
            elm.css("display","block");
            elm.find("div > span:eq(1)").remove();
        },1200);
    }
</script>
<?php } ?>

<?php
    echo "<!-- bn: ".$build_number." -->";
?>