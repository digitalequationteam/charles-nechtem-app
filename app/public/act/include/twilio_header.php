<?php

// @start snippet
require_once dirname(__FILE__) . '/Services/Twilio.php';

if (!defined('TWILIO_APPLICATION_NAME')):

    global $DB_NAME;
    define('TWILIO_APPLICATION_NAME', 'ACT_V_THREE_' . md5($DB_NAME) . "_" . @$_SESSION['user_id']);

endif;

if (!function_exists('s8_get_current_webpage_uri')) {

    /**
     * This function returns the URI of current webpage loaded in browser
     * @return string The url string
     */
    function s8_get_current_webpage_uri() {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

}



if (!function_exists('s8_is_str_blank')) {

    /**
     * Function to check if provided string or variable is blank or not
     * @param string $str the string to check
     * @return boolean return true or false based on check performed
     */
    function s8_is_str_blank($str) {
        if ($str == '' || $str == ' ' || $str == NULL) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

if (!function_exists('generate_twilio_auth_token')):

    /**
     * This function generates the twilio auth token for TWILIO BROWSER APPS
     * @return string Returns string containing twilio generated token
     */
    function generate_twilio_auth_token() {
        Global $AccountSid, $AuthToken;

        //By default, setting the live credentials to use in generating the JS token
        $accountSid = $AccountSid;
        $auth_token = $AuthToken;

        try {
            //Initializing the twilio API
            $token = new Services_Twilio_Capability($accountSid, $auth_token);
            $token->allowClientOutgoing(get_twilio_application_sid($accountSid, $auth_token));
            $token->allowClientIncoming($_SESSION['user_id']);
            // @end snippet
            $output = $token->generateToken(18000);
        } catch (Services_Twilio_RestException $e) {
            //Nothing to do here
            $output = '';
        }

        return $output;
    }

endif;

if (!function_exists('get_twilio_autodial_client_name')):

    /**
     * This function generates the client name for incoming calls
     * @return string Twilio incoming client name
     */
    function get_twilio_autodial_client_name() {
        return 'actautodialclient';
    }

endif;

if (!function_exists('generate_twilio_autodial_auth_token')):

    /**
     * This function generates the twilio auth token for TWILIO BROWSER APPS
     * @return string Returns string containing twilio generated token
     */
    function generate_twilio_autodial_auth_token() {
        Global $AccountSid, $AuthToken;

        //By default, setting the live credentials to use in generating the JS token
        $accountSid = $AccountSid;
        $auth_token = $AuthToken;

        try {
            //Initializing the twilio API
            $token = new Services_Twilio_Capability($accountSid, $auth_token);

            $token->allowClientOutgoing(get_twilio_autodial_application_sid($accountSid, $auth_token));
//            $token->allowClientIncoming(get_twilio_autodial_client_name());
            // @end snippet
            $output = $token->generateToken(18000);
        } catch (Services_Twilio_RestException $e) {
            //Nothing to do here
            $output = '';
        }

        return $output;
    }

endif;

if (!function_exists('generate_twilio_testvoice_auth_token')):

    /**
     * This function generates the twilio auth token for TWILIO BROWSER APPS
     * @return string Returns string containing twilio generated token
     */
    function generate_twilio_testvoice_auth_token() {
        Global $AccountSid, $AuthToken;

        //By default, setting the live credentials to use in generating the JS token
        $accountSid = $AccountSid;
        $auth_token = $AuthToken;

        try {
            //Initializing the twilio API
            $token = new Services_Twilio_Capability($accountSid, $auth_token);

            $token->allowClientOutgoing(get_twilio_testvoice_application_sid($accountSid, $auth_token));
//            $token->allowClientIncoming(get_twilio_autodial_client_name());
            // @end snippet
            $output = $token->generateToken(18000);
        } catch (Services_Twilio_RestException $e) {
            //Nothing to do here
            $output = '';
        }

        return $output;
    }

endif;

if (!function_exists('generate_twilio_recordaudio_auth_token')):

    /**
     * This function generates the twilio auth token for TWILIO BROWSER APPS
     * @return string Returns string containing twilio generated token
     */
    function generate_twilio_recordaudio_auth_token() {
        Global $AccountSid, $AuthToken;

        //By default, setting the live credentials to use in generating the JS token
        $accountSid = $AccountSid;
        $auth_token = $AuthToken;

        try {
            //Initializing the twilio API
            $token = new Services_Twilio_Capability($accountSid, $auth_token);

            $token->allowClientOutgoing(get_twilio_recordaudio_application_sid($accountSid, $auth_token));
//            $token->allowClientIncoming(get_twilio_autodial_client_name());
            // @end snippet
            $output = $token->generateToken(18000);
        } catch (Services_Twilio_RestException $e) {
            //Nothing to do here
            $output = '';
        }

        return $output;
    }

endif;

if (!function_exists('get_twilio_application_sid')):

    /**
     * Retrieving the APPSID by either creating a new application in twilio
     * OR by retrieving it from app created earlier
     * @param string $accountSid Twilio account sid
     * @param string $auth_token Twilio auth token
     * @return string
     */
    function get_twilio_application_sid($accountSid, $auth_token) {
        // Your Account Sid and Auth Token from twilio.com/user/account
        $client = new Services_Twilio($accountSid, $auth_token);

        // Loop over the list of apps and echo a property for each one
        //Setting the appsid to null by default
        $appsid = '';

        $mainapp = "";

        //Checking and retrieving the APP side from twilio account
        //If APP is already created
        foreach ($client->account->applications->getIterator(0, 50, array(
            "FriendlyName" => TWILIO_APPLICATION_NAME
        )) as $app
        ) {
            //Retrieving the APP sid
            $mainapp = $app;
            $voiceurl = $app->voice_url;
            $appsid = $app->sid;
        }

        //If APPSID is not set indicating that app is not already
        if (!isset($appsid) || s8_is_str_blank($appsid)) {

            $callbackurl = dirname(s8_get_current_webpage_uri()) . "/include/twilio_dial_number.php?userid=" . @$_SESSION['user_id'] . "&app=browsersoftphone";

            $callbackurl = str_replace("/i/", "/", dirname(s8_get_current_webpage_uri()) . "/include/twilio_dial_number.php?userid=" . @$_SESSION['user_id'] . "&app=browsersoftphone");

            //Creating a new APP in twilio account with voice URL and VOICE method
            $app = $client->account->applications->create(TWILIO_APPLICATION_NAME, array(
                "VoiceUrl" => $callbackurl,
                "VoiceMethod" => "POST"
            ));

            //Retrieving the APP sid.
            $appsid = $app->sid;
        } else {
            $callbackurl = dirname(s8_get_current_webpage_uri()) . "/include/twilio_dial_number.php?userid=" . @$_SESSION['user_id'] . "&app=browsersoftphone";
            $callbackurl = str_replace("/i/", "/", dirname(s8_get_current_webpage_uri()) . "/include/twilio_dial_number.php?userid=" . @$_SESSION['user_id'] . "&app=browsersoftphone");

            if ($voiceurl != $callbackurl)
                $mainapp->update(array(
                    'VoiceUrl' => $callbackurl
                ));
        }

        //Returning the APP sid to calle rof this function.
        return $appsid;
    }

endif;

if (!function_exists('get_twilio_autodial_application_sid')):

    /**
     * Retrieving the APPSID by either creating a new application in twilio
     * OR by retrieving it from app created earlier
     * @param string $accountSid Twilio account sid
     * @param string $auth_token Twilio auth token
     * @return string
     */
    function get_twilio_autodial_application_sid($accountSid, $auth_token) {
        // Your Account Sid and Auth Token from twilio.com/user/account
        $client = new Services_Twilio($accountSid, $auth_token);

        // Loop over the list of apps and echo a property for each one
        //Setting the appsid to null by default
        $appsid = '';

        $mainapp = "";

        //Checking and retrieving the APP side from twilio account
        //If APP is already created
        foreach ($client->account->applications->getIterator(0, 50, array(
            "FriendlyName" => 'AUTODIAL_' . TWILIO_APPLICATION_NAME
        )) as $app
        ) {
            //Retrieving the APP sid
            $mainapp = $app;
            $voiceurl = $app->voice_url;
            $appsid = $app->sid;
        }

        //If APPSID is not set indicating that app is not already
        if (!isset($appsid) || s8_is_str_blank($appsid)) {

            //Creating a new APP in twilio account with voice URL and VOICE method
            $app = $client->account->applications->create('AUTODIAL_' . TWILIO_APPLICATION_NAME, array(
                "VoiceUrl" => dirname(s8_get_current_webpage_uri()) . "/include/ad_auto_dialer_files/ad_autodial_twiml.php?userid=" . @$_SESSION['user_id'] . "&app=autodialer",
                "VoiceMethod" => "POST",
                "Record" => true,
            ));

            //Retrieving the APP sid.
            $appsid = $app->sid;
        } else {
            if ($voiceurl != dirname(s8_get_current_webpage_uri()) . "/include/ad_auto_dialer_files/ad_autodial_twiml.php?userid=" . @$_SESSION['user_id'] . "&app=autodialer")
                $mainapp->update(array(
                    'VoiceUrl' => dirname(s8_get_current_webpage_uri()) . "/include/ad_auto_dialer_files/ad_autodial_twiml.php?userid=" . @$_SESSION['user_id'] . "&app=autodialer"
                ));
        }

        //Returning the APP sid to calle rof this function.
        return $appsid;
    }

endif;

if (!function_exists('get_twilio_testvoice_application_sid')):

    /**
     * Retrieving the APPSID by either creating a new application in twilio
     * OR by retrieving it from app created earlier
     * @param string $accountSid Twilio account sid
     * @param string $auth_token Twilio auth token
     * @return string
     */
    function get_twilio_testvoice_application_sid($accountSid, $auth_token) {
        // Your Account Sid and Auth Token from twilio.com/user/account
        $client = new Services_Twilio($accountSid, $auth_token);

        // Loop over the list of apps and echo a property for each one
        //Setting the appsid to null by default
        $appsid = '';

        $mainapp = "";

        //Checking and retrieving the APP side from twilio account
        //If APP is already created
        foreach ($client->account->applications->getIterator(0, 50, array(
            "FriendlyName" => 'TESTVOICE_' . TWILIO_APPLICATION_NAME
        )) as $app
        ) {
            //Retrieving the APP sid
            $mainapp = $app;
            $voiceurl = $app->voice_url;
            $appsid = $app->sid;
        }

        //If APPSID is not set indicating that app is not already
        if (!isset($appsid) || s8_is_str_blank($appsid)) {

            //Creating a new APP in twilio account with voice URL and VOICE method
            $app = $client->account->applications->create('TESTVOICE_' . TWILIO_APPLICATION_NAME, array(
                "VoiceUrl" => dirname(s8_get_current_webpage_uri()) . "/include/twilio_test_voice.php",
                "VoiceMethod" => "POST",
                "Record" => true,
            ));

            //Retrieving the APP sid.
            $appsid = $app->sid;
        } else {
            if ($voiceurl != dirname(s8_get_current_webpage_uri()) . "/include/twilio_test_voice.php")
                $mainapp->update(array(
                    'VoiceUrl' => dirname(s8_get_current_webpage_uri()) . "/include/twilio_test_voice.php"
                ));
        }

        //Returning the APP sid to calle rof this function.
        return $appsid;
    }

endif;

if (!function_exists('get_twilio_recordaudio_application_sid')):

    /**
     * Retrieving the APPSID by either creating a new application in twilio
     * OR by retrieving it from app created earlier
     * @param string $accountSid Twilio account sid
     * @param string $auth_token Twilio auth token
     * @return string
     */
    function get_twilio_recordaudio_application_sid($accountSid, $auth_token) {
        // Your Account Sid and Auth Token from twilio.com/user/account
        $client = new Services_Twilio($accountSid, $auth_token);

        // Loop over the list of apps and echo a property for each one
        //Setting the appsid to null by default
        $appsid = '';

        $mainapp = "";

        //Checking and retrieving the APP side from twilio account
        //If APP is already created
        foreach ($client->account->applications->getIterator(0, 50, array(
            "FriendlyName" => 'RECORDAUDIO_' . TWILIO_APPLICATION_NAME
        )) as $app
        ) {
            //Retrieving the APP sid
            $mainapp = $app;
            $voiceurl = $app->voice_url;
            $appsid = $app->sid;
        }

        //If APPSID is not set indicating that app is not already
        if (!isset($appsid) || s8_is_str_blank($appsid)) {

            //Creating a new APP in twilio account with voice URL and VOICE method
            $app = $client->account->applications->create('RECORDAUDIO_' . TWILIO_APPLICATION_NAME, array(
                "VoiceUrl" => dirname(s8_get_current_webpage_uri()) . "/include/twilio_record_audio.php",
                "VoiceMethod" => "POST",
                "Record" => true,
            ));

            //Retrieving the APP sid.
            $appsid = $app->sid;
        } else {
            if ($voiceurl != dirname(s8_get_current_webpage_uri()) . "/include/twilio_record_audio.php")
                $mainapp->update(array(
                    'VoiceUrl' => dirname(s8_get_current_webpage_uri()) . "/include/twilio_record_audio.php"
                ));
        }

        //Returning the APP sid to calle rof this function.
        return $appsid;
    }

endif;

if (!function_exists('s8_add_params_to_url')):

    /**
     * This function will return the new URL containigng the paramater specified
     * @param string $url The url on which parameter will be appended
     * @param array $params An associate array containing key=>value pairs of url parameters
     * @return mixed The url containing the paramters. Boolean FALSE if $url argument is blank
     */
    function s8_add_params_to_url($url, $params = array()) {
        //Declaring the output variable which will be returned to caller of this function
        $output = '';

        //Checking if url is not blank
        if (!s8_is_str_blank($url)) {

            //By default, Setting the "?" as need between URL and params
            $needle = '?';

            //Retrieving if there are any parameters in URL
            $url_params = parse_url($url, PHP_URL_QUERY);

            //Retrieving the fragment of URL qithout query string
            $url = parse_url($url, PHP_URL_FRAGMENT);

            //Merging both parameters of URL and $params argument of function.
            parse_str($url_params, $url_params);

            //Generating a final unique list of parmaters to be appended with url
            $params = array_unique(array_merge($url_params, $params));

            //Building the query string from final parameters list
            $query_string = http_build_query($params);

            //Storing the final URL with params in $output variable
            $output = $url . $needle . $query_string;
        } else {

            //If $url argument is NULL, We are storing the boolean false as output in $output variable
            $output = false;
        }

        //Returning the output generated
        return $output;
    }



endif;