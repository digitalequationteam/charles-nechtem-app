<?php
header('Content-type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8" ?>';

$voice = isset($_REQUEST['voice']) ? $_REQUEST['voice'] : "man";
$language = isset($_REQUEST['language']) ? $_REQUEST['language'] : "en";
$text = $_REQUEST['text'];

$language = str_replace("|W", "", $language);
$language = str_replace("|M", "", $language);

if (empty($text)) {
	$text = "Please enter some text first.";
	$voice = "man";
	$language = "en";
}
?>
<Response>
    <Say voice="<?php echo $voice; ?>" language="<?php echo $language; ?>"><?php echo $text; ?></Say>
</Response>