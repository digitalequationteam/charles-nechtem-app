<?php


// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>ACT Error</title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->
</head>




<body>

<div id="hld">

    <div class="wrapper">		<!-- wrapper begins -->
        <div class="block small center login">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>Error</h2>
                <ul>
                    <li></li>
                </ul>
            </div>		<!-- .block_head ends -->


            <div class="block_content">

            <p>Twilio Account SID and/or Auth Token are missing.
                Please add them to "/includes/config.php"<br><br/>

                If you need assistance, please visit: <a href="http://support.web1.co/index.php?/Knowledgebase/Article/View/63/0/error-twilio-account-sid-andor-auth-token-are-missing" target="_blank">Our Support Desk</a>
            </p>

            </div>		<!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>

        </div>		<!-- .login ends -->

    </div>						<!-- wrapper ends -->

</div>		<!-- #hld ends -->


<!--[if IE]><script type="text/javascript" src="js/excanvas.js"></script><![endif]-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.img.preload.js"></script>
<script type="text/javascript" src="js/jquery.filestyle.mini.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/jquery.date_input.pack.js"></script>
<script type="text/javascript" src="js/facebox.js"></script>
<script type="text/javascript" src="js/jquery.visualize.js"></script>
<script type="text/javascript" src="js/jquery.visualize.tooltip.js"></script>
<script type="text/javascript" src="js/jquery.select_skin.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/ajaxupload.js"></script>
<script type="text/javascript" src="js/jquery.pngfix.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

</body>
</html>