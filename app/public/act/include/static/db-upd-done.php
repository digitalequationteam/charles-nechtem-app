<?php


// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>ACT Info</title>

    <meta name="robots" content="noindex">
    <style type="text/css" media="all">
        @import url("../css/style.css");
        @import url("../css/jquery.wysiwyg.css");
        @import url("../css/facebox.css");
        @import url("../css/visualize.css");
        @import url("../css/date_input.css");
        @import url("../css/Aristo/Aristo.css");
    </style>
    <style>
        #newUserDialog { font-size:62% }
        .ui-dialog label, input { display:block !important; font-weight: bold; }
        .ui-dialog p { padding:5px !important; }
        .ui-dialog input.text { margin-bottom:12px !important; width:95% !important; padding: .4em !important; }
        .ui-dialog fieldset { padding:0 !important; border:0 !important; margin-top:25px !important; }
        div#users-contain { width: 350px; margin: 20px 0; }
        div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
        div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
        .ui-dialog .ui-state-error { padding: .3em !important; }
        .validateTips { border: 1px solid transparent; padding: 0.3em; }
        div.jp-audio, div.jp-video { font-size:1.25em; }
        .annotatedtimelinetable td {padding:0 !important;}
        #report_logo { margin-bottom:10px; }
    </style>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("../css/ie.css");</style><![endif]-->
</head>




<body>

<div id="hld">

    <div class="wrapper">		<!-- wrapper begins -->
        <div class="block small center login">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>Info</h2>
                <ul>
                    <li></li>
                </ul>
            </div>		<!-- .block_head ends -->


            <div class="block_content">

                <p>The database was updated successfully. You can Login below.</p>
                <form action="../index.php" method="get">
                    <center><a href="../index.php" style="display: block;"><input type="submit" style="" class="submit long" value="Login"></a></center>
                </form>
                <br/>
            </div>		<!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>

        </div>		<!-- .login ends -->

    </div>						<!-- wrapper ends -->

</div>		<!-- #hld ends -->


<!--[if IE]><script type="text/javascript" src="../js/excanvas.js"></script><![endif]-->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.img.preload.js"></script>
<script type="text/javascript" src="../js/jquery.filestyle.mini.js"></script>
<script type="text/javascript" src="../js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="../js/jquery.date_input.pack.js"></script>
<script type="text/javascript" src="../js/facebox.js"></script>
<script type="text/javascript" src="../js/jquery.visualize.js"></script>
<script type="text/javascript" src="../js/jquery.visualize.tooltip.js"></script>
<script type="text/javascript" src="../js/jquery.select_skin.js"></script>
<script type="text/javascript" src="../js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="../js/ajaxupload.js"></script>
<script type="text/javascript" src="../js/jquery.pngfix.js"></script>
<script type="text/javascript" src="../js/custom.js"></script>

</body>
</html>