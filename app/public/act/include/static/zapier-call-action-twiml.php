<?php

header('Content-type: text/xml');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

$message = isset($_REQUEST['message']) ? $_REQUEST['message'] : "";
$company_num = isset($_REQUEST['company_num']) ? $_REQUEST['company_num'] : "";
$caller = isset($_REQUEST['lead_number']) ? $_REQUEST['lead_number'] : "";
echo '<?xml version="1.0" encoding="UTF-8"?>'."\r\n";
?>
<Response>
<?php if(isset($_REQUEST['Digits']) && $_REQUEST['Digits']=="1"){ ?>
    <Dial method="POST" action="../../record_call.php?DialCallTo=<?php echo urlencode($company_num); ?>">
        <Number><?php echo $caller; ?></Number>
    </Dial>
<?php }else{ ?>
    <Gather timeout="5" numDigits="1">
        <Say><?php echo $message; ?></Say>
        <Say>Please press 1 to connect.</Say>
    </Gather>
    <Gather timeout="5" numDigits="1">
        <Say><?php echo $message; ?></Say>
        <Say>Please press 1 to connect.</Say>
    </Gather>
    <Gather timeout="5" numDigits="1">
        <Say><?php echo $message; ?></Say>
        <Say>Please press 1 to connect.</Say>
    </Gather>
    <Gather timeout="5" numDigits="1">
        <Say><?php echo $message; ?></Say>
        <Say>Please press 1 to connect.</Say>
    </Gather>
    <Gather timeout="10" numDigits="1">
        <Say><?php echo $message; ?></Say>
        <Say>Please press 1 to connect.</Say>
    </Gather>
    <Gather timeout="20" numDigits="1">
        <Say><?php echo $message; ?></Say>
        <Say>Please press 1 to connect.</Say>
    </Gather>
<?php } ?></Response>