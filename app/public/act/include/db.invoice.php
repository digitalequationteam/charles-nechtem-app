<?php

require_once(realpath(dirname(__FILE__))."/db.connection.php");

/**
 * Database invoices class encapsulating invoices methods.
 */
class Invoice {

	private $_dbConnection;

	public function __construct() {
		
		$this->_dbConnection = DBConnection::getConnection();
	}

    public static function interpolateQuery($query, $params) {
        $keys = array();

        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }
        }

        $query = preg_replace($keys, $params, $query, 1, $count);

        #trigger_error('replaced '.$count.' keys');

        return $query;
    }

    public function getPending($invoice_id){
        $stmt = $this->_dbConnection->prepare("SELECT is_pending FROM invoices WHERE id = ?");
        $stmt->execute(array($invoice_id));

        $pending = $stmt->fetch(PDO::FETCH_OBJ);

        return $pending->is_pending;
    }

    public function setPending($status, $invoice_id)
    {
        $stmt = $this->_dbConnection->prepare("UPDATE invoices SET is_pending = ? WHERE id = ?");
        $stmt->execute(array($status, $invoice_id));
    }

    public function getDeclined($invoice_id){
        $stmt = $this->_dbConnection->prepare("SELECT is_declined FROM invoices WHERE id = ?");
        $stmt->execute(array($invoice_id));

        $pending = $stmt->fetch(PDO::FETCH_OBJ);

        return $pending->is_declined;
    }

    public function setDeclined($status, $invoice_id)
    {
        $stmt = $this->_dbConnection->prepare("UPDATE invoices SET is_declined = ? WHERE id = ?");
        $stmt->execute(array($status, $invoice_id));
    }
	
	/**
	 * Get invoices.
	 */
	public function getInvoices($data) {
		
		global $TIMEZONE;

		//the sql statement.
		$sql = "select cl.*,
					inv.*,
					date,
					date_format(inv.date, '%Y-%m-%d') date_mysql_format,
					date_format(inv.from_date, '%m-%d-%Y') from_date,
					to_date,
					inv.from_date as from_date_sql,
					inv.to_date as to_date_sql
					from invoices inv
					left join clients cl on inv.client_id = cl.id

					WHERE inv.is_pending = 0
					";
				
		if (isset($data["client_id"]) || isset($data["invoice_id"]) || isset($data["invoice_type"])) {
			
			$conditionsArray = array();
			$paramsArray = array();
			
			if (isset($data["client_id"])) {
				
				$conditionsArray[] = "inv.client_id = ?";
				$paramsArray[] = $data["client_id"];
			}
			
			if (isset($data["invoice_id"])) {
				
				$conditionsArray[] = "inv.id = ?";
				$paramsArray[] = $data["invoice_id"];
			}
			
			if (isset($data["invoice_type"])) {
				
				$conditionsArray[] = "active = ?";
				
				$invoiceType = $data["invoice_type"];
			
				$invoiceActive = "1";
				
				$paramsArray[] = $invoiceActive;
			}
			
			$sql .= " AND " . implode(" and ", $conditionsArray) . " ";
		}
					
		$sql .=	" order by inv.date desc ";
				
		if (isset($data["page"]) && isset($data["page_size"])) {
			
			$page = $data["page"];
			$pageSize = $data["page_size"];
			
			$limitOffset = $page * $pageSize;
			
			$limitSql = " limit {$limitOffset} , {$pageSize} ";
			
			$sql .= $limitSql;
		}

        //die($this->interpolateQuery($sql,$paramsArray));

		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		if (isset($data["client_id"]) || isset($data["invoice_id"]) || isset($data["invoice_type"])) {
			
			//execute the sql statement.
			$result->execute($paramsArray);
		}
		else {
			
			//execute the sql statement.
			$result->execute();
		}
		
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		foreach ($result as $key => $item) {
            $tz = new DateTimeZone($TIMEZONE);
			$date = new DateTime($result[$key]['date'], new DateTimeZone("UTC"));
		    $date->setTimezone($tz);

			$to_date = new DateTime($result[$key]['to_date']);
			$to_date->modify("-1 day");
		    
		    $result[$key]['date'] = $date->format("m-d-Y H:i:s");
		    $result[$key]['to_date'] = $to_date->format("m-d-Y");
		}
		
		return ($result);
	}
	
	/**
	 * Get invoices.
	 */
	public function getInvoicesCount($data) {
		
		//the sql statement.
		$sql = "select count(*) invoices_count from invoices";
		
		if (isset($data["client_id"])) {
			
			$sql .= " where client_id = ? and active = ? ";
		}
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		if (isset($data["client_id"])) {
			
			$invoiceType = $data["invoice_type"];
			
			$invoiceActive = ($invoiceType == "normal" ? "1" : "0");
			
			//execute the sql statement.
			$result->execute(array(
				$data["client_id"],
				$invoiceActive
			));
		}
		else if (isset($data["all"])) {
			
			//execute the sql statement.
			$result->execute();
		}
		
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return ($result[0]["invoices_count"]);
	}
	
	/**
	 * Add invoice.
	 */
	public function addInvoice($data) {

		//the sql statement.
		$sql = "insert into invoices (
			client_id, date, from_date, to_date, amount_currency, amount_paid, invoice_details, sess_key
			)
			values (?, ?, ?, ?, ?, ?, ?, ?)
		";

		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);

        $rand_key = md5("act22".microtime());

		//execute the sql statement.
		$result->execute(array(
			$data["client_id"],
			$data["date"],
			$data["from_date"],
			$data["to_date"],
			$data["amount_currency"],
			$data["amount_paid"],
			$data["invoice_details"],
            $rand_key
		));
        return array($this->_dbConnection->lastInsertId('id'),$rand_key);
	}

    /**
     * Re-insert Invoice
     */
    public function regenerateInvoice($data) {

        //the sql statement.
        $sql = "replace into invoices (
			id, client_id, date, from_date, to_date, amount_currency, amount_paid, invoice_details, sess_key
			)
			values (?,?,?,?,?,?,?,?,?)
		";

        //prepare the sql statement.
        $result = $this->_dbConnection->prepare($sql);

        $rand_key = md5("act22".microtime());

        //execute the sql statement.
        $result->execute(array(
            $data['invoice_id'],
            $data["client_id"],
            $data["date"],
            $data["from_date"],
            $data["to_date"],
            $data["amount_currency"],
            $data["amount_paid"],
            $data["invoice_details"],
            $rand_key
        ));
        return $rand_key;
    }

    /**
	 * Update invoice payment info.
	 */
	public function updateInvoicePaymentInfo($data) {
		
		//the sql statement.
		$sql = "update invoices set
			amount_paid = ?,
			is_paid = 1,
			is_pending = 0
			where id = ?
		";
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute(array(
			$data["amount_paid"],
			$data["invoice_id"]
		));
	}
	
	/**
	 * Mark the invoice as deleted.
	 */
	public function deleteInvoice($data) {
		
		//the sql statement.
		$sql = "update invoices set
			active = 0
			where id = ?
		";
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute(array(
			$data["invoice_id"]
		));
	}
}

?>