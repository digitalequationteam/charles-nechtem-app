<?php
session_start();
require_once('util.php');

$db = new DB();
if (@$_SESSION['permission'] < 1)
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);
else
    $companies = $db->getAllCompanies();

if (@$_GET['sd'] != "" && (@$_GET['ed'] == "" || @$_GET['cm'] == "")) {
?>
	<script type="text/javascript">
	alert('An error has occured. Please contact support.');
	</script>
<?php
    exit;
}

//Declare shore-hand variables
$start_date      = ($_GET['sd']/1000);
$end_date        = ($_GET['ed']/1000)+86399;

global $TIMEZONE;
$tz = new DateTimeZone($TIMEZONE);

if (strlen($_GET['sd']) < 10) $_GET['sd'] = substr($_GET['sd'], 4, 4)."-".substr($_GET['sd'], 0, 2)."-".substr($_GET['sd'], 2, 2)." 00:00:00";
if (strlen($_GET['ed']) < 10) $_GET['ed'] = substr($_GET['ed'], 4, 4)."-".substr($_GET['ed'], 0, 2)."-".substr($_GET['ed'], 2, 2)." 00:00:00";

$start_date = new DateTime($_GET['sd']);
$year = $start_date->format("Y");
$month = $start_date->format("n");
$day = $start_date->format("j");
$d1 = strtotime($start_date->format("Y-m-d H:i:s"));
$start_date->setTimezone($tz);
$d2 = strtotime($start_date->format("Y-m-d H:i:s"));
$seconds_difference = abs($d2 - $d1);
$start_date->setDate($year, $month, $day);
$start_date->setTime(0, 0, 0);

$end_date = new DateTime($_GET['ed']);
$year = $end_date->format("Y");
$month = $end_date->format("n");
$day = $end_date->format("j");
$end_date->setTimezone($tz);
$end_date->setDate($year, $month, $day);
$end_date->setTime(23, 59, 59);

$start_date = strtotime($start_date->format("Y-m-d H:i:s")) + $seconds_difference;
$end_date = strtotime($end_date->format("Y-m-d H:i:s")) + $seconds_difference;

$outgoing = (isset($_GET['outgoing']) ? $_GET['outgoing'] : false);
    
$company = $_GET['cm'];
$call_result = $_GET['cr'];
$phone_code = $_GET['pc'];

$agent           = $_GET['agent'];

if (!empty($_GET['company'])) $company = $db->getCompanyId($_GET['company']);

$filter_user = @$_GET['user'];;

$report_campaign     = (empty($_GET['ca']) ? "" : $_GET['ca']);

if(isset($_GET['on']) && $_GET['on']!="")
    $outgoing_number = "+".$_GET['on'];
else
    $outgoing_number = "";

//Check if user belongs to company
if(!$db->isUserInCompany($company,$_SESSION['user_id']) and $_SESSION['permission'] < 1)
{
?>
	<script type="text/javascript">
	alert('An error has occured. You don\'t belong to that company! Redirecting.');
	window.location = "call_report.php";
	</script>
<?php
			exit;
}

$order_by = (empty($_REQUEST['order_by']) ? "DateCreated" : $_REQUEST['order_by']);
$order_type = (empty($_REQUEST['order_type']) ? "DESC" : $_REQUEST['order_type']);

$records = $db->getCallReportforExport($_SESSION['user_id'], $start_date, $end_date, $company, $call_result, $phone_code, $outgoing_number, $outgoing, $filter_user, @$report_campaign, $agent, $order_by, $order_type);
if ($records[0]) {
    $file_name = "Call_Tracking_Report";

    if (!empty($agent)) {
        $file_name = $file_name."_".$agent;
    }
    $file_name .= "_".time();

    header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=" . $file_name . ".csv");
    header("Pragma: no-cache");
    header("Expires: 0");

    if($company=="-1")
        echo "#Date, Campaign, Company, From, To, Rang, From City, From State, From Zip, Duration (seconds), Status, Phone Code, Recording URL\r\n";
    else
        echo "#Date, Campaign, From, To, Rang, From City, From State, From Zip, Duration (seconds), Status, Phone Code, Recording URL\r\n";

	$twilio_numbers=Util::get_all_twilio_numbers();

    $out = fopen('php://output', 'w');

    foreach ($records[1] as $call) {
        if (!$outgoing) {
            if (array_key_exists(format_phone($call['CallTo']), $twilio_numbers))
                $campaign = $twilio_numbers[format_phone($call['CallTo'])];
            else
                $campaign = "";
        }
        else {
            if (array_key_exists(format_phone($call['CallFrom']), $twilio_numbers))
                $campaign = $twilio_numbers[format_phone($call['CallFrom'])];
            else
                $campaign = "";

            $call['DialCallDuration'] = $call['CallDuration'];
        }

        if($call['RecordingUrl']!="")
            $record_url = $call['RecordingUrl'].".mp3";
        else
            $record_url = "";

        if ($db->getVar("mask_recordings") == "true") {
            $record_url = Util::maskRecordingURL($record_url);
        }

        $tz = new DateTimeZone($TIMEZONE);
        $date = new DateTime($call['DateCreated']);
        $date->setTimezone($tz);
        //$call['DateCreated'] = $date->format("D n\/j Y g\:iA");
        $call['DateCreated'] = $date->format("n/j/y G:i");

        if($company=="-1")
        {
            $company_name = "";
            if(!Util::isNumberIntl($call['CallTo']))
            {
                $company_name = $db->getCompanyName($db->getCompanyOfNumber($db->format_phone_db($call['CallTo'])));
            }else{
                $company_name = $db->getCompanyName($db->getCompanyOfNumber(substr($call['CallTo'],1,(strlen($call['CallTo'])-1))));
            }
            $csv_row_data = array($call['DateCreated'], $campaign, $company_name, $call['CallFrom'], $call['CallTo'], $call['DialCallTo'], $call['FromCity'],$call['FromState'],
                $call['FromZip'], $call['DialCallDuration'], $call['DialCallStatus'], $db->getPhoneCodeName($call['PhoneCode']),
                $record_url);
        }
        else
            $csv_row_data = array($call['DateCreated'], $campaign, $call['CallFrom'], $call['CallTo'], $call['DialCallTo'], $call['FromCity'],$call['FromState'],
                $call['FromZip'], $call['DialCallDuration'], $call['DialCallStatus'], $db->getPhoneCodeName($call['PhoneCode']),
                $record_url);

        $notes = $db->getCallNotes($call['CallSid']);

        foreach($notes as $note)
        {
            $tz = new DateTimeZone($TIMEZONE);
            $date = new DateTime($note['DateAdded']);
            $date->setTimezone($tz);

            array_push($csv_row_data,"[".$note['User']." @ ".$date->format("n\/j\/y")."]: ".$note['NoteContents']);
        }

        fputcsv($out, $csv_row_data);
    }
    fclose($out);
} else {
    switch ($records[1]) {
        case "AUTH_FAIL": {
?>
		<script type="text/javascript">
            alert('You don\'t have access to these Records.');
		</script>
<?php
            break;
        }
        default: {
?>
		<script type="text/javascript">
		alert('You don\'t have access to these Records.');
		</script>
<?php
            break;
        }
            
    }
}


?>