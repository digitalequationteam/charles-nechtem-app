<?php

require_once(realpath(dirname(__FILE__))."/db.connection.php");

/**
 * Database companies class encapsulating companies methods.
 */
class Company {

	private $_dbConnection;

	public function __construct() {
		
		$this->_dbConnection = DBConnection::getConnection();
	}
	
	/**
	 * Get companies.
	 */
	public function getCompanies() {
		
		//the sql statement.
		$sql = "select * from companies";
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute();
		
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return ($result);
	}
	
	/**
	 * Get outgoing numbers for all companies.
	 */
	public function getOutgoingNumbers($data) {
		
		//the sql statement.
		$sql = "select * from
				(
				select company_id as company_id,
					number as number
				from cf_round_robin
				union
				select company_id as company_id,
					number as number
				from cf_multiple_numbers
				) query
				";

        $sql2 = "select idx as company_id,assigned_number as `number`, international from companies";
        $sql3 = "select companyId, content from call_ivr_widget where flowtype = 'Dial' and temporary = '0' ";
        $sql4 = "select wId, companyId, content from call_ivr_widget where flowtype = 'RoundRobin' and temporary = '0' ";
        $sql5 = "select wId, companyId, content from call_ivr_widget where flowtype = 'MultipleNumbers' and temporary = '0' ";
		
		if (isset($data["company_id"])) {
			
			$sql .= " where company_id = ?";
            $sql2 .= " where idx = ?";
            $sql3 .= " and companyId = ?";
            $sql4 .= " and companyId = ?";
            $sql5 .= " and companyId = ?";
		}
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		$result2 = $this->_dbConnection->prepare($sql2);
		$result3 = $this->_dbConnection->prepare($sql3);
		$result4 = $this->_dbConnection->prepare($sql4);
		$result5 = $this->_dbConnection->prepare($sql5);

		if (isset($data["company_id"])) {
			
			//execute the sql statement.
			$result->execute(array($data["company_id"]));
            $result2->execute(array($data['company_id']));
            $result3->execute(array($data['company_id']));
            $result4->execute(array($data['company_id']));
            $result5->execute(array($data['company_id']));
		}
		else if (isset($data["all"])) {
			
			//execute the sql statement.
			$result->execute();
            $result2->execute();
            $result3->execute();
            $result4->execute();
            $result5->execute();
		}
		
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
        $result2 = $result2->fetchAll(PDO::FETCH_ASSOC);
		$result3 = $result3->fetchAll(PDO::FETCH_ASSOC);
		$result4 = $result4->fetchAll(PDO::FETCH_ASSOC);
		$result5 = $result5->fetchAll(PDO::FETCH_ASSOC);

		if ($result3) {
			foreach ($result3 as $dial_result) {
				if (!empty($dial_result['content'])) {
					$content = json_decode($dial_result['content'], true);
					if (!empty($content['open_number'])) {
						$result[] = array(
							"company_id" => $dial_result['companyId'],
							"number" => $content['open_number'],
							"international" => (int)$content['open_number_int']
						);
					}
					if (!empty($content['close_number'])) {
						$result[] = array(
							"company_id" => $dial_result['companyId'],
							"number" => $content['close_number'],
							"international" => (int)$content['close_number_int']
						);
					}
				}
			}
		}

		if ($result4) {
			foreach ($result4 as $rr_result) {
        		$sqlrr = "select number from call_ivr_round_robin where wId = ? ";
				$resultrr = $this->_dbConnection->prepare($sqlrr);
            	$resultrr->execute(array($rr_result['wId']));
				$resultrrs = $resultrr->fetchAll(PDO::FETCH_ASSOC);

				foreach ($resultrrs as $resultrr) {
					if (!empty($resultrr['number'])) {
						$result[] = array(
							"company_id" => $rr_result['companyId'],
							"number" => $resultrr['number'],
							"international" => (substr($resultrr['number'], 0, 2) == "+1") ? 0 : 1
						);
					}
				}
			}
		}

		if ($result5) {
			foreach ($result5 as $mn_result) {
        		$sqlmn = "select number from call_ivr_multiple_numbers where wId = ? ";
				$resultmn = $this->_dbConnection->prepare($sqlmn);
            	$resultmn->execute(array($mn_result['wId']));
				$resultmns = $resultmn->fetchAll(PDO::FETCH_ASSOC);

				foreach ($resultmns as $resultmn) {
					if (!empty($resultmn['number'])) {
						$result[] = array(
							"company_id" => $mn_result['companyId'],
							"number" => $resultmn['number'],
							"international" => (substr($resultmn['number'], 0, 2) == "+1") ? 0 : 1
						);
					}
				}
			}
		}
		
		$resultFiltered = array();
		
		//loop through the result.
		for ($i = 0; $i < count($result); $i++) {
			
			//the number could be null because of the first query.
			if (!is_null($result[$i]["number"])) {

				$exists_in_parent = false;

				for ($j = 0; $j < count($result2); $j++) {
					if(!is_null($result2[$j]["number"])) {
						$company_num = $result2[$j]["number"];
						$company_num2 = "1".$result2[$j]["number"];

						if (($company_num == $result[$i]["number"] || $company_num2 == $result[$i]["number"]) && $result[$i]["company_id"] == $result2[$j]["company_id"]) {
							$exists_in_parent = true;
						}
					}
				}

				if (!$exists_in_parent) {
					$resultFiltered[] = $result[$i];
				}
			}
 		}
        for ($i = 0; $i < count($result2); $i++) {
            if(!is_null($result2[$i]["number"])) {

                $resultFiltered[] = $result2[$i];
            }
        }
		
		return ($resultFiltered);
	}
	
	/**
	 * Get phone codes for all companies.
	 */
	public function getPhoneCodes() {
		
		//the sql statement.
		$sql = "select * from company_phone_code";
		
		//prepare the sql statement.	
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute();
		
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return ($result);
	}
	
	/**
	 * Get email tags for all companies.
	 */
    function getEmailTags() {
    	//the sql statement.
		$sql = "select * from tracking_emails_tags";
		
		//prepare the sql statement.	
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute();
		
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		foreach ($result as $key=>$value) {
			$sql = "select * from tracking_emails WHERE id = ?";
		
			//prepare the sql statement.	
			$result2 = $this->_dbConnection->prepare($sql);
			
			//execute the sql statement.
			$result2->execute(array($result[$key]['email_id']));
			
			$result2 = $result2->fetch(PDO::FETCH_ASSOC);

			$result[$key]['company_id'] = $result2['company_id'];
		}
		
		return ($result);
    }
}

?>