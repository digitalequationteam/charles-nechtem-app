<?php
error_reporting(E_ALL);
ini_set('display_errors', true);

class Real_PDO extends PDO {
    public function quote($value, $parameter_type = PDO::PARAM_STR ) {
        if( is_null($value) ) {
            return "NULL";
        }
        return parent::quote($value, $parameter_type);
    }
    public function escape($value){
        return stripslashes(str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array("", '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $value));
    }
}

class DateTime_52 extends DateTime{
    /**
     * Set the time of the datetime object by a unix timestamp
     * @param int $unixtimestamp
     * @return DateTime_52
     */
    public function setTimestamp($unixtimestamp){
        if(!is_numeric($unixtimestamp) && !is_null($unixtimestamp)){
            trigger_error('DateTime::setTimestamp() expects parameter 1 to be long, '.gettype($unixtimestamp).' given', E_USER_WARNING);
        } else {
            $this->setDate(date('Y', $unixtimestamp), date('n', $unixtimestamp), date('d', $unixtimestamp));
            $this->setTime(date('G', $unixtimestamp), date('i', $unixtimestamp), date('s', $unixtimestamp));
        }
        return $this;
    }
    /**
     * Get the time of the datetime object as a unix timestamp
     * @return int a unix timestamp representing the time in the datetime object
     */
    public function getTimestamp(){
        return $this->format('U');
    }
}
$forversion = 0;
function ConstructT($s){
    class DatabaseVersion {

        function get()
        {
            $db = new DB();
            if($db->getVar("arcver")==false)
            {
                header("Location: checkversion.php");
                die();
            }else{
                $arcver = unserialize(base64_decode($db->getVar("arcver")));

                if($arcver->lc==false)
                {
                    $lcl = $this->checkWithAPI();

                    if(!$lcl)
                    {
                        header("Location: checkversion.php?failed=2");
                        die();
                    }else{

                        $arcver->lc = strtotime(date('Y-m-d'));
                        $arcver->lcl = $lcl;
                        $db->setVar("arcver",base64_encode(serialize($arcver)));
                        return $arcver->lcl;
                    }
                }else{
                    $currdate = strtotime(date('Y-m-d'));
                    $date = strtotime('+ 1 day',$arcver->lc);
                    //echo $date." - ".$currdate;
            
                    if ($date < $currdate) {
                        $lcl = $this->checkWithAPI();

                        if(!$lcl)
                        {
                            header("Location: checkversion.php?failed=3");
                            die();
                        }else{
                            $arcver->lc = $currdate;
                            $arcver->lcl = $lcl;
                            $db->setVar("arcver",base64_encode(serialize($arcver)));
                            return $arcver->lcl;
                        }
                    }

                    if($arcver->lc >= 2000000000)
                    {
                        $lcl = $this->checkWithAPI();

                        if(!$lcl)
                        {
                            header("Location: checkversion.php?failed=4");
                            die();
                        }else{
                            $arcver->lc = $currdate;
                            $arcver->lcl = $lcl;
                            $db->setVar("arcver",base64_encode(serialize($arcver)));
                            return $arcver->lcl;
                        }
                    }
                    return $arcver->lcl;
                }
            }
        }

        function checkWithAPI()
        {
            $dir = './include/config.php';
            if(!file_exists($dir)){
                $dir = "../include/config.php";
                if(!file_exists($dir)){
                    $dir = "../config.php";
                }
            }
            require_once($dir);
            
            $db = new DB();

            $email = unserialize(base64_decode($db->getVar("arcver")))->lce;

            if(substr($_SERVER["HTTP_HOST"],0,4)=="www.")
                $host = substr($_SERVER["HTTP_HOST"],4,strlen($_SERVER["HTTP_HOST"]-4));
            else
                $host = $_SERVER["HTTP_HOST"];
        
        if (strpos($_SERVER['SCRIPT_NAME'], "/ivr") !== false)
            $host = $host.str_replace($_SERVER['SCRIPT_NAME'],"",$_SERVER['SCRIPT_NAME']);
        else
                $host = $host.str_replace("/".basename($_SERVER['SCRIPT_NAME'],""),"",$_SERVER['SCRIPT_NAME']);

            global $AccountSid;

            $jsonurl = "http://license.web1syndication.com/abc123/checkACTLicense/".urlencode($email)."/".urlencode(base64_encode($host))."/".urlencode($AccountSid);
            //echo $jsonurl; exit;
            $json = $db->curlGetData($jsonurl);
            $json_output = json_decode($json);

            if($json_output->result == "success")
            {
                $url = "http://license.web1syndication.com/abc123/getVersionInfo";

                $json = json_decode($db->curlGetData($url));


                if((int)$db->getVar("product_version")>=(int)$json->intenal_version)
                    $db->setVar("update_needed","0");
                else
                    $db->setVar("update_needed","1");

                return $json_output->lcl;
            }else{
                return false;
            }
        }
    }
}

class DB {
    protected $db;

    function __construct() {
        require_once(realpath(dirname(__FILE__))."/config.php");
        try{
            global $DB_HOST,$DB_NAME,$DB_USER,$DB_PASS;
            //echo "mysql:host=".$DB_HOST.";dbname=".$DB_NAME;
            $this->db = new Real_PDO("mysql:host=".$DB_HOST.";dbname=".$DB_NAME, $DB_USER, $DB_PASS);
        }catch(PDOException $ex)
        {
            die('<font color="red" face="dotum" style="margin-left:5px;"><b>[Database Error]</b></font> <pre style="background:#eee; padding:5px;">'.$ex.'</pre>');
        }
    }

    public function getDb(){
        return $this->db;
    }

    /**
     * @param $phone
     * @return string
     */
    public function format_phone_db($phone)
    {
        if(substr($phone,0,2)=="+1")
            $phone = substr($phone,2,(strlen($phone)-2));

        if(substr($phone,0,1)=="+")
            $phone = substr($phone,1,(strlen($phone)-1));

        return $phone;
    }

    public function query($query) {
        $this->db->query($query);
    }

    function save_outgoing_call_api ($call_data)
    {
        $stmt = $this->db->prepare("INSERT INTO outbound_calls (CallSid,AccountSid,CallStatus,CallTo,CallFrom,RecordingUrl,Direction,ApiVersion,DialCallSid,CallDuration,DialCallStatus,DateCreated,AnsweredBy,MadeUsing,UserSource) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

        if($stmt->execute($call_data))
            return true;
        else
            return false;
    }

    function save_call_api ($call_data)
    {
        $stmt = $this->db->prepare("INSERT INTO calls (CallSid,AccountSid,CallStatus,CallTo,CallFrom,RecordingUrl,Direction,ApiVersion,DialCallSid,DialCallDuration,DialCallStatus,DateCreated,DialCallTo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);");

        if($stmt->execute($call_data))
            return true;
        else
            return false;
    }

    function format_phone_us($phone = '', $convert = true, $trim = true)
    {
        if (empty($phone)) {
            return false;
        }

        $phone = preg_replace("/[^0-9A-Za-z]/", "", $phone);
        $OriginalPhone = $phone;

        if ($trim == true && strlen($phone)>11) {
            $phone = substr($phone, 0, 11);
        }

        if ($convert == true && !is_numeric($phone)) {
            $replace = array('2'=>array('a','b','c'),
                '3'=>array('d','e','f'),
                '4'=>array('g','h','i'),
                '5'=>array('j','k','l'),
                '6'=>array('m','n','o'),
                '7'=>array('p','q','r','s'),
                '8'=>array('t','u','v'),
                '9'=>array('w','x','y','z'));

            foreach($replace as $digit=>$letters) {
                $phone = str_ireplace($letters, $digit, $phone);
            }
        }

        $length = strlen($phone);
        switch ($length) {
            case 7:
                // Format: xxx-xxxx
                return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1-$2", $phone);
            case 10:
                // Format: (xxx) xxx-xxxx
                return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "($1) $2-$3", $phone);
            case 11:
                // Format: x(xxx) xxx-xxxx
                return preg_replace("/([0-9a-zA-Z]{1})([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1($2) $3-$4", $phone);
            default:
                // Return original phone if not 7, 10 or 11 digits long
                return $OriginalPhone;
        }
    }

    /**
     * @return array|PDOException
     */
    function save_call() {
        require_once("config.php");
        //http://www.twilio.com/docs/api/twiml/twilio_request#synchronous-request-parameters
        try{
            $CallSid = @$_REQUEST['CallSid'];
            $AccountSid=@$_REQUEST['AccountSid'];
            $CallFrom=@$_REQUEST['From'];
            $CallTo=@$_REQUEST['To'];
            $CallStatus=@$_REQUEST['CallStatus'];
            $ApiVersion=@$_REQUEST['ApiVersion'];
            $Direction=@$_REQUEST['Direction'];

            $CallFrom = str_replace(" ", "+",  $CallFrom);
            $CallTo = str_replace(" ", "+",  $CallTo);
            
            if (isset($_REQUEST['FromCity'])){
                $FromCity=$_REQUEST['FromCity'];
                $FromState=$_REQUEST['FromState'];
                $FromZip=$_REQUEST['FromZip'];
                $FromCountry=$_REQUEST['FromCountry'];
            } else {
                $FromCity="";
                $FromState="";
                $FromZip="";
                $FromCountry="";
            }
            if (isset($_REQUEST['FromCity'])){
                $ToCity=$_REQUEST['ToCity'];
                $ToState=$_REQUEST['ToState'];
                $ToZip=$_REQUEST['ToZip'];
                $ToCountry=$_REQUEST['ToCountry'];
            }else{
                $ToCity="";
                $ToState="";
                $ToZip="";
                $FromCountry="";
                $ToCountry="";
            }
            if(@$_REQUEST['DateCreated']!=""){
                $DateCreated = $_REQUEST['DateCreated'];
            }else{
                $DateCreated = date('Y-m-d H:i:s',time());
            }

            $DialCallStatus = empty($_REQUEST['DialCallStatus']) ? "no-answer" : $_REQUEST['DialCallStatus'];

            $SpId = (int)$this->getNumberDetails($CallTo);

            if (!empty($SpId)) {
                $urls = $this->getVisitedUrls($SpId);
                $i = count($urls) - 1;
                $urls[$i]['made_call'] = true;
                $urls = serialize($urls);
                
                $stmt = $this->db->prepare('UPDATE url_visited set urls = :urls WHERE sp_id = :sp_id');
                $stmt->execute(array(':urls' => $urls, ':sp_id' => $SpId));
            }
            
            $stmt = $this->db->prepare('INSERT INTO calls (DateCreated,CallSid,AccountSid,SpId,CallFrom,CallTo,CallStatus,ApiVersion,Direction,FromCity,FromState,FromZip,FromCountry,ToCity,ToState,ToZip,ToCountry,DialCallStatus) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);');
            $vars=array( $DateCreated,$CallSid,$AccountSid,$SpId,$CallFrom,$CallTo,$CallStatus,$ApiVersion,$Direction,$FromCity,$FromState,$FromZip,$FromCountry,$ToCity,$ToState,$ToZip,$ToCountry,$DialCallStatus);
            $stmt->execute($vars);
        }catch(PDOException $ex){
            return $ex;
        }
        $arr = $stmt->errorInfo();
        return $arr;
    }

    function save_outgoing_call() {
        require_once(realpath(dirname(__FILE__))."/config.php");
        //http://www.twilio.com/docs/api/twiml/twilio_request#synchronous-request-parameters
        try{
            $CallSid = @$_REQUEST['CallSid'];
            $AccountSid=@$_REQUEST['AccountSid'];
            $CallFrom=@$_REQUEST['From'];
            $CallTo=@$_REQUEST['To'];
            $CallStatus=@$_REQUEST['CallStatus'];
            $ApiVersion=@$_REQUEST['ApiVersion'];
            $Direction=@$_REQUEST['Direction'];

            if (isset($_REQUEST['FromCity'])){
                $FromCity=$_REQUEST['FromCity'];
                $FromState=$_REQUEST['FromState'];
                $FromZip=$_REQUEST['FromZip'];
                $FromCountry=$_REQUEST['FromCountry'];
            } else {
                $FromCity="";
                $FromState="";
                $FromZip="";
                $FromCountry="";
            }
            if (isset($_REQUEST['FromCity'])){
                $ToCity=$_REQUEST['ToCity'];
                $ToState=$_REQUEST['ToState'];
                $ToZip=$_REQUEST['ToZip'];
                $ToCountry=$_REQUEST['ToCountry'];
            }else{
                $ToCity="";
                $ToState="";
                $ToZip="";
                $FromCountry="";
                $ToCountry="";
            }
            if(@$_REQUEST['DateCreated']!=""){
                $DateCreated = $_REQUEST['DateCreated'];
            }else{
                $DateCreated = date('Y-m-d H:i:s',time());
            }
            $DialCallSid=@$_REQUEST['DialCallSid'];
            $DialCallDuration=@$_REQUEST['DialCallDuration'];
            $DialCallStatus=@$_REQUEST['DialCallStatus'];
            $RecordingUrl=@$_REQUEST['RecordingUrl'];

            if ($this->getVar("mask_recordings") == "true") {
                $RecordingUrl = Util::maskRecordingURL($RecordingUrl);
            }

            $userid = @$_REQUEST['userid'];
            $app    = @$_REQUEST['app'];
            $phone_code = 0;

            if(isset($_REQUEST['PhoneCode']))
                $phone_code = $_REQUEST['PhoneCode'];

            //error_log("PhoneCode: ".$phone_code);
            $stmt = $this->db->prepare('INSERT INTO outbound_calls (DateCreated,CallSid,AccountSid,CallFrom,CallTo,CallStatus,ApiVersion,Direction,FromCity,FromState,FromZip,FromCountry,ToCity,ToState,ToZip,ToCountry,DialCallSid,DialCallStatus,RecordingUrl,CallDuration,UserSource,MadeUsing,PhoneCode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $vars=array( $DateCreated,$CallSid,$AccountSid,$CallFrom,$CallTo,$CallStatus,$ApiVersion,$Direction,$FromCity,$FromState,$FromZip,$FromCountry,$ToCity,$ToState,$ToZip,$ToCountry,$DialCallSid,$DialCallStatus,$RecordingUrl,$DialCallDuration,$userid,$app,$phone_code);
            $stmt->execute($vars);

            $company_id = $this->getCompanyofOutgoingCall($_REQUEST['CallSid']);
            $stmt = $this->db->prepare("SELECT * FROM zapier_subscriptions WHERE `event`='incoming_call_browserphone' AND company_id = ?");
            $stmt->execute(array($company_id));
            $subs = $stmt->fetchAll(PDO::FETCH_OBJ);

            if(is_array($subs) || is_object($subs))
                foreach($subs as $sub){
                    $check1 = false;
                    $check2 = false;
                    if(!$this->isUserAdmin($_REQUEST['userid'])){
                        if($sub->user_id == $_REQUEST['userid'])
                            $check1 = true;

                        $outgoing_numbers = $this->getUserOutgoingAccessNumbers($sub->user_id);
                        if(count($outgoing_numbers)==0)
                            $check2 = true;
                        else{
                            foreach($outgoing_numbers as $num)
                                if($num->number == $_REQUEST['From'])
                                    $check2 = true;
                        }
                    }else{
                        $check1 = true;
                        $check2 = true;
                    }

                    if($check1 && $check2){
                        global $TIMEZONE;
                        $_obj = new stdClass();
                        $_obj->CallSid = $_REQUEST['CallSid'];
                        $_date = new DateTime($DateCreated,new DateTimeZone("UTC"));
                        $_date->setTimezone(new DateTimeZone($TIMEZONE));
                        $_obj->DateCreated = $_date->format("D n\/d Y g\:iA T");
                        $_obj->Status = ucfirst($DialCallStatus);
                        $_obj->Duration = $DialCallDuration != null ? $DialCallDuration : "0";
                        $_obj->TrackingNumber = $CallFrom;
                        $_obj->To = $CallTo;
                        $_obj->Recording = $RecordingUrl;
                        $pcs = $this->getPhoneCodes($company_id,2);
                        $_obj->PhoneCode = "None";
                        if(is_array($pcs) || is_object($pcs))
                            foreach($pcs as $pc){
                                if($pc->idx == $phone_code)
                                    $_obj->PhoneCode = $pc->name;
                            }

                        $data_string = json_encode($_obj);
                        $ch = curl_init($sub->subscription_url);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                'Content-Type: application/json',
                                'Content-Length: ' . strlen($data_string))
                        );

                        $result = curl_exec($ch);
                        $responseInfo = curl_getinfo($ch);
                        if($responseInfo['http_code']!=200){
                            $stmt = $this->customExecute("DELETE FROM zapier_subscriptions WHERE id = ?");
                            $stmt->execute(array($sub->id));
                        }
                    }
                }
        }catch(PDOException $ex){
            return $ex;
        }
        $arr = $stmt->errorInfo();
        return $arr;
    }

    /**
     * @return bool|PDOException
     */
    function save_dialed_call() {
        try{
            $CallSid = @$_REQUEST['CallSid'];
            $DialCallSid= @$_REQUEST['DialCallSid'];
            $DialCallTo = @$_REQUEST['DialCallTo'];
            $DialCallDuration= @$_REQUEST['DialCallDuration'];
            $DialCallStatus= @$_REQUEST['DialCallStatus'];
            $RecordingUrl= @$_REQUEST['RecordingUrl'];

            $pc = $this->getVar("browserphone_".$_REQUEST['CallSid']);
            if($pc!=false){
                $_REQUEST['PhoneCode'] = $pc;
                $this->deleteVar("browserphone_".$_REQUEST['CallSid']);
            }

            if(isset($_REQUEST['PhoneCode']))
                $phone_code = $_REQUEST['PhoneCode'];
            else
                $phone_code = 0;

            $VoiceMail = @$_REQUEST['voicemail'];
            //if($VoiceMail==1) $DialCallStatus = "voicemail";

            $CallerID = "";

            /*$opencnam_AccountSid = $this->getVar("opencnam_AccountSid");
            $opencnam_AuthKey = $this->getVar("opencnam_AuthKey");
            if ($opencnam_AccountSid != false || $opencnam_AccountSid != ""){
                $retn = json_decode($this->curlGetData("https://$opencnam_AccountSid:$opencnam_AuthKey@api.opencnam.com/v2/phone/".@$_REQUEST['From']."?format=json&ref=act"));
                if(isset($retn->name) && $retn->name != ""){
                    $CallerID = $retn->name;
                }
            }*/
            
            if (empty($CallerID)) $CallerID = $_REQUEST['From'];

            $stmt = $this->db->prepare('UPDATE calls set DialCallSid=?, DialCallTo=?, DialCallStatus=?, CallerID=?, PhoneCode = ? WHERE CallSid=?');
            $stmt->execute(array($DialCallSid, $DialCallTo, $DialCallStatus, $CallerID, $phone_code, $CallSid));
            
            $stmt = $this->db->prepare('SELECT RecordingUrl FROM calls WHERE CallSid=?');
            $stmt->execute(array($CallSid));
            $call = $stmt->fetch();
            
            if (empty($call['RecordingUrl'])) {
                $stmt = $this->db->prepare('UPDATE calls set DialCallDuration=?, RecordingUrl=? WHERE CallSid=?');
                $stmt->execute(array($DialCallDuration, $RecordingUrl, $CallSid));
            }
            else {
                $stmt = $this->db->prepare('INSERT INTO call_recordings(CallSid, DialCallDuration, RecordingUrl) VALUES(?, ?, ?)');
                $stmt->execute(array($CallSid, $DialCallDuration, $RecordingUrl));
            }
        }catch(PDOException $ex){
            return $ex;
        }
        if(isset($_REQUEST['callsync']) && $_REQUEST['callsync']==true){

        }else{
            if ($this->getVar("mask_recordings") == "true") {
                $RecordingUrl = Util::maskRecordingURL($RecordingUrl);
            }
            require_once("class.emailtemplates.php");

            // Run Email Notification Worker
            $company = $this->getCompanyofCall($CallSid);

            $users = $this->customQuery("SELECT * FROM users WHERE email IS NOT NULL");

            $stmt = $this->db->prepare('SELECT * FROM calls WHERE CallSid=?');
            $stmt->execute(array($CallSid));
            $call = $stmt->fetch();

            if (empty($call)) return false;

            $CallTo = $call['CallTo'];
            $CallFrom = $call['CallFrom'];

            $company_name = $this->getCompanyName($company);

            $status_str = array(
                'failed' => array(
                    "A call failed!",
                    "You have received a call from ".$CallFrom." to ".$company_name." ".$CallTo." that did not go through."
                )
            );

            foreach($users as $user)
            {
                $notification_type = $user['notification_type'];
                $email_contents = null;

                switch($notification_type)
                {
                    case 0:// NONE
                    {
                        break;
                    }
                    case 1:// ALL
                    {
                        if($DialCallStatus=="failed")
                        {
                            $email_contents = $status_str[$DialCallStatus];
                        }elseif($DialCallStatus=="busy" || $DialCallStatus=="no-answer"){
                            $email_contents = 6;
                        }elseif($DialCallStatus=="completed"){
                            $email_contents = 5;
                        }
                        break;
                    }
                    case 2:// Missed
                    {
                        if($DialCallStatus=="failed")
                        {
                            $email_contents = $status_str[$DialCallStatus];
                        }elseif($DialCallStatus=="busy" || $DialCallStatus=="no-answer"){
                            $email_contents = 6;
                        }
                        break;
                    }
                    case 3:// Answered
                    {
                        if($DialCallStatus=="completed")
                            $email_contents = 7;
                        break;
                    }
                    case 4:// Over 30 sec
                    {
                        if($DialCallStatus=="completed" && $DialCallDuration >= 30)
                            $email_contents = 8;
                        break;
                    }
                    case 5:// Over 45 sec
                    {
                        if($DialCallStatus=="completed" && $DialCallDuration >= 45)
                            $email_contents = 9;
                        break;
                    }
                    case 6:// Over 60 sec
                    {
                        if($DialCallStatus=="completed" && $DialCallDuration >= 60)
                            $email_contents = 10;
                        break;
                    }
                    case 7:// Over 90 sec
                    {
                        if($DialCallStatus=="completed" && $DialCallDuration >= 90)
                            $email_contents = 11;
                        break;
                    }
                    case 8:// Over 2 min
                    {
                        if($DialCallStatus=="completed" && $DialCallDuration >= 120)
                            $email_contents = 12;
                        break;
                    }
                }

                if($email_contents!=null)
                {
                    if(is_numeric($email_contents)){
                        $template = new EmailTemplate();
                        $email_template = $template->getEmailTemplate($email_contents);
                        $msg = str_replace("[from]",$CallFrom,$email_template->content);
                        $msg = str_replace("[to]",$CallTo,$msg);
                        $msg = str_replace("[company]",$company_name,$msg);
                        if($email_contents != 6){
                            $msg = str_replace("[duration]",$DialCallDuration,$msg);
                            $msg = str_replace("[recordingurl]",$RecordingUrl,$msg);
                        }
                        $email_contents = array($email_template->subject,$msg);
                        //error_log("T2 ".print_r($email_contents,true));
                    }

                    if($user['access_lvl']>0)
                    {
                        $email_params = array(
                            "subject" => $email_contents[0],
                            "msg" => $email_contents[1],
                            "emails" => array($user['email'])
                        );
                        if (isset($user['extra_notification_emails'])) {
                            $extra_emails = explode(",", $user['extra_notification_emails']);
                            foreach ($extra_emails as $email_address) {
                                if (!empty($email_address)) $email_params['emails'][] = $email_address;
                            }
                        }
                        Util::sendEmail($email_params);
                    }else{
                        if($this->isUserInCompany($company,$user['idx']))
                        {
                            $send = true;
                            $disabled_numbers = $this->getUserAccessNumbers($user['idx']);
                            foreach($disabled_numbers as $disabled_number)
                            {
                                if($disabled_number->number==$CallTo)
                                    $send = false;
                            }

                            if($send)
                            {
                                $send = false;
                                $filter_numbers = $this->getUserOutgoingAccessNumbers($user['idx']);

                                if(count($filter_numbers)>0){
                                    if( is_array($filter_numbers) || is_object($filter_numbers) )
                                    {
                                        foreach($filter_numbers as $filter_number)
                                        {
                                            if($filter_number->number == $DialCallTo)
                                                $send = true;
                                        }
                                    }else{
                                        $send = true;
                                    }
                                }else{
                                    $send = true;
                                }
                                if($send)
                                {
                                    $email_params = array(
                                        "subject" => $email_contents[0],
                                        "msg" => $email_contents[1],
                                        "emails" => array($user['email'])
                                    );
                                    if (isset($user['extra_notification_emails'])) {
                                        $extra_emails = explode(",", $user['extra_notification_emails']);
                                        foreach ($extra_emails as $email_address) {
                                            if (!empty($email_address)) $email_params['emails'][] = $email_address;
                                        }
                                    }
                                    Util::sendEmail($email_params);
                                }
                            }
                        }
                    }
                }
            }
        }

        $stmt = $this->db->prepare("SELECT * FROM calls WHERE CallSid = ?");
        $stmt->execute(array($_REQUEST['CallSid']));
        $data = $stmt->fetch(PDO::FETCH_OBJ);
        return $data;
    }

    function getCallRecordings($CallSid) {
        $stmt = $this->db->prepare("SELECT * FROM call_recordings WHERE CallSid = ?");
        $stmt->execute(array($CallSid));
        $data = $stmt->fetchAll();
        return $data;
    }

    /**
     * @return array
     */
    function get_calls(){
        $result = $this->db->query('SELECT * FROM calls ORDER BY DateCreated DESC');
        
        $calls=array();

        foreach ($result as $row)
        {
            $call['CallSid'] = $row['CallSid'];
            $call['CallFrom'] = $row['CallFrom'];
            $call['CallTo'] = $row['CallTo'];
            $call['FromCity'] = $row['FromCity'];
            $call['FromState'] = $row['FromState'];
            $call['FromZip'] = $row['FromZip'];
            $call['DialCallTo'] = $row['DialCallTo'];
            $call['DialCallSid'] = $row['DialCallSid'];
            $call['DialCallDuration'] = $row['DialCallDuration'];
            $call['DialCallStatus'] = $row['DialCallStatus'];
            $call['RecordingUrl'] = $row['RecordingUrl'];
            $call['DateCreated'] = $row['DateCreated'];
            $calls[] = $call;
        }

        return $calls;
        
    }

    /**
     * @param $cid
     * @return bool|mixed
     */
    function getCallDetails($cid)
    {
        $cid = $this->db->escape($cid);
        
        $result = $this->db->query("SELECT * FROM calls WHERE CallSid='$cid'");
        $data = $result->fetch();
        if($data)
        {
            return $data;
        }else
            return false;
    }

    /**
     * @param $cid
     * @return bool|mixed
     */
    function getOutgoingCallDetails($cid)
    {
        $cid = $this->db->escape($cid);
        
        $result = $this->db->query("SELECT * FROM outbound_calls WHERE CallSid='$cid'");
        if($result)
            $data = $result->fetch();
        else
            return false;

        if($data)
        {
            return $data;
        }else
            return false;
    }

    /**
     * Gets all the calls from a Company.
     *
     * @param int $company_id Company's ID
     * @param object $pagination Pagination Object
     * @return array Returns calls in array.
     *
     */ 
    function cget_calls($company_id, $pagination){
        try{
            require_once('Pagination.php');
            $company_id = $this->db->escape($company_id);
            
            $resultValidNum = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id'");
                        
            $numbers = "";
            $calls=array();

            foreach ($resultValidNum as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==0)
                    $numbers .= "'+1".$row['number']."', ";
                else
                    $numbers .= "'+".$row['number']."', ";
            }

            $ringto_numbers = "";
            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                    if(count($filter_ringto_numbers)>0)
                    {

                        foreach($filter_ringto_numbers as $ringto_num)
                        {
                            $ringto_numbers .= "'".$ringto_num->number."', ";
                        }

                        $ringto_numbers = substr($ringto_numbers,0,-2);
                        $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                    }
                }
            }

            $numbers = substr($numbers, 0, -2);

            $date_access_range = "";

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1)
                            $when = ">";
                        elseif($access_range[1]==2)
                            $when = "<";
                        else
                            $when = ">";
                        $date_access_range = " AND DateCreated ".$when." '".$access_range[0]."'";
                    }
                }
            }
            
            $count = $this->db->query('SELECT COUNT(*) as total FROM calls WHERE CallTo IN ('.$numbers.')'.$ringto_numbers.$date_access_range.' ORDER BY DateCreated DESC');
            if($count) {
                $count = $count->fetch();
                $count = $count['total'];
            }
            else
                $count = 0;

            $result = $this->db->query('SELECT * FROM calls WHERE CallTo IN ('.$numbers.')'.$ringto_numbers.$date_access_range.' ORDER BY DateCreated DESC '.$pagination->getLimitSql());
            if($result)
            {
                foreach ($result as $row)
                {
                    $call['CallSid']          = $row['CallSid'];
                    $call['CallFrom']         = $row['CallFrom'];
                    $call['CallTo']           = $row['CallTo'];
                    $call['FromCity']         = $row['FromCity'];
                    $call['FromState']        = $row['FromState'];
                    $call['FromZip']          = $row['FromZip'];
                    $call['DialCallDuration'] = $row['DialCallDuration'];
                    $call['DialCallStatus']   = $row['DialCallStatus'];
                    $call['RecordingUrl']     = $row['RecordingUrl'];
                    $call['DateCreated']      = $row['DateCreated'];
                    $call['PhoneCode']        = $row['PhoneCode'];
                    $calls[] = $row;
                }
            }
            else
                return false;

            return array($calls,$count);
        }catch(PDOException $ex)
        {
            //echo $ex->getMessage();
            return $ex->getMessage();
        }
    }

    function cget_all_calls($company_id, $outgoing = false){
        try{
            $company_id = $this->db->escape($company_id);

            $resultValidNum = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id'");

            $numbers = "";
            $calls=array();

            foreach ($resultValidNum as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==0)
                    $numbers .= "'+1".$row['number']."', ";
                else
                    $numbers .= "'+".$row['number']."', ";
            }

            $ringto_numbers = "";
            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                    if(count($filter_ringto_numbers)>0)
                    {

                        foreach($filter_ringto_numbers as $ringto_num)
                        {
                            $ringto_numbers .= "'".$ringto_num->number."', ";
                        }

                        $ringto_numbers = substr($ringto_numbers,0,-2);
                        $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                    }
                }
            }

            $numbers = substr($numbers, 0, -2);

            $access_range_date = "";

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1)
                            $when = ">";
                        elseif($access_range[1]==2)
                            $when = "<";
                        else
                            $when = ">";
                        $access_range_date = " AND DateCreated ".$when." '".$access_range[0]."'";
                    }
                }
            }

            $extra_ad = "";
            if($outgoing==false){
                $table = "calls";
                $target_field = "CallTo";
            }else{
                $table = "outbound_calls";
                $target_field = "CallFrom";
                if (empty($_GET['agent']))
                    $extra_ad = " AND MadeUsing != 'autodialer' ";
            }

            $count = $this->db->query('SELECT COUNT(*) FROM '.$table.' WHERE '.$target_field.' IN ('.$numbers.')'.$extra_ad.$ringto_numbers.$access_range_date.' ORDER BY DateCreated DESC');
            if($count) {
                $count = $count->fetch();
                $count = $count['total'];
            }
            else
                $count = 0;

            $result = $this->db->query('SELECT * FROM '.$table.' WHERE '.$target_field.' IN ('.$numbers.')'.$extra_ad.$ringto_numbers.$access_range_date.' ORDER BY DateCreated DESC');
            if($result)
            {
                $twilio_numbers = Util::get_all_twilio_numbers();
                foreach ($result as $row)
                {
                    if (array_key_exists(format_phone($row['CallTo']), $twilio_numbers))
                    {
                        $campaign=(string)$twilio_numbers[format_phone($row['CallTo'])];
                    }
                    else
                        $campaign="";

                    $call['CallSid']          = $row['CallSid'];
                    $call['CallFrom']         = $row['CallFrom'];
                    $call['CallTo']           = $row['CallTo'];
                    $call['Campaign']         = $campaign;
                    $call['FromCity']         = $row['FromCity'];
                    $call['FromState']        = $row['FromState'];
                    $call['FromZip']          = $row['FromZip'];
                    $call['FromCountry']      = $row['FromCountry'];
                    if($outgoing==false){
                        $call['DialCallDuration'] = $row['DialCallDuration'];
                    }else{
                        $call['DialCallDuration'] = $row['CallDuration'];
                    }
                    $call['DialCallStatus']   = $row['DialCallStatus'];
                    $call['RecordingUrl']     = $row['RecordingUrl'];
                    $call['DateCreated']      = $row['DateCreated'];
                    $call['PhoneCode']        = $row['PhoneCode'];
                    $call['DialCallTo']       = @$row['DialCallTo'];
                    $call['CallerID']         = $row['CallerID'];
                    $calls[] = $call;
                }
            }
            else
                return false;

            return array($calls,$count);
        }catch(PDOException $ex)
        {
            //echo $ex->getMessage();
            return $ex->getMessage();
        }
    }

    function getAllCallsFromNumber($company_id,$fromnumber){
        try{
            $company_id = $this->db->escape($company_id);

            $resultValidNum = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id'");

            $numbers = "";
            $calls=array();

            foreach ($resultValidNum as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==0)
                    $numbers .= "'+1".$row['number']."', ";
                else
                    $numbers .= "'+".$row['number']."', ";
            }

            $ringto_numbers = "";
            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                    if(count($filter_ringto_numbers)>0)
                    {

                        foreach($filter_ringto_numbers as $ringto_num)
                        {
                            $ringto_numbers .= "'".$ringto_num->number."', ";
                        }

                        $ringto_numbers = substr($ringto_numbers,0,-2);
                        $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                    }
                }
            }

            $numbers = substr($numbers, 0, -2);

            $access_range_date = "";

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1)
                            $when = ">";
                        elseif($access_range[1]==2)
                            $when = "<";
                        else
                            $when = ">";
                        $access_range_date = " AND DateCreated ".$when." '".$access_range[0]."'";
                    }
                }
            }

            $count = $this->db->query('SELECT COUNT(*) as total FROM calls WHERE CallTo IN ('.$numbers.')'.$ringto_numbers.$access_range_date.' AND CallFrom = '.$fromnumber.' ORDER BY DateCreated DESC');
            if($count) {
                $count = $count->fetch();
                $count = $count['total'];
            }
            else
                $count = 0;

            $result = $this->db->query('SELECT * FROM calls WHERE CallTo IN ('.$numbers.')'.$ringto_numbers.$access_range_date.' AND CallFrom = '.$fromnumber.' ORDER BY DateCreated DESC');
            if($result)
            {
                $twilio_numbers = Util::get_all_twilio_numbers();
                foreach ($result as $row)
                {
                    if (array_key_exists(format_phone($row['CallTo']), $twilio_numbers))
                    {
                        $campaign=(string)$twilio_numbers[format_phone($row['CallTo'])];
                    }
                    else
                        $campaign="";

                    $call['CallSid']          = $row['CallSid'];
                    $call['CallFrom']         = $row['CallFrom'];
                    $call['CallTo']           = $row['CallTo'];
                    $call['Campaign']         = $campaign;
                    $call['FromCity']         = $row['FromCity'];
                    $call['FromState']        = $row['FromState'];
                    $call['FromZip']          = $row['FromZip'];
                    $call['DialCallDuration'] = $row['DialCallDuration'];
                    $call['DialCallStatus']   = $row['DialCallStatus'];
                    $call['RecordingUrl']     = $row['RecordingUrl'];
                    $call['DateCreated']      = $row['DateCreated'];
                    $call['PhoneCode']        = $row['PhoneCode'];
                    $calls[] = $call;
                }
            }
            else
                return false;

            return array($calls,$count);
        }catch(PDOException $ex)
        {
            //echo $ex->getMessage();
            return $ex->getMessage();
        }
    }

    function getAllCallsToNumber($company_id,$tonumber){
        try{
            $company_id = $this->db->escape($company_id);

            $resultValidNum = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id'");

            $numbers = "";
            $calls=array();

            foreach ($resultValidNum as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==0)
                    $numbers .= "'+1".$row['number']."', ";
                else
                    $numbers .= "'+".$row['number']."', ";
            }

            $ringto_numbers = "";
            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                    if(count($filter_ringto_numbers)>0)
                    {

                        foreach($filter_ringto_numbers as $ringto_num)
                        {
                            $ringto_numbers .= "'".$ringto_num->number."', ";
                        }

                        $ringto_numbers = substr($ringto_numbers,0,-2);
                        $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                    }
                }
            }

            $numbers = substr($numbers, 0, -2);

            $access_range_date = "";

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1)
                            $when = ">";
                        elseif($access_range[1]==2)
                            $when = "<";
                        else
                            $when = ">";
                        $access_range_date = " AND DateCreated ".$when." '".$access_range[0]."'";
                    }
                }
            }

            $count = $this->db->query('SELECT COUNT(*) as total FROM outbound_calls WHERE CallFrom IN ('.$numbers.')'.$ringto_numbers.$access_range_date.' AND CallTo = '.$tonumber.' ORDER BY DateCreated DESC');
            if($count) {
                $count = $count->fetch();
                $count = $count['total'];
            }
            else
                $count = 0;

            $result = $this->db->query('SELECT * FROM outbound_calls WHERE CallFrom IN ('.$numbers.')'.$ringto_numbers.$access_range_date.' AND CallTo = '.$tonumber.' ORDER BY DateCreated DESC');
            if($result)
            {
                $twilio_numbers = Util::get_all_twilio_numbers();
                foreach ($result as $row)
                {
                    if (array_key_exists(format_phone($row['CallFrom']), $twilio_numbers))
                    {
                        $campaign=(string)$twilio_numbers[format_phone($row['CallFrom'])];
                    }
                    else
                        $campaign="";

                    $call['CallSid']          = $row['CallSid'];
                    $call['CallFrom']         = $row['CallFrom'];
                    $call['CallTo']           = $row['CallTo'];
                    $call['Campaign']         = $campaign;
                    $call['FromCity']         = $row['FromCity'];
                    $call['FromState']        = $row['FromState'];
                    $call['FromZip']          = $row['FromZip'];
                    $call['DialCallDuration'] = $row['CallDuration'];
                    $call['DialCallStatus']   = $row['DialCallStatus'];
                    $call['RecordingUrl']     = $row['RecordingUrl'];
                    $call['DateCreated']      = $row['DateCreated'];
                    $call['PhoneCode']        = $row['PhoneCode'];
                    $calls[] = $call;
                }
            }
            else
                return false;

            return array($calls,$count);
        }catch(PDOException $ex)
        {
            //echo $ex->getMessage();
            return $ex->getMessage();
        }
    }

    function cget_all_outgoing_calls($company_id){
        try{
            $company_id = $this->db->escape($company_id);

            $resultValidNum = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id'");

            $numbers = "";
            $calls=array();

            foreach ($resultValidNum as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==0)
                    $numbers .= "'+1".$row['number']."', ";
                else
                    $numbers .= "'+".$row['number']."', ";
            }

            $numbers = substr($numbers, 0, -2);

            $access_range_date = "";

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1)
                            $when = ">";
                        elseif($access_range[1]==2)
                            $when = "<";
                        else
                            $when = ">";
                        $access_range_date = " AND DateCreated ".$when." '".$access_range[0]."'";
                    }
                }
            }

            $extra_ad = "";
            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";

            $count = $this->db->query('SELECT COUNT(*) FROM outbound_calls WHERE CallFrom IN ('.$numbers.')'.$extra_ad.$access_range_date.' ORDER BY DateCreated DESC');
            if($count) {
                $count = $count->fetch();
                $count = $count['total'];
            }
            else
                $count = 0;

            $result = $this->db->query('SELECT * FROM outbound_calls WHERE CallFrom IN ('.$numbers.')'.$extra_ad.$access_range_date.' ORDER BY DateCreated DESC');
            if($result)
            {
                $twilio_numbers = Util::get_all_twilio_numbers();
                foreach ($result as $row)
                {
                    $new_row = array();
                    foreach($row as $key => $value)
                    {
                        if($key==="CallFrom")
                        {
                            if (array_key_exists(format_phone($value), $twilio_numbers))
                            {
                                $campaign=(string)$twilio_numbers[format_phone($value)];
                            }
                            else
                                $campaign="";

                            $new_row["Campaign"] = $campaign;
                        }

                        if(!is_numeric($key))
                            $new_row[$key] = $value;
                    }
                    $calls[] = $new_row;
                }
            }
            else
                return false;

            return array($calls,$count);
        }catch(PDOException $ex)
        {
            //echo $ex->getMessage();
            return $ex->getMessage();
        }
    }

    function cget_outgoing_calls($company_id, $pagination){
        try{
            require_once('Pagination.php');
            $company_id = $this->db->escape($company_id);

            $resultValidNum = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id'");

            $numbers = "";
            $calls=array();

            foreach ($resultValidNum as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==0)
                    $numbers .= "'+1".$row['number']."', ";
                else
                    $numbers .= "'+".$row['number']."', ";
            }

            $numbers = substr($numbers, 0, -2);

            $access_range_date = "";

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1)
                            $when = ">";
                        elseif($access_range[1]==2)
                            $when = "<";
                        else
                            $when = ">";
                        $access_range_date = " AND DateCreated ".$when." '".$access_range[0]."'";
                    }
                }
            }

            $extra_ad = "";
            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";

            $count = $this->db->query('SELECT COUNT(*) as total FROM outbound_calls WHERE CallFrom IN ('.$numbers.')'.$extra_ad.$access_range_date.' ORDER BY DateCreated DESC');
            if($count) {
                $count = $count->fetch();
                $count = $count['total'];
            }
            else
                $count = 0;

            $result = $this->db->query('SELECT * FROM outbound_calls WHERE CallFrom IN ('.$numbers.')'.$extra_ad.$access_range_date.' ORDER BY DateCreated DESC '.$pagination->getLimitSql());
            if($result)
            {
                foreach ($result as $row)
                    $calls[] = $row;
            }
            else
                return false;

            return array($calls,$count);
        }catch(Exception $ex)
        {
            //echo $ex->getMessage();
            return $ex->getMessage();
        }
    }
    
    function get_voicemail($company_id, $pagination){
        try{
            require_once('Pagination.php');
            $company_id = $this->db->escape($company_id);

            $count = $this->db->query("SELECT COUNT(*) as total FROM messages WHERE message_frn_vmb_extension ='$company_id'");

            if($count) {
                $count = $count->fetch();
                $count = $count['total'];
            }
            else
                $count = 0;

            $result = $this->db->query('SELECT * FROM messages WHERE message_frn_vmb_extension ='. $company_id .' ORDER BY message_date DESC '.$pagination->getLimitSql());
            
            if(!$result) return false;
            $voicemails= $result->fetchAll();
            
            return array($voicemails,$count);
            
        }catch(Exception $ex)
        {
            //echo $ex->getMessage();
            return $ex->getMessage();
        }
    }
    
    /**
     * @param $company_id
     * @return array
     */
    function cget_calls_count($company_id){
        $company_id = $this->db->escape($company_id);

        //error_log("quoted: ".$company_id);

        $resultValidNum = $this->db->query("SELECT number FROM company_num WHERE company_id='$company_id'");

        $access_range_date = "";

        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                if($access_range[0]!="")
                {
                    if($access_range[1]==1)
                        $when = ">";
                    elseif($access_range[1]==2)
                        $when = "<";
                    else
                        $when = ">";
                    $access_range_date = " AND DateCreated ".$when." '".$access_range[0]."'";
                }

                $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                $number_array = array();
                foreach($disabled_numbers as $disabled_number)
                {
                    $number_array[] = $disabled_number->number;
                }
                if(count($number_array)>0)
                {
                    if($access_range!=false)
                        $addon_sql = " AND CallTo NOT IN (";
                    else
                        $addon_sql = " AND CallTo NOT IN (";
                    foreach($number_array as $d_number)
                    {
                        $addon_sql .= "'".$d_number."',";
                    }
                    $addon_sql = substr($addon_sql, 0, -1).")";
                    $access_range_date .= $addon_sql;
                }
                $allowed_outgoing_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                if(count($allowed_outgoing_numbers)>0)
                {
                    if($access_range!=false || strlen($addon_sql)>0)
                        $addon_sql2 = " AND DialCallTo IN (";
                    else{
                        $addon_sql2 = " AND DialCallTo IN (";
                    }
                    if( is_array($allowed_outgoing_numbers) || is_object($allowed_outgoing_numbers) )
                    foreach($allowed_outgoing_numbers as $outgoing_num)
                    {
                        $addon_sql2 .= "'".$outgoing_num->number."',";
                    }
                    $addon_sql2 = substr($addon_sql2, 0 , -1).")";
                    $access_range_date .= $addon_sql2;
                }
            }
        }

        //echo 'SELECT count(*) as cnt, CallTo FROM calls'.$access_range_date.' GROUP BY CallTo ORDER BY cnt DESC';

        $result = $this->db->query('SELECT count(*) as cnt, CallTo FROM calls WHERE DialCallStatus != "voicemail2" '.$access_range_date.' GROUP BY CallTo ORDER BY cnt DESC');

        $validNumbers = array();
        $calls=array();

        foreach ($resultValidNum as $row)
        {
            $validNumbers[] = $row['number'];
        }
        if( is_array($result) || is_object($result) )
            foreach ($result as $row1)
            {
                if(in_array($this->format_phone_db($row1['CallTo']), $validNumbers))
                {
                    $call['cnt'] = $row1['cnt'];
                    $call['CallTo'] = $row1['CallTo'];
                    $calls[] = $call;
                }
            }

        return $calls;

    }

    function cget_outgoing_calls_count($company_id){
        $company_id = $this->db->escape($company_id);

        $resultValidNum = $this->db->query("SELECT number FROM company_num WHERE company_id='$company_id'");

        $access_range_date = "";

        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                if($access_range!=false)
                {
                    if($access_range[1]==1)
                        $when = ">";
                    elseif($access_range[1]==2)
                        $when = "<";
                    else
                        $when = ">";
                    $access_range_date = " WHERE DateCreated ".$when." '".$access_range[0]."'";
                }

                $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                $number_array = array();
                foreach($disabled_numbers as $disabled_number)
                {
                    $number_array[] = $disabled_number->number;
                }
                if(count($number_array)>0)
                {
                    if($access_range!=false)
                        $addon_sql = " AND CallFrom NOT IN (";
                    else
                        $addon_sql = " WHERE CallFrom NOT IN (";
                    foreach($number_array as $d_number)
                    {
                        $addon_sql .= "'".$d_number."',";
                    }
                    $addon_sql = substr($addon_sql, 0, -1).")";
                    $access_range_date .= $addon_sql;
                }
            }
        }

        $extra_ad = "";
        if (empty($_GET['agent']))
            $extra_ad = " AND MadeUsing != 'autodialer' ";

        $result = $this->db->query('SELECT count(*) as cnt, CallFrom FROM outbound_calls'.$extra_ad.$access_range_date.' GROUP BY CallFrom ORDER BY cnt DESC');

        $validNumbers = array();
        $calls=array();

        foreach ($resultValidNum as $row)
        {
            $validNumbers[] = $row['number'];
        }
        if( is_array($result) || is_object($result) )
            foreach ($result as $row1)
            {
                if(in_array($this->format_phone_db($row1['CallFrom']), $validNumbers))
                {
                    $call['cnt'] = $row1['cnt'];
                    $call['CallFrom'] = $row1['CallFrom'];
                    $calls[] = $call;
                }
            }

        return $calls;

    }

    /**
     * @param $call_sid
     * @return array
     */
    function getCallNotes($call_sid){
        $call_sid = $this->db->escape($call_sid);
        
        $query = $this->db->query("SELECT * FROM call_notes WHERE CallSid='$call_sid' ORDER BY DateAdded ASC");
        
        return $query->fetchAll();
    }

    /**
     * @return int
     */
    function getCompanyCount(){
        $result = $this->db->query("SELECT * FROM companies;");
        $count = 0;
        foreach ($result as $row)
        {
            $count++;
        }
        return $count;
    }

    /**
     * @param $number
     * @return mixed
     */
    function getOutgoingNumber($number)
    {
        $number = $this->db->escape($number);

        $query = $this->db->query("SELECT company_id FROM company_num WHERE number='$number'");
        $company_id = $query->fetch(); $company_id = $company_id['company_id'];

        $query = $this->db->query("SELECT assigned_number,international FROM companies WHERE idx='$company_id'");

        if($query === false)
            return "";
        else{
            $number = $query->fetch();
        }

        if($number['international']==1)
            return $number['assigned_number'];
        else
            return "+1".$number['assigned_number'];
    }

    /**
     * @param $number
     * @return mixed
     */
    function getOutgoingNumberDetails($number)
    {
        $number = $this->db->escape($number);

        $query = $this->db->query("SELECT * FROM company_num WHERE number='$number'");

        if($query === false)
            return "";
        else{
            return $query->fetch();
        }
    }
    
    
    function getCompanyOfNumber($number)
    {
        $stmt = $this->db->prepare("SELECT company_id FROM company_num WHERE number=:num OR number=:num2");
        if($stmt->execute(array(":num"=>$number, ":num2"=>"1".$number)))
        {
            if($result = $stmt->fetch(PDO::FETCH_OBJ))
                return $result->company_id;
            else
                return false;
        }else{
            return false;
        }
    }

    function getNumbersOfCompany($company_id, $pool = false)
    {
        $pool_criteria = ($pool) ? "" : " AND pool_id IS NULL";

        $stmt = $this->db->prepare("SELECT number FROM company_num WHERE company_id=:company_id ".$pool_criteria);
        if($stmt->execute(array(":company_id"=>$company_id)))
        {
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $result;
        }else{
            return false;
        }
    }
    function getLiveCalls($numbers)
    {
        $stmt = $this->db->prepare("SELECT CallSid,`Index`, CallFrom, CallTo, DateCreated FROM calls WHERE DateCreated LIKE '%".date("Y-m-d")."%' AND CallStatus = 'ringing' AND (DialCallStatus IS NULL OR DialCallStatus = '') AND CallTo IN ".$numbers);

        if($stmt->execute())
        {
            $calls = array();
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach($result as $call){
                $date = new DateTime("now",new DateTimeZone("UTC"));
                $now = $date->modify("-1 hour");
                $call_date = new DateTime($call->DateCreated, new DateTimeZone("UTC"));
                if($call_date < $now) continue;
                $calls[] = $call;
            }

            return $calls;
        }else{
            return false;
        }
    }

    /**
     * @param $number
     * @param $company_id
     * @return bool
     */
     
    function setfowardNumber($number,$sec ,$intl,$company_id)
    {
        $number = $this->db->escape($number);
        $company_id = $this->db->escape($company_id);
        $sec = $this->db->escape($sec);
        $intl = $this->db->escape($intl);
            
        $result = $this->db->query("UPDATE companies SET international='$intl',forward_sec ='$sec', forward_number='$number' where idx ='$company_id'");
        
    }
    
    
    function setOutgoingNumber($number, $company_id,$intl,$voicemail)
    {
        $number = $this->db->escape($number);
        $company_id = $this->db->escape($company_id);

        $result = $this->db->prepare("UPDATE companies SET assigned_number=?,international=?,voicemail=? WHERE idx=?");
        if($result->execute(array($number,$intl,$voicemail,$company_id)))
        {
            if ( $voicemail)
            {
                $result = $this->db->query("insert into  voicemailbox set vmb_extension='$company_id',vmb_description ='compnay extension',vmb_passcode ='$company_id'");
            }else{  
                $result = $this->db->query("delete  from voicemailbox where vmb_extension='$company_id'");          
            }
            return true;
        }else{
            error_log(var_export($result->errorInfo(),true));
            return false;
        }
    }

function setSIP($sip_endpoint, $sip_msg,$sip_username,$sip_password,$sip_header,$company_id)
    {
        $sip_endpoint = $this->db->escape($sip_endpoint);
        $sip_msg = $this->db->escape($sip_msg);
        $sip_username = $this->db->escape($sip_username);
        $sip_password = $this->db->escape($sip_password);
        $sip_header = $this->db->escape($sip_header);
        $company_id = $this->db->escape($company_id);

        $result = $this->db->prepare("UPDATE companies SET sip_endpoint=?,sip_msg=? ,sip_username=?,sip_password=?,sip_header=?  WHERE idx=?");
        if($result->execute(array($sip_endpoint,$sip_msg,$sip_username,$sip_password,$sip_header,$company_id)))
        {
            return true;
        }else{
            error_log(var_export($result->errorInfo(),true));
            return false;
        }
    }
    
    function getPreviousOutgoingNumbertoCall($callTo,$callFrom,$company_id)
    {
        $check1 = false;

        $callTo = str_replace(" ","+",$this->db->escape(trim($callTo)));
        $callFrom = str_replace(" ","+",$this->db->escape(trim($callFrom)));
        if(substr($callTo,0,1)!=="+")
            $callTo = "+".$callTo;
        if(substr($callFrom,0,1)!=="+")
            $callFrom = "+".$callFrom;

        $query = $this->db->prepare("SELECT DialCallTo FROM calls WHERE CallTo = ? AND CallFrom = ? AND DialCallStatus = 'completed'");
        $query->execute(array($callTo,$callFrom));
        $data = $query->fetch(PDO::FETCH_OBJ);

        if($data){
            $stmt = $this->customQuery("SELECT * FROM cf_round_robin WHERE company_id = $company_id GROUP BY idx ASC");
            $round_robin_numbers = $stmt->fetchAll(PDO::FETCH_OBJ);
            foreach($round_robin_numbers as $number) { if($number->number == $data->DialCallTo) $check1 = true; }

            $stmt = $this->customQuery("SELECT * FROM cf_multiple_numbers WHERE company_id = $company_id GROUP BY idx ASC");
            $multiple_numbers = $stmt->fetchAll(PDO::FETCH_OBJ);
            foreach($multiple_numbers as $number) { if($number->number == $data->DialCallTo) $check1 = true; }

            $numbers = array();
            $stmt = $this->customQuery("SELECT wId, flowtype FROM call_ivr_widget WHERE (`flowtype` = 'MultipleNumbers' OR `flowtype` = 'RoundRobin') AND `companyId` = $company_id");
            $widgets = $stmt->fetchAll(PDO::FETCH_OBJ);
            foreach($widgets as $widget){
                if($widget->flowtype == "MultipleNumbers"){
                    $stmt = $this->customQuery("SELECT number FROM call_ivr_multiple_numbers WHERE wId = ".$widget->wId);
                    $mn = $stmt->fetchAll(PDO::FETCH_OBJ);
                    foreach($mn as $number) {$numbers[] = $number->number;}
                }else{
                    $stmt = $this->customQuery("SELECT number FROM call_ivr_round_robin WHERE wId = ".$widget->wId);
                    $mn = $stmt->fetchAll(PDO::FETCH_OBJ);
                    foreach($mn as $number) {$numbers[] = $number->number;}
                }
            }
            if(in_array($data->DialCallTo,$numbers))
                $check1 = true;
        }

        if((@$data->DialCallTo!="" || @$data->DialCallTo!="+1") && $check1===true)
            return $data->DialCallTo;
        else
            return false;
    }

    function getCompanySettings($company_id)
    {
        $stmt = $this->db->prepare("SELECT whisper,whisper_type,whisper_voice,whisper_language,recording_notification,recording_notification_type,recording_notification_voice,recording_notification_language,recording_disable,record_from_ring,call_flow,international,international_closed,assigned_number,last_roundrobin,sip_endpoint,sip_msg,sip_username,sip_password,sip_header ,voicemail,voicemail_closed,close_number,opt_hours,forward_number,forward_sec,voicemail_type,voicemail_content,voicemail_text_voice,voicemail_text_language,ring_count,send_transcript,transcriptEmail FROM companies WHERE idx=:company_id");
        $stmt->execute(array(":company_id"=>$company_id));
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    function updateCompanySettings($settings)
    {
        $stmt = $this->db->prepare("UPDATE companies SET 
            whisper=:whisper,
            whisper_type=:whisper_type,
            whisper_voice=:whisper_voice,
            whisper_language=:whisper_language,
            recording_notification=:recording_notification,
            recording_notification_type=:recording_notification_type,
            recording_notification_voice=:recording_notification_voice,
            recording_notification_language=:recording_notification_language,
            recording_disable=:rec_dis,
            record_from_ring=:rec_ring

        WHERE idx=:id");
        if($stmt->execute(array(
            ":whisper"=>$settings['whisper'],
            ":whisper_type"=>$settings['whisper_type'],
            ":whisper_voice"=>$settings['whisper_voice'],
            ":whisper_language"=>$settings['whisper_language'],
            ":recording_notification"=>$settings['recording_notification'],
            ":recording_notification_type"=>$settings['recording_notification_type'],
            ":recording_notification_voice"=>$settings['recording_notification_voice'],
            ":recording_notification_language"=>$settings['recording_notification_language'],
            "rec_dis"=>$settings['rec_disable'], 
            "rec_ring"=>$settings['record_from_ring'], 
            ":id"=>$settings['id']))){
            return true;
        }else{
            return false;
        }
    }

    function isCompanyRecordingDisabled($company_id)
    {
        $stmt = $this->db->prepare("SELECT `recording_disable` FROM companies WHERE `idx`=:id");
        $stmt->execute(array(":id"=>$company_id));
        if($stmt->fetch(PDO::FETCH_OBJ)->recording_disable==1)
            return true;
        else
            return false;
    }

    function setCompanyRecordingDisable($company_id,$num)
    {
        $stmt = $this->db->prepare("UPDATE companies SET `recording_disable`=:num WHERE `idx`=:id");
        if($stmt->execute(array(":id"=>$company_id,":num"=>$num)))
            return true;
        else
            return false;
    }

    /**
     * @param $user_id
     * @return int
     */
    function getCompanyCountForUser($user_id){
        $user_id = $this->db->escape($user_id);
        $result = $this->db->query("SELECT * FROM user_company WHERE user_id='$user_id';");
        $count = 0;
        foreach ($result as $row)
        {
            $count++;
        }
        return $count;
    }

    function getCompaniesForUser($user_id)
    {
        $user_id = $this->db->escape($user_id);
        $result = $this->db->query("SELECT * FROM user_company WHERE user_id='$user_id'");

        return $result;
    }

    /**
     * @param $user_id
     * @param $new_pass
     * @return bool
     */
    function changePassword($user_id,$new_pass)
    {
        $user_id = $this->db->escape($user_id);
        $new_pass = md5($new_pass);
        $result = $this->db->prepare("UPDATE users SET password=? WHERE `idx`=?");
        if($result->execute(array($new_pass,$user_id)))
        {
            return true;
        }else{
            return false;
        }
    }

    function getAllCompanies(){
        $result = $this->db->prepare("SELECT * FROM companies GROUP BY `company_name` ASC;");
        $result->execute();
        $result = $result->fetchAll(PDO::FETCH_ASSOC);

        $companies = array();
        foreach($result as $row)
        {
            $row['company_name'] = $this->db->escape($row['company_name']);
            array_push($companies, $row);
        }
        return $companies;
    }

    /**
     * @param $user_id
     * @return array
     */
    function getAllCompaniesForUser($user_id){
        $user_id = $this->db->escape($user_id);
        $resultCompanyIds = $this->db->prepare("SELECT * FROM user_company WHERE user_id=?");
        $resultCompanyIds->execute(array($user_id));
        $resultCompanyIds = $resultCompanyIds->fetchAll(PDO::FETCH_ASSOC);

        $result = $this->db->prepare("SELECT * FROM companies GROUP BY `company_name` ASC;");
        $result->execute();
        $result = $result->fetchAll(PDO::FETCH_ASSOC);

        $companyIds = array();
        $companies = array();

        if($resultCompanyIds===false || $result === false)
            return false;

        foreach($resultCompanyIds as $rowc)
        {
            $companyIds[] = $rowc['company_id'];
        }
        foreach($result as $row)
        {
            if(in_array($row['idx'],$companyIds))
                array_push($companies,$row);
        }
        return $companies;
    }

    /**
     * @param $company_id
     * @return array
     */
    function getCompanyNum($company_id)
    {
        $result = $this->db->query("SELECT number FROM company_num WHERE company_id='$company_id' AND pool_id IS NULL");
        $numbers = array();
        foreach($result as $row)
        {
            $numbers[] = $row['number'];
        }
        return $numbers;
    }

    /**
     * @param $company_id
     * @return array
     */
    function companyHasNum($company_id, $number)
    {
        $number = $this->format_phone_db($number);
        $result = $this->db->prepare("SELECT COUNT(*) as total FROM company_num WHERE company_id = ? AND number = ? AND pool_id IS NULL");
        $result->execute(array($company_id, $number));
        $data = $result->fetch();

        if ($data['total'] > 0)
            return true;
        else
            return false;
    }

    function getAllCompanyNum()
    {
        $result = $this->db->query("SELECT number,international FROM company_num WHERE pool_id IS NULL");
        $numbers = array();
        if($result === false)
            return false;
        else{
            foreach($result as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==1)
                    $intl = true;
                else
                    $intl = false;
                $numbers[] = array($row['number'],$intl);
            }
        }
        return $numbers;
    }

    function getCompanyNumIntlForUser($company_id,$user_id)
    {
        $company_id = $this->db->escape($company_id);
        $result = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id' AND pool_id IS NULL");
        $numbers = array();
        
        if($result === false)
            return false;
        else{
            foreach($result as $row)
            {
                $disabled = false;
                if(@$user_id!="")
                {
                    if(!$this->isUserAdmin($user_id))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($user_id);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==1)
                    $intl = true;
                else
                    $intl = false;
                $numbers[] = array($row['number'],$intl);
            }
        }
        return $numbers;
    }

    /**
     * @param $company_id
     * @return array
     */
    function getCompanyNumIntl($company_id)
    {
        $company_id = $this->db->escape($company_id);
        $result = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id' AND pool_id IS NULL");
        $numbers = array();
        if($result === false)
            return false;
        else{
            foreach($result as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==1)
                    $intl = true;
                else
                    $intl = false;
                $numbers[] = array($row['number'],$intl);
            }
        }
        return $numbers;
    }

    /**
     * @param $company_id
     * @return array
     */
    function getCompanyNumIntlPool($company_id)
    {
        $company_id = $this->db->escape($company_id);
        $result = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id' AND pool_id IS NOT NULL");
        $numbers = array();
        if($result === false)
            return false;
        else{
            foreach($result as $row)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($row['international']==0)
                                    $number_ = "+1".$row['number'];
                                else
                                    $number_ = $row['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($row['international']==1)
                    $intl = true;
                else
                    $intl = false;
                $numbers[] = array($row['number'],$intl);
            }
        }
        return $numbers;
    }

    function getPhoneCodeTemplates()
    {
        $result = $this->db->query("SELECT * FROM phone_code_templates;");
        $pc_templates = array();
        foreach($result as $row)
        {
            $pc_templates[] = $row;
        }
        return $pc_templates;
    }

    function getBlacklists()
    {
        $result = $this->db->query("SELECT * FROM blacklists;");
        $blacklists = array();
        foreach($result as $row)
        {
            $blacklists[] = array($row['idx'], $row['name'], $row['numbers']);
        }

        return $blacklists;
    }

    function getBlacklist($id)
    {
        $id = $this->db->escape($id);
        $result = $this->db->query("SELECT * FROM blacklists WHERE `idx`=".$id.";");
        $results = array();
        if($result === false)
            return "";
        else{
            foreach($result as $row){
                $results[] = $row['numbers'];
            }
        }

        if(@$results[0]===false)
        {
            return "";
        }else{
            return @$results[0];
        }
    }

    function editCompanyBlacklist($company_id,$blacklist_id)
    {
        $result = $this->db->prepare("UPDATE companies SET `blacklist_id`=:blid WHERE `idx`=:id");
        $params = array(":blid"=>$blacklist_id, ":id"=>$company_id);
        if($result->execute($params))
            return true;
        else
            return false;
    }

    function editBlacklist($id,$numbers)
    {
        $result = $this->db->prepare("UPDATE blacklists SET numbers=:num WHERE `idx`=:id");
        $params = array(":id"=>$id,":num"=>$numbers);
        if($result->execute($params))
            return true;
        else
            return false;
    }

    function isNumberBlocked($company_id,$number)
    {
        $number = str_replace("+", "", $number);
        $result = $this->db->query("SELECT * FROM blacklists");
        $blacklists = $result->fetchAll();
        $company_id = $this->db->escape($company_id);
        if ($blacklists) {
            foreach ($blacklists as $blacklist) {
                $numbers = str_replace("+", "", $this->getBlacklist($blacklist['idx']));
                if (!empty($numbers) && !empty($number)) {
                    if (strpos($numbers, $number) !== false) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    function getBlacklistName($id)
    {
        $id = $this->db->escape($id);
        $result = $this->db->query("SELECT `name` FROM blacklists WHERE `idx`=".$id);
        if($result === false)
            return "";
        else{
            $result = $result->fetch();
            return $result['name'];
        }
    }

    function getCallerId($company_id)
    {
        $company_id = $this->db->escape($company_id);
        $result = $this->db->query("SELECT `callerid` FROM `companies` WHERE `idx`=".$company_id);
        if($result === false)
            return false;
        else{
            $result = $result->fetch();
            return $result['callerid'];
        }
    }

    function setCallerId($company_id,$number)
    {
        $company_id = $this->db->escape($company_id);
        $number = $this->db->escape($number);
        $result = $this->db->prepare("UPDATE `companies` SET `callerid`=:callerid WHERE `idx`=:id;");
        if($result->execute(array(":callerid"=>$number,":id"=>$company_id)))
            return true;
        else
            return false;
    }

    /**
     * @return int
     */
    function getUserCount(){
        $result = $this->db->query("SELECT idx FROM users;");
        $count = 0;
        foreach ($result as $row)
        {
            $count++;
        }
        return $count;
    }

    function getUser($user_id){
        $stmt = $this->db->prepare("SELECT * FROM users WHERE idx = ?");
        $stmt->execute(array($user_id));
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    /**
     * @return array
     */
    function getAllUsers(){
        $result = $this->db->query("SELECT * FROM users;");
        $result = $result->fetchAll(PDO::FETCH_ASSOC);
        $users = array();
        foreach($result as $row)
        {
            $resultCompany = $this->db->query("SELECT * FROM user_company WHERE user_id='".$row['idx']."'");
            $resultCompany = $resultCompany->fetchAll(PDO::FETCH_ASSOC);
            $companies = array();
            foreach($resultCompany as $rowc)
            {
                $resultCompanyDetail = $this->db->query("SELECT * FROM companies WHERE idx='".$rowc['company_id']."'");
                $resultCompanyDetail = $resultCompanyDetail->fetchAll(PDO::FETCH_ASSOC);
                foreach($resultCompanyDetail as $rowcd)
                {
                    $companies[] = array($rowcd['idx'],$this->db->escape($rowcd['company_name']));
                }
            }
            $users[] = array("user_data" => $row, "user_companies"=> $companies);
        }
        return $users;
    }

    function getAllUsersLimited(){
        $result = $this->db->query("SELECT DISTINCT full_name, email FROM users WHERE email != '' AND full_name IS NOT NULL");
        $result = $result->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    function getUserName($user_id)
    {
        $user_id = $this->db->escape($user_id);
        $result = $this->db->query("SELECT username FROM users WHERE idx='$user_id'");
        $result = $result->fetch();
        return $result['username'];
    }

    function userLogout($user_id){
        $this->setUserStatus($user_id,"OFFLINE");
    }

    function getUserStatus($user_id){
        $stmt = $this->db->prepare("SELECT status FROM users WHERE idx = ?");
        $stmt->execute(array($user_id));
        $data = $stmt->fetch(PDO::FETCH_OBJ);
        return $data->status;
    }

    function setUserStatus($user_id,$status){
        $stmt = $this->db->prepare("UPDATE users SET `status` = ? WHERE `idx` = ?");
        $stmt->execute(array($status,$user_id));
    }

    function updateUserStatusTime($user_id){
        $stmt = $this->db->prepare("SELECT * FROM users WHERE idx = ?");
        $stmt->execute(array($user_id));
        $user = $stmt->fetch(PDO::FETCH_OBJ);
        $stmt = $this->db->prepare("UPDATE users SET status_update_time = ? WHERE idx = ?");
        $stmt->execute(array(strtotime("now"),$user_id));
    }

    function getUserAccessNumbers($user_id)
    {
        $stmt = $this->db->prepare("SELECT `number` FROM user_incoming_num_access WHERE user_id=?");

        if($stmt->execute(array($user_id)))
        {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }else{
            return false;
        }
    }

    function saveUserAccessNumbers($user_id,$disabled_phones)
    {
        $stmt = $this->db->prepare("DELETE FROM user_incoming_num_access WHERE `user_id`=?");

        if($stmt->execute(array($user_id)))
        {
            if( is_array($disabled_phones) || is_object($disabled_phones) )
                foreach($disabled_phones as $disabled_phone)
                {
                    $stmt2 = $this->db->prepare("INSERT INTO user_incoming_num_access (`user_id`,`number`) VALUES (?,?)");
                    if(!$stmt2->execute(array($user_id,$disabled_phone)))
                        return false;
                }
        }else{
            return false;
        }
    }

    function getUserOutgoingAccessNumbers($user_id)
    {
        $stmt = $this->db->prepare("SELECT `number` FROM user_outgoing_num_access WHERE user_id=?");

        if($stmt->execute(array($user_id)))
        {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }else{
            return false;
        }
    }

    function saveUserOutgoingAccessNumbers($user_id,$phone_numbers)
    {
        $stmt = $this->db->prepare("DELETE FROM user_outgoing_num_access WHERE `user_id`=?");

        if($stmt->execute(array($user_id)))
        {
            if( is_array($phone_numbers) || is_object($phone_numbers) )
                foreach($phone_numbers as $phone)
                {
                    if($phone=="")
                        continue;
                    $stmt2 = $this->db->prepare("INSERT INTO user_outgoing_num_access (`user_id`,`number`) VALUES (?,?)");
                    if(!$stmt2->execute(array($user_id,$phone)))
                        return false;
                }
        }else{
            return false;
        }
    }

    function getUserAccessRange($user_id)
    {
        $user_id = $this->db->escape($user_id);
        $stmt = $this->db->prepare("SELECT access_from,access_type FROM users WHERE idx=?");

        if($stmt->execute(array($user_id)))
        {
            $data = $stmt->fetch(PDO::FETCH_OBJ);
            if($data->access_from!=NULL)
            {
                return array($data->access_from, $data->access_type);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    function isUserOutboundLinksDisabled($userid)
    {
        $stmt = $this->db->prepare("SELECT disable_outbound_link FROM users WHERE idx=?");
        $stmt->execute(array($userid));
        $res = $stmt->fetch(PDO::FETCH_OBJ);

        return $res->disable_outbound_link;
    }

    function setUserOutboundLinkDisable($userid,$disabled)
    {
        $stmt = $this->db->prepare("UPDATE users SET disable_outbound_link=? WHERE idx=?");
        $stmt->execute(array($disabled,$userid));
    }

    function isUserAbleToSetPhoneCodes($userid){
        @session_start();
        if(@$_SESSION['prems']['set_phone_code']==""){
            $stmt = $this->db->prepare("SELECT allow_assign_phone_code FROM users WHERE idx=?");
            $stmt->execute(array($userid));
            $res = $stmt->fetch(PDO::FETCH_OBJ);
            if($res->allow_assign_phone_code==1){
                $_SESSION['prems']['set_phone_code'] = true;
                return true;
            }else{
                $_SESSION['prems']['set_phone_code'] = false;
                return false;
            }
        }else{
            return $_SESSION['prems']['set_phone_code'];
        }
    }

    function setUserPhoneCodeAccess($userid,$disabled){
        $stmt = $this->db->prepare("UPDATE users SET allow_assign_phone_code=? WHERE idx=?");
        $stmt->execute(array($disabled,$userid));
    }

    function setUserAccessRange($user_id,$timestamp,$type)
    {
        $user_id = $this->db->escape($user_id);
        $timestamp = $this->db->escape($timestamp);

        if($timestamp==null)
        {
            $stmt = $this->db->prepare("UPDATE users SET access_from=NULL, access_type=0 WHERE idx=?");

            if($stmt->execute(array($user_id)))
            {
                return true;
            }else{
                return false;
            }
        }else{
            $stmt = $this->db->prepare("UPDATE users SET access_from=?, access_type=? WHERE idx=?");

            if($stmt->execute(array($timestamp,$type,$user_id)))
            {
                return true;
            }else{
                return false;
            }
        }
    }

    function checkAddonAccess($user_id,$addon_id)
    {
        $stmt = $this->db->prepare("SELECT `access_lvl` FROM users WHERE idx=?");

        if($stmt->execute(array($user_id)))
        {
            $data = $stmt->fetch(PDO::FETCH_OBJ);
            if($data->access_lvl>0)
                return true;
            else{
                $stmt = $this->db->prepare("SELECT * FROM `user_addon_access` WHERE `user_id`=? AND `addon_id`=?");
                if($stmt->execute(array($user_id,$addon_id)))
                {
                    if(count($stmt->fetchAll())>0)
                        return true;
                    else
                        return false;
                }
            }
        }
        return false;
    }

    function addAddonAccess($user_id,$addon_id)
    {
        $stmt = $this->db->prepare("SELECT * FROM `user_addon_access` WHERE `user_id`=? AND `addon_id`=?");
        $stmt->execute(array($user_id,$addon_id));
        if(count($stmt->fetchAll())>0)
            return true;
        else
        {
            $stmt = $this->db->prepare("INSERT INTO `user_addon_access`(`user_id`,`addon_id`) VALUES (?,?);");
            $stmt->execute(array($user_id,$addon_id));
            return true;
        }
    }

    function removeAddonAccess($user_id,$addon)
    {
        $stmt = $this->db->prepare("DELETE FROM `user_addon_access` WHERE `user_id`=? AND `addon_id`=?");
        $stmt->execute(array($user_id,$addon));
    }

    /**
     * @param $username
     * @param $password
     * @return bool|int
     */
    function authUser($username, $password)
    {
        $username = $this->db->escape($username);
        //$password = md5($password);

        $result = $this->db->query("SELECT * FROM users WHERE username='$username'");

        $data = $result->fetchAll();

        $count = count(array_keys($data));
        if($count != 0)
        {
            if($password == $data[0]['password'])
            {
                if(session_id() == '') {
                    session_start();
                }
                $_SESSION['user_id'] = $data[0]['idx'];
                $_SESSION['permission'] = $data[0]['access_lvl'];
                $_SESSION['username'] = $data[0]['username'];
                $_SESSION['full_name'] = $data[0]['full_name'];
                $_SESSION['handle'] = (!empty($data[0]['full_name']) ? $data[0]['full_name']." (".$data[0]['username'].")" : $data[0]['username']);
                $_SESSION['sel_co'] = NULL;
                $this->setUserStatus($_SESSION['user_id'], "ONLINE");
                return 0;// Success.
            }else{
                return 1;// Wrong password.
            }

        }else{
            return 2;// No such account.
        }

        return true;
    }

    function authUserAPI($username,$password_md5)
    {
        $result = $this->db->query("SELECT * FROM users WHERE username='$username'");

        $data = $result->fetchAll();
        $count = count(array_keys($data));
        if($count != 0)
        {
            if($password_md5 == $data[0]['password'])
            {
                @session_start();
                $_SESSION['user_id'] = $data[0]['idx'];
                $_SESSION['permission'] = $data[0]['access_lvl'];
                $_SESSION['username'] = $data[0]['username'];
                $_SESSION['sel_co'] = NULL;
                return array(true,$data[0]['idx']);
            }else{
                return array(false,1);
            }
        }else{
            return array(false,2); // No such account
        }
    }

    /**
     * @param $user_id
     * @return bool
     */
    function promoteUserToAdmin($user_id)
    {
        $user_id = $this->db->escape($user_id);
        $result = $this->db->prepare("UPDATE users SET access_lvl='1' WHERE idx=?");
        if($result->execute(array($user_id)))
        {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $user_id
     * @return bool
     */
    function demoteUser($user_id)
    {
        $user_id = $this->db->escape($user_id);
        $result = $this->db->prepare("UPDATE users SET access_lvl='0' WHERE idx=?");
        if($result->execute(array($user_id)))
        {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $username
     * @param $full_name
     * @param $password
     * @return bool
     */
    function createNewUser($username,$full_name,$password,$email)
    {
        $username = $this->db->escape($username);
        $full_name = $this->db->escape($full_name);
        $password = $this->db->escape($password);
        $email = $this->db->escape($email);
        $result = $this->db->prepare("INSERT INTO users (username,password,access_lvl,full_name,email) VALUES (?,?,0,?,?);");
        $params = array($username,md5($password),$full_name,$email);

        $stmt = $this->db->prepare("SELECT COUNT(*) as total FROM users WHERE username = ?");

        $stmt->execute(array($username));
        $count = $stmt->fetch(PDO::FETCH_ASSOC);
        $count = $count['total'];

        if($count!=0)
            return 'exist';
        else{
            if($result->execute($params)){
                $userid = $this->db->lastInsertId();
                return $this->getUser($userid);
            }
            else
                return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    function deleteUser($id)
    {
        $id = $this->db->escape($id);
        $result = $this->db->prepare("DELETE FROM users WHERE idx=?");
        if($result->execute(array($id)))
            return true;
        else
            return false;
    }

    /**
     * @param $user
     * @param $start_date
     * @param $end_date
     * @param $company
     * @param $call_result
     * @return array
     */
    function getCallReportforExport($user,$start_date,$end_date,$company,$call_result,$phone_code,$outgoing_filter, $outgoing = false, $filter_user = "", $campaign, $agent = "")
    {
        try
        {
            if(!$this->isUserInCompany($company, $user) && !$this->isUserAdmin($user))
                return array(false, "AUTH_FAIL");

            //$start_date = $this->db->escape($start_date);
            //$end_date = $this->db->escape($end_date);
            $company = $this->db->escape($company);
            $call_result = $this->db->escape($call_result);
            $number_filter = "";

            //Get Numbers of the Company
            if($company==-1)
            {
                $resultCompany = $this->db->query("SELECT number,international FROM company_num;");
                $numbers = $resultCompany->fetchAll();
                foreach($numbers as $number)
                {
                    $disabled = false;
                    if(@$_SESSION['user_id']!="")
                    {
                        if(!$this->isUserAdmin($_SESSION['user_id']))
                        {
                            $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                            $number_array = array();
                            foreach($disabled_numbers as $disabled_number)
                            {
                                $number_array[] = $disabled_number->number;
                            }
                            if(count($number_array)>0)
                            {
                                foreach($number_array as $d_number)
                                {
                                    if($number['international']==0)
                                        $number_ = "+1".$number['number'];
                                    else
                                        $number_ = $number['number'];

                                    if($number_===$d_number)
                                        $disabled = true;
                                }
                            }
                        }
                    }
                    if($disabled)
                        continue;

                    if($number['international']==0)
                        $number_filter .= "'+1".$number['number']."', ";
                    else
                        $number_filter .= "'+".$number['number']."', ";
                }
                $number_filter = substr($number_filter, 0, -2);
            }else{
                $resultCompany = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company'");
                $numbers = $resultCompany->fetchAll();
                foreach($numbers as $number)
                {
                    $disabled = false;
                    if(@$_SESSION['user_id']!="")
                    {
                        if(!$this->isUserAdmin($_SESSION['user_id']))
                        {
                            $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                            $number_array = array();
                            foreach($disabled_numbers as $disabled_number)
                            {
                                $number_array[] = $disabled_number->number;
                            }
                            if(count($number_array)>0)
                            {
                                foreach($number_array as $d_number)
                                {
                                    if($number['international']==0)
                                        $number_ = "+1".$number['number'];
                                    else
                                        $number_ = $number['number'];

                                    if($number_===$d_number)
                                        $disabled = true;
                                }
                            }
                        }
                    }
                    if($disabled)
                        continue;

                    if($number['international']==0)
                        $number_filter .= "'+1".$number['number']."', ";
                    else
                        $number_filter .= "'+".$number['number']."', ";
                }
                $number_filter = substr($number_filter, 0, -2);
            }

            $filter_user_add = "";
            if($filter_user!="" || $filter_user!=0){
                $filter_user_add = " AND UserSource = ".$filter_user;
            }

            $outgoing_filter_add = "";
            if($outgoing_filter!="" && $outgoing==false)
            {
                $outgoing_filter_add = " AND (DialCallTo LIKE '%".$outgoing_filter."' OR CallTo LIKE '%".$outgoing_filter."') ";
            }

            $ringto_numbers = "";
            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                    if(count($filter_ringto_numbers)>0)
                    {

                        foreach($filter_ringto_numbers as $ringto_num)
                        {
                            $ringto_numbers .= "'".$ringto_num->number."', ";
                        }

                        $ringto_numbers = substr($ringto_numbers,0,-2);
                        $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                    }
                }
            }

            $extra_ad = "";
            if($outgoing==false){
                $table = "calls";
                $target_field = "CallTo";
                $duration_field = "DialCallDuration";
            }else{
                $table = "outbound_calls";
                $target_field = "CallFrom";
                $duration_field = "CallDuration";

                if (empty($_GET['agent']))
                    $extra_ad = " AND MadeUsing != 'autodialer' ";
            }

            $phone_code_add = "";
            if($phone_code!=0)
            {
                $phone_code_add = "AND `PhoneCode`='".$this->db->escape($phone_code)."'";
            }

            //Generate Some filters
            $call_result_filter = "";
            switch($call_result)
            {
                case "answered":
                {
                    $call_result_filter = " AND `DialCallStatus`='completed'";
                    break;
                }
                case "missed":
                {
                    $call_result_filter = " AND `DialCallStatus`='no-answer'";
                    break;
                }

                case 30:
                case 45:
                case 60:
                case 90:
                case 120:
                case 180:
                case 240:
                case 300:
                case 360:
                case 420:
                case 480:
                case 540:
                case 600:
                case 1200:
                case 1800:
                case 2400:
                {
                    $call_result_filter = " AND `$duration_field` > ".$call_result;
                    break;
                }

                case "all":
                {
                    $call_result_filter = "";
                    break;
                }
            }

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1){
                            if(($start_date)<strtotime($access_range[0]))
                            {
                                $start_date = strtotime($access_range[0]);
                            }
                        }else{
                            if(($end_date)>strtotime($access_range[0]))
                            {
                                $end_date = strtotime($access_range[0]);
                            }
                        }
                    }
                }
            }

            $agent_filter_add = "";
            if (!empty($agent) && !$outgoing) {
                $agent_details = $this->customExecute("SELECT * FROM users WHERE username = ? OR full_name = ? LIMIT 1");
                $agent_details->execute(array($agent, $agent));
                $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

                if (!empty($agent_details)) {
                    $name = trim($agent_details['full_name']);
                    if (empty($name)) $name = trim($agent_details['username']);
                    else $name .= " (".$agent_details['username'].")";

                    $agent_filter_add = " AND participants LIKE '[\"".$name."%' ";
                }
            }
            elseif (!empty($agent) && $outgoing) {
                $agent_details = $this->customExecute("SELECT id FROM users WHERE username = ? OR full_name = ? LIMIT 1");
                $agent_details->execute(array($agent, $agent));
                $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

                if (!empty($agent_details)) {
                    $user_add = " AND UserSource = ".$agent_details['idx'];
                }
            }

            if (empty($number_filter)) {
                $calls = array();
            }
            else {
                $result = $this->db->query("SELECT * FROM $table WHERE  
                `$target_field` IN ($number_filter)" . 
                $extra_ad . 
                $ringto_numbers . 
                $outgoing_filter_add." AND 
                `DateCreated` BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND 
                '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add." ".$filter_user_add.$agent_filter_add);
                $calls = $result->fetchAll();
            }

            $twilio_numbers = Util::get_all_twilio_numbers();

            if (!empty($campaign)) {
                $number_filter = "";
            }

            foreach($calls as $key => $call)
            {
                if($call['CallTo']!="")
                {
                    if (array_key_exists(format_phone($call['CallTo']), $twilio_numbers))
                    {
                        $this_campaign=(string)$twilio_numbers[format_phone($call['CallTo'])];
                    }
                    else
                        $this_campaign="";
                }

                if (empty($campaign) || $campaign === "false" || (!empty($campaign) && $campaign == $this_campaign)) {
                    $number_filter .= "'".$call['CallTo']."', ";
                }
            }
            $number_filter = substr($number_filter, 0, -2);

            $number_filter = str_replace("''", "','", $number_filter);

			$result = $this->db->query("SELECT * FROM $table WHERE 
                `$target_field` IN ($number_filter)" . 
                $ringto_numbers . 
                $outgoing_filter_add." AND 
                `DateCreated` BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND 
                '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add." ".$filter_user_add.$agent_filter_add);

            return array(true,$result->fetchAll());
        }catch(exception $ex)
        {
            return array(false,0);
        }
    }

    /**
     * @param $user
     * @param $start_date
     * @param $end_date
     * @param $company
     * @param $call_result
     * @param $pagination
     * @return array|bool
     */
    function getCallReport($user,$start_date,$end_date,$company,$call_result,$phone_code,$pagination,$outgoing_filter,$campaign = "",$agent = "",$order_by = "DateCreated", $order_type = "DESC")
	{
		try{
			require_once('Pagination.php');

            if(!$this->isUserInCompany($company, $user) && !$this->isUserAdmin($user))
                return false;

            //$start_date = $this->db->escape($start_date);
            //$end_date = $this->db->escape($end_date);
            $company = $this->db->escape($company);
            $call_result = $this->db->escape($call_result);
            $number_filter = "";

            $twilio_numbers = Util::get_all_twilio_numbers();

			//Get Numbers of the Company
            if($company==-1)
            {
                $resultCompany = $this->db->query("SELECT number,international FROM company_num;");
                $numbers = $resultCompany->fetchAll();
                foreach($numbers as $number)
                {
                    $disabled = false;
                    if(@$_SESSION['user_id']!="")
                    {
                        if(!$this->isUserAdmin($_SESSION['user_id']))
                        {
                            $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                            $number_array = array();
                            foreach($disabled_numbers as $disabled_number)
                            {
                                $number_array[] = $disabled_number->number;
                            }
                            if(count($number_array)>0)
                            {
                                foreach($number_array as $d_number)
                                {
                                    if($number['international']==0)
                                        $number_ = "+1".$number['number'];
                                    else
                                        $number_ = $number['number'];

                                    if($number_===$d_number)
                                        $disabled = true;
                                }
                            }
                        }
                    }
                    if($disabled)
                        continue;

                    if($number['international']==0)
                        $number_filter .= "'+1".$number['number']."', ";
                    else
                        $number_filter .= "'+".$number['number']."', ";
                }
                $number_filter = substr($number_filter, 0, -2);
            }else{
                $resultCompany = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company'");
                $numbers = $resultCompany->fetchAll();
                foreach($numbers as $number)
                {
                    $disabled = false;
                    if(@$_SESSION['user_id']!="")
                    {
                        if(!$this->isUserAdmin($_SESSION['user_id']))
                        {
                            $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                            $number_array = array();
                            foreach($disabled_numbers as $disabled_number)
                            {
                                $number_array[] = $disabled_number->number;
                            }
                            if(count($number_array)>0)
                            {
                                foreach($number_array as $d_number)
                                {
                                    if($number['international']==0)
                                        $number_ = "+1".$number['number'];
                                    else
                                        $number_ = $number['number'];

                                    if($number_===$d_number)
                                        $disabled = true;
                                }
                            }
                        }
                    }
                    if($disabled)
                        continue;

                    if($number['international']==0)
                        $number_filter .= "'+1".$number['number']."', ";
                    else
                        $number_filter .= "'+".$number['number']."', ";
                }
                $number_filter = substr($number_filter, 0, -2);
            }

            $phone_code_add = "";
            if($phone_code!=0)
            {
                $phone_code_add = "AND `PhoneCode`='".$this->db->escape($phone_code)."'";
            }

            //Generate Some filters
            $call_result_filter = "";
            switch($call_result)
            {
                case "answered":
                {
                    $call_result_filter = " AND `DialCallStatus`='completed'";
                    break;
                }
                case "missed":
                {
                    $call_result_filter = " AND `DialCallStatus`='no-answer'";
                    break;
                }

                case 30:
                case 45:
                case 60:
                case 90:
                case 120:
                case 180:
                case 240:
                case 300:
                case 360:
                case 420:
                case 480:
                case 540:
                case 600:
                case 1200:
                case 1800:
                case 2400:
                {
                    $call_result_filter = " AND `DialCallStatus`!='no-answer' AND `DialCallDuration` > ".$call_result;
                    break;
                }

                case "all":
                {
                    $call_result_filter = "";
                    break;
                }
            }

            $outgoing_filter_add = "";
            if($outgoing_filter!="")
            {
                $outgoing_filter_add = " AND (DialCallTo LIKE '%".$outgoing_filter."' OR CallTo LIKE '%".$outgoing_filter."') ";
            }

            $ringto_numbers = "";

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1){
                            if(($start_date)<strtotime($access_range[0]))
                            {
                                $start_date = strtotime($access_range[0]);
                            }
                        }else{
                            if(($end_date)>strtotime($access_range[0]))
                            {
                                $end_date = strtotime($access_range[0]);
                            }
                        }
                    }

                    $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                    if(count($filter_ringto_numbers)>0)
                    {

                        foreach($filter_ringto_numbers as $ringto_num)
                        {
                            $ringto_numbers .= "'".$ringto_num->number."', ";
                        }

                        $ringto_numbers = substr($ringto_numbers,0,-2);
                        $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                    }
                }
            }

            $agent_filter_add = "";

            if (!empty($agent) && !$outgoing) {
                $agent_details = $this->customExecute("SELECT * FROM users WHERE username = ? OR full_name = ? LIMIT 1");
                $agent_details->execute(array($agent, $agent));
                $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

                if (!empty($agent_details)) {
                    $name = trim($agent_details['full_name']);
                    if (empty($name)) $name = trim($agent_details['username']);
                    else $name .= " (".$agent_details['username'].")";

                    $agent_filter_add = " AND participants LIKE '[\"".$name."%' ";
                }
            }
            elseif (!empty($agent) && $outgoing) {
                $agent_details = $this->customExecute("SELECT id FROM users WHERE username = ? OR full_name = ? LIMIT 1");
                $agent_details->execute(array($agent, $agent));
                $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

                if (!empty($agent_details)) {
                    $user_add = " AND UserSource = ".$agent_details['idx'];
                }
            }

            $total_duration = 0;
            $count = 0;
            if (empty($number_filter)) {
                $calls = array();
            }
            else {
                $result = $this->db->query("SELECT * FROM calls WHERE `CallTo` IN ($number_filter)".$ringto_numbers.$outgoing_filter_add.$agent_filter_add." AND `DateCreated` BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add);

                //$calls = $result->fetchAll();

                if (!empty($campaign)) {
                    $number_filter = "";
                }

                while ($call = $result->fetch())
                {
                    if($call['CallTo']!="")
                    {
                        if (array_key_exists(format_phone($call['CallTo']), $twilio_numbers))
                        {
                            $this_campaign=(string)$twilio_numbers[format_phone($call['CallTo'])];
                        }
                        else
                            $this_campaign="";
                    }

                    if (empty($campaign) || $campaign === "false" || (!empty($campaign) && $campaign == $this_campaign)) {
                        $calls[$key]['campaign'] = $this_campaign;
                        $count++;
                        $number_filter .= "'".$call['CallTo']."', ";
                        $total_duration = $total_duration + (int)$call['DialCallDuration'];
                    }
                    else {
                        unset($calls[$key]);
                    }
                }
                $number_filter = substr($number_filter, 0, -2);
            }

            $number_filter = str_replace("''", "','", $number_filter);

            if (!empty($order_by) && !empty($order_type)) {
                if ($order_by == "CityState")
                    $sort = " ORDER BY CONCAT(FromCity, FromState) ".$order_type;
                else
                    $sort = " ORDER BY ".$order_by." ".$order_type;
            }

			$result = $this->db->query("SELECT * FROM calls WHERE `CallTo` IN ($number_filter)".$ringto_numbers.$outgoing_filter_add.$agent_filter_add." AND `DateCreated` BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add." ".$sort." ".$pagination->getLimitSql());
            //die("SELECT * FROM calls WHERE `CallTo` IN ($number_filter) AND `DateCreated` BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add." ".$pagination->getLimitSql());
            //return "SELECT * FROM calls WHERE `CallTo` IN ($number_filter) AND `DateCreated` BETWEEN '".date("Y-m-d H:i:s",($start_date/1000))."' AND '".date("Y-m-d H:i:s",($end_date/1000))."'".$call_result_filter." ".$pagination->getLimitSql();

            if($result === false)
                $data_arr = null;
            else
                $data_arr = $result->fetchAll();


            return array($data_arr,$count,$total_duration);

        }catch(Exception $ex)
        {
            return array(false, $ex);
        }
    }

    function getOutgoingCallReport($user,$start_date,$end_date,$company,$call_result,$phone_code,$pagination,$filter_user,$campaign = "", $agent = "")
    {
        try{
            require_once('Pagination.php');

            if(!$this->isUserInCompany($company, $user) && !$this->isUserAdmin($user))
                return false;

            //$start_date = $this->db->escape($start_date);
            //$end_date = $this->db->escape($end_date);
            $company = $this->db->escape($company);
            $call_result = $this->db->escape($call_result);
            $number_filter = "";

            //Get Numbers of the Company
            if($company==-1)
            {
                if($this->isUserAdmin($user))
                {
                    $resultCompany = $this->db->query("SELECT number,international FROM company_num;");
                    $numbers = $resultCompany->fetchAll();
                    foreach($numbers as $number)
                    {
                        if($number['international']==0)
                            $number_filter .= "'+1".$number['number']."', ";
                        else
                            $number_filter .= "'+".$number['number']."', ";
                    }
                    $number_filter = substr($number_filter, 0, -2);
                }else{
                    return false;
                }
            }else{
                $resultCompany = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company'");
                $numbers = $resultCompany->fetchAll();
                foreach($numbers as $number)
                {
                    $disabled = false;
                    if(@$_SESSION['user_id']!="")
                    {
                        if(!$this->isUserAdmin($_SESSION['user_id']))
                        {
                            $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                            $number_array = array();
                            foreach($disabled_numbers as $disabled_number)
                            {
                                $number_array[] = $disabled_number->number;
                            }
                            if(count($number_array)>0)
                            {
                                foreach($number_array as $d_number)
                                {
                                    if($number['international']==0)
                                        $number_ = "+1".$number['number'];
                                    else
                                        $number_ = $number['number'];

                                    if($number_===$d_number)
                                        $disabled = true;
                                }
                            }
                        }
                    }
                    if($disabled)
                        continue;

                    if($number['international']==0)
                        $number_filter .= "'+1".$number['number']."', ";
                    else
                        $number_filter .= "'+".$number['number']."', ";
                }
                $number_filter = substr($number_filter, 0, -2);
            }

            $phone_code_add = "";
            if($phone_code!=0)
            {
                $phone_code_add = "AND `PhoneCode`='".$this->db->escape($phone_code)."'";
            }

            //Generate Some filters
            $call_result_filter = "";
            switch($call_result)
            {
                case "answered":
                {
                    $call_result_filter = " AND `DialCallStatus`='completed'";
                    break;
                }
                case "missed":
                {
                    $call_result_filter = " AND `DialCallStatus`='no-answer'";
                    break;
                }

                case 30:
                case 45:
                case 60:
                case 90:
                case 120:
                case 180:
                case 240:
                case 300:
                case 360:
                case 420:
                case 480:
                case 540:
                case 600:
                case 1200:
                case 1800:
                case 2400:
                {
                    $call_result_filter = " AND `DialCallStatus`!='no-answer' AND `CallDuration` > ".$call_result;
                    break;
                }

                case "all":
                {
                    $call_result_filter = "";
                    break;
                }
            }

            if(@$_SESSION['user_id']!="")
            {
                if(!$this->isUserAdmin($_SESSION['user_id']))
                {
                    $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                    if($access_range!=false)
                    {
                        if($access_range[1]==1){
                            if(($start_date)<strtotime($access_range[0]))
                            {
                                $start_date = strtotime($access_range[0]);
                            }
                        }else{
                            if(($end_date)>strtotime($access_range[0]))
                            {
                                $end_date = strtotime($access_range[0]);
                            }
                        }
                    }
                }
            }

            $filter_user_add = "";
            if($filter_user!="" || $filter_user!=0){
                $filter_user_add = " AND UserSource = ".$filter_user;
            }

            $agent_filter_add = "";

            if (!empty($agent)) {
                $agent_details = $this->customExecute("SELECT * FROM users WHERE username = ? OR full_name = ? LIMIT 1");
                $agent_details->execute(array($agent, $agent));
                $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

                if (!empty($agent_details)) {
                    $filter_user_add = " AND UserSource = ".$agent_details['idx'];
                }
            }

            $extra_ad = "";
            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";

            $result = $this->db->query("SELECT * FROM outbound_calls WHERE `CallFrom` IN ($number_filter) AND `DateCreated` BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$extra_ad.$call_result_filter." ".$phone_code_add.$filter_user_add);

            $total_duration = 0;
            $count = 0;
            while ($call = $result->fetch())
            {
                $total_duration = $total_duration + (int)$call['CallDuration'];
                $count++;
            }

            $result = $this->db->query("SELECT * FROM outbound_calls WHERE `CallFrom` IN ($number_filter) AND `DateCreated` BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$extra_ad.$call_result_filter." ".$phone_code_add.$filter_user_add." ORDER BY DATE(DateCreated) DESC ".$pagination->getLimitSql());
            //die("SELECT * FROM calls WHERE `CallTo` IN ($number_filter) AND `DateCreated` BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add." ".$pagination->getLimitSql());
            //return "SELECT * FROM calls WHERE `CallTo` IN ($number_filter) AND `DateCreated` BETWEEN '".date("Y-m-d H:i:s",($start_date/1000))."' AND '".date("Y-m-d H:i:s",($end_date/1000))."'".$call_result_filter." ".$pagination->getLimitSql();

            if($result === false)
                $data_arr = null;
            else
                $data_arr = $result->fetchAll();



            return array($data_arr,$count,$total_duration);

        }catch(Exception $ex)
        {
            return array(false, $ex);
        }
    }

    /**
     * @param $company_id
     * @param $user_id
     * @return bool|mixed
     */
    function isUserInCompany($company_id, $user_id)
    {
        return true;

        $company = $this->db->escape($company_id);
        $user = $this->db->escape($user_id);
        $result = $this->db->query("SELECT * FROM user_company WHERE user_id='$user' AND company_id='$company'");
        $data = $result->fetch();
        if($data)
            return$data;
        else
            return false;
    }

    /**
     * @param $number
     * @return bool
     */
    function isNumberFree($number)
    {
        $number = $this->db->escape($number);
        $result = $this->db->query("SELECT * FROM company_num WHERE number='$number'");
        $count = count($result->fetchAll());
        if($count!=0)
        {
            return false;
        }else{
            return true;
        }
    }

    /**
     * @param $user
     * @return bool
     */
    function isUserAdmin($user)
    {
        $user = $this->db->escape($user);
        $result = $this->db->query("SELECT `access_lvl` FROM users WHERE `idx`='$user'");
        $result = $result->fetch();
        return $result['access_lvl']>=1 ? true : false;
    }

    function setUserEmail($user_id,$emails)
    {
        $stmt = $this->db->prepare("UPDATE users SET email=? WHERE idx=?");

        if(count($emails)>0)
        {
            $stmt->execute(array($emails[0],$user_id));

            if (count($emails) > 1) {
                unset($emails[0]);
                $stmt = $this->db->prepare("UPDATE users SET extra_notification_emails=? WHERE idx=?");
                $stmt->execute(array(implode(",", $emails), $user_id));
            }
            else {
                $stmt = $this->db->prepare("UPDATE users SET extra_notification_emails=? WHERE idx=?");
                $stmt->execute(array("",$user_id));
            }
        }else{
            $stmt->execute(array(null,$user_id));

            $stmt = $this->db->prepare("UPDATE users SET extra_notification_emails=? WHERE idx=?");
            $stmt->execute(array("",$user_id));
        }
    }

    function getUserEmail($user_id)
    {
        $stmt = $this->db->prepare("SELECT email FROM users WHERE idx=?");

        $stmt->execute(array($user_id));

        $res = $stmt->fetch(PDO::FETCH_OBJ);

        if($res->email)
            return $res->email;
        else
            return false;
    }

    function getUserExtraNotificationEmails($user_id)
    {
        $stmt = $this->db->prepare("SELECT extra_notification_emails FROM users WHERE idx=?");

        $stmt->execute(array($user_id));

        $res = $stmt->fetch(PDO::FETCH_OBJ);

        if($res->extra_notification_emails)
            return $res->extra_notification_emails;
        else
            return false;
    }

    function setUserNotificationSetting($user_id,$notification_id)
    {
        $stmt = $this->db->prepare("UPDATE users SET notification_type=? WHERE idx=?");

        $stmt->execute(array($notification_id,$user_id));
    }

    function getUserNotificationSetting($user_id)
    {
        $stmt = $this->db->prepare("SELECT notification_type FROM users WHERE idx=?");

        $stmt->execute(array($user_id));
        $row = $stmt->fetch(PDO::FETCH_OBJ);

        return $row->notification_type;
    }

    /**
     * @param $company_id
     * @param $number
     * @return bool
     */
    function addNumberToCompany($company_id,$number,$intl)
    {
        $company_id = $this->db->escape($company_id);
        $number = $this->db->escape($number);
        $number = str_replace(array('(', ' ', ')', '-'), '', $number);

        $result = $this->db->prepare("INSERT INTO company_num(`company_id`, `number`, `international`) VALUES (?,?,?)");
        if($result->execute(array($company_id,$number,$intl))) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param $company_id
     * @param $number
     * @return bool
     */
    function deleteNumberFromCompany($company_id,$number)
    {
        $company_id = $this->db->escape($company_id);
        $number = $this->db->escape($number);
        $result = $this->db->prepare("DELETE FROM company_num WHERE number=? AND company_id=?");
        if($result->execute(array($number,$company_id)))
            return true;
        else
            return false;
    }

    /**
     * @param $number
     * @return bool
     */
    function deleteNumber($number)
    {
        $number = $this->db->escape($number);
        $result = $this->db->prepare("DELETE FROM company_num WHERE number=?");
        if($result->execute(array($number)))
            return true;
        else
            return false;
    }

    /**
     * @param $pool_id
     * @return bool
     */
    function deletePool($pool_id)
    {
        $number = $this->db->escape($number);
        $result = $this->db->prepare("DELETE FROM pools WHERE id=?");
        if($result->execute(array($pool_id)))
            return true;
        else
            return false;
    }

    /**
     * @param $company_id
     * @param $user_id
     * @return bool
     */
    function addUserToCompany($company_id,$user_id)
    {
        $company_id = $this->db->escape($company_id);
        $user_id = $this->db->escape($user_id);
        $result = $this->db->prepare("INSERT INTO user_company VALUES (?,?)");
        if($result->execute(array($user_id,$company_id)))
            return true;
        else
            return false;       
    }

    /**
     * @param $company_id
     * @param $user_id
     * @return bool
     */
    function deleteUserFromCompany($company_id,$user_id)
    {
        $company_id = $this->db->escape($company_id);
        $user_id = $this->db->escape($user_id);
        $result = $this->db->prepare("DELETE FROM user_company WHERE user_id=? AND company_id=?");
        if($result->execute(array($user_id,$company_id)))
            return true;
        else
            return false;       
    }

    /**
     * @param $company_id
     * @return mixed
     */
    function getCompanyName($company_id)
    {
        $company_id = $this->db->escape($company_id);
        $result = $this->db->query("SELECT company_name FROM companies WHERE idx='$company_id'");
        $data = $result->fetch();

        return $this->db->escape($data['company_name']);
    }

    /**
     * @param $name
     * @return bool
     */
    function addCompany($name)
    {
        $stmt = $this->db->prepare("SELECT company_name FROM companies WHERE company_name=?");
        $stmt->execute(array(addslashes($name)));
        $count = count($stmt->fetchAll());
        if($count==0){
            $result = $this->db->prepare("INSERT INTO companies (company_name) VALUES (?)");
            if($result->execute(array($name)))
                return true;
            else
                return false;
        }else{
            return false;
        }
    }

    function addBlacklist($name)
    {
        $name = $this->db->escape($name);
        $sth = $this->db->prepare("SELECT `name` FROM blacklists WHERE `name`= ?");
        $sth->execute(array($name));
        $resultName = $sth->fetchAll();

        if($sth->rowCount()==0){
            $result = $this->db->prepare("INSERT INTO blacklists (`name`) VALUES (?)");
            if($result->execute(array($name)))
                return true;
            else
                return false;
        }else{
            return false;
        }
    }

    function deleteBlacklist($idx)
    {
        $result  = $this->db->prepare("DELETE FROM blacklists WHERE `idx`=?");
        $result2 = $this->db->prepare("UPDATE companies SET `blacklist_id`=0 WHERE `blacklist_id`=?");
        if($result->execute(array($idx)) && $result2->execute(array($idx)))
            return true;
        else
            return false;
    }

    /**
     * @param $call_sid
     * @param $contents
     * @param $user
     * @return array
     */
    function addNote($call_sid,$contents,$user)
    {
        $user = $this->getUserName($user);
        $result = $this->db->prepare("INSERT INTO call_notes (CallSid,NoteContents,User) VALUES (?,?,?);");
        if($result->execute(array($call_sid,$contents,$user)))
        {
            $tmp = $this->db->query("SELECT idx, DateAdded FROM call_notes WHERE idx=LAST_INSERT_ID()");
            $tmp = $tmp->fetch();
            return array(true,$tmp['idx'],$tmp['DateAdded'],$user);
        }
        else
            return array(false,print_r($result->errorInfo(),true));
    }

    /**
     * @param $call_sid
     * @param $note_id
     * @param $user
     * @param bool $is_admin
     * @return array
     */
    function deleteNote($call_sid,$note_id,$user,$is_admin=false)
    {
        if(!$is_admin)
        {
            if($this->isUserInCompany($this->getCompanyofCall($call_sid),$user))
            {
                $deleteProc = $this->db->prepare("DELETE FROM call_notes WHERE idx=? AND CallSid=?");
                if($deleteProc->execute(array($note_id,$call_sid)))
                    return array(true);
                else
                    return array(false, 1);
            }else
            {
                return array(false,2);
            }
        }else{
            $deleteProc = $this->db->prepare("DELETE FROM call_notes WHERE idx=? AND CallSid=?");
            if($deleteProc->execute(array($note_id,$call_sid)))
                return array(true);
            else
                return array(false, 1);
        }
    }

    /**
     * @param $CallSid
     * @return mixed
     */
    function getCompanyofCall($CallSid)
    {
        $result = $this->getCallDetails($CallSid);
        $phone = $this->format_phone_db($result['CallTo']);
        $companyqw = $this->db->query("SELECT company_id FROM company_num WHERE number='$phone'");
        $result = $companyqw->fetch();

        return $result['company_id'];
    }

    function getCompanyofOutgoingCall($CallSid)
    {
        $result = $this->getOutgoingCallDetails($CallSid);
        $phone = $this->format_phone_db($result['CallFrom']);
        $companyqw = $this->db->query("SELECT company_id FROM company_num WHERE number='$phone'");
        $result = $companyqw->fetch();

        return $result['company_id'];
    }

    /**
     * @param $company_id
     * @return bool
     */
    function deleteCompany($company_id)
    {
        $company_id = $this->db->escape($company_id);
        $_delete_users    = $this->db->prepare("DELETE FROM user_company WHERE company_id=?");
        $_delete_numbers  = $this->db->prepare("DELETE FROM company_num WHERE company_id=?");
        $_delete_company  = $this->db->prepare("DELETE FROM companies WHERE idx=?");
        $_delete_cf_mn    = $this->db->prepare("DELETE FROM cf_multiple_numbers WHERE company_id = ?");
        $_delete_cf_rr    = $this->db->prepare("DELETE FROM cf_round_robin WHERE company_id = ?");
        $_set_null_client = $this->db->prepare("UPDATE clients SET company_id = null, outgoing_numbers = null WHERE company_id = ?");
        $_delete_cpc      = $this->db->prepare("DELETE FROM company_phone_code WHERE company_id = ?");
        if( $_delete_users->execute(array($company_id)) &&
            $_delete_numbers->execute(array($company_id)) &&
            $_delete_company->execute(array($company_id)) &&
            $_delete_cf_mn->execute(array($company_id)) &&
            $_delete_cf_rr->execute(array($company_id)) &&
            $_set_null_client->execute(array($company_id)) &&
            $_delete_cpc->execute(array($company_id)) )
        {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @depreciated
     * @param $number
     * @return array
     */
    function getCallsToday($number)
    {
        $number = $this->db->escape("+1".$number);
        $date = new DateTime("Midnight");

        $ringto_numbers = "";
        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                if($access_range!=false)
                {
                    if($access_range[1]==1){
                        if($date->format("U") < strtotime($access_range))
                            $date = new DateTime($access_range);
                    }else{
                        if($date->format("U") > strtotime($access_range))
                            $date = new DateTime($access_range);
                    }
                }
                $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                if(count($filter_ringto_numbers)>0)
                {

                    foreach($filter_ringto_numbers as $ringto_num)
                    {
                        $ringto_numbers .= "'".$ringto_num->number."', ";
                    }

                    $ringto_numbers = substr($ringto_numbers,0,-2);
                    $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                }
            }
        }

        $hours = array();
        for($i = 0; $i <= 23; $i++)
        {
            $hours[] = 'null';
        }
        $calls = $this->db->query("SELECT DateCreated FROM calls WHERE CallTo='$number'".$ringto_numbers." AND `DateCreated` BETWEEN FROM_UNIXTIME(".$date->format('U').") AND NOW()");
        foreach ($calls as $call)
        {
            $time_of_call = new DateTime($call['DateCreated']);
            $hour = ($time_of_call->format('j')-1);
            if($hours[$hour] == 'null') { $hours[$hour] = 1; }else{
                $hours[$hour]++;
            }
        }
        
        return $hours;
    }

//    function getCallsforNumber($number,$intl)
//    {
//        if(!$intl){
//            $number = "+1".$number;
//        }
//
//        $result = $this->db->prepare("SELECT * FROM calls WHERE CallTo=?");
//
//        if($result->execute(array($number)))
//        {
//            return $result->fetchAll(PDO::FETCH_OBJ);
//        }else{
//            return false;
//        }
//    }

    /**
     * @depreciated
     * @param $number
     * @return array
     */
    function getCallsYesterday($number, $outgoing = false)
    {
        $number = $this->db->escape("+1".$number);
        $date = new DateTime("Midnight -1 day");//yesterday 12AM

        $ringto_numbers = "";
        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                if($access_range!=false)
                {
                    if($access_range[1]==1){
                        if($date->format("U") < strtotime($access_range))
                            $date = new DateTime($access_range);
                    }else{
                        if($date->format("U") > strtotime($access_range))
                            $date = new DateTime($access_range);
                    }
                }
                $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                if(count($filter_ringto_numbers)>0)
                {

                    foreach($filter_ringto_numbers as $ringto_num)
                    {
                        $ringto_numbers .= "'".$ringto_num->number."', ";
                    }

                    $ringto_numbers = substr($ringto_numbers,0,-2);
                    $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                }
            }
        }

        $extra_ad = "";
            
        if($outgoing==false){
            $table = "calls";
            $target_field = "CallTo";
        }else{
            $table = "outbound_calls";
            $target_field = "CallFrom";

            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";
        }

        $date_end = new DateTime_52("Midnight");
        $date_end->setTimestamp($date_end->format('U')-3600);
        $hours = array();
        for($i = 0; $i <= 23; $i++)
        {
            $hours[] = 'null';
        }
        $calls = $this->db->query("SELECT DateCreated FROM $table WHERE $target_field='$number'".$extra_ad.$ringto_numbers." AND `DateCreated` BETWEEN FROM_UNIXTIME(".$date->format('U').") AND FROM_UNIXTIME(".$date_end->format('U').")");

        foreach ($calls as $call)
        {
            $time_of_call = new DateTime($call['DateCreated']);
            $hour = ($time_of_call->format('j')-1);
            if($hours[$hour] == 'null') { $hours[$hour] = 1; }else{
                $hours[$hour]++;
            }
        }
        
        return $hours;
    }

    function getCallsInRange($company_id, $start_date, $end_date, $phone_code = 0, $call_result = 'all', $distinct=false, $outgoing_filter = "", $outgoing = false, $user = "", $campaign = "", $agent = "")
    {
        require_once('config.php');
        global $TIMEZONE;
        $company_id = $this->db->escape($company_id);

        $number_filter = "";

        //Get Numbers of the Company
        if($company_id==-1)
        {
            $resultCompany = $this->db->query("SELECT number,international FROM company_num;");
            $numbers = $resultCompany->fetchAll();
            foreach($numbers as $number)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($number['international']==0)
                                    $number_ = "+1".$number['number'];
                                else
                                    $number_ = $number['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($number['international']==0)
                    $number_filter .= "'+1".$number['number']."', ";
                else
                    $number_filter .= "'+".$number['number']."', ";
            }
            $number_filter = substr($number_filter, 0, -2);
        }else{
            $resultCompany = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id'");
            $numbers = $resultCompany->fetchAll();
            foreach($numbers as $number)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($number['international']==0)
                                    $number_ = "+1".$number['number'];
                                else
                                    $number_ = $number['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($number['international']==0)
                    $number_filter .= "'+1".$number['number']."', ";
                else
                    $number_filter .= "'+".$number['number']."', ";
            }
            $number_filter = substr($number_filter, 0, -2);
        }

        $phone_code_add = "";
        if($phone_code!=0)
        {
            $phone_code_add = "AND `PhoneCode`='".$this->db->escape($phone_code)."'";
        }

        $extra_ad = "";
        if($outgoing==false){
            $table = "calls";
            $target_field = "CallTo";
            $distinct_field = "CallFrom";
            $duration_field = "DialCallDuration";
        }else{
            $table = "outbound_calls";
            $target_field = "CallFrom";
            $distinct_field = "CallTo";
            $duration_field = "CallDuration";

            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";
        }

        $outgoing_filter_add = "";
        if($outgoing_filter!="" && $outgoing==false)
        {
            $outgoing_filter_add = " AND (DialCallTo LIKE '%".$outgoing_filter."' OR CallTo LIKE '%".$outgoing_filter."') ";
        }

        //Generate Some filters
        $call_result_filter = "";
        switch($call_result)
        {
            case "answered":
                {
                $call_result_filter = " AND `DialCallStatus`='completed'";
                break;
                }
            case "missed":
                {
                $call_result_filter = " AND `DialCallStatus`='no-answer'";
                break;
                }

            case 30:
            case 45:
            case 60:
            case 90:
            case 120:
            case 180:
            case 240:
            case 300:
            case 360:
            case 420:
            case 480:
            case 540:
            case 600:
            case 1200:
            case 1800:
            case 2400:
                {
                $call_result_filter = " AND `DialCallStatus`!='no-answer' AND `$duration_field` > ".$call_result;
                break;
                }

            case "all":
                {
                $call_result_filter = "";
                break;
                }
        }

        $ringto_numbers = "";
        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                if($access_range!=false)
                {
                    if($access_range[1]==1){
                        if(($start_date)<strtotime($access_range[0]))
                        {
                            $start_date = strtotime($access_range[0]);
                        }
                    }else{
                        if(($end_date)>strtotime($access_range[0]))
                        {
                            $end_date = strtotime($access_range[0]);
                        }
                    }
                }
                $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                if(count($filter_ringto_numbers)>0 && $outgoing==false)
                {

                    foreach($filter_ringto_numbers as $ringto_num)
                    {
                        $ringto_numbers .= "'".$ringto_num->number."', ";
                    }

                    $ringto_numbers = substr($ringto_numbers,0,-2);
                    $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                }
            }
        }
        
        if (trim($number_filter) == "") {
            
            $number_filter = "'none'";
        }

        $user_add = "";
        if($user!="")
            $user_add = " AND UserSource = ".$user;

        $agent_filter_add = "";

        if (!empty($agent) && !$outgoing) {
            $agent_details = $this->customExecute("SELECT * FROM users WHERE username = ? OR full_name = ? LIMIT 1");
            $agent_details->execute(array($agent, $agent));
            $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

            if (!empty($agent_details)) {
                $name = trim($agent_details['full_name']);
                if (empty($name)) $name = trim($agent_details['username']);
                else $name .= " (".$agent_details['username'].")";

                $agent_filter_add = " AND participants LIKE '[\"".$name."%' ";
            }
        }
        elseif (!empty($agent) && $outgoing) {
            $agent_details = $this->customExecute("SELECT * FROM users WHERE username = ? OR full_name = ? LIMIT 1");
                $agent_details->execute(array($agent, $agent));
                $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

                if (!empty($agent_details)) {
                    $user_add = " AND UserSource = ".$agent_details['idx'];
                }
        }

        if (empty($number_filter))
            $calls = array();
        else {
            if(!$distinct) {
                $calls = $this->db->query("SELECT * FROM $table WHERE `$target_field` IN ($number_filter)".$extra_ad.$ringto_numbers.$outgoing_filter_add.$agent_filter_add." AND DateCreated BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add.$user_add);
            }
            else
                $calls = $this->db->query("SELECT DISTINCT `$distinct_field` FROM $table WHERE `$target_field` IN ($number_filter)".$extra_ad.$ringto_numbers.$outgoing_filter_add.$agent_filter_add." AND DateCreated BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add.$user_add);

            $calls = $calls->fetchAll(PDO::FETCH_ASSOC);
        }
        
        $twilio_numbers = Util::get_all_twilio_numbers();

        $_calls = array();

        foreach($calls as $call)
        {
            $new_row = $call;
            if($call['CallTo']!="")
            {
                if (array_key_exists(format_phone($call['CallTo']), $twilio_numbers))
                {
                    if ($outgoing)
                        $this_campaign=(string)$twilio_numbers[format_phone($call['CallFrom'])];
                    else
                        $this_campaign=(string)$twilio_numbers[format_phone($call['CallTo'])];
                }
                else
                    $this_campaign="";

                $new_row["Campaign"] = $this_campaign;
            }

            if (empty($campaign) || $campaign === "false" || (!empty($campaign) && $campaign == $this_campaign))
                $_calls[] = $new_row;
        }

        return $_calls;
    }

    function getCallsInRangeCount($company_id, $start_date, $end_date, $phone_code = 0, $call_result = 'all', $distinct=false, $outgoing_filter = "", $outgoing = false, $user = "", $campaign = "", $agent = "")
    {
        require_once('config.php');
        global $TIMEZONE;
        $company_id = $this->db->escape($company_id);

        $number_filter = "";

        //Get Numbers of the Company
        if($company_id==-1)
        {
            $resultCompany = $this->db->query("SELECT number,international FROM company_num;");
            $numbers = $resultCompany->fetchAll();
            foreach($numbers as $number)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($number['international']==0)
                                    $number_ = "+1".$number['number'];
                                else
                                    $number_ = $number['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($number['international']==0)
                    $number_filter .= "'+1".$number['number']."', ";
                else
                    $number_filter .= "'+".$number['number']."', ";
            }
            $number_filter = substr($number_filter, 0, -2);
        }else{
            $resultCompany = $this->db->query("SELECT number,international FROM company_num WHERE company_id='$company_id'");
            $numbers = $resultCompany->fetchAll();
            foreach($numbers as $number)
            {
                $disabled = false;
                if(@$_SESSION['user_id']!="")
                {
                    if(!$this->isUserAdmin($_SESSION['user_id']))
                    {
                        $disabled_numbers = $this->getUserAccessNumbers($_SESSION['user_id']);
                        $number_array = array();
                        foreach($disabled_numbers as $disabled_number)
                        {
                            $number_array[] = $disabled_number->number;
                        }
                        if(count($number_array)>0)
                        {
                            foreach($number_array as $d_number)
                            {
                                if($number['international']==0)
                                    $number_ = "+1".$number['number'];
                                else
                                    $number_ = $number['number'];

                                if($number_===$d_number)
                                    $disabled = true;
                            }
                        }
                    }
                }
                if($disabled)
                    continue;

                if($number['international']==0)
                    $number_filter .= "'+1".$number['number']."', ";
                else
                    $number_filter .= "'+".$number['number']."', ";
            }
            $number_filter = substr($number_filter, 0, -2);
        }

        $phone_code_add = "";
        if($phone_code!=0)
        {
            $phone_code_add = "AND `PhoneCode`='".$this->db->escape($phone_code)."'";
        }

        $extra_ad = "";
        if($outgoing==false){
            $table = "calls";
            $target_field = "CallTo";
            $distinct_field = "CallFrom";
            $duration_field = "DialCallDuration";
            //$extra_voicemail = " DialCallStatus != 'voicemail' AND ";
        }else{
            $table = "outbound_calls";
            $target_field = "CallFrom";
            $distinct_field = "CallTo";
            $duration_field = "CallDuration";

            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";
        }

        $outgoing_filter_add = "";
        if($outgoing_filter!="" && $outgoing==false)
        {
            $outgoing_filter_add = " AND (DialCallTo LIKE '%".$outgoing_filter."' OR CallTo LIKE '%".$outgoing_filter."') ";
        }

        //Generate Some filters
        $call_result_filter = "";
        switch($call_result)
        {
            case "answered":
            {
                $call_result_filter = " AND `DialCallStatus`='completed'";
                break;
            }
            case "missed":
            {
                $call_result_filter = " AND `DialCallStatus`='no-answer'";
                break;
            }

            case "busy":
            {
                $call_result_filter = " AND `DialCallStatus`='busy'";
                break;
            }

            case "error":
            {
                $call_result_filter = " AND `DialCallStatus`='failed'";
                break;
            }

            case 30:
            case 45:
            case 60:
            case 90:
            case 120:
            case 180:
            case 240:
            case 300:
            case 360:
            case 420:
            case 480:
            case 540:
            case 600:
            case 1200:
            case 1800:
            case 2400:
            {
                $call_result_filter = " AND `DialCallStatus`!='no-answer' AND `$duration_field` > ".$call_result;
                break;
            }

            case "all":
            {
                $call_result_filter = "";
                break;
            }
        }

        $ringto_numbers = "";
        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                if($access_range!=false)
                {
                    if($access_range[1]==1){
                        if(($start_date)<strtotime($access_range[0]))
                        {
                            $start_date = strtotime($access_range[0]);
                        }
                    }else{
                        if(($end_date)>strtotime($access_range[0]))
                        {
                            $end_date = strtotime($access_range[0]);
                        }
                    }
                }
                $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                if(count($filter_ringto_numbers)>0 && $outgoing==false)
                {

                    foreach($filter_ringto_numbers as $ringto_num)
                    {
                        $ringto_numbers .= "'".$ringto_num->number."', ";
                    }

                    $ringto_numbers = substr($ringto_numbers,0,-2);
                    $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                }
            }
        }

        $user_add = "";
        if($user!="")
            $user_add = " AND UserSource = ".$user;

        $agent_filter_add = "";
        if (!empty($agent) && !$outgoing) {
            $agent_details = $this->customExecute("SELECT * FROM users WHERE username = ? OR full_name = ? LIMIT 1");
            $agent_details->execute(array($agent, $agent));
            $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

            if (!empty($agent_details)) {
                $name = trim($agent_details['full_name']);
                if (empty($name)) $name = trim($agent_details['username']);
                else $name .= " (".$agent_details['username'].")";

                $agent_filter_add = " AND participants LIKE '[\"".$name."%' ";
            }
        }
        elseif (!empty($agent) && $outgoing) {
            $agent_details = $this->customExecute("SELECT * FROM users WHERE username = ? OR full_name = ? LIMIT 1");
                $agent_details->execute(array($agent, $agent));
                $agent_details = $agent_details->fetch(PDO::FETCH_ASSOC);

                if (!empty($agent_details)) {
                    $user_add = " AND UserSource = ".$agent_details['idx'];
                }
        }

        if (empty($number_filter))
            $calls = array();
        else {
            if(!$distinct) 
                $query = "SELECT CallTo FROM $table WHERE `$target_field` IN ($number_filter)".$extra_ad.$ringto_numbers.$outgoing_filter_add.$agent_filter_add." AND DateCreated BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add.$user_add;
            else
                $query = "SELECT DISTINCT (`$distinct_field`), CallTo FROM $table WHERE  `$target_field` IN ($number_filter)".$extra_ad.$ringto_numbers.$outgoing_filter_add.$agent_filter_add." AND DateCreated BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'".$call_result_filter." ".$phone_code_add.$user_add;

            $calls_result = $this->db->query($query);
        }

        $total_calls = 0;

        $twilio_numbers = Util::get_all_twilio_numbers();

        if (empty($campaign) || $campaign == "false") {
            if (is_object($calls_result))
                $total_calls = $calls_result->rowCount();
            else
                $total_calls = 0;
        }
        else {
            while ($call = $calls_result->fetch()) {
                $callTo = format_phone($call["CallTo"]);
                if (array_key_exists($callTo, $twilio_numbers))
                {
                    $this_campaign=(string)$twilio_numbers[$callTo];
                }
                else {
                    $this_campaign="";
                }

                if ($this_campaign == $campaign) {
                    $total_calls++;
                }
            }
        }

        return $total_calls;
    }


    function getCallsInRangeForNum($twilio_number, $start_date, $end_date, $outgoing = false)
    {
        $distinct=false;
        require_once('config.php');
        global $TIMEZONE;

        $ringto_numbers = "";
        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                if($access_range!=false)
                {
                    if($access_range[1]==1){
                        if(($start_date)<strtotime($access_range[0]))
                        {
                            $start_date = strtotime($access_range[0]);
                        }
                    }else{
                        if(($end_date)>strtotime($access_range[0]))
                        {
                            $end_date = strtotime($access_range[0]);
                        }
                    }
                }
                $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                if(count($filter_ringto_numbers)>0 && $outgoing==false)
                {

                    foreach($filter_ringto_numbers as $ringto_num)
                    {
                        $ringto_numbers .= "'".$ringto_num->number."', ";
                    }

                    $ringto_numbers = substr($ringto_numbers,0,-2);
                    $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                }
            }
        }

        $extra_ad = "";
        if($outgoing==false){
            $table = "calls";
            $target_field = "CallTo";
            $distinct_field = "CallFrom";
            //$extra_voicemail = " DialCallStatus != 'voicemail' AND ";
        }else{
            $table = "outbound_calls";
            $target_field = "CallFrom";
            $distinct_field = "CallTo";
            $extra_voicemail = "";

            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";
        }

        if(!$distinct)
            $calls = $this->db->query("SELECT * FROM $table WHERE `$target_field` IN ('$twilio_number')".$extra_ad.$ringto_numbers." AND DateCreated BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'");
        else
            $calls = $this->db->query("SELECT DISTINCT `$distinct_field` FROM $table WHERE `$target_field` IN ('$twilio_number')".$extra_ad.$ringto_numbers." AND DateCreated BETWEEN '".date("Y-m-d H:i:s",$start_date)."' AND '".date("Y-m-d H:i:s",$end_date)."'");

        $twilio_numbers = Util::get_all_twilio_numbers();

        $_calls = array();

        $calls = $calls->fetchAll(PDO::FETCH_ASSOC);

        foreach($calls as $call)
        {
            $new_row = $call;
            if($call['CallTo']!="")
            {
                if (array_key_exists(format_phone($call['CallTo']), $twilio_numbers))
                {
                    $campaign=(string)$twilio_numbers[format_phone($call['CallTo'])];
                }
                else
                    $campaign="";

                $new_row["Campaign"] = $campaign;
            }
            $_calls[] = $new_row;
        }

        return $_calls;
    }


    /**
     * @param $number
     * @return array
     */
    function getCallsThisYear($number,$intl = false, $outgoing = false)
    {
        if(!$intl)
            $number = $this->db->escape("+1".$number);
        else
            $number = "+".$number;

        $month_stats = array("Jan"=>array(1,0),"Feb"=>array(2,0),
            "Mar"=>array(3,0),"Apr"=>array(4,0),
            "May"=>array(5,0),"Jun"=>array(6,0),
            "Jul"=>array(7,0),"Aug"=>array(8,0),
            "Sep"=>array(9,0),"Oct"=>array(10,0),
            "Nov"=>array(11,0),"Dec"=>array(12,0));

        $date_access_range = "";

        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);
                if($access_range!=false)
                {
                    if($access_range[1]==1)
                        $when = ">";
                    elseif($access_range[1]==2)
                        $when = "<";
                    else
                        $when = ">";
                    $date_access_range = " AND DateCreated ".$when." '".$access_range[0]."'";
                }
                $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);

                if(count($filter_ringto_numbers)>0)
                {
                    $ringto_numbers = "";
                    foreach($filter_ringto_numbers as $ringto_num)
                    {
                        $ringto_numbers .= "'".$ringto_num->number."', ";
                    }

                    $ringto_numbers = substr($ringto_numbers,0,-2);
                    $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                    $date_access_range = $ringto_numbers.$date_access_range;
                }
            }
        }

        $extra_ad = "";
        if($outgoing==false){
            $table = "calls";
            $target_field = "CallTo";
        }else{
            $table = "outbound_calls";
            $target_field = "CallFrom";

            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";
        }

        $calls = $this->db->query("SELECT DateCreated FROM $table WHERE $target_field='$number'".$extra_ad.$date_access_range);
        
        foreach($calls as $call)
        {
            $date = date("Y-n",strtotime($call['DateCreated']));
            $this_year = date("Y",strtotime("now"));
            switch($date)
            {
                case $this_year."-1":
                    $month_stats["Jan"][1]++;
                    break;
                case $this_year."-2":
                    $month_stats["Feb"][1]++;
                    break;
                case $this_year."-3":
                    $month_stats["Mar"][1]++;
                    break;
                case $this_year."-4":
                    $month_stats["Apr"][1]++;
                    break;
                case $this_year."-5":
                    $month_stats["May"][1]++;
                    break;
                case $this_year."-6":
                    $month_stats["Jun"][1]++;
                    break;
                case $this_year."-7":
                    $month_stats["Jul"][1]++;
                    break;
                case $this_year."-8":
                    $month_stats["Aug"][1]++;
                    break;
                case $this_year."-9":
                    $month_stats["Sep"][1]++;
                    break;
                case $this_year."-10":
                    $month_stats["Oct"][1]++;
                    break;
                case $this_year."-11":
                    $month_stats["Nov"][1]++;
                    break;
                case $this_year."-12":
                    $month_stats["Dec"][1]++;
                    break;
            }
        }
        return $month_stats;
    }

    /**
     * @param $number
     * @return array
     */
    function getCallsLastSeven($number,$intl = false, $outgoing = false)
    {
        require_once('config.php');
        global $TIMEZONE;
        if(!$intl)
            $number = $this->db->escape("+1".$number);
        else
            $number = "+".$number;

        //$thisSpan = date('Y-m-d',strtotime('-7 days ago'));
        $day_stats = array("1"=>0, "2"=>0,
            "3"=>0, "4"=>0,
            "5"=>0, "6"=>0,
            "7"=>0, "0"=>0);

        $date_access_range = "";

        if(@$_SESSION['user_id']!="")
        {
            if(!$this->isUserAdmin($_SESSION['user_id']))
            {
                $access_range = $this->getUserAccessRange($_SESSION['user_id']);

                if($access_range!=false)
                {
                    if($access_range[1]==1)
                        $when = ">";
                    elseif($access_range[1]==2)
                        $when = "<";
                    else
                        $when = ">";
                    $date_access_range = " AND DateCreated ".$when." '".$access_range[0]."'";
                }
                $filter_ringto_numbers = $this->getUserOutgoingAccessNumbers($_SESSION['user_id']);


                if(count($filter_ringto_numbers)>0)
                {
                    $ringto_numbers = "";

                    if( is_array($filter_ringto_numbers) || is_object($filter_ringto_numbers) )
                    foreach($filter_ringto_numbers as $ringto_num)
                    {
                        $ringto_numbers .= "'".$ringto_num->number."', ";
                    }

                    $ringto_numbers = substr($ringto_numbers,0,-2);
                    $ringto_numbers = " AND DialCallTo IN (".$ringto_numbers.")";
                    $date_access_range = $ringto_numbers.$date_access_range;
                }
            }
        }

        $extra_ad = "";
        if($outgoing==false){
            $table = "calls";
            $target_field = "CallTo";
        }else{
            $table = "outbound_calls";
            $target_field = "CallFrom";

            if (empty($_GET['agent']))
                $extra_ad = " AND MadeUsing != 'autodialer' ";
        }

        $calls = $this->db->query("SELECT DateCreated FROM $table WHERE $target_field='$number'".$extra_ad.$date_access_range);

        $calls = $calls->fetchAll();

        if( is_array($calls) || is_object($calls) )
        foreach($calls as $call)
        {
            //$comp_date = new DateTime();
            $tz = new DateTimeZone($TIMEZONE);
            $date = new DateTime($call['DateCreated']);
            $date->setTimezone($tz);
            $date = $date->format("Y-m-d");
            switch($date)
            {
                case date("Y-m-d",strtotime('7 days ago')):
                    $day_stats['7'] = $day_stats['7']+1;
                    break;
                case date("Y-m-d",strtotime('6 days ago')):
                    $day_stats['6'] = $day_stats['6']+1;
                    break;
                case date("Y-m-d",strtotime('5 days ago')):
                    $day_stats['5'] = $day_stats['5']+1;
                    break;
                case date("Y-m-d",strtotime('4 days ago')):
                    $day_stats['4'] = $day_stats['4']+1;
                    break;
                case date("Y-m-d",strtotime('3 days ago')):
                    $day_stats['3'] = $day_stats['3']+1;
                    break;
                case date("Y-m-d",strtotime('2 days ago')):
                    $day_stats['2'] = $day_stats['2']+1;
                    break;
                case date("Y-m-d",strtotime('1 days ago')):
                    $day_stats['1'] = $day_stats['1']+1;
                    break;
                case date("Y-m-d",strtotime('today')):
                    $day_stats['0'] = $day_stats['0']+1;
                    break;
            }
        }

        return $day_stats;
    }


    function setPhoneCode($call_sid,$phonecode,$type)
    {
        if($type==1)
            $stmt = $this->db->prepare("UPDATE calls SET `PhoneCode`=:pc WHERE `CallSid`=:sid");
        else
            $stmt = $this->db->prepare("UPDATE outbound_calls SET `PhoneCode`=:pc WHERE `CallSid`=:sid");
        if($stmt->execute(array(":pc"=>$phonecode,":sid"=>$call_sid)))
            return true;
        else
            return false;
    }
    function getPhoneCodes($company_id, $type = 1)
    {
        $stmt = $this->db->prepare("SELECT * FROM company_phone_code WHERE `company_id`=:c_id AND `type` = :pc_type");
        $stmt->execute(array(":c_id"=>$company_id, ":pc_type"=>$type));
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        if(!$result)
            return false;
        else
            return $result;
    }
    function deletePhoneCode($company_id,$idx)
    {
        $stmt = $this->db->prepare("DELETE FROM company_phone_code WHERE `company_id`=:c_id AND `idx`=:idx");
        if($stmt->execute(array(":c_id"=>$company_id,":idx"=>$idx)))
            return true;
        else
            return false;
    }
    function addPhoneCode($company_id,$name,$dc, $type = 1)
    {
        $stmt = $this->db->prepare("INSERT INTO company_phone_code (`company_id`,`name`,`order`, `type`) VALUES (:c_id,:name,:order,:type)");
        if($stmt->execute(array(":c_id"=>$company_id,":name"=>$name,":order"=>$dc, ":type"=>$type)))
            return true;
        else
            return false;
    }
    function getPhoneCodeName($idx,$type = 1)
    {
        $stmt = $this->db->prepare("SELECT `name` FROM company_phone_code WHERE `idx`=:idx AND `type`=:type");
        if($stmt->execute(array(":idx"=>$idx,":type"=>$type)))
            return $stmt->fetch(PDO::FETCH_OBJ)->name;
        else
            return "";
    }

    function getForwardedNumbers()
    {
        $results = $this->db->query("SELECT * FROM twilio_forward_number;");

        return $results->fetchAll(PDO::FETCH_OBJ);
    }

    function forward_num($Twilio_num){

        $result = $this->db->query("SELECT * FROM twilio_forward_number WHERE twilio_number='$Twilio_num'");

        $data = $result->fetchAll();

        return $data;
    }

    function add_forward_num($twilio_num,$caller_num,$forward_num){

        $result = $this->db->prepare("INSERT INTO twilio_forward_number(twilio_number,caller_number,forward_number) VALUES (?,?,?)");

        if($result->execute(array($twilio_num,$caller_num,$forward_num)))

            return $this->db->lastInsertId();

        else

            return false;
    }

    function delete_forward_num($id){

        $id = $this->db->escape($id);

        $result = $this->db->prepare("DELETE FROM twilio_forward_number WHERE id=?");

        if($result->execute(array($id)))

            return true;

        else

            return false;

    }

    function check_call_forward($twilio_num,$caller_num1){

        $result = $this->db->query("SELECT * FROM twilio_forward_number WHERE twilio_number='$twilio_num' and caller_number like '".substr($caller_num1,0,7)."%'");

        $data = $result->fetchAll();

        if(count($data)>0)
            return $data;
        else{
            $result = $this->db->query("SELECT * FROM twilio_forward_number WHERE twilio_number='$twilio_num' and caller_number like '".substr($caller_num1,0,4)."%'");

            $data = $result->fetchAll();
        }

        return $data;
    }

    //System Functions
    function customQuery($sql)
    {
        return $this->db->query($sql);
    }
    
    function lastInsertId(){
        return $this->db->lastInsertId();
    }

    function customExecute($sql)
    {
        return $this->db->prepare($sql);
    }

    /**
     * Gets the database version to verify update.
     *
     * @return int
     */
    function getDatabaseVersion()
    {
        global $forversion;
        smsin();
        $version = $forversion;
        return array($this->getVar("db_version"),$version);
    }

    function gaFireHit( $data = null ) {
        if ( $data ) {
            $getString = 'https://ssl.google-analytics.com/collect';
            $getString .= '?payload_data&';
            $getString .= http_build_query($data);
            $result = $this->curlGetData( $getString );

            return $result;
        }
        return false;
    }

//    function curlPostData($url, $data) {
//        $fields = '';
//        foreach($data as $key => $value) {
//            $fields .= $key . '=' . urlencode($value) . '&';
//        }
//        rtrim($fields, '&');
//
//        $post = curl_init();
//
//        curl_setopt($post, CURLOPT_URL, $url);
//        curl_setopt($post, CURLOPT_POST, count($data));
//        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
//        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
//
//        $result = curl_exec($post);
//        curl_close($post);
//
//        return $result;
//    }

    function curlGetData($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'ACT',
            CURLOPT_TIMEOUT => 5
        ));
        $resp = curl_exec($curl);
        curl_close($curl);

        return $resp;
    }

    /**
     * Gets a variable from the database.
     *
     * @param $key
     * @return mixed
     */
    function getVar($key)
    {
        $stmt = $this->db->prepare("SELECT `value` FROM options WHERE `key` = :key");
        $stmt->execute(array(":key" => $key));
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        if(!$result){
            return false;
        }else{
            return $result->value;
        }
    }

    function setVar($key,$value)
    {
        $stmt = $this->db->prepare("REPLACE INTO options(`key`,`value`) VALUES(:key,:value);");
        if($stmt->execute(array(":key"=>$key,":value"=>$value)))
            return true;
        else
            return false;
    }

    function deleteVar($key){
        $stmt = $this->db->prepare("DELETE FROM options WHERE `key` = ?");
        if($stmt->execute(array($key)))
            return true;
        else
            return false;
    }

    function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        $start  = $length * -1; //negative
        return (substr($haystack, $start) === $needle);
    }
    
    function getStarted($cId)
    {

        $res = $result = $this->db->query("SELECT * FROM call_ivr_widget WHERE companyId = $cId AND pId = '0'");
        $data = $result->fetch();
        if ( !empty($data) )
        {
            return  $data['wId'];
        }else{
            return '';
        }
    }
    function getWidget($wId,$keypress)
    {
        $kpr = ($keypress != "-" && !empty($keypress)) ? " and keypress ='$keypress' " : "";
        $res = $result = $this->db->query("SELECT * FROM call_ivr_widget WHERE wId = '$wId' ".$kpr." AND temporary = 0");
        $data = $result->fetch();
        return $data;
            
                    /*$widget['wId'] = $rs_nxt['wId'];
                    $widget['pId'] = $rs_nxt['pId'];
                    $widget['flowtype'] = $rs_nxt['flowtype'];
                    $widget['content_type'] = $rs_nxt['content_type'];
                    $widget['content'] = $rs_nxt['content'];
                    $widget['nId'] = $rs_nxt['nId'];
                    $widget['data'] = $rs_nxt['data']
                    $widget['keypress'] = $rs_nxt['keypress'];*/    
            
    }
    function getMenuItems($pId)
    {
        $result = $this->db->query("SELECT * FROM call_ivr_widget WHERE keypress != '-' and pId = $pId");
        $MenuItems = $result->fetchAll();
        
        $data = array();
        foreach ( $MenuItems  as $mk=>$mv) {

            $data[$mv['keypress']]  =$mv;
        }
        return $data;
    }
    
    //function for retrieving voicemail box by exten
    function getMailbox($voicemail_exten) {
    
        //make sure inputs are db safe
        $voicemail_exten = $this->db->escape($voicemail_exten);
        
        $result = $this->db->query("select * from voicemailbox where vmb_extension='$voicemail_exten'");
    
        $data = $result->fetch();
        $mailbox = false;
        if ( !empty($data) )
        {
            $mailbox = array();
            $mailbox['exten'] = $data['vmb_extension'];
            $mailbox['desc'] = $data['vmb_description'];
            $mailbox['passcode'] = $data['vmb_passcode'];
        }
        return $mailbox;
    
    }
    
    function addMessage($voicemail_exten, $caller_id, $recording_url, $call_sid, $recording_duration) {
    
        $voicemail_exten = $this->db->escape($voicemail_exten);
        $caller_id = $this->db->escape($caller_id);
        $recording_url = $this->db->escape($recording_url);
        
        // Performing SQL query
        $query = sprintf("insert into messages (message_frn_vmb_extension,"
            . "message_date,message_from,message_audio_url,message_flag,CallSid,RecordingDuration)"
            . " values ('%s','%s','%s','%s',0,'%s','%s')", $voicemail_exten, date('Y-m-d H:i:s',time()), $caller_id,
            $recording_url,$call_sid,$recording_duration);
        
        $this->db->query($query);
        $id =$this->db->lastInsertId(); 
        return $id;
    }
    
    function updateMessageFlag($msg_id, $flag=0){
    
        //make sure inputs are db safe
        $msg_id = $this->db->escape($msg_id);
        $flag = $this->db->escape($flag);
        
        // Performing SQL query
        $query = sprintf("update messages set message_flag=%d where message_id=%d",
            $flag, $msg_id);
        $this->db->query($query);
    }
    
    function getMessages($voicemail_exten,$flag=0){
    
    
        //make sure inputs are db safe
        $voicemail_exten = $this->db->escape($voicemail_exten);
        $flag = $this->db->escape($flag);
        
        // Performing SQL query
        $query = sprintf("select * from messages where message_flag=%d and "
            . "message_frn_vmb_extension='%s' order by message_date", $flag,
            $voicemail_exten);
        
        $result = $this->db->query("select * from voicemailbox where vmb_extension='$voicemail_exten'");
        
         $data = $result->fetchAll();
        
        $messages = array();
        foreach ( $data  as $mk=>$mv) {
            $messages[]=$mv['message_id'];
        }   
        return $messages;
    }
    
    function getMessage($msg_id){
        //make sure inputs are db safe
        $msg_id = $this->db->escape($msg_id);
        
        // Performing SQL query
        $query = sprintf("select * from messages where message_id=%d",$msg_id);
        
        
        $result = $this->db->query($query);
        
         $data = $result->fetch();
        
        $message = array();
        if(!empty($data)) {
            $message['id']=$data['message_id'];
            $message['date']=$data['message_date'];
            $message['from']=$data['message_from'];
            $message['url']=$data['message_audio_url'];
        }
        return $message;
    }
    
    function getNumberToDial($company_id,$open_number, $close_number)
    {

            require_once('config.php');
            global $TIMEZONE;
        
            
            

            $tz = new DateTimeZone($TIMEZONE);
            $date = new DateTime();
            $date->setTimezone($tz);
            
            
            $today = strtolower( $date->format("D")); 
            $todayDate = $date->format("Y-m-d");
            $curTime = strtotime($date->format("Y-m-d H:i"));
            
            
            $result = $this->db->query("select * from call_hours where `day` = '$today'  and  company_id=$company_id"); 


            $data = $result->fetch();

            if ( !empty($data) )
            {
                 $openTime =  strtotime($todayDate.' '.$data['open_time']);
                 $closeTime =  strtotime($todayDate.' '.$data['close_time']);
                 //echo '==curTime'.date("Y-m-d H:i", $curTime) .'==openTime'.date("Y-m-d H:i", $openTime) .'==closeTime'. date("Y-m-d H:i", $closeTime) ;
                if( $data['status']  == '1') return $close_number;          
                if ( $curTime >= $openTime &&  $closeTime > $curTime ) 
                    return $open_number; 
                else 
                    return $close_number;
                /////////////       
            }else 
                    return $open_number;
    
    }

    /*
    * Description: This function get the companyId through company name
    * Param : coampany name
    * Returns : company ID
    */
    function getCompanyId($comapnyName){
        $stmt = $this->db->prepare("SELECT `idx` FROM companies WHERE `company_name` = :comapnyName");
        $stmt->execute(array(":comapnyName" => $comapnyName));
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        if(!$result){
            return false;
        }else{
            return $result->idx;
        }
    }

    /*
    * Description : this function create a entry in pool table
    * Prama :   company_id and Quantity
    * Returns : Id of the pool
    */
    function createPool($company_id, $quantity){

        $stmt = $this->db->prepare("SELECT id FROM pools WHERE `company_id` = :c_id");
        $stmt->execute(array(":c_id"=>$company_id));

        $result = $stmt->fetch();
        
        if ($result['id']) {
            return $result['id'];
        }
        else {

            $stmt = $this->db->prepare("INSERT INTO pools (`company_id`,`quantity`) VALUES (:c_id,:quantity)");

            if($stmt->execute(array(":c_id"=>$company_id,":quantity"=>$quantity))){

                $pool_id = $this->db->lastInsertId();

                return $pool_id;

            }else{

                return false;

            }
        }

    }

    

    /*
    * Description : This function create a entry pof  new buy number through pool
    * Prama : array company_id, number, international, pool_id, keyword
    * Returns : boolean
    */
    function poolNumberInsert($data){
        $number = (string)$data['number'];
        if($data['international']==1)
            $number = substr($number,1);
        else
            $number = substr($number, 2);

        $stmt = $this->db->prepare("INSERT INTO company_num (`company_id`, `number`, `international`, `pool_id`) VALUES (:c_id,:number,:international,:pool_id)");
        if($stmt->execute(array(":c_id"=>$data['company_id'],":number"=>$number,":international"=>$data['international'],":pool_id"=>$data['pool_id']))){
            return true;
        }else{
            return false;
        }
    }

    

    /*
    * Description: This function get the all the company names
    * Param :
    * Returns : company names in an array
    */
    function getAllCompanyNames(){
        $result = $this->db->query("SELECT `company_name` FROM companies");
        $company = array();
        foreach($result as $key => $value){
            $company[] = $this->db->escape($value['company_name']);
        }
        return $company;
    }

    

    /*
    * Description: This function get the all the pool
    *
    *
    */

    function getAllPoolNumbers(){
        $result = $this->db->query("SELECT `number`, `pool_id`, `company_name` FROM `company_num` LEFT JOIN `companies` On company_num.company_id=companies.idx");

        $company = array();
        foreach($result as $key=>$value){
            $company[$key]['number']=$value['number'];
            $company[$key]['pool_id']=$value['pool_id'];
            $company[$key]['company_name']=$this->db->escape($value['company_name']);
        }
        return $company;
    }

    

    

    function getAllPoolNumbersGroupBy(){
        $result = $this->db->query("SELECT `number`,`company_name`,`reset_call_time`, company_num.company_id as company_id FROM `company_num` LEFT JOIN `companies` On company_num.company_id=companies.idx WHERE pool_id IS NOT NULL ORDER BY company_name");

        $company = array();
        foreach($result as $key=>$value){
            $company[$key][$value['company_name']]['number']=$value['number'];
            $company[$key][$value['company_name']]['company_name']=$this->db->escape($value['company_name']);
            $company[$key][$value['company_name']]['reset_call_time'] = $value['reset_call_time'];
            $company[$key][$value['company_name']]['company_id'] = $value['company_id'];
        }
        return $company;
    }

    
    
    function getAllPoolNumbersGroupByPoolId(){

        $result = $this->db->query("SELECT `pool_id`,`number`,`company_name`,`reset_call_time`,`assigned_number`, company_num.company_id as company_id FROM `company_num` LEFT JOIN `companies` On company_num.company_id=companies.idx WHERE pool_id IS NOT NULL ORDER BY pool_id");       
            
        $company = array();
        
        $pool_id = array();
        
        foreach($result as $key=>$value){
            
            $company[$key]['pool_id']=$value['pool_id'];
            
            $pool_id[] = $value['pool_id'];

            $company[$key]['number']=$value['number'];

            $company[$key]['company_name']=$this->db->escape($value['company_name']);
            
            $company[$key]['reset_call_time'] = $value['reset_call_time'];
            
            $company[$key]['company_id'] = $value['company_id'];
            
            $company[$key]['assigned_number'] = $value['assigned_number'];
            
        }
        $pool_id = array_unique($pool_id);
        
        $temp = array();
        
        $i=0;
        
        foreach($company as $key => $value){
            foreach($pool_id as $p ){
                if($p == $value['pool_id']){
                    $temp[$p]['numbers'][] = $value['number'];
                    if(!isset($temp[$p]['reset_call_time']) ){
                        $temp[$p]['reset_call_time']= $value['reset_call_time'];
                    }
                    if(!isset($temp[$p]['company_name'])){
                        $temp[$p]['company_name']= $this->db->escape($value['company_name']);
                    }
                    if(!isset($temp[$p]['company_id'])){
                        $temp[$p]['company_id']= $value['company_id'];  
                    }
                    if(!isset($temp[$p]['pool_id'])){
                        $temp[$p]['pool_id']= $value['pool_id'];    
                    }
                    if(!isset($temp[$p]['assigned_number'])){
                        $temp[$p]['assigned_number']= $value['assigned_number'];    
                    }
                }else{
                    $i = 0; 
                }
                
            }
        }
        return $temp;
    }
    
    
    function getAllPoolNumbersByPoolId($pool_id){
        $pool_id = (int)$pool_id;
        $result = $this->db->query("SELECT `number` FROM `company_num` WHERE `pool_id` = $pool_id");
        foreach($result as $r){
            $numbers[] = $r['number'];
        }       
        return $numbers;    
    }

    /*
    * Description: This function get the all the pool
    *
    *
    */

    function getAllPoolid(){
        $result = $this->db->query("SELECT `id` FROM `pools`");
        $company = array();
        foreach($result as $key => $value){
            $company[] = $value['id'];
        }
        return $company;
    }


    /*
    * description : This function insert the data in keyword pool table
    */

    function insertKeywordPool($sessionData, $spId=NULL){
        if($spId == NULL){
            $stmt = $this->db->prepare("INSERT INTO keyword_pool (`session_id`,`number`,`inuse`,`timestamp`,`lastseen`,`is_default_number`,`company_id`) VALUES (:session_id ,:number, :inuse, :timestamp, :lastseen, :is_default_number, :company_id)");
            if($stmt->execute(array(":session_id" =>$sessionData['session_id'], ":number"  => $sessionData['number'], ":inuse"  => 1, ":timestamp" => $sessionData['timestamp'], ":lastseen"  => $sessionData['lastseen'], ":is_default_number" => $sessionData['is_default_number'], ":company_id"  => $sessionData['company_id']))){
                $sp_id = $this->db->lastInsertId();

                return $sp_id;
            }
        }else{
             $stmt = $this->db->prepare("UPDATE keyword_pool SET `lastseen`= :lastseen WHERE `sp_id` = :spId");
             if($stmt->execute(array(":lastseen"  => $sessionData['lastseen'], ":spId"  => $spId))){
                return 'true';
             }else{
                return 'false';
             }

        }
    }

    function getKmIdByCallSid($call_sid){
        $stmt = $this->db->prepare("SELECT km_id FROM user_info WHERE sp_id = (SELECT SpId FROM calls WHERE CallSid = ?)");
        $stmt->execute(array($call_sid));
        return $stmt->fetch(PDO::FETCH_OBJ)->km_id;
    }

    /*
    * description : it will get the rendom number from company_num table
    * params : company Ids
    * return : number
    */

    function getRandomNumbers($company_id){
        //get the reset_call_time from companies table to use it further
        $stmt = $this->db->prepare("SELECT `reset_call_time` FROM companies WHERE `idx` = :company_id");
        $stmt->execute(array(":company_id" => $company_id));
        $resetCallTime = $stmt->fetch(PDO::FETCH_OBJ)->reset_call_time;
        //print_r($resetCallTime);die;

        //deduct specific seconds from the current time 
        $startTime = time()+100;
        
        $endTime =  time() - $resetCallTime ;
        
        $q = "SELECT number from keyword_pool Where lastseen <= $startTime AND lastseen >= $endTime AND company_id =$company_id";
        
        $result = $this->db->query($q);
        foreach($result as $n)
            $number_array[] = $n['number'];
            
        if(!empty($number_array)){
            $number_arr = implode(',',$number_array);
            $sql = "SELECT number from company_num WHERE number NOT IN ($number_arr) AND company_id=$company_id AND pool_id IS NOT NULL order by rand() limit 1";
        }else{
            $sql = "SELECT number from company_num WHERE company_id = $company_id AND pool_id IS NOT NULL order by rand() limit 1";
        }
        $rows = $this->db->query($sql);

        
        foreach($rows as $num) {
            $number = $num['number'];
        }

        return $number;
    }

    function snInPool( $to_num, $s_id ){
        $stmt = $this->db->prepare("SELECT `sp_id` FROM keyword_pool WHERE number=:number and sp_id = :sp_id");
        $stmt->execute(array(":number" => $to_num, ":sp_id" => $s_id));
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        if( !empty($result) ){
            return true;
        }else{
            return false;
        }
    }

    function numberInUse($number , $company_id){
        $callResetTime = $this->getCompanySettings($company_id);
        $compareTime = time()- $callResetTime->reset_call_time;
        $stmt =  $this->db->prepare("SELECT number from keyword_pool Where lastseen < :compareTime AND number = :number");
        $stmt->execute(array(":compareTime" => $compareTime,":number" => $number));
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        if($result == 0 && $result == NULL){
            return false;
        }else{
            return true;
        }
    }

    /*
    * Description : check keyword_detail table if its having the record or not.
    * Params : keyword (string), SpId (int)
    * returns : booln
    */

    

    function checkKeywordSpid($keyword, $spId, $sessionId)
    {
        $stmt =  $this->db->prepare("SELECT keyword_id FROM `keyword_pool` As k, keyword_detail As d WHERE k.sp_id = d.sp_id AND session_id = :sessionId AND k.sp_id = :spId AND d.keywords = :keyword");
        $stmt->execute(array(":sessionId" => $sessionId, ":spId" => $spId, ":keyword" => $keyword));
        $result = $stmt->fetch(PDO::FETCH_OBJ);

        if(empty($result) || $result == NULL)
            return 0;
        else
            return 1;
    }
    

    function InsertUserInfo($userInfo)
    {
         $stmt = $this->db->prepare("INSERT INTO user_info (`ipaddress`,`host`,`referer`,`user_agent`,`date`,`firstvisit`,`sp_id`,`km_id`) VALUES (:ipaddress, :host, :referer, :user_agent, :date, :firstvisit, :sp_id, :km_id)");
         return $stmt->execute(array(":ipaddress"=>$userInfo['ipaddress'], ":host"=>$userInfo['host'], ":referer"=>$userInfo['referer'],":user_agent"=>$userInfo['user_agent'], ":date"=>$userInfo['date'], ":firstvisit" =>$userInfo['firstvisit'], ":sp_id"=>$userInfo['sp_id'], ":km_id"=>$userInfo['km_id']));
    }
    /*
    * Description: This function check if we have an entry in keyword_detail table for that particular IP
    */
    

    function checkKeywordInfoEntry($keyword, $ip_address)
    {
        $stmt = $this->db->prepare("SELECT `keyword_id` FROM keyword_detail WHERE keywords=:keywords and remote_addr=:remote_addr and firsthit=:firsthit");
        $stmt->execute(array(":keywords" => $keyword, ":remote_addr" => $ip_address, ":firsthit" => 1));
        $result = @$stmt->fetch(PDO::FETCH_OBJ)->keyword_id;
        if($result)
            return true;
        else
            return false;
    }

    /*
    * Description: This function insert data in keyword info table
    */


    function insertKeywordInfo($keywordInfo){
         $stmt = $this->db->prepare("INSERT INTO keyword_detail (`keywords`,`medium`,`source`,`campaign`,`glcid`,`bouncerate`,`first_hit`,`remote_addr`,`sp_id`,`referrer`,`content`,`url`) VALUES (:keywords, :medium, :source, :campaign, :glcid, :bouncerate, :first_hit, :remote_addr, :sp_id, :referrer, :content, :url)");
         return $stmt->execute(array(":keywords"=>$keywordInfo['keywords'], ":medium"=>$keywordInfo['medium'], ":source"=>$keywordInfo['source'], ":campaign"=>$keywordInfo['campaign'], ":glcid"=>$keywordInfo['gclid'], ":bouncerate"=> 100, ":first_hit"=>$keywordInfo['firsthit'], ":remote_addr" =>$keywordInfo['remote_addr'] , ":sp_id"=>$keywordInfo['sp_id'], ":referrer"=>$keywordInfo['referrer'], ":content"=>$keywordInfo['content'], ":url"=>$keywordInfo['url']));
    }
    
    
    function getPoolDetail($pool_id){
        $data = array();
        $query = "SELECT * FROM pools where pools.id = ".$pool_id;
        $result = $this->db->query($query)->fetch();
        $data = $result;

        $query = "SELECT * FROM company_num where pool_id = ".$pool_id;
        $result = $this->db->query($query)->fetchAll();
        foreach($result as $value){
            $numbers[]          = $value['number'];
        }

        $data['numbers'] = $numbers;

        return $data;
    }
    

    /*

    * Description : updates reset call time at companies table 

    * params : company_id and value which must be above 60

    * return bool

    */

    function resetCallTime($company_id, $newValue){
        $stmt = $this->db->prepare("UPDATE companies SET `reset_call_time` = :newValue WHERE `idx` = :company_id");
        if($stmt->execute(array(":newValue"=>$newValue, "company_id"=>$company_id))){
            return true;
        }
        return false;
    }
    

    /*
    * Description : use to generate keyword reports
    * return : report data
    */
    function getKeywordReport($date1, $date2, $company_id, $limit, $keyword){
        $where ='';
        $condition ='';

        if(!empty($date1) && !empty($date2)){
            $condition[] = 'timestamp Between '.$date1.' AND '.$date2;
        }

        if(!empty($company_id)){
            if($company_id != '-1'){
                $condition[] = "company_id = '".$company_id."'" ;
            }
        }

        if(!empty($keyword)){
            $condition[] = "keywords = '".$keyword."'";
        }

        if(!empty($condition)){
            $where = 'WHERE '.implode(' AND ',$condition);
        }

        //$sql = "SELECT `keywords` AS keywords,count(`Index`) as total_calls, SUM(timestamp) AS sumt, SUM(lastseen) AS suml FROM `keyword_pool` AS p INNER JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id left join calls c on p.sp_id=c.SpId $where GROUP BY keywords ORDER BY total_calls DESC $limit";
        $sql = "SELECT `keywords` AS keywords,count(`Index`) as total_calls, SUM(timestamp) AS sumt, SUM(lastseen) AS suml,count(lastseen) AS totalhits FROM `keyword_pool` AS p INNER JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id left join calls c on p.sp_id=c.SpId $where GROUP BY keywords ORDER BY total_calls DESC $limit";  
        
        $row = $this->db->query($sql);
        foreach($row as $r){
            $keywords[$r['keywords']]['total_calls'] = $r['total_calls'];
            //total average time spend on the site
            $avg_time = $r['suml'] - $r['sumt'];

            $total_number = $r['totalhits'];
            
            $keywords[$r['keywords']]['average_time'] = ceil($avg_time/$total_number);
            //bounce rate in percentage through the keyword
            $query = "SELECT k.keywords AS keyword, (SELECT count(*) FROM keyword_detail WHERE keywords='".$r['keywords']."' AND bouncerate=100 ) As bounced,(SELECT count(*) FROM keyword_detail WHERE keywords='".$r['keywords']."' AND bouncerate=0 ) As Notbounced FROM keyword_detail k, keyword_pool s WHERE s.sp_id=k.sp_id and k.keywords='".$r['keywords']."' GROUP BY k.remote_addr ";
            $result = $this->db->query($query);

            $totalhits = 0;
            foreach($result as $value){
                $bounced    = $value['bounced'];
                $notBounced = $value['Notbounced'];
                $totalhits = $bounced+$notBounced;
                $bounceRate = round($bounced /$totalhits *100);
            }

            $keywords[$r['keywords']]['bounce_rate'] = $bounceRate;
        }
        return $keywords;
    }
    

    /*
    * Description : use to count the number of the records
    * return : number of records
    */

    function getKeywordReportCount($date1, $date2, $company_id){
        $where ='';
        if(!empty($date1)&& !empty($date2)&&!empty($company_id)){
            if($company_id == -1 || $company_id == 'none' ){
                $where = "WHERE timestamp Between $date1 AND $date2";
            }else{
                $where = "WHERE company_id = $company_id AND timestamp Between $date1 AND $date2";
            }
        }else if(empty($date1)&& empty($date2)&&!empty($company_id)){
            if($company_id == -1 || $company_id == 'none' ){
                $where = "";
            }else{
                $where = "WHERE company_id = $company_id";
            }
        }
        $sql = "SELECT `keywords` AS keywords,count(`Index`) as total_calls, SUM(timestamp) AS sumt, SUM(lastseen) AS suml FROM `keyword_pool` AS p INNER JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id left join calls c on p.sp_id=c.SpId $where GROUP BY keywords ORDER BY total_calls DESC";

        $row = $this->db->query($sql);

        foreach($row as $r){
            $data[] = $r['keywords'];
        }
        return count($data);
    }
    

    /*

    * Description : use to count the number of the records

    * return : number of records

    */

    function getKeywordGenreportCount($date1, $date2, $company_id){

                
        $where ='';
        

        if(!empty($date1)&& !empty($date2)&&!empty($company_id)){

            if($company_id == -1 || $company_id == 'none' ){

                $where = "AND timestamp Between $date1 AND $date2";
                
            }else{

                $where = "AND company_id = $company_id AND timestamp Between $date1 AND $date2";

            }

        }else if(empty($date1)&& empty($date2)&&!empty($company_id)){

            if($company_id == -1 || $company_id == 'none' ){

            }else{

                $where  = "AND company_id = $company_id";

            }

        }


        $sql = "SELECT `keywords` AS keywords, count( `Index` ) AS total_calls, count( 'p.keywords' ) AS total_hits, sum( lastseen - timestamp) as sum, k.source  FROM `keyword_pool` AS p INNER JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id JOIN calls c ON p.sp_id = c.SpId  $where GROUP BY keywords, source ORDER BY total_calls DESC";  

        $row = $this->db->query($sql);

        $data = array();

        if ($row) {
            foreach($row as $r){

                $data[]= $r['keywords'];

            }
        }

        return count($data);

    }

    

    

    /*

    * Description : use to gather the report of the generated calls data

    * return : data of the report in array 

    */

    function getKeywordGenreport($date1, $date2, $company_id, $limit){

                
        $where ='';
        
        $where2 = '';

        if(!empty($date1)&& !empty($date2)&&!empty($company_id)){

            if($company_id == -1 || $company_id == 'none' ){

                $where = "AND timestamp Between $date1 AND $date2"; 
                
                $where2 = "AND timestamp Between $date1 AND $date2";
                
            }else{

                $where = "AND company_id = $company_id AND timestamp Between $date1 AND $date2";
                
                $where2 = "AND company_id = $company_id AND timestamp Between $date1 AND $date2";   

            }

        }else if(empty($date1)&& empty($date2)&&!empty($company_id)){

            if($company_id == -1 || $company_id == 'none' ){

            }else{

                $where  = "AND company_id = $company_id";
                
                $where2 = "AND company_id = $company_id";   

            }

        }


        $sql = "SELECT `keywords` AS keywords, count( `Index` ) AS total_calls, count( 'p.keywords' ) AS total_hits, sum( lastseen - timestamp) as sum, k.source  FROM `keyword_pool` AS p INNER JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id JOIN calls c ON p.sp_id = c.SpId  $where GROUP BY keywords, source ORDER BY total_calls DESC $limit";  
        $row = $this->db->query($sql);

        $data = array();

        if ($row) {
            foreach($row as $r){

                $data[$r['keywords']]['total_hits'] = $r['total_hits'];

                $data[$r['keywords']]['total_calls'] = $r['total_calls'];

                $avg = round($r['sum'] / $r['total_calls']);

                $inSec = round($avg);

                $data[$r['keywords']]['average_time'] = $inSec;

                $medium = array();

                $source = array();

                //Getting medium and source for these keyword calls

                $keyword = $r['keywords'];

                $query1 = "SELECT source, medium FROM keyword_detail k, keyword_pool p, calls c  WHERE k.sp_id = p.sp_id AND p.sp_id = c.SpId AND keywords = '$keyword' $where2";
                
                $smValue = $this->db->query($query1);
                
                foreach($smValue as $value){

                    if($value['medium'] != '-' && $value['source'] != '-'){

                        $medium[] = $value['medium'];

                        $source[] = $value['source'];

                    }

                }

                $m = implode(',',$medium);

                $s = implode(',',$source);

                $data[$r['keywords']]['source'] = $s;

                $data[$r['keywords']]['medium'] = $m;

                // Unique hits from keyword_detail data

                $uniqueQuery = "SELECT count(DISTINCT remote_addr) as unq FROM keyword_detail k, keyword_pool p, calls c WHERE k.sp_id = p.sp_id AND p.sp_id = c.SpId AND keywords = '".$keyword."' $where2";

                $uniqueHit = $this->customExecute($uniqueQuery);
                $uniqueHit->execute(array());
                $uniqueHit = $uniqueHit->fetch();

                $data[$r['keywords']]['unique_hit'] = $uniqueHit['unq'];  

                //calculating bounce rate

                $query = "SELECT k.keywords AS keyword, (SELECT count(*) FROM keyword_detail WHERE keywords='".$r['keywords']."' AND bouncerate=100 ) As bounced,(SELECT count(*) FROM keyword_detail WHERE keywords='".$r['keywords']."' AND bouncerate=0 ) As Notbounced FROM keyword_detail k, keyword_pool s WHERE s.sp_id=k.sp_id and k.keywords='".$r['keywords']."' GROUP BY k.remote_addr ";

                $result = $this->db->query($query);

                            

                $totalhits = 0;

                foreach($result as $value){

                    $bounced    = $value['bounced'];

                    $notBounced = $value['Notbounced'];

                    $totalhits = $bounced+$notBounced;

                    $bounceRate = round($bounced /$totalhits *100);

                }

                $data[$r['keywords']]['bounce_rate'] = $bounceRate;

            }
        }

        return $data;

    }

    

    function getTotalCallsDone($date1,$date2, $company_id){



        $where = '';

        if(!empty($date1)&& !empty($date2)){

            if (empty($company_id) || $company_id == -1 || $company_id == 'none') {
                $where = "WHERE timestamp Between $date1 AND $date2";
            }
            else {
                $where = "WHERE company_id = $company_id AND timestamp Between $date1 AND $date2";
            }
        }
        else {
            if (!empty($company_id) && $company_id != -1 && $company_id != 'none') {
                $where = "WHERE company_id = $company_id";
            }
        }

        $sql = "SELECT `keywords` AS keywords, count( `Index` ) AS total_calls, count( 'p.keywords' ) AS total_hits, sum( lastseen - timestamp) as sum  FROM `keyword_pool` AS p INNER JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id JOIN calls c ON p.sp_id = c.SpId  $where GROUP BY keywords ORDER BY total_calls DESC $limit";    

        $row = $this->db->query($sql);

        $totalCalls = 0;

        $totalHits  = 0;

        foreach($row as $r){

            $data[$r['keywords']]['total_hits'] = $r['total_hits'];

            $data[$r['keywords']]['total_calls'] = $r['total_calls'];

            $totalCalls +=  $r['total_calls'];

            $totalHits  +=  $r['total_hits'];

        }

        $data['totalCalls'] = $totalCalls;

        $data['totalHits'] = $totalHits;

        return $data;

    }

    

    function getTotalHitsGraph($date1,$date2,$company_id){

        $where = '';

        if(!empty($date1)&& !empty($date2)){

            if (empty($company_id) || $company_id == -1 || $company_id == 'none') {
                $where = "AND timestamp Between $date1 AND $date2";
            }
            else {
                $where = "AND company_id = $company_id AND timestamp Between $date1 AND $date2";
            }

        }
        else {
            if (!empty($company_id) && $company_id != -1 && $company_id != 'none') {
                $where = "AND company_id = $company_id";
            }
        }

        $sql = "SELECT `keywords` AS keywords, count( 'p.keywords' ) AS total_hits, sum( lastseen - timestamp) as sum  FROM `keyword_pool` AS p JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id where p.sp_id not in (select SpId from calls WHERE SpId is not null) $where GROUP BY keywords"; 

        $row = $this->db->query($sql);

        $totalHits  = 0;

        foreach($row as $r){

            $data[$r['keywords']]['total_hits'] = $r['total_hits'];

            $totalHits  +=  $r['total_hits'];

        }

        $data['totalHits'] = $totalHits;

        return $data;

    }

    

    /*

    * Description : use to count the number of the records

    * return : number of records

    */

    function getKeywordNotGenreportCount($date1, $date2, $company_id){
        
        $where ='';

        if(!empty($date1)&& !empty($date2)&&!empty($company_id)){

            if($company_id == -1 || $company_id == 'none' ){

                $where = "AND timestamp Between $date1 AND $date2"; 

            }else{

                $where = "AND company_id = $company_id AND timestamp Between $date1 AND $date2";    

            }

        }else if(empty($date1)&& empty($date2)&&!empty($company_id)){

            if($company_id == -1 || $company_id == 'none' ){

                $where = "";    

            }else{

                $where = "AND company_id = $company_id";    

            }

        }

        $sql = "SELECT `keywords` AS keywords, count( 'p.keywords' ) AS total_hits, sum( lastseen - timestamp) as sum  FROM `keyword_pool` AS p JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id where (select COUNT(*) from calls c WHERE c.SpId = k.sp_id) = 0 $where GROUP BY keywords";  

        $row = $this->db->query($sql);

        foreach($row as $r){

            $data[]= $r['keywords'];

        }

        return count($data);

    }

    

    /*

    * Description : use to gather the report of the generated calls data

    * return : data of the report in array 

    */

    function getKeywordNotGenreport($date1, $date2, $company_id, $limit){
        
        $where ='';

        if(!empty($date1)&& !empty($date2)&&!empty($company_id)){

            if($company_id == -1 || $company_id == 'none' ){

                $where = "AND timestamp Between $date1 AND $date2"; 

            }else{

                $where = "AND company_id = $company_id AND timestamp Between $date1 AND $date2";    

            }

        }else if(empty($date1)&& empty($date2)&&!empty($company_id)){

            if($company_id == -1 || $company_id == 'none' ){

                $where = "";    

            }else{

                $where = "AND company_id = $company_id";    

            }

        }

        $sql = "SELECT `keywords` AS keywords, count( 'p.keywords' ) AS total_hits, sum( lastseen - timestamp) as sum  FROM `keyword_pool` AS p JOIN `keyword_detail` AS k ON p.sp_id = k.sp_id where (select COUNT(*) from calls c WHERE c.SpId = k.sp_id) = 0 $where GROUP BY keywords $limit";  
        
        $row = $this->db->query($sql);
        
        foreach($row as $r){

            $data[$r['keywords']]['total_hits'] = $r['total_hits'];

            $avg = round($r['sum'] / $r['total_hits']);

            $inSec = round($avg);

            $data[$r['keywords']]['average_time'] = $inSec;

            

            $medium = array();

            $source = array();

            

            //Getting medium and source for these keyword calls

            $keyword = $r['keywords'];

            $query1 = "SELECT source, medium FROM keyword_detail k, keyword_pool p, calls c  WHERE k.sp_id = p.sp_id AND p.sp_id != c.SpId AND keywords = '$keyword' $where GROUP BY keyword_id";

            $smValue = $this->db->query($query1);

            foreach($smValue as $value){

                if($value['medium'] != '-' && $value['source'] != '-'){

                    $medium[] = $value['medium'];

                    $source[] = $value['source'];

                }

            }

            $m = implode(', ',$medium);

            $s = implode(', ',$source);

            

            $data[$r['keywords']]['source'] = $s;

            $data[$r['keywords']]['medium'] = $m;

            

            // Unique hits from keyword_detail data

            $uniqueQuery = "SELECT count(DISTINCT remote_addr) as unq FROM keyword_detail k, keyword_pool p WHERE k.sp_id = p.sp_id AND (select COUNT(*) from calls c WHERE c.SpId = k.sp_id) = 0 AND keywords = '".$keyword."' $where";

            $uniqueHit = $this->customExecute($uniqueQuery);
            $uniqueHit->execute(array());
            $uniqueHit = $uniqueHit->fetch();

            $data[$r['keywords']]['unique_hit'] = $uniqueHit['unq'];    

            

            //calculating bounce rate

            $query = "SELECT k.keywords AS keyword, (SELECT count(*) FROM keyword_detail WHERE keywords='".$r['keywords']."' AND bouncerate=100 ) As bounced,(SELECT count(*) FROM keyword_detail WHERE keywords='".$r['keywords']."' AND bouncerate=0 ) As Notbounced FROM keyword_detail k, keyword_pool s WHERE s.sp_id=k.sp_id and k.keywords='".$r['keywords']."' GROUP BY k.remote_addr ";

            $result = $this->db->query($query);

                        

            $totalhits = 0;

            foreach($result as $value){

                $bounced    = $value['bounced'];

                $notBounced = $value['Notbounced'];

                $totalhits = $bounced+$notBounced;

                $bounceRate = round($bounced /$totalhits *100);

            }

            $data[$r['keywords']]['bounce_rate'] = $bounceRate;

        }

        return $data;

    }

    function getLockedNumber($company_id, $limit = ""){

        $where = '' ;

        if(!empty($company_id)){

            $where = "WHERE idx = $company_id ";

        }

        $sql = "SELECT idx as company_id, reset_call_time, company_name FROM `companies` $where";

        $row = $this->db->query($sql);

        $data = array();

        if ($row) {
            foreach($row as $r){

                $callTime = $r['reset_call_time']; 

                $company_id = $r['company_id'];

                $company_name = $this->db->escape($r['company_name']);

                $diffTime = time() - $callTime;

                $query = "SELECT number,timestamp, lastseen, keywords, medium, source, remote_addr, host, user_agent FROM `keyword_pool` k, keyword_detail  i, user_info u WHERE k.sp_id = i.sp_id AND k.sp_id = u.sp_id AND `lastseen` > $diffTime AND company_id =$company_id $limit";

                $result = $this->db->query($query);

                foreach($result as $key => $value){

                    $data[$key]['number'] = $value['number'];

                    $data[$key]['timestamp'] = $value['timestamp'];

                    $data[$key]['lastseen'] = $value['lastseen'];

                    $data[$key]['keywords'] = $value['keywords'];

                    $data[$key]['medium'] = $value['medium'];

                    $data[$key]['source'] = $value['source'];

                    $data[$key]['remote_addr'] = $value['remote_addr'];

                    $data[$key]['host'] = $value['host'];

                    $data[$key]['user_agent'] = $value['user_agent'];

                }

            }
        }

        return $data;

    }

    

    

    function getLockedNumberCount($company_id){

        $where = '' ;

        if(!empty($company_id)){

            $where = "WHERE idx = $company_id ";

        }

        $sql = "SELECT idx as company_id, reset_call_time, company_name FROM `companies` $where";

        $row = $this->db->query($sql);

        $data = array();

        if ($row) {
            foreach($row as $r){

                $callTime = $r['reset_call_time']; 

                $company_id = $r['company_id'];

                $company_name = $this->db->escape($r['company_name']);

                $diffTime = time() - $callTime;

                $query = "SELECT number,timestamp, lastseen, keywords, medium, source, remote_addr, host, user_agent FROM `keyword_pool` k, keyword_detail  i, user_info u WHERE k.sp_id = i.sp_id AND k.sp_id = u.sp_id AND `lastseen` > $diffTime AND company_id =$company_id";

                $result = $this->db->query($query);

                foreach($result as $key => $value){

                    $data[$key]['number'] = $value['number'];

                }

            }
        }

        return count($data);

    }

    

    

    function getLastSessionreport($date1, $date2, $company_id, $limit){     
        $where = " WHERE k.sp_id = i.sp_id AND k.sp_id = u.sp_id ";

        if (!empty($date1)&& !empty($date2))
            $where .= "AND timestamp Between $date1 AND $date2 ";

        if($company_id == -1 || $company_id == 'none' ){

        } else {
            if (!empty($company_id))
                $where .= "AND company_id = $company_id ";
        }

        $data = array();

        $query = "SELECT number,timestamp, lastseen, keywords, medium, source, remote_addr, host, user_agent FROM `keyword_pool` k, keyword_detail  i, user_info u $where ORDER BY timestamp DESC $limit";

        $result = $this->db->query($query);

        foreach($result as $key => $value){
            $data[$key]['number'] = $value['number'];
            $data[$key]['timestamp'] = $value['timestamp'];
            $data[$key]['lastseen'] = $value['lastseen'];
            $data[$key]['keywords'] = $value['keywords'];
            $data[$key]['medium'] = $value['medium'];
            $data[$key]['source'] = $value['source'];
            $data[$key]['remote_addr'] = $value['remote_addr'];
            $data[$key]['host'] = $value['host'];
            $data[$key]['user_agent'] = $value['user_agent'];
        }

        return $data;
    }

    function getLastSessionreportCount($date1, $date2, $company_id){     
        $where = " WHERE k.sp_id = i.sp_id AND k.sp_id = u.sp_id ";

        if (!empty($date1)&& !empty($date2))
            $where .= " AND timestamp Between $date1 AND $date2 ";

        if($company_id == -1 || $company_id == 'none' ){

        } else {
            if (!empty($company_id))
                $where .= "AND company_id = $company_id ";
        }

        $data = array();

        $query = "SELECT COUNT(*) as total FROM `keyword_pool` k, keyword_detail  i, user_info u $where ORDER BY timestamp DESC ";

        $row = $this->db->query($query);
        $row = $row->fetch();

        $row['total'];

        return $row['total'];
    }

    function getDefaultNumberLastSessionReport($company_id){

        $where = '' ;
        
        $where2 = '';

        if(!empty($company_id)){

            $where = "WHERE idx = $company_id ";

            $where2 = "AND company_id = $company_id";

        }

        $sql = "SELECT idx as company_id, reset_call_time, company_name FROM `companies` $where";

        $row = $this->db->query($sql);

        $data = array();

        if ($row) {
            foreach($row as $r){

                $callTime = $r['reset_call_time']; 

                $company_id = $r['company_id'];

                $company_name = $this->db->escape($r['company_name']);

                $diffTime = time() - $callTime;
                // AND `is_default_number` = 1
                $query = "SELECT number,timestamp, lastseen, keywords, medium, source, remote_addr, host, user_agent FROM `keyword_pool` k, keyword_detail  i, user_info u WHERE k.sp_id = i.sp_id AND k.sp_id = u.sp_id AND `lastseen` > $diffTime $where2 GROUP BY k.number ORDER BY timestamp DESC";

                //$query;

                $result = $this->db->query($query);

                foreach($result as $key => $value){

                    $data[$key]['number'] = $value['number'];

                    $data[$key]['timestamp'] = $value['timestamp'];

                    $data[$key]['lastseen'] = $value['lastseen'];

                    $data[$key]['keywords'] = $value['keywords'];

                    $data[$key]['medium'] = $value['medium'];

                    $data[$key]['source'] = $value['source'];

                    $data[$key]['remote_addr'] = $value['remote_addr'];

                    $data[$key]['host'] = $value['host'];

                    $data[$key]['user_agent'] = $value['user_agent'];

                }

            }
        }

        return $data;

    }
/*  

    function getSPID($sessionID){

        $stmt = $this->db->prepare("SELECT sp_id FROM keyword_pool WHERE `session_id` = :sessionId order by sp_id DESC limit 1");

        $stmt->execute(array(":sessionId" => $sessionID));

        $result = $stmt->fetch(PDO::FETCH_OBJ)->sp_id;

        if($result){

            

        }else{

            

        }

    }*/

    

    /*

    * Description : this function will get the sp_id from keyword_pool table

    * params : number 

    * returns : sp_id if found else empty

    * important : used in save_call function

    */

    

    function getNumberDetails($number){
        
        $CallFrom = @$_REQUEST['From'];
        $CallTo   = @$_REQUEST['To'];

        $number2 = trim($number);
        $number2 = str_replace("+", "", $number2);
        $number = str_replace("+1", "", $number);
        $number = str_replace("+", "", $number);
        $number = urldecode($number);
        $number = trim($number);

        $stmt = $this->db->prepare("SELECT company_id, reset_call_time FROM company_num INNER JOIN `companies` ON company_id = idx  WHERE number = :number OR number = :number2");

        $stmt->execute(array(":number" => $number, ":number2" => $number2));

        $result = $stmt->fetch(PDO::FETCH_OBJ);

        $company_id = @$result->company_id;

        $resetCallTime = @$result->reset_call_time;

        //getting the sp_id if number is in use

        $compareTime = time()- $resetCallTime;

        $stmt =  $this->db->prepare("SELECT * from keyword_pool Where number = :number OR number = :number2");

        $stmt->execute(array(":number" => $number, ":number2" => $number2));

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        $stmt =  $this->db->prepare("SELECT sp_id from keyword_pool Where lastseen > :compareTime AND number = :number OR number = :number2");

        $stmt->execute(array(":compareTime" => $compareTime,":number" => $number, ":number2" => $number2));

        $result = $stmt->fetch(PDO::FETCH_OBJ);

        if($result == 0 && $result == NULL){
            
            $sql = "SELECT SpId FROM `calls` WHERE `CallTo` ='".$CallTo."' AND `CallFrom`='".$CallFrom."' AND SpId IS NOT NULL ORDER BY `calls`.`Index` DESC LIMIT 1";
            $result2 = $this->db->query($sql);
            if(!empty($result2)){
                foreach($result2 as $r){
                    return (int)$r['SpId']; 
                }
            }else{
                return 0;
            }
        }else{
            return (int)$result->sp_id; 
        }
    }


        
    function checkNumberExsist($number, $company_id){
        
        $stmt =  $this->db->prepare("SELECT number from company_num Where company_id = :comapny_id AND number = :number");

        $stmt->execute(array(":comapny_id" => $company_id,":number" => $number));
        
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        if($result)
            return true;
        else
            return false;
    }


    /**
    * Description : this function will update the pool size in pools table
    * params : session_id 
    * returns boolen
    */

    function updatePoolQuantity($poolSize,$poolId){
        
        $poolSize = (int)$poolSize;
        
        $poolId   = (int)$poolId;
        
        $stmt = $this->db->prepare('UPDATE pools set quantity = :quantity WHERE id = :poolId');
        
        if($stmt->execute(array(':quantity' => $poolSize, ':poolId' => $poolId)))
            return true;
        else
            return false;
    }
    

    /**

    * Description : this function will update the bounce rate in keyword_detail table

    * params : session_id 

    * returns boolen

    */

    

    function updateBounceRate($spId){

        /*

        $session = trim($sessionId);

        $stmt = $this->db->prepare("SELECT `sp_id` FROM `keyword_pool` WHERE `sp_id` = :spId");

        $stmt->execute(array(":spId" => $spId));

        $result = $stmt->fetch(PDO::FETCH_OBJ);

        $spId = $result->sp_id;*/

        

        // Updating the bounce rate  at keyword_detail table through sp_id

        $stmt = $this->db->prepare('UPDATE keyword_detail set bouncerate = 0 WHERE sp_id = ?');

        if($stmt->execute(array($spId)))

            return true;

        else

            return false;

        

    }
    
    /**
    * Description : This function will get the SpId of the particluar call from calls table through callSid
    * params : callSid
    * returns (string)Spid
    */ 
    
    function getSpIdByCallSid($callSid){
        $callSid = $this->db->escape($callSid);
        $query = $this->db->prepare("SELECT SpId FROM calls WHERE CallSid = :callSid");
        $query->execute(array('callSid' => $callSid));
        $Spid = $query->fetch(PDO::FETCH_OBJ)->SpId;
        
        return $Spid;
    }
    
    /**

    * Description : this function will get the url visited by user 

    * params : sp_id 

    * returns array containing url

    */
    
    function getVisitedUrls($sp_id){
        
        $spId = $this->db->escape($sp_id);
        
        $query = $this->db->prepare("SELECT * FROM  url_visited  WHERE sp_id = :sp_id");
        
        $query->execute(array('sp_id' => $sp_id));
        
        $result = $query->fetch(PDO::FETCH_OBJ)->urls;  
        
        $urls = unserialize($result);

        return $urls;
    }



    /**
    * Description : this function will update the url visited by user
    * params : sp_id and url
    * returns boolen
    */

    
    function updateUrlVisited($sp_id, $url){
        
        $spId = $this->db->escape($sp_id);

        $urls = $this->getVisitedUrls($sp_id);
        
        $i = count($urls);
        
        $urls[$i]['url']= $url;
        
        $urls[$i]['time']= time();
        
        $urls = serialize($urls);
        
        $stmt = $this->db->prepare('UPDATE url_visited set urls = :urls WHERE sp_id = :sp_id');
        if($stmt->execute(array(':urls' => $urls, ':sp_id' => $sp_id))){
            return true;
        }else{
            return false;
        }
        
    }
    
    
    
    /**

    * Description : this function will insert the url visited by user 

    * params : sp_id and url

    * returns boolen

    */
    
    function insertUrls($spId, $url){
        
        $spId = $this->db->escape($spId);
        //$url = $this->db->escape($url);
        $urls = array();
        $urls[0]['url']     = $url;
        $urls[0]['time']    = time();
        $urls = serialize($urls);
        try{
            $stmt = $this->db->prepare('INSERT INTO url_visited (sp_id, urls) VALUES (:sp_id,:url)');
            $vars=array(':sp_id' => $spId,':url' =>$urls);
            $stmt->execute($vars);
        }catch(PDOException $ex){
            return $ex;
        }
        return true;
    }
    
    
    /**

    * Description : this function will 

    * params : visitedUrls

    * returns boolen

    */
    
    function getCalledPaged($visitedUrls, $spId, $callTime){
        $timeUrl = array();
        $callTime = strtotime($callTime);
        if(!empty($visitedUrls)){
            foreach($visitedUrls as $time){
                $timeUrl[] = $time['time']; 
            }
            sort($timeUrl);
            
            foreach($timeUrl as $ar){
                
                if($ar <= $callTime){
                    $time = $ar;
                }
                
            }
            return  $time;
        }

        return  '';         
    }

    /**
     * @param $number
     * @return mixed
     */
    function getAdVbCampaignByNumber($number)
    {
        $query = $this->db->query("SELECT * FROM ad_vb_campaigns WHERE phone_number='$number'");
        if($query === false)
            return false;
        else{
            return $query->fetch();
        }
    }

    /**
     * @param $number
     * @return mixed
     */
    function getContactListById($list_id)
    {
        $query = $this->db->query("SELECT * FROM ad_advb_contact_lists WHERE idx='$list_id'");
        if($query === false)
            return false;
        else{
            return $query->fetch();
        }
    }

    /**
     * @param $number
     * @return mixed
     */
    function checkNumberOptedOut($list_id, $number)
    {
        $query = $this->db->query("SELECT opt_out FROM ad_advb_cl_contacts WHERE phone_number = '".$number."'");
        $result = $query->fetch();

        $query = $this->db->query("SELECT id FROM opt_out WHERE list_id = '".$list_id."' AND number_opted_out='".$number."'");
        $result2 = $query->fetchAll();

        if($result['opt_out'] == 1 || sizeof($result2) > 0)
            return true;
        else{
            return false;
        }
    }

    /**
     * @param $number
     * @return mixed
     */
    function getContactByPhoneNumber($number)
    {
        $query = $this->db->query("SELECT * FROM ad_advb_cl_contacts WHERE phone_number = '".$this->format_phone_db($number)."'");
        $result = $query->fetch();

        if($result)
        {
            return $result;
        }else
            return false;
    }

    /**
     * @param $number
     * @return mixed
     */
    function getContactByPhoneNumberIfInAList($number)
    {
        $query = $this->db->query("SELECT * FROM ad_advb_cl_contacts WHERE phone_number = '".$this->format_phone_db($number)."'");
        $result = $query->fetch();

        if($result)
        {
            $query2 = $this->db->query("SELECT COUNT(*) as total FROM ad_advb_cl_link WHERE contact_idx = '".$result['idx']."'");
            $result2 = $query2->fetch();

            if ($result2['total'] > 0) return $result; else return false;
        }else
            return false;
    }

    function getRecording($recordingId)
    {
        $stmt = $this->db->prepare("SELECT * FROM recordings WHERE unique_hash=:unique_hash");
        if($stmt->execute(array(":unique_hash"=>$recordingId)))
        {
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $result[0];
        }else{
            return false;
        }
    }

    function save_sms_forward ($from, $to)
    {
        $stmt = $this->db->prepare("REPLACE INTO sms_forward_number (sms_forward_number_from, sms_forward_number_to) VALUES (?,?);");

        if($stmt->execute(array($from, $this->format_phone_db($to))))
            return true;
        else
            return false;
    }

    function get_sms_forward ($from)
    {
        $stmt = $this->db->prepare("SELECT sms_forward_number_to FROM sms_forward_number WHERE sms_forward_number_from = ?");
        $stmt->execute(array($from));
        $result = $stmt->fetch();

        if($result)
        {
            return $result['sms_forward_number_to'];
        }else
            return false;
    }

    function saveTrackingSettings($company_id, $clients, $distribution, $emails, $billing_tag, $client_email_tags)
    {
        $stmt = $this->db->prepare("REPLACE INTO email_tracking_settings (company_id, clients, distribution, emails, billing_tag, client_email_tags) VALUES (?,?,?,?,?,?);");

        if($stmt->execute(array($company_id, $clients, $distribution, $emails, $billing_tag, $client_email_tags)))
            return true;
        else
            return false;
    }

    function getTrackingSettings ()
    {
        $stmt = $this->db->prepare("SELECT * FROM email_tracking_settings");
        if($stmt->execute())
        {
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $result;
        }else{
            return false;
        }
    }

    function getTrackingSettingsForCompany($company_id)
    {
        $stmt = $this->db->prepare("SELECT * FROM email_tracking_settings WHERE company_id = ?");
        if($stmt->execute(array($company_id)))
        {
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            return $result;
        }else{
            return false;
        }
    }

    function companyHasTrackingSetup ($company_id)
    {
        $stmt = $this->db->prepare("SELECT id FROM email_tracking_settings WHERE company_id = ?");
        $stmt->execute(array($company_id));
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        if (count($result) == 0) {
            return false;
        }else{
            return true;
        }
    }

    function deleteTrackingSettings ($company_id)
    {
        $stmt = $this->db->prepare("DELETE FROM email_tracking_settings WHERE company_id = ?");
        $stmt->execute(array($company_id));
    }

    function getTrackingEmailsLog($company_id, $start_date, $end_date, $client_id, $email_address, $pagination)
    {
        require_once('Pagination.php');

        $return = array(
            'total' => 0,
            'emails' => array()
        );

        $where = " WHERE company_id = '".addslashes($company_id)."' ";

        if (!empty($start_date))
            $where .= " AND email_date >= '".addslashes($start_date)."' ";

        if (!empty($end_date))
            $where .= " AND email_date <= '".addslashes($end_date)."' ";

        if (!empty($client_id))
            $where .= " AND client_id = '".addslashes($client_id)."' ";

        if (!empty($email_address))
            $where .= " AND to_email = '".addslashes($email_address)."' ";

        $stmt = $this->customQuery("SELECT * FROM email_tracking_log ".$where." ORDER BY id DESC ".$pagination->getLimitSql());

        if ($stmt->execute()) {
            $return['emails'] = $stmt->fetchAll(PDO::FETCH_OBJ);

            $stmt2 = $this->customQuery("SELECT COUNT(*) as total FROM email_tracking_log ".$where." ORDER BY id DESC ");
            $stmt2->execute();
            $result = $stmt2->fetch();

            $return['total'] = $result['total'];

        }

        return $return;
    }
}

function smsin(){
    $dir = "";
    if(__DIR__ == "__DIR__")
        $dir = $_SERVER["DOCUMENT_ROOT"]."/include/Services/ChromePhp.php";
    else
        $dir = __DIR__."/Services/ChromePhp.php";
    if(!file_exists($dir)){
        $dir = "./include/Services/ChromePhp.php";
        if(!file_exists($dir)){
            $dir = "../include/Services/ChromePhp.php";
            if(!file_exists($dir)){
                $dir = "Services/ChromePhp.php";
            }
        }
        require_once($dir);
    }else{
        require_once($dir);
    }
    global $forversion;
    $dv = new DatabaseVersion;
    $forversion = $dv->get();
}
