<?php

/**
 * Database Connection class which is just a wrapper around PDO.
 */
class DBConnection extends PDO {

	public static function getConnection() {
		
		//for security reasons, we include the config file only here.
		require(realpath(dirname(__FILE__))."/config.php");
		
		try {
			
			//create a persistent PDO object that can be used be subsequent scripts.
			return new PDO("mysql:host=" . $DB_HOST . ";dbname=" . $DB_NAME, $DB_USER, $DB_PASS, array(
				PDO::ATTR_PERSISTENT => true
			));
			
		} catch(PDOException $ex) {
		
			echo("Error!: " . $ex->getMessage() . "<br/>");
			exit();
		}
	}
}
?>