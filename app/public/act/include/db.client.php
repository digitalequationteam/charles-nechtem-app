<?php

require_once(realpath(dirname(__FILE__))."/db.connection.php");

/**
 * Database companies class encapsulating companies methods.
 */
class Client {

	private $_dbConnection;

	public function __construct() {
		
		$this->_dbConnection = DBConnection::getConnection();
	}
	
	/**
	 * Get clients.
	 */
	public function getClients($data) {
		
		//the sql statement.
		$sql = "select * from clients";
		
		if (isset($data["client_id"])) {
			
			$sql .= " where id = ?";
		}
		
		if (isset($data["company_id"])) {
			
			$sql .= " where company_id = ?";
		}
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		if (isset($data["client_id"])) {
			
			//execute the sql statement.
			$result->execute(array($data["client_id"]));
		}
		elseif (isset($data["company_id"])) {
			
			//execute the sql statement.
			$result->execute(array($data["company_id"]));
		}
		elseif (isset($data["all"])) {
			
			//execute the sql statement.
			$result->execute();
		}else{

            //execute the sql statement.
            $result->execute();
        }

		$result = $result->fetchAll(PDO::FETCH_ASSOC);

        $sql = "select * from invoices where `client_id` = ? and active=1 ORDER BY `id` DESC LIMIT 1";
        $sql2 = "select * from invoices where `client_id` = ? and active=1 AND is_pending = 0 ORDER BY `id` DESC LIMIT 1";

		//unserialize serialized fields. And get last invoice.
		for ($i = 0; $i < count($result); $i++) {

            $res = $this->_dbConnection->prepare($sql);
            $res->execute(array($result[$i]['id']));

            $res = $res->fetch(PDO::FETCH_OBJ);

            $res2 = $this->_dbConnection->prepare($sql2);
            $res2->execute(array($result[$i]['id']));

            $res2 = $res2->fetch(PDO::FETCH_OBJ);

            if($res->is_offline==1)
                $res->is_paid = 4;

            if($res->is_pending==1)
                $res->is_paid = 2;

            if($res->is_declined==1)
                $res->is_paid = 3;

            $frequency = $result[$i]['frequency'];
            $timechange = "";
	        switch($frequency)
	        {
	            case "daily":{
	                $timechange = "+1 day";
	                break;
	            }
	            case "weekly":{
	                $timechange = "+1 week";
	                break;
	            }
	            case "monthly":{
	                $timechange = "+1 month";
	                break;
	            }
	            case "bimonthly":{
	                $timechange = "+2 months";
	                break;
	            }
	            case "yearly":{
	                $timechange = "+1 year";
	                break;
	            }
	        }

	        if ($result[$i]['lead_gen_delay_period'] == 0)
                $extra_days = 7;
            elseif (empty($result[$i]['lead_gen_delay_period']))
                $extra_days = 1;
            else
                $extra_days = $result[$i]['lead_gen_delay_period'];

	        $next_date = new DateTime($res->to_date, new DateTimeZone("UTC"));
	        $next_date->modify($timechange);
	        $next_date_1 = strtotime($next_date->format("Y-m-d H:i:s"));
	        $next_date->modify("+".$extra_days." day");
			$next_date_2 = strtotime($next_date->format("Y-m-d H:i:s"));

			$today_date = new DateTime(date("Y-m-d H:i:s"), new DateTimeZone("UTC"));
			$today_date = strtotime($today_date->format("Y-m-d H:i:s"));

            if($res->client_id != "")
            {
                $result[$i]["last_invoice"] = array(
                    "last_invoice_date"=>$res2->date,
                    "last_invoice_id"=>$res->id,
                    "last_invoice_status"=>$res->is_paid,
                    "last_invoice_from"=>date("Y-m-d", strtotime($res->from_date)),
                    "last_invoice_to"=>date("Y-m-d", strtotime($res->to_date))
                );
            }

			if ($result[$i]["outgoing_numbers"] != "") {
				
				$result[$i]["outgoing_numbers"] = unserialize($result[$i]["outgoing_numbers"]);
			}
			
			if ($result[$i]["lead_gen_bill_by_phone_codes"] != "") {
				
				$result[$i]["lead_gen_bill_by_phone_codes"] = unserialize($result[$i]["lead_gen_bill_by_phone_codes"]);
			}
			
			if ($result[$i]["lead_gen_bill_by_email_tags"] != "") {
				
				$result[$i]["lead_gen_bill_by_email_tags"] = json_decode($result[$i]["lead_gen_bill_by_email_tags"]);
			}

            $start_date = new DateTime($result[$i]["startdate"],new DateTimeZone("UTC"));
            $timechange = "";
            switch($result[$i]['frequency'])
            {
                case "daily":{
                    $timechange = "-1 day";
                    break;
                }
                case "weekly":{
                    $timechange = "-1 week";
                    break;
                }
                case "monthly":{
                    $timechange = "-1 month";
                    break;
                }
                case "bimonthly":{
                    $timechange = "-2 months";
                    break;
                }
                case "yearly":{
                    $timechange = "-1 year";
                    break;
                }
            }
            $start_date->modify($timechange);
            $result[$i]['startdate'] = $start_date->format("Y-m-d h:i:s");
		}
		
		return ($result);
	}
	
	/**
	 * Add client.
	 */
	public function addClient($data) {
		
		//the sql statement.
		$sql = "insert into clients (
			company_id, outgoing_numbers, bill_for, lead_gen_bill_for, lead_gen_bill_by_phone_codes,
			lead_gen_free_leads_number, lead_gen_delay_period, lead_gen_delay_email, lead_gen_amount_per_qualified_lead, lead_gen_amount_per_qualified_lead_currency, 
			call_tracking_flat_rate_per_month, call_tracking_per_number_fee, call_tracking_per_minute_fee, call_tracking_currency,
			user_to_send_bill_to, frequency, bill_not_paid_past_due_days, startdate, invoice_email_template,
			client_name, address, city, state, zip_code
			)
			values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
		";
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute(array(
			$data["company_id"],
			$data["outgoing_numbers"],
			$data["bill_for"],
			$data["lead_gen_bill_for"],
			$data["lead_gen_bill_by_phone_codes"],
			$data["lead_gen_free_leads_number"],
			$data["lead_gen_delay_period"],
			$data["lead_gen_delay_email"],
			$data["lead_gen_amount_per_qualified_lead"],
			$data["lead_gen_amount_per_qualified_lead_currency"],
			$data["call_tracking_flat_rate_per_month"],
			$data["call_tracking_per_number_fee"],
			$data["call_tracking_per_minute_fee"],
			$data["call_tracking_currency"],
			$data["user_to_send_bill_to"],
			$data["frequency"],
			$data["bill_not_paid_past_due_days"],
			$data["startdate"],
			$data["invoice_email_template"],
			$data["client_name"],
			$data["address"],
			$data["city"],
			$data["state"],
			$data["zip_code"]
		));
		
		return ($this->_dbConnection->lastInsertId());
	}
	
	/**
	 * Edit client info.
	 */
	public function editClient($data) {
		
		//the sql statement.
		$sql = "update clients set
			company_id = ?, outgoing_numbers = ?, bill_for = ?, lead_gen_bill_for = ?, lead_gen_bill_by_phone_codes = ?,
			lead_gen_free_leads_number = ?, lead_gen_delay_period = ?, lead_gen_delay_email = ?, lead_gen_amount_per_qualified_lead = ?, lead_gen_amount_per_qualified_lead_currency = ?,
			call_tracking_flat_rate_per_month = ?, call_tracking_per_number_fee = ?, call_tracking_per_minute_fee = ?, call_tracking_currency = ?,
			user_to_send_bill_to = ?, frequency = ?, bill_not_paid_past_due_days = ?, startdate = ?, invoice_email_template = ?,
			client_name = ?, address = ?, city = ?, state = ?, zip_code = ?
			where id = ?
		";
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute(array(
			$data["company_id"],
			$data["outgoing_numbers"],
			$data["bill_for"],
			$data["lead_gen_bill_for"],
			$data["lead_gen_bill_by_phone_codes"],
			$data["lead_gen_free_leads_number"],
			$data["lead_gen_delay_period"],
			$data["lead_gen_delay_email"],
			$data["lead_gen_amount_per_qualified_lead"],
			$data["lead_gen_amount_per_qualified_lead_currency"],
			$data["call_tracking_flat_rate_per_month"],
			$data["call_tracking_per_number_fee"],
			$data["call_tracking_per_minute_fee"],
			$data["call_tracking_currency"],
			$data["user_to_send_bill_to"],
			$data["frequency"],
			$data["bill_not_paid_past_due_days"],
			$data["startdate"],
			$data["invoice_email_template"],
			$data["client_name"],
			$data["address"],
			$data["city"],
			$data["state"],
			$data["zip_code"],
			$data["client_id"]
		));
	}
	
	/**
	 * Update client cronid.
	 */
	public function updateClientCronId($clientId, $cronId) {
		
		//the sql statement.
		$sql = "update clients set
			cron_id = ?
			where id = ?
		";
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute(array(
			$cronId,
			$clientId
		));
	}

    public function updateClientStartDate($client_id, $date){
        $sql = "UPDATE clients SET `startdate`= ? WHERE `id` = ?";

        $result = $this->_dbConnection->prepare($sql);
        $result->execute(array(
            $date,
            $client_id
        ));
    }
	
	/**
	 * Delete client.
	 */
	public function deleteClient($data) {
		
		//the sql statement.
		$sql = "delete from clients where id = ?";
		
		//prepare the sql statement.
		$result = $this->_dbConnection->prepare($sql);
		
		//execute the sql statement.
		$result->execute(array($data["client_id"]));
	}
	
	
}

?>