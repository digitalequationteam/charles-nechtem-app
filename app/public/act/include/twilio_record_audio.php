<?php
header('Content-type: text/xml');
require 'twilio_header.php';
echo '<?xml version="1.0" encoding="UTF-8" ?>';

$url = dirname(s8_get_current_webpage_uri());
$url = str_replace("/include", "", $url);
?>
<Response>
    <Say>Hello. Record your message after the beep. Press the star key when finished.</Say>
    <Record timeout="10" action="<?php echo $url; ?>/record_audio.php?wId=<?php echo $_REQUEST['wId']; ?>&amp;companyId=<?php echo $_REQUEST['companyId']; ?>&amp;recordingId=<?php echo $_REQUEST['recordingId']; ?>" finishOnKey="*" />
</Response>