<?php
header('Content-type: text/xml');
require_once 'util.php';

$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : "default";

switch ($action) {
	case "default":
		?>
		<Response>
		    <Dial method="post" action="<?php echo dirname(s8_get_current_webpage_uri()) . '/twilio_status_callback.php?userid=' . @$_REQUEST['userid'] . '&amp;app=' . @$_REQUEST['app']; ?>" record="true" callerId="<?php echo htmlspecialchars($_REQUEST["Fromcall"]); ?>"><?php echo htmlspecialchars($_REQUEST["tocall"]); ?></Dial>
		</Response>
		<?php
	break;

	case "answer_incoming_call":
		require_once('config.php');
		
		$conference_name = trim($_REQUEST['conference_name']);
		$change_caller_twiml = true;

		if ($_REQUEST['caller_is_agent'] != 1) {
			$sth = $db->customExecute("DELETE FROM call_conferences WHERE name = ?");
			$sth->execute(array($conference_name));
		}

		$sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
		$sth->execute(array($conference_name));
		$conference = $sth->fetch();

		if ($conference['answered'] == 1) $change_caller_twiml = false;

		if ($_REQUEST['caller_is_agent'] == 1) {
			$participants = json_decode($conference['participants'], true);

			foreach ($participants as $key => $participant) {
				if (isset($_REQUEST['phone_number']) && $participant["user_id"] == $_REQUEST['user_id']) {
					$participants[$key] = array(
						"type" => "phone_number",
						"user_id" => 0,
						"handle" => $_REQUEST['phone_number'],
						"call_sid" => $_REQUEST['CallSid'],
						"is_host" => false
					);
				}
				elseif (isset($_REQUEST['user_id']) && $participant["user_id"] == $_REQUEST['user_id']) {
					$participants[$key] = array(
						"type" => "agent",
						"user_id" => $_REQUEST['user_id'],
						"handle" => str_replace("RRRRaandddd", "Rand", $_REQUEST['handle']),
						"call_sid" => $_REQUEST['CallSid'],
						"is_host" => false
					);
				}
				else {
					$participants[$key]['dont_show'] = 0;
				}
			}

			$sth = $db->customExecute("UPDATE call_conferences set participants = ?, answered = '1' WHERE id = ?");
			$sth->execute(array(json_encode($participants), $conference['id']));
		}
		else {
			$call_participants = array();
			$participants = array(
				array(
					"type" => "agent",
					"user_id" => $_REQUEST['user_id'],
					"handle" => str_replace("RRRRaandddd", "Rand", $_REQUEST['handle']),
					"call_sid" => $_REQUEST['CallSid'],
					"is_host" => true
				)
			);
			$call_participants[] = str_replace("RRRRaandddd", "Rand", $_REQUEST['handle']);

			$from = $_REQUEST['Fromcall'];
			$contact = $db->getContactByPhoneNumber($_REQUEST['From']);
			if ($contact) {
	            $from = trim(Util::escapeString($contact['first_name']." ".$contact['last_name']));
	            if (empty($from)) $from = trim(Util::escapeString($contact['business_name']));
	        }
			$call_participants[] = $from;

			$sth = $db->customExecute("INSERT INTO call_conferences(`from`, `to`, `name`, `participants`, `answered`) VALUES(?,?,?,?,?)");
			$sth->execute(array($from, $_REQUEST['tocall'], $conference_name, json_encode($participants), 1));

			$sth = $db->customExecute("UPDATE calls SET participants = ? WHERE CallSid = ?");
			$sth->execute(array(json_encode($call_participants), $_REQUEST['call_sid']));
		}

    	try {
    		if ($change_caller_twiml) {
	    		$client = new Services_Twilio($AccountSid, $AuthToken);
			    $call = $client->account->calls->get($_REQUEST['call_sid']);

			    $exploded = explode("include", dirname(s8_get_current_webpage_uri()));
			    $call->update(array(
			        "Url" => $exploded[0].'ivrmenu.php?company_id=' . @$_REQUEST['company_id'] . '&action=activate_answer&conference_name='.$conference_name.'&caller_is_agent='.$_REQUEST['caller_is_agent'],
			        "Method" => "POST"
			    ));
		    }
		}
		catch (Exception $e) {
			mail("deejayundoo@gmail.com", "error", http_build_query($e));
		}

		$sth = $db->customExecute("DELETE FROM calls_missed WHERE call_sid = ?");
    	$sth->execute(array($_REQUEST['call_sid']));

		$sth = $db->customExecute("DELETE FROM calls_missed WHERE call_sid = ?");
    	$sth->execute(array($_REQUEST['CallSid']));

		$url = $exploded[0]."record_call.php?company_id=".@$_REQUEST['company_id']."&amp;exit_from_conference=1&amp;conference_name=".$conference_name;
		?>
			<Response><Dial record="true" action="<?php echo $url; ?>"><Conference record="true" beep="false"><?php echo $conference_name; ?></Conference></Dial></Response>
		<?php
	break;

	case "call_another_agent":
		require_once('config.php');

		$conference_name = trim($_REQUEST['conference_name']);
		$sth = $db->customExecute("DELETE FROM call_conferences WHERE name = ?");
		$sth->execute(array($conference_name));

		$participants = array(
			array(
				"type" => "agent",
				"user_id" => $_REQUEST['user_id'],
				"handle" => str_replace("RRRRaandddd", "Rand", $_REQUEST['handle']),
				"call_sid" => $_REQUEST['CallSid'],
				"is_host" => true
			),
			array(
				"type" => "agent",
				"user_id" => $_REQUEST['to_user_id'],
				"handle" => $_REQUEST['to_user_name'],
				"call_sid" => "",
				"is_host" => false,
				"pending" => 1,
				"dont_show" => 1,
				"time_added" => strtotime("now")
			)
		);

		$from = str_replace("RRRRaandddd", "Rand", $_REQUEST['handle']);

		$sth = $db->customExecute("INSERT INTO call_conferences(`from`, `to`, `name`, `participants`) VALUES(?,?,?,?)");
		$sth->execute(array($from, $_REQUEST['to_user_name'], $conference_name, json_encode($participants)));

		$company_id = trim($_REQUEST['company_id']);
		$sth = $db->customExecute("SELECT * FROM call_ivr_widget WHERE companyId = ? AND flowtype = 'AgentIVR'");
		$sth->execute(array($company_id));
		$widget = $sth->fetch();
		$content = json_decode($widget['content'], true);

		$exploded = explode("include", dirname(s8_get_current_webpage_uri()));

		$sth = $db->customExecute("UPDATE users SET last_call_sid = ? WHERE idx = ?");
		$sth->execute(array($_REQUEST['CallSid'], $_REQUEST['user_id']));
		?>
		<Response><Redirect><?php echo $exploded[0].'ivrmenu.php?action=default&amp;company_id='.$company_id.'&amp;wId='. $widget['wId'].'&amp;tcx='.strtotime('now'); ?>&amp;forced_agent_id=<?php echo $_REQUEST['to_user_id']; ?></Redirect></Response>
		<?php
	break;

	case "call_another_number":
		require_once('config.php');

		$conference_name = trim($_REQUEST['conference_name']);
		$sth = $db->customExecute("DELETE FROM call_conferences WHERE name = ?");
		$sth->execute(array($conference_name));

		$user_id = rand(1, 99999999);

		$_REQUEST['tocall'] = str_replace("(", "", $_REQUEST['tocall']);
		$_REQUEST['tocall'] = str_replace(")", "", $_REQUEST['tocall']);
		$_REQUEST['tocall'] = str_replace(" ", "", $_REQUEST['tocall']);
		$_REQUEST['tocall'] = str_replace("-", "", $_REQUEST['tocall']);

		$participants = array(
			array(
				"type" => "agent",
				"user_id" => $_REQUEST['user_id'],
				"handle" => str_replace("RRRRaandddd", "Rand", $_REQUEST['handle']),
				"call_sid" => $_REQUEST['CallSid'],
				"is_host" => true
			),
		    array(
		        "type" => "phone_number",
		        "user_id" => $user_id,
		        "handle" => $_REQUEST['phone_number'],
		        "call_sid" => "",
		        "is_host" => false,
		        "pending" => 1
		    )
		);

		$from = $_REQUEST['Fromcall'];

		$sth = $db->customExecute("INSERT INTO call_conferences(`from`, `to`, `name`, `participants`) VALUES(?,?,?,?)");
		$sth->execute(array($from, $_REQUEST['tocall'], $conference_name, json_encode($participants)));

		$company_id = trim($_REQUEST['company_id']);

		$exploded = explode("include", dirname(s8_get_current_webpage_uri()));

		$action_url = $exploded[0] . "/include/twilio_dial_number.php?company_id=".$_REQUEST['company_id']."&amp;action=answer_incoming_call&amp;conference_name=".$_REQUEST['conference_name']."&amp;caller_is_agent=1&amp;phone_number=".$_REQUEST["tocall"]."&amp;user_id=".$user_id."&amp;call_sid=".$_REQUEST['CallSid'];

		$url = $exploded[0]."record_call.php?company_id=".@$_REQUEST['company_id']."&amp;exit_from_conference=1&amp;conference_name=".$conference_name;
		
		$ringTone = Util::getRingTone();
		if ($ringTone == "auto")
			$ringTone = $_REQUEST['outgoing_country'];

		if ($ringTone == "GB") $ringTone = "UK";
		?>
			<Response>
				<Dial method="post" action="<?php echo dirname(s8_get_current_webpage_uri()) . '/twilio_status_callback.php?app=' . @$_REQUEST['app']; ?>&amp;userid=<?php echo @$_REQUEST['userid']; ?>&amp;phone_number=<?php echo $_REQUEST['tocall']; ?>" record="true" callerId="<?php echo htmlspecialchars($_REQUEST["Fromcall"]); ?>" ringTone="<?php echo $ringTone; ?>">
					<Number method="post" action="<?php echo dirname(s8_get_current_webpage_uri()) . '/twilio_status_callback.php?app=' . @$_REQUEST['app']; ?>&amp;conference_name=<?php echo $conference_name; ?>&amp;user_id=<?php echo $user_id; ?>&amp;phone_number=<?php echo $_REQUEST['tocall']; ?>"><?php echo trim(htmlspecialchars($_REQUEST["tocall"])); ?></Number>
				</Dial>
			</Response>
		<?php

	break;

	case "rejoin_ongoing_call":
		require_once('config.php');

		$conference_name = trim($_REQUEST['conference_name']);
		$user_id = trim($_REQUEST['user_id']);
		$handle = trim(str_replace("RRRRaandddd", "Rand", $_REQUEST['handle']));
		$company_id = trim($_REQUEST['company_id']);
		$call_data_i_am_host = trim($_REQUEST['call_data_i_am_host']);
		
		$sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
		$sth->execute(array($conference_name));
		$conference = $sth->fetch();

		$new_participants = array();
		$participants = json_decode($conference['participants'], true);

		foreach ($participants as $participant) {
			if ($participant['user_id'] == $user_id) {
				$participant['on_hold'] = 0;
				$participant['call_sid'] = $_REQUEST['CallSid'];
			}
			else {
				if ($call_data_i_am_host == '1') $participant['bringing_them_back'] = 1;
			}
			if ($call_data_i_am_host == '1') $participant['forced_hold'] = 0;
			$new_participants[] = $participant;
		}

		$exploded = explode("include", dirname(s8_get_current_webpage_uri()));

		if ($call_data_i_am_host == '1') {
	        $sth = $db->customExecute("UPDATE call_conferences SET on_hold = 0 WHERE id = ?");
	        $sth->execute(array($conference['id']));

	        $client = new Services_Twilio($AccountSid,$AuthToken);
	        foreach ($participants as $participant) {
	            if (@$participant['pending'] != 1 && !empty($participant['call_sid'])) {
	                $call = $client->account->calls->get($participant['call_sid']);

	                try {
		                $call->update(array(
					        "Url" => $exploded[0].'ivrmenu.php?company_id=' . @$_REQUEST['company_id'] . '&action=activate_answer&conference_name='.$conference_name.'&caller_is_agent=1&bringing_back=1&call_sid='.$participant['call_sid'],
					        "Method" => "POST"
					    ));
				    }
				    catch (Exception $e) {}
	            }
	        }
	    }

	    $sth = $db->customExecute("UPDATE call_conferences SET on_hold = 0, participants = ? WHERE id = ?");
	    $sth->execute(array(json_encode($new_participants), $conference['id']));

		$url = $exploded[0]."record_call.php?company_id=".@$_REQUEST['company_id']."&amp;exit_from_conference=1&amp;conference_name=".$conference_name;
		?>
			<Response><Dial record="true" action="<?php echo $url; ?>"><Conference record="true" beep="false" waitUrl=""><?php echo $conference_name; ?></Conference></Dial></Response>
		<?php

	break;
}
?>