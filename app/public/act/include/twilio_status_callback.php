<?php
//Passing thr http 200 ok header to override the 404 error by wp-blog-header.php
header("HTTP/1.1 200 OK");

//Setting the content tyoe of document to XML, So, callers should treat this accordingly
header('Content-type: text/xml');

require_once "config.php";
require_once "util.php";
require_once "db.php";
require_once 'twilio_header.php';

$db = new DB();

if (isset($_REQUEST['conference_name']) && isset($_REQUEST['user_id'])) {
	$conference_name = trim($_REQUEST['conference_name']);
	$change_caller_twiml = true;

	$sth = $db->customExecute("SELECT * FROM call_conferences WHERE name = ?");
	$sth->execute(array($conference_name));
	$conference = $sth->fetch();

	$participants = json_decode($conference['participants'], true);

	foreach ($participants as $key => $participant) {
		if ($participant["user_id"] == $_REQUEST['user_id']) {
			$participants[$key] = array(
				"type" => "phone_number",
				"user_id" => 0,
				"handle" => $_REQUEST['phone_number'],
				"call_sid" => $_REQUEST['CallSid'],
				"is_host" => false
			);
		}
	}

	$sth = $db->customExecute("UPDATE call_conferences set participants = ?, answered = '1' WHERE id = ?");
	$sth->execute(array(json_encode($participants), $conference['id']));
	?>
	<Response/>
	<?php
	exit();
}

//Retrieving the DialCallSid from request params as this will be used 
//for retriving call To and From numbers
//They are not supllied in request params
$DialCallSid = $_REQUEST['DialCallSid'];

Global $AccountSid, $AuthToken;

//Creating a new instance of Twilio API 
$client = new Services_Twilio($AccountSid, $AuthToken);
// Get an object from its sid. If you do not have a sid,
$call = $client->account->calls->get($DialCallSid);

//The fields below are supllied to us empty, therefore, retrieving fields from API response only
//Retrieving the To number
$_REQUEST['To'] = $call->to;

//Retrieving the From number
$_REQUEST['From'] = $call->from;

//Retrieving the Called number
$_REQUEST['Called'] = $call->to;

//Retrieving the Caller number
$_REQUEST['Caller'] = $call->from;


$var = $db->getVar("browserphone_".$_REQUEST['userid']);
if($var!=false){
    $_REQUEST['PhoneCode'] = $var;
    $db->deleteVar("browserphone_".$_REQUEST['userid']);
}

$resp = $db->save_outgoing_call();

//<!--??return the XML response to twilio-->
//<!--They require this to confirm successfull post of data here-->
//$file=fopen("outgoing_log.txt","a+");
//fwrite($file, "[RECV CALL DETAILS]\n");
//fwrite($file,print_r($_REQUEST, true));
//fwrite($file,print_r($resp,true)."\n");
//fwrite($file, "[/RECV CALL DETAILS]\n\n");
//fclose($file);
?>
<Response/>