<?php
global $build_number, $lcl;
$today_ = new DateTime('now');
if ($build_number == "[[build_number]]")
    $build_number = $today_->format("U");

if (!isset($_SESSION)):
    @session_start();
endif;

if (isset($_SESSION['user_id'])) {
    if($_SESSION['permission']<1 && !isset($_SESSION['prems'])){
        $_SESSION['prems']['ad'] = $db->checkAddonAccess(@$_SESSION['user_id'],10006);
        $_SESSION['prems']['vb'] = $db->checkAddonAccess(@$_SESSION['user_id'],10007);
        $_SESSION['prems']['mc'] = $db->checkAddonAccess(@$_SESSION['user_id'],10008);
        $dialer = $db->checkAddonAccess(@$_SESSION['user_id'],10003);
        $_SESSION['prems']['dialer'] = $db->checkAddonAccess(@$_SESSION['user_id'],10003);
        $_SESSION['prems']['outgoing_disabled'] = $db->isUserOutboundLinksDisabled(@$_SESSION['user_id']);
        $_SESSION['prems']['purchase_numbers'] = $db->checkAddonAccess(@$_SESSION['user_id'],10009);
        $_SESSION['prems']['customize_call_flow'] = $db->checkAddonAccess(@$_SESSION['user_id'],10010);
    }elseif($_SESSION['permission']>=1){
        $_SESSION['prems']['ad'] = true;
        $_SESSION['prems']['vb'] = true;
        $_SESSION['prems']['mc'] = true;
        $_SESSION['prems']['dialer'] = true;
        $_SESSION['prems']['outgoing_disabled'] = true;
    }
}

if (isset($_SESSION['user_id']) && isset($_SESSION['sel_co']) && $lcl>=2) {
    if($_SESSION['prems']['dialer'] || $_SESSION['permission'] >= 1) {
        require 'twilio_header.php';
    }
}
?>
<meta name="robots" content="noindex">
<style type="text/css" media="all">
    @import url("css/style.css<?php echo "?" . $build_number; ?>");
    @import url("css/jquery.wysiwyg.css<?php echo "?" . $build_number; ?>");
    @import url("css/facebox.css<?php echo "?" . $build_number; ?>");
    @import url("css/visualize.css<?php echo "?" . $build_number; ?>");
    @import url("css/date_input.css<?php echo "?" . $build_number; ?>");
    @import url("css/Aristo/Aristo.css<?php echo "?" . $build_number; ?>");
    @import url("css/farbtastic.css<?php echo "?" . $build_number; ?>");
    @import url('css/select2.css<?php echo "?".$build_number; ?>');
</style>
<style>
    #newUserDialog { font-size:62% }
    .ui-dialog label, input { display:block !important; font-weight: bold; }
    .ui-dialog p { padding:5px !important; }
    .ui-dialog input.text { margin-bottom:12px !important; width:95% !important; padding: .4em !important; }
    .ui-dialog fieldset { padding:0 !important; border:0 !important; margin-top:25px !important; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em !important; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
    div.jp-audio, div.jp-video { font-size:1.25em; }
    .annotatedtimelinetable td {padding:0 !important;}
    #report_logo { margin-bottom:10px; }
    
    .tablerow-button-icon {
	    margin-right: 10px;
    }
    
    .tablerow-button-icon img {
	    width: 20px;
	    height: 20px;
    }
    
</style>
<link href="player/skin/jplayer-black-and-blue.css<?php echo "?" . $build_number; ?>" rel="stylesheet" type="text/css" />
<link href="css/tooltipster.css<?php echo "?".$build_number; ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    try {
        console
    } catch (e) {
        console = {};
        console.log = function() {
        };
    }
</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="js/jquery.js<?php echo "?" . $build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.tooltipster.min.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/modernizr.custom.83977.js<?php echo "?" . $build_number; ?>"></script>
<script type="text/javascript" src="player/jquery.jplayer.min.js<?php echo "?" . $build_number; ?>"></script>
<script type="text/javascript">google.load("visualization", "1", {packages: ["corechart", "annotatedtimeline"]});</script>
<script type="text/javascript" src="js/farbtastic.js<?php echo "?" . $build_number; ?>"></script>

<script type="text/javascript" src="js/noty-2.1.0/js/noty/jquery.noty.js"></script>
<script type="text/javascript" src="js/noty-2.1.0/js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty-2.1.0/js/noty/layouts/topLeft.js"></script>
<script type="text/javascript" src="js/noty-2.1.0/js/noty/layouts/topRight.js"></script>
<script type="text/javascript" src="js/noty-2.1.0/js/noty/themes/default.js"></script>
<script type="text/javascript" src="js/select2.min.js"></script>

<script type="text/javascript" src="js/PhoneFormat.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/tts.audio.plugin.js<?php echo "?".$build_number; ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
        <?php if (@$page != "dial_number") { ?>
        if(document.location.pathname != "/login.php")
            checkIncomingLiveCalls();
        <?php } ?>

        if(getCookie2("ignoreNotifications")=="" || typeof getCookie2("ignoreNotifications") === "undefined")
            window.ignore_CallSid = [];
        else
            window.ignore_CallSid = getCookie2("ignoreNotifications").split(",");
    });

    function shouldRunOnThisTab() {
        if (typeof(setCookie) == "undefined") return false;

        <?php
        if ($is_popout_dialer) {
            ?>
            var this_time = new Date().getTime();
            setCookie("cicLastRun", this_time, 1);
            setCookie("cicTab", this_tab, 1);
            runIt = true;
            <?php
        }
        else {
        ?>
            var runIt = false;
            if (typeof getCookie2("cicLastRun") === "undefined") {
                var this_time = new Date().getTime();
                setCookie("cicLastRun", this_time, 1);
                setCookie("cicTab", this_tab, 1);
                runIt = true;
            }
            else {
                var this_time = new Date().getTime();
                var last_run_time = parseInt(getCookie2("cicLastRun"));
                if (this_time - last_run_time > 1500) {
                    var this_time = new Date().getTime();
                    setCookie("cicLastRun", this_time, 1);
                    setCookie("cicTab", this_tab, 1);
                    runIt = true;
                }
                else {
                    if (getCookie2("cicTab") == this_tab) {
                        setCookie("cicLastRun", this_time, 1);
                        runIt = true;
                    }
                }
            }
        <?php
        }
        ?>

        return runIt;
    }

    var incXhr;
    var this_tab = <?php echo rand(1111111111, 9999999999); ?>;
    var all_incoming_calls = {};
    var wtXhr;
    function checkIncomingLiveCalls(once) {
        <?php if (!$is_popout_dialer) { ?>
            if ($('.callmaker').length == 0) return false;
        <?php } ?>

        if (!wtXhr) {
            wtXhr = setInterval(function() {
                $("div#client-calls-queue div#client-calls-list .incoming_call").each(function() {
                    var call_sid = $(this).find('.call_data_call_sid').val();
                    var call = all_incoming_calls[call_sid];
                    call.wait_time++;

                    if (call.wait_time < 59)
                        var wait_time = '00:' + ((call.wait_time < 10) ? "0" : "") + call.wait_time.toString();
                    else
                    {
                        var mins = Math.floor(call.wait_time / 60);
                        var new_seconds = call.wait_time - Math.round(mins * 60);
                        var wait_time = ((mins < 10) ? "0" : "") + mins.toString() + ':' + ((call.wait_time < 10) ? "0" : "") + new_seconds.toString();
                    }

                    $(this).find('.wait_time').text(wait_time);
                });

                $("div#client-list-calls-list div#client-calls-list .incoming_call").each(function() {
                    var call_sid = $(this).find('.call_data_call_sid').val();
                    var call = all_incoming_calls[call_sid];

                    if (call.wait_time < 59)
                        var wait_time = '00:' + ((call.wait_time < 10) ? "0" : "") + call.wait_time.toString();
                    else
                    {
                        var mins = Math.floor(call.wait_time / 60);
                        var new_seconds = call.wait_time - Math.round(mins * 60);
                        var wait_time = ((mins < 10) ? "0" : "") + mins.toString() + ':' + ((call.wait_time < 10) ? "0" : "") + new_seconds.toString();
                    }

                    $(this).find('.wait_time').text(wait_time);
                });
            }, 1000);
        }

        if (shouldRunOnThisTab()) {
            var extra_query = "";

            if ($('#client-ui-actions').is(":visible")) {
                extra_query += "&conference_name=" + current_conference["name"];
            }

            function makeTheRequest() {
                return $.get('check_incoming_live_calls.php?1=1<?php if (isset($is_popout_dialer)) { ?>&popout_dialer=1<?php } ?>' + extra_query, function(data) {
                    if(data) {
                        if (typeof(data.session_expired) != "undefined") {
                            $("#dialer").remove();
                            errMsgDialogCB("Your session expired! Please re-login.", function(){window.location = "login.php";});
                            return false;
                        }
                        if(typeof dialerMuteButtonDisplay === 'undefined') return false;

                        <?php if (!isset($is_popout_dialer) && isset($_SESSION['sel_co'])) { ?>
                            if (data.popout_dialer_time_diff <= 5)
                                disableSlideOutDialer();
                            else
                                enableSlideOutDialer();
                        <?php } ?>

                        if ($('#client-ui-actions').length > 0 && !$('#client-ui-actions').is(":visible")) dialerMuteButtonDisplay(data.browsersoftphone_mute_btn_display);

                        if ($('#client-ui-actions').is(":visible") && data.conference) updateConferenceDetails(data.conference);

                        if (data.my_status && !shouldRunOnThisTab()) $("#user-status").val(data.my_status);

                        $("div#client-calls-list").html('');
                        if (data.user_queue.length > 0 && (data.my_status == "ONLINE" || data.my_status == "BUSY" || data.my_status == "AWAY")) {
                            if ($('.callmaker').hasClass('disabled')) {
                                $("#client-calls-queue").hide();
                            }
                            else {
                                if ($("#divstatus").val() == "") {
                                    $("#client-calls-queue").show();
                                }
                                else {
                                    $("#client-calls-queue").hide();
                                }
                            }

                            for (var i in data.user_queue) {
                                var call = data.user_queue[i];
                                if (call.call_sid == received_call_sid) continue;

                                if (typeof(all_incoming_calls[call.call_sid]) == "undefined") {
                                    all_incoming_calls[call.call_sid] = call;
                                }
                                else {
                                    call = all_incoming_calls[call.call_sid];
                                }

                                if (call.wait_time < 59)
                                    var wait_time = '00:' + ((call.wait_time < 10) ? "0" : "") + call.wait_time.toString();
                                else
                                {
                                    var mins = Math.floor(call.wait_time / 60);
                                    var new_seconds = call.wait_time - Math.round(mins * 60);
                                    var wait_time = ((mins < 10) ? "0" : "") + mins.toString() + ':' + ((call.wait_time < 10) ? "0" : "") + new_seconds.toString();
                                }

                                var call_from_display = (call.Name == call.From) ? formatNumber(call.From) : call.Name;

                                if (typeof(call.incoming_call) == "undefined") call.incoming_call = 0;
                                if (typeof(call.ContactName) != "undefined") {
                                    var main_call_from_display = call.ContactName;
                                }
                                else {
                                    var main_call_from_display = call_from_display;
                                }

                                if (typeof(call.Department) != "undefined") {
                                    call_from_display = call_from_display + ' (' + call.Department + ')';
                                }

                                var html = '<div class="incoming_call call_' + call.call_sid + '">' + 
                                        '<input type="hidden" class="call_data_call_sid" value="' + call.call_sid + '" />' +
                                        '<input type="hidden" class="call_data_user_id" value="' + call.user_id + '" />' +
                                        '<input type="hidden" class="call_data_type" value="' + call["type"] + '" />' +
                                        '<input type="hidden" class="call_data_name" value="' + call.Name + '" />' +
                                        '<input type="hidden" class="call_data_from" value="' + call.From + '" />' +  
                                        '<input type="hidden" class="call_data_to" value="' + call.To + '" />' + 
                                        '<input type="hidden" class="call_data_display" value="' + main_call_from_display + '" />' + 
                                        '<input type="hidden" class="call_data_incoming" value="' + call.incoming_call + '" />' + 
                                        '<div class="call_action_buttons">'

                                if ($('#client-ui-actions').is(":visible")) {
                                    html += '<a href="javascript: void(0);"><img src="images/icons/merge.png" onclick="answerIncomingCall(\'' + call.call_sid + '\');" style="width: 16px; margin-right: 5px;" /></a>';
                                }
                                else {
                                    html += '<a href="javascript: void(0);"><img src="images/icons/answer.png" onclick="answerIncomingCall(\'' + call.call_sid + '\');" style="width: 16px; margin-right: 5px;" /></a>';
                                }

                                html +=     '<a href="javascript: void(0);"><img src="images/icons/decline.png" onclick="declineIncomingCall(\'' + call.call_sid + '\');" style="width: 16px;" /></a>' +
                                        '</div>' +
                                        '<strong>' + call_from_display + '</strong> (<span class="wait_time">' + wait_time + '</span>)' + 
                                        '<div style="clear: both;"></div>' +
                                    '</div>';

                                $("div#client-calls-list").append(html);
                            }

                            if ($("div#client-calls-list").html() == "") $("div#client-calls-list").html('No calls.');
                        }
                        else {
                            $("#client-calls-queue").hide();
                            $("div#client-calls-list").html('No calls.');
                        }

                        if (data.user_ongoing_queue.length > 0 && (data.my_status == "ONLINE" || data.my_status == "BUSY")) {
                            $("#client-ongoing-wrapper").show();
                            $("div#client-calls-ongoing-list").html('');

                            for (var i in data.user_ongoing_queue) {
                                var call = data.user_ongoing_queue[i];

                                var call_from_display = call.Name;

                                if (call_from_display.length > 45) call_from_display = call_from_display.substr(0, 45) + '...';

                                var html = '<div class="incoming_call call_' + call.conference_name + '">' + 
                                        '<input type="hidden" class="call_data_i_am_host" value="' + call.i_am_host + '" />' +
                                        '<div class="call_action_buttons">';

                                if ($('#client-ui-actions').is(":visible")) {
                                    html += '<a href="javascript: void(0);"><img class="tooltipster" title="Merge Call" src="images/icons/merge.png" onclick="rejoinOngoingCall(\'' + call.conference_name + '\');" style="width: 16px; margin-right: 5px;" /></a>';
                                }
                                else {
                                    html += '<a href="javascript: void(0);"><img class="tooltipster" title="Resume Call" src="images/icons/answer.png" onclick="rejoinOngoingCall(\'' + call.conference_name + '\');" style="width: 16px; margin-right: 5px;" /></a>';
                                }

                                html +=     '</div>' +
                                        '<strong>' + call_from_display + '</strong>' + 
                                        '<div style="clear: both;"></div>' +
                                    '</div>';

                                $("div#client-calls-ongoing-list").append(html);
                                $(".tooltipster").tooltipster();
                            }

                            if ($("div#client-calls-ongoing-list").html() == "") $("div#client-calls-ongoing-list").html('No calls.');
                        }
                        else {
                            $("#client-ongoing-wrapper").hide();
                            $("div#client-calls-ongoing-list").html('No calls.');
                        }



                        if (data.missed_calls.length > 0) {
                            $("#client-missed-wrapper").show();
                            $("div#client-missed-calls-list").html('');

                            for (var i in data.missed_calls) {
                                var call = data.missed_calls[i];

                                var call_from_display = call.from;

                                if (call_from_display.length > 45) call_from_display = call_from_display.substr(0, 45) + '...';

                                var html = '<div class="incoming_call call_' + call.call_sid + '">' + 
                                        '<input type="hidden" class="call_data_type" value="' + call.from_type + '" />' +
                                        '<input type="hidden" class="call_data_from_number" value="' + call.from_number + '" />' +
                                        '<input type="hidden" class="call_data_from" value="' + call.from + '" />' +
                                        '<div class="call_action_buttons">' +
                                            '<a href="javascript: void(0);"><img src="images/icons/answer.png" onclick="callBackMissedCall(\'' + call.call_sid + '\'); " style="width: 16px; margin-right: 5px;" /></a>' +
                                            '<a href="javascript: void(0);"><img src="images/delete.gif" onclick="deleteMisedCall(\'' + call.call_sid + '\');" style="width: 16px;" /></a>' +
                                        '</div>' +
                                        '<strong>' + call_from_display + '</strong> <span style="font-size: 11px;">(' + call.timestamp + ')</span>' + 
                                        '<div style="clear: both;"></div>' +
                                    '</div>';

                                $("div#client-missed-calls-list").append(html);
                            }

                            if ($("div#client-missed-calls-list").html() == "") $("div#client-missed-calls-list").html('No calls.');
                        }
                        else {
                            $("#client-missed-wrapper").hide();
                            $("div#client-missed-calls-list").html('No calls.');
                        }

                        if (data.online_agents.length > 0) {
                            $("div#client-agents-list").html('');

                            for (var i in data.online_agents) {
                                var agent = data.online_agents[i];

                                var agent_status = agent.status;
                                if (agent_status == "ONLINE") agent_status = '<span style="color: #71dd47;">' + agent_status + '</span>';
                                else if (agent_status == "BUSY") agent_status = '<span style="color: #dc3912;">' + agent_status + '</span>';
                                else if (agent_status == "AWAY") agent_status = '<span style="color: #f7af62;">' + agent_status + '</span>';

                                $("div#client-agents-list").append('<div class="incoming_call agent_' + agent.user_id + '">' + 
                                        '<div style="float: right; font-weight: bold;">' + agent_status + '</div>' +
                                        '<div class="call_action_buttons">' +
                                            '<a href="javascript: void(0);"><img src="images/icons/answer.png" onclick="callAnotherAgent(\'' + agent.user_id + '\', \'' + encodeURIComponent(agent["name"]) + '\');" style="width: 16px; margin-right: 5px;" /></a>' +
                                        '</div>' +
                                        '<strong>' + agent.name +'</strong>' +
                                        '<div style="clear: both;"></div>' +
                                    '</div>');
                            }
                        }
                        else {
                            $("div#client-agents-list").html('No agents are online at this time.');
                        }

                        $(".noty_bar").parents("li").remove();
                            if (data.calls.length > 0) {
                                for (var i in data.calls) {
                                    var this_call = data.calls[i];
                                    if($.inArray(this_call.CallSid,window.ignore_CallSid)==-1){
                                        var notynote = noty({
                                            layout: 'topRight',
                                            text: 'Incoming call from <strong>' + this_call.CallFrom + ' to ' + this_call.CompanyName + '</strong>',
                                            type: 'success',
                                            animation: {
                                                open: {height: 'toggle'},
                                                close: {height: 'toggle'},
                                                easing: 'swing',
                                                speed: 0
                                            },
                                            callback: {
                                                onCloseClick: function(e){
                                                    window.ignore_CallSid.push(this.CallSid);
                                                    setCookie("ignoreNotifications",window.ignore_CallSid,1);
                                                }
                                            }
                                        });
                                        notynote.CallSid = this_call.CallSid;
                                    }
                                }
                            }
                    }
                    
                    if (typeof(once) == "undefined") {
                        shouldRunOnThisTab();
                        setTimeout(checkIncomingLiveCalls, 5000);
                    }
                }, "json");
            }

            if (typeof(once) == "undefined") {
                incXhr = makeTheRequest();
            }
            else {
                makeTheRequest();
            }
        }
        else {
            setTimeout(checkIncomingLiveCalls, 5000);
        }
    }
</script>

<?php

global $page;
if (isset($_SESSION['user_id']) && isset($_SESSION['sel_co']) && $lcl>=2 && $page != 'ad_auto_dialer' && $page != 'ad_ad_dialing_details' && $page != 'ad_contactlist') {
    if($_SESSION['prems']['dialer'] || $_SESSION['permission'] >= 1) {
        if(!isset($_SESSION['sel_co_pc']['id']) || $_SESSION['sel_co'] != $_SESSION['sel_co_pc']['id']){
            $_SESSION['sel_co_pc']['id'] = $_SESSION['sel_co'];
            $_SESSION['sel_co_pc']['codes'] = $db->getPhoneCodes($_SESSION['sel_co'],2);
        }

        require_once('dialer_actions.php');
    }
}
?>