<?php
//Passing the 200 Success message
header("HTTP/1.1 200 OK");

//Passing the content type of current document to treat this accordingly
header("Content-type: text/xml");

//Loading the neccessary files
require_once '../util.php';
require_once '../Pagination.php';
require_once '../twilio_header.php';

//Including the library Auto Dialer and Voice Broadcast files
require_once 'lib/ad_lib_funcs.php';

//Initializing the DB object
$db = new DB();

$url = dirname(s8_get_current_webpage_uri());
$url = str_replace("/include/ad_auto_dialer_files", "", $url);

//if cmp_idx param is set in request url
if (isset($_REQUEST['cmp_idx'])):

    //Retrieving the unique ID of campaign
    $campaign_idx = $_REQUEST['cmp_idx'];
    //$campaign_type = $_REQUEST['cmp_type'];

    //Retrieving the campaign details from system
    $campaign_details = ad_vb_get_campaign_data($campaign_idx);

    //Retrieving the contact list details from system
    $list_details = $db->getContactListById($campaign_details['list_id']);

    if ($list_details['vb_opt_out_type'] == "Text") {
        $opt_out_type = "text";
        $opt_out_text_to_say = $list_details['vb_opt_out_content'];
        $opt_out_text_voice = $list_details['vb_opt_out_voice'];
        $opt_out_text_language = $list_details['vb_opt_out_language'];
        $opt_out_text_language = str_replace("|W", "", $opt_out_text_language);
        $opt_out_text_language = str_replace("|M", "", $opt_out_text_language);
    }
    else {
        $opt_out_type = "audio";
        $opt_out_audio_to_play = $list_details['vb_opt_out_content'];

        if (substr($opt_out_audio_to_play, 0, 4) != "http")
            $opt_out_audio_to_play = $url."/audio/opt_out_files/".$campaign_details['user']."/".urlencode($opt_out_audio_to_play);
    }

    $global_opt_out_type = ($db->getVar("global_opt_out_type")) ? $db->getVar("global_opt_out_type") : "Text";
    $global_opt_out_key = ($db->getVar("global_opt_out_key")) ? $db->getVar("global_opt_out_key") : "1";
    if ($list_details['vb_opt_out_type'] == "Text") {
        $global_opt_out_type = "text";
        $global_opt_out_text_to_say = ($db->getVar("global_opt_out_content")) ? $db->getVar("global_opt_out_content") : "Press 1 to opt-out from all our contact lists.";
        $global_opt_out_text_voice = ($db->getVar("global_opt_out_voice")) ? $db->getVar("global_opt_out_voice") : "man";
        $global_opt_out_text_language = ($db->getVar("global_opt_out_language")) ? $db->getVar("global_opt_out_language") : "en";
        $global_opt_out_text_language = str_replace("|W", "", $global_opt_out_text_language);
        $global_opt_out_text_language = str_replace("|M", "", $global_opt_out_text_language);
    }
    else {
        $global_opt_out_type = "audio";
        $global_opt_out_audio_to_play = $db->getVar("global_opt_out_content");

        if (substr($global_opt_out_audio_to_play, 0, 4) != "http")
            $global_opt_out_audio_to_play = $url."/audio/global_opt_out_files/".urlencode($global_opt_out_audio_to_play);
    }

    //CHCK IF OPTOUT_WAS_SENT
    if (isset($_REQUEST['opt_out'])) {
        ?>
        <Response>
        <?php
        if ($_REQUEST['Digits'] == $list_details['vb_opt_out_key']) {
            $opt_out_number = $db->format_phone_db($_REQUEST['to']);
            $db->customQuery("REPLACE INTO opt_out(list_id, number_opted_out) VALUES('".$list_details['idx']."', '".$opt_out_number."')");
            ?>
                <Say voice="<?php echo $opt_out_text_voice; ?>" language="<?php echo $opt_out_text_language; ?>">You have opted out. Good bye.</Say>
            <?php
        }
        elseif ($_REQUEST['Digits'] == $global_opt_out_key) {
            $opt_out_number = $db->format_phone_db($_REQUEST['to']);
            $db->customQuery("UPDATE ad_advb_cl_contacts SET opt_out = '1' WHERE phone_number = '".$opt_out_number."'");
            ?>
                <Say voice="<?php echo $global_opt_out_text_voice; ?>" language="<?php echo $global_opt_out_text_language; ?>">You have opted out from all contacts list. Good bye.</Say>
            <?php
        }
        else {
            ?>
                <Say>Invalid digit entered.</Say>
            <?php
        }
        ?>
        </Response>
        <?php
        exit();
    }
    //CHCK IF OPTOUT_WAS_SENT

    //CHECK IF PRESSED KEY TO DIAL PHONE NUMBER
    if (isset($_REQUEST['go_to_ivr'])) {
        if ($_REQUEST['Digits'] == $campaign_details['ivr_key']) {
            ?>
                <Response><Dial><?php echo $campaign_details['ivr_phone_number']; ?></Dial></Response>
            <?php
        }
        else {
            ?>
                <Response><Say>Invalid digit entered.</Say></Response>
            <?php
        }
        exit();
    }
    //CHECK IF PRESSED KEY TO DIAL PHONE NUMBER

    if($campaign_details['type']=="AutoResponder"){
        $stmt = $db->customExecute("SELECT * FROM ad_vb_sequences WHERE id = ?");
        $stmt->execute(array($_GET['seq_id']));
        $seq_data = $stmt->fetch(PDO::FETCH_OBJ);

        if ($seq_data->live_answer_type == "Text") {
            $live_answer_type = "text";
            $live_answer_text_to_say = $seq_data->live_answer_content;
            $live_answer_text_voice = $seq_data->live_answer_voice;
            $live_answer_text_language = $seq_data->live_answer_language;
            $live_answer_text_language = str_replace("|W", "", $live_answer_text_language);
            $live_answer_text_language = str_replace("|M", "", $live_answer_text_language);
        }
        else {
            $live_answer_type = "audio";
            $live_answer_audio_to_play = $seq_data->live_answer_content;

            if (substr($live_answer_audio_to_play, 0, 4) != "http")
                $live_answer_audio_to_play = $url."/audio/voice_broadcast_files/".$campaign_details['user']."/".urlencode($live_answer_audio_to_play);
        }

        if ($seq_data->voicemail_message_type == "Text") {
            $voicemail_message_type = "text";
            $voicemail_message_text_to_say = $seq_data->voicemail_message_content;
            $voicemail_message_text_voice = $seq_data->voicemail_message_voice;
            $voicemail_message_text_language = $seq_data->voicemail_message_language;
            $voicemail_message_text_language = str_replace("|W", "", $voicemail_message_text_language);
            $voicemail_message_text_language = str_replace("|M", "", $voicemail_message_text_language);
        }
        else {
            $voicemail_message_type = "audio";
            $voicemail_message_audio_to_play = $seq_data->voicemail_message_content;

            if (substr($voicemail_message_audio_to_play, 0, 4) != "http")
                $voicemail_message_audio_to_play = $url."/audio/voice_broadcast_files/".$campaign_details['user']."/".urlencode($voicemail_message_audio_to_play);
        }
    }else{
        if ($campaign_details['live_answer_type'] == "Text") {
            $live_answer_type = "text";
            $live_answer_text_to_say = $campaign_details['live_answer_content'];
            $live_answer_text_voice = $campaign_details['live_answer_voice'];
            $live_answer_text_language = $campaign_details['live_answer_language'];
            $live_answer_text_language = str_replace("|W", "", $live_answer_text_language);
            $live_answer_text_language = str_replace("|M", "", $live_answer_text_language);
        }
        else {
            $live_answer_type = "audio";
            $live_answer_audio_to_play = $campaign_details['live_answer_content'];

            if (substr($live_answer_audio_to_play, 0, 4) != "http")
                $live_answer_audio_to_play = $url."/audio/voice_broadcast_files/".$campaign_details['user']."/".urlencode($live_answer_audio_to_play);
        }

        if ($campaign_details['voicemail_message_type'] == "Text") {
            $voicemail_message_type = "text";
            $voicemail_message_text_to_say = $campaign_details['voicemail_message_content'];
            $voicemail_message_text_voice = $campaign_details['voicemail_message_voice'];
            $voicemail_message_text_language = $campaign_details['voicemail_message_language'];
            $voicemail_message_text_language = str_replace("|W", "", $voicemail_message_text_language);
            $voicemail_message_text_language = str_replace("|M", "", $voicemail_message_text_language);
        }
        else {
            $voicemail_message_type = "audio";
            $voicemail_message_audio_to_play = $campaign_details['voicemail_message_content'];

            if (substr($voicemail_message_audio_to_play, 0, 4) != "http")
                $voicemail_message_audio_to_play = $url."/audio/voice_broadcast_files/".$campaign_details['user']."/".urlencode($voicemail_message_audio_to_play);
        }
    }

endif;
?>
<Response>
<?php
$play_opt_out = true;

switch ($_REQUEST['AnsweredBy']) {
    case 'machine':
        if ($campaign_details['live_answer_only']) {
            ?>
<Hangup/>
            <?php
        }
        else {
            if ($voicemail_message_type == "text") {
                ?>
    <Say voice="<?php echo $voicemail_message_text_voice; ?>" language="<?php echo $voicemail_message_text_language; ?>"><?php echo $voicemail_message_text_to_say; ?></Say><?php
            }
            else {
                ?>
    <Play><?php echo str_replace(" ", "%20", $voicemail_message_audio_to_play); ?></Play><?php   
            }
        }
        $play_opt_out = false;
    break;

    default:
        if ($campaign_details['voicemail_only']) {
            $play_opt_out = false;
            ?>
<Hangup/>
            <?php
        }
        else {
            if ($campaign_details['allow_ivr']) { 
                ?>
                    <Gather action="<?php echo $url; ?>/include/ad_auto_dialer_files/ad_twiml.php?cmp_idx=<?php echo $campaign_idx; ?>&amp;cmp_type=vb&amp;to=<?php echo urlencode(trim($_REQUEST['to'])); ?>&amp;caller_id=<?php urlencode(trim($_REQUEST['caller_id'])); ?>&amp;go_to_ivr=true" method="GET" numDigits="1">
                <?php 
            }

            if ($live_answer_type == "text") { ?>
                <Say voice="<?php echo $live_answer_text_voice; ?>" language="<?php echo $live_answer_text_language; ?>"><?php echo $live_answer_text_to_say; ?></Say><?php
            }
            else {
                ?>
                <Play><?php echo str_replace(" ", "%20", $live_answer_audio_to_play); ?></Play>
                <?php   
            }

            if ($campaign_details['allow_ivr']) { 
                ?>
                    </Gather>
                <?php 
            }
        }
    break;
}

if ($play_opt_out) {
    ?>
        <Gather action="<?php echo $url; ?>/include/ad_auto_dialer_files/ad_twiml.php?cmp_idx=<?php echo $campaign_idx; ?>&amp;cmp_type=vb&amp;to=<?php echo urlencode(trim($_REQUEST['to'])); ?>&amp;caller_id=<?php urlencode(trim($_REQUEST['caller_id'])); ?>&amp;opt_out=true" method="GET" numDigits="1">
    <?php
         if ($opt_out_type == "text") {
                ?>
    <Say voice="<?php echo $opt_out_text_voice; ?>" language="<?php echo $opt_out_text_language; ?>"><?php echo $opt_out_text_to_say; ?></Say><?php
            }
            else {
                ?>
    <Play><?php echo str_replace(" ", "%20", $opt_out_audio_to_play); ?></Play><?php   
        }
    ?>

    <?php
         if ($global_opt_out_type == "text") {
                ?>
    <Say voice="<?php echo $global_opt_out_text_voice; ?>" language="<?php echo $global_opt_out_text_language; ?>"><?php echo $global_opt_out_text_to_say; ?></Say><?php
            }
            else {
                ?>
    <Play><?php echo str_replace(" ", "%20", $global_opt_out_audio_to_play); ?></Play><?php   
        }
    ?>
    </Gather>
<?php
}
?>    
</Response>