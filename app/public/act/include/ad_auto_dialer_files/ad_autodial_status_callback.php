<?php
//Passing thr http 200 ok header to override the 404 error by wp-blog-header.php
header("HTTP/1.1 200 OK");

//Setting the content tyoe of document to XML, So, callers should treat this accordingly
header('Content-type: text/xml');

//Loading the neccessary files
require_once '../util.php';
require_once '../Pagination.php';
require_once '../twilio_header.php';

//Including the library Auto Dialer and Voice Broadcast files
require_once 'lib/ad_lib_funcs.php';

error_reporting(E_ALL);
ini_set("display_errors", true);

//Initializing the database object
$db = new DB();

//Setting the APP type
$_REQUEST['app'] = 'autodialer';

//Adding the params to global $_REQUEST variable
$params = unserialize(base64_decode($_REQUEST['params']));
$_REQUEST_DATA = array_merge($_REQUEST, $params);

//Adding a log entry
ad_ad_add_log($_REQUEST_DATA);

//If camp_idx param is set in request URL
if (isset($_REQUEST_DATA['cmp_idx'])):

    //Retrieving the required info from request URL
    $campaign_idx = trim($_REQUEST_DATA['cmp_idx']);
    $to = trim($_REQUEST_DATA['To']);

    //updating the campaign progress and contact status
    ad_ad_update_campaign_contact_status($campaign_idx, $to, 'completed');

    //Total campaign completed calls 
    $camp_completed_calls = ad_ad_get_campaign_completed_calls_count($campaign_idx);
    //total calls of campaign
    $camp_all_calls = ad_ad_get_campaign_all_calls_count($campaign_idx);

    $status = $_REQUEST_DATA['CallStatus'];

    if ($camp_completed_calls == $camp_all_calls)
        $status = "completed";

    //Updating the overall campaign progress along with associated details
    ad_ad_update_campaign_progress($campaign_idx, $status);

    $_REQUEST['userid'] = $_REQUEST_DATA['user'];
    $result = $db->customExecute("SELECT international FROM company_num WHERE number = ?");
    $result->execute(array($_REQUEST_DATA['From']));
    $result = $result->fetch();

    if ($result['international'] == 1)
        $_REQUEST['From'] = "+".$_REQUEST_DATA['From'];
    else
        $_REQUEST['From'] = "+1".$_REQUEST_DATA['From'];

    $_REQUEST['To'] = "+".$_REQUEST_DATA['To'];
    $resp = $db->save_outgoing_call();

    $stmt = $db->customExecute("SELECT * FROM zapier_subscriptions WHERE event = 'incoming_call_autodialer' AND campaign_id = ?");
    $stmt->execute(array($_REQUEST_DATA['cmp_idx']));
    $subs = $stmt->fetchAll(PDO::FETCH_OBJ);
    if(is_array($subs)||is_object($subs))
        foreach($subs as $sub){
            $check1 = false;
            $check2 = false;
            if($db->isUserAdmin($_REQUEST_DATA['user'])==false){
                if($sub->user_id != $_REQUEST_DATA['user'])
                    $check1 = false;
                session_start();
                $_SESSION['user_id'] = $sub->user_id;
                $_SESSION['permission'] = 0;
                $cmps = ad_ad_get_campaigns("",1,9000);
                foreach($cmps as $cmp){
                    if($cmp['idx']==$_REQUEST_DATA['cmp_idx'])
                        $check2 = true;
                }
            }else{
                $check1 = true;
                $check2 = true;
            }

            if($check1 && $check2){
                $stmt = $db->customExecute("SELECT * FROM ad_ad_call_log WHERE `CallSid` = ?");
                $stmt->execute(array($_REQUEST_DATA['CallSid']));
                $call_data = $stmt->fetch(PDO::FETCH_OBJ);

                $stmt = $db->customExecute("SELECT * FROM ad_advb_cl_contacts WHERE phone_number = ?");
                $stmt->execute(array($_REQUEST_DATA['To']));
                $contact = $stmt->fetch(PDO::FETCH_OBJ);

                $_obj = new stdClass();
                $_date = new DateTime($call_data->date,new DateTimeZone("UTC"));
                $_date->setTimezone(new DateTimeZone($TIMEZONE));
                $_obj->DateCreated = $_date->format("D n\/d Y g\:iA T");
                $pcs = $db->getPhoneCodes($call_data->company_id,2);
                $_obj->PhoneCode = "None";
                if(is_array($pcs) || is_object($pcs))
                    foreach($pcs as $pc){
                        if($pc->idx == $log->code)
                            $_obj->PhoneCode = $pc->name;
                    }
                $_obj->CampaignNumber = $call_data->from;
                $_obj->PhoneNumber = $call_data->to;
                $_obj->Duration = $call_data->duration==""?0:(int)$call_data->duration;
                $_obj->Status = $call_data->response;
                $_obj->Recording = $call_data->recording_url;
                $_obj->BusinessName = $contact->business_name;
                $_obj->FirstName = $contact->first_name;
                $_obj->LastName = $contact->last_name;
                $_obj->Email = $contact->email;
                $_obj->Address = $contact->address;
                $_obj->City = $contact->city;
                $_obj->State = $contact->state;
                $_obj->Zip = $contact->zip;
                $_obj->Website = $contact->website;
                $_obj->OptOut = $contact->opt_out;

                $data_string = json_encode($_obj);
                $ch = curl_init($sub->subscription_url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );

                $result = curl_exec($ch);
                $responseInfo = curl_getinfo($ch);
                if($responseInfo['http_code']!=200){
                    $stmt = $db->customExecute("DELETE FROM zapier_subscriptions WHERE id = ?");
                    $stmt->execute(array($sub->id));
                }
            }
        }

endif;

//$fp = fopen('test2.txt', 'a+');
//fwrite($fp, print_r($_REQUEST, true));
//fclose($fp);
?>
<Response/>