<?php
//Passing the 200 Success message
header("HTTP/1.1 200 OK");

//Passing the content type of current document to treat this accordingly
header("Content-type: text/xml");

//Initializing the session
session_start();

//Loading the neccessary library files
require "../config.php";
require "../db.php";
require_once '../twilio_header.php';

//$fp = fopen('calldata.txt', 'a+');
//fwrite($fp, print_r($_REQUEST, TRUE));
//fclose($fp);

$status_callback_params = base64_encode(serialize(array(
    'user' => $_REQUEST['userid'],
    'To' => $_REQUEST["tocall"],
    'From' => $_REQUEST["Fromcall"],
    'cmp_idx' => $_REQUEST['campaign_idx'],
    'company_id'=> $_REQUEST['company_id']
        )));
?>
<Response>
    <Dial method="post" action="<?php echo dirname(s8_get_current_webpage_uri()) . '/ad_autodial_status_callback.php?params=' . $status_callback_params; ?>" record="true"  callerId="<?php echo htmlspecialchars($_REQUEST["Fromcall"]); ?>">
        <Number url="<?php echo str_replace("include/ad_auto_dialer_files", "", dirname(s8_get_current_webpage_uri())) . 'ad_ajax.php?action=store_current_call_sid'; ?>">
            <?php echo htmlspecialchars($_REQUEST["tocall"]); ?>
        </Number>
    </Dial>
</Response>
<?php
exit;
?>