<?php

/*
 * This file contains all the voice broadcast functions
 */
if (!function_exists('ad_vb_add_campaign')):

    /**
     * This function will add a campaign into the system
     * @global DB $db The global database object perform db operations
     * @param array $_campaign_data An array containing campaign data against db column names
     * @return mixed ID of last inserted row or boolean false on failed query
     */
    function ad_vb_add_campaign($_campaign_data = array()) {
        //Intializing the response variable
        //Assuming the system failed to add campaign in db
        $output = FALSE;

        //Defining the voice bradcast campaigns table column name
        $campaign_columns = array(
            'campaign_name',
            'phone_number',
            'live_answer_content',
            'live_answer_type',
            'live_answer_voice',
            'live_answer_language',
            'voicemail_message_content',
            'voicemail_message_type',
            'voicemail_message_voice',
            'voicemail_message_language',
            'calls_status',
            'progress',
            'last_ran',
            'when_to_run',
            'user',
            'shared',
            'type',
            'sms_message',
            'list_id',
            'voicemail_only',
            'live_answer_only',
            'allow_ivr',
            'ivr_key',
            'ivr_phone_number'
        );
        $campaign_columns_data = array();

        //Looping through campaign column names
        foreach ($campaign_columns as $campaign_col_name):
            //if column does not exist in campaign data received via argument
            if (!isset($_campaign_data[$campaign_col_name])):
                //setting the null value to it
                $campaign_columns_data[$campaign_col_name] = '';
            else:
                //ELSE filtering the value
                $campaign_columns_data[$campaign_col_name] = $_campaign_data[$campaign_col_name];
            endif;
        endforeach;

        //Initializing the global database object
        global $db;
        //if database object does not exist
        if (!is_object($db)):
            //creating a new instance
            $db = new DB();
        endif;

        $query = $db->customQuery("SELECT count(*) as total FROM ad_vb_campaigns WHERE campaign_name = '".$campaign_columns_data['campaign_name']."'");
        $count = $query->fetchAll(PDO::FETCH_ASSOC);

        if($count[0]['total']>0){
            return false;
        }

        //Executing the query to insert in db
        $query_status = $db->customQuery("
            INSERT INTO " . AD_VOICE_BROADCAST_CAMPAIGNS . " (" . implode(',', $campaign_columns) . ")
                VALUES ('" . implode("','", $campaign_columns_data) . "')
        ");

        //If query status is not equals to false
        if ($query_status !== FALSE)
            $output = $db->lastInsertId();

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_vb_update_campaign')):

    /**
     * This function will update campaign of system
     * @global DB $db The global database object perform db operations
     * @param array $_campaign_data An array containing campaign data against db column names
     * @return mixed ID of last inserted row or boolean false on failed query
     */
    function ad_vb_update_campaign($campaign_idx, $_campaign_data = array()) {
        //Intializing the response variable
        //Assuming the system failed to add campaign in db
        $output = FALSE;

        //Defining the voice bradcast campaigns table column name
        $campaign_columns = array(
            'campaign_name',
            'phone_number',
            'live_answer_content',
            'live_answer_type',
            'live_answer_voice',
            'live_answer_language',
            'voicemail_message_content',
            'voicemail_message_type',
            'voicemail_message_voice',
            'voicemail_message_language',
            'when_to_run',
            'user',
            'shared',
            'sms_message',
            'voicemail_only',
            'live_answer_only',
            'allow_ivr',
            'ivr_key',
            'ivr_phone_number'
        );
        $campaign_columns_data = array();

        //Looping through campaign column names
        foreach ($campaign_columns as $campaign_col_name):
            //if column does not exist in campaign data received via argument
            if (!isset($_campaign_data[$campaign_col_name])):
                //setting the null value to it
                $campaign_columns_data[$campaign_col_name] = '';
            else:
                //ELSE filtering the value
                $campaign_columns_data[$campaign_col_name] = " " . $campaign_col_name . " = '$_campaign_data[$campaign_col_name]'";
            endif;
        endforeach;

        //Initializing the global database object
        global $db;
        //if database object does not exist
        if (!is_object($db)):
            //creating a new instance
            $db = new DB();
        endif;

        //echo "UPDATE " . AD_VOICE_BROADCAST_CAMPAIGNS . " SET " . implode(',', $campaign_columns_data) . " WHERE idx = '$campaign_idx'";

        //Executing the query to insert in db
        $query_status = $db->customQuery("UPDATE " . AD_VOICE_BROADCAST_CAMPAIGNS . " SET " . implode(',', $campaign_columns_data) . " WHERE idx = '$campaign_idx'");

        //If query status is not equals to false
        if ($query_status !== FALSE)
            $output = TRUE;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

function ad_vb_update_sequence($sequence_id, $_sequence_data = array()) {
    //Intializing the response variable
    //Assuming the system failed to add campaign in db
    $output = FALSE;

    //Defining the voice bradcast campaigns table column name
    $sequence_columns = array(
        'type',
        'name',
        'days_after',
        'time',
        'sms_message',
        'live_answer_content',
        'live_answer_type',
        'live_answer_voice',
        'live_answer_language',
        'voicemail_message_content',
        'voicemail_message_type',
        'voicemail_message_voice',
        'voicemail_message_language'
    );
    $sequence_columns_data = array();

    //Looping through campaign column names
    foreach ($sequence_columns as $sequence_col_name):
        //if column does not exist in campaign data received via argument
        if (!isset($_sequence_data[$sequence_col_name])):
            //setting the null value to it
            $sequence_columns_data[$sequence_col_name] = '';
        else:
            //ELSE filtering the value
            $sequence_columns_data[$sequence_col_name] = " " . $sequence_col_name . " = '$_sequence_data[$sequence_col_name]'";
        endif;
    endforeach;

    //Initializing the global database object
    global $db;
    //if database object does not exist
    if (!is_object($db)):
        //creating a new instance
        $db = new DB();
    endif;

    //echo "UPDATE " . AD_VOICE_BROADCAST_CAMPAIGNS . " SET " . implode(',', $campaign_columns_data) . " WHERE idx = '$campaign_idx'";

    //Executing the query to insert in db
    $query_status = $db->customQuery("UPDATE ad_vb_sequences SET " . implode(',', $sequence_columns_data) . " WHERE id = '$sequence_id'");

    //If query status is not equals to false
    if ($query_status !== FALSE)
        $output = TRUE;

    //Returning the response back to caller of this function
    return $output;
}

if (!function_exists('ad_vb_get_campaigns')):

    /**
     * This function will retrieve the voice broadcast campaigns listing from system db
     * @global DB $db The global database object variable
     * @return array AN array containing campaings listing
     */
    function ad_vb_get_campaigns($query = "", $page = 1, $default_rows = 20) {
        session_start();
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //calculating the starting row index
        $start = (($page - 1) * 20);

        $sql_to_add = "";
        if($_SESSION['permission']<1)
        {
            $sql_to_add = " AND user = '".$_SESSION['user_id']."' OR shared = 1";
        }

        //Querying the db
        $result = $db->customQuery("Select * from " . AD_VOICE_BROADCAST_CAMPAIGNS . " WHERE campaign_name LIKE '%$query%'$sql_to_add ORDER BY idx DESC LIMIT $start, $default_rows");

        $realres = array();

        if(is_array($result) || is_object($result))
        foreach($result->fetchAll() as $res){
            if($res['shared']==1 && $_SESSION['permission']<1 && $res['user']!=$_SESSION['user_id']){
                $result = $db->customQuery("SELECT COUNT(*) as total FROM user_company WHERE user_id = '".$res['user']."' AND company_id IN (SELECT company_id FROM user_company WHERE user_id = '".$_SESSION['user_id']."')");
                $count = $result->fetchAll(PDO::FETCH_ASSOC);
                if($count[0]['total']>0){
                    $realres[] = $res;
                }
            }else{
                $realres[] = $res;
            }
        }

        //Returning the response back to caller of this function
        return $realres;
    }

endif;

function ad_vb_get_campaigns_cron() {
    global $db;
    if (!is_object($db)):
        $db = new DB();
    endif;

    //Querying the db
    $result = $db->customQuery("Select * from " . AD_VOICE_BROADCAST_CAMPAIGNS . ";");

    $realres = $result->fetchAll(PDO::FETCH_ASSOC);

    //Returning the response back to caller of this function
    return $realres;
}


if (!function_exists('ad_vb_get_campaigns_count')):

    /**
     * This function will retrieve the total number of campiagn that did exist in system db.
     * @global DB $db The global database object variable
     * @return int Returns the count of campaigns as numeric value
     */
    function ad_vb_get_campaigns_count($query = "") {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("SELECT count(*) as total FROM " . AD_VOICE_BROADCAST_CAMPAIGNS." WHERE campaign_name LIKE '%$query%'");

        //Retrieving the queried data
        $queried_data = $result->fetchAll();

        //Returning the response back to caller of this function.
        return $queried_data[0]['total'];
    }

endif;

if (!function_exists('ad_vb_get_campaign_data')):

    /**
     * This function will retrieve the details of single campaign 
     * using the campaign ID provided in function argument.
     * @global DB $db The global database object variable
     * @return array An array containing campaign data.
     */
    function ad_vb_get_campaign_data($campaign_idx) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db to select the data to be retrieved
        $result = $db->customQuery("Select * from " . AD_VOICE_BROADCAST_CAMPAIGNS . " WHERE idx = '$campaign_idx'");

        //Retrieving the queried data
        $queried_data = $result->fetchAll();

        //Returning the response back to caller of this function
        return $queried_data[0];
    }

endif;

if (!function_exists('ad_vb_get_campaign_name')):

    /**
     * This function will retrieve the VOICE BROADCAST campaign name by ID provided in argument
     * @global DB $db The global database object variable
     * @param int $campaign_id The campaign ID of Voice broadcast
     * @return string The campign name of prvided ID if exists ELSE boolean FALSE
     */
    function ad_vb_get_campaign_name($campaign_id) {
        //Intializing the response variable
        //Assuming that no campaign exists against provided ID
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_response = $db->customQuery("Select campaign_name from " . AD_VOICE_BROADCAST_CAMPAIGNS . " WHERE idx = '$campaign_id'");

        $result = $query_response->fetchAll();

        //If campaign does exist in system
        if (isset($result[0]['campaign_name'])):

            //Storing the campaign name in response variable
            $output = $result[0]['campaign_name'];

        endif;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_vb_delete_campaign')):

    /**
     * This function will delete the campaign from system db
     * @param int $campaign_idx The unique and numeric campaign ID.
     */
    function ad_vb_delete_campaign($campaign_idx) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $db->customQuery("DELETE FROM " . AD_VOICE_BROADCAST_CAMPAIGNS . " WHERE idx = '$campaign_idx'");
        $db->customQuery("DELETE FROM " . AD_VOICE_BROADCAST_CALL_LOG . " WHERE campaign_idx = '$campaign_idx'");
        $db->customQuery("DELETE FROM " . AD_ADVB_CONTACTS . " WHERE campaign_idx = '$campaign_idx'");
        $db->customQuery("DELETE FROM ad_vb_sequences WHERE campaign_id = '$campaign_idx'");
    }

endif;

if (!function_exists('ad_vb_add_log')):

    /**
     * This function will add a voice broadcast log into the system db.
     * @global DB $db The global database object variable
     * @param array $log_details An arry containing log data
     * @return boolean Returns boolean TRUE or FALSE based on insert operation performed.
     */
    function ad_vb_add_log($log_details) {
        //Initializing the response variable
        //Assuming that insert operation failed
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_status = $db->customQuery("
                INSERT INTO " . AD_VOICE_BROADCAST_CALL_LOG . "
                (campaign_idx, `date`, `from`, `to`, duration, response, code, user, recording_url)
                VALUES
                (
                    '" . @$log_details['cmp_idx'] . "',
                    '" . @date( 'Y-m-d H:i:s', strtotime("now")) . "',
                    '" . @$log_details['From'] . "',
                    '" . @$log_details['To'] . "',
                    '" . @$log_details['CallDuration'] . "',
                    '" . @$log_details['CallStatus'] . "',
                    '" . @$log_details['code'] . "',
                    '" . @$log_details['user'] . "',
                    '" . @$log_details['RecordingUrl'] . "'
                    )
            ");

        //If query executes successfully
        //Setting the response variable to TRUE
        if ($query_status !== FALSE)
            $output = TRUE;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_vb_get_logs')):

    /**
     * This function will retrieve the voice broadcast call logs from system db
     * @global DB $db The global database object variable
     * @return array An array containing voice broadcast call logs
     */
    function ad_vb_get_logs($page = 1, $default_rows = 20, $campaign_idx = FALSE) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //calculating the starting row index
        $start = (($page - 1) * 20);

        //Building the where conditions
        //if it is set in argument
        $where = '';
        if ($campaign_idx !== FALSE)
            $where = " WHERE campaign_idx = '$campaign_idx' ";

        //Querying the db
        $result = $db->customQuery("Select * from " . AD_VOICE_BROADCAST_CALL_LOG . " " . $where . " ORDER BY idx DESC LIMIT $start, $default_rows");

        //Setting the fetch mode as associative
        $result->setFetchMode(PDO::FETCH_ASSOC);

        //Retrieving the queried data
        $realres2 = array();

        if(is_object($result)){
            $add_to_query = "";
            if($_SESSION['permission']<1){
                $add_to_query = " WHERE user = '".$_SESSION['user_id']."' OR shared = 1";
            }
            //Querying the db
            $result2 = $db->customQuery("Select * from " . AD_VOICE_BROADCAST_CAMPAIGNS . "$add_to_query ORDER BY idx DESC");

            $realres = array();

            if(is_array($result2) || is_object($result2))
                foreach($result2->fetchAll() as $res){
                    if($res['shared']==1 && $_SESSION['permission']<1 && $res['user']!=$_SESSION['user_id']){
                        $result2 = $db->customQuery("SELECT COUNT(*) as total FROM user_company WHERE user_id = '".$res['user']."' AND company_id IN (SELECT company_id FROM user_company WHERE user_id = '".$_SESSION['user_id']."')");
                        $count = $result2->fetchAll(PDO::FETCH_ASSOC);
                        if($count[0]['total']>0){
                            $realres[] = $res;
                        }
                    }else{
                        $realres[] = $res;
                    }
                }

            foreach($result->fetchAll() as $res){
                foreach($realres as $res2){
                    if($res2['idx'] == $res['campaign_idx']){
                        $realres2[] = $res;
                    }
                }
            }
        }

        // $queried_data = $result->fetchAll();

        //Returning the response back to caller of this function.
        return $realres2;
    }

endif;

if (!function_exists('ad_vb_get_logs_count')):

    /**
     * This function will retrieve the total number of logs that did exist in system db.
     * @global DB $db The global database object variable
     * @return int Returns the count of logs as numeric value
     */
    function ad_vb_get_logs_count($campaign_idx = FALSE) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //If campaign idx is set
        //Building the where conditions
        $where = '';
        if ($campaign_idx !== false):
            $where = " WHERE campaign_idx = '$campaign_idx' ";
        endif;

        //Querying the db
        $result = $db->customQuery("SELECT count(*) as total FROM " . AD_VOICE_BROADCAST_CALL_LOG . " " . $where);

        //Retrieving the queried data
        $queried_data = $result->fetchAll();

        //Returning the response back to caller of this function.
        return $queried_data[0]['total'];
    }

endif;

if (!function_exists('ad_vb_process_voice_broadcast_campaign')):

    /**
     * This function will process the calls to be made of campaign.
     * @global string $AccountSid The unique twilio account sid.
     * @global string $AuthToken The unique twilio auth token.
     * @param int $campaign_idx The unique campaign ID of voice broadcast.
     * @param string $ad_include_uri The web accessible uri of auto dialer folder 
     * but without any trailing slash.
     */
    function ad_vb_process_voice_broadcast_campaign($campaign_idx, $ad_include_uri) {
        //Generate DB
        $db = new DB();

        //Retrieving the campaign data
        $campaign_details = ad_vb_get_campaign_data($campaign_idx);

        $list_details = $db->getContactListById($campaign_details['list_id']);

        //Retrieving all the contacts of newly created campaign
        $campaign_contacts = ad_advb_retrieve_contacts('vb', $campaign_idx);

        //Extending the scope of global variables
        Global $AccountSid, $AuthToken;

        //Initializing the client
        $client = new Services_Twilio($AccountSid, $AuthToken);

        //Retrieving the caller ID
        $caller_id = $campaign_details['phone_number'];

        $campaign_details['calls_status'] = "NA";

        //If there is atleast on contact in campaign
        if (is_array($campaign_contacts) && count($campaign_contacts) > 0 && $campaign_details['calls_status']!="running"):

            //Looping over each contact  campaign

            if($campaign_details['type']=="VoiceBroadcast") {
                foreach ($campaign_contacts as $to) {
                    $opted_out = $db->checkNumberOptedOut($campaign_details['list_id'], $db->format_phone_db($to));

                    if (!$opted_out) {
                         try {
                            $client->account->calls->create(trim($caller_id), trim($to), $ad_include_uri . "/include/ad_auto_dialer_files/ad_twiml.php?cmp_idx=$campaign_idx&cmp_type=vb&to=" . urlencode(trim($to)) . "&caller_id=" . urlencode(trim($caller_id)), array(
                                "Method" => "POST",
                                "IfMachine" => "Continue",
                                "Record" => true,
                                "StatusCallback" => $ad_include_uri . "/include/ad_auto_dialer_files/ad_status_callback.php?cmp_idx=$campaign_idx&cmp_type=vb&to=" . urlencode(trim($to)) . "&caller_id=" . urlencode(trim($caller_id)) . "&userid=" . trim(@$_SESSION['user_id']),
                                "StatusCallbackMethod" => "POST"
                            ));
                        }
                        catch (Exception $e) {

                        }
                    }
                    else {
                        ad_vb_add_log(array(
                            "cmp_idx" => $campaign_idx,
                            "From" => $caller_id,
                            "To" => $to,
                            "CallDuration" => "0",
                            "CallStatus" => "Opted-Out",
                            "code" => "",
                            "user" => "",
                            "RecordingUrl" => ""
                        ));

                        //updating the campaign progress and contact status
                        ad_vb_update_campaign_contact_status($campaign_idx, $to, 'completed');

                        //Total campaign completed calls 
                        $camp_completed_calls = ad_vb_get_campaign_completed_calls_count($campaign_idx);
                        //total calls of campaign
                        $camp_all_calls = ad_vb_get_campaign_all_calls_count($campaign_idx);

                        //If completedt calls equals to total calls of campaign
                        if ($camp_completed_calls >= $camp_all_calls):
                            //Setting the campaign status to ompleted
                            $status = 'completed';
                        else:
                            //Else setting the campaign status to running
                            $status = 'running';
                        endif;

                        //Updating the overall campaign progress along with associated details
                        ad_vb_update_campaign_progress($campaign_idx, $status);
                    }
                }
            }
            else {
                // 2.5.1+ Have twilio run this in the background. So we set a callback for Twilio to call to continue this...
                $to = $campaign_contacts[0];
                $opted_out = $db->checkNumberOptedOut($campaign_details['list_id'], $db->format_phone_db($to));

                if (!$opted_out) {
                    // Switch tokens for real data
                    $stmt = $db->customExecute("SELECT * FROM ad_advb_cl_contacts WHERE phone_number = ?");
                    $stmt->execute(array($to));
                    $contact = $stmt->fetch(PDO::FETCH_OBJ);
                    $msg = $campaign_details['sms_message'];
                    $msg = str_replace("[FirstName]",$contact->first_name,$msg);
                    $msg = str_replace("[LastName]",$contact->last_name,$msg);
                    $msg = str_replace("[Email]",$contact->email,$msg);
                    $msg = str_replace("[Phone]",$contact->phone_number,$msg);
                    $msg = str_replace("[Address]",$contact->address,$msg);
                    $msg = str_replace("[City]",$contact->city,$msg);
                    $msg = str_replace("[State]",$contact->state,$msg);
                    $msg = str_replace("[Zip]",$contact->zip,$msg);
                    $msg = str_replace("[Website]",$contact->website,$msg);
                    $msg = str_replace("[Business]",$contact->business_name,$msg);

                    $list_details = $db->getContactListById($campaign_details['list_id']);
                    $msg .= "\n\n".$list_details['sms_opt_out_message'];

                    $global_sms_opt_out_message = ($db->getVar("global_sms_opt_out_message")) ? $db->getVar("global_sms_opt_out_message") : "Reply with CANCEL ALL to stop.";

                    $msg .= "\n\n".$global_sms_opt_out_message;

                    ad_vb_update_campaign_contact_status($campaign_idx, $to, 'completed');

                    try {
                        $client->account->messages->sendMessage($caller_id, $to, html_entity_decode($msg), null,
                            array(
                                'StatusCallback'=>$ad_include_uri . "/include/ad_auto_dialer_files/ad_status_callback.php?cmp_idx=$campaign_idx&cmp_type=sms&from=". urlencode(trim($caller_id)) ."&to=" . urlencode(trim($to)) . "&userid=" . trim(@$_SESSION['user_id'])
                            )
                        );
                        $status = 'completed';
                    }
                    catch (Exception $e) {
                        error_log($e->getMessage()."\r\n".$e->getTraceAsString());

                        if(strpos($e->getMessage(),"not a valid phone number")!==false){
                            $status = 'continue_on_error';
                        }else
                            $status = 'error';
                    }

                    // There was an invalid phone number so we call this again..
                    if($status == 'continue_on_error'){

                        ad_vb_update_campaign_contact_status($campaign_idx, $to, 'completed');
                        $camp_completed_calls = ad_vb_get_campaign_completed_calls_count($campaign_idx);
                        $camp_all_calls = ad_vb_get_campaign_all_calls_count($campaign_idx);
                        if ($camp_completed_calls >= $camp_all_calls):
                            $status = 'completed';
                        else:
                            $status = 'running';
                        endif;
                        ad_vb_update_campaign_progress($campaign_idx, $status);
                        ad_vb_process_voice_broadcast_campaign($campaign_idx, $ad_include_uri);

                    }else{

                        //updating the campaign progress and contact status
                        ad_vb_update_campaign_contact_status($campaign_idx, $to, $status);

                        //Total campaign completed calls
                        $camp_completed_calls = ad_vb_get_campaign_completed_calls_count($campaign_idx);
                        //total calls of campaign
                        $camp_all_calls = ad_vb_get_campaign_all_calls_count($campaign_idx);


                        //If completedt calls equals to total calls of campaign
                        if($status != 'error'){
                            if ($camp_completed_calls >= $camp_all_calls):
                                //Setting the campaign status to ompleted
                                $status = 'completed';
                            else:
                                //Else setting the campaign status to running
                                $status = 'running';
                            endif;
                        }

                        //Updating the overall campaign progress along with associated details
                        ad_vb_update_campaign_progress($campaign_idx, $status);
                    }
                }
                else {
                    ad_vb_add_log(array(
                        "cmp_idx" => $campaign_idx,
                        "From" => $caller_id,
                        "To" => $to,
                        "CallDuration" => "0",
                        "CallStatus" => "Opted-Out",
                        "code" => "",
                        "user" => "",
                        "RecordingUrl" => ""
                    ));

                    //updating the campaign progress and contact status
                    ad_vb_update_campaign_contact_status($campaign_idx, $to, 'completed');

                    //Total campaign completed calls
                    $camp_completed_calls = ad_vb_get_campaign_completed_calls_count($campaign_idx);
                    //total calls of campaign
                    $camp_all_calls = ad_vb_get_campaign_all_calls_count($campaign_idx);

                    //If completedt calls equals to total calls of campaign
                    if ($camp_completed_calls >= $camp_all_calls):
                        //Setting the campaign status to ompleted
                        $status = 'completed';
                    else:
                        //Else setting the campaign status to running
                        $status = 'running';
                    endif;

                    //Updating the overall campaign progress along with associated details
                    ad_vb_update_campaign_progress($campaign_idx, $status);
                }
            }

            if (empty($status))
                $status = 'running';

            //Updating the overall campaign progress along with associated details
            ad_vb_update_campaign_progress($campaign_idx, $status);

        endif;
    }

endif;

if (!function_exists('ad_vb_process_sms_broadcast_campaign')):

    /**
     * This function will process the calls to be made of sms broadcast campaign.
     * @global string $AccountSid The unique twilio account sid.
     * @global string $AuthToken The unique twilio auth token.
     * @param int $campaign_idx The unique campaign ID of sms broadcast.
     * @param string $ad_include_uri The web accessible uri of auto dialer folder 
     * but without any trailing slash.
     */
    function ad_vb_process_sms_broadcast_campaign($campaign_idx, $ad_include_uri) {
        
    }

endif;

if (!function_exists('ad_vb_get_campaign_all_calls_count')):

    /**
     * This function will retrieve the number of contacts to which call has to be made exist in system
     * @global DB $db The global database object
     * @param int $campaign_idx The unique ID of campaign
     * @return int The number of contacts does exist in system to which call has to be made.
     */
    function ad_vb_get_campaign_all_calls_count($campaign_idx) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("Select count(*) as total from " . AD_ADVB_CONTACTS . " WHERE contact_of = '" . AD_ADVB_CONTACT_OF_VOICE_BROADCAST . "' and campaign_idx = '$campaign_idx'");

        //retrrieving the queried data
        $output = $result->fetchAll();

        //Returning the response back to caller of this function
        return $output[0]['total'];
    }

endif;

if (!function_exists('ad_vb_get_campaign_completed_calls_count')):

    /**
     * This function will retrieve all the completed calls from the system
     * @global DB $db The global database object
     * @param int $campaign_idx The unique campaign ID
     * @return int The number of completed calls in system
     */
    function ad_vb_get_campaign_completed_calls_count($campaign_idx) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("Select count(*) as total from " . AD_ADVB_CONTACTS . " WHERE contact_of = '" . AD_ADVB_CONTACT_OF_VOICE_BROADCAST . "' AND campaign_idx = '$campaign_idx' AND call_status = 'completed'");

        //retrrieving the queried data
        $output = $result->fetchAll();

        //Returning the response back to caller of this function
        return $output[0]['total'];
    }

endif;

if (!function_exists('ad_vb_update_campaign_contact_status')):

    /**
     * This function will update the status of campign contact
     * @global DB $db The global databse object
     * @param int $campaign_idx The unique ID of campaign.
     * @param string $contact_number The contact number of campaign.
     * @param string $status Valid values are "pending" and "completed".
     * @return boolean Returns boolean TRUE or FALSE based on update operation performed.
     */
    function ad_vb_update_campaign_contact_status($campaign_idx, $contact_number, $status) {
        //Initializing the response variable
        //Assuming that system failed to update campaign progress.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $query_response = FALSE;

        $result = $db->customQuery("Select idx, contact_number from " . AD_ADVB_CONTACTS . " WHERE campaign_idx = '$campaign_idx' AND contact_of = '" . AD_ADVB_CONTACT_OF_VOICE_BROADCAST . "'");

        //Returning the response back to caller of this function
        $contacts_data = $result->fetchAll();

        if (count($contacts_data) > 0):

            //Looping through contacts data.
            foreach ($contacts_data as $single_contact_data):

                //Storing the number in response variable of this function
                $phone_number = urldecode(utf8_decode($single_contact_data['contact_number']));
                $phone_number = str_replace("?", "", $phone_number);
                $phone_number = str_replace(" ", "", $phone_number);
                $phone_number = trim($phone_number);
                $phone_number = str_replace("\r", "", $phone_number);
                
                if ($phone_number == $contact_number) {
                    $query_response = $db->customQuery("UPDATE " . AD_ADVB_CONTACTS . " SET call_status = '$status' WHERE idx = '".$single_contact_data['idx']."'");
                }

            endforeach;

        endif;

        //If update query executes successfully, setting the response variable to TRUE
        if ($query_response !== FALSE)
            $output = TRUE;

        //Returning the response back to caller of this funtion
        return $output;
    }

endif;

if (!function_exists('ad_vb_update_campaign_progress')):

    /**
     * This function will update the campaign progress in system db
     * @global DB $db The global database object variable
     * @param int $campaign_idx The unique campaign ID.
     * @param string $status Valid values are "running", "completed".
     * @return boolean Returns boolean TRUE or FALSE based on update operation performed.
     */
    function ad_vb_update_campaign_progress($campaign_idx, $status) {
        //Initializing the response variable
        //Assuming that system failed to update campaign progress.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Updating the campaign details
        $query_response = $db->customQuery("UPDATE " . AD_VOICE_BROADCAST_CAMPAIGNS . " SET calls_status = '$status', last_ran = '" . time() . "' WHERE idx = '$campaign_idx'");

        //If query does not execute successfully
        if ($query_response !== FALSE)
            $output = true;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_vb_get_campaign_progress')):

    /**
     * This function will retrieve the campaign progress from system db
     * @param int $campaign_idx The unique campaign ID.
     * @return boolean Returns string containing campaign progress.
     */
    function ad_vb_get_campaign_progress($campaign_idx) {
        //Retrieving the current campaign calls progress from system db
        return ad_vb_get_campaign_completed_calls_count($campaign_idx) . '/' . ad_vb_get_campaign_all_calls_count($campaign_idx);
    }









endif;