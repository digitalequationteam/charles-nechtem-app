<?php

//Loading the configuration file
require_once 'config.ini.php';

//Loading the associated files too
require_once 'ad_ad_lib_funcs.php';
require_once 'ad_vb_lib_funcs.php';

//If function not already loaded in program
if (!function_exists('ad_db_handle_data_tables')):

    /**
     * (REMOVED) This function will create auto dialer tables in DB if not already exists.
     * @global DB $db The global database object
     */
    function ad_db_handle_data_tables() {

    }

endif;

if (!function_exists('ad_advb_cl_is_cl_exists')):

    /**
     * This function will check if contact list do exist in system or not.
     * @global DB $db The global dtaabse object variable
     * @param string $cl_name The name of contact list
     * @return boolean Returns boolean TRUE or FALSE based on check performed.
     */
    function ad_advb_cl_is_cl_exists($cl_name) {
        //Initializing response variable
        //Assuming that not contact list do exist in system.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Executing query
        $result = $db->customQuery("SELECT count(*) as total FROM " . AD_ADVB_CONTACT_LISTS . " WHERE list_name = '$cl_name'");

        //If query executes successfully
        if (is_object($result)):

            //Retrieving results
            $queried_data = $result->fetchAll();

            //If total rows count is greater than 0
            //Setting the resonse variable to TRUE
            //Indicates list exist in the system
            if ($queried_data['0']['total'] > 0)
                $output = TRUE;

        endif;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_cl_idx')):

    /**
     * This function will retrive the unique contact list IDx 
     * @param string $list_name The name of contact list
     */
    function ad_advb_cl_get_cl_idx($list_name) {
        //Initializing the response variable
        //Assuming that contact list do not exist the system
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        if (ad_advb_cl_is_cl_exists($list_name)):

            //Executing retrieval query
            $query = $db->customQuery("SELECT idx FROM " . AD_ADVB_CONTACT_LISTS . " WHERE list_name = '$list_name'");

            //Retrieving queried data
            $queried_data = $query->fetchAll();

            //Storing the listIDx in response variable
            $output = $queried_data[0]['idx'];

        endif;

        //Returning the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_add_cl')):

    /**
     * This function will add contact list in the system
     * @global DB $db The global databse object variable.
     * @param string $cl_name The name of contact list.
     */
    function ad_advb_cl_add_cl($cl_name, $sms_keyword = "", $sms_number = "", $sms_response = "",$shared = 0, $vb_opt_out_content = "", $vb_opt_out_type = "", $vb_opt_out_voice = "", $vb_opt_out_language = "", $vb_opt_out_key = "", $sms_opt_out_message = "", $sms_opt_out_trigger = "") {
        session_start();
        //Initializing the response variable
        //Assuming that system failed to add contact list into the system
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //If contact list do not exist in system
        //Executing query to add lkst in the system
        if (!ad_advb_cl_is_cl_exists($cl_name)):

            if($sms_number!=""){
                $stmt = $db->customExecute("SELECT * FROM ad_advb_contact_lists WHERE optin_number = ?");
                $stmt->execute(array($sms_number));
                $ret = $stmt->rowCount();
                if($ret>0) {
                    return FALSE;
                }
            }

            $cl_name = addslashes($cl_name);
            $sms_response = $db->getDb()->escape($sms_response);
            //Executing the insert operation
            $query_status = $db->customQuery("INSERT INTO 
                " . AD_ADVB_CONTACT_LISTS . " (list_name, last_updated, user, shared, optin_number, optin_keyword, optin_response, vb_opt_out_content, vb_opt_out_type, vb_opt_out_voice, vb_opt_out_language, vb_opt_out_key, sms_opt_out_message, sms_opt_out_trigger) 
                VALUES ('$cl_name', '" . date('Y-m-d H:i:s') . "', '".$_SESSION['user_id']."', ".$shared.", '".$sms_number."', '".strtolower($sms_keyword)."', '".$sms_response."', '".$vb_opt_out_content."', '".$vb_opt_out_type."', '".$vb_opt_out_voice."', '".$vb_opt_out_language."', '".$vb_opt_out_key."', '".$sms_opt_out_message."', '".$sms_opt_out_trigger."')");

            //If insert query executes successfully
            if ($query_status !== FALSE):

                //Retrieving the IDx of last row inserted into the system
                $output = $db->lastInsertId();

            endif;

        //If contact list do not exist in the system
        else:

            //ELSE retrieving the unique IDx of already existing list
            $output = ad_advb_cl_get_cl_idx($cl_name);

            if($sms_number!=""){
                $stmt = $db->customExecute("SELECT * FROM ad_advb_contact_lists WHERE optin_number = ? AND idx != ?");
                $stmt->execute(array($sms_number, $output));
                $ret = $stmt->rowCount();
                if($ret>0) {
                    return FALSE;
                }
            }

            $sms_response = $db->getDb()->escape($sms_response);
            //Updating 'last_updated' column of this campaign
            $db->customQuery("UPDATE " . AD_ADVB_CONTACT_LISTS . " SET last_updated = '" . date("Y-m-d H:i:s") . "', shared = '".$shared."', optin_keyword = '".strtolower($sms_keyword)."', optin_number = '".$sms_number."', optin_response = '".$sms_response."', vb_opt_out_content = '".$vb_opt_out_content."', vb_opt_out_type = '".$vb_opt_out_type."', vb_opt_out_voice = '".$vb_opt_out_voice."', vb_opt_out_language = '".$vb_opt_out_language."', vb_opt_out_key = '".$vb_opt_out_key."', sms_opt_out_message = '".$sms_opt_out_message."', sms_opt_out_trigger = '".$sms_opt_out_trigger."'  WHERE idx = '$output'");

        endif;

        //Returning the response back to calle rof this function
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_contact_lists')):

    /**
     * This function will retrieve the contact lists data from system
     * @global DB $db The global variable containing database object
     * @return array An array containing data of all the lists against this keys
     * idx, list_name, status, last_updated, date.
     */
    function ad_advb_cl_get_contact_lists($page = 1, $default_rows = 20, $all = false, $order_by = "last_updated", $order_type = "DESC") {
        session_start();

        //Initializing the response variable
        //Assuming that no contact list exist in system yet
        $output = array();

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $start = (($page - 1) * 20);

        $sql_to_add = "";
        if($_SESSION['permission']<1)
        {
            $sql_to_add = " WHERE user = '".$_SESSION['user_id']."' OR shared = 1";
        }

        if ($order_by == 'contacts_nr')
            $order_by = " (SELECT count(*) FROM ad_advb_cl_link cll WHERE cl.idx = cll.list_idx) ";

        if ($order_by == 'user')
            $order_by = " (SELECT username FROM users u WHERE u.idx = cl.user) ";

        //Executing the db query to retrieve our required data
        if ($all)
            $query = $db->customQuery("SELECT * FROM " . AD_ADVB_CONTACT_LISTS . " cl $sql_to_add ORDER BY $order_by $order_type");
        else
            $query = $db->customQuery("SELECT * FROM " . AD_ADVB_CONTACT_LISTS . " cl $sql_to_add ORDER BY $order_by $order_type LIMIT $start, $default_rows");

        if (is_object($query)):

            //Retrieving all the queried data
            //and Storing the queried data in response variable

            $realres = array();

            if(is_array($query) || is_object($query))
            foreach($query->fetchAll() as $res){
                if($res['shared']==1 && $_SESSION['permission']<1 && $res['user']!=$_SESSION['user_id']){
                    $result = $db->customQuery("SELECT COUNT(*) as total FROM user_company WHERE user_id = '".$res['user']."' AND company_id IN (SELECT company_id FROM user_company WHERE user_id = '".$_SESSION['user_id']."')");
                    $count = $result->fetchAll(PDO::FETCH_ASSOC);
                    if($count[0]['total']>0){
                        $realres[] = $res;
                    }
                }else{
                    $realres[] = $res;
                }
            }
            $output = $realres;
//Returning the response back to caller of this function

        endif;

        //Returning the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_contact_lists_count')):

    /**
     * This function will retrieve the contact lists data from system
     * @global DB $db The global variable containing database object
     * @return array An array containing data of all the lists against this keys
     * idx, list_name, status, last_updated, date.
     */
    function ad_advb_cl_get_contact_lists_count() {
        //Initializing the response variable
        //Assuming that no contact list exist in system yet
        $output = array();

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $sql_to_add = "";
        if($_SESSION['permission']<1)
        {
            $sql_to_add = " WHERE user = '".$_SESSION['user_id']."' OR shared = 1";
        }

        //Executing the db query to retrieve our required data
        $query = $db->customQuery("SELECT COUNT(*) as total FROM " . AD_ADVB_CONTACT_LISTS . "$sql_to_add ORDER BY last_updated DESC");

        if (is_object($query)):

            //Retrieving all the queried data
            //and Storing the queried data in response variable
            $output = $query->fetch();
            $output = $output['total'];

        endif;

        //Returning the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_contact_list_details')):

    /**
     * This function will retrieve the contact lists data from system
     * @global DB $db The global variable containing database object
     * @param int $list_idx The unique contact list IDx.
     * @return array An array containing data of the lists against this keys
     * idx, list_name, status, last_updated, date.
     */
    function ad_advb_cl_get_contact_list_details($list_idx) {
        //Initializing the response variable
        //Assuming that no contact list exist in system yet
        $output = array();

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Executing the db query to retrieve our required data
        $query = $db->customQuery("SELECT * FROM " . AD_ADVB_CONTACT_LISTS . " WHERE idx = '$list_idx'");

        if (is_object($query)):

            //Retrieving all the queried data
            //and Storing the queried data in response variable
            $queried_data = $query->fetchAll();
            $output = $queried_data[0];

        endif;

        //Returning the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_cl_name')):

    /**
     * This function will retrieve the list_name from system if it exist against provided list idx.
     * @param int $list_idx The unique list ID>
     * @return mixed The list name or boolean FALSE if list do not ecist in system
     */
    function ad_advb_cl_get_cl_name($list_idx) {
        //Initializing the response variable
        //Assuming that list do not exist
        $output = FALSE;

        //First, Retrieving list details
        $list_details = ad_advb_cl_get_contact_list_details($list_idx);

        //If list name exist against provided list IDx
        if (isset($list_details['list_name']))
        //Storing the list name in response variable
            $output = $list_details['list_name'];

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if(!function_exists('s8_is_str_blank')) {
    function s8_is_str_blank($str) {
        if ($str == '' || $str == ' ' || $str == NULL) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('ad_advb_cl_add_contacts')):

    /**
     * This function will add contacts in system db for particular contact list
     * @global DB $db The global database object variable.
     * @param type $list_idx An unique list ID.
     * @param type $contacts An array of contact numbers. 
     * The keys against which values are be rpovided is as follows... 
     * phone_number, business_name, first_name, last_name, email, address, city, state, zip, website
     */
    function ad_advb_cl_add_contacts($list_idx, $contacts = array(), $add_type = "Added Manually") {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $single_set_contacts = array();

        //If contacts is an array and contacts is greater than 0
        if (is_array($contacts) && count($contacts) > 0):

            //Looping over contacts
            foreach ($contacts as $single_number_data):

                //Retrieving the phone number from contact data provided in argument.                
                $single_number = isset($single_number_data[0]) ? $single_number_data[0] : '';

                //If single number is blank,
                //Skipping the insert operations
                //ELSE optimizing the number
                if (s8_is_str_blank($single_number)):
                    continue;
                else:
                    $single_number = str_replace(array('+', ' ', '(', ')', '-', "\r"), array('', '', '', '', '', ""), trim($single_number));

                    $single_set_contacts[] = $single_number;

                    //Executing the query to check existence of provided data in system db
                    $result = $db->customQuery("Select count(*) as total from " . AD_ADVB_CL_CONTACTS ." WHERE phone_number = '" . $single_number . "'");

                    if (is_object($result)):

                        //Retrieving the queried info
                        $result_data = $result->fetchAll();

                        $date = new DateTime("now",new DateTimeZone("UTC"));

                        //If contact number in loop 
                        //does not exists already for provided campaign id and contact of
                        if ($result_data[0]['total'] == 0 && !s8_is_str_blank($single_number)){

                            //Querying the db
                            $db->customQuery("INSERT INTO " . AD_ADVB_CL_CONTACTS . " (phone_number,date) VALUES ('$single_number','" . @date( 'Y-m-d H:i:s', strtotime("now")) . "')");

                            $query = $db->customQuery("SELECT idx FROM ad_advb_cl_contacts WHERE phone_number = '$single_number'");
                            $idx = $query->fetch();
                            $idx = $idx['idx'];

                            if($idx!=0)
                                $db->customQuery("INSERT into ad_advb_cl_link (contact_idx,list_idx,date_added,add_type) VALUES ('$idx','$list_idx','".$date->format("Y-m-d H:i:s")."','$add_type')");

                        }else{
                            $query = $db->customQuery("SELECT idx FROM ad_advb_cl_contacts WHERE phone_number = '$single_number'");
                            $idx = $query->fetch();
                            $idx = $idx['idx'];
                            if($idx!=0)
                                $db->customQuery("INSERT into ad_advb_cl_link (contact_idx,list_idx,date_added,add_type) VALUES ('$idx','$list_idx','".$date->format("Y-m-d H:i:s")."','$add_type')");
                        }

                        //Updating contact details
                        ad_advb_cl_update_contact_details(ad_advb_cl_get_contact_idx($list_idx, $single_number), $single_number_data);

                    endif;

                endif;

            endforeach;

            $result = $db->customQuery("SELECT idx FROM ad_ad_campaigns WHERE contacts_list_idx = '".$list_idx."'");

            if (is_object($result)):

                //Retrieving the queried info
                $result_data = $result->fetchAll();

                foreach ($result_data as $campaign) {
                    ad_advb_add_contacts('ad', $campaign['idx'], $single_set_contacts);
                }

            endif;

        endif;
    }

endif;

if (!function_exists('ad_advb_cl_is_contact_exist')):

    /**
     * This function checks whether contact do exist against the provided campaign ID.
     * @global DB $db The global database object variable.
     * @param int $list_idx The unique ID of contact list
     * @param string $contact_number The phone number to check for existence.
     * @return boolean Returns boolean TRUE or FALSE based on check performed.
     */
    function ad_advb_cl_is_contact_exist($list_idx, $contact_number) {
        $output = FALSE;
        //Initializing the response variable. Assuming that contact do not exist
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Executing the query to check existence of provided data in system db
        $result = $db->customQuery("Select COUNT(*) as total from ad_advb_cl_contacts t1 inner join ad_advb_cl_link t2 on t1.idx = t2.contact_idx WHERE t2.list_idx = '$list_idx' AND t1.phone_number = '$contact_number'");

        if (is_object($result)):

            //Retrieving the queried info
            $result_data = $result->fetchAll();

            //If contact number in loop 
            //does exists for provided campaign id and contact of
            if ($result_data[0]['total'] > 0):
                //Setting the response variable to TRUE
                $output = TRUE;
            endif;

        endif;

        //Returning the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_update_contact_details')):

    /**
     * This function will update the contact details of contact number ID provided
     * @global DB $db The global database object variable.
     * @param string $contact_idx The unique contact IDx
     * @param array $details An array containing cotact details against this keys 
     * 1 => business_name, 2 => first_name, 3 => last_name, 4 => email, 
     * 5 => address, 6 => city, 7 => state, 8 => zip, 9 => website.
     * @return boolean Returns TRUE or FALSE based on update operation performed.
     */
    function ad_advb_cl_update_contact_details($contact_idx, $details = array(), $add = true) {
        //Intializing the response variable
        //Assuming that system faield to update data in system
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Defining details key that can be updated
        $details_key = array(
            'business_name',
            'first_name',
            'last_name',
            'email',
            'address',
            'city',
            'state',
            'zip',
            'website'
        );

        //intializing the details data variable
        $details_data = '';

        //Looping over details key
        foreach ($details_key as $key => $single_col_name):
            if($details[$key + 1] !="" || !$add)
                $details_data[] = " " . $single_col_name . " = '" . (isset($details[$key + 1]) ? $details[$key + 1] : "") . "' ";

        endforeach;

        //Querying the db
        $update_status = $db->customQuery("UPDATE " . AD_ADVB_CL_CONTACTS . " SET " . @implode(', ', $details_data) . " WHERE idx = '$contact_idx'");

        //If update operation is successfull
        if ($update_status !== FALSE)
        //Setting the response variable to TRUE
            $output = TRUE;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_contact_idx')):

    /**
     * This function will retrieve the contact index number from system db
     * @global DB $db The global databse object variable
     * @param int $list_idx The unique contact list ID.
     * @param string $contact_number The unique phone number.
     * @return mixed The contact ID ELSE boolean false if contact do not exist.
     */
    function ad_advb_cl_get_contact_idx($list_idx, $contact_number) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("Select t1.idx as idx from ad_advb_cl_contacts t1 inner join ad_advb_cl_link t2 on t1.idx = t2.contact_idx WHERE t2.list_idx = '$list_idx' AND t1.phone_number = '$contact_number'");

        //Returning the response back to caller of this function
        $contacts_data = $result->fetchAll();

        //if contact does exist in system
        //Assigning the contact index number to response variable
        if (isset($contacts_data[0]['idx']))
            $output = $contacts_data[0]['idx'];

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_contact_details')):

    /**
     * This function will retrieve the contact details of contact index provided.
     * @global DB $db The global database object variable.
     * @param int $contact_idx The unique contact index
     * @return mixed Returns boolean FALSE if no data exist else contact details as ARRAY
     */
    function ad_advb_cl_get_contact_details($contact_idx) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("Select * from " . AD_ADVB_CL_CONTACTS . " WHERE idx = '$contact_idx'");

        //If query executes successfully and contains some informations
        if (is_object($result)):

            //Setting the fetch mode as associative
            $result->setFetchMode(PDO::FETCH_ASSOC);

            //Returning the response back to caller of this function
            $contacts_data = $result->fetchAll();

            //if contact does exist in system
            //Assigning the contact number to response variable
            if (isset($contacts_data[0]))
                $output = $contacts_data[0];

            $output['first_name'] = urldecode($output['first_name']);
            $output['last_name'] = urldecode($output['last_name']);
            $output['address'] = urldecode($output['address']);
            $output['business_name'] = urldecode($output['business_name']);
            $output['city'] = urldecode($output['city']);
            $output['state'] = urldecode($output['state']);
            $output['zip'] = urldecode($output['zip']);
            $output['website'] = urldecode($output['website']);
            $output['email'] = urldecode($output['email']);

        endif;

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_list_contacts')):

    /**
     * This function will retrieve the contacts of list.
     * @global DB $db The global variable containing database object.
     * @param int $list_idx The unique contact list ID.
     * @return array An array containing contacts data of provided list ID.
     * Data will be available against this keys 
     * `idx`,`list_idx`,`phone_number`,`business_name`,`first_name`,`last_name`,`email`,
     * `address`,`city`,`state`,`zip`,`website`,`date`.
     */
    function ad_advb_cl_get_list_contacts($list_idx) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = array();

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        // "Select t1.* from ad_advb_cl_contacts t1 inner join ad_advb_cl_link t2 on t1.idx = t2.contact_idx WHERE t2.list_idx = '1'
        $result = $db->customQuery("Select t1.*,t2.date_added as date_added, t2.add_type as add_type from ad_advb_cl_contacts t1 inner join ad_advb_cl_link t2 on t1.idx = t2.contact_idx WHERE t2.list_idx = '$list_idx' ORDER BY t1.idx DESC");

        //If query executed successfully
        if (is_object($result)):

            //Returning the response back to caller of this function
            $output = $result->fetchAll();

        endif;

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_contact_number')):

    /**
     * This function will retrieve the contact number based on contact ID provided.
     * @global DB $db The global database object variable.
     * @param int $contact_idx The unique contact index.
     * @return mixed Boolean FALSE if contact not found ELSE contact number.
     */
    function ad_advb_cl_get_contact_number($contact_idx) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("Select phone_number from " . AD_ADVB_CL_CONTACTS . " WHERE idx = '$contact_idx'");

        //Returning the response back to caller of this function
        $contacts_data = $result->fetchAll();

        //if contact does exist in system
        //Assigning the contact number to response variable
        if (isset($contacts_data[0]['phone_number']))
            $output = $contacts_data[0]['phone_number'];

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_delete_contact_list')):

    /**
     * This function will delete contact list from system db 
     * @global DB $db The global database object variable.
     * @param type $list_idx The unique list index.
     * @return boolean Returns TRUE or FALSE as status of delete operation.
     */
    function ad_advb_cl_delete_contact_list($list_idx) {
        //initializing the response variabe
        //Assuming that system failed to delete contact number in db.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_response = $db->customQuery("DELETE FROM " . AD_ADVB_CONTACT_LISTS . " WHERE idx = '$list_idx'");

        //If query executes successfully
        if ($query_response != FALSE):

            //Deleting list contacts too
            $db->customQuery("DELETE FROM ad_advb_cl_link WHERE list_idx = '$list_idx'");

            //Setting the respons variable to true
            //Because delete operation executed successfully
            $output = TRUE;
        endif;

        //Returning the response back to calle rof this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_delete_contact')):

    /**
     * This function will delete contact from system db of particular contact list
     * @global DB $db The global database object variable.
     * @param type $list_idx The unique list index.
     * @param string $contact_number The number to deleted from contacts list
     * @return boolean Returns TRUE or FALSE as status of delete operation.
     */
    function ad_advb_cl_delete_contact($list_idx,$contact_number) {
        //initializing the response variabe
        //Assuming that system failed to delete contact number in db.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $contact_idx = ad_advb_cl_get_contact_idx($list_idx,$contact_number);

        //Querying the db
        //$query_response = $db->customQuery("DELETE FROM " . AD_ADVB_CL_CONTACTS . " WHERE phone_number = '$contact_number'");
        $query2_response = $db->customQuery("DELETE FROM ad_advb_cl_link WHERE contact_idx = '$contact_idx' AND list_idx = '$list_idx'");

        //If query executes successfully
        if ($query2_response != FALSE):
            //Setting the respons variable to true
            //Because delete operation executed successfully
            $output = TRUE;
        endif;

        //Returning the response back to calle rof this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_list_contacts_count')):

    /**
     * This function will retrieve the number of contacts exist 
     * in a particular contact list inside system.
     * @global DB $db The global variable containing database object.
     * @param int $list_idx The unique contact list ID.
     * @return int The number of contacts counts against provided list ID
     */
    function ad_advb_cl_get_list_contacts_count($list_idx) {
        //initializing the response variabe
        //Assuming that no contacts exists against the provided ID.
        $output = 0;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db for required data
        $resource = $db->customQuery("SELECT count(*) as total FROM ad_advb_cl_link WHERE list_idx = '$list_idx'");

        if (is_object($resource)):

            //Retriving the results
            $results = $resource->fetchAll();

            //Retrieving the final result
            $output = $results[0]['total'];

        endif;

        //Returning the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_add_contacts')):

    /**
     * This function will add contacts in system db for particular campaign and app type
     * @global DB $db The global database object variable.
     * @param string $ad_or_vb Valid values include ad and vb.
     * @param int $campaign_idx The index/unique ID of campaign.
     * @param array $contacts An array of contact numbers.
     */
    function ad_advb_add_contacts($ad_or_vb, $campaign_idx, $contacts = array()) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Switching over first argument value
        switch (strtolower($ad_or_vb)):
            case 'ad':
                $ad_or_vb = AD_ADVB_CONTACT_OF_AUTO_DIALER;
                break;
            case 'vb':
                $ad_or_vb = AD_ADVB_CONTACT_OF_VOICE_BROADCAST;
                break;
        endswitch;

        //If contacts is an array and contacts is greater than 0
        if (is_array($contacts) && count($contacts) > 0):

            //Looping over contacts
            foreach ($contacts as $single_number):

                //If single number is blank,
                //Skipping the insert operations
                //ELSE optimizing the number
                if (s8_is_str_blank($single_number))
                    continue;
                else
                    $single_number = str_replace(array('+', ' ', '(', ')', '-'), array('', '', '', '', ''), trim($single_number));

                $number_details = $db->customQuery("Select opt_out from ad_advb_cl_contacts WHERE phone_number = '" . str_replace("\r", "", $single_number) . "'");
                $number_details = $number_details->fetch();

                if ($number_details['opt_out'] == 0) {

                    //Executing the query to check existence of provided data in system db
                    $result = $db->customQuery("Select count(*) as total from " . AD_ADVB_CONTACTS . " WHERE contact_of = '$ad_or_vb' AND campaign_idx = '$campaign_idx' AND contact_number = '" . str_replace("\r", "", $single_number) . "'");

                    if (is_object($result)):

                        //Retrieving the queried info
                        $result_data = $result->fetchAll();

                        //If contact number in loop 
                        //does not exists already for provided campaign id and contact of
                        if ($result_data[0]['total'] == 0):
                            //Querying the db
                            $db->customQuery("INSERT INTO " . AD_ADVB_CONTACTS . " (contact_of, campaign_idx, contact_number)
                                VALUES ('$ad_or_vb', '$campaign_idx', '$single_number')
                            ");
                        endif;

                    endif;
                }

            endforeach;

        endif;
    }

endif;

if (!function_exists('ad_advb_is_contact_exist')):

    /**
     * This function checks whether contact do exist against the provided campaign ID.
     * @global DB $db The lobal database object variable
     * @param string $ad_or_vb Valid values are "ad" and "vb"
     * @param int $campaign_idx The unique campaign ID.
     * @param string $contact_number The contact number to check
     * @return boolean Returns TRUE or FALSE based on check performed.
     */
    function ad_advb_is_contact_exist($ad_or_vb, $campaign_idx, $contact_number) {
        //Initializing the response variable. Assuming that contact do not exist
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Switching over first argument value
        switch (strtolower($ad_or_vb)):
            case 'ad':
                $ad_or_vb = AD_ADVB_CONTACT_OF_AUTO_DIALER;
                break;
            case 'vb':
                $ad_or_vb = AD_ADVB_CONTACT_OF_VOICE_BROADCAST;
                break;
        endswitch;

        //Executing the query to check existence of provided data in system db
        $result = $db->customQuery("Select count(*) as total from " . AD_ADVB_CONTACTS . " WHERE contact_of = '$ad_or_vb' AND campaign_idx = '$campaign_idx' AND contact_number = '" . $contact_number . "'");

        if (is_object($result)):

            //Retrieving the queried info
            $result_data = $result->fetchAll();

            //If contact number in loop 
            //does exists for provided campaign id and contact of
            if ($result_data[0]['total'] > 0):
                //Setting the response variable to TRUE
                $output = TRUE;
            endif;

        endif;

        //Returning the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_update_contact_details')):

    /**
     * This function will update the contact details of contact number ID provided
     * @global DB $db The global database object variable.
     * @param string $contact_idx The unique contact ID
     * @param array $details An array containing cotact details against
     * this keys 
     * 'first_name', 'last_name', 'email', business_name,
     *  `address`, `city`, `state`, `zip`, `website`.
     */
    function ad_advb_update_contact_details($contact_idx, $details = array()) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Defining details key that can be updated
        $details_key = array(
            'first_name',
            'last_name',
            'email',
            'business_name',
            'address',
            'city',
            'state',
            'zip',
            'website'
        );

        //intializing the details data variable
        $details_data = '';

        //Looping over details key
        foreach ($details_key as $single_col_name):

            //Adding details data
            $details_data[] = " " . $single_col_name . " = '" . (isset($details[$single_col_name]) ? $details[$single_col_name] : "") . "' ";

        endforeach;

        //Querying the db
        $db->customQuery("UPDATE " . AD_ADVB_CL_CONTACTS . " SET " . implode(', ', $details_data) . " WHERE idx = '$contact_idx'");
    }

endif;

if (!function_exists('ad_advb_get_contact_idx')):

    /**
     * This function will retrieve the contact index number from system db
     * @global DB $db The global database object variable
     * @param string $ad_or_vb Valid values are ad (for auto dialer) and vb (for voice broadcast).
     * @param int $campaign_idx The campaign ID to which contact number assigned
     * @param string $contact_number The contact number 
     * of which index has to be retrieved from system db.
     * @return mixed The unique index number of contact or boolean false if contact does not exist.
     */
    function ad_advb_get_contact_idx($contact_number) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("Select idx from ad_advb_cl_contacts WHERE phone_number = '$contact_number'");

        //Returning the response back to caller of this function
        $contacts_data = $result->fetchAll();

        //if contact does exist in system
        //Assigning the contact index number to response variable
        if (isset($contacts_data[0]['idx']))
            $output = $contacts_data[0]['idx'];

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

    function ad_ad_ext_update_pc($phone_code,$ext_idx){
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $res = $db->customQuery("UPDATE ad_advb_contacts SET phone_code = '$phone_code' WHERE idx = '$ext_idx'");

        if($res != FALSE){
            return true;
        }else{
            return false;
        }
    }

    function ad_advb_get_extended_contact_idx($campaign_idx,$contact_number){
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $res = $db->customQuery("SELECT idx FROM ad_advb_contacts WHERE contact_number='$contact_number' AND campaign_idx='$campaign_idx'");

        $data = $res->fetchAll(PDO::FETCH_ASSOC);

        if(isset($data[0])){
            return $data[0]['idx'];
        }else{
            return false;
        }
    }

if (!function_exists('ad_advb_get_contact_details')):

    /**
     * This function will retrieve the contact details of contact index provided.
     * @global DB $db The global database object variable.
     * @param int $contact_idx The unique contact index
     * @return mixed Returns boolean FALSE if no data exist else contact details as ARRAY
     */
    function ad_advb_get_contact_details($contact_idx) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("Select * from ad_advb_cl_contacts WHERE idx = '$contact_idx'");

        //If query executes successfully and contains some informations
        if (is_object($result)):

            //Setting the fetch mode as associative
            $result->setFetchMode(PDO::FETCH_ASSOC);

            //Returning the response back to caller of this function
            $contacts_data = $result->fetchAll();

            //if contact does exist in system
            //Assigning the contact number to response variable
            if (isset($contacts_data[0]))
                $output = $contacts_data[0];

        endif;

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_get_contact_number')):

    /**
     * This function will retrieve the contact number based on contact ID provided.
     * @global DB $db The global database object variable.
     * @param int $contact_idx The unique contact index.
     * @return mixed Boolean FALSE if contact not found ELSE contact number.
     */
    function ad_advb_get_contact_number($contact_idx) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $result = $db->customQuery("Select contact_number from " . AD_ADVB_CONTACTS . " WHERE idx = '$contact_idx'");

        //Returning the response back to caller of this function
        $contacts_data = $result->fetchAll();

        //if contact does exist in system
        //Assigning the contact number to response variable
        if (isset($contacts_data[0]['contact_number']))
            $output = $contacts_data[0]['contact_number'];

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_delete_contact')):

    /**
     * This function will delete contact from system db of particular campaign and app type
     * @global DB $db The global database object variable.
     * @param string $ad_or_vb Valid values include ad and vb.
     * @param int $campaign_idx The index/unique ID of campaign.
     * @param string $contact_number The contact number to delete.
     * @param boolean $output Resturns status of delete operation as boolean TRUE or FALSE.
     */
    function ad_advb_delete_contact($ad_or_vb, $campaign_idx, $contact_number) {
        //initializing the response variabe
        //Assuming that system failed to delete contact number in db.
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Switching over first argument value
        switch (strtolower($ad_or_vb)):
            case 'ad':
                $ad_or_vb = AD_ADVB_CONTACT_OF_AUTO_DIALER;
                break;
            case 'vb':
                $ad_or_vb = AD_ADVB_CONTACT_OF_VOICE_BROADCAST;
                break;
        endswitch;

        //Querying the db
        $query_response = $db->customQuery("DELETE FROM " . AD_ADVB_CONTACTS . " WHERE contact_of = '$ad_or_vb' AND campaign_idx = '$campaign_idx' AND contact_number = '$contact_number'");

        //If query executes successfully
        if ($query_response != FALSE):
            //Setting the respons variable to true
            //Because delete operation executed successfully
            $output = TRUE;
        endif;

        //Returning the response back to calle rof this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_retrieve_contacts')):

    /**
     * This function will retrieve all the pending call contacts of supplied campaign ID
     * @global DB $db The global database object
     * @param type $ad_or_vb Valid values are "AD" for auto dialer and "VB" for voice broadcast.
     * @param type $campaign_idx The numeric and unique campaign ID.
     * @return array An array containing all the pending call numbers
     */
    function ad_advb_retrieve_contacts($ad_or_vb, $campaign_idx) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = array();

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Switching over first argument value
        switch (strtolower($ad_or_vb)):
            case 'ad':
                $ad_or_vb = AD_ADVB_CONTACT_OF_AUTO_DIALER;
                break;
            case 'vb':
                $ad_or_vb = AD_ADVB_CONTACT_OF_VOICE_BROADCAST;
                break;
        endswitch;

        //Querying the db
        $result = $db->customQuery("Select contact_number from " . AD_ADVB_CONTACTS . " WHERE contact_of = '$ad_or_vb' and campaign_idx = '$campaign_idx' AND call_status != 'completed'");

        //Returning the response back to caller of this function
        $contacts_data = $result->fetchAll();

        if (count($contacts_data) > 0):

            //Looping through contacts data.
            foreach ($contacts_data as $single_contact_data):

                //Storing the number in response variable of this function
                $phone_number = urldecode(utf8_decode($single_contact_data['contact_number']));
                $phone_number = str_replace("?", "", $phone_number);
                $phone_number = str_replace(" ", "", $phone_number);
                $phone_number = trim($phone_number);
                $phone_number = str_replace("\r", "", $phone_number);
                $output[] = $phone_number;

            endforeach;

        endif;

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_advb_retrieve_all_contacts')):

    /**
     * This function will retrieve all the contacts of supplied campaign ID
     * @global DB $db The global database object
     * @param type $ad_or_vb Valid values are "AD" for auto dialer and "VB" for voice broadcast.
     * @param type $campaign_idx The numeric and unique campaign ID.
     * @return array An array containing all the numbers
     */
    function ad_advb_retrieve_all_contacts($ad_or_vb, $campaign_idx) {
        //Initializing the response variable
        //Assuming that no contacts exist currently.
        $output = array();

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Switching over first argument value
        switch (strtolower($ad_or_vb)):
            case 'ad':
                $ad_or_vb = AD_ADVB_CONTACT_OF_AUTO_DIALER;
                break;
            case 'vb':
                $ad_or_vb = AD_ADVB_CONTACT_OF_VOICE_BROADCAST;
                break;
        endswitch;

        //Querying the db
        $result = $db->customQuery("Select contact_number from " . AD_ADVB_CONTACTS . " WHERE contact_of = '$ad_or_vb' and campaign_idx = '$campaign_idx'");

        //Returning the response back to caller of this function
        $contacts_data = $result->fetchAll();

        if (count($contacts_data) > 0):

            //Looping through contacts data.
            foreach ($contacts_data as $single_contact_data):

                //Storing the number in response variable of this function
                $output[] = str_replace("\r", "", $single_contact_data['contact_number']);

            endforeach;

        endif;

        //Returnig the response back to caller of this function.
        return $output;
    }

endif;

if (!function_exists('ad_get_user')):

    /**
     * This function will retrieve the username of user_id provided in argument.
     * @global DB $db The global database object variable
     * @param int $user_id The user_id
     * @return string The username associated with user_id stored in system database.
     */
    function ad_get_user($user_id) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Executing the query to retrieve userame from system
        $db_query = $db->customQuery("SELECT username FROM users WHERE idx='$user_id'");

        //Retrieving the data
        $user_data = $db_query->fetchAll();

        //Returning the response back to caller of this function
        return $user_data[0]['username'];
    }

endif;

if (!function_exists('ad_advb_add_note')):

    /**
     * This function will insert the note into the system DB.
     * @global DB $db The global database object variable.
     * @param type $note_details An array containing note details
     * @return boolean Returns boolean TRUE or FALSE based on insert operation performed.
     */
    function ad_advb_add_note($note_details) {
        //Initializing the response variable
        //Assuming that insert operation failed
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_status = $db->customQuery("
                INSERT INTO " . AD_ADVB_NOTES . "
                (note_of, campaign_idx, note, date)
                VALUES
                (
                    '" . $note_details['note_of'] . "',
                    '" . $note_details['campaign_idx'] . "',
                    '" . $note_details['note'] . "',
                    '" . @date( 'Y-m-d H:i:s', strtotime("now")) . "'
                    )
            ");

        //If query executes successfully
        //Setting the response variable to TRUE
        if ($query_status !== FALSE)
            $output = TRUE;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_advb_get_notes')):

    /**
     * This function will retrieve the notes from the system for provided campaign_idx
     * @global DB $db The global databse object variable.
     * @return array Returns an array containing notes data.
     */
    function ad_advb_get_notes($campaign_idx) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_response = $db->customQuery("Select * from " . AD_ADVB_NOTES . " WHERE campaign_idx = '$campaign_idx' ORDER by idx DESC");

        //Returning the response back to caller of this function
        return $query_response->fetchAll();
    }

endif;

if (!function_exists('ad_num_pages')):

    /**
     * This function will calculate number of pages in pagination
     * @param int $total_elements total number of rows
     * @param int $rows_on_single_page Number of rows to show on single page
     * @return int Number of pages in pagination
     */
    function ad_num_pages($total_elements, $rows_on_single_page = 20) {
        //calculating pages and returning the response back to caler of this function
        return ceil($total_elements / $rows_on_single_page);
    }

endif;

if (!function_exists('ad_get_company_phone_codes')):

    /**
     * This function will retrieve the phone codes of company from system
     * @global DB $db The global database object variable
     * @param int $company_id The unique company ID
     * @return array An array containing phone codes.
     */
    function ad_get_company_phone_codes($company_id) {
        //Initializing the response variable
        $output = array();

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_response = $db->customQuery("SELECT * FROM company_phone_code WHERE company_id = '$company_id' AND `type` = 2");

        //Retrieving the queried data
        $queried_data = $query_response->fetchAll();

        return $queried_data;
    }

endif;

if (!function_exists('ad_add_cron_task')):

    /**
     * This function will add the cron task 
     * that will run on a specified time supplied in first argument.
     * @param string $datetime The mysql datetime form Y-m-d H:i:s
     * @param string $url_to_hit The url to be invoked at specified time where cron script resides.
     * @return array An array containing status of insert operation.
     */
    function ad_add_cron_task($datetime, $url_to_hit) {
        //Initializing the curl wrappers
        $ch = curl_init('http://cronapi.web1syndication.com/addOneTimePing');

        //Setting the return transfer option to 1 to get the API response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //Setting the post to true
        curl_setopt($ch, CURLOPT_POST, true);

        //Preparing and setting the post data to send
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('datetime' => $datetime, 'url' => $url_to_hit)));

        //executing the curl, retrieving the status
        $response = curl_exec($ch);

        //error_log("CURL RESPONSE: ".$response);
        //Returning the response back to caller of this function
        return $response != "" ? json_decode($response) : json_encode(array());
    }

endif;

    function ad_del_cron_task($cron_id){
        $ch = curl_init('http://cronapi.web1syndication.com/removeOneTimePing');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('id' => $cron_id)));
        $res = curl_exec($ch);
        return $res;
    }

if (!function_exists('ad_advb_cl_add_note')):

    /**
     * This function will insert the CL note into the system DB.
     * @global DB $db The global database object variable.
     * @param array $note_details An array containing note details against 
     * this keys "contact_idx" and "note".
     * @return boolean Returns boolean TRUE or FALSE based on insert operation performed.
     */
    function ad_advb_cl_add_note($note_details) {
        //Initializing the response variable
        //Assuming that insert operation failed
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_status = $db->customQuery("
                INSERT INTO " . AD_ADVB_CL_NOTES . "
                (contact_idx, note, type, date)
                VALUES
                (
                    '" . $note_details['contact_idx'] . "',
                    '" . $note_details['note'] . "',
                    '" . $note_details['type'] . "',
                    '" . @date( 'Y-m-d H:i:s', strtotime("now")) . "'
                    )
            ");

        //If query executes successfully
        //Setting the response variable to TRUE
        if ($query_status !== FALSE)
            $output = TRUE;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_advb_cl_get_notes')):

    /**
     * This function will retrieve the notes from the system for provided contact_idx
     * @global DB $db The global databse object variable.
     * @return array Returns an array containing notes data.
     */
    function ad_advb_cl_get_notes($contact_idx) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_response = $db->customQuery("Select * from " . AD_ADVB_CL_NOTES . " WHERE contact_idx = '$contact_idx' ORDER by idx DESC");

        //Returning the response back to caller of this function
        return $query_response->fetchAll();
    }

endif;

if (!function_exists('ad_advb_cl_delete_note')):

    /**
     * This function will delte the note from the system.
     * @global DB $db The global variable containing database object.
     * @param int $note_idx The unique note index
     * @return boolean Returns boolean TRUE or FALSE as status of delete operation.
     */
    function ad_advb_cl_delete_note($note_idx) {
        //Initializing the response variable
        //Assuming that system failed to delete note
        $output = false;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Querying the db
        $query_response = $db->customQuery("DELETE from " . AD_ADVB_CL_NOTES . " WHERE idx = '$note_idx'");

        //If query executes successfully
        //Setting the response variable to TRUE
        if ($query_response !== FALSE)
            $output = TRUE;

        //Returning the response back to caller of this function.
        return $output;
    }

endif;