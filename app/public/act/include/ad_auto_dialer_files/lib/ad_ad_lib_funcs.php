<?php

/*
 * This file contains all the auto dialer functions
 */

if (!function_exists('ad_ad_add_campaign')):

    /**
     * This function will add a campaign into the system
     * @global DB $db The global database object perform db operations
     * @param array $_campaign_data An array containing campaign data against db column names
     * @return mixed ID of last inserted row or boolean false on failed query
     */
    function ad_ad_add_campaign($_campaign_data = array()) {
//Intializing the response variable
//Assuming the system failed to add campaign in db
        $output = FALSE;

//Defining the voice bradcast campaigns table column name
        $campaign_columns = array(
            'campaign_name',
            'phone_number',
            'contacts_list_idx',
            'voicemail_messages',
            'voicemail_message_mp3',
            'voicemail_message_mp3_2',
            'voicemail_message_mp3_3',
            'voicemail_message_mp3_4',
            'call_script_tokens',
            'call_script_text',
            'calls_status',
            'progress',
            'last_ran',
            'user',
            'shared'
        );
        $campaign_columns_data = array();

//Looping through campaign column names
        foreach ($campaign_columns as $campaign_col_name):
//if column does not exist in campaign data received via argument
            if (!isset($_campaign_data[$campaign_col_name])):
//setting the null value to it
                $campaign_columns_data[$campaign_col_name] = '';
            else:
//ELSE filtering the value
                $campaign_columns_data[$campaign_col_name] = $_campaign_data[$campaign_col_name];
            endif;
        endforeach;

//Initializing the global database object
        global $db;
//if database object does not exist
        if (!is_object($db)):
//creating a new instance
            $db = new DB();
        endif;

        $res = $db->customQuery("SELECT COUNT(*) as total FROM ad_ad_campaigns WHERE campaign_name = '".$campaign_columns_data['campaign_name']."'");
        $data = $res->fetchAll(PDO::FETCH_ASSOC);

        if($data[0]['total']>0){
            return false;
        }

//Executing the query to insert in db
        $query_status = $db->customQuery("
            INSERT INTO " . AD_AUTO_DIALER_CAMPAIGNS . " (" . implode(',', $campaign_columns) . ")
                VALUES ('" . implode("','", $campaign_columns_data) . "')
        ");

//If query status is not equals to false
        if ($query_status !== FALSE)
            $output = $db->lastInsertId();

//Returning the response back to caller of this function
        return $output;
    }

endif;


if (!function_exists('ad_ad_update_campaign')):

    /**
     * This function will update the campaign of system
     * @global DB $db The global database object perform db operations
     * @param array $_campaign_data An array containing campaign data against db column names
     * @return mixed ID of last inserted row or boolean false on failed query
     */
    function ad_ad_update_campaign($campaign_idx, $_campaign_data = array()) {
        //Intializing the response variable
        //Assuming the system failed to add campaign in db
        $output = FALSE;

        //Defining the voice bradcast campaigns table column name
        $campaign_columns = array(
            'campaign_name',
            'phone_number',
            'voicemail_messages',
            'voicemail_message_mp3',
            'call_script_tokens',
            'call_script_text',
            'shared'
        );
        $campaign_columns_data = array();

        //Looping through campaign column names
        foreach ($campaign_columns as $campaign_col_name):
            //if column does not exist in campaign data received via argument
            if (!isset($_campaign_data[$campaign_col_name])):
                //setting the null value to it
                $campaign_columns_data[$campaign_col_name] = "";
            else:
                //ELSE filtering the value
                $campaign_columns_data[$campaign_col_name] = " " . $campaign_col_name . " = '".addslashes($_campaign_data[$campaign_col_name])."'";
            endif;
        endforeach;

        //Initializing the global database object
        global $db;

        //if database object does not exist
        if (!is_object($db)):

            //creating a new instance
            $db = new DB();
        endif;

        //Executing the query to insert in db
        $query_status = $db->customQuery("
            UPDATE " . AD_AUTO_DIALER_CAMPAIGNS . " SET " . implode(',', $campaign_columns_data) . " WHERE idx = '$campaign_idx'");

        //If query status is not equals to false
        if ($query_status !== FALSE)
            $output = TRUE;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_ad_get_campaigns')):

    /**
     * This function will retrieve the auto dialer campaigns listing from system db
     * @global DB $db The global database object variable
     * @return array AN array containing campaings listing
     */
    function ad_ad_get_campaigns($query = "", $page = 1, $default_rows = 20) {
        session_start();
//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

//calculating the starting row index
        $start = (($page - 1) * 20);

        $add_to_query = "";
        if($_SESSION['permission']<1){
            $add_to_query = " AND user = '".$_SESSION['user_id']."' OR shared = 1 ";
        }

//Querying the db
        $result = $db->customQuery("SELECT * FROM " . AD_AUTO_DIALER_CAMPAIGNS . " WHERE campaign_name LIKE '%$query%' $add_to_query ORDER BY last_ran DESC LIMIT $start, $default_rows");

        $realres = array();

        if(is_array($result) || is_object($result))
        foreach($result->fetchAll() as $res){
            if($res['shared']==1 && $_SESSION['permission']<1 && $res['user']!=$_SESSION['user_id']){
                $result = $db->customQuery("SELECT COUNT(*) as total FROM user_company WHERE user_id = '".$res['user']."' AND company_id IN (SELECT company_id FROM user_company WHERE user_id = '".$_SESSION['user_id']."')");
                $count = $result->fetchAll(PDO::FETCH_ASSOC);
                if($count[0]['total']>0){
                    $realres[] = $res;
                }
            }else{
                $realres[] = $res;
            }
        }

//Returning the response back to caller of this function
        return $realres;
    }

endif;

if (!function_exists('ad_ad_get_campaigns_count')):

    /**
     * This function will retrieve the total number of campiagn that did exist in system db.
     * @global DB $db The global database object variable
     * @return int Returns the count of campaigns as numeric value
     */
    function ad_ad_get_campaigns_count($query = "") {
//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $add_to_query = "";
        if($_SESSION['permission']<1){
            $add_to_query = " AND user = '".$_SESSION['user_id']."' OR shared = 1 ";
        }

//Querying the db
        $result = $db->customQuery("SELECT count(*) as total FROM " . AD_AUTO_DIALER_CAMPAIGNS. " WHERE campaign_name LIKE '%$query%' $add_to_query");


//Retrieving the queried data
        $queried_data = $result->fetchAll();

//Returning the response back to caller of this function.
        return $queried_data[0]['total'];
    }

endif;

if (!function_exists('ad_ad_get_campaign_data')):

    /**
     * This function will retrieve the details of single campaign 
     * using the campaign ID provided in function argument.
     * @global DB $db The global database object variable
     * @return array An array containing campaign data.
     */
    function ad_ad_get_campaign_data($campaign_idx) {
//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

//Querying the db to select the data to be retrieved
        $result = $db->customQuery("Select * from " . AD_AUTO_DIALER_CAMPAIGNS . " WHERE idx = '$campaign_idx'");

//Retrieving the queried data
        $queried_data = $result->fetchAll();

//Returning the response back to caller of this function
        return $queried_data[0];
    }

endif;

if (!function_exists('ad_ad_get_campaign_name')):

    /**
     * This function will retrieve the auto dialer campaign name by ID provided in argument
     * @global DB $db The global database object variable
     * @param int $campaign_id The campaign ID of Voice broadcast
     * @return string The campign name of provided ID if exists ELSE boolean FALSE
     */
    function ad_ad_get_campaign_name($campaign_id) {
//Intializing the response variable
//Assuming that no campaign exists against provided ID
        $output = FALSE;

//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

//Querying the db
        $db_query = $db->customQuery("Select campaign_name from " . AD_AUTO_DIALER_CAMPAIGNS . " WHERE idx = '$campaign_id'");

//Retrieving the data
        $result = $db_query->fetchAll();

//If campaign does exist in system
        if (isset($result[0]['campaign_name'])):

//Storing the campaign name in response variable
            $output = $result[0]['campaign_name'];

        endif;

//Returning the response back to caller of this function
        return $output;
    }

endif;

    function ad_advb_update_pc($callsid, $pc){
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $db->customQuery("UPDATE ad_ad_call_log SET code = '$pc' WHERE CallSid = '$callsid'");
    }

if (!function_exists('ad_vb_add_log')):

    /**
     * This function will add a auto dialer log into the system db.
     * @global DB $db The global database object variable
     * @param array $log_details An arry containing log data
     * @return boolean Returns boolean TRUE or FALSE based on insert operation performed.
     */
    function ad_ad_add_log($log_details) {
//Initializing the response variable
//Assuming that insert operation failed
        $output = FALSE;

//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $res = $db->customQuery("SELECT phone_code FROM ad_advb_contacts WHERE campaign_idx = '".@$log_details['cmp_idx']."' AND contact_number = '".@$log_details['To']."'");
        $pc = $res->fetchAll(PDO::FETCH_ASSOC);

        $phone_code = 0;

        if($pc!=null){
            $phone_code = (int)$pc[0]['phone_code'];
        }

//Querying the db
        $query_status = $db->customQuery("
                INSERT INTO " . AD_AUTO_DIALER_CALL_LOG . "
                (campaign_idx, `date`, `from`, `to`, duration, response, code, user, recording_url, company_id, CallSid)
                VALUES
                (
                    '" . @$log_details['cmp_idx'] . "',
                    '" . @date( 'Y-m-d H:i:s', strtotime("now")) . "',
                    '" . @$log_details['From'] . "',
                    '" . @$log_details['To'] . "',
                    '" . @$log_details['DialCallDuration'] . "',
                    '" . @$log_details['DialCallStatus'] . "',
                    '" . $phone_code . "',
                    '" . @$log_details['user'] . "',
                    '" . @$log_details['RecordingUrl'] . "',
                    '" . @$log_details['company_id'] . "',
                    '" . @$log_details['CallSid'] . "'
                    )
            ");

//If query executes successfully
//Setting the response variable to TRUE
        if ($query_status !== FALSE)
            $output = TRUE;

//Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_ad_get_logs')):

    /**
     * This function will retrieve the auto dialer calls logs from system db
     * @global DB $db The global database object variable
     * @return array An array containing calls logs
     */
    function ad_ad_get_logs($page = 1, $default_rows = 20, $campaign_idx = FALSE, $filter_by = FALSE, $all = false) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //calculating the starting row index
        $start = (($page - 1) * $default_rows);

        //Building the where conditions
        //if it is set in argument
        $where_string = '';
        $where = array();
        if ($campaign_idx !== FALSE)
            $where[] = " campaign_idx = '$campaign_idx' ";

        //If value for filter_by argument is provided
        //Setting the condition to query the filtered data
        if ($filter_by !== FALSE)
            $where[] = " response = '$filter_by' ";

        //Merging where conditions
        if (count($where) > 0)
            $where_string = " WHERE " . implode(' AND ', $where);

        $order = "";

        if (!$all)
            $order = "  LIMIT $start, $default_rows ";

        //Querying the db
        $result = $db->customQuery("Select idx, company_id, CallSid, campaign_idx, `from`, `to`, duration, response, `code`, `user`, `date`, recording_url from ad_ad_call_log " . $where_string . " ORDER BY idx DESC ".$order);

        //Setting the fetch mode as associative
        $result->setFetchMode(PDO::FETCH_ASSOC);

        //Retrieving the queried data
        $realres2 = array();

        if(is_object($result)){
            $add_to_query = "";
            if($_SESSION['permission']<1){
                $add_to_query = " WHERE user = '".$_SESSION['user_id']."' OR shared = 1";
            }
            //Querying the db
            $result2 = $db->customQuery("Select * from " . "ad_ad_campaigns" . "$add_to_query ORDER BY idx DESC");

            $realres = array();

            if(is_array($result2) || is_object($result2))
                foreach($result2->fetchAll() as $res){
                    if($res['shared']==1 && $_SESSION['permission']<1 && $res['user']!=$_SESSION['user_id']){
                        $result3 = $db->customQuery("SELECT COUNT(*) as total FROM user_company WHERE user_id = '".$res['user']."' AND company_id IN (SELECT company_id FROM user_company WHERE user_id = '".$_SESSION['user_id']."')");
                        $count = $result3->fetchAll(PDO::FETCH_ASSOC);
                        if($count[0]['total']>0){
                            $realres[] = $res;
                        }
                    }else{
                        $realres[] = $res;
                    }
                }

            foreach($result->fetchAll() as $res){
                foreach($realres as $res2){
                    if($res2['idx'] == $res['campaign_idx']){
                        $realres2[] = $res;
                    }
                }
            }
        }

        //Returning the response back to caller of this function.
        return $realres2;
    }

endif;

if (!function_exists('ad_ad_get_logs_count')):

    /**
     * This function will retrieve the total number of logs that did exist in system db.
     * @global DB $db The global database object variable
     * @return int Returns the count of logs as numeric value
     */
    function ad_ad_get_logs_count($campaign_idx = FALSE, $filter_by = FALSE) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Building the where conditions
        //if it is set in argument
        $where_string = '';
        $where = array();
        if ($campaign_idx !== FALSE)
            $where[] = " campaign_idx = '$campaign_idx' ";

        //If value for filter_by argument is provided
        //Setting the condition to query the filtered data
        if ($filter_by !== FALSE)
            $where[] = " response = '$filter_by' ";

        //Merging where conditions
        if (count($where) > 0)
            $where_string = " WHERE " . implode(' AND ', $where);

        //Querying the db
        $result = $db->customQuery("SELECT count(*) as total FROM " . AD_AUTO_DIALER_CALL_LOG . " " . $where_string);

        //Retrieving the queried data
        $queried_data = $result->fetchAll();

        //Returning the response back to caller of this function.
        return $queried_data[0]['total'];
    }

endif;

if (!function_exists('ad_ad_get_campaign_all_calls_count')):

    /**
     * This function will retrieve the number of contacts to which call has to be made exist in system
     * @global DB $db The global database object
     * @param int $campaign_idx The unique ID of campaign
     * @return int The number of contacts does exist in system to which call has to be made.
     */
    function ad_ad_get_campaign_all_calls_count($campaign_idx) {
//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

//Querying the db
        $result = $db->customQuery("Select count(*) as total from " . AD_ADVB_CONTACTS . " WHERE contact_of = '" . AD_ADVB_CONTACT_OF_AUTO_DIALER . "' and campaign_idx = '$campaign_idx'");

//retrrieving the queried data
        $output = $result->fetchAll();

//Returning the response back to caller of this function
        return $output[0]['total'];
    }

endif;

if (!function_exists('ad_ad_get_campaign_completed_calls_count')):

    /**
     * This function will retrieve all the completed calls from the system
     * @global DB $db The global database object
     * @param int $campaign_idx The unique campaign ID
     * @return int The number of completed calls in system
     */
    function ad_ad_get_campaign_completed_calls_count($campaign_idx) {
//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

//Querying the db
        $result = $db->customQuery("Select count(*) as total from " . AD_ADVB_CONTACTS . " WHERE contact_of = '" . AD_ADVB_CONTACT_OF_AUTO_DIALER . "' AND campaign_idx = '$campaign_idx' AND call_status = 'completed'");

//retrrieving the queried data
        $output = $result->fetchAll();

//Returning the response back to caller of this function
        return $output[0]['total'];
    }

endif;

if (!function_exists('ad_ad_update_campaign_contact_status')):

    /**
     * This function will update the status of campign contact
     * @global DB $db The global databse object
     * @param int $campaign_idx The unique ID of campaign.
     * @param string $contact_number The contact number of campaign.
     * @param string $status Valid values are "pending" and "completed".
     * @return boolean Returns boolean TRUE or FALSE based on update operation performed.
     */
    function ad_ad_update_campaign_contact_status($campaign_idx, $contact_number, $status) {
//Initializing the response variable
//Assuming that system failed to update campaign progress.
        $output = FALSE;

//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

//Executing the update query
        $query_response = $db->customQuery("UPDATE " . AD_ADVB_CONTACTS . " SET call_status = '$status' WHERE campaign_idx = '$campaign_idx' AND contact_of = '" . AD_ADVB_CONTACT_OF_AUTO_DIALER . "' AND contact_number = '$contact_number'");

//If update query executes successfully, setting the response variable to TRUE
        if ($query_response !== FALSE)
            $output = TRUE;

//Returning the response back to caller of this funtion
        return $output;
    }

endif;

if (!function_exists('ad_ad_update_campaign_progress')):

    /**
     * This function will update the campaign progress in system db
     * @global DB $db The global database object variable
     * @param int $campaign_idx The unique campaign ID.
     * @param string $status Valid values are "running", "completed".
     * @return boolean Returns boolean TRUE or FALSE based on update operation performed.
     */
    function ad_ad_update_campaign_progress($campaign_idx, $status) {
//Initializing the response variable
//Assuming that system failed to update campaign progress.
        $output = FALSE;

//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $campaign_data = ad_ad_get_campaign_data($campaign_idx);

        if ($campaign_data['calls_status'] == "ongoing")
            $status = "ongoing";

//Updating the campaign details
        $query_response = $db->customQuery("UPDATE " . AD_AUTO_DIALER_CAMPAIGNS . " SET calls_status = '$status', last_ran = '" . time() . "' WHERE idx = '$campaign_idx'");

//If query does not execute successfully
        if ($query_response !== FALSE)
            $output = true;

//Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_ad_get_campaign_progress')):

    /**
     * This function will retrieve the campaign progress.
     * @param int $campaign_idx The unique campaign ID.
     * @return string Returns the progress string.
     */
    function ad_ad_get_campaign_progress($campaign_idx) {
        //Retrieving the current campaign calls progress from system db
        return ad_ad_get_campaign_completed_calls_count($campaign_idx) . '/' . ad_ad_get_campaign_all_calls_count($campaign_idx);
    }

endif;

if (!function_exists('ad_ad_get_call_script')):

    /**
     * This function will retrieve the call script of campaign
     * @global DB $db The global database object variable
     * @param int $campaign_idx The unique campaign ID
     * @return string Returns the call script stored for campaign in system. 
     */
    function ad_ad_get_call_script($campaign_idx) {
//Intializing the response variable
//Assuming that no campaign open script against provided ID
        $output = 'No Open Script';

//Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

//Querying the db
        $db_query = $db->customQuery("Select call_script_text from " . AD_AUTO_DIALER_CAMPAIGNS . " WHERE idx = '$campaign_idx'");

//Retrieving the data
        $result = $db_query->fetchAll();

//If campaign does exist in system
        if (isset($result[0]['call_script_text'])):

//Storing the campaign name in response variable
            $output = "<pre>".$result[0]['call_script_text']."<pre>";

        endif;

//Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_ad_get_next_contact_to_call')):

    /**
     * This function will retrieve the next number to call from the system db
     * @param int $campaign_idx The unique campaign ID
     * @return mixed Returns the next number available for call 
     * or boolean false if no next number does exist.
     */
    function ad_ad_get_next_contact_to_call($campaign_idx) {
//Initializing the response variable
//Assuming that no next number does exist in system
        $output = FALSE;

//Retrieving the pending calls contacts list of campaign
        $next_contacts = ad_advb_retrieve_contacts('ad', $campaign_idx);

//If next number to call does exist in system
//Assigning the number to response variable
        if (isset($next_contacts[0]))
            $output = $next_contacts[0];

//Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_ad_dd_add_call_details')):

    /**
     * This function will add the dialled call details into the system
     * @global DB $db The global database object variable
     * @param array $call_details An array containing call details
     * @return boolean Returns TRUE or FALSE based on insert operation performed.
     */
    function ad_ad_dd_add_call_details($call_details) {
        //Initializing the response variable
        //Assuming that insert operation failed
        $output = FALSE;

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Defining the data keys that are going to be manipulated
        $data_keys = array(
            'idx',
            'first_name',
            'last_name',
            'email',
            'phone_number',
            'address',
            'city',
            'state',
            'zip',
            'website',
            'business_name'
        );

        //Intializing the array to used in furthur processing
        $new_call_details = array();

        //Looping over keys to cke if key exist in call details or not to avoid db related error
        foreach ($data_keys as $single_data_key):

            //If keys does not exist in call details
            //we are create it with an empty string
            //ELSE copying it as it is in new call details variable
            if (!isset($call_details[$single_data_key]))
                $new_call_details[$single_data_key] = '';
            else
                $new_call_details[$single_data_key] = $call_details[$single_data_key];

        endforeach;

        //Querying the db
        $query_status = $db->customQuery("
                REPLACE INTO ad_advb_cl_contacts
                (" . implode(',', $data_keys) . ")
                VALUES
                ('" . implode("', '", $new_call_details) . "')
            ");

        //Returning the response back to caller of this function
        return true;
    }

endif;

if (!function_exists('ad_ad_dd_get_call_details')):

    /**
     * This function will retrieve the dialling details of contact index provided in argument.
     * @global DB $db The global database object variable
     * @param int $contact_idx The unique contact index
     * @return mixed An array containing dialling details of contact or boolean FALSE if not found.
     */
    function ad_ad_dd_get_call_details($phone_number) {
        //Initializing the response variable
        $output = array();

        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Setting data to retrieve
        $data_keys = array(
            'first_name',
            'last_name',
            'email',
            'phone_number',
            'address',
            'city',
            'state',
            'zip',
            'website',
            'business_name'
        );

        //Querying the db
        $query = $db->customQuery("SELECT " . implode(', ', $data_keys) . " FROM ad_advb_cl_contacts WHERE phone_number = '$phone_number'");

        //If query contains data
        if (is_object($query)):
            //Setting the format of retrival data
            $query->setFetchMode(PDO::FETCH_ASSOC);

            //Retrieving the data from query
            $queried_data = $query->fetchAll();

            //Storing the queried data in response variable of this function
            $output = $queried_data[0];

        endif;

        //Returning the response back to caller of this function
        return $output;
    }

endif;

if (!function_exists('ad_ad_delete_campaign')):

    /**
     * This function will delete the campaign from system db
     * @param int $campaign_idx The unique and numeric campaign ID.
     */
    function ad_ad_delete_campaign($campaign_idx) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        $db->customQuery("DELETE FROM " . AD_AUTO_DIALER_CAMPAIGNS . " WHERE idx = '$campaign_idx'");
        $db->customQuery("DELETE FROM " . AD_AUTO_DIALER_CALL_LOG . " WHERE campaign_idx = '$campaign_idx'");
        $db->customQuery("DELETE FROM " . AD_ADVB_CONTACTS . " WHERE campaign_idx = '$campaign_idx'");
        $db->customQuery("DELETE FROM " . AD_ADVB_NOTES . " WHERE campaign_idx = '$campaign_idx'");
    }

endif;

if (!function_exists('ad_get_campaign_contact_idx')):

    /**
     * This function will retrieve the contact idx of number.
     * @param int $campaign_idx The unique campaign index
     * @param string $contact_number The contact number
     * @return mixed The unique contact index ELSE boolean false if not found.
     */
    function ad_ad_get_campaign_contact_idx($campaign_idx, $contact_number) {
        return ad_advb_get_contact_idx('ad', $campaign_idx, $contact_number);
    }

endif;

if (!function_exists('ad_ad_get_call_phone_code')):

    /**
     * This function will retrieve the phone_Code of contact_number_idx provided
     * that exist in dialling details table.
     * @global DB $db The global database object variable
     * @param int $contact_number_idx The contact number index
     * @return mixed This will return the phone code stored against call to number of idx supplied
     * ELSE if no call details do not exist or phone code not found.
     */
    function ad_ad_get_call_phone_code($idx) {
        //Global DB object variable handler
        global $db;
        if (!is_object($db)):
            $db = new DB();
        endif;

        //Query the db for data selection
        $query_response = $db->customQuery("Select code FROM ad_ad_call_log WHERE idx = '$idx'");

        //Retrieving the queried data
        $queried_data = $query_response->fetchAll();

        //Returning the response back to caller of this function
        return isset($queried_data[0]['code']) ? $queried_data[0]['code'] : FALSE;
    }

endif;

if (!function_exists('ad_ad_get_current_call_sid')):

    /**
     * This function will retrieve the current call sid.
     * @param string $ad_auto_dialer_files_ABSPATH The absolute path of directory 
     * without trailing slash having current call sid.
     * @return string The current call sid.
     */
    function ad_ad_get_current_call_sid($ad_auto_dialer_files_ABSPATH) {

        //Retriving the current call sid
        $fp = @fopen($ad_auto_dialer_files_ABSPATH . '/CurrentCallSid.txt', 'r+');
        $CurrentCallSid = @fread($fp, '512');
        @fclose($fp);

        //Returning the response back to caller o fthis function
        return $CurrentCallSid;
    }

endif;