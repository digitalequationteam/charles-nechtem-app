<?php

/*
 * This file contains the constants that are going to be used in auot dialer and voice broadcast.
 */

/**
 * Table for auto dialer campaigns
 */
define('AD_AUTO_DIALER_CAMPAIGNS', 'ad_ad_campaigns');

/**
 * Table for auto dialer single call details
 */
define('AD_AUTO_DIALER_CALL_DETAILS', 'ad_ad_call_details'); // TODO: Remove. This was deleted.

/**
 * Table for auto dialer call log
 */
define('AD_AUTO_DIALER_CALL_LOG', 'ad_ad_call_log');

/**
 * Table for voice broadcast campaigns
 */
define('AD_VOICE_BROADCAST_CAMPAIGNS', 'ad_vb_campaigns');

/**
 * Table for voice broadcast campaigns
 */
define('AD_VOICE_BROADCAST_CALL_LOG', 'ad_vb_call_log');

/**
 * Table for auto dialer contacts
 */
define('AD_ADVB_CONTACTS', 'ad_advb_contacts');
define('AD_ADVB_CONTACT_OF_VOICE_BROADCAST', 'voice_broadcast');
define('AD_ADVB_CONTACT_OF_AUTO_DIALER', 'auto_dialer');

/**
 * Table for campaign notes
 */
define('AD_ADVB_NOTES', 'ad_advb_notes');

/**
 * Table for contact lists
 */
define('AD_ADVB_CONTACT_LISTS', 'ad_advb_contact_lists');

/**
 * Table for contacts
 */
define('AD_ADVB_CL_CONTACTS', 'ad_advb_cl_contacts');

/**
 * Table for contacts
 */
define('AD_ADVB_CL_NOTES', 'ad_advb_cl_notes');