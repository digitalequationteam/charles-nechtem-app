<?php
/*
 * This file will print the html containing notes and textarea to add a new one. 
 */

//Retrieving the campaign ID
$contact_idx = $_REQUEST['contact_idx'];
?>
<div style="width: 600px;height: auto;">
    <h2>Notes for "<b><?php echo ad_advb_cl_get_contact_number($contact_idx); ?></b>"</h2>
    <div class="ad_ad_dd_campaign_notes" style="height: 200px; overflow: auto;padding: 10px;">
        <?php
        //Retrieving the campaign notes
        $contact_notes = ad_advb_cl_get_notes($contact_idx);

        //If campign notes is an array and atleast one note does exist in system
        if (is_array($contact_notes) && count($contact_notes) > 0):
            //Looping through each note
            foreach ($contact_notes as $single_note):
                //Printing the note on screen
                $date = Util::convertToLocalTZ($single_note['date'])->format(Util::STANDARD_LOG_DATE_FORMAT);
                echo '<b>' . $date . ': </b>' . $single_note['note'] . '<br/>';
            endforeach;
        endif;
        ?>
    </div>
    <div class="block" style="background: none;">
        <form action="#" id="ad_ad_dd_add_note_form">
            <textarea class="wysiwyg" name="ad_ad_dd_note" cols="48" rows="3"></textarea>
            <div style="clear: both;padding: 5px;"></div>
            <input class="submit mid" style="position: relative; display: inline-block !important; left: 79%;" type="submit" value="Add Note" />
        </form>
    </div>
</div>
<script type="text/javascript">
    $('#ad_ad_dd_add_note_form').submit(function(e) {
        var ad_ad_dd_notes_form_data = $(this).serialize();
        $.post('ad_ajax.php?action=ad_ad_dd_add_note&ad_ad_dd_contact_idx=<?php echo $contact_idx; ?>', ad_ad_dd_notes_form_data, function(response) {
            $('.ad_ad_dd_campaign_notes').html(response);
            $('[name=ad_ad_dd_note]').val(' ');
        });
        e.preventDefault();
    });
</script>