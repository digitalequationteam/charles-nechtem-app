<?php
//Passing thr http 200 ok header to override the 404 error by wp-blog-header.php
header("HTTP/1.1 200 OK");

//Setting the content tyoe of document to XML, So, callers should treat this accordingly
header('Content-type: text/xml');

//Loading the neccessary files
require_once '../util.php';
require_once '../Pagination.php';
require_once '../twilio_header.php';

//Including the library Auto Dialer and Voice Broadcast files
require_once 'lib/ad_lib_funcs.php';

//Initializing the database object
$db = new DB();

// 2.5.1+ Continue to process the SMS broadcast campaign in the background
if(isset($_REQUEST['cmp_type']) && $_REQUEST['cmp_type'] == "sms"){

    if ($_REQUEST['SmsStatus'] == "sent") {
        $_REQUEST['CallStatus'] = "SMS Sent";
        $_REQUEST['RecordingUrl'] = $_REQUEST['Body'];
        //Adding a log entry
        ad_vb_add_log($_REQUEST);

        run_sms();
    }

}else{
    //Setting the APP type
    $_REQUEST['app'] = 'voicebroadcast';

    //Custom log manipulations
    //If call is answered by human
    if ($_REQUEST['AnsweredBy'] == 'human'):
        //Setting the response to Live Answer
        $_REQUEST['CallStatus'] = 'Live Answer';
    //ELSE if call is made to machine
    elseif ($_REQUEST['AnsweredBy'] == 'machine'):
        //Setting the response to voice mail
        $_REQUEST['CallStatus'] = 'Voicemail';
    elseif (isset($_REQUEST['Body'])):
        $_REQUEST['CallStatus'] = "SMS Sent";
        $_REQUEST['RecordingUrl'] = $_REQUEST['Body'];
    endif;

    Global $AccountSid, $AuthToken;

    //Adding a log entry
    ad_vb_add_log($_REQUEST);

//    $fp = fopen('vb_test.txt', 'a+');
//    fwrite($fp, print_r($_REQUEST, true));
//    fclose($fp);
    //If camp_idx param is set in request URL
    if (isset($_REQUEST['cmp_idx'])):

        //Retrieving the required info from request URL
        $campaign_idx = trim($_REQUEST['cmp_idx']);
        $to = trim($_REQUEST['to']);

        //updating the campaign progress and contact status
        ad_vb_update_campaign_contact_status($campaign_idx, $to, 'completed');

        //Total campaign completed calls
        $camp_completed_calls = ad_vb_get_campaign_completed_calls_count($campaign_idx);
        //total calls of campaign
        $camp_all_calls = ad_vb_get_campaign_all_calls_count($campaign_idx);

        //If completedt calls equals to total calls of campaign
        if ($camp_completed_calls >= $camp_all_calls):
            //Setting the campaign status to ompleted
            $status = 'completed';
        else:
            //Else setting the campaign status to running
            $status = 'running';
        endif;

        //Updating the overall campaign progress along with associated details
        ad_vb_update_campaign_progress($campaign_idx, $status);

    endif;
?>
<Response>
    <Hangup/>
</Response><?php
}

function run_sms() {
    $db = new DB();
    global $AccountSid, $AuthToken;

    $campaign_idx = $_REQUEST['cmp_idx'];
    $client = new Services_Twilio($AccountSid, $AuthToken);
    $campaign_details = ad_vb_get_campaign_data($_REQUEST['cmp_idx']);
    $caller_id = $_REQUEST['from'];

    if($campaign_details['calls_status'] == 'paused')
        die('<Response/>');

    $result = $db->customQuery("Select contact_number from " . AD_ADVB_CONTACTS . " WHERE contact_of = 'voice_broadcast' and campaign_idx = '".$_REQUEST['cmp_idx']."' AND call_status != 'completed'");
    $contacts_data = $result->fetchAll(PDO::FETCH_OBJ);

    $to = $contacts_data[0];

    if (empty($to)) {
        ad_vb_update_campaign_progress($campaign_idx, "completed");
        exit();
    }

    $to = str_replace("\r", "", $to->contact_number);
    $opted_out = $db->checkNumberOptedOut($campaign_details['list_id'], $db->format_phone_db($to));

    if(!$opted_out){
        // Switch tokens for real data
        $stmt = $db->customExecute("SELECT * FROM ad_advb_cl_contacts WHERE phone_number = ?");
        $stmt->execute(array($to));
        $contact = $stmt->fetch(PDO::FETCH_OBJ);
        $msg = $campaign_details['sms_message'];
        $msg = str_replace("[FirstName]",$contact->first_name,$msg);
        $msg = str_replace("[LastName]",$contact->last_name,$msg);
        $msg = str_replace("[Email]",$contact->email,$msg);
        $msg = str_replace("[Phone]",$contact->phone_number,$msg);
        $msg = str_replace("[Address]",$contact->address,$msg);
        $msg = str_replace("[City]",$contact->city,$msg);
        $msg = str_replace("[State]",$contact->state,$msg);
        $msg = str_replace("[Zip]",$contact->zip,$msg);
        $msg = str_replace("[Website]",$contact->website,$msg);
        $msg = str_replace("[Business]",$contact->business_name,$msg);

        $list_details = $db->getContactListById($campaign_details['list_id']);
        $msg .= "\n\n".$list_details['sms_opt_out_message'];

        $global_sms_opt_out_message = ($db->getVar("global_sms_opt_out_message")) ? $db->getVar("global_sms_opt_out_message") : "Reply with CANCEL ALL to stop.";

        $msg .= "\n\n".$global_sms_opt_out_message;

        $status = 'completed';
        $continue = false;

        ad_vb_update_campaign_contact_status($campaign_idx, $to, $status);

        try {
            $client->account->messages->sendMessage($caller_id, $to, html_entity_decode($msg), null,
                array(
                    'StatusCallback'=> dirname(s8_get_current_webpage_uri()) . "/ad_status_callback.php?cmp_idx=$campaign_idx&cmp_type=sms&from=". urlencode(trim($caller_id)) ."&to=" . urlencode(trim($to)) . "&userid=" . trim(@$_SESSION['user_id'])
                )
            );
        }
        catch (Exception $e) {
            $continue = true;
        }

        $camp_completed_calls = ad_vb_get_campaign_completed_calls_count($campaign_idx);
        $camp_all_calls = ad_vb_get_campaign_all_calls_count($campaign_idx);

        if($status != 'error'){
            if ($camp_completed_calls >= $camp_all_calls):
                $status = 'completed';
            else:
                $status = 'running';
            endif;
            
            $continue = false;
        }
        ad_vb_update_campaign_progress($campaign_idx, $status);

        if(@$continue===true){
            run_sms();
        }else{
            echo "<Response/>";
        }
    }else{
        ad_vb_add_log(array(
            "cmp_idx" => $campaign_idx,
            "From" => $caller_id,
            "To" => $to,
            "CallDuration" => "0",
            "CallStatus" => "Opted-Out",
            "code" => "",
            "user" => "",
            "RecordingUrl" => ""
        ));

        ad_vb_update_campaign_contact_status($campaign_idx, $to, 'completed');

        $camp_completed_calls = ad_vb_get_campaign_completed_calls_count($campaign_idx);
        $camp_all_calls = ad_vb_get_campaign_all_calls_count($campaign_idx);

        if ($camp_completed_calls >= $camp_all_calls):
            $status = 'completed';
        else:
            $status = 'running';
        endif;

        ad_vb_update_campaign_progress($campaign_idx, $status);
        if($status == 'running')
            run_sms();
    }
}