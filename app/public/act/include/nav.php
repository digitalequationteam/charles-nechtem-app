<?php
global $lcl;

if (isset($_SESSION['user_id']) && isset($_SESSION['sel_co']) && $lcl>=2 && $page != 'ad_ad_dialing_details') {
    if($_SESSION['prems']['dialer'] || $_SESSION['permission'] > 1) {
    ?>


<audio id="phoneRing" loop>
  <source src="mp3/telephone-ring-04.mp3" type="audio/mp3" />
Your browser does not support the audio element.
</audio>

<?php if (isset($is_popout_dialer)) { ?>
    <div style="position: absolute; left: 5px; top: 5px; z-index: 9999999;">
        <a href="javascript: void(0);" onclick="window.close();"><img src="images/delete.gif" /></a>
    </div>
<?php } ?>

<div id="dialer" class="open">
    <input type='hidden' id='divstatus' value="">
    <input type='hidden' id='callOptionsInput' value="">

    <?php if (!isset($is_popout_dialer)) { ?>
        <div class="client-ui-tab open">

            <div class="client-ui-bg-overlay"><!-- leave me alone! --></div>
            <div class="client-ui-inset">
                <div id="client-ui-tab-status">
                    <div class="client-ui-tab-wedge">
                        <img src="images/callmaker-phone-ico.png" class="callmaker-phone-ico" />
                        <a class="callmaker" href="#dialer"><span class="symbol"></span> Hide</a>
                        <input class="dialer_popout_icon" type="image" src="images/popout_icon.png" onclick="openPopoutDialer(this);" style="position: absolute; margin-left: 70px; margin-top: 5px;" />
                    </div>
                    <div class="client-ui-tab-status-inner">
                        <div class="mic"></div>
                        <h3 class="client-ui-timer">00:00</h3>
                    </div><!-- .client-ui-tab-status-inner -->
                </div><!-- #client-ui-tab-status -->
            </div><!-- #client-ui-tab-inset -->

        </div><!-- .client-ui-tab .open -->
    <?php } ?>

    <div id="client-calls-queue">
        <h2>Incoming Calls</h2>

        <div id="client-calls-list"></div>
    </div>

    <div class="client-ui-content">
        <div class="client-ui-bg-overlay"><!-- leave me alone! --></div>
        <div class="client-ui-inset">

            <div id="client-make-call">
                <form id="make-call-form" action="" method="post">

                    <div id="call-options">
                        <div id="call-options-summary">
                            <span id="summary-call-using" class="browser"></span>
                            <span id="summary-caller-id">Caller ID: <span class="device-number">None</span></span>
                            <span id="summary-call-toggle"><img src="images/caller_id_dd.png" width="12" height="11" /></span>
                        </div>

                        <div id="call-options-inputs" style="display: none; color: #FFFFFF;">
                            <label class="field-label" style="display: inline-block; padding-left: 10px;">Caller ID
                                <select name="browserphone_caller_id" id="caller-id-phone-number">
                                    <?php
                                    $numbers = $db->getCompanyNumIntl($_SESSION['sel_co']);
                                    //echo "<pre>";print_r($numbers);
                                    for ($i = 0; $i <= count($numbers) - 1; $i++) {
                                        $number = $numbers[$i];
                                        if(!$number[1])
                                            $num = "1" . $number[0];
                                        else
                                            $num = $number[0];
                                        ?>
                                        <option value="<?php echo $num; ?>"><?php echo "+" . $num; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </label><br/>
                            <label class="field-label" style="display: inline-block; padding-left: 10px;width: 93%;">Ring Volume
                                <div id="volumeslider" style="width: 205px; display: inline-block;"></div>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        if(getCookie2("callVolume")=="" || typeof getCookie2("callVolume") === "undefined")
                                        {
                                            setCookie("callVolume",50,1);
                                            document.getElementById("phoneRing").volume = 1;
                                        }
                                        document.getElementById("phoneRing").volume = (getCookie2("callVolume") / 100);
                                        $("#volumeslider").slider({
                                            value : getCookie2("callVolume"),
                                            step  : 1,
                                            range : 'min',
                                            min   : 0,
                                            max   : 100,
                                            slide : function(){
                                                var value = $("#volumeslider").slider("value");
                                                setCookie("callVolume",value,1);
                                                document.getElementById("phoneRing").volume = (value / 100);
                                            }
                                        });
                                    });
                                </script>
                            </label>
                        </div>

                    </div>

                    <?php $status = $db->getUserStatus($_SESSION['user_id']); ?>
                    <div style="clear: both; color: #FFFFFF; padding: 13px; font-size: 12px; padding-top: 0px; padding-bottom: 10px;">
                        <label style="display: inline-block; width: 98px; padding-right: 3px; text-align: right;">User:</label> <span class="user-name"><?php echo $_SESSION['handle']; ?></span><br />
                        <label style="display: inline-block; width: 98px; padding-right: 3px; text-align: right;">Company:</label> <span class="company-name"><?php echo $_SESSION['sel_co_name']; ?></span><br />
                        <label style="display: inline-block; width: 98px; padding-right: 3px; text-align: right;">Status:</label>
                        <span class="user-status-wrapper">
                            <select id="user-status" style="font-size: 12px;" onchange="changeUserStatus(this);">
                                <option value="ONLINE" <?php if ($status == "ONLINE") { ?>selected="selected"<?php } ?>>Online</option>
                                <option value="AWAY" <?php if ($status == "AWAY") { ?>selected="selected"<?php } ?>>Away</option>
                                <option value="BUSY" <?php if ($status == "BUSY") { ?>selected="selected"<?php } ?>>Busy</option>
                                <option value="OFFLINE" <?php if ($status == "OFFLINE") { ?>selected="selected"<?php } ?>>Offline</option>
                            </select>
                        </span>
                        <br />
                    </div>

                    <fieldset>
                        <input style="float: left;" type="text" name="dial_phone_number" value="" id="dial-phone-number" />
                    </fieldset>
                </form><!-- #make-call-form -->

            </div><!-- #client-make-call -->

            <div id="client-on-call" style="display: block;">
                <div id="client-ui-status" class="clearfix" style="display:none;">
                            <h2 id="client-ui-message">Ready</h2>
                            <h3 class="client-ui-timer" style="float:left;margin-left:87px;width:33px;">
                                <div id="minutes"></div>
                            <h3>
                            <h3 class="client-ui-timer" style="float:left; margin-left:10px; width:12px; margin-top: 13px !important;">:</h3>
                            <h3 class="client-ui-timer" style="float:left;width:50px;"><div id="seconds"></div><h3>

                            <div class="conference_details" style="clear: both; color: #FFFFFF; padding: 13px; font-size: 12px; padding-bottom: 0px;">
                                <span id="client-from-wrapper" style="display: none;"><label style="display: inline-block; width: 85px; padding-right: 12px; text-align: right; height: 20px; vertical-align: middle;">From:</label> <span class="call-from" style="height: 20px; display: inline-block; vertical-align: middle;"></span><br /></span>
                                <span id="client-to-wrapper" style="display: none;"><label style="display: inline-block; width: 85px; padding-right: 12px; text-align: right; height: 20px; vertical-align: middle;">To:</label> <span class="call-to" style="height: 20px; display: inline-block; vertical-align: middle;"></span><br /></span>
                                <span id="client-participants-wrapper" style="display: none;"><label style="display: inline-block; width: 85px; padding-right: 12px; text-align: right; height: 20px; vertical-align: middle;">Participants:</label> <span class="call-participants" style="height: 20px; display: inline-block; vertical-align: middle;"></span><br /></span>
                            </div>
                            <input type=hidden id="TimeStatus" value="">

                                <?php
                                if($_SESSION['permission']>=1 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])){
                                ?>
                                <div id="client-ui-phonecode" class="clearfix" style="display: none; margin-top: 5px; margin-left: 0px; clear: both; text-align: center; padding-top: 5px; padding-bottom: 10px;">
                                    <select id="client-phonecode" style="font-size: 12px;">
                                        <option value="0">Select a Phone Code</option>
                                        <?php
                                        if(is_array($_SESSION['sel_co_pc']['codes']) || is_object($_SESSION['sel_co_pc']['codes']))
                                            foreach(@$_SESSION['sel_co_pc']['codes'] as $phone_code){
                                                ?><option value="<?php echo $phone_code->idx;?>"><?php echo $phone_code->name;?></option><?php
                                            }
                                        ?>
                                    </select>
                                </div>
                                <?php
                                }
                                ?>

                                <div id="client-ui-phonecode-incoming" class="clearfix" style="display: none; margin-top: 5px; margin-left: 0px; clear: both; text-align: center; padding-top: 5px; padding-bottom: 10px;">
                                    <select id="client-phonecode-incoming" style="font-size: 12px;">
                                        <option value="0">Select a Phone Code</option>
                                        <?php
                                        $phone_codes = $db->getPhoneCodes($_SESSION['sel_co']);
                                        if(is_array($phone_codes) || is_object($phone_codes))
                                            foreach(@$phone_codes as $phone_code){
                                                ?><option value="<?php echo $phone_code->idx;?>"><?php echo $phone_code->name;?></option><?php
                                            }
                                        ?>
                                    </select>
                                </div>

                        </div>
                        <!-- #client-ui-status -->
                <div id="client-ui-pad" class="clearfix">
                    <div class="client-ui-button-row">
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">1</div>
                            <div class="client-ui-button-letters"></div>
                        </div>
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">2</div>
                            <div class="client-ui-button-letters">abc</div>
                        </div>
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">3</div>
                            <div class="client-ui-button-letters">def</div>
                        </div>
                    </div>
                    <div class="client-ui-button-row">
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">4</div>
                            <div class="client-ui-button-letters">ghi</div>
                        </div>
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">5</div>
                            <div class="client-ui-button-letters">jkl</div>
                        </div>
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">6</div>
                            <div class="client-ui-button-letters">mno</div>
                        </div>
                    </div>
                    <div class="client-ui-button-row">
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">7</div>
                            <div class="client-ui-button-letters">pqrs</div>
                        </div>
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">8</div>
                            <div class="client-ui-button-letters">tuv</div>
                        </div>
                        <div class="client-ui-button">
                            <div class="client-ui-button-number">9</div>
                            <div class="client-ui-button-letters">wxyz</div>
                        </div>
                    </div>
                    <div class="client-ui-button-row">
                        <div class="client-ui-button">
                            <div class="client-ui-button-number asterisk" style="margin-top: 3px;">*</div>
                            <!--<div class="client-ui-button-letters"></div>-->
                        </div>
                        <div class="client-ui-button">
                            <div class="client-ui-button-number" style="margin-top: -2px;">0</div>
                            <!--<div class="client-ui-button-letters"></div>-->
                        </div>
                        <div class="client-ui-button">
                            <div class="client-ui-button-number" style="margin-top: -2px;">#</div>
                            <!--<div class="client-ui-button-letters"></div>-->
                        </div>
                    </div>
                    <div class="client-ui-button-row">
                        <div class="client-ui-button hold-btn">
                            <div class="tooltipster" style="display: none;" title="Put on hold." onclick="putCallOnHold();">
                                <div class="client-ui-button-number">
                                    <img src="images/icons/hold.png" />
                                </div>
                                <div class="client-ui-button-letters" style="margin-left:-4px;">Hold</div>
                            </div>
                        </div>
                        <div class="client-ui-button clear-ui-button-data">
                            <div class="client-ui-button-number" style="font-size: 20px;">C</div>
                            <div class="client-ui-button-letters" style="margin-left:-4px;">Clear</div>
                        </div>
                        <?php
                        $mutted = (@$_SESSION['browsersoftphone_muted']) ? "yes" : "no";
                        ?>
                        <div class="client-ui-button mute-btn" data-mutted="<?php echo $mutted; ?>" onclick="toggleDialerMute();">
                            <div class="client-ui-button-number">
                                <div class="muted" <?php if ($mutted == "no") { ?>style="display: none;"<?php } ?>>
                                    <img src="images/icons/forbidden.png" style="position: absolute; margin-top: 11px; margin-left: 7px; width: 9px; height: 9px;" />
                                    <img src="images/icons/microphone.png" />
                                </div>
                                <div class="unmuted" <?php if ($mutted == "yes") { ?>style="display: none;"<?php } ?>>
                                    <img src="images/icons/microphone.png" />
                                </div>
                            </div>
                            <div class="client-ui-button-letters" style="margin-left:-4px;"><?php if ($mutted == "no") { ?>Mute<?php } else { ?>Unmute<?php } ?></div>
                        </div>
                    </div>
                </div><!-- /client-ui-pad -->

                <div id="client-make-call" style="text-align: center; padding-top: 4px;">
                    <input type="image" src="images/cd_dial_btn.png" id="dial-input-button" name="dial_input_button" style="display: inline-block !important;" />
                </div>
                <div id="client-ui-actions" style="text-align: center; padding-top: 4px; display:none;">
                    <input type="image" src="images/cd_hangup_btn.png" id="client-ui-hangup" style="display: inline-block !important;" />
                </div><!-- #client-ui-actions -->           </div><!-- #client-on-call -->
                <div id="client-lists" style="padding-left: 5px; width: 320px; font-size: 12px;">
                    <div id="client-lists-toggle" style="height: 50px; margin: 0 auto; display: block;">
                        <div class="client-list-calls on flash" id="client-list-calls" onclick="clientListToggle(this);" style="width: 125px; font-size: 15px;">Calls</div>
                        <div class="client-list-agent-transfer" id="client-list-agent-transfer" onclick="clientListToggle(this)" style="font-size: 15px;">Agents</div>
                    </div>
                    <div id="client-list-calls-list">
                        <span class="incoming-calls-label">Incoming</span>
                        <div id="client-calls-list">No calls.</div>
                        <div id="client-ongoing-wrapper" style="display: none;">
                            <span class="ongoing-calls-label">Ongoing</span>
                            <div id="client-calls-ongoing-list">No calls.</div>
                        </div>
                        <div id="client-missed-wrapper" style="display: none;">
                            <span class="missed-calls-label">Missed</span>
                            <div id="client-missed-calls-list" style="height: 125px; overflow-y: auto;">No calls.</div>
                        </div>
                    </div>
                    <div id="client-list-agents" style="display: none;">
                        <div id="client-add-external-number-wrapper" style="display: none;">
                            <div style="float: left; padding-right: 5px; padding-left: 30px;">
                                <input type="text" id="client-add-external-number-input" style="padding: 2px;" />
                            </div>
                            <div id="client-add-external-number" style="float: left;" onclick="addPhoneNumberToCall();">Add Number to Call</div>
                            <div style='clear: both;'></div>
                        </div>
                        <span class="online-agents-label">Agents</span>
                        <div id="client-agents-list">No agents are online at this time.</div>
                    </div>
                </div>
        </div>
    </div>

</div>
<?php
    }
}
?>

<?php
echo "<!-- lcl: $lcl -->";
$logoURL = "";
if($db->getVar("company_logo"))
    $logoURL = $db->getVar("company_logo");

?>
<div id="report_logo"><?php if($logoURL!="") echo "<img src=\"".$logoURL."\" />"; ?></div>


<div id="header">
    <div class="hdrl"></div>
    <div class="hdrr"></div>

    <h1><a href="index.php">Call Tracking</a><span class="header-version">&nbsp;v<?php echo $VERSION; echo $lcl == 2 ? " E" : ""; echo @$INDEV == true ? " [DEV]":"";?></span></h1>

    <ul id="nav">
        <?php if(@$_SESSION['permission'] >= 1) { ?>
        <li class="<?php if($page=="adminpage") echo(" active") ?>"><a href="#">Admin</a>
            <ul class="nav-admin">
                <li><a href="manage_companies.php">Manage Companies</a></li>
                <li><a href="manage_users.php">Manage Users</a></li>
                <li><a href="manage_email.php">Manage Email</a></li>
                <li><a href="setup_phone_numbers.php">Manage Phone Numbers</a></li>
                <?php if($lcl>=2) { ?>
                    <li><a href="#">Billing &nbsp;&raquo;</a>
                        <ul class="nav-billing">
                            <li><a href="manage_billing.php">Manage Billing</a></li>
                            <li><a href="email_tracking_setup.php">Manage Email Tracking</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Keyword Level Tracking &nbsp;&raquo;</a>
                        <ul class="nav-tracking">
                            <li><a href="keyword_locked_number.php">Keyword Locked Number</a></li>
                            <li><a href="keyword_generated_call_report.php">Keyword Generated Call Report</a></li>
                            <!--<li><a href="keyword_report.php">Keyword Report</a></li>-->
                            <li><a href="pool_number.php">Pooling Numbers</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <li><a href="act_settings.php">ACT Settings</a></li>
            </ul>
        </li>
        <li<?php if($page=="adminpage") echo(" class=\"active\"") ?>><a href="#">Manage</a>
            <ul class="nav-manage">
                <li><a href="manage_companies.php">Manage Call Flow</a></li>
                <li><a href="setup_phone_numbers.php"onclick="checkIfCompanySelected(this, <?php echo (isset($_SESSION['sel_co']) ? 1 : 0); ?>); return false;">Manage Phone Numbers</a></li>
            </ul>
        </li>
        <?php }
        else {
            if ($_SESSION['prems']['purchase_numbers'] || $_SESSION['prems']['customize_call_flow']) {
                ?>
        <li<?php if($page=="adminpage") echo(" class=\"active\"") ?>><a href="#">Manage</a>
                        <ul class="nav-manage">
                            <?php if ($_SESSION['prems']['customize_call_flow']) { ?><li><a href="manage_companies.php">Manage Call Flow</a></li><?php } ?>
                            <?php if ($_SESSION['prems']['purchase_numbers']) { ?><li><a href="setup_phone_numbers.php"onclick="checkIfCompanySelected(this, <?php echo (isset($_SESSION['sel_co']) ? 1 : 0); ?>); return false;">Manage Phone Numbers</a></li><?php } ?>
                        </ul>
                    </li>
                <?php
            }
        } ?>
        <li<?php if($page=="index") echo(" class=\"active\"") ?>><a href="index.php">Dashboard</a>
            <ul>
                <li><a href="index.php" onclick="checkIfCompanySelected(this, <?php echo (isset($_SESSION['sel_co']) ? 1 : 0); ?>); return false;">Incoming Calls</a></li>
                <?php if($_SESSION['permission']>=1 || !$db->isUserOutboundLinksDisabled($_SESSION['user_id'])){ ?>
                <li><a href="index.php?out=1" onclick="checkIfCompanySelected(this, <?php echo (isset($_SESSION['sel_co']) ? 1 : 0); ?>); return false;">Outgoing Calls</a></li>
                <?php } ?>
            </ul>
        </li>
        <li<?php if($page=="companies") echo(" class=\"active\"") ?>><a href="companies.php">Companies</a></li>
        <li<?php if(substr($page,0,11)=="call_report") echo(" class=\"active\"") ?>><a href="call_report.php">Call Reports</a>
            <ul>
                <?php if(!$_SESSION['prems']['outgoing_disabled'] || $_SESSION['permission']>=1) {?><li><a href="call_report.php">Incoming Call Report</a></li><?php } ?>
                <?php if(!$_SESSION['prems']['outgoing_disabled'] || $_SESSION['permission']>=1) {?><li><a href="call_report_outgoing.php">Outgoing Call Report</a></li><?php } ?>
            </ul>
        </li>
                                            <?php if($lcl>=2) {
                                                if((@$_SESSION['permission']>=1) || (@$_SESSION['permission']<1 && ($_SESSION['prems']['ad'] ||
                                                    $_SESSION['prems']['vb'] || $_SESSION['prems']['mc']))){
                                            ?>
                                            <li<?php if (substr($page,0,3)=="ad_") echo(" class=\"active\"") ?>><a href="#">Advanced</a>
                                                <ul>
                                                    <?php if($_SESSION['prems']['ad'] || $_SESSION['permission']>=1) {?><li><a href="ad_ad_campaigns.php" onclick="checkIfCompanySelected(this, <?php echo (isset($_SESSION['sel_co']) ? 1 : 0); ?>); return false;">Auto Dialer</a></li><?php } ?>
                                                    <?php if($_SESSION['prems']['vb'] || $_SESSION['permission']>=1) {?><li><a href="ad_vb_campaigns.php" onclick="checkIfCompanySelected(this, <?php echo (isset($_SESSION['sel_co']) ? 1 : 0); ?>); return false;">Broadcasts</a></li><?php } ?>
                                                    <?php if($_SESSION['prems']['mc'] || $_SESSION['permission']>=1) {?><li><a href="ad_contactlist_log.php" onclick="checkIfCompanySelected(this, <?php echo (isset($_SESSION['sel_co']) ? 1 : 0); ?>); return false;">Manage Contacts</a></li><?php } ?>
                                                </ul>
                                            </li>
                                            <?php }
                                                } ?>

    </ul>

    <p class="user">Hello, <a href="#" id="USR_CHANGE_PASSWORD"><?php echo @$_SESSION['username']; ?></a><?php if($_SESSION['permission']>=1) { ?> | <a href="http://help.analyticcalltracking.com/" target="_BLANK">Help</a><?php }?> | <a href="index.php?op=logout">Logout</a></p>
</div>

<?php
// Check for updates
if($update_needed=="1" && $_SESSION['permission'] > 1){ ?>
<div class="block" id="upd_mes" style="background: none; margin-bottom: 10px; display: none;">
    <div class="message info" style="width: 903px; margin: 0px 0px 11px; display: block;"><p><a href="update-core.php">There is an update available. Click here to go to the update page.</a></p><span onclick="updateDismiss(this);" class="close" title="Dismiss and don't show me for 1 day."></span></div>
</div>
<?php } ?>
