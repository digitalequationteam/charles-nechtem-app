<?php
session_start();
require_once('util.php');

//Check if user is admin
if($_SESSION['permission'] < 1)
{
    ?>
<script type="text/javascript">
    alert('An error has occured.');
    window.location = "index.php";
</script>
<?php
    exit;
}

$db = new DB();
$companies = $db->getAllCompanies();

if (@$_GET['company'] != "" && (@$_GET['s_date'] == "" || @$_GET['e_date'] == "")) {
    ?>
<script type="text/javascript">
    alert('An error has occured. Please contact support.');
</script>
<?php
    exit;
}

global $AccountSid, $AuthToken;
global $TIMEZONE;

//Declare variables
$start_date = $_GET['s_date'];
$end_date = $_GET['e_date'];
$company = $_GET['company'];

if($company != -1)
    $comapny_name = $db->getCompanyName($company);
else
    $company_name = "All";

$data_array = array();
if($company != -1)
    $numbers = $db->getCompanyNumIntl($company);
else
    $numbers = $db->getAllCompanyNum();

$start_date = ($start_date/1000);
$end_date = ($end_date/1000)+71999;


$start_date = new DateTime("@".$start_date,new DateTimeZone($TIMEZONE));
$end_date = new DateTime("@".$end_date,new DateTimeZone($TIMEZONE));
$start_date->setTimezone(new DateTimeZone("UTC"));
$start_date->modify("-1 day");
$end_date->setTimezone(new DateTimeZone("UTC"));
$end_date->modify("+1 day");

$search_start_date = $start_date->format("Y-m-d");
$search_end_date = $end_date->format("Y-m-d");

$start_date->modify("+1 day");
$end_date->modify("-1 day");
$start_date->setTime(0,0,0);
$end_date->setTime(23,59,59);
$start_date->setTimezone(new DateTimeZone("UTC"));
$end_date->setTimezone(new DateTimeZone("UTC"));
$sd_ts = $start_date->format("U");
$ed_ts = $end_date->format("U");

$calls = array();

$client = new Services_Twilio($AccountSid, $AuthToken);
foreach ($client->account->calls->getIterator(0, 50, array(
    'StartTime>' => $search_start_date,
    'StartTime<' => $search_end_date
)) as $call) {
    $call_date = DateTime::createFromFormat(DateTime::RFC2822, $call->date_created);
    $ts = $call_date->format("U");
    if($ts >= $sd_ts && $ts<=$ed_ts){
        $calls[] = $call;
    }
}


$total_company_seconds = 0;
$total_company_price = 0.00000;

foreach($numbers as $number)
{
    $call_count = 0;
    $accum_seconds_inc = 0;
    $accum_seconds_out = 0;
    $accum_price_inc = (float)0.00000;
    $accum_price_out = (float)0.00000;

    if(!$number[1])
        $number = "+1".$number[0];
    else
        $number = "+".$number[0];

    foreach($calls as $call)
    {
        if($number == $call->to)
        {
            $accum_seconds_inc += ceil($call->duration/60)*60;
            $accum_price_inc = bcadd($accum_price_inc, abs((float)$call->price), 5);
            $call_count++;
        }elseif($number == $call->from){
            $accum_seconds_out += ceil($call->duration/60)*60;
            $accum_price_out = bcadd($accum_price_out, abs((float)$call->price), 5);
        }
    }
    $total_company_price = bcadd($total_company_price, $accum_price_inc, 5);
    $total_company_price = bcadd($total_company_price, $accum_price_out, 5);
    $total_company_seconds += ceil($accum_seconds_inc/60)*60;
    $total_company_seconds += ceil($accum_seconds_out/60)*60;

    $data_array[$number.",(incoming)"] = array("price"=>number_format($accum_price_inc,2),"calls"=>$call_count,"seconds"=>ceil($accum_seconds_inc/60)*60  );
    $data_array[$number.",(outgoing)"] = array("price"=>number_format($accum_price_out,2),"calls"=>$call_count,"seconds"=>ceil($accum_seconds_out/60)*60  );
}
if($company_name == "")
    $company_name = "All";
//CSV print
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=" . $comapny_name."_Company_Report_" . time() . ".csv");
header("Pragma: no-cache");
header("Expires: 0");
echo "Number, Type, Minutes, Twilio Charge\r\n";
foreach($data_array as $key => $row)
{
    echo $key.",".sec2hms($row["seconds"]).",$".$row["price"]."\r\n";
}
echo ",,\r\n";
if($total_company_price==0)
    $total_company_price = '0.00';
echo "Total:,".sec2hms($total_company_seconds).",$".$total_company_price;

function sec2hms ($sec, $padHours = false) {
    $hms = "";
    $hours = intval(intval($sec) / 3600);
    $hms .= ($padHours)
        ? str_pad($hours, 2, "0", STR_PAD_LEFT). ':'
        : $hours. ':';
    $minutes = intval(($sec / 60) % 60);
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ':';
    $seconds = intval($sec % 60);
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);
    return $hms;
}


?>