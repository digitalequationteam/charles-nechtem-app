<?php
//Initiaizing the session
session_start();

//Defining the name of page
$page = "pool_number";

//Including necessary files
require_once 'include/util.php';
require_once 'include/twilio_header.php';

//Initializing the DB object
$db = new DB();
global $RECORDINGS;

if (!empty($_REQUEST['RecordingUrl'])) {
    $con = mysqli_connect($DB_HOST,$DB_USER,$DB_PASS);
    mysqli_select_db($con, $DB_NAME );

    if (isset($_REQUEST['wId']))
        mysqli_query($con, "UPDATE call_ivr_widget SET content_type = 'RECORD_AUDIO', content = '".addslashes($_REQUEST['RecordingUrl'])."' WHERE wId = '".addslashes($_REQUEST['wId'])."'");

    if (isset($_REQUEST['companyId']))
        mysqli_query($con, "UPDATE companies SET voicemail_type = 'RECORD_AUDIO', voicemail_content = '".addslashes($_REQUEST['RecordingUrl'])."' WHERE idx = '".addslashes($_REQUEST['companyId'])."'");

    if (isset($_REQUEST['recordingId']))
        mysqli_query($con, "REPLACE INTO recordings(unique_hash, value) VALUES('".addslashes($_REQUEST['recordingId'])."','".addslashes($_REQUEST['RecordingUrl'])."')");
    ?>

    <Response>
        <Say>Thanks for your recording.</Say>
        <Say>Goodbye</Say>
    </Response>
    <?php
    exit();
}

Global $AccountSid, $AuthToken;
$client = new Services_Twilio($AccountSid, $AuthToken);

$recordingId = (isset($_REQUEST['recordingId']) ? $_REQUEST['recordingId'] : "");
$companyId = (isset($_REQUEST['companyId']) ? $_REQUEST['companyId'] : "");
$wId = (isset($_REQUEST['wId']) ? $_REQUEST['wId'] : "");

$action_url = dirname(s8_get_current_webpage_uri()) . "/include/twilio_record_audio.php?recordingId=$recordingId&companyId=$companyId&wId=$wId&record_to=" . urlencode(trim($_REQUEST['record_to'])) . "&record_from=" . urlencode(trim($_REQUEST['record_from']));

$client->account->calls->create(trim($_REQUEST["record_from"]), trim($_REQUEST["record_to"]), $action_url, array(
    "Method" => "POST",
    "IfMachine" => "Continue",
    "Record" => true
));
exit();
//Starting the html buffering on screen from here onwards
?>