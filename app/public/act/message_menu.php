<?php

require_once('include/config.php');
require_once('include/db.php');
require_once('include/Services/Twilio.php');
$db = new DB();


if (strlen($_REQUEST['exten'])){
	$exten=$_REQUEST['exten'];
} else {
	$response = new Services_Twilio_Twiml();
	$response->say("An error occured in the voicemail system");
	die((string) $response);
}

if (strlen($_REQUEST['Digits'])){
	$digits = $_REQUEST['Digits'];
	if ($digits == 1 || $digits == 2) {
		$messages = $db->getMessages($exten, $digits - 1);
		$location = "location: listen.php?exten=$exten&messages="
			. urlencode(implode(",",$messages));
		header($location);
		exit();
	} else {
		$error=true;
	}
}

$messages = $db->getMessages($exten, 0);
$new_msgs = count($messages);

$response = new Services_Twilio_Twiml();

if($error)
	$response->say('That was not a valid option');

$gather = $response->gather(array("numDigits" => "1"));
$gather->say("You have $new_msgs new messages");
$gather->say("To listen to new messages press 1.");
$gather->say("To listen to saved messages press 2.");
$response->say("Goodbye");
print $response;

?>
