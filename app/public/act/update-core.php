<?php
require_once('include/util.php');
session_start();

//Pre-load Checks
if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit;
}
if ($_SESSION['permission'] < 1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}
smsin();

$state = @$_POST['state'];

if(isset($BETA) && @$BETA === true)
    $manifesturl = "http://license.web1syndication.com/abc123/getVersionInfoBeta";
else
    $manifesturl = "http://license.web1syndication.com/abc123/getVersionInfo";

$json = json_decode($db->curlGetData($manifesturl));


// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        .yourversion{
            <?php if($json->internal_version > $product_version){ ?>
            color: rgb(165, 165, 0);
            <?php }else{ ?>
            color: rgb(0, 138, 0);
            <?php } ?>
        }
        .serverversion{
            color: rgb(0, 138, 0);
        }
    </style>
</head>




<body>

<div id="hld">

    <div class="wrapper">		<!-- wrapper begins -->
        <div id="teh_box" class="block small center login" style="width:650px; margin-top: 50px;">

<?php if($state=="") { ?>
    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>Update ACT<?php if(isset($BETA) && @$BETA === true) { ?> (Beta Channel)<?php } ?></h2>
        <ul>
            <li>Your Version: <span class="yourversion"><?php echo $version_str; ?></span></li>
            <li>Latest Version: <span class="serverversion"><?php echo $json->display_version; ?></span></li>
        </ul>
    </div>		<!-- .block_head ends -->


    <div class="block_content">

        <?php if($json->internal_version > $product_version) { ?>
        <div class="message info" style="width: 554px; margin: 0px 0px 11px;"><p>There is an update available. Click the button to update.</b></p></div>
            <form method="post" id="act_upd_form" action="update-core.php">
                <input type="hidden" name="state" value="1">
                <center><input type="submit" style="" class="submit long" id="act_update" value="Update to <?php echo $json->display_version; ?>"></center>
            </form>
        <br>
        <?php }else{ ?>
        <div class="message success" style="width: 554px; margin: 0px 0px 11px;"><p>You are already on the latest version.</b></p></div><br>
        <?php } ?>

        <h3>Changelog:</h3>
        <?php
            if(isset($BETA) && @$BETA === true)
                $feedurl = "http://license.web1syndication.com/abc123/getBetaFeed";
            else
                $feedurl = "http://bit.ly/10XRhj3";
        ?>
        <iframe style="border: 1px solid #cccccc; border-radius: 5px; margin-bottom: 15px;" src="<?php echo $feedurl; ?>" width="100%" height="600" ></iframe>

    </div>		<!-- .block_content ends -->
<?php } ?>

<?php
if($state==1) {
    //$product_version = 23;
    if($json->internal_version <= $product_version){ ?>
        <div class="block_head">
            <div class="bheadl"></div>
            <div class="bheadr"></div>

            <h2>Info</h2>
        </div>		<!-- .block_head ends -->


    <div class="block_content">
    <center><p>You are already on the latest version. No reason to update.</p></center>
        <form action="index.php" method="get">
            <center><a href="index.php" style="display: block;"><input type="submit" style="" class="submit long" value="Login"></a></center>
        </form><br>
    </div>
    <?php }else{

        $url = $json->download_url;

        $part_arr = explode("/",$url);

        $file_name = array_pop($part_arr);

        ?>
        <div class="block_head">
            <div class="bheadl"></div>
            <div class="bheadr"></div>

            <h2>Updating</h2>
        </div>		<!-- .block_head ends -->

        <script type="text/javascript">
            $("#teh_box").css("width","400px");
        </script>

        <div class="block_content">
            <center>
                <p>
                    <?php

                    if(!class_exists("PclZip"))
                        require_once('include/pclzip.lib.php');

                    //echo "Downloading '".$file_name."' <br/>";

                    $file = fopen($file_name,'w+') or die("<b><span style='color:red'>Error: </span></b> Could not create update file.");
                    $zipcontents = $db->curlGetData($url);
                    fwrite($file,$zipcontents) or die("<b><span style='color:red'>Error: </span></b> Could not write to file.");
                    fclose($file);

                    //echo "Extracting '".$file_name."' <br/>";

                    $zip = new PclZip($file_name);
                    if($zip->extract(PCLZIP_OPT_REPLACE_NEWER) == 0) {
                        die("<b><span style='color:red'>Error: </span></b> Could not extract from zip file. Details:<br>".$zip->errorInfo(true));
                    }

                    $db->setVar("product_version",$json->internal_version);
                    $db->setVar("update_needed","0");

                    echo "<br/><strong>Update Complete!</strong><br/>Click the button below to login.<br>";

                    unlink("./".$file_name) or die("<b><span style='color:red'>Error: </span></b> Could not delete file.");

                    ?>
                </p>

            </center>
            <form action="index.php" method="get">
                <center><a href="index.php" style="display: block;"><input type="submit" style="" class="submit long" value="Login"></a></center>
            </form><br>
        </div>
   <?php }
} ?>

            <div class="bendl"></div>
            <div class="bendr"></div>

        </div>		<!-- .login ends -->

    </div>						<!-- wrapper ends -->

</div>		<!-- #hld ends -->


<!--[if IE]><script type="text/javascript" src="js/excanvas.js"></script><![endif]-->
<script type="text/javascript" src="js/jquery.ui.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.img.preload.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.filestyle.mini.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.date_input.pack.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/facebox.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.visualize.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.visualize.tooltip.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.select_skin.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/ajaxupload.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.pngfix.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.blockUI.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/jquery.ui.timepicker.addon.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="jsmain/admin.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="jsmain/user.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="jsmain/graphs.js<?php echo "?".$build_number; ?>"></script>
<script type="text/javascript" src="js/custom.js<?php echo "?".$build_number; ?>"></script>

<script type="text/javascript">
    $("#act_update").click(function(e){
        e.preventDefault();
        promptMsg("<span style='font-weight:bold;color: rgb(255, 68, 68);'>Warning</span>: This updates " +
                "<strong>ALL</strong> the files in your ACT directory." +
                "<br>It also will update your database," +
                " so you may want to make a backup.<br>" +
                "If you are unsure how to do this, please look at the knowledgebase article:" +
                " <a style='color: #008ee8;' href='http://support.web1.co/index.php?/Knowledgebase/Article/View/64/1/making-a-backup-of-your-act-database' target='_blank'>web1support.com</a><br><br>" +
                "Do you want to continue?",
                function(){
            document.getElementById("act_upd_form").submit();
        });
    });
</script>

</body>
</html>