<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
require_once('include/db.client.php');
require_once('include/Pagination.php');
$db = new DB();
//This is an admin-only page.
//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}

if(@$_SESSION['permission']<1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

global $AccountSid, $AuthToken, $ApiVersion;

$clientClass = new Client();

$page="adminpage";

$company_id = $_REQUEST['company_id'];
$tracking_settings = $db->getTrackingSettingsForCompany($company_id);
$company_name = $db->getCompanyName($company_id);

$clients = json_decode($tracking_settings->clients);
$emails = json_decode($tracking_settings->emails);
$client_email_tags = json_decode($tracking_settings->client_email_tags);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        #purchase_number, #reset_url, #update_number,#delete_number{
            width: 85px;
            height: 30px;
            line-height: 30px;
            background: url(images/btns.gif) top center no-repeat;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            text-transform: uppercase;
            color: white;
            text-shadow: 1px 1px 0 #0A5482;
            cursor: pointer;
            margin-right: 10px;
            vertical-align: middle;
            display: inline-block !important;
        }
        .ui-dialog label, input {
            display: inline-block !important;
        }
        .cmf-skinned-select{
            display: inline-block !important;
        }
        select#country option {
            background-repeat:no-repeat;
            background-position:bottom left;
            padding-left:30px;
        }
        select#country option[value="US"] {
            background-image:url(images/us.png);
        }
        select#country option[value="CA"] {
            background-image:url(images/ca.png);
        }
        select#country option[value="GB"] {
            background-image:url(images/gb.png);
        }
        .voice_url, .sms_url {
            font-size: 10px;
            width: 350px;
            height: 17px;
        }
        .incorrect-url {
            background-color: #FFFF5F  !important;
            border: 1px gray solid !important;
        }
    </style>

</head>

<body>

<div id="hld">
<div class="wrapper">		<!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->

    <div class="block">

        <div class="block_head">
            <div class="bheadl"></div>
            <div class="bheadr"></div>

            <h2>
                Email Tracking Stats for <?php echo $company_name; ?>
            </h2>

            <ul>
                <li class="nobg"><a href="email_tracking_setup.php">Back to Emails Setup</a></li>
            </ul>

        </div>		<!-- .block_head ends -->


        <div class="block_content">

            <?php
            $start_date = (!empty($_GET['start_date'])) ? date("Y-m-d 00:00:00", strtotime(urldecode($_GET['start_date']))) : date("Y-m-d 00:00:00", time() - 2592000);
            $end_date = (!empty($_GET['end_date'])) ? date("Y-m-d 00:00:00", strtotime(urldecode($_GET['end_date']))) : date("Y-m-d 00:00:00", time() + 86400);
            $client_id = (!empty($_GET['client_id'])) ? $_GET['client_id'] : "";
            $email_address = (!empty($_GET['email'])) ? $_GET['email'] : "";
            ?>

            <form action="" method="get">
                <input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
                Date range:

                <input type="text" id="start_date" name="start_date" value="<?php if (isset($_GET['start_date'])) { echo $_GET['start_date']; } ?>" class="ui-widget-content ui-corner-all" style="display:inline-block !important;height: 20px; width: 100px; height: 24px; padding: 2px;" />
                to
                <input type="text" id="end_date" name="end_date" value="<?php if (isset($_GET['end_date'])) { echo $_GET['end_date']; } ?>" class="ui-widget-content ui-corner-all" style="display:inline-block !important;height: 20px; width: 100px; height: 24px; padding: 2px;" />

                &nbsp;

                Client:
                <select id="client_id" name="client_id" class="styled" style="width: 115px;">
                    <option value="">All</option>
                    <?php foreach ($clients as $client) { ?>
                        <option value="<?php echo $client->id; ?>" <?php if ($client->id == $client_id) { ?>selected="selected"<?php } ?>><?php echo $client->name; ?></option>
                    <?php } ?>
                </select>

                &nbsp;

                Email Address:
                <select id="email" name="email" class="styled" style="width: 165px;">
                    <option value="">All</option>
                    <?php foreach ($emails as $email) { ?>
                        <option value="<?php echo $email; ?>"<?php if ($email == $email_address) { ?>selected="selected"<?php } ?>><?php echo $email; ?>@trackingyourleads.com</option>
                    <?php } ?>
                </select>

                <input type="submit" class="submit small" value="Filter" style="margin-top: -7px; margin-right: 0px;" />
            </form>

            <br clear="all" />

            <div id="graph" style="width: 960px; height: 300px; position: relative; "></div>

            <br clear="all" />

            <form action="" onsubmit="return false;">
                <table cellpadding="0" cellspacing="0" width="100%" id="email_log_table">

                    <thead>
                    <tr>
                        <th>Date</th>
                        <th style="width: 100px;">Client</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Content</th>
                        <th style="width: 200px;">Tags</th>
                    </tr>
                    </thead>

                    <tbody>
                        <?php
                        $pagination = new Pagination();
                        $page_n = 1;
                        if (isset($_GET['page'])){
                            $page_n = (int) $_GET['page'];
                        }
                        if(strpos($_SERVER['REQUEST_URI'],"page") === false)
                        {
                            $url="//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                            $pagination->setLink($url."&page=%s");
                        }else{
                            $url="//".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['page']));
                            $pagination->setLink($url."%s");
                        }
                        $pagination->setPage($page_n);
                        $pagination->setSize(10);

                        $tracking_emails_log = $db->getTrackingEmailsLog($company_id, $start_date, $end_date, $client_id, $email_address, $pagination);

                        $pagination->setTotalRecords($tracking_emails_log['total']);

                        if (!$tracking_emails_log['emails']) {
                            ?>
                            <tr>
                                <td colspan="6">No emails.
                            </tr>
                            <?php
                        }
                        else {
                            foreach ($tracking_emails_log['emails'] as $email_log) {
                                $this_client = $clientClass->getClients(array('client_id' => $email_log->client_id));
                                $this_client = $this_client[0];
                        ?>
                        <tr>
                                <td>
                                    <?php 
                                    $date = new DateTime($email_log->email_date);
                                    echo $date->format("D n\/j Y g\:iA");
                                    ?>
                                </td>
                                <td><?php echo $this_client['client_name']; ?>
                                <td><?php echo htmlspecialchars($email_log->from_email); ?></td>
                                <td>
                                    <div id="email_column_<?php echo $email_log->to_email; ?>" class="tooltip" title="<?php echo $email_log->to_email; ?>@trackingyourleads.com">
                                        <?php echo $email_log->to_email; ?>@track...
                                    </div>
                                </td>
                                <td>
                                    <a href="javascript: void(0);" onclick="viewEmailContent('<?php echo $email_log->id; ?>');">View</a>
                                    <div id="email_log_content_<?php echo $email_log->id; ?>" style="display: none;" title="Email Content">
                                        <strong>Subject:</strong> <?php echo $email_log->subject; ?><br /><br />
                                        <strong>Body:</strong> <br />

                                        <?php 
                                        $body = utf8_encode($email_log->body);
                                        //if(strlen($body) != strlen(strip_tags($body))) {
                                            $body = nl2br($body);
                                        //}

                                        echo $body;
                                        ?>
                                    </div>
                                </td>
                                <td>
                                    <?php
                                    $email_tags = json_decode($email_log->billing_tags);
                                    $all_tags = @$client_email_tags->{$email_log->client_id}->{$email_log->to_email};
                                    $removed_tags = array();

                                    if (!empty($all_tags)) {
                                        foreach ($email_tags as $key=>$tag) {
                                            $exists = false;
                                            foreach ($all_tags as $tag2) {
                                                if ($tag2->tag == $tag->tag)
                                                    $exists = true;
                                            }
                                            if (!$exists) {
                                                $removed_tags[] = $tag;
                                            }
                                            $email_tags[$key]->exists = $exists;
                                        }
                                    }
                                    ?>
                                    <div class="edit_tags" style="background: #FFFFFF; border: solid 1px #CCCCCC; padding: 5px; position: absolute; width: 200px; display: none;">
                                        <strong>Edit tags</strong><br />
                                        <?php
                                        $all_tags_count = 0;
                                        if (!empty($removed_tags)) {
                                            foreach ($removed_tags as $tag) {
                                                $all_tags_count++;
                                                ?>
                                                    <div style="padding-top: 3px; padding-bottom: 3px; color: red;" class="tooltip" title="This tag has been deleted, your client will still be billed <strong><?php echo $this_client['lead_gen_amount_per_qualified_lead_currency']; ?> <?php echo number_format($tag->cost, '2', '.', ','); ?></strong> for it.">
                                                        <input type="checkbox" class="tag-checkbox" value="<?php echo $tag->tag; ?>" data-cost="<?php echo $tag->cost; ?>" data-removed="yes" data-currency="<?php echo $this_client['lead_gen_amount_per_qualified_lead_currency']; ?>" checked="checked" /> <?php echo $tag->tag; ?>
                                                    </div>
                                                <?php
                                            }
                                        }

                                        if (!empty($all_tags)) {
                                            foreach ($all_tags as $tag) {
                                                $all_tags_count++;

                                                $checked = false;
                                                foreach ($email_tags as $tag2) {
                                                    if ($tag2->tag == $tag->tag) {
                                                        $checked = true;
                                                    }
                                                }
                                                ?>
                                                <div style="padding-top: 3px; padding-bottom: 3px;">
                                                    <input type="checkbox" class="tag-checkbox" value="<?php echo $tag->tag; ?>" data-cost="<?php echo $tag->cost; ?>" <?php if ($checked) { ?>checked="checked"<?php } ?> /> <?php echo $tag->tag; ?>
                                                </div>
                                                <?php
                                            }
                                        }

                                        if ($all_tags_count == 0) {
                                            echo "No tags available.";
                                        }
                                        else {
                                            ?>
                                            <input type="submit" class="submit small" value="Save" onclick="saveTags(this, <?php echo $email_log->id; ?>);" />
                                            <?php                                            
                                        }
                                        ?>
                                    </div>
                                    <div class="view_tags tooltip" title="Double click to edit." style="background: #f4f4f4; border: solid 1px #CCCCCC; padding: 5px;" ondblclick="$('.edit_tags').hide(); $(this).parents('td').find('.edit_tags').show();">
                                        <?php

                                        if (count($email_tags) == 0) { 
                                            echo "no tags";
                                        }
                                        else {
                                            foreach ($email_tags as $tag) {
                                                ?>
                                                <div class="tag <?php if (!@$tag->exists) { ?>tooltip<?php } ?>" <?php if (!@$tag->exists) { ?>title="This tag has been deleted, your client will still be billed <strong><?php echo $this_client['lead_gen_amount_per_qualified_lead_currency']; ?> <?php echo number_format($tag->cost, '2', '.', ','); ?></strong> for it." style="background: red; color: white;"<?php } ?>><?php echo $tag->tag; ?></div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </td>
                        </tr>
                    <?php }
                    } ?>
                </table>
                <?php 
                $navigation = $pagination->create_links(); 
                echo $navigation; 
                ?>
            </form>
        </div>		<!-- .block_content ends -->

        <div class="bendl"></div>
        <div class="bendr"></div>
    </div>		<!-- .block ends -->

<?php include "include/footer.php"; ?>

    <script type="text/javascript">
            function viewEmailContent(id) {
                $("#email_log_content_" + id).dialog({
                    autoOpen: true,
                    modal: true,
                    width: 600
                });
            }

            function saveTags(el, id)
            {
                el = $(el);
                el.attr('disabled', true);
                el.val('Saving...');
                var td = el.parents('td');
                var tags = [];
                var tags_html = '';

                td.find('.tag-checkbox:checked').each(function() {
                    tags.push({
                        tag: $(this).val(),
                        cost: $(this).attr('data-cost')
                    });

                    var removed = $(this).attr('data-removed');
                    var currency = $(this).attr('data-currency');
                    var removed_class = "";
                    var removed_details = "";

                    if (removed == "yes") {
                        removed_class = ' tooltip';
                        var cost = parseFloat($(this).attr('data-cost'));
                        cost = cost.toFixed(2);
                        removed_details = 'title="This tag has been deleted, your client will still be billed <strong>' + currency + ' ' + cost + '</strong> for it." style="background: red; color: white;"';
                    }

                    tags_html += '<div class="tag ' + removed_class + '" ' + removed_details + '>' + $(this).val() + '</div>';
                });

                if (tags_html == '')
                    tags_html = 'No tags.';

                var response = JSONajaxCall({
                    func: "SYSTEM_SAVE_TRACKING_EMAIL_SAVE_TAGS",
                    data: {
                        id: id,
                        tags: JSON.stringify(tags)
                    }
                });
                response.done(function(msg) {
                    el.attr('disabled', false);
                    el.val('Save');
                    td.find('.view_tags').html(tags_html);
                    td.find('.edit_tags').hide();

                    $('.tooltip').tooltipster({
                        theme: '.tooltipster-shadow'
                    });
                });
            }

            $(document).ready(function() {
                $('.tooltip').tooltipster({
                    theme: '.tooltipster-shadow'
                });

                Web1Graphs.graphTypes.emailTrackingClients.draw_graph(
                    "graph",
                    <?php echo $company_id; ?>,
                    '<?php echo $start_date; ?>',
                    '<?php echo $end_date; ?>');

                $("#dataTable").tablesorter({
                    widgets: ['zebra']
                });
            });
    </script>
</body>
</html>