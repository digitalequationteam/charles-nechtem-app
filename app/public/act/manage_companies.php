<?php
//Check for config..
if(!file_exists("include/config.php"))
{
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');
$page = "adminpage";
$db = new DB();

if($_SESSION['permission'] == 0)
{
    $company_count = $db->getCompanyCountForUser($_SESSION['user_id']);
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);
}else{
    $company_count = $db->getCompanyCount();
    $companies = $db->getAllCompanies();
}


//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php");
    exit;
}
if(@$_SESSION['permission']<1 && !$_SESSION['prems']['customize_call_flow']) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

$act = @$_GET['act'];

if (isset($act)) {
    switch ($act) {
        case 'uploadMp3':
            $cId = $_REQUEST['cId'];

            $uploaddir = 'audio/'.$cId.'/';
            @mkdir ($uploaddir, 0777,true);
            @chmod ($uploaddir, 0777);
            
            $file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name'])); 
             
            if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
            { 
                $filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
            
                echo "$filename"; 
            
            } else {
                echo "error";
            }

            exit();
        break;

        case 'company_settings_form':

        $cId = $_GET['cId'];
        $settings   = $db->getcompanySettings($cId);
        $ga_id      = $db->getVar($cId."[ga_id]");
        $ga_domain  = $db->getVar($cId."[ga_domain]");
        $km_api_key = $db->getVar($cId."[kiss_metrics_api_key]");
        ?>
            <div id="edit_company_form" class="block" style="background: none; padding-bottom: 0; margin-bottom: 0;">
                <p class="validateTips">
                    Keep any of these fields blank to disable.
                </p>
                <form>
                    <fieldset>
                        <label>Whisper MP3</label>
                        
                        <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 15px !important; width: 490px; border: 1px solid #DDDDDD !important;">
                            <input type="hidden" class="content" name="whisper" value="<?php echo $settings->whisper; ?>" />
                            <input type="hidden" class="type" name="whisper_type" value="<?php echo $settings->whisper_type; ?>" />
                            <input type="hidden" class="voice" name="whisper_voice" value="<?php echo $settings->whisper_voice; ?>" />
                            <input type="hidden" class="language" name="whisper_language" value="<?php echo $settings->whisper_language; ?>" />
                            <div class="ivr-Menu-selector" style="display: block">
                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                  <?php if ($settings->whisper_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                  <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($settings->whisper_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                </div>
                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                  <?php if ($settings->whisper_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                  <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($settings->whisper_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                </div>
                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                  <?php if ($settings->whisper_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                  <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($settings->whisper_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                </div>
                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                  <?php if ($settings->whisper_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                  <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($settings->whisper_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                </div>
                              </div>
                              <div class="ivr-Menu-editor ">
                                <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                  <div class="ivr-Menu-read-text" style="display: none;">
                                    <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                    <br>
                                    <div>
                                      <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                        <label class="field-label">
                                            <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="width: 450px;"><?php echo (($settings->whisper_type == 'Text')? $settings->whisper:''); ?></textarea>

                                            <?php
                                            $voice = (($settings->whisper_type == 'Text')? $settings->whisper_voice:'');
                                            $language = (($settings->whisper_type == 'Text')? $settings->whisper_language:'');
                                            ?>
                                            <label class="field-label-left" style="width: 55px; display: inline-block !important;">Voice: </label>
                                            <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                              <?php
                                              echo Util::getTwilioVoices($voice);
                                              ?>
                                            </select>

                                            <br clear="all" />

                                            <label class="field-label-left" style="width: 55px; display: inline-block !important;">Dialect: </label>
                                            <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                              <?php
                                              echo Util::getTwilioLanguages($voice, $language);
                                              ?>
                                            </select>

                                            <br clear="all" /><br />

                                            <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                            <script type="text/javascript">
                                              $(document).ready(function() {
                                                $("#voice").trigger("change");
                                              });
                                            </script>

                                            <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; display: inline !important;" />
                                        </label>
                                      </fieldset>
                                    </div>
                                    <br>
                                    <br>
                                  </div>
                                  <div class="ivr-audio-upload" style="display: none;">
                                    <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                    <div class="swfupload-container">
                                      <div class="explanation"> <br>
                                        
                                        <span class="title" <?php if ( $settings->whisper_type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                        <?php 
                                  echo (($settings->whisper_type == 'Audio')? $settings->whisper:''); ?>
                                        </strong></span> <br>
                                        
                                        
                                        <span class="title">Click to select a file: </span>
                                        <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                        <span class="title" id="statusUpload">&nbsp;</span>
                                      </div>                                                   
                                    
                                    </div>
                                  </div>
                                  <div class="ivr-mp3-url" style="display: none;">
                                    <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                    <div class="swfupload-container">
                                      <div class="explanation"> <br>           
                                        
                                        <span class="title">
                                          <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($settings->whisper_type == 'MP3_URL')? $settings->whisper:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                          <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                        </span>

                                        <br /><br />

                                        <span class="title" <?php if ( $settings->whisper_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                        <?php 
                                  echo (($settings->whisper_type == 'MP3_URL')? $settings->whisper:''); ?>
                                        </strong></span>

                                      </div>
                                    
                                    
                                    </div>
                                  </div>
                                  <div class="ivr-record-audio" style="display: none;">
                                    <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                    <div class="swfupload-container">
                                      <div class="explanation"> <br>
                                       <center>
                                           <table>
                                               <tr>
                                                   <td>Caller ID: </td>
                                                   <td>
                                                       <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                           <option value="">Select number</option>
                                                           <?php
                                                           $numbers = $db->getNumbersOfCompany($cId);
                                                           foreach ($numbers as $number) {
                                                               ?>
                                                               <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                               <?php
                                                           }
                                                           ?>
                                                       </select>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td>Your phone number: </td>
                                                   <td><input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 120px !important; display: inline !important;" /></td>
                                               </tr>
                                           </table>
                                       </center>
                                        <br />
                                        <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 180px;" />
                                        <br />
                                        <span class="title" <?php if ( $settings->whisper_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                          <?php if ($settings->whisper_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($settings->whisper, 'sm'); } ?>
                                        </span>

                                      </div>
                                    
                                    
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </fieldset>

                        <label for="rec_url">Recording Notification</label>
                        
                        <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 15px !important; width: 490px; border: 1px solid #DDDDDD !important;">
                            <input type="hidden" class="content" name="recording_notification" value="<?php echo $settings->recording_notification; ?>" />
                            <input type="hidden" class="type" name="recording_notification_type" value="<?php echo $settings->recording_notification_type; ?>" />
                            <input type="hidden" class="voice" name="recording_notification_voice" value="<?php echo $settings->recording_notification_voice; ?>" />
                            <input type="hidden" class="language" name="recording_notification_language" value="<?php echo $settings->recording_notification_language; ?>" />
                            <div class="ivr-Menu-selector" style="display: block">
                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                  <?php if ($settings->recording_notification_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                  <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($settings->recording_notification_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                                </div>
                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                  <?php if ($settings->recording_notification_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                  <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($settings->recording_notification_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                                </div>
                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                  <?php if ($settings->recording_notification_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                  <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($settings->recording_notification_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                                </div>
                                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                                  <?php if ($settings->recording_notification_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                                  <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($settings->recording_notification_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                                </div>
                              </div>
                              <div class="ivr-Menu-editor ">
                                <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                                  <div class="ivr-Menu-read-text" style="display: none;">
                                    <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                    <br>
                                    <div>
                                      <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                        <label class="field-label">
                                            <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="width: 450px;"><?php echo (($settings->recording_notification_type == 'Text')? $settings->recording_notification:''); ?></textarea>

                                            <?php
                                            $voice = (($settings->recording_notification_type == 'Text')? $settings->recording_notification_voice:'');
                                            $language = (($settings->recording_notification_type == 'Text')? $settings->recording_notification_language:'');
                                            ?>
                                            <label class="field-label-left" style="width: 55px; display: inline-block !important;">Voice: </label>
                                            <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                              <?php
                                              echo Util::getTwilioVoices($voice);
                                              ?>
                                            </select>

                                            <br clear="all" />

                                            <label class="field-label-left" style="width: 55px; display: inline-block !important;">Dialect: </label>
                                            <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                              <?php
                                              echo Util::getTwilioLanguages($voice, $language);
                                              ?>
                                            </select>

                                            <br clear="all" /><br />

                                            <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                            <script type="text/javascript">
                                              $(document).ready(function() {
                                                $("#voice").trigger("change");
                                              });
                                            </script>

                                            <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; display: inline !important;" />
                                        </label>
                                      </fieldset>
                                    </div>
                                    <br>
                                    <br>
                                  </div>
                                  <div class="ivr-audio-upload" style="display: none;">
                                    <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                    <div class="swfupload-container">
                                      <div class="explanation"> <br>
                                        
                                        <span class="title" <?php if ( $settings->recording_notification_type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                        <?php 
                                  echo (($settings->recording_notification_type == 'Audio')? $settings->recording_notification:''); ?>
                                        </strong></span> <br>
                                        
                                        
                                        <span class="title">Click to select a file: </span>
                                        <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                        <span class="title" id="statusUpload">&nbsp;</span>
                                      </div>                                                   
                                    
                                    </div>
                                  </div>
                                  <div class="ivr-mp3-url" style="display: none;">
                                    <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                    <div class="swfupload-container">
                                      <div class="explanation"> <br>           
                                        
                                        <span class="title">
                                          <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($settings->recording_notification_type == 'MP3_URL')? $settings->recording_notification:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                          <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                        </span>

                                        <br /><br />

                                        <span class="title" <?php if ( $settings->recording_notification_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                        <?php 
                                  echo (($settings->recording_notification_type == 'MP3_URL')? $settings->recording_notification:''); ?>
                                        </strong></span>

                                      </div>
                                    
                                    
                                    </div>
                                  </div>
                                  <div class="ivr-record-audio" style="display: none;">
                                    <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                                    <div class="swfupload-container">
                                      <div class="explanation"> <br>
                                          <center>
                                              <table>
                                                  <tr>
                                                      <td>Caller ID: </td>
                                                      <td>
                                                          <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                                              <option value="">Select number</option>
                                                              <?php
                                                              $numbers = $db->getNumbersOfCompany($cId);
                                                              foreach ($numbers as $number) {
                                                                  ?>
                                                                  <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                                                  <?php
                                                              }
                                                              ?>
                                                          </select>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Your phone number: </td>
                                                      <td><input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 120px !important; display: inline !important;" /></td>
                                                  </tr>
                                              </table>
                                          </center>
                                        <br />
                                        <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 180px;" />
                                        <br />

                                        <span class="title" <?php if ( $settings->recording_notification_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                          <?php if ($settings->recording_notification_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($settings->recording_notification, 'sm'); } ?>
                                        </span>

                                      </div>
                                    
                                    
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </fieldset>

                        <div style="width:100%; height: 1px; background: rgb(220, 220, 220); margin-bottom:10px;margin-top: 10px;"></div>
                        
                         <label style="vertical-align: middle;display:inline-block !important;" onclick="if ($('#rec_disable').is(':checked')) { $('#record_from_ring_label').hide(); } else { $('#record_from_ring_label').show(); }">
                            <input type="checkbox" name="rec_disable" id="rec_disable" style="display:inline-block !important;" <?php if ($settings->recording_disable == 1) { ?>checked="checked"<?php } ?> />
                            Disable Recordings for this Company
                        </label>
                        <br>
                        
                        <div id="record_from_ring_label" style="display: <?php if ($settings->recording_disable == 1) { ?>none<?php } else { ?>inline-block<?php } ?>;">
                           <label style="vertical-align: middle;display: inline-block !important;">
                              <input type="checkbox" name="record_from_ring" id="record_from_ring" style="display:inline-block !important;" <?php if ($settings->record_from_ring == 1) { ?>checked="checked"<?php } ?> />
                              Record from ring
                          </label>
                        </diV>

                        <?php global $lcl; if($lcl>1) { ?>

                        <label style="vertical-align: middle;display:inline-block !important;">
                            <input type="checkbox" name="ga_enable" id="ga_enable" style="display:inline-block !important;" <?php if (!empty($ga_id) || !empty($ga_domain)) { ?>checked="checked"<?php } ?> />
                            Enable Google Analytics tracking for this Company
                        </label>

                        <br>
                        <table id="ga_table" style="margin-left: 20px; display: <?php if (!empty($ga_id) || !empty($ga_domain)) { ?>block<?php } else { ?>none<?php } ?>;"><tr><td><label style="display: inline-block !important;" for="ga_id">Google Analytics ID: </label> </td><td><input disabled style="display: inline-block !important; margin-bottom: 0 !important;" class="text ui-widget-content ui-corner-all" type="text" name="ga_id" id="ga_id" value="<?php echo $ga_id; ?>" /></td></tr>
                        <tr><td><label style="display: inline-block !important;" for="ga_domain">Domain:</label></td><td><input disabled style="width: 215px !important;display: inline-block !important; margin-bottom: 0 !important;" class="text ui-widget-content ui-corner-all" type="text" name="ga_domain" id="ga_domain" value="<?php echo $ga_domain; ?>" /></td></tr>
                        </table>
                        <label style="vertical-align: middle;display:inline-block !important;">
                            <input type="checkbox" name="kissmetrics_enable" id="kissmetrics_enable" style="display: inline-block !important;" <?php if (!empty($km_api_key)) { ?>checked="checked"<?php } ?> />
                            Enable <span style="cursor: pointer;color: #008ee8;" onclick="window.open('http://kissmetrics.com/?ref=act'); return false;">KISSmetrics</span> tracking for this company
                        </label>
                        <br/>
                        <table id="kw_table" style="margin-left: 20px; display: <?php if (!empty($km_api_key)) { ?>block<?php } else { ?>none<?php } ?>;"><tr><td><label style="display: inline-block !important;" for="km_api_key">API Key: </label> </td><td><input disabled style="width: 350px !important;display: inline-block !important; margin-bottom: 0 !important;" class="text ui-widget-content ui-corner-all" type="text" name="km_api_key" id="km_api_key" value="<?php echo $km_api_key; ?>"/></td></tr></table>

                        <?php }else{ ?>
                        <input type="hidden" name="kissmetrics_enable" id="kissmetrics_enable"/>
                        <input type="hidden" name="ga_enable" id="ga_enable"/>
                        <input type="hidden" name="ga_id" id="ga_id"/>
                        <input type="hidden" name="ga_domain" id="ga_domain"/>
                        <input type="hidden" name="km_api_key" id="km_api_key"/>
                        <?php } ?>

                    </fieldset>
                </form>
            </div>

                <script type="text/javascript">
                    function showAudioText(obj)
                    {
                        
                        
                        var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
                        audioChoice.hide();
                        audioChoice.parent().children('.ivr-Menu-editor').show();
                        var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');
                        
                        if ( obj.id  == 'txt' ) 
                        {       
                            
                            subDiv.children('.ivr-Menu-read-text').show();
                            
                            //////////////// only to avoid  file button clickable in text area      
                            $('[name="uploadfile"]').css('z-index','-1');       
                        
                        }
                        else if ( obj.id  == 'upload_mp3' ) {
                            $('[name="uploadfile"]').css('z-index','2147483583');
                            
                            subDiv.children('.ivr-audio-upload').show();    
                            SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');        
                            UploadFile(SubObj); 
                        }
                        else if ( obj.id  == 'mp3_url' ) {
                            subDiv.children('.ivr-mp3-url').show();
                        }
                        else if ( obj.id  == 'record_audio' ) {
                            subDiv.children('.ivr-record-audio').show();
                        }
                        
                    }

                    function CloseButton(obj)
                    {
                        var audioChoice = $(obj).closest('.ivr-Menu');
                        
                        var audioChoiceEditor   = audioChoice.children('.ivr-Menu-editor');
                        var audioChoiceSelector = audioChoice.children('.ivr-Menu-selector');
                        
                        var subDiv  = audioChoiceEditor.children('.ivr-Menu-editor-padding');
                        
                        audioChoiceSelector.show();
                        audioChoiceEditor.hide();
                        subDiv.children('.ivr-audio-upload').hide();    
                        subDiv.children('.ivr-Menu-read-text').hide();
                        subDiv.children('.ivr-mp3-url').hide();
                        subDiv.children('.ivr-record-audio').hide();
                    }

                    function testVoice(voice, language, text) {
                        if ($("#test_voice_iframe_wrapper").length == 0) {
                            $("#edit_company_form").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
                                                '<form id="test_voice_form" action="test_voice.php" method="post" target="test_voice_iframe">' +
                                                    '<input type="hidden" name="voice" id="voice" />' +
                                                    '<input type="hidden" name="language" id="language" />' +
                                                    '<input type="hidden" name="text" id="text" />' +
                                                    '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                                '</form>' +
                                                '<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
                                            '</form>'
                            );
                        }

                        $("#test_voice_iframe_wrapper #voice").val(voice);
                        $("#test_voice_iframe_wrapper #language").val(language);
                        $("#test_voice_iframe_wrapper #text").val(text);
                        $('#test_voice_iframe_wrapper #submit').click()
                     }

                     var closeBtnHtml = '<a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a>';

                     function SaveContent(obj, content_type) {
                        var thisBlock = $(obj).closest('.ivr-Menu');

                        switch (content_type) {
                            case "Text_mail":
                            {
                                var voice_text = $(thisBlock).find('#readtxt_mail').val();
                                var voice = $(thisBlock).find('#voice').val();
                                var language = $(thisBlock).find('#language').val();
                                $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('.ivr-Menu-close-button').click();

                                $(thisBlock).find('.content').val(voice_text);
                                $(thisBlock).find('.type').val("Text");
                                $(thisBlock).find('.voice').val(voice);
                                $(thisBlock).find('.language').val(language);

                                $(thisBlock).find(".ttsMwCloseBtn").remove();
                                $(thisBlock).find('#txt').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                                break;
                            }
                            case "MP3_URL":
                            {

                                var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                                $(thisBlock).find('.ivr-Menu-close-button').click();

                                $(thisBlock).find('.content').val(mp3_url);
                                $(thisBlock).find('.type').val("MP3_URL");

                                $(thisBlock).find(".ttsMwCloseBtn").remove();
                                $(thisBlock).find('#mp3_url').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                                break;
                            }
                        }
                    }

                    function removeSelectedOption(obj) {
                        promptMsg('Are you sure ?', function() {
                          var thisBlock = $(obj).closest('.ivr-Menu');
                          $(thisBlock).find(".ttsMwCloseBtn").remove();

                          $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                          $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                          $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                          $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                          $(thisBlock).find('.content').val('');
                          $(thisBlock).find('.type').val('');
                          $(thisBlock).find('.voice').val('');
                          $(thisBlock).find('.language').val('');
                        });
                    }

                    function UploadFile(obj)
                    {
                        var status = $(obj).closest('.explanation').find('#statusUpload');
                        var fileNameStatus = $(obj).closest('.explanation').find('#voicefilename');
                        var fileNameStatusWrapper = $(obj).closest('.explanation').find('#voicefilenameWrapper');

                        new AjaxUpload(obj, {
                            
                            
                            action: 'manage_companies.php?act=uploadMp3&cId=<?php echo $cId; ?>',
                            name: 'uploadfile',
                            onSubmit: function(file, ext){
                                 if (! (ext && /^(mp3|wma)$/.test(ext))){ 
                                    // extension is not allowed 
                                    status.text('Only MP3 files are allowed');
                                    return false;
                                }
                                (fileNameStatus).html('Uploading...');
                            },
                            onComplete: function(file, response){
                                //On completion clear the status
                                
                                //Add uploaded file to list
                                
                                if(response!="error"){
                                    $(fileNameStatusWrapper).css('display' ,'block');
                                    $(fileNameStatus).html(response);
                                    var thisBlock = $(obj).closest('.ivr-Menu');

                                    $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                                    $(thisBlock).find('#upload_mp3').addClass('ivr-Menu-Selected');
                                    $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                                    $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                                    $(thisBlock).find('.content').val(response);
                                    $(thisBlock).find('.type').val("Audio");
                                    $(thisBlock).find('.ivr-Menu-close-button').click();

                                    $(thisBlock).find(".ttsMwCloseBtn").remove();
                                    $(thisBlock).find('#upload_mp3').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                                } else{
                                }
                            }
                        });
                            
                    }

                    var recordingId = "";
                    function recordAudio(obj) {
                        if ($(obj).val() == "Call Me") {
                            recordingId = createUUID();
                            var record_from = $(obj).parents('.ivr-Menu').find('#record_from').val();
                            var record_to = $(obj).parents('.ivr-Menu').find('#record_to').val();

                            if (record_from == "") {
                                errMsgDialog("Please select Caller ID.");
                                return false;
                            }

                            if (record_to == "") {
                                errMsgDialog("Please enter your phone number.")
                                return false;
                            }

                            $(obj).val('Stop');

                            if ($("#record_audio_iframe_wrapper").length == 0) {
                                $("#edit_company_form").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
                                                    '<form id="record_audio_form" action="record_audio.php" method="post" target="record_audio_iframe">' +
                                                        '<input type="hidden" name="recordingId" id="recordingId" />' +
                                                        '<input type="hidden" name="record_from" id="record_from" />' +
                                                        '<input type="hidden" name="record_to" id="record_to" />' +
                                                        '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                                    '</form>' +
                                                    '<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
                                                '</form>'
                                );
                            }

                            $("#record_audio_iframe_wrapper #recordingId").val(recordingId);
                            $("#record_audio_iframe_wrapper #record_from").val(record_from);
                            $("#record_audio_iframe_wrapper #record_to").val(record_to);
                            $('#record_audio_iframe_wrapper #submit').click()
                        }
                        else {
                            $(obj).val('Please wait...');
                            $("#record_audio_iframe").contents().find("#disconnectBtn").click();

                            setTimeout(function() {
                                $.post("admin_ajax_handle.php", { func: "GET_RECORDING", recordingId: recordingId },  function(response) {
                                    var thisBlock = $(obj).closest('.ivr-Menu');

                                    $(thisBlock).find('#recordedAudioSavedWrapper').css('display' ,'block');
                                    if (response) {
                                        $(thisBlock).find('#recordedAudioSavedWrapper').html(response.playable);
                                        
                                        $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                                        $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                                        $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                                        $(thisBlock).find('.content').val(response.url);
                                        $(thisBlock).find('.type').val("RECORD_AUDIO");
                                        $(thisBlock).find('.ivr-Menu-close-button').click();

                                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                                        $(thisBlock).find('#record_audio').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                                    }
                                    else {
                                        errMsgDialog('Audio was not recorded, please try again.');
                                        $(thisBlock).find('#recordedAudioSavedWrapper').html('');
                                    }
                                    $(obj).val('Call Me');
                                }, "json");
                            }, 2000);
                        }
                     }

                     function createUUID() {
                        // http://www.ietf.org/rfc/rfc4122.txt
                        var s = [];
                        var hexDigits = "0123456789abcdef";
                        for (var i = 0; i < 36; i++) {
                            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
                        }
                        s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
                        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
                        s[8] = s[13] = s[18] = s[23] = "-";

                        var uuid = s.join("");
                        return uuid;
                    }
                </script>
            <?php
        break;
    }
    exit();
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

</head>





<body>

<div id="hld">

<div class="wrapper">		<!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->


<div class="block">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>
            <?php
            if($company_count==1)
                echo "1 Company";
            else
                echo $company_count." Companies";
            ?>
            <?php if (@$_SESSION['permission'] > 0) { ?>
            &nbsp; &nbsp; <a href="#" id="SYSTEM_ADD_COMPANY">Add Company</a>
            <?php } ?>
        </h2>

        <?php if (@$_SESSION['permission'] > 0) { ?>
          <ul>
              <li><a href="company_report.php">Run Usage Report</a></li>
              <li><a href="manage_blacklists.php">Blacklists</a></li>
              <li><a href="manage_pc_templates.php">Phone Code Templates</a></li>
          </ul>
        <?php } ?>

    </div>		<!-- .block_head ends -->


    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%" class="sortable">

            <thead>
            <tr>
                <th>Company</th>
                <th rowspan="6">Numbers</th>
                <th>Call Flow</th>
                <th>Caller ID</th>
                <th>Blacklist</th>
                <?php if (@$_SESSION['permission'] > 0) { ?><th>Actions</th><?php } ?>
            </tr>
            </thead>

            <tbody>

            <?php foreach ($companies as $company) {
                $c_nums = $db->getCompanyNumIntl($company['idx']);
                $numbers = "";
                if($c_nums!="")
                    foreach($c_nums as $number)
                    {
                        if(!$number[1])
                            $numbers .= format_phone($number[0]).", ";
                        else
                            $numbers .= format_phone("+".$number[0]).", ";
                    }
                $numbers = rtrim($numbers, ", ");
                if(strlen($numbers)==0)
                    $numbers = "None";

                // Call Flow
                $call_flow = CallFlow::getCallFlow($company['call_flow']);

                if (empty($call_flow['name']))
                  $call_flow['name'] = "None";
            ?>
            <tr>
                <td>
                  <?php if (@$_SESSION['permission'] > 0) { ?><a id="SYSTEM_EDIT_COMPANY_SETTINGS" data-params='<?php echo $company['idx']; ?>' href="#"><?php } ?><?php echo Util::escapeString($company['company_name']); ?><?php if (@$_SESSION['permission'] > 0) { ?></a><?php } ?>
                </td>
                <td>
                  <?php if (@$_SESSION['permission'] > 0) { ?>
                    <?php 
                      echo "<a href=\"#\" id=\"SYSTEM_EDIT_COMPANY_NUMBERS\" data-params='" . $company['idx'] . "' title='Edit Numbers' style='cursor:pointer'>".$numbers."</a>";
                    ?>
                  <?php } else {
                      echo $numbers;
                  } ?>
                </td>
                <td style="white-space: nowrap;"><?php echo "<a href=\"edit_call_flow.php?company=".$company['idx']."\" title='Edit Call Flow' style='cursor:pointer'>".$call_flow['name']."</a>"; ?></td>
                <td><?php
                  if (@$_SESSION['permission'] > 0)
                    if($company['callerid']=="") 
                      echo "<a href='#' data-params='".$company['idx']."' id='SYSTEM_EDIT_CALLERID'>None</a>";
                    else 
                      echo "<a href='#' data-params='".$company['idx']."' id='SYSTEM_EDIT_CALLERID'>".$company['callerid']."</a>";
                  else
                    if($company['callerid']=="") 
                      echo "None";
                    else 
                      echo $company['callerid'];
                 ?></td>
                <td>
                  <?php 
                  if (@$_SESSION['permission'] > 0) {
                      if(!$company['blacklist_id'])$blacklist="None"; else $blacklist=$db->getBlacklistName($company['blacklist_id']); echo "<a href=\"#\" id='SYSTEM_EDIT_BLACKLIST_COMPANY' data-params='" . $company['idx'] . "' title='Edit Blacklist'>$blacklist</a>";
                  }
                  else {
                    if(!$company['blacklist_id'])$blacklist="None"; else $blacklist=$db->getBlacklistName($company['blacklist_id']); echo $blacklist;
                  }
                  ?></td>
                <?php if (@$_SESSION['permission'] > 0) { ?>
                  <td>

                  <?php echo "<a href=\"#\" id='SYSTEM_EDIT_PHONE_CODE' data-params='" . $company['idx'] . "' title='Edit Company Phone Codes'><img src=\"images/phone_code.png\" /></a>&nbsp;"; ?>

                  <?php echo "<a href=\"#\" id='SYSTEM_DELETE_COMPANY' data-params='" . $company['idx'] . "' title='Delete Company'><img src=\"images/delete.gif\" /></a>"; ?>

                </td>
                <?php } ?>
            </tr>
                <?php } ?>
        </table>

    </div>		<!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>
</div>		<!-- .block ends -->


<?php include "include/footer.php"; ?>
</body>
</html>