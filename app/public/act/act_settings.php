<?php
//Check for config..
if (!file_exists("include/config.php")) {
    die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('include/util.php');

$page = "adminpage";
$db = new DB();
$companies = $db->getAllCompanies();

//Pre-load Checks
if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit;
}
if ($_SESSION['permission'] < 1) {
    ?>
<script type="text/javascript">
    alert('You can\'t access this page');
    window.location = "index.php";
</script>
<?php
    exit;
}

$act = (isset($_GET['act']) ? $_GET['act'] : "");
if (  $act == 'uploadMp3' )
{   
    $uploaddir = 'audio/global_opt_out_files/'; 
    
    @mkdir ($uploaddir,0777,true);
    @chmod($uploaddir,0777);
    
    $file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name'])); 
     
    if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
    { 
        $filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
    
        echo "$filename"; 
    
    } else {
        echo "error";
    }

    exit();
}

// DISABLE CACHE
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]>
    <style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        .ui-dialog label, input {
            display: inline-block !important;
        }

        .cmf-skinned-select {
            display: inline-block !important;
        }

        div.wysiwyg ul.panel {
            width: 422px !important;
        }
        textarea {
            width: 98%;
            height: 140px;
            padding: 5px;
            background: #fefefe;
            border: 1px solid #bbb;
            font-family: "Lucida Grande", Verdana, sans-serif;
            font-size: 14px;
            color: #333;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            outline: none;
        }
        input.submit {
            width: 85px;
            height: 30px;
            line-height: 30px;
            background: url(images/btns.gif) top center no-repeat;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            text-transform: uppercase;
            color: #fff;
            text-shadow: 1px 1px 0 #0a5482;
            cursor: pointer;
            margin-right: 10px;
            vertical-align: middle;
        }
        input.submit.mid {
            width: 115px;
            background: url(images/btnm.gif) top center no-repeat;
        }
    </style>

</head>

<body>

<div id="hld">

<div class="wrapper">        <!-- wrapper begins -->


<?php include_once("include/nav.php"); ?>
<!-- #header ends -->

<div class="block" id="filter-box" style="width: 619px; margin:0 auto; margin-bottom: 25px;">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>ACT Settings</h2>
        <ul>
            <li><a id="changelog_link" style="color: #008ee8;" title="Click here to view the ACT changelog and updates." href="update-core.php">Changelog and Updates</a></li>
        </ul>
    </div>
    <!-- .block_head ends -->

    <div class="block_content">

        <?php
        // Check for update
        if($update_needed=="1") {
        ?>
            <div class="message info" style="width: 380px; margin: 0 0 11px; font-size:10px;"><p><a href="update-core.php">There is an update available. Click here to go to the update page.</a></b></p></div>
        <?php } ?>


        <h2>Twilio Options</h2>

        <p>
            <?php
            $MASK_RECORDINGS = $db->getVar("mask_recordings");
            if ($MASK_RECORDINGS == "true") {
                ?>
                <input type="checkbox" id="mask_recordings" name="mask_recordings" checked />
                <?php
            } else {
                ?>
                <input type="checkbox" id="mask_recordings" name="mask_recordings" />
                <?php
            }
            ?>
            <label for="mask_recordings">Mask recordings URLs</label>
        </p>

        <p>
            <?php
            if ($RECORDINGS == true) {
                ?>
                <input type="checkbox" id="option_transcriptions" name="option_transcriptions"/>
                <?php
            } else {
                ?>
                <input type="checkbox" id="option_transcriptions" name="option_transcriptions" checked/>
                <?php
            }
            ?>
            <label for="option_transcriptions">Disable recordings for <i>ALL</i> companies.</label>
        </p>
        <?php
        $intl_dialtone = false;
        if ($db->getVar("intl_dialtone") == "yes")
            $intl_dialtone = true;

        $option_ringtone = Util::getRingTone();
        ?>

        <form>
            <label>Ringtone:</label>
            <select class="styled" name="option_ringtone" id="option_ringtone" style="width: 300px;">
                <option value="" <?php if ($option_ringtone == "") { ?>selected="selected"<?php } ?>>Auto Detect</option>
                <option value="US" <?php if ($option_ringtone == "US") { ?>selected="selected"<?php } ?>>United States</option>
                <?php
                foreach(Util::$supported_countries as $country_code => $country){
                    if ($country_code == "GB") $country_code = "UK";
                    ?>
                    <option value="<?php echo $country_code; ?>" <?php if ($option_ringtone == $country_code) { ?>selected="selected"<?php } ?>><?php echo $country[0]; ?></option>
                <?php
                }
                ?>
            </select>
        </form>
        
        <br />
        <p style="display: none;">
            <input type="checkbox" id="option_intldialtone"
                   name="option_intldialtone" <?php if ($intl_dialtone) echo "checked"; ?> />
            <label for="option_intldialtone">Enable International Ringback Tone</label>
        </p>

        <form style="display: none;">
            <input id="dial_tone_url" style="width:431px;font-size: 10px;height: 14px;" type="text"
                   class="text small <?php if (!$intl_dialtone) echo "disabled"; ?>"
                   value="<?php echo $db->getVar("intl_dialtone_url"); ?>" <?php if (!$intl_dialtone) echo "disabled"; ?> />
            <input type="submit"
                   style="margin-top:1px; <?php if (!$intl_dialtone) echo "opacity: 0.4;"; ?>" <?php if (!$intl_dialtone) echo "disabled"; ?>
                   class="submit mid" id="SYSTEM_SET_DIALTONE_URL"
                   value="Save URL"/>
        </form>

        <div style="width:100%; height: 1px; background:#888; margin-bottom:10px;"></div>
        <h2>ACT Options</h2>

        <?php
        $global_opt_out_content = ($db->getVar("global_opt_out_content")) ? $db->getVar("global_opt_out_content") : "";
        $global_opt_out_type = ($db->getVar("global_opt_out_type")) ? $db->getVar("global_opt_out_type") : "";
        $global_opt_out_voice = ($db->getVar("global_opt_out_voice")) ? $db->getVar("global_opt_out_voice") : "";
        $global_opt_out_language = ($db->getVar("global_opt_out_language")) ? $db->getVar("global_opt_out_language") : "";
        $global_opt_out_key = ($db->getVar("global_opt_out_key")) ? $db->getVar("global_opt_out_key") : "";
        $global_sms_opt_out_message = ($db->getVar("global_sms_opt_out_message")) ? $db->getVar("global_sms_opt_out_message") : "";
        $global_sms_opt_out_trigger = ($db->getVar("global_sms_opt_out_trigger")) ? $db->getVar("global_sms_opt_out_trigger") : "";
        ?>

        <form onsubmit="return false;">
            <div id="opt_out_key_wrapper">
                <label>Global VB Opt-out:</label>
                <br clear="all" />
                
                <fieldset class="ivr-Menu ivr2-input-container" style="margin-bottom: 10px; width: 574px;">
                    <input type="hidden" class="content" id="global_opt_out_content" name="global_opt_out_content" value="<?php echo $global_opt_out_content; ?>" />
                    <input type="hidden" class="type" id="global_opt_out_type" name="global_opt_out_type" value="<?php echo $global_opt_out_type; ?>" />
                    <input type="hidden" class="voice" id="global_opt_out_voice" name="global_opt_out_voice" value="<?php echo $global_opt_out_voice; ?>" />
                    <input type="hidden" class="language" id="global_opt_out_language" name="global_opt_out_language" value="<?php echo $global_opt_out_language; ?>" />
                    <div class="ivr-Menu-selector" style="display: block">
                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                          <?php if ($global_opt_out_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                          <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($global_opt_out_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                        </div>
                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                          <?php if ($global_opt_out_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                          <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($global_opt_out_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                        </div>
                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                          <?php if ($global_opt_out_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                          <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($global_opt_out_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                        </div>
                        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                          <?php if ($global_opt_out_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                          <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($global_opt_out_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                        </div>
                      </div>

                      <div class="ivr-Menu-editor ">
                        <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                          <div class="ivr-Menu-read-text" style="display: none;">
                            <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                            <br>
                            <div>
                              <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                                <label class="field-label">
                                    <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($global_opt_out_type == 'Text')? $global_opt_out_content:''); ?></textarea>

                                    <?php
                                    $voice = (($global_opt_out_type == 'Text')? $global_opt_out_voice:'');
                                    $language = (($global_opt_out_type == 'Text')? $global_opt_out_language:'');
                                    ?>
                                    <label class="field-label-left" style="width: 55px; display: inline-block;">Voice: </label>
                                    <select id="voice" onchange="var language = $(this).parents('.ivr-Menu').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                                      <?php
                                      echo Util::getTwilioVoices($voice);
                                      ?>
                                    </select>

                                    <br clear="all" />

                                    <label class="field-label-left" style="width: 55px; display: inline-block;">Dialect: </label>
                                    <select id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-top: 5px !important;">
                                      <?php
                                      echo Util::getTwilioLanguages($voice, $language);
                                      ?>
                                    </select>

                                    <br clear="all" /><br />

                                    <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('.ivr-Menu').find('#voice').val(), $(this).parents('.ivr-Menu').find('#language').val(), $(this).parents('.ivr-Menu').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline !important;" />

                                    <script type="text/javascript">
                                      $(document).ready(function() {
                                        $("#voice").trigger("change");
                                      });
                                    </script>

                                    <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px; display: inline !important;" />
                                </label>
                              </fieldset>
                            </div>
                            <br>
                            <br>
                          </div>
                          <div class="ivr-audio-upload" style="display: none;">
                            <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                            <div class="swfupload-container">
                              <div class="explanation"> <br>
                                
                                <span class="title" <?php if ( $global_opt_out_type != 'Audio' ) echo ' style="display:none" ' ?> id="voicefilenameWrapper"  >Voice to play: <strong id="voicefilename">
                                <?php 
                          echo (($global_opt_out_type == 'Audio')? $global_opt_out_content:''); ?>
                                </strong></span> <br>
                                
                                
                                <span class="title">Click to select a file: </span>
                                <div style="width: 100px; margin: auto;"><input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></div>
                                <span class="title" id="statusUpload">&nbsp;</span>
                              </div>                                                   
                            
                            </div>
                          </div>
                          <div class="ivr-mp3-url" style="display: none;">
                            <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                            <div class="swfupload-container">
                              <div class="explanation"> <br>           
                                
                                <span class="title">
                                  <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($global_opt_out_type == 'MP3_URL')? $global_opt_out_content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                                  <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                                </span>

                                <br /><br />

                                <span class="title" <?php if ( $global_opt_out_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                                <?php 
                          echo (($global_opt_out_type == 'MP3_URL')? $global_opt_out_content:''); ?>
                                </strong></span>

                              </div>
                            
                            
                            </div>
                          </div>
                          <div class="ivr-record-audio" style="display: none;">
                            <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                            <div class="swfupload-container">
                              <div class="explanation"> <br>           
                                
                                Caller ID:

                                <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                                  <option value="">Select number</option>
                                  <?php
                                  $numbers = $db->getNumbersOfCompany($_SESSION['sel_co']);
                                  foreach ($numbers as $number) {
                                    ?>
                                      <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>

                                Your phone number:

                                <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 162px; display: inline !important;" />

                                <br /><br />

                                <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                                <br /><br />

                                <span class="title" <?php if ( $global_opt_out_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSavedWrapper" >
                                    <?php if ($global_opt_out_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($global_opt_out_content, 'sm'); } ?>
                                </span>

                              </div>
                            
                            
                            </div>
                          </div>
                        </div>
                    </div>
                </fieldset>

                <br />
                <label>Key:</label>
                <select class="styled" name="global_opt_out_key" id="global_opt_out_key" style="width: 50px;">
                    <option value="1" <?php if ($global_opt_out_key == "1") { ?>selected="selected"<?php } ?>>1</option>
                    <option value="2" <?php if ($global_opt_out_key == "2") { ?>selected="selected"<?php } ?>>2</option>
                    <option value="3" <?php if ($global_opt_out_key == "3") { ?>selected="selected"<?php } ?>>3</option>
                    <option value="4" <?php if ($global_opt_out_key == "4") { ?>selected="selected"<?php } ?>>4</option>
                    <option value="5" <?php if ($global_opt_out_key == "5") { ?>selected="selected"<?php } ?>>5</option>
                    <option value="6" <?php if ($global_opt_out_key == "6") { ?>selected="selected"<?php } ?>>6</option>
                    <option value="7" <?php if ($global_opt_out_key == "7") { ?>selected="selected"<?php } ?>>7</option>
                    <option value="8" <?php if ($global_opt_out_key == "8") { ?>selected="selected"<?php } ?>>8</option>
                    <option value="9" <?php if ($global_opt_out_key == "9") { ?>selected="selected"<?php } ?>>9</option>
                    <option value="0" <?php if ($global_opt_out_key == "0") { ?>selected="selected"<?php } ?>>0</option>
                    <option value="*" <?php if ($global_opt_out_key == "*") { ?>selected="selected"<?php } ?>>*</option>
                </select>
            </div>

            <br clear="all" />

            <div id="opt_out_trigger_wrapper">
                <label>Global SMS Opt-out Message:</label> <input type="text" class="text big" id="global_sms_opt_out_message" name="global_sms_opt_out_message" value="<?php echo isset($global_sms_opt_out_message) ? $global_sms_opt_out_message : ''; ?>" style="width: 560px;" />
                <br />
                <br />
                <label>Trigger:</label>
                <select class="styled" id="global_sms_opt_out_trigger" name="global_sms_opt_out_trigger" style="width: 200px;">
                    <option value="QUIT ALL" <?php if ($global_sms_opt_out_trigger == "QUIT ALL") { ?>selected="selected"<?php } ?>>QUIT ALL</option>
                    <option value="CANCEL ALL" <?php if ($global_sms_opt_out_trigger == "CANCEL ALL") { ?>selected="selected"<?php } ?>>CANCEL ALL</option>
                    <option value="UNSUBSCRIBE ALL" <?php if ($global_sms_opt_out_trigger == "UNSUBSCRIBE ALL") { ?>selected="selected"<?php } ?>>UNSUBSCRIBE ALL</option>
                </select>
            </div>
            <br clear="all" />

            <center>
                <input type="submit" class="submit long" id="save_global_opt_out" value="Save Opt-Out Settings" />
            </center>
        </form>

        <div style="width:100%; height: 1px; background: rgb(220, 220, 220); margin-bottom:10px;margin-top: 10px;"></div>

        <p>
            <?php
            $enable_support_link = "no";
            if ($db->getVar("enable_support_link") == "yes")
                $enable_support_link = "yes";

            ?>
            <input id="SYSTEM_ENABLE_SUPPORTLINK" data-params="<?php echo $enable_support_link; ?>"
                   type="checkbox" <?php if ($enable_support_link == "yes") echo "checked " ?>>
            <label for="SYSTEM_ENABLE_SUPPORTLINK">Enable the support sidebar link.</label>
        </p>
        <p>
            <?php
            $disable_footer = "no";
            if ($db->getVar("disable_footer") == "yes")
                $disable_footer = "yes";

            ?>
            <input id="SYSTEM_DISABLE_FOOTER" data-params="<?php echo $disable_footer; ?>"
                   type="checkbox" <?php if ($disable_footer == "yes") echo "checked " ?>>
            <label for="SYSTEM_DISABLE_FOOTER">Disable the footer in ACT.</label>
        </p>

        <p>
            <?php
            $opencnam = "no";
            $opencnam_AccountSid = $db->getVar("opencnam_AccountSid");
            $opencnam_AuthKey = $db->getVar("opencnam_AuthKey");
            if ($opencnam_AccountSid != false || $opencnam_AccountSid != "")
                $opencnam = "yes";
            ?>
            <input id="opencnam_option" data-params="<?php echo $opencnam; ?>"
                   type="checkbox" <?php if ($opencnam == "yes") echo "checked " ?>>
            <label for="opencnam_option">Use OpenCNAM for Caller ID Lookup</label>
            <form id="opencnam_info" style="display: none;">
                <span style="font-size: 10px;">Only supported for +1 numbers (US and CA). For more information please see: <a href="https://www.opencnam.com/pricing?ref=act" target="_blank">OpenCNAM's Website</a></span>
                <table>
                    <tr>
                        <td><label for="opencnam_AccountSid">AccountSid: </label></td>
                        <td><input type="text" style="width: 426px;" class="text" name="opencnam_AccountSid" id="opencnam_AccountSid" value="<?php echo $opencnam_AccountSid; ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="opencnam_AuthKey">AuthKey: </label></td>
                        <td><input type="text" style="width: 426px;" class="text" name="opencnam_AuthKey" id="opencnam_AuthKey" value="<?php echo $opencnam_AuthKey; ?>" /></td>
                    </tr>
                </table>
        <center>
            <input type="submit" class="submit long" id="test_cnam_settings" value="Test Credentials"/>
            <input type="submit" style="display: none !important;" class="submit mid" id="save_cnam_settings" value="Save"></center>
            </form>
        <?php if($opencnam == "yes") { ?>
        <script>$("#opencnam_info").slideToggle(600);</script>
        <?php } ?>
        </p>
        <br/>
        <p>
            <?php
            $logoURL = "";
            if ($db->getVar("company_logo"))
                $logoURL = $db->getVar("company_logo");

            ?>
            Company Logo URL:<br/>

        <form>
            <input id="company_logo_url" style="width:431px;font-size: 10px;height: 14px;" type="text"
                   class="text small" value="<?php echo $logoURL; ?>">
            <input type="submit" style="margin-top:1px;" class="submit mid" id="SYSTEM_SET_LOGO_URL" value="Save URL"/>
        </form>
        </p>

        <br/>

        <p>
            <?php
            $info = "";
            if ($db->getVar("company_info"))
                $info = $db->getVar("company_info");

            ?>
            Company Info for Call Reports:<br/>


            <textarea id="company_info" style="width:431px;"><?php echo $info; ?></textarea>
            <input type="submit" style="margin-top: 10px;float: right;margin-bottom: 5px;" class="submit mid"
                   id="SYSTEM_SET_COMPANY_INFO" value="Save INFO"/>
        </p>

        <div style="width:100%; height: 1px; background: rgb(220, 220, 220); margin-bottom:10px;margin-top: 10px;"></div>

        <div style="width: 415px; margin: auto;">
            <p>
                Generate <b>referrer tracking</b> javascript code for company:<br>

            <form id="company_frm">
                <select id="company_select" class="styled">
                    <option value="NONE" selected>SELECT A COMPANY</option>
                    <?php
                    foreach ($companies as $company) {
                        ?>
                        <option value="<?php echo $company['idx']; ?>"><?php echo $company['company_name']; ?></option><?php
                    }
                    ?>
                </select>
                <input type="submit" style="margin-bottom: 4px;" class="submit mid" id="getUrl" value="Generate Code"/>
            </form>
            </p>

            <div style="width:100%; height: 1px; background: rgb(220, 220, 220); margin-bottom:10px;margin-top: 10px;"></div>


            <p>
                Generate <b>keyword tracking</b> javascript code for company:<br>

            <form id="company_frm2">
                <select id="company_select2" class="styled">
                    <option value="NONE" selected>SELECT A COMPANY</option>
                    <?php
                    foreach ($companies as $company) {
                        ?>
                        <option value="<?php echo $company['idx']; ?>"><?php echo $company['company_name']; ?></option><?php
                    }
                    ?>
                </select>
                <input type="submit" style="margin-bottom: 4px;" class="submit mid" id="getUrlKW" value="Generate Code"/>
            </form>
            </p>
        </div>

        <div style="width:100%; height: 1px; background: rgb(220, 220, 220); margin-bottom:10px;margin-top: 10px;"></div>

        <p>
            <center><input type="submit" class="submit long" id="reAuth" value="Reauthorize Account"/></center><br>

            <center><input type="submit" <?php if($opencnam == "yes") echo 'style="display:inline-block !important;"'; else echo 'style="display:none !important;"'; ?> class="submit vlong" id="run_cnam_on_db" value="Fetch CallerID for All Calls"> </center>
        </p>

    </div>
    <!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>
</div>
<select id="country" name="country" style="display:none;">
<option value="default">Default</option>
<option value="US">United States</option>
<option value="CA">Canada</option>
<option value="GB">United Kingdom</option>
<option value="AU">Australia</option>
<option value="AF">Afghanistan</option>
<option value="AL">Albania</option>
<option value="DZ">Algeria</option>
<option value="AS">American Samoa</option>
<option value="AD">Andorra</option>
<option value="AO">Angola</option>
<option value="AI">Anguilla</option>
<option value="AQ">Antarctica</option>
<option value="AG">Antigua And Barbuda</option>
<option value="AR">Argentina</option>
<option value="AM">Armenia</option>
<option value="AW">Aruba</option>
<option value="AT">Austria</option>
<option value="AZ">Azerbaijan</option>
<option value="BS">Bahamas</option>
<option value="BH">Bahrain</option>
<option value="BD">Bangladesh</option>
<option value="BB">Barbados</option>
<option value="BY">Belarus</option>
<option value="BE">Belgium</option>
<option value="BZ">Belize</option>
<option value="BJ">Benin</option>
<option value="BM">Bermuda</option>
<option value="BT">Bhutan</option>
<option value="BO">Bolivia</option>
<option value="BA">Bosnia And Herzegovina</option>
<option value="BW">Botswana</option>
<option value="BV">Bouvet Island</option>
<option value="BR">Brazil</option>
<option value="IO">British Indian Ocean Territory</option>
<option value="BN">Brunei</option>
<option value="BG">Bulgaria</option>
<option value="BF">Burkina Faso</option>
<option value="BI">Burundi</option>
<option value="KH">Cambodia</option>
<option value="CM">Cameroon</option>
<option value="CV">Cape Verde</option>
<option value="KY">Cayman Islands</option>
<option value="CF">Central African Republic</option>
<option value="TD">Chad</option>
<option value="CL">Chile</option>
<option value="CN">China</option>
<option value="CX">Christmas Island</option>
<option value="CC">Cocos (Keeling) Islands</option>
<option value="CO">Columbia</option>
<option value="KM">Comoros</option>
<option value="CG">Congo</option>
<option value="CK">Cook Islands</option>
<option value="CR">Costa Rica</option>
<option value="CI">Cote D'Ivorie (Ivory Coast)</option>
<option value="HR">Croatia (Hrvatska)</option>
<option value="CU">Cuba</option>
<option value="CY">Cyprus</option>
<option value="CZ">Czech Republic</option>
<option value="CD">Democratic Republic Of Congo (Zaire)</option>
<option value="DK">Denmark</option>
<option value="DJ">Djibouti</option>
<option value="DM">Dominica</option>
<option value="DO">Dominican Republic</option>
<option value="TP">East Timor</option>
<option value="EC">Ecuador</option>
<option value="EG">Egypt</option>
<option value="SV">El Salvador</option>
<option value="GQ">Equatorial Guinea</option>
<option value="ER">Eritrea</option>
<option value="EE">Estonia</option>
<option value="ET">Ethiopia</option>
<option value="FK">Falkland Islands (Malvinas)</option>
<option value="FO">Faroe Islands</option>
<option value="FJ">Fiji</option>
<option value="FI">Finland</option>
<option value="FR">France</option>
<option value="FX">France, Metropolitan</option>
<option value="GF">French Guinea</option>
<option value="PF">French Polynesia</option>
<option value="TF">French Southern Territories</option>
<option value="GA">Gabon</option>
<option value="GM">Gambia</option>
<option value="GE">Georgia</option>
<option value="DE">Germany</option>
<option value="GH">Ghana</option>
<option value="GI">Gibraltar</option>
<option value="GR">Greece</option>
<option value="GL">Greenland</option>
<option value="GD">Grenada</option>
<option value="GP">Guadeloupe</option>
<option value="GU">Guam</option>
<option value="GT">Guatemala</option>
<option value="GN">Guinea</option>
<option value="GW">Guinea-Bissau</option>
<option value="GY">Guyana</option>
<option value="HT">Haiti</option>
<option value="HM">Heard And McDonald Islands</option>
<option value="HN">Honduras</option>
<option value="HK">Hong Kong</option>
<option value="HU">Hungary</option>
<option value="IS">Iceland</option>
<option value="IN">India</option>
<option value="ID">Indonesia</option>
<option value="IR">Iran</option>
<option value="IQ">Iraq</option>
<option value="IE">Ireland</option>
<option value="IM">Isle of Man</option>
<option value="IL">Israel</option>
<option value="IT">Italy</option>
<option value="JM">Jamaica</option>
<option value="JP">Japan</option>
<option value="JO">Jordan</option>
<option value="KZ">Kazakhstan</option>
<option value="KE">Kenya</option>
<option value="KI">Kiribati</option>
<option value="KW">Kuwait</option>
<option value="KG">Kyrgyzstan</option>
<option value="LA">Laos</option>
<option value="LV">Latvia</option>
<option value="LB">Lebanon</option>
<option value="LS">Lesotho</option>
<option value="LR">Liberia</option>
<option value="LY">Libya</option>
<option value="LI">Liechtenstein</option>
<option value="LT">Lithuania</option>
<option value="LU">Luxembourg</option>
<option value="MO">Macau</option>
<option value="MK">Macedonia</option>
<option value="MG">Madagascar</option>
<option value="MW">Malawi</option>
<option value="MY">Malaysia</option>
<option value="MV">Maldives</option>
<option value="ML">Mali</option>
<option value="MT">Malta</option>
<option value="MH">Marshall Islands</option>
<option value="MQ">Martinique</option>
<option value="MR">Mauritania</option>
<option value="MU">Mauritius</option>
<option value="YT">Mayotte</option>
<option value="MX">Mexico</option>
<option value="FM">Micronesia</option>
<option value="MD">Moldova</option>
<option value="MC">Monaco</option>
<option value="MN">Mongolia</option>
<option value="MS">Montserrat</option>
<option value="MA">Morocco</option>
<option value="MZ">Mozambique</option>
<option value="MM">Myanmar (Burma)</option>
<option value="NA">Namibia</option>
<option value="NR">Nauru</option>
<option value="NP">Nepal</option>
<option value="NL">Netherlands</option>
<option value="AN">Netherlands Antilles</option>
<option value="NC">New Caledonia</option>
<option value="NZ">New Zealand</option>
<option value="NI">Nicaragua</option>
<option value="NE">Niger</option>
<option value="NG">Nigeria</option>
<option value="NU">Niue</option>
<option value="NF">Norfolk Island</option>
<option value="KP">North Korea</option>
<option value="MP">Northern Mariana Islands</option>
<option value="NO">Norway</option>
<option value="OM">Oman</option>
<option value="PK">Pakistan</option>
<option value="PW">Palau</option>
<option value="PA">Panama</option>
<option value="PG">Papua New Guinea</option>
<option value="PY">Paraguay</option>
<option value="PE">Peru</option>
<option value="PH">Philippines</option>
<option value="PN">Pitcairn</option>
<option value="PL">Poland</option>
<option value="PT">Portugal</option>
<option value="PR">Puerto Rico</option>
<option value="QA">Qatar</option>
<option value="RE">Reunion</option>
<option value="RO">Romania</option>
<option value="RU">Russia</option>
<option value="RW">Rwanda</option>
<option value="SH">Saint Helena</option>
<option value="KN">Saint Kitts And Nevis</option>
<option value="LC">Saint Lucia</option>
<option value="PM">Saint Pierre And Miquelon</option>
<option value="VC">Saint Vincent And The Grenadines</option>
<option value="SM">San Marino</option>
<option value="ST">Sao Tome And Principe</option>
<option value="SA">Saudi Arabia</option>
<option value="SN">Senegal</option>
<option value="SC">Seychelles</option>
<option value="SL">Sierra Leone</option>
<option value="SG">Singapore</option>
<option value="SK">Slovak Republic</option>
<option value="SI">Slovenia</option>
<option value="SB">Solomon Islands</option>
<option value="SO">Somalia</option>
<option value="ZA">South Africa</option>
<option value="GS">South Georgia And South Sandwich Islands</option>
<option value="KR">South Korea</option>
<option value="ES">Spain</option>
<option value="LK">Sri Lanka</option>
<option value="SD">Sudan</option>
<option value="SR">Suriname</option>
<option value="SJ">Svalbard And Jan Mayen</option>
<option value="SZ">Swaziland</option>
<option value="SE">Sweden</option>
<option value="CH">Switzerland</option>
<option value="SY">Syria</option>
<option value="TW">Taiwan</option>
<option value="TJ">Tajikistan</option>
<option value="TZ">Tanzania</option>
<option value="TH">Thailand</option>
<option value="TG">Togo</option>
<option value="TK">Tokelau</option>
<option value="TO">Tonga</option>
<option value="TT">Trinidad And Tobago</option>
<option value="TN">Tunisia</option>
<option value="TR">Turkey</option>
<option value="TM">Turkmenistan</option>
<option value="TC">Turks And Caicos Islands</option>
<option value="TV">Tuvalu</option>
<option value="UG">Uganda</option>
<option value="UA">Ukraine</option>
<option value="AE">United Arab Emirates</option>
<option value="UM">United States Minor Outlying Islands</option>
<option value="UY">Uruguay</option>
<option value="UZ">Uzbekistan</option>
<option value="VU">Vanuatu</option>
<option value="VA">Vatican City (Holy See)</option>
<option value="VE">Venezuela</option>
<option value="VN">Vietnam</option>
<option value="VG">Virgin Islands (British)</option>
<option value="VI">Virgin Islands (US)</option>
<option value="WF">Wallis And Futuna Islands</option>
<option value="EH">Western Sahara</option>
<option value="WS">Western Samoa</option>
<option value="YE">Yemen</option>
<option value="YU">Yugoslavia</option>
<option value="ZM">Zambia</option>
<option value="ZW">Zimbabwe</option>
</select>

<?php include "include/footer.php"; ?>
<script type="text/javascript">
    $("#run_cnam_on_db").click(function(e){
        $("#filter-box").block({
            message:'<h1 style="color:#fff">Calculating Cost</h1>',
            css:{
                border:'none',
                padding:'15px',
                backgroundColor:'#000',
                '-webkit-border-radius':'10px',
                '-moz-border-radius':'10px',
                opacity:.7,
                color:'#fff !important'
            }
        });
        var response = JSONajaxCall({
            func:"SYSTEM_OPENCNAM_COST",
            data:{}
        });
        response.done(function (msg) {
            if(msg.result=="success"){
                promptMsg("The calculated cost is around $"+msg.cost+" for "+msg['count']+" calls. Do you want to continue?<br>" +
                        "<strong>Note:</strong> This may take a long time to run. (Estimated: "+msg.time+")",function(){
                    window.onbeforeunload = function() { return "Are you sure you want to leave? This will cancel the lookup!"; }
                    $("body").block({
                        message:'<h1 style="color:#fff">Running CallerID Lookup on Call Database. Please do not leave this page!</h1>',
                        css:{
                            border:'none',
                            padding:'15px',
                            backgroundColor:'#000',
                            '-webkit-border-radius':'10px',
                            '-moz-border-radius':'10px',
                            opacity:.7,
                            color:'#fff !important'
                        }
                    });
                    var response = JSONajaxCall({
                        func:"SYSTEM_OPENCNAM_RUNNER",
                        data:{}
                    });
                    response.done(function(msg){
                        infoMsgDialog("Done running CallerID Lookup! You may now leave this page.");
                        $("body").unblock();
                        window.onbeforeunload = null;
                    });
                });
            }else if(msg.result == "fail" && msg.reason==1){
                errMsgDialog("There is no calls to lookup!");
            }
            $("#filter-box").unblock();
        });
    });
    $("#test_cnam_settings").click(function(e){
        e.preventDefault();
        var AccountSid = $("#opencnam_AccountSid").val();
        var AuthKey = $("#opencnam_AuthKey").val();
        if(AccountSid=="" || AuthKey==""){
            errMsgDialog("Please enter your OpenCNAM AccountSid and AuthKey.");
        }else{
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });
            var response = JSONajaxCall({
                func:"SYSTEM_OPENCNAM_TEST",
                data:{accountsid:AccountSid,authkey:AuthKey}
            });
            response.done(function (msg) {
                if(msg.result=="success"){
                    infoMsgDialog("Your account appears to be working!");
                    $("#run_cnam_on_db").attr("style","display:inline-block !important;");
                }
                else if(msg.result=="fail" && msg.reason==1){
                    errMsgDialog("Your account credentials appear to be incorrect or you are not on the 'Professional' tier.");
                }
                $("#filter-box").unblock();
            });
        }
    });
    $("#save_cnam_settings").click(function(e){
        e.preventDefault();
        var AccountSid = $("#opencnam_AccountSid").val();
        var AuthKey = $("#opencnam_AuthKey").val();
        if(AccountSid=="" || AuthKey==""){
            errMsgDialog("Please enter your OpenCNAM AccountSid and AuthKey.");
        }else{
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });
            var response = JSONajaxCall({
                func:"SYSTEM_OPENCNAM_UPDATE",
                data:{accountsid:AccountSid,authkey:AuthKey}
            });
            response.done(function (msg) {
                infoMsgDialog("Saved!");
                $("#filter-box").unblock();
            });
        }
    });
    $("input[id^='opencnam_'][type='text']").change(function(e){
        $("#save_cnam_settings").show();
    });
    $("#opencnam_option").click(function(e){
        if(!$("#opencnam_option").is(":checked") && $("#opencnam_AccountSid").val()!=""){
            promptMsg2("Are you sure you want to disable OpenCNAM?<br>This will remove your OpenCNAM settings.",function(){
                $("#save_cnam_settings").attr("style","display:none !important;");
                $("#opencnam_AccountSid").val("");
                $("#opencnam_AuthKey").val("");
                $("#run_cnam_on_db").attr("style","display:none !important;");
                $("#filter-box").block({
                    message:'<h1 style="color:#fff">Loading</h1>',
                    css:{
                        border:'none',
                        padding:'15px',
                        backgroundColor:'#000',
                        '-webkit-border-radius':'10px',
                        '-moz-border-radius':'10px',
                        opacity:.7,
                        color:'#fff !important'
                    }
                });
                var response = JSONajaxCall({
                    func:"SYSTEM_OPENCNAM_DISABLE",
                    data:{}
                });
                response.done(function (msg) {
                    $("#opencnam_info").slideToggle(600);
                    $("#filter-box").unblock();
                });
            },function(){
                $("#opencnam_option").attr("checked","checked");
                e.preventDefault();
                return false;
            });
        }else{
            $("#opencnam_info").slideToggle(600);
        }
    });
    $("#reAuth").click(function (event){
        event.preventDefault();
        promptMsg("You can use this to check for Enterprise.<br> Do you want to reauthorize?",function(){
            var response = JSONajaxCall({
                func:"SYSTEM_REAUTH_ACT"
            });
            response.done(function(data){
                if(typeof data.msg !== 'undefined'){
                    msgDialogCB(data.msg,function(){window.location='act_settings.php';});
                }else{
                    errMsgDialogCB("An error has occurred. Please try again shortly.",function(){
                        window.location='act_settings.php';
                    });
                }
            });
        });
    });

    var defaultscript = function(company,format) {
        if(typeof format === 'undefined')
            return "<scr" + "ipt type=\"text/javascript\" src=\"" + document.location.href.split("act_settings.php")[0] + 'referrer_dynjs.php?c_id='
                    + company + "\"></scr" + "ipt>";
        else
            return "<scr" + "ipt type=\"text/javascript\" src=\"" + document.location.href.split("act_settings.php")[0] + 'referrer_dynjs.php?c_id='
                    + company + "&format="+format+"\"></scr" + "ipt>";
    };
    $('#getUrl').click(function (event) {
        event.preventDefault();
        if ($("#company_select").val() == "NONE")
            errMsgDialog("Select a company.");
        else {
            msgDialog("<p>Insert this before the closing \"&lt;/body&gt;\".</p>" +
                    "<label for='country'>Local Number Formatting: </label>&nbsp;&nbsp;&nbsp;"+$("#country").attr("onchange","regenerate(this,"+$("#company_select").val()+")")[0].outerHTML.replace("display:none;","") +
                    "<br/><textarea id='scriptcopy' style='width: 501px; height: 65px;'>"+defaultscript($("#company_select").val())+"</textarea>");
        }
    });
    function regenerate(elm,company){
        var country_code = elm.value;
        if(country_code!='default')
            $("#scriptcopy").text(
                    "<scr"+"ipt type=\"text/javascript\" src=\""+document.location.href.split("act_settings.php")[0] + 'js/PhoneFormat.js' +"\"></scr"+"ipt>"
                    +"\n"+defaultscript(company,country_code));
        else
            $("#scriptcopy").text(defaultscript(company));
    }

    $("#getUrlKW").click(function(event){
        event.preventDefault();
        if ($("#company_select2").val() == "NONE")
            errMsgDialog("Select a company.");
        else {
            generateKWCode($("#company_select2").val());
            //msgDialog("<p>Insert this before the closing \"&lt;/body&gt;\".</p><textarea style='width: 501px; height: 65px;'><scr" + "ipt> var cid="+$("#company_select2").val()+"; var url=\""+ document.location.href.split("act_settings.php")[0] +"\";</scr" + "ipt><scr" + "ipt type=\"text/javascript\" src=\"" + document.location.href.split("act_settings.php")[0] + 'keyword-tracking.js' + "\"></scr" + "ipt></textarea>");
        }
    });

        $('#option_ringtone').change(function () {
            var ring_tone = $("#option_ringtone").val();
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });

            var ajaxparams = {
                ring_tone: ring_tone
            };
            var response = JSONajaxCall({
                func:"SYSTEM_RING_TONE",
                data:ajaxparams
            });
            response.done(function (msg) {
                $("#filter-box").unblock();
            });
        });

    $('#option_intldialtone').click(function () {
        if (!$('#option_intldialtone').is(':checked')) {
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });

            var ajaxparams = {
                status:'disable'
            };
            var response = JSONajaxCall({
                func:"SYSTEM_INTL_DIALTONE",
                data:ajaxparams
            });
            response.done(function (msg) {
                if (msg.result=="success") {
                    $("#SYSTEM_SET_DIALTONE_URL").css("opacity", "0.4").attr("disabled", "disabled");
                    $("#dial_tone_url").addClass("disabled").attr("disabled","disabled");
                }
                $("#filter-box").unblock();
            });
        } else {
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });

            var ajaxparams = {
                status:'enable'
            };
            var response = JSONajaxCall({
                func:"SYSTEM_INTL_DIALTONE",
                data:ajaxparams
            });
            response.done(function (msg) {
                if (msg.result=="success") {
                    $("#SYSTEM_SET_DIALTONE_URL").css("opacity", "1").removeAttr("disabled");
                    $("#dial_tone_url").removeClass("disabled").removeAttr("disabled");
                }
                $("#filter-box").unblock();
            });
        }
    });

    $('#option_transcriptions').click(function () {
        if ($('#option_transcriptions').is(':checked')) {
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });

            var ajaxparams = {
                status:'disable'
            };
            var response = JSONajaxCall({
                func:"SYSTEM_DISABLE_RECORDINGS",
                data:ajaxparams
            });
            response.done(function (msg) {
                $("#filter-box").unblock();
            });
        } else {
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });

            var ajaxparams = {
                status:'enable'
            };
            var response = JSONajaxCall({
                func:"SYSTEM_DISABLE_RECORDINGS",
                data:ajaxparams
            });
            response.done(function (msg) {
                $("#filter-box").unblock();
            });
        }
    });

    $('#mask_recordings').click(function () {
        if ($('#mask_recordings').is(':checked')) {
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });

            var ajaxparams = {
                status: "true"
            };
            var response = JSONajaxCall({
                func:"SYSTEM_MASK_RECORDINGS",
                data:ajaxparams
            });
            response.done(function (msg) {
                $("#filter-box").unblock();
            });
        } else {
            $("#filter-box").block({
                message:'<h1 style="color:#fff">Loading</h1>',
                css:{
                    border:'none',
                    padding:'15px',
                    backgroundColor:'#000',
                    '-webkit-border-radius':'10px',
                    '-moz-border-radius':'10px',
                    opacity:.7,
                    color:'#fff !important'
                }
            });

            var ajaxparams = {
                status: "false"
            };
            var response = JSONajaxCall({
                func:"SYSTEM_MASK_RECORDINGS",
                data:ajaxparams
            });
            response.done(function (msg) {
                $("#filter-box").unblock();
            });
        }
    });

    var countryList = '';
    $("#changelog_link").tooltipster({
        position:'top',
        theme:'.tooltipster-shadow'
    });

    $('#save_global_opt_out').click(function () {
        $("#filter-box").block({
            message:'<h1 style="color:#fff">Loading</h1>',
            css:{
                border:'none',
                padding:'15px',
                backgroundColor:'#000',
                '-webkit-border-radius':'10px',
                '-moz-border-radius':'10px',
                opacity:.7,
                color:'#fff !important'
            }
        });

        var ajaxparams = {
            global_opt_out_content: $("#global_opt_out_content").val() || "",
            global_opt_out_type: $("#global_opt_out_type").val() || "",
            global_opt_out_voice: $("#global_opt_out_voice").val() || "",
            global_opt_out_language: $("#global_opt_out_language").val() || "",
            global_opt_out_key: $("#global_opt_out_key").val() || "",
            global_sms_opt_out_message: $("#global_sms_opt_out_message").val() || "",
            global_sms_opt_out_trigger: $("#global_sms_opt_out_trigger").val() || "",
        };

        var response = JSONajaxCall({
            func: "SYSTEM_SAVE_GLOBAL_OPT_OUT_SETTINGS",
            data: ajaxparams
        });

        response.done(function (msg) {
            infoMsgDialog("Global Opt-Out settings saved!");
            $("#filter-box").unblock();
        });
    });


    function showAudioText(obj)
    {
        
        
        var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
        audioChoice.hide();
        audioChoice.parent().children('.ivr-Menu-editor').show();
        var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');
        
        if ( obj.id  == 'txt' ) 
        {       
            
            subDiv.children('.ivr-Menu-read-text').show();
            
            //////////////// only to avoid  file button clickable in text area      
            $('[name="uploadfile"]').css('z-index','-1');       
        
        }
        else if ( obj.id  == 'upload_mp3' ) {
            $('[name="uploadfile"]').css('z-index','2147483583');
            
            subDiv.children('.ivr-audio-upload').show();    
            SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');        
            UploadFile(SubObj); 
        }
        else if ( obj.id  == 'mp3_url' ) {
            subDiv.children('.ivr-mp3-url').show();
        }
        else if ( obj.id  == 'record_audio' ) {
            subDiv.children('.ivr-record-audio').show();
        }
        
    }

    function CloseButton(obj)
    {
        var audioChoice = $(obj).closest('.ivr-Menu');
        
        var audioChoiceEditor   = audioChoice.children('.ivr-Menu-editor');
        var audioChoiceSelector = audioChoice.children('.ivr-Menu-selector');
        
        var subDiv  = audioChoiceEditor.children('.ivr-Menu-editor-padding');
        
        audioChoiceSelector.show();
        audioChoiceEditor.hide();
        subDiv.children('.ivr-audio-upload').hide();    
        subDiv.children('.ivr-Menu-read-text').hide();
        subDiv.children('.ivr-mp3-url').hide();
        subDiv.children('.ivr-record-audio').hide();
    }

    function testVoice(voice, language, text) {
        if ($("#test_voice_iframe_wrapper").length == 0) {
            $("body").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
                                '<form id="test_voice_form" action="test_voice.php" method="post" target="test_voice_iframe">' +
                                    '<input type="hidden" name="voice" id="voice" />' +
                                    '<input type="hidden" name="language" id="language" />' +
                                    '<input type="hidden" name="text" id="text" />' +
                                    '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                '</form>' +
                                '<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
                            '</form>'
            );
        }

        $("#test_voice_iframe_wrapper #voice").val(voice);
        $("#test_voice_iframe_wrapper #language").val(language);
        $("#test_voice_iframe_wrapper #text").val(text);
        $('#test_voice_iframe_wrapper #submit').click()
     }

     var closeBtnHtml = '<a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a>';

     function SaveContent(obj, content_type) {
        var thisBlock = $(obj).closest('.ivr-Menu');
        switch (content_type) {
            case "Text_mail":
            {
                var voice_text = $(thisBlock).find('#readtxt_mail').val();
                var voice = $(thisBlock).find('#voice').val();
                var language = $(thisBlock).find('#language').val();
                $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('.ivr-Menu-close-button').click();

                $(thisBlock).find('.content').val(voice_text);
                $(thisBlock).find('.type').val("Text");
                $(thisBlock).find('.voice').val(voice);
                $(thisBlock).find('.language').val(language);

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#txt').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                break;
            }
            case "MP3_URL":
            {

                var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('.ivr-Menu-close-button').click();

                $(thisBlock).find('.content').val(mp3_url);
                $(thisBlock).find('.type').val("MP3_URL");

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#mp3_url').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                break;
            }
        }
    }

    function removeSelectedOption(obj) {
        promptMsg('Are you sure ?', function() {
          var thisBlock = $(obj).closest('.ivr-Menu');
          $(thisBlock).find(".ttsMwCloseBtn").remove();

          $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

          $(thisBlock).find('.content').val('');
          $(thisBlock).find('.type').val('');
          $(thisBlock).find('.voice').val('');
          $(thisBlock).find('.language').val('');
        });
    }

    function UploadFile(obj)
    {
        var status = $(obj).closest('.explanation').find('#statusUpload');
        var fileNameStatus = $(obj).closest('.explanation').find('#voicefilename');
        var fileNameStatusWrapper = $(obj).closest('.explanation').find('#voicefilenameWrapper');
        
        new AjaxUpload(obj, {
            
            
            action: 'act_settings.php?act=uploadMp3',
            name: 'uploadfile',
            onSubmit: function(file, ext){
                 if (! (ext && /^(mp3|wma)$/.test(ext))){ 
                    // extension is not allowed 
                    status.text('Only MP3 files are allowed');
                    return false;
                }
                (fileNameStatus).html('Uploading...');
            },
            onComplete: function(file, response){
                //On completion clear the status
                
                //Add uploaded file to list
                
                if(response!="error"){
                    $(fileNameStatusWrapper).css('display' ,'block');
                    $(fileNameStatus).html(response);
                    var thisBlock = $(obj).closest('.ivr-Menu');

                    $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                    $(thisBlock).find('#upload_mp3').addClass('ivr-Menu-Selected');
                    $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                    $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                    $(thisBlock).find('.content').val(response);
                    $(thisBlock).find('.type').val("Audio");
                    $(thisBlock).find('.ivr-Menu-close-button').click();

                    $(thisBlock).find(".ttsMwCloseBtn").remove();
                    $(thisBlock).find('#upload_mp3').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                } else{
                }
            }
        });
            
    }

    var recordingId = "";
    function recordAudio(obj) {
        if ($(obj).val() == "Call Me") {
            recordingId = createUUID();
            var record_from = $(obj).parents('.ivr-Menu').find('#record_from').val();
            var record_to = $(obj).parents('.ivr-Menu').find('#record_to').val();

            if (record_from == "") {
                errMsgDialog("Please select Caller ID.");
                return false;
            }

            if (record_to == "") {
                errMsgDialog("Please enter your phone number.")
                return false;
            }

            $(obj).val('Stop');

            if ($("#record_audio_iframe_wrapper").length == 0) {
                $("body").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
                                    '<form id="record_audio_form" action="../record_audio.php" method="post" target="record_audio_iframe">' +
                                        '<input type="hidden" name="recordingId" id="recordingId" />' +
                                        '<input type="hidden" name="record_from" id="record_from" />' +
                                        '<input type="hidden" name="record_to" id="record_to" />' +
                                        '<input type="submit" name="submit" id="submit" value="Submit" />' +
                                    '</form>' +
                                    '<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
                                '</form>'
                );
            }

            $("#record_audio_iframe_wrapper #recordingId").val(recordingId);
            $("#record_audio_iframe_wrapper #record_from").val(record_from);
            $("#record_audio_iframe_wrapper #record_to").val(record_to);
            $('#record_audio_iframe_wrapper #submit').click()
        }
        else {
            $(obj).val('Please wait...');
            $("#record_audio_iframe").contents().find("#disconnectBtn").click();

            setTimeout(function() {
                $.post("admin_ajax_handle.php", { func: "GET_RECORDING", recordingId: recordingId },  function(response) {
                    var thisBlock = $(obj).closest('.ivr-Menu');

                    $(thisBlock).find('#recordedAudioSavedWrapper').css('display' ,'block');
                    if (response) {
                        $(thisBlock).find('#recordedAudioSavedWrapper').html(response.playable);
                        
                        $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                        $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                        $(thisBlock).find('.content').val(response.url);
                        $(thisBlock).find('.type').val("RECORD_AUDIO");
                        $(thisBlock).find('.ivr-Menu-close-button').click();

                        $(thisBlock).find(".ttsMwCloseBtn").remove();
                        $(thisBlock).find('#record_audio').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                    }
                    else {
                        errMsgDialog('Audio was not recorded, please try again.');
                        $(thisBlock).find('#recordedAudioSavedWrapper').html('');
                    }
                    $(obj).val('Call Me');
                }, "json");
            }, 2000);
        }
     }

     function createUUID() {
        // http://www.ietf.org/rfc/rfc4122.txt
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";

        var uuid = s.join("");
        return uuid;
    }
</script>
</body>
</html>