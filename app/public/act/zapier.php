<?php
require_once('include/util.php');

if (!function_exists('http_response_code')) {
    function http_response_code($code = NULL) {

        if ($code !== NULL) {

            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                    exit('Unknown http status code "' . htmlentities($code) . '"');
                    break;
            }

            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

            header($protocol . ' ' . $code . ' ' . $text);

            $GLOBALS['http_response_code'] = $code;

        } else {

            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);

        }

        return $code;

    }
}

if(!isset($_REQUEST['action']) || $_REQUEST['action']==""){
    genericFail("No Method Specified");
}else{
    switch($_REQUEST['action']){
        case "subscribe":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);
            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                $db = new DB();
                $status = $db->authUserAPI($username,md5($password));
                if($status[0]){
                    $company_id = urldecode($_REQUEST['id']);
                    $campaign_id = urldecode($_REQUEST['campaign_id']);
                    if(!isset($company_id) || $company_id==""){
                        header('HTTP/1.1 401 Unauthorized');
                        genericFail("Missing id");
                    }else{
                        $data = json_decode(file_get_contents("php://input"));
                        $db = new DB();
                        $stmt = $db->customExecute("SELECT * FROM zapier_subscriptions WHERE `subscription_url` = ?");
                        $stmt->execute(array($data->subscription_url));
                        if(count($stmt->fetchAll(PDO::FETCH_OBJ))>0){
                            http_response_code(409);
                        }else{
                            http_response_code(201);
                            $stmt = $db->customExecute("INSERT INTO zapier_subscriptions (`event`,`subscription_url`,`target_url`,`company_id`,`campaign_id`,`user_id`) VALUES (?,?,?,?,?,?)");
                            $stmt->execute(array($data->event, $data->subscription_url, $data->target_url, $company_id, $campaign_id, $status[1]));
                            $id = $db->lastInsertId();
                            header('Content-type: application/json');
                            die(json_encode(array("id"=>$id)));
                        }
                    }
                }else{
                    switch($status[1]){
                        case 1:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Invalid Username or Password");
                            break;
                        }
                        case 2:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Account does not exist!");
                            break;
                        }
                    }
                }
            }

            //header('HTTP/1.1 201 Created');

            break;
        }

        case "unsubscribe":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);
            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                $db = new DB();
                $status = $db->authUserAPI($username,md5($password));
                if($status[0]){
                    $data = json_decode(file_get_contents("php://input"));
                    $stmt = $db->customExecute("DELETE FROM zapier_subscriptions WHERE subscription_url = ? AND `target_url` = ?");
                    $stmt->execute(array($data->subscription_url,$data->target_url));
                    http_response_code(200);
                }else{
                    switch($status[1]){
                        case 1:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Invalid Username or Password");
                            break;
                        }
                        case 2:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Account does not exist!");
                            break;
                        }
                    }
                }
            }
            break;
        }

        case "poll_outgoing_call_browserphone":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);
            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                $db = new DB();
                $status = $db->authUserAPI($username,md5($password));
                if($status[0]){
                    $company_id = urldecode($_REQUEST['id']);
                    if(!isset($company_id) || $company_id==""){
                        header('HTTP/1.1 401 Unauthorized');
                        genericFail("Missing id");
                    }else{
                        // Available Fields: CallSid, DateCreated (timezone accounted for),
                        //   Status, Duration, Tracking Number (From), To, Recording, Phone Code
                        $limit = 0;
                        $calls = $db->cget_all_calls($company_id,true);
                        $calls = @$calls[0];
                        $_phone_codes = $db->getPhoneCodes($company_id,2);
                        $phone_codes = array();

                        if(is_array($_phone_codes) || is_object($_phone_codes))
                            foreach($_phone_codes as $phone_code)
                                $phone_codes[$phone_code->idx] = $phone_code->name;

                        $retn_calls = array();
                        if(is_array($calls) || is_object($calls))
                            foreach($calls as $call){
                                if($limit>=10)
                                    continue;
                                $call = (object)$call;
                                $_obj = new stdClass();
                                $_obj->CallSid = $call->CallSid;
                                $_date = new DateTime($call->DateCreated,new DateTimeZone("UTC"));
                                $_date->setTimezone(new DateTimeZone($TIMEZONE));
                                $_obj->DateCreated = $_date->format("D n\/d Y g\:iA T");
                                $_obj->Status = ucfirst($call->DialCallStatus);
                                $_obj->Duration = $call->DialCallDuration != null ? $call->DialCallDuration : "0";
                                $_obj->TrackingNumber = $call->CallFrom;
                                $_obj->To = $call->CallTo;
                                $_obj->Recording = $call->RecordingUrl;
                                $_obj->PhoneCode = @$phone_codes[$call->PhoneCode] != "" ? $phone_codes[$call->PhoneCode] : "None" ;
                                $retn_calls[] = $_obj;
                                $limit++;
                            }

                        if(count($retn_calls)>0)
                        {
                            shuffle($retn_calls);
                            header('Content-type: application/json');
                            die(json_encode($retn_calls));
                        }else{
                            header("HTTP/1.0 204 No Content");
                        }
                    }
                }else{
                    switch($status[1]){
                        case 1:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Invalid Username or Password");
                            break;
                        }
                        case 2:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Account does not exist!");
                            break;
                        }
                    }
                }
            }
            break;
        }

        case "poll_outgoing_call_autodialer":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);
            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                $username = urldecode($_REQUEST['user']);
                $password = urldecode($_REQUEST['pass']);
                if(!isset($username) || $username == "" ||
                    !isset($password) || $password == "")
                {
                    header('HTTP/1.1 401 Unauthorized');
                    genericFail("Invalid Username or Password");
                }else{
                    $db = new DB();
                    $status = $db->authUserAPI($username,md5($password));
                    if($status[0]){
                        $campaign_id = urldecode($_REQUEST['id']);
                        if(!isset($campaign_id) || $campaign_id==""){
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Missing id");
                        }else{
                            session_start();
                            if($db->isUserAdmin($status[1]))
                                $_SESSION['permission'] = 1;
                            else
                                $_SESSION['permission'] = 0;
                            $_SESSION['user_id'] = $status[1];

                            require_once("include/ad_auto_dialer_files/lib/ad_ad_lib_funcs.php");
                            // Available Fields:
                            $limit = 0;
                            $data = ad_ad_get_logs(1,4000,$campaign_id);

                            $retn_calls = array();
                            foreach($data as $log){
                                if($limit>=10)
                                    continue;
                                $log = (object)$log;

                                $stmt = $db->customExecute("SELECT * FROM ad_advb_cl_contacts WHERE phone_number = ?");
                                $stmt->execute(array($log->to));
                                $contact = $stmt->fetch(PDO::FETCH_OBJ);

                                $_obj = new stdClass();
                                $_date = new DateTime($log->date,new DateTimeZone("UTC"));
                                $_date->setTimezone(new DateTimeZone($TIMEZONE));
                                $_obj->DateCreated = $_date->format("D n\/d Y g\:iA T");
                                $pcs = $db->getPhoneCodes($log->company_id,2);
                                $_obj->PhoneCode = "None";
                                if(is_array($pcs) || is_object($pcs))
                                    foreach($pcs as $pc){
                                        if($pc->idx == $log->code)
                                            $_obj->PhoneCode = $pc->name;
                                    }
                                $_obj->CampaignNumber = $log->from;
                                $_obj->PhoneNumber = $log->to;
                                $_obj->Duration = $log->duration==""?0:(int)$log->duration;
                                $_obj->Status = $log->response;
                                $_obj->Recording = $log->recording_url;
                                $_obj->BusinessName = $contact->business_name;
                                $_obj->FirstName = $contact->first_name;
                                $_obj->LastName = $contact->last_name;
                                $_obj->Email = $contact->email;
                                $_obj->Address = $contact->address;
                                $_obj->City = $contact->city;
                                $_obj->State = $contact->state;
                                $_obj->Zip = $contact->zip;
                                $_obj->Website = $contact->website;
                                $_obj->OptOut = $contact->opt_out;
                                $retn_calls[] = $_obj;
                                $limit++;
                            }
                            if(count($retn_calls)>0)
                            {
                                shuffle($retn_calls);
                                header('Content-type: application/json');
                                die(json_encode($retn_calls));
                            }else{
                                header("HTTP/1.0 204 No Content");
                            }
                        }
                    }else{
                        switch($status[1]){
                            case 1:{
                                header('HTTP/1.1 401 Unauthorized');
                                genericFail("Invalid Username or Password");
                                break;
                            }
                            case 2:{
                                header('HTTP/1.1 401 Unauthorized');
                                genericFail("Account does not exist!");
                                break;
                            }
                        }
                    }
                }
            }
            break;
        }

        case "poll":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);
            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                $db = new DB();
                $status = $db->authUserAPI($username,md5($password));
                if($status[0]){
                    $company_id = urldecode($_REQUEST['id']);
                    if(!isset($company_id) || $company_id==""){
                        header('HTTP/1.1 401 Unauthorized');
                        genericFail("Missing id");
                    }else{
                        // Available Fields: CallSid, DateCreated (timezone accounted for),
                        //  Country, State, Zip, City, Status, Duration, From, To, Tracking Number,
                        //  Recording URL
                        $db = new DB();
                        $limit = 0;
                        $calls = $db->cget_all_calls($company_id);
                        $calls = $calls[0];

                        $retn_calls = array();
                        foreach($calls as $call){
                            if($limit>=10)
                                continue;
                            $call = (object)$call;
                            //print_r($call);
                            $_obj = new stdClass();
                            $_obj->CallSid = $call->CallSid;
                            $_date = new DateTime($call->DateCreated,new DateTimeZone("UTC"));
                            $_date->setTimezone(new DateTimeZone($TIMEZONE));
                            $_obj->DateCreated = $_date->format("D n\/d Y g\:iA T");
                            $_obj->CallerID = $call->CallerID;
                            $_obj->Country = $call->FromCountry;
                            $_obj->State = $call->FromState;
                            $_obj->Zip = $call->FromZip;
                            $_obj->City = $call->FromCity;
                            $_obj->Status = ucfirst($call->DialCallStatus);
                            $_obj->Duration = $call->DialCallDuration != null ? $call->DialCallDuration : "0";
                            $_obj->From = $call->CallFrom;
                            $_obj->To = $call->DialCallTo;
                            $_obj->TrackingNumber = $call->CallTo;
                            $_obj->Recording = $call->RecordingUrl;
                            $retn_calls[] = $_obj;
                            $limit++;
                        }
                        if(count($retn_calls)>0)
                        {
                            shuffle($retn_calls);
                            header('Content-type: application/json');
                            die(json_encode($retn_calls));
                        }else{
                            header("HTTP/1.0 204 No Content");
                        }
                    }
                }else{
                    switch($status[1]){
                        case 1:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Invalid Username or Password");
                            break;
                        }
                        case 2:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Account does not exist!");
                            break;
                        }
                    }
                }
            }
            break;
        }

        case "get_autodialer_campaigns":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);

            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                $db = new DB();
                $status = $db->authUserAPI($username,md5($password));
                if($status[0]){
                    $retn_campaigns = array();
                    if($db->isUserAdmin($status[1])){
                        $stmt = $db->customExecute("SELECT * FROM ad_ad_campaigns;");
                        $stmt->execute();
                        $data = $stmt->fetchAll(PDO::FETCH_OBJ);
                        foreach($data as $campaign){
                            $retn_campaigns[] = array("id"=>$campaign->idx,"name"=>$campaign->campaign_name);
                        }
                    }else{

                    }
                    if(count($retn_campaigns)>0){
                        header('Content-type: application/json');
                        die(json_encode($retn_campaigns));
                    }else{
                        header("HTTP/1.0 204 No Content");
                    }
                }else{
                    switch($status[1]){
                        case 1:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Invalid Username or Password");
                            break;
                        }
                        case 2:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Account does not exist!");
                            break;
                        }
                    }
                }
            }
            break;
        }

        case "get_tracking_numbers":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);

            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                if($company_id = ""){
                    header('HTTP/1.1 401 Unauthorized');
                    genericFail("Company ID is required");
                }
                $db = new DB();
                $status = $db->authUserAPI($username,md5($password));
                if($status[0]){
                    $retn_numbers = array();

                    if($db->isUserAdmin($status[1]))
                        $companies = $db->getAllCompanies();
                    else
                        $companies = $db->getAllCompaniesForUser($status[1]);

                    foreach($companies as $company){
                        $numbers = $db->getCompanyNumIntlForUser($company['idx'],$status[1]);
                        foreach($numbers as $number){
                            if($number[1])
                                $retn_numbers[] = array("formatted"=>$number[0],"raw"=>"+".$number[0]);
                            else
                                $retn_numbers[] = array("formatted"=>Util::format_phone_us($number[0]),"raw"=>"+1".$number[0]);
                        }
                    }
                    if(count($retn_numbers)>0){
                        header('Content-type: application/json');
                        die(json_encode($retn_numbers));
                    }else{
                        header("HTTP/1.0 204 No Content");
                    }
                }else{
                    switch($status[1]){
                        case 1:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Invalid Username or Password");
                            break;
                        }
                        case 2:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Account does not exist!");
                            break;
                        }
                    }
                }
            }
            break;
        }

        case "trigger_call":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);

            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                $db = new DB();
                $status = $db->authUserAPI($username,md5($password));
                $payload = json_decode(file_get_contents('php://input'));
                if($status[0]){
                    $company_access_check = false;
                    if($db->isUserAdmin($status[1])){
                        $company_access_check = true;
                    }else{
                        if($db->isUserInCompany($payload->company,$status[1])){
                            $company_access_check = true;
                        }
                    }

                    if($company_access_check){
                        $numbers = $db->getCompanyNumIntl($payload->company);
                        $number = $numbers[rand(0,count($numbers)-1)];
                        if($number[1])
                            $company_num = $number[0];
                        else
                            $company_num = "+1".$number[0];

                        if(!$db->startsWith($payload->caller,"1")){
                            $caller = "+1".$payload->caller;
                        }else{
                            $caller = "+".$payload->caller;
                        }

                        if(!$db->startsWith($payload->from,"1")){
                            $from = "+1".$payload->from;
                        }else{
                            $from = "+".$payload->from;
                        }

                        require_once('include/Services/Twilio.php');
                        global $RECORDINGS, $AccountSid, $AuthToken;
                        //Call the contact
                        $client = new Services_Twilio($AccountSid,$AuthToken);
                        $call = $client->account->calls->create(
                            $payload->selected_number,
                            $company_num,
                            dirname(s8_get_current_webpage_uri()).'/include/static/zapier-call-action-twiml.php?company_num='.urlencode($company_num).
                                '&lead_number='.urlencode($caller).'&message='.urlencode($payload->whisper_message)
                        );


                        genericSuccess(array("company_number"=>$company_num,"company_id"=>$payload->company));
                    }else{
                        header('HTTP/1.1 401 Unauthorized');
                        genericFail("User does not have access to that company");
                    }
                }else{
                    switch($status[1]){
                        case 1:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Invalid Username or Password");
                            break;
                        }
                        case 2:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Account does not exist!");
                            break;
                        }
                    }
                }
            }
            break;
        }

        case "auth":{
            $username = urldecode($_REQUEST['user']);
            $password = urldecode($_REQUEST['pass']);

            if(!isset($username) || $username == "" ||
                !isset($password) || $password == "")
            {
                header('HTTP/1.1 401 Unauthorized');
                genericFail("Invalid Username or Password");
            }else{
                $db = new DB();
                $status = $db->authUserAPI($username,md5($password));
                if($status[0]){
                    $retn_companies = array();
                    if($db->isUserAdmin($status[1])){
                        $companies = $db->getAllCompanies();
                    }else{
                        $companies = $db->getAllCompaniesForUser($status[1]);
                    }
                    foreach($companies as $company){
                        $retn_companies[] = array("id"=>$company['idx'],"name"=>$company['company_name']);
                    }
                    if(count($retn_companies)>0){
                        header('Content-type: application/json');
                        die(json_encode($retn_companies));
                    }else{
                        header("HTTP/1.0 204 No Content");
                    }
                }else{
                    switch($status[1]){
                        case 1:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Invalid Username or Password");
                            break;
                        }
                        case 2:{
                            header('HTTP/1.1 401 Unauthorized');
                            genericFail("Account does not exist!");
                            break;
                        }
                    }
                }
            }
            break;
        }

        default:{
            genericFail("No Such Method");
            break;
        }
    }
}





// -------------------------------------------------------------------------------------- //
function genericFail($msg = null){
    $res = array("result"=>"fail");

    if($msg!=null)
        $res = array("result"=>"fail","reason"=>$msg);

    header('Content-type: application/json');
    die(json_encode($res));
}
function genericSuccess($data = null){
    if($data==null){
        header('Content-type: application/json');
        die(json_encode(array("result"=>"success")));
    }else{
        $arr = array("result"=>"success");
        $arr = array_merge($arr,$data);
        header('Content-type: application/json');
        die(json_encode($arr));
    }
}
