<?php

//Check for config..
if (!file_exists("../include/config.php")) {
	die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
}
session_start();
require_once('../include/util.php');


$page = "adminpage";
$db = new DB();
//Database settings
$con = mysqli_connect($DB_HOST,$DB_USER,$DB_PASS);
mysqli_select_db($con, $DB_NAME );


//Pre-load Checks
if (!isset($_SESSION['user_id'])) {		header("Location: ../login.php");		exit;	}
$cId='0';
if (isset($_GET['cId']))
{
	$cId = addslashes($_GET['cId']);
}

$where = isset($_GET['from_cId']) ? " WHERE companyId = '".addslashes($_GET['from_cId'])."' AND temporary = '0' " : " WHERE companyId = '".$cId."' ";

mysqli_query($con, "DELETE FROM call_ivr_widget WHERE companyId = '".$cId."' AND temporary = '1'");

$ids_inserted = array();
$sql = mysqli_query($con, "SELECT * FROM call_ivr_widget $where AND meta_data != 'agent_ivr_master_voicemail'");
while ($row = mysqli_fetch_array($sql)) {
	mysqli_query($con, "INSERT INTO call_ivr_widget(pId, flowtype, content_type, content, nId, data, companyId, keypress, meta_data, temporary)
		values('".$row['pId']."', '".$row['flowtype']."', '".$row['content_type']."', '".addslashes($row['content'])."', '".$row['nId']."', '".$row['data']."', '".$cId."', '".$row['keypress']."', '".$row['meta_data']."', 1)");

	$inserted_id = mysqli_insert_id($con);
	$ids_inserted[$row['wId']] = $inserted_id;

	$sql2 = mysqli_query($con, "SELECT * FROM call_ivr_data WHERE wId = '".$row['wId']."'");
	while ($row2 = mysqli_fetch_array($sql2)) {
		mysqli_query($con, "INSERT INTO call_ivr_data(wId, meta_key, meta_data)
			values('".$inserted_id."', '".addslashes($row2['meta_key'])."', '".addslashes($row2['meta_data'])."')");
	}

	$sql2 = mysqli_query($con, "SELECT * FROM call_ivr_multiple_numbers WHERE wId = '".$row['wId']."'");
	while ($row2 = mysqli_fetch_array($sql2)) {
		mysqli_query($con, "INSERT INTO call_ivr_multiple_numbers(wId, `number`)
			values('".$inserted_id."', '".$row2['number']."')");
	}

	$sql2 = mysqli_query($con, "SELECT * FROM call_ivr_round_robin WHERE wId = '".$row['wId']."'");
	while ($row2 = mysqli_fetch_array($sql2)) {
		mysqli_query($con, "INSERT INTO call_ivr_round_robin(wId, `number`)
			values('".$inserted_id."', '".$row2['number']."')");
	}

	if ($row['flowtype'] == "AgentIVR") {
		$content = json_decode($row['content'], true);
		if (!empty($content['master_voicemail_wId'])) {
			$sql2 = mysqli_query($con, "SELECT * FROM call_ivr_widget WHERE pId = '".$row['wId']."' AND flowtype = 'VoicemailWithoutRing'");
			$row2 = mysqli_fetch_array($sql2);
			$sql3 = mysqli_query($con, "SELECT * FROM call_ivr_widget WHERE pId = '".$inserted_id."' AND flowtype = 'VoicemailWithoutRing'");
			$row3 = mysqli_fetch_array($sql3);

			if (!empty($row2)) {
				if (empty($row3)) {
					mysqli_query($con, "INSERT INTO call_ivr_widget(pId, flowtype, content_type, content, nId, data, companyId, keypress, meta_data, temporary)
					values('".$row2['pId']."', '".$row2['flowtype']."', '".$row2['content_type']."', '".addslashes($row2['content'])."', '".$row2['nId']."', '".$row2['data']."', '".$cId."', '".$row2['keypress']."', '".$row2['meta_data']."', 1)");
					$voicemail_id = mysqli_insert_id($con);
				}
				else {
					$voicemail_id = $row3['wId'];
				}
				$content['master_voicemail_wId'] = $voicemail_id;
				mysqli_query($con, "UPDATE call_ivr_widget set content ='".json_encode($content)."' WHERE wId = ".$inserted_id);

				$sql4 = mysqli_query($con, "SELECT * FROM call_ivr_data WHERE wId = '".$row2['wId']."'");
				while ($row4 = mysqli_fetch_array($sql4)) {
					mysqli_query($con, "INSERT INTO call_ivr_data(wId, meta_key, meta_data)
						values('".$voicemail_id."', '".addslashes($row4['meta_key'])."', '".addslashes($row4['meta_data'])."')");
				}
			}
		}
	}

	if ($row['flowtype'] == "Voicemail") {

	}
}

foreach ($ids_inserted as $wId => $tempWId) {
	mysqli_query($con, "UPDATE call_ivr_widget SET nId = '".$tempWId."' WHERE companyId = '".$cId."' AND temporary = 1 AND nId = '".$wId."'");
	mysqli_query($con, "UPDATE call_ivr_widget SET pId = '".$tempWId."' WHERE companyId = '".$cId."' AND temporary = 1 AND pId = '".$wId."'");
}

// Remove orphaned metadata....
$stmt = $db->customExecute("DELETE FROM call_ivr_data WHERE `wId` NOT IN (SELECT tl.`wId` FROM call_ivr_widget tl GROUP BY tl.`wId`)");
$stmt->execute();
$stmt = $db->customExecute("DELETE FROM call_ivr_round_robin WHERE `wId` NOT IN (SELECT tl.`wId` FROM call_ivr_widget tl GROUP BY tl.`wId`)");
$stmt->execute();
$stmt = $db->customExecute("DELETE FROM call_ivr_multiple_numbers WHERE `wId` NOT IN (SELECT tl.`wId` FROM call_ivr_widget tl GROUP BY tl.`wId`)");
$stmt->execute();

require_once('ivrblock.php');

?>
<!doctype html>
<html lang="us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>IVR -  Customized</title>

<link href="../css/style.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/ivr2.css" />

<link href="css/jqcss/jquery-ui-1.10.3.custom.css" rel="stylesheet">

<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>

<link href="../player/skin/jplayer-black-and-blue.css<?php echo "?" . $build_number; ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../player/jquery.jplayer.min.js<?php echo "?" . $build_number; ?>"></script>

<link href="../css/tooltipster.css<?php echo "?" . $build_number; ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.tooltipster.min.js<?php echo "?" . $build_number; ?>"></script>

<script type="text/javascript" src="../js/functions.js" ></script>


    <style  type="text/css">
body {
	text-align: left !important
}
.warpperIVR{ width:830px; margin:15px;}
.blockRight {
	width: 200px;
	float: right;
	text-align: center;
}
#flotingDiv {
	position: relative;
}
.blockLeft {
	width: 600px;
	float: left;
}
#statusUpload
{
	margin-left:10px; width:400px !important; 
}
.fileupload{

	margin: 10px 0px  0px 20px !important;
}
.notDrg{
	-moz-box-shadow: 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: 0px 1px 0px 0px #ffffff;
	box-shadow: 0px 1px 0px 0px #ffffff;
	background-color: transparent;
	-webkit-border-top-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	border-top-left-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-bottom-left-radius: 10px;
	text-indent: 0px;
	
	display: inline-block;
	color: #008ee8;
	font-family: "Titillium999", "Trebuchet MS", Arial, sans-serif;
	font-size: 18px;
	font-weight: normal;
	font-style: normal;
	height: 40px;
	line-height: 40px;
	width: 140px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	margin: 5px;
	text-shadow: 1px 1px 0 #fff;
	
   
    
    
    
}
.drg {
	-moz-box-shadow: 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: 0px 1px 0px 0px #ffffff;
	box-shadow: 0px 1px 0px 0px #ffffff;
	background-color: transparent;
	-webkit-border-top-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	border-top-left-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-bottom-left-radius: 10px;
	text-indent: 0px;
	border: 2px solid #dcdcdc;
	display: inline-block;
	color: #777777;
	font-family: "Titillium999", "Trebuchet MS", Arial, sans-serif;
	font-size: 18px;
	font-weight: normal;
	font-style: normal;
	
	height: 30px;
	line-height: 30px;
	width: 140px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	margin: 5px;
}
.drg:active {
	position: relative;/* top: 1px; */
}
.drp {
	-moz-box-shadow: 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: 0px 1px 0px 0px #ffffff;
	box-shadow: 0px 1px 0px 0px #ffffff;
	background-color: transparent;
	-webkit-border-top-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	border-top-left-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-bottom-left-radius: 10px;
	text-indent: 0px;
	border: 2px solid #dcdcdc;
	display: inline-block;
	color: #777777;
	
	font-family: "Titillium999", "Trebuchet MS", Arial, sans-serif;
	font-size: 18px;
	font-weight: normal;
	font-style: normal;
	
	height: 50px;
	line-height: 50px;
	width: 140px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	margin: 5px;
}
.drp2 {
	-moz-box-shadow: 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: 0px 1px 0px 0px #ffffff;
	box-shadow: 0px 1px 0px 0px #ffffff;
	background-color: transparent;
	-webkit-border-top-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	border-top-left-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-bottom-left-radius: 10px;
	text-indent: 0px;
	border: 2px solid #dcdcdc;
	display: inline-block;
	color: #00F;
	font-family: Arial;
	font-size: 17px;
	font-weight: bold;
	font-style: normal;
	height: 50px;
	line-height: 50px;
	width: 140px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	margin: 5px;
}
.droping {
	border: 2px solid #666666 !important;
	color: #333;
}


.block input.submit.mid { margin-left:430px;}

textarea {
    padding: 5px;
}
</style>
</head>
<body>
<div class="warpperIVR">
        
        
        
        
        <div class="blockLeft" id="blockLeft">
          <div class="block" id="blockMain">
            <div class="block_head">
              <div class="bheadl"></div>
              <div class="bheadr"></div>
              <h2>Your Call Flow </h2>
              <div style="display:inline-block; float: right;">
                <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="showFlow()">SHOW FLOW</a></h2>
              </div>
            </div>
            <!-- .block_head ends -->
            
            
            <div class="block_content"> <span>Drag any option to call flow.</span><br/>
            <div
        	class="message success" id="flow_msg_parent" style="width: 500px !important; margin: 10px; display:none"> </div>
              <br/>
              <?php $mainBlock = getStartId($cId);
			  $wId = $mainBlock['wId'];
			  ?>
              <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="empty" data-pid="" data-nid=""  id="drop_here_obj"  data-wid=""   class="drp"><?php echo $blockTitle[$mainBlock['flowtype']] ?></a> 
              
              <!-- .block_content ends --> 
            </div>
            <div class="bendl"></div>
            <div class="bendr"></div>
          </div>

          <?php  /////////////Sub Blocks Starts //////// 
          	
		  	
		  	getWidget($wId,'-');
		  
           /////////////Sub Blocks Ends //////// ?>
        </div>
        <div class="blockRight" id="blockRight">
          <div class="block" id="flotingDiv">
            <div class="block_head">
              <div class="bheadl"></div>
              <div class="bheadr"></div>
              <h2>MENU OPTIONS </h2>
            </div>
            <!-- .block_head ends -->
            
            <div class="block_content">
              	<?php /*?><div class="message warning" style="width: 93%;margin: 0 auto;margin-top: 8px;"><p>Please select a company to view.</p></div><?php */?>
              	<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="Greetings"  class="drg" >Greetings</a>
              	<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="Menu" data-flowtitle="Menu"  class="drg" >Menu</a>
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="Dial"  data-flowtitle="Dial" class="drg" >Dial</a>
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="RoundRobin"  data-flowtitle="Round Robin" class="drg" >Round Robin</a>
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="MultipleNumbers"  data-flowtitle="Multiple Numbers" class="drg" style="font-size: 16px;" >Multiple Numbers</a>
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="SMS"  data-flowtitle="SMS" class="drg" >SMS</a> 
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="Voicemail"  data-flowtitle="Voicemail" class="drg" >Voicemail</a> 
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="OptIn"  data-flowtitle="Opt-In" class="drg" >Opt-In</a> 
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="SingleAgent"  data-flowtitle="Agent" class="drg" >Agent</a> 
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="AgentIVR"  data-flowtitle="Agent IVR" class="drg" >Agent IVR</a> 
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="BusinessHours"  data-flowtitle="Business Hours" class="drg" >Business Hours</a> 
				<a href="javascript:void(0)" onClick="showContent(this)" data-flowtype="Hangup"  data-flowtitle="Hangup" class="drg" >Hangup</a> 
              	<a href="javascript:void(0)" onClick="showFlow()" class="notDrg" >Show flow</a></div>
            <!-- .block_content ends -->
            
            <div class="bendl"></div>
            <div class="bendr"></div>
          </div>
        </div>
      </div>
      
<script type="text/javascript" >

	var companyId = '<?php echo $cId ?>';
	var drpOptions ='';
	
	
				
$(document).ready(function()
{
	///////////// hide show audio choices ////////////////
	$(".ivr-Menu-editor").hide();
	$(".ivr-audio-upload").hide();
	$(".ivr-Menu-read-text").hide();
	///////////// hide show audio choices ////////////////

	$(".drp" ).each(function(index, element) {
        if($(element).text() == "Multiple Numbers")
            $(element).attr("style","font-size:16px");
        else
            $(element).attr("style","");
	});
		

	/////////////////// drag options ////////
    $( ".drg" ).draggable({ 
		scope: "flow_opts",						
		helper: 'clone',
		appendTo: 'body',
		cursor: "move",
		revert: true,
		revertDuration: 400
	});
    
	/////////////////// drop options ////////
	 drpOptions = {
		scope: "flow_opts",
		activeClass : "droping",
		drop: function( event, ui )
			{
				RemoveShowFlow();
				var $drag = $(ui.draggable);
				$drag.draggable( "option", "revertDuration", 400);

				dataRep = true;				
				
				if (  $(this).html() == "Hangup")
				{
					var wId= $(this).data("wid");
					var pId= $(this).data("pid");
					var nId= $(this).data("nid")
					var flowtype= $(this).data("flowtype")
					
						$.ajax({
						  type: "POST",
						  url: "process_ivr.php?companyId="+companyId,
						   dataType: "html",
						  data:{wId: nId,pId:pId,nId:nId , flowtype:flowtype ,act:'REMOVE-Hangup' }
						})
						dataRep = true;
						$(this).html('Drop here');
				}


				var replace = false;
				
				if ($(this).data("agentivr-only") != "yes" && $drag.html() == "Agent") {
					parent.errMsgDialog('You can drop Agent applets only inside Agent IVR!');
					return false;
				}
				
				if ($(this).data("agentivr-only") == "yes") {
					if ($drag.html() != "Agent") {
						parent.errMsgDialog('You can only drop an Agent applet here!');
						return false;
					}
					else if ($(this).html() == "Agent") {
						parent.errMsgDialog('You already added an Agent applet here!');
						return false;
					}
					else {

					}
				}
				
				if ($(this).html() != "Drop here")
				{
					if ($(this).data("voicemail-only") == "yes") {
						if ($drag.html() != "Voicemail") {
							parent.errMsgDialog('You can only drop a Voicemail applet here!');
							return false;
						}
						else if ($(this).html() == "Voicemail") {
							parent.errMsgDialog('You already added a Voicemail applet here!');
							return false;
						}
						else {

						}
					}
					else if ($drag.html() == "Hangup") {
						parent.errMsgDialog("You can't do that!");
						return false;
					}
					else {
						$drag.draggable( "option", "revertDuration", 0);

						var this_el = this;
						var drag_el = $drag;

						//dataRep	= confirm('Do you want to replace previous item?'); //$( "#dialog" ).dialog( "open" );
						parent.promptMsg('Are you sure you want to replace ' + $(this).html() + ' with ' + $drag.html() + '?', function() {
							continueReplacement(this_el, drag_el);
						});
						
						return false;
					}
				}
				
				if (dataRep) 
				{
					$drag.draggable( "option", "revertDuration", 0);

					var dho_el = this;
					
					$(this).html($drag.html());
										
					var flowtype= $drag.data("flowtype");

					if (typeof($(dho_el).attr('data-agent-ivr-voicemail')) != "undefined")
						flowtype = "VoicemailWithoutRing";
					
					$(dho_el).data('flowtype' ,flowtype);							
					
					var MainContainer = $(dho_el).closest('.blockLeft');
					var thisBlock = $(dho_el).closest('.block');

					//$(MainContainer).append($('#'+flowtype +'-content').html());
					//data-flowtype="empty" data-pid="" data-nid=""  id="drop_here_obj"  data-wid=""  
					
					var wId= $(dho_el).data("wid");
					var pId= $(dho_el).data("pid");
					var nId= $(dho_el).data("nid");
					var PreviousObj= $(dho_el);
					var submenu= $(dho_el).data("submenu");
					var voicemail_only= $(dho_el).data("voicemail-only");
					var business_hours_only= $(dho_el).data("business-hours-only");
					var agentivr_only= $(dho_el).data("agentivr-only");
					var data_agent_ivr_voicemail= $(dho_el).data("agent-ivr-voicemail");
					var number= $(dho_el).data("number");

					var SubMenuQuery='';
					if ( submenu == 'yes' ) SubMenuQuery +='&submenu=yes&keypress=' + $(dho_el).closest('.row').find('#keypress').val();
					if ( agentivr_only == 'yes' ) SubMenuQuery +='&agent-applet=yes';
					if ( data_agent_ivr_voicemail == 'yes' ) SubMenuQuery +='&data_agent_ivr_voicemail=yes';
					if (replace) SubMenuQuery +='&replace=yes';
					if (voicemail_only == "yes") SubMenuQuery +='&voicemail_only=yes&number=' + number;
					if (business_hours_only == "yes") SubMenuQuery +='&business_hours_only=yes&number=' + number;

					var htmlData = $('#'+flowtype +'-content').clone(true,true).html();
					
					var timestamp = new Date().getTime();
					$.getJSON("process_ivr.php?companyId="+companyId+'&wId='+wId+'&pId='+pId+'&nId='+nId+'&flowtype='+flowtype+'&act=save'+SubMenuQuery+'&ts=' + timestamp, 
					function(jsdata) 
					{
						htmlData= htmlData.replace(/data-submenu="yes" data-pid=""/g,' data-submenu="yes" data-pid="'+jsdata.data['wId']+'"');
						htmlData= htmlData.replace(/data-agentivr-only="yes" data-pid=""/g,' data-agentivr-only="yes" data-pid="'+jsdata.data['wId']+'"');
						htmlData= htmlData.replace(/data-pid=""/g,'data-pid="'+jsdata.data['pId']+'"');
						htmlData= htmlData.replace(/data-wid=""/g,'data-wid="'+jsdata.data['wId']+'"');
						htmlData= htmlData.replace(/myId/g,jsdata.data['wId']);
						var voicemail_addition = ($(dho_el).html() == "Voicemail") ? ' data-data="' + number + '" ' : '';
						htmlData= htmlData.replace('="blockObj"',' ="blockObj-'+jsdata.data['wId']+'" ' + voicemail_addition);
						
						if ( flowtype !='Hangup' ) 	$(thisBlock).after(htmlData);

						if (replace) {
							var dho = thisBlock.next('div.block').find("#drop_here_obj").last();
							dho.html(keep_text);
							$(dho).data("wid", $(dho_el).data("wid"));
							$(dho).data("pid", $(dho_el).data("pid"));
							$(dho).data("nid", $(dho_el).data("nid"));
							$(dho).data("submenu", $(dho_el).data("submenu"));
						}

						//$( ".block" ).draggable({containment:  "#flowContainer",axis: "y"});
						
						
						if (jsdata.data['pId'] != '' )
						{
							$(PreviousObj).data('nid', jsdata.data['wId']);
						}	
						
							
							$(".drp" ).each(function(index, element) {
							  //$(element).removeClass('.drp');
                                if($(element).text() == "Multiple Numbers")
                                    $(element).attr("style","font-size:16px");
                                else
                                    $(element).attr("style","");
							    $(element).droppable(drpOptions);//$(element).bind('click', function (){alert('x')});
							  
							});

							$('input, textarea, select').bind('change keyup',function(e) {
								$("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
						        $("#save_call_flow_btn", window.parent.document).prop('disabled', false);
							});

							$("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
        					$("#save_call_flow_btn", window.parent.document).prop('disabled', false);
					});
				}
      }};
		
	$(".drp" ).droppable(drpOptions	);
	/////////////////////////  Drop Function ends////////////////
	
	setTimeout(function() {
		$('input, textarea, select').bind('change keyup',function(e) {
			triggerSaveChanges();
		});
	}, 1000);

	$("#save_call_flow_btn", window.parent.document).fadeTo(0, 0.6);
    $("#save_call_flow_btn", window.parent.document).prop('disabled', true);

    <?php if (isset($_GET['from_cId'])) { ?>
    	$("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
        $("#save_call_flow_btn", window.parent.document).prop('disabled', false);
    	parent.infoMsgDialog("Don't forget to update your numbers!")
    <?php } ?>

    setInterval(function() {
	    $("input#agents_use_extensions").each(function() {
	    	var parent = $(this).parents('.block');

	    	if ($(this).is(":checked")) {
	    		parent.find("input#agents_extension_length").prop("disabled", false);
	    		parent.find("input.extension").prop("disabled", false);
	    	}
	    	else {
	    		parent.find("input#agents_extension_length").prop("disabled", true);
	    		parent.find("input.extension").prop("disabled", true);
	    	}
	    });

    }, 500)
});

function SaveCallFlow(company_id)
{
	var unsavedChanges = [];
	var validation = true;
	var advanced_och = [];

	advanced_och = {
		enabled: $("#advanced_och").val(),
		voicemail: {
			enabled: ($("#advanced_och_voicemail").is(":checked") ? 1 : 0),
			content: $('input[name="advancedOCHoursVoicemail_content"]').val(),
			type: $('input[name="advancedOCHoursVoicemail_type"]').val(),
			voice: $('input[name="advancedOCHoursVoicemail_voice"]').val(),
			language: $('input[name="advancedOCHoursVoicemail_language"]').val()
		},
		weekdays: {}
	};

	$('input.advanced_och_weekdays').each(function() {
		var day = $(this).val();

		if (day != "") {
			advanced_och.weekdays[day] = {
				day: day,
				hour_open: $('select#advanced_och_hour_open_' + day + '').val(),
				minute_open: $('select#advanced_och_minute_open_' + day + '').val(),
				ampm_open: $('select#advanced_och_ampm_open_' + day + '').val(),
				hour_close: $('select#advanced_och_hour_close_' + day + '').val(),
				minute_close: $('select#advanced_och_minute_close_' + day + '').val(),
				ampm_close: $('select#advanced_och_ampm_close_' + day + '').val(),
				closed: ($('input#advanced_och_closed_' + day + '').is(":checked") ? 1 : 0)
			}
		}
	});

	$('#blockLeft .block[data-myflowtype]').each(function() {
		var main_el = $(this);
		if (main_el.attr('data-myflowtype') == "AgentIVR") {
			var exploded = main_el.attr('id').split("-");
			var agentIVRData = {
				type: "AgentIVR",
				applet_name: main_el.find('input[name="applet_name"]').val(),
				wId: exploded[1],
				greeting: {
					content: main_el.find('input[name="agentIVRgreeting_content"]').val(),
					type: main_el.find('input[name="agentIVRgreeting_type"]').val(),
					voice: main_el.find('input[name="agentIVRgreeting_voice"]').val(),
					language: main_el.find('input[name="agentIVRgreeting_language"]').val()
				},
				flow_type: main_el.find('select#flow_type').val(),
				hold_music: {
					content: main_el.find('input[name="agentIVRHoldMusic_content"]').val(),
					type: main_el.find('input[name="agentIVRHoldMusic_type"]').val(),
					voice: main_el.find('input[name="agentIVRHoldMusic_voice"]').val(),
					language: main_el.find('input[name="agentIVRHoldMusic_language"]').val()
				},
				prompt_message_type: main_el.find('select#prompt_message').val(),
				prompt_messages: []
			};

			main_el.find("#prompt_messages .pm_template").each(function() {
				var pm_el = $(this);

				var pm = {
					content: pm_el.find('input.content').val(),
					type: pm_el.find('input.type').val(),
					voice: pm_el.find('input.voice').val(),
					language: pm_el.find('input.language').val()
				};

				agentIVRData.prompt_messages.push(pm);
			});

			agentIVRData.agents_use_extensions = main_el.find('input#agents_use_extensions').is(":checked") ? 1 : 0; //Characters
			agentIVRData.agents_extension_length = main_el.find('input#agents_extension_length').val(); //Characters
			agentIVRData.max_queue_size = main_el.find('input#max_queue_size').val(); //Callers

			agentIVRData.agents = [];

			var extensions_good = true;

			main_el.find("table#ivrAgentTable .row").each(function() {
				var row_el = $(this);
				var drp_el = row_el.find('.drp');
				if (drp_el.text() != "Drop here") {
					var agent = {};
					agent.wId = drp_el.data('nid');
					agent.extension = row_el.find('.extension').val();

					if (agent.extension.length != agentIVRData.agents_extension_length) {
						if (agentIVRData.agents_use_extensions == 1) {
							extensions_good = false;
						}
					}
					
					var ag_block = $('#blockLeft #blockObj-' + agent.wId);
					agent.user_id = ag_block.find("#agent_user").val();
					agent.voicemail= {
						content: ag_block.find('input.content').val(),
						type: ag_block.find('input.type').val(),
						voice: ag_block.find('input.voice').val(),
						language: ag_block.find('input.language').val()
					};

					agent.send_transcript = ag_block.find('#send_transcript').is(':checked');
					agent.transcript_email = ag_block.find('#TranscriptEmail').val();

					agentIVRData.agents.push(agent);
				}
			});

			agentIVRData.repeat = main_el.find('input#repeat').val(); //Times

			var dhoVmObj = main_el.find('a[data-agent-ivr-voicemail="yes"]');
			if (dhoVmObj.data('nid') != "")
				agentIVRData.master_voicemail_wId = dhoVmObj.data('nid');
			else
				agentIVRData.master_voicemail_wId = 0;

			var has_master_voicemail = false;

			if (agentIVRData.master_voicemail_wId != 0) {
				var mvm_block = $('#blockLeft #blockObj-' + agentIVRData.master_voicemail_wId);

				if (mvm_block.find(".ivr-Menu-Selected").length > 0)
					has_master_voicemail = true;
			}

			var emptyAgents = false;

			for (var i in agentIVRData.agents) {
				if (agentIVRData.agents[i].user_id == 0) {
					emptyAgents = true;
					break;
				}
			}

			if (agentIVRData.agents.length == 0) {
				validation = false;
				parent.errMsgDialog('Please add at least one agent!');
				return false;
			}
			else if (emptyAgents) {
				validation = false;
				parent.errMsgDialog('All agent applets need to have an agent attached!');
				return false;
			}
			else if (!has_master_voicemail) {
				validation = false;
				parent.errMsgDialog('Please setup master voicemail!');
				return false;
			}
			else if (!extensions_good && agentIVRData.agents_use_extensions == 1) {
				validation = false;
				parent.errMsgDialog('Agents extensions must have ' + agentIVRData.agents_extension_length + ' characters!');
				return false;	
			}

			unsavedChanges.push(agentIVRData);
		}
	});

	if (validation) {
		$.post('process_ivr.php', { company_id: company_id, act:'saveCallFlow', unsavedChanges: JSON.stringify(unsavedChanges), advanced_och: JSON.stringify(advanced_och) }, function() {
			$("#save_call_flow_btn", window.parent.document).fadeTo(0, 0.6);
	        $("#save_call_flow_btn", window.parent.document).prop('disabled', true);
			window.location = 'index.php?cId=' + company_id;
			parent.infoMsgDialog('Call flow saved!');
		});
	}
}
		
$(window).scroll(function () {
	resizeFloatingDiv();
});

$(document).ready(function() {
	resizeFloatingDiv();
});

function resizeFloatingDiv() {
	//after window scroll fire it will add define pixel added to that element.
	var height = $(parent.window).height();
	if (height > 800) {
		scrollAmount = $(document).scrollTop()+"px";
		sc = parseInt(scrollAmount.replace('px',''));
		
		//this is the jQuery animate function to fixed the div position after scrolling.
		if( sc > 15) scrollAmount = ( sc -15) + 'px';
		$('#flotingDiv').animate({top:scrollAmount},{duration:1000,queue:false});
	}
	else {
		$("#blockRight").css('position', 'fixed');
		$("#blockRight").css('right', '20px');
		var new_height = height - 160;
		$("#blockRight .block_content").css('height', new_height + 'px');
		$("#blockRight .block_content").css('overflow-y', 'auto');
	}
}

function continueReplacement(this_el, drag_el) {
	$(this_el).html(drag_el.html());

	var isInsideMenu = false;
	if ($(this_el).parents('.ivrMenu').length > 0)
		isInsideMenu = true;

	var dho_el = $(this_el).parents('div.block').next('div.block').find("#drop_here_obj").last();

	if (isInsideMenu)
		var wId= $(this_el).data("wid");
	else
		var wId= $(dho_el).data("wid");

	var pId= $(dho_el).data("pid");
	var nId= $(dho_el).data("nid");
	var submenu= $(dho_el).data("submenu");
	var MainContainer = $(dho_el).closest('.blockLeft');
	var thisBlock = $(dho_el).closest('.block');

	var flowtype= drag_el.data("flowtype");
	$(dho_el).data('flowtype' ,flowtype);	

	$.getJSON('process_ivr.php?&wId='+wId+'&flowtype='+flowtype+'&act=save&replace=yes',
	function(jsdata) 
	{
		var htmlData = $('#'+flowtype +'-content').clone(true, true).html();
		htmlData= htmlData.replace(/data-submenu="yes" data-pid=""/g,' data-submenu="yes" data-pid="'+pId+'"');
		htmlData= htmlData.replace(/data-pid=""/g,'data-pid="'+pId+'"');
		htmlData= htmlData.replace(/data-wid=""/g,'data-wid="'+wId+'"');
		htmlData= htmlData.replace(/data-nid=""/g,'data-nid="'+nId+'"');
		htmlData= htmlData.replace(/data-submenu=""/g,'data-submenu="'+submenu+'"');
		htmlData= htmlData.replace(/myId/g,wId);											
		htmlData= htmlData.replace('="blockObj"',' ="blockObj-'+wId+'"');					
		htmlData= htmlData.replace('Drop here', $(dho_el).html());
		
		if ( flowtype !='Hangup') $(thisBlock).after(htmlData);

		var next_dho = $(thisBlock).next('div.block').find("#drop_here_obj").last();
		
		$(".drp" ).each(function(index, element) {
		  //$(element).removeClass('.drp');
            if($(element).text() == "Multiple Numbers")
                $(element).attr("style","font-size:16px");
            else
                $(element).attr("style","");
		    $(element).droppable(drpOptions);//$(element).bind('click', function (){alert('x')});
		  
		});

		$(thisBlock).remove();

		$('input, textarea, select').bind('change keyup',function(e) {
			triggerSaveChanges();
		});

		$("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
        $("#save_call_flow_btn", window.parent.document).prop('disabled', false);
	});
}

function triggerSaveChanges() {
	$("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
	$("#save_call_flow_btn", window.parent.document).prop('disabled', false);
}

function saveRepeat(obj)
{
		var digit = $(obj).val();
		var blockId = $(obj).closest('.block').prop('id');
			blockId= blockId.replace(/blockObj-/g, '');
			$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				   dataType: "html",
				  data:{wId: blockId , digit: digit ,act:'saveRepeat' }
				})
		
}

function saveDelay(obj)
{
		var digit = $(obj).val();
		var blockId = $(obj).closest('.block').prop('id');
			blockId= blockId.replace(/blockObj-/g, '');
			$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				   dataType: "html",
				  data:{wId: blockId , digit: digit ,act:'saveDelay' }
				})
		
}

function SaveDigit(obj)
{
	var digit = $(obj).val();
	wId = $(obj).closest('.row').find('#drop_here_obj').data('wid');
	if ( digit && wId)
	$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				   dataType: "html",
				  data:{wId: wId , digit:digit ,act:'saveDigit' }
				})
	
}

function addMoreRow(obj){
	RemoveShowFlow();
	
	MyTable = $(obj).closest('#mytable');	

	var last_row_nr = $(MyTable).find(".keypress").length + 1;	
	var LastDigit = $(MyTable).find('.row').last().find('#keypress').val();

	LastDigit = parseInt( LastDigit) +1;
	if ( LastDigit >9) LastDigit =0;

	var alreadyExists = false;
	MyTable.find(".keypress").each(function() {
		if (LastDigit == $(this).val().trim()) {
			alreadyExists = true;
		}
	});
	
	if (alreadyExists)
		return false;

	clonedRow= $(obj).closest('.row').clone(true).html();

	 //This line wraps the clonedRow and wraps it <tr> tags since cloning ignores those tags
	appendRow = '<tr class = "row">' + clonedRow + '</tr>'; 
	appendRow= appendRow.replace('<a class="add action btnAddMore" href="javascript:void(0)" id="btnAddMore" onclick="addMoreRow(this)"><span class="replace">Add</span> </a>', '<a class="remove action deleteThisRow" href="javascript:void(0)" onclick="deleteRow(this)"> <span class="replace" >Remove</span> </a>');

	
	appendRow= appendRow.replace(/data-wid=/g, ' data-wid="" ');
	appendRow= appendRow.replace(/data-nid=/g, ' data-nid="" ');
	appendRow= appendRow.replace(/data-flowtype=/g, ' data-flowtype="" ');
	
	
	appendRow= appendRow.replace(/Dial/g, 'Drop here');
	
	appendRow= appendRow.replace(/Multiple Numbers/g, 'Drop here');
	appendRow= appendRow.replace(/Round Robin/g, 'Drop here');
	
	appendRow= appendRow.replace(/MultipleNumbers/g, 'Drop here');
	appendRow= appendRow.replace(/RoundRobin/g, 'Drop here');
	
	
	appendRow= appendRow.replace(/Greetings/g, 'Drop here');
	appendRow= appendRow.replace(/Menu/g, 'Drop here');
	appendRow= appendRow.replace(/SMS/g, 'Drop here');
	appendRow= appendRow.replace(/Hangup/g, 'Drop here');
	appendRow= appendRow.replace(/Voicemail/g, 'Drop here');
	appendRow= appendRow.replace(/Opt-In/g, 'Drop here');
	appendRow= appendRow.replace(/>Agent</g, '>Drop here<');
	appendRow= appendRow.replace(/AgentIVR/g, 'Drop here');
	appendRow= appendRow.replace(/Agent IVR/g, 'Drop here');

	

	
	$(MyTable).append(appendRow);

	var add_el = $(MyTable).find("tr.row:last .btnAddMore");
	add_el.removeClass("btnAddMore");
	add_el.addClass("deleteThisRow");
	add_el.unbind("click");
	add_el.click(function() {
		addMoreRow(this);
	});

	$(MyTable).find(".keypress").last().attr('data-rownr', last_row_nr);
	
	$(".drp" ).each(function(index, element)
	{			
		$(element).droppable(drpOptions);
	});	
	
	$(MyTable).find('.row').last().find('#keypress').val(LastDigit);

	
}
function deleteRow(currentNode)
{
	RemoveShowFlow();
	var dho = $(currentNode).closest('.row').find('#drop_here_obj');
	var myNextId= $(dho).data("nid");
	RemoveBlock($("#blockObj-" + myNextId).find('.block_head h2>a'), currentNode);
}

function addMoreAgentRow(obj){
	RemoveShowFlow();
	
	MyTable = $(obj).closest('#ivrAgentTable');

	clonedRow= $(obj).closest('.row').clone(true).html();

	 //This line wraps the clonedRow and wraps it <tr> tags since cloning ignores those tags
	appendRow = '<tr class = "row">' + clonedRow + '</tr>'; 
	
	appendRow= appendRow.replace(/data-wid=/g, ' data-wid="" ');
	appendRow= appendRow.replace(/data-nid=/g, ' data-nid="" ');
	appendRow= appendRow.replace(/data-flowtype=/g, ' data-flowtype="" ');
	appendRow= appendRow.replace(/Dial/g, 'Drop here');
	appendRow= appendRow.replace(/Multiple Numbers/g, 'Drop here');
	appendRow= appendRow.replace(/Round Robin/g, 'Drop here');
	appendRow= appendRow.replace(/MultipleNumbers/g, 'Drop here');
	appendRow= appendRow.replace(/RoundRobin/g, 'Drop here');
	appendRow= appendRow.replace(/Greetings/g, 'Drop here');
	appendRow= appendRow.replace(/Menu/g, 'Drop here');
	appendRow= appendRow.replace(/SMS/g, 'Drop here');
	appendRow= appendRow.replace(/Hangup/g, 'Drop here');
	appendRow= appendRow.replace(/Voicemail/g, 'Drop here');
	appendRow= appendRow.replace(/Opt-In/g, 'Drop here');
	appendRow= appendRow.replace(/>Agent</g, '>Drop here<');
	appendRow= appendRow.replace(/AgentIVR/g, 'Drop here');
	appendRow= appendRow.replace(/Agent IVR/g, 'Drop here');
	
	$(MyTable).append(appendRow);

	var add_el = $(MyTable).find("tr.row:last .btnAddMore");
	add_el.removeClass("btnAddMore");
	add_el.removeClass("add");
	add_el.addClass("deleteThisRow");
	add_el.addClass("remove");
	add_el.removeAttr("onclick");
	add_el.unbind("click");
	add_el.click(function() {
		deleteAgentRow(this);
	});
	
	$(".drp" ).each(function(index, element)
	{			
		$(element).droppable(drpOptions);
	});

	var count = 1;
	$(MyTable).find('.row').each(function() {
		$(this).find('.number').text(count);
		count++;
	});

	$(MyTable).find('.row').last().find('.extension').val('');

	triggerSaveChanges();
}

function deleteAgentRow(currentNode)
{
	MyTable = $(currentNode).closest('#ivrAgentTable');

	RemoveShowFlow();
	var dho = $(currentNode).closest('.row').find('#drop_here_obj');
	var myNextId= $(dho).data("nid");
	RemoveBlock($("#blockObj-" + myNextId).find('.block_head h2>a'), currentNode);

	var count = 1;
	$(MyTable).find('.row').each(function() {
		$(this).find('.number').text(count);
		count++;
	});

	triggerSaveChanges();
}

function moveAgent(obj, where) {
	RemoveShowFlow();
	var row1 = $(obj).parents('.row');
	var row2 = (where == "up") ? row1.prev('.row') : row1.next('.row');

	if (row1.length > 0 && row2.length > 0) {
		var drp1 = row1.find('.drp');
		var temp = drp1.clone(true);
		var drp2 = row2.find('.drp');
		var new_drp1_text = drp2.text();
		var new_drp2_text = temp.text();
		var new_drp1_wid = drp2.data('wid');
		var new_drp2_wid = temp.data('wid');
		var new_drp1_pid = drp2.data('pid');
		var new_drp2_pid = temp.data('pid');
		var new_drp1_nid = drp2.data('nid');
		var new_drp2_nid = temp.data('nid');
		var new_drp1_class = drp2.attr('class');
		var new_drp2_class = temp.attr('class');

		drp1.data('wid', new_drp1_wid);
		drp1.data('pid', new_drp1_pid);
		drp1.data('nid', new_drp1_nid);
		drp1.attr('class', new_drp1_class);
		drp1.text(new_drp1_text);

		drp2.data('wid', new_drp2_wid);
		drp2.data('pid', new_drp2_pid);
		drp2.data('nid', new_drp2_nid);
		drp2.attr('class', new_drp2_class);
		drp2.text(new_drp2_text);

		var extension1 = row1.find('.extension');
		var extension2 = row2.find('.extension');
		var temp = extension1.clone(true);
		extension1.replaceWith(extension2.clone(true));
		extension2.replaceWith(temp);

		$(".drp" ).each(function(index, element)
		{
			$(element).droppable(drpOptions);
		});

		triggerSaveChanges();
	}
}

function addPromptMessage(obj) {
	var thisBlock = $(obj).closest('.block');
	var template = thisBlock.find('.prompt_template').first();

	thisBlock.find("#prompt_messages").append(template.html());

	var last_added = thisBlock.find("#prompt_messages").find('.pm_template').last();
	last_added.find(".ivr-Menu-selector-item").removeClass('ivr-Menu-Selected');
	last_added.find("#text").val('');
	last_added.find("#voice").val('man');
	last_added.find("#language").val('en|M');
	last_added.find("#voicefilename").hide('');
	last_added.find("#mp3_url_text").val('');
	last_added.find("#record_from").val('');
	last_added.find("#record_to").val('');
	last_added.find("#recordedAudioSaved").hide();

	if (thisBlock.find("#prompt_message").val() == 'call_queue_notification') {
		last_added.find(".content").val("Your call is important to us. You currently are number [NumberInQueue] in the queue.");
		last_added.find("#text").val("Your call is important to us. You currently are number [NumberInQueue] in the queue.");
		last_added.find(".type").val("Text");

		last_added.find(".ivr-Menu-selector-item-wrapper:eq(1)").hide();
		last_added.find(".ivr-Menu-selector-item-wrapper:eq(2)").hide();
		last_added.find(".ivr-Menu-selector-item-wrapper:eq(3)").hide();
		last_added.find(".ivr-Menu-selector-item-wrapper:eq(1) .ivr-Menu-selector-item").removeClass('ivr-Menu-Selected');
		last_added.find(".ivr-Menu-selector-item-wrapper:eq(2) .ivr-Menu-selector-item").removeClass('ivr-Menu-Selected');
		last_added.find(".ivr-Menu-selector-item-wrapper:eq(3) .ivr-Menu-selector-item").removeClass('ivr-Menu-Selected');
		last_added.find(".ivr-Menu-selector-item-wrapper:eq(0)").css('width', '100%');
		last_added.find(".ivr-Menu-selector-item-wrapper:eq(0) .ivr-Menu-selector-item").addClass('ivr-Menu-Selected');
	}

	triggerSaveChanges();
}

function removePromptMessage(obj) {
	if ($(obj).parents("#prompt_messages").find('.pm_template').length == 1) {
		var pm_template = $(obj).parents('.pm_template');
		pm_template.find(".ivr-Menu-selector-item-wrapper .ivr-Menu-selector-item").removeClass('ivr-Menu-Selected');
		pm_template.find(".content, .type, .voice, .language").val('');
		pm_template.find("#text").val('');
		pm_template.find("#voice").val('man');
		pm_template.find("#language").val('en|M');
		pm_template.find("#voicefilename").hide();
		pm_template.find("#mp3_url_text").val('');
		pm_template.find("#recordedAudioSaved").val('');
	}
	else {
		$(obj).parents('.pm_template').remove();
		triggerSaveChanges();
	}
}

function movePMUp(obj) {
	var div1 = $(obj).parents('.pm_template');
	var div2 = div1.prev('.pm_template');

	if (div1.length > 0 && div2.length > 0) {
		div1.insertBefore(div2);
		triggerSaveChanges();
	}
}

function changePMType(obj) {
	var thisBlock = $(obj).closest('.block');
	thisBlock.find(".pm_template .ivr-Menu-selector").show();
	thisBlock.find(".pm_template .ivr-Menu-editor").hide();

	if (obj.value == "none") {
		thisBlock.find("#prompt_messages").hide();
		thisBlock.find(".btnAddMorePrompts").hide();
	}
	else if (obj.value == "call_queue_notification") {
		thisBlock.find("#prompt_messages").show();
		thisBlock.find(".btnAddMorePrompts").hide();
		
		var counter = 0;
		thisBlock.find(".pm_template").each(function() {
			if (counter > 1) {
				$(this).hide();
			}
			else {
				$(this).find(".ivr-Menu-selector-item-wrapper:eq(1)").hide();
				$(this).find(".ivr-Menu-selector-item-wrapper:eq(2)").hide();
				$(this).find(".ivr-Menu-selector-item-wrapper:eq(3)").hide();
				$(this).find(".ivr-Menu-selector-item-wrapper:eq(1) .ivr-Menu-selector-item").removeClass('ivr-Menu-Selected');
				$(this).find(".ivr-Menu-selector-item-wrapper:eq(2) .ivr-Menu-selector-item").removeClass('ivr-Menu-Selected');
				$(this).find(".ivr-Menu-selector-item-wrapper:eq(3) .ivr-Menu-selector-item").removeClass('ivr-Menu-Selected');
				$(this).find(".ivr-Menu-selector-item-wrapper:eq(0)").css('width', '100%');
				$(this).find(".ivr-Menu-selector-item-wrapper:eq(0) .ivr-Menu-selector-item").addClass('ivr-Menu-Selected');
				$(this).find(".type").val("Text");
				var new_text = ($(this).find("#text").val() == "" ? "Your call is important to us. You currently are number [NumberInQueue] in the queue." : $(this).find("#text").val());
				$(this).find(".content").val(new_text);
				$(this).find("#text").val(new_text);
				$(this).find(".controls").hide();
				$(this).find(".deleteThisRow").hide();
				counter++;
			}
		});
	}
	else {
		thisBlock.find("#prompt_messages").show();
		thisBlock.find(".btnAddMorePrompts").show();
		var counter = 0;
		thisBlock.find(".pm_template").each(function() {
			if (counter > 1) $(this).show();
			$(this).find(".ivr-Menu-selector-item-wrapper:eq(1)").show();
			$(this).find(".ivr-Menu-selector-item-wrapper:eq(2)").show();
			$(this).find(".ivr-Menu-selector-item-wrapper:eq(3)").show();
			$(this).find(".ivr-Menu-selector-item-wrapper:eq(0)").css('width', '25%');
			$(this).find(".controls").show();
			$(this).find(".deleteThisRow").show();
			counter++;
		});
	}
}

function movePMDown(obj) {
	var div1 = $(obj).parents('.pm_template');
	var div2 = div1.next('.pm_template');

	if (div1.length > 0 && div2.length > 0) {
		div1.insertAfter(div2);
	}	triggerSaveChanges();
}

var	appendRow='';
 
  //$('.ivr-Menu-close-button').click(function(){});
function showHideNumBox(obj){


if( $(obj).val() == 'dial' )
{
	$(obj).siblings('#phoneNumber').show();
}else{
	$(obj).siblings('#phoneNumber').hide();	
}

}

function CloseButton(obj)
{
	
	
	
	var audioChoice = $(obj).closest('.ivr-Menu');
	
	var audioChoiceEditor	= audioChoice.children('.ivr-Menu-editor');
	var audioChoiceSelector	= audioChoice.children('.ivr-Menu-selector');
	
	var subDiv	= audioChoiceEditor.children('.ivr-Menu-editor-padding');
	
	audioChoiceSelector.show();
	audioChoiceEditor.hide();
	subDiv.children('.ivr-audio-upload').hide();	
	subDiv.children('.ivr-Menu-read-text').hide();
	subDiv.children('.ivr-mp3-url').hide();
	subDiv.children('.ivr-record-audio').hide();
	

}

function showAudioText(obj)
{
	
	
	var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
	audioChoice.hide();
	audioChoice.parent().children('.ivr-Menu-editor').show();
	var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');
	
	if ( obj.id  == 'txt' ) 
	{		
		
		subDiv.children('.ivr-Menu-read-text').show();
		
		//////////////// only to avoid  file button clickable in text area		
		$('[name="uploadfile"]').css('z-index','-1');		
	
	}
	else if ( obj.id  == 'upload_mp3' ) {
		$('[name="uploadfile"]').css('z-index','2147483583');
		
		subDiv.children('.ivr-audio-upload').show();	
		SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');
		UploadFile(SubObj);	
	}
	else if ( obj.id  == 'mp3_url' ) {
		subDiv.children('.ivr-mp3-url').show();
	}
	else if ( obj.id  == 'record_audio' ) {
		subDiv.children('.ivr-record-audio').show();
	}
	
}



function UploadFile(obj)
	{
		
		var status = $(obj).closest('.explanation').find('#statusUpload');
		var fileNameStatus = $(obj).closest('.explanation').find('#voicefilename');
		

		var blockId = $(obj).closest('.block').prop('id');	

		wId= blockId.replace(/blockObj-/g, '');
//			autoSubmit:true,
	new AjaxUpload(obj, {
			
			
			action: 'process_ivr.php?act=audioUpdate&wId='+wId+'&companyId='+companyId,
			name: 'uploadfile',
			onSubmit: function(file, ext){
				 if (! (ext && /^(mp3|wma)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('Only MP3 files are allowed');
					return false;
				}
				(fileNameStatus).html('Uploading...');
			},
			onComplete: function(file, response){
				$("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
	        	$("#save_call_flow_btn", window.parent.document).prop('disabled', false);
				//On completion clear the status
				
				//Add uploaded file to list
				
				if(response!="error"){
					//status.text('upoaded');
					//update_msg ('File Uploaded  Successfully');

					$(fileNameStatus).css('display' ,'block');
					$(fileNameStatus).html(response);
					var thisBlock = $(obj).closest('.block');

	                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
	                $(thisBlock).find('#upload_mp3').addClass('ivr-Menu-Selected');
	                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
	                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

	                $(thisBlock).find(".ivr-Menu-selector").show();
	                $(thisBlock).find(".ivr-Menu-editor").hide();
	                $(thisBlock).find('.ivr-audio-upload').hide();

                    $(thisBlock).find(".ttsMwCloseBtn").remove();
                    $(thisBlock).find('#upload_mp3').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
					//status.text('');
					
					//alert ("image stored");
					//$('<li></li>').appendTo('#files').html('<img src="./uploads/'+file+'" alt="" /><br />'+file).addClass('success');
				} else{
					//$('<li></li>').appendTo('#files').text(file).addClass('error');
				}
			}
		});
			
	}
	
function RemoveBlock(obj, currentNode)
{
	
	var thisBlock = $(obj).closest('.block');
	var myflowtype = $(thisBlock).data('myflowtype');

	var dho = $(thisBlock).find('#drop_here_obj')
	
	var flowtype= $(dho).data("flowtype");

	var nId= $(dho).data("nid");
	//if ( nId != '' )
	
	if ( myflowtype == 'Menu') {
		RemoveLargeBlock(obj, 'Menu');
	}
	else if ( myflowtype == 'AgentIVR' && !currentNode) {
		RemoveLargeBlock(obj, 'AgentIVR');
	}
	else
	{
		var BlockH2 = $(thisBlock).find('h2').html();
		if (!BlockH2) {
			if (currentNode)
        		$(currentNode).parent().parent().remove();
		}
		else {
			parent.promptMsg('Are you sure you want to remove "'+ BlockH2 +'" from call flow?', function() {
				$("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
	        	$("#save_call_flow_btn", window.parent.document).prop('disabled', false);

				if (currentNode)
	        		$(currentNode).parent().parent().remove();

				RemoveSubBlock(obj);
			});
		}
	}
}

function RemoveLargeBlock(obj, flow_type)
{

	var thisBlock = $(obj).closest('.block');
	var BlockH2 = $(thisBlock).find('h2').html();
	var nextId ='';
	var ParentId='';
	var SubItems = false;
	$( thisBlock).find('#drop_here_obj').each(function( index, element ) 
	{
		if ( 
			(
				$(element).data('menu') != 'next' && 
				$(element).data("flowtype")  != 'Hangup' && 
				$(element).data('nid') != ''
			)
				||
			(
				$(element).text() != "Drop Here"
				&&
				$(element).text() != "Drop here"
				&&
				$(element).text() != "Drop Voicemail"
			)
		   )
		{
			
			SubItems= true;
			
			
		}
		
		/*if ( $(element).data('menu') == 'next' )
		{
			 NextId = $(element).data('nid');
			 ParentId= $(element).data('pid');
		}*/
		
	});
	
	if ( SubItems){ 
		var message = (flow_type == "Menu") ? 'Please remove menu options first.' : 'Please remove agents and master voicemail first.';
		parent.errMsgDialog(message);
		return;
	}

	parent.promptMsg('Are you sure you want to remove "'+ BlockH2 +'" from call flow?', function() {
		$("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
        $("#save_call_flow_btn", window.parent.document).prop('disabled', false);

		$( thisBlock).find('#drop_here_obj').each(function( index, element ) 
		{
			if ( $(element).data('flowtype') == 'Hangup' )
			{
				$.ajax({
				 	type: "POST",
					url: "process_ivr.php",
					dataType: "html",
					data:{wId:  $(element).data('nId'),pId: $(element).data('pid'),nId: $(element).data('nid') , flowtype: $(element).data('flowtype') ,act:'REMOVE-Hangup' }
				});
						
			}
			if ( $(element).data('menu') != 'next')
			{
				$(element).remove();
			}
		});
		
		RemoveSubBlock(obj);
	});
}


function RemoveSubBlock(obj, ignore_remove_from_db){
	var thisBlock = $(obj).closest('.block');
	var dho = $(thisBlock).find('#drop_here_obj').last();
	var wId= $(dho).data("wid");
	if (!wId) {
		var theId = thisBlock.attr("id");
		var exploded = theId.split("-");
		wId = exploded[1];
	}

	var pId= $(dho).data("pid");
	if (!pId) pId = '';
	var nId= $(dho).data("nid");
	if (!nId) nId = '';
	var oc_voicemail= $(dho).data("oc-voicemail");
	var oc_type= $(dho).data("oc-type");

	var flowtype= $(dho).data("flowtype");
	RemoveShowFlow();
	if ( flowtype == 'Hangup' )
	{
			$.ajax({
				  type: "POST",
				  url: "process_ivr.php?companyId=" + companyId,
				   dataType: "html",
				  data:{wId: wId,pId:pId,nId:nId , flowtype:flowtype ,act:'REMOVE-Hangup' }
				})
	}
	
	if ( nId != '' )
	{
		if ( pId == '' ){ 
			
			$('#blockMain').find('#drop_here_obj').html($( '#blockObj-'+ wId).find('#drop_here_obj').html());
			$('#blockMain').find('#drop_here_obj').data('nid',nId);
			$( '#blockObj-'+ nId).find('#drop_here_obj').data('pid','');
			
		}else{
			
			$( '#blockObj-'+ pId).find('#drop_here_obj').each(function( index, element ) 
			{
				if ( $(element).data('nid') == wId)
				{
					$(element).html($( '#blockObj-'+ wId).find('#drop_here_obj').html());
					$(element).data('nid',nId);
				}
			});
			
			
			$( '#blockObj-'+ nId).find('#drop_here_obj').data('pid',pId);
			
		}
		
	}else{
		if ( pId == '' && flowtype != "SingleAgent")
		{
			$('#blockMain').find('#drop_here_obj').html('Drop here');
			$('#blockMain').find('#drop_here_obj').data('nid','');

		}else{
			if (oc_voicemail == "yes") {
				$( '#blockObj-'+ pId).find('#drop_here_obj[data-number="' + oc_type + '"]').each(function( index, element ) 
				{
					$(element).html('Drop Voicemail');
					$(element).data('nid','');
				});
			}
			else {
				$( '#blockObj-'+ pId).find('#drop_here_obj').each(function( index, element ) 
				{
					if ( $(element).data('nid') == wId)
					{
						$(element).html('Drop here');
						$(element).data('nid','');
					}
				});

				if (flowtype == "SingleAgent") {
					var agentRowDrp = $('#blockLeft').find('#drop_here_obj[data-nid="' + wId + '"]');
					agentRowDrp.html('Drop here');
					agentRowDrp.data('nid', '');
				}
			}
			/*
			$( '#blockObj-'+ pId).find('#drop_here_obj').html('Drop here');
			$( '#blockObj-'+ pId).find('#drop_here_obj').data('nid','')
			*/
		}
	}

	$('#blockLeft .block #drop_here_obj[data-pid="' + wId + '"]').each(function() {
		if ($(this).data("oc-voicemail") == "yes") {
			RemoveSubBlock($(this).parents('.block').find('h2'));
		}
	});

	if (ignore_remove_from_db) {
		$('#blockObj-'+ wId).hide('slow',function() {	$( '#blockObj-'+ wId ).remove();  });
	 }
	 else {
	 	var flowtype= $(dho).data("flowtype");
		
		//////
		$.ajax({
		  type: "POST",
		  url: "process_ivr.php",
		   dataType: "html",
		  data:{wId: wId,pId:pId,nId:nId , flowtype:flowtype ,act:'REMOVE' }
		})
		  .done(function( msg ) {
			//alert( "Option Removed " + msg );
			
			$('#blockObj-'+ wId).hide('slow',function() {	$( '#blockObj-'+ wId ).remove();  });
			
		  });
	 }
		
}

function update_msg (msg)
{
    parent.window.infoMsgDialog(msg);
}
function DeleteRoundRobinNumber(obj,idx){
	$(obj).parent().parent().remove();
	$.ajax({
	  type: "POST",
	  url: "process_ivr.php",
	   dataType: "html",
	  	data:{idx: idx , act:'DeleteRoundRobinNumber'}
	})
	  .done(function( msg ) {
		  if (msg == '1')
		  {
			update_msg ("Phone Number Removed Successfully");	
			
		  }
	  });	
}
function DeleteMultiNumber(obj,idx)
{
	$(obj).parent().parent().remove();
	$.ajax({
	  type: "POST",
	  url: "process_ivr.php",
	   dataType: "html",
	  	data:{idx: idx , act:'DeleteMultiNumber'}
	})
	  .done(function( msg ) {
		  if (msg == '1')
		  {
			update_msg ("Phone Number Removed Successfully");	
			
		  }
	  });	
	
}
function SaveMultiNumberinternational(obj)
{
	var thisBlock = $(obj).closest('.block');
	var blockId = $(obj).closest('.block').prop('id');
	blockId= blockId.replace(/blockObj-/g, '');
	var dho = $(thisBlock).find('#drop_here_obj');
	var wId = $(dho).data("wid");
	var international_number = 0;
	if($(thisBlock).find("#international_number").is(":checked")){
		international_number = 1;
	}
	//////
	$.ajax({
	  type: "POST",
	  url: "process_ivr.php",
	   dataType: "html",
	  	data:{wId: wId , act:'MultiNumberInternational' ,international_number:international_number }
	})
	  .done(function( msg ) {
		  if (msg == '1')
		  {
			//update_msg ("Phone Number Saved Successfully");	
			
		  }
	  });									
	
	
}
function SaveMultiNumber(obj)
{
	var thisBlock = $(obj).closest('.block');
	var blockId = $(obj).closest('.block').prop('id');
	blockId= blockId.replace(/blockObj-/g, '');
	
	var dho = $(thisBlock).find('#drop_here_obj');
	var wId = $(dho).data("wid");
				
	var PhoneNumber = $(thisBlock).find('#PhoneNumber').val().trim();
	
	var international_number = 0;
	if($(thisBlock).find("#international_number").is(":checked")){
		international_number = 1;
	}

    if (PhoneNumber.length != 10 && !$(thisBlock).find('#international_number').is(":checked")) {
        parent.errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
    }else{
        if(international_number==0)
            PhoneNumber = "+1"+PhoneNumber;
        else{
            if(PhoneNumber[0]!="+")
                PhoneNumber = "+"+PhoneNumber;
        }

            //////
        $.ajax({
          type: "POST",
          url: "process_ivr.php",
           dataType: "html",
            data:{wId: wId , PhoneNumber:PhoneNumber ,act:'MultiNumber' ,international_number:international_number }
        })
          .done(function( msg ) {
              if (msg > 0)
              {
                  $(thisBlock ).find("#number_t > tbody").append('<tr><td>'+PhoneNumber +'</td><td><a href="javascript:void(0);" onclick="DeleteMultiNumber(this,\''+ msg + '\')" ><img src="images/delete.gif"></td></tr>');
                 $(thisBlock).find('#PhoneNumber').val('');

                update_msg ("Phone Number Saved Successfully");

              }
          });
    }
}

function SaveRoundRobinNumber(obj)
{
	var thisBlock = $(obj).closest('.block');
	var blockId = $(obj).closest('.block').prop('id');
	blockId= blockId.replace(/blockObj-/g, '');
	
	var dho = $(thisBlock).find('#drop_here_obj');
	var wId = $(dho).data("wid");
				
	var PhoneNumber = $(thisBlock).find('#PhoneNumber').val().trim();
	
	var international_number = 0;
	if($(thisBlock).find("#international_number").is(":checked")){
		international_number = 1;
	}
	var forward_number = '';	
	var forward_sec = '';	
		
	//////
    if (PhoneNumber.length != 10 && !$(thisBlock).find('#international_number').is(":checked")) {
        parent.errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
    }else{
        if(international_number==0)
            PhoneNumber = "+1"+PhoneNumber;
        else{
            if(PhoneNumber[0]!="+")
                PhoneNumber = "+"+PhoneNumber;
        }

        $.ajax({
          type: "POST",
          url: "process_ivr.php",
           dataType: "html",
            data:{wId: wId , PhoneNumber:PhoneNumber ,act:'RoundRobinNumber' ,international_number:international_number,forward_number:forward_number, forward_sec:forward_sec }
        })
          .done(function( msg ) {
              if (msg > 0)
              {
                 $(thisBlock ).find("#number_t > tbody").append('<tr><td>'+PhoneNumber +'</td><td><a href="javascript:void(0);" onclick="DeleteRoundRobinNumber(this,\''+ msg + '\')" ><img src="images/delete.gif"></td></tr>');
                 $(thisBlock).find('#PhoneNumber').val('');
                update_msg ("Phone Number Saved Successfully");

              }
          });
    }
}

function saveTranscriptDetails(obj)
{
	var thisBlock = $(obj).closest('.block');
	var blockId = $(obj).closest('.block').prop('id');
	blockId= blockId.replace(/blockObj-/g, '');
	
	var dho = $(thisBlock).find('#drop_here_obj');
	var wId = $(dho).data("wid");
				
	var TranscriptEmail = $(thisBlock).find('#TranscriptEmail').val();
	
	var send_transcript = 0;
	if($(thisBlock).find("#send_transcript").is(":checked")){
		send_transcript = 1;
	}


    $.ajax({
      type: "POST",
      url: "process_ivr.php",
       dataType: "html",
        data:{wId: wId , TranscriptEmail:TranscriptEmail ,act:'VoicemailTranscript' ,send_transcript:send_transcript }
    })
	.done(function( msg ) {
	});
}

function saveVoicemailRingCount(obj)
{
	var thisBlock = $(obj).closest('.block');
	var blockId = $(obj).closest('.block').prop('id');
	blockId= blockId.replace(/blockObj-/g, '');
	
	var dho = $(thisBlock).find('#drop_here_obj');
	var wId = $(dho).data("wid");
				
	var ring_count = $(thisBlock).find('#ring_count').val();

    $.ajax({
      type: "POST",
      url: "process_ivr.php",
       dataType: "html",
        data:{wId: wId , ring_count:ring_count ,act:'VoicemailRingCount' }
    })
	.done(function( msg ) {
	});
}

function SaveRoundRobin(obj)
{
	var thisBlock = $(obj).closest('.block');
	var blockId = $(obj).closest('.block').prop('id');
	blockId= blockId.replace(/blockObj-/g, '');
	
	var dho = $(thisBlock).find('#drop_here_obj');
	var wId = $(dho).data("wid");
	
	var international_number = 0;
	if($(thisBlock).find("#international_number").is(":checked")){
		international_number = 1;
	}
	
	var forward_number ='';
	var forward_sec = '';	
	
	//////
	$.ajax({
	  type: "POST",
	  url: "process_ivr.php",
	   dataType: "html",
	  	data:{wId: wId ,forward_number:forward_number, forward_sec:forward_sec ,act:'SaveRoundRobin' ,international_number:international_number }
	})
	  .done(function( msg ) {
		  if (msg == '1')
		  {
			//update_msg ("Phone Number Saved Successfully");

		  }
	  });
			
}

	var closeBtnHtml = '<a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a>';

    function removeSelectedOption(obj) {
        parent.promptMsg('Are you sure ?', function() {
          SaveContent(obj, 'EMPTY_WIDGET');

          var thisBlock = $(obj).closest('.ivr-Menu');
          $(thisBlock).find(".ttsMwCloseBtn").remove();

          $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

          $(thisBlock).find('.content').val('');
          $(thisBlock).find('.type').val('');
          $(thisBlock).find('.voice').val('');
          $(thisBlock).find('.language').val('');

          $("#save_call_flow_btn", window.parent.document).fadeTo(0, 1);
          $("#save_call_flow_btn", window.parent.document).prop('disabled', false);
        });
    }

    function SaveContent(obj, content_type) {
        var thisBlock = $(obj).closest('.block');
        var blockId = $(obj).closest('.block').prop('id');
        blockId = blockId.replace(/blockObj-/g, '');
        switch (content_type) {
            case "SMS":
            {
                var dho = $(thisBlock).find('#drop_here_obj');
                var sms_content = $(thisBlock).find('#sms_content').val();
                var sms_delay = $(thisBlock).find('#sms_delay').val();
                var wId = $(dho).data("wid");
                var pId = $(dho).data("pid");
                var nId = $(dho).data("nid");
                /*console.log(dho);
                    alert(sms_content + text);*/
                //////
                $.ajax({
                    type: "POST",
                    url: "process_ivr.php",
                    dataType: "html",
                    data: {
                        wId: wId,
                        sms_content: sms_content,
                        sms_delay: sms_delay,
                        act: 'SMS'
                    }
                })
                        .done(function (msg) {
                            if (msg == '1') {
                                update_msg("SMS Text Saved Successfully");
                            }
                        });
                break;
            }
            case "Dail":
            {
                var dho = $(thisBlock).find('#drop_here_obj');

                call_hours = [];

                $(thisBlock).find(".weekday_val").each(function() {
                	var day = $(this).val();
                	call_hours.push({
                		name: day,
                		hour_open: $(thisBlock).find('.hour_open_' + day).val(),
                		minute_open: $(thisBlock).find('.minute_open_' + day).val(),
                		ampm_open: $(thisBlock).find('.ampm_open_' + day).val(),
                		hour_close: $(thisBlock).find('.hour_close_' + day).val(),
                		minute_close: $(thisBlock).find('.minute_close_' + day).val(),
                		ampm_close: $(thisBlock).find('.ampm_close_' + day).val(),
                		chk: $(thisBlock).find('.chk_' + day).is(":checked") ? 1 : 0
                	});
                });

                var specify_hours = $(thisBlock).find('select[name="opt_hours"]').val();

                var open_number = $(thisBlock).find('input[name="assigned_number"]').val().trim();

                if (open_number.length != 10 && !$(thisBlock).find('#international').is(":checked")) {
                	parent.errMsgDialog("Please enter the 10 digit number without punctuation or spaces in Phone number open. If this is an international number (not US or CA), please check the box.");
                	return false;
                }

                var close_number = $(thisBlock).find('input[name="close_number"]').val().trim();

                if ($(thisBlock).find("#forward_number").is(":checked") == 1 && close_number.length != 10 && !$(thisBlock).find('#international_closed').is(":checked")) {
                	parent.errMsgDialog("Please enter the 10 digit number without punctuation or spaces in Forward number. If this is an international number (not US or CA), please check the box.");
                	return false;
                }

                var all_data = {
                	specify_hours: specify_hours,
                	open_number: open_number,
                	open_number_int: $(thisBlock).find('#international').is(":checked") ? 1 : 0,
                	forward_number_enabled: $(thisBlock).find('#forward_number').is(":checked") ? 1 : 0,
                	close_number: close_number,
                	close_number_int: $(thisBlock).find('#international_closed').is(":checked") ? 1 : 0
                };


                var wId = $(dho).data("wid");
                var pId = $(dho).data("pid");
                var nId = $(dho).data("nid");
                /*console.log(dho);
                    alert(dail_number + wId + dail_international + nId);
                    */
                //////
                /*if (dial_number.length != 10 && !$(thisBlock).find('#option_International').is(":checked") && dial_number != "") {
                    parent.errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
                }else{*/
                $.ajax({
                    type: "POST",
                    url: "process_ivr.php",
                    dataType: "html",
                    data: {
                        wId: wId,
                        all_data: all_data,
                        call_hours: call_hours,
                        act: 'Dail'
                    }
                })
                        .done(function (msg) {
                            if (msg == '1') {
                                update_msg("Dial Details Saved Successfully");
                            }
                        });
                //}
                break;
            }

            case "BusinessHours":
            {
                var dho = $(thisBlock).find('#drop_here_obj');

                call_hours = [];

                $(thisBlock).find(".weekday_val").each(function() {
                	var day = $(this).val();
                	call_hours.push({
                		name: day,
                		hour_open: $(thisBlock).find('.hour_open_' + day).val(),
                		minute_open: $(thisBlock).find('.minute_open_' + day).val(),
                		ampm_open: $(thisBlock).find('.ampm_open_' + day).val(),
                		hour_close: $(thisBlock).find('.hour_close_' + day).val(),
                		minute_close: $(thisBlock).find('.minute_close_' + day).val(),
                		ampm_close: $(thisBlock).find('.ampm_close_' + day).val(),
                		chk: $(thisBlock).find('.chk_' + day).is(":checked") ? 1 : 0
                	});
                });

                var wId = $(dho).data("wid");
                var pId = $(dho).data("pid");
                var nId = $(dho).data("nid");

                $.ajax({
                    type: "POST",
                    url: "process_ivr.php",
                    dataType: "html",
                    data: {
                        wId: wId,
                        call_hours: call_hours,
                        act: 'BusinessHours'
                    }
                })
                .done(function (msg) {
                    if (msg == '1') {
                        update_msg("Business Hours Saved Successfully");
                    }
                });
                break;
            }

            case "Text_mail":
            {
                var dho = $(thisBlock).find('#drop_here_obj');
                var voice_text = $(thisBlock).find('#readtxt_mail').val();
                var voice = $(thisBlock).find('#voice').val();
                var language = $(thisBlock).find('#language').val();
                $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#txt').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                var wId = $(dho).data("wid");
                var pId = $(dho).data("pid");
                var nId = $(dho).data("nid");
                //alert(voice_text + wId );
                /*console.log(dho);

                    */
                //////
                $.ajax({
                    type: "POST",
                    url: "process_ivr.php",
                    dataType: "html",
                    data: {
                        wId: blockId,
                        text_content: voice_text,
                        language: language,
                        voice: voice,
                        act: 'Text_mail'
                    }
                })
                        .done(function (msg) {
                            if (msg == '1') {
				                $(thisBlock).find(".ivr-Menu-selector").show();
				                $(thisBlock).find(".ivr-Menu-editor").hide();
				                $(thisBlock).find('.ivr-Menu-read-text').hide();
                                //update_msg("Text Saved Successfully");
                            }
                        });
                break;
            }



            case "EMPTY_WIDGET":
            {
                $.ajax({
                    type: "POST",
                    url: "process_ivr.php",
                    dataType: "html",
                    data: {
                        wId: blockId,
                        act: 'EMPTY_WIDGET'
                    }
                })
                .done(function (msg) {
                });
                break;
            }

            case "MP3_URL":
            {
                var dho = $(thisBlock).find('#drop_here_obj');
                var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#mp3_url').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                var wId = $(dho).data("wid");
                var pId = $(dho).data("pid");
                var nId = $(dho).data("nid");
                //alert(voice_text + wId );
                /*console.log(dho);

                    */
                //////
                $.ajax({
                    type: "POST",
                    url: "process_ivr.php",
                    dataType: "html",
                    data: {
                        wId: blockId,
                        mp3_url: mp3_url,
                        act: 'MP3_URL'
                    }
                })
                .done(function (msg) {
                    if (msg == '1') {
                        //update_msg("MP3 URL Saved Successfully");

		                $(thisBlock).find(".ivr-Menu-selector").show();
		                $(thisBlock).find(".ivr-Menu-editor").hide();
		                $(thisBlock).find('.ivr-mp3-url').hide();

                        $(obj).closest('.explanation').find('#mp3UrlSaved').css('display' ,'block');
                        $(obj).closest('.explanation').find('#mp3UrlSaved').html('MP3 to play: <strong>' + mp3_url + '</strong>');
                    }
                });
                break;
            }

            case "OptIn":
            {
                var list_name = $(thisBlock).find('#list_name').val();
                var new_list_name = $(thisBlock).find('#new_list_name').val();
                
                if (list_name == "new_list") {
                	list_name = new_list_name;
                }

                var wId = $(dho).data("wid");
                var pId = $(dho).data("pid");
                var nId = $(dho).data("nid");

                $.ajax({
                    type: "POST",
                    url: "process_ivr.php",
                    dataType: "html",
                    data: {
                        wId: blockId,
                        list_name: list_name,
                        act: 'OptIn'
                    }
                })
                .done(function (msg) {
                	if (msg == '1') {
	                    update_msg("Details saved.");

	                    if ($(thisBlock).find('#list_name option[value="' + list_name + '"]').length == 0) {
	                    	$(thisBlock).find('#list_name').append('<option value="' + list_name + '">' + list_name + '</option>');
	                    	$(thisBlock).find('#list_name').val(list_name);
	                    }

	                    $(thisBlock).find("#new_list_wrapper").hide();
	                    $(thisBlock).find("#new_list_name").val('');
	                }
	                else {
	                	parent.errMsgDialog("Please select or add a new list.");
	                }
                });
                break;
            }
        }
    }

 function testVoice(voice, language, text) {
 	if ($("#test_voice_iframe_wrapper").length == 0) {
 		$("body").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
 							'<form id="test_voice_form" action="../test_voice.php" method="post" target="test_voice_iframe">' +
 								'<input type="hidden" name="voice" id="voice" />' +
 								'<input type="hidden" name="language" id="language" />' +
 								'<input type="hidden" name="text" id="text" />' +
 								'<input type="submit" name="submit" id="submit" value="Submit" />' +
 							'</form>' +
 							'<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
 						'</form>'
 		);
 	}

 	$("#test_voice_iframe_wrapper #voice").val(voice);
 	$("#test_voice_iframe_wrapper #language").val(language);
 	$("#test_voice_iframe_wrapper #text").val(text);
 	$('#test_voice_iframe_wrapper #submit').click()
 }

function recordAudio(wId, obj) {
 	if (wId == "") {
	 	var blockId = $(obj).closest('.block').prop('id');
		wId= blockId.replace(/blockObj-/g, '');
 	}

 	if ($(obj).val() == "Call Me") {
	 	var record_from = $(obj).parents('div.block').find('#record_from').val();
	 	var record_to = $(obj).parents('div.block').find('#record_to').val();

	 	if (record_from == "") {
	 		parent.errMsgDialog("Please select Caller ID.");
	 		return false;
	 	}

	 	if (record_to == "") {
	 		parent.errMsgDialog("Please enter your phone number.")
	 		return false;
	 	}

	 	$(obj).val('Stop');

	 	if ($("#record_audio_iframe_wrapper").length == 0) {
	 		$("body").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
	 							'<form id="record_audio_form" action="../record_audio.php" method="post" target="record_audio_iframe">' +
	 								'<input type="hidden" name="wId" id="wId" />' +
	 								'<input type="hidden" name="record_from" id="record_from" />' +
	 								'<input type="hidden" name="record_to" id="record_to" />' +
	 								'<input type="submit" name="submit" id="submit" value="Submit" />' +
	 							'</form>' +
	 							'<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
	 						'</form>'
	 		);
	 	}

	 	$("#record_audio_iframe_wrapper #wId").val(wId);
	 	$("#record_audio_iframe_wrapper #record_from").val(record_from);
	 	$("#record_audio_iframe_wrapper #record_to").val(record_to);
	 	$('#record_audio_iframe_wrapper #submit').click()
 	}
 	else {
 		$(obj).val('Please wait...');
 		$("#record_audio_iframe").contents().find("#disconnectBtn").click();

 		setTimeout(function() {
 			$.get("process_ivr.php?act=GET_RECORDED_AUDIO&wId=" + wId, function(response) {
	 			var thisBlock = $(obj).closest('.block');

	 			$(thisBlock).find('#recordedAudioSaved').css('display' ,'block');
	            $(thisBlock).find('#recordedAudioSaved').html(response);

	            //parent.infoMsgDialog("Audio recording saved!");
	            
	            $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                $(thisBlock).find(".ivr-Menu-selector").show();
                $(thisBlock).find(".ivr-Menu-editor").hide();
                $(thisBlock).find('.ivr-record-audio').hide();

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#record_audio').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

	            $(obj).val('Call Me');
	 		});
 		}, 2000);
 	}
 }

function selectOptHours(el) {
  el = $(el);

  if (el.val() == 1) {
    for (var i = 2; i <= 11; i++) {
      el.parents('.block').find("#opt_hours_table tr:eq(" + i + ")").show();
    }
    $('#ring1numberIframe', window.parent.document).attr('height', '600');
  }
  else {
    for (var i = 2; i <= 11; i++) {
      el.parents('.block').find("#opt_hours_table tr:eq(" + i + ")").hide();
    }
    $('#ring1numberIframe', window.parent.document).attr('height', '170');
  }
}

function checkSimilarKeypresses(el) {
	var parentTable = $(el).parents(".ivr2-menu-grid");
	var this_row_nr = $(el).attr("data-rownr");
	parentTable.find(".keypress").each(function() {
		if ($(el).val().trim() == $(this).val().trim() && this_row_nr != $(this).attr("data-rownr")) {
			parent.errMsgDialog("There is already an item with that Presskey. Please choose another one.");
			$(el).val('');
			$(el).focus();
		}
	});
}

 </script>
<div id="Hangup-content"   style="display:none">
  <div class="block" id="blockObj" data-myflowtype="Hangup"></div>
</div>

<div id="Greetings-content" style="display:none" >
  <?php	genGreetingsBlock(); 	?>
</div>
<div id="Voicemail-content" style="display:none" >
  <?php	genVoicemailBlock(); 	?>
</div>
<div id="VoicemailWithoutRing-content" style="display:none" >
  <?php	genVoicemailWithoutRingBlock(); 	?>
</div>
<div id="SMS-content" style="display:none" >
  <?php	genSMSBlock(); 			?>
</div>
<div id="Dial-content" style="display:none">
  <?php	genDialBlock();  		?>
</div>
<div id="Menu-content" style="display:none">
  <?php	genMenuBlock();  		?>
</div>
<div id="MultipleNumbers-content" style="display:none">
  <?php	genMultipleNumbersBlock();  		?>
</div>

<div id="RoundRobin-content" style="display:none">
  <?php	genRoundRobinBlock();  		?>
</div>

<div id="OptIn-content" style="display:none">
  <?php	genOptInBlock();  		?>
</div>

<div id="AgentIVR-content" style="display:none">
  <?php	genAgentIVRBlock();  		?>
</div>

<div id="SingleAgent-content" style="display:none">
  <?php	genSingleAgentBlock();  		?>
</div>

<div id="BusinessHours-content" style="display:none">
  <?php	genBusinessHoursBlock();  		?>
</div>



<script src="js/jquery.jsPlumb-1.5.2-min.js"></script> 
<script src="js/script.js?v=<?php echo time(); ?>"></script>
<script type="text/javascript" src="../js/tts.audio.plugin.js?v=<?php echo time(); ?>"></script>

<script type="text/javascript">
    $(".tt").tooltipster({
        position: 'bottom',
        theme:'.tooltipster-shadow'
    });
</script>
</body>
</html>
