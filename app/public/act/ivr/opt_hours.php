<?php
	
	//Check for config..
	if (!file_exists("../include/config.php")) {
		die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
	}
	session_start();
	require_once('../include/util.php');
	
	
	$page = "adminpage";
	$db = new DB();	
	//Database settings
	$con = mysqli_connect($DB_HOST,$DB_USER,$DB_PASS);
  mysqli_select_db($con, $DB_NAME );


	//Pre-load Checks
	if (!isset($_SESSION['user_id'])) {		header("Location: ../login.php");		exit;	}
	
	



	$companyId = $_REQUEST['cId'];

  if (isset($_POST['voice_text'])) {
    $update_qry= "UPDATE companies set voicemail_type='Text', voicemail_content='".addslashes($_POST['voice_text'])."', voicemail_text_voice='".addslashes($_POST['voice'])."', voicemail_text_language='".addslashes($_POST['language'])."' WHERE idx = '".addslashes($companyId)."'";
    mysqli_query($con, $update_qry) or die (mysqli_error($con));
    exit();
  }

  if (isset($_POST['mp3_url'])) {
    $update_qry= "UPDATE companies set voicemail_type='MP3_URL', voicemail_content='".addslashes($_POST['mp3_url'])."' WHERE idx = '".addslashes($companyId)."'";
    mysqli_query($con, $update_qry) or die (mysqli_error($con));
    exit();
  }

  if (isset($_POST['EMPTY_WIDGET'])) {
    $update_qry= "UPDATE companies set voicemail_type='', voicemail_content='' WHERE idx = '".addslashes($companyId)."'";
    mysqli_query($con, $update_qry) or die (mysqli_error($con));
    exit();
  }

  if (isset($_FILES['uploadfile'])) {
    $uploaddir = '../audio/'.$companyId.'/'; 
  
    @mkdir ('../audio/'.$companyId.'/',0777,true);
    @chmod ('../audio/'.$companyId.'/',0777);
    
    $file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name'])); 
     
    if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
    { 
      $filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
      $update_qry= "UPDATE companies set voicemail_type='Audio', voicemail_content='".$filename."' WHERE idx = '".addslashes($companyId)."'";
      mysqli_query($con, $update_qry) or die (mysqli_error($con));
      echo "Voice to play: <strong>$filename</strong>";
    
    } else {
      echo "error";
    }

    exit();
  }


	if (isset($_POST['save'])){
		
		
		//print_r($_POST);exit;
		$qry= "delete from call_hours where company_id = $companyId";
		mysqli_query($con, $qry) or die (mysqli_error($con));
		
		if (!empty($_POST['weekdays'])) {
  		foreach ($_POST['weekdays'] as $k => $v )
  		{
  			$chk = '0';
  			if ( isset ($_POST['chk'][$k] ))  $chk = '1';

        $open = date("H:i:s", strtotime(date("Y-m-d")." ".$_POST['hour_open'][$k].":".$_POST['minute_open'][$k].":00 ".$_POST['ampm_open'][$k]));

        $close = date("H:i:s", strtotime(date("Y-m-d")." ".$_POST['hour_close'][$k].":".$_POST['minute_close'][$k].":00 ".$_POST['ampm_close'][$k]));
  			 
  			 $insert_qry= "insert into call_hours set company_id='$companyId',
  											  day='".$_POST['weekdays'][ $k]."',
  											  open_time='".$open."',
  											  close_time='".$close."',
  											  status='".$chk."' ";
  											  
  			mysqli_query($con, $insert_qry) or die (mysqli_error($con));
  											  
  		}
    }
    
     $voicemail =0;
     $international =0;
     $voicemail_closed =0;
     $international_closed =0;

    if ( isset( $_POST['voicemail'] ) ) $voicemail =1;
    if ( isset( $_POST['international'] ) ) $international =1;
    if ( isset( $_POST['voicemail_closed'] ) ) $voicemail_closed =1;
    if ( isset( $_POST['international_closed'] ) ) $international_closed =1;

		 $opt_hours	=$_POST['opt_hours'];
		$assigned_number = $_POST['assigned_number'];	
		$close_number  = $_POST['close_number'];
    $send_transcript = isset($_POST['send_transcript']) ? 1 : 0;
    $transcriptEmail = $_POST['transcriptEmail'];
    $ring_count = $_POST['ring_count'];
		mysqli_query($con, " update  companies set voicemail='$voicemail', voicemail_closed='$voicemail_closed',  opt_hours='$opt_hours',  international='$international', international_closed='$international_closed',  close_number='$close_number',  		assigned_number='$assigned_number', ring_count = '$ring_count', send_transcript = '$send_transcript', transcriptEmail = '$transcriptEmail' where idx=$companyId");

		header ('location:opt_hours.php?cId='.$companyId.'&saved=1');
}


	

	$days['mon'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Monday');
	$days['tue'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Tuesday' );
	$days['wed'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Wednesday' );
	$days['thu'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Thursday' );
	$days['fri'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Friday' );
	$days['sat'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Saturday' );
	$days['sun'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Sunday' );

$select_qry =mysqli_query($con, "select * from call_hours where company_id=$companyId ") or die (mysqli_error($con));
while ($items=mysqli_fetch_array($select_qry))
{
	
	$days[$items['day']]['open_time'] 	=  $items['open_time'];
	$days[$items['day']]['close_time'] 	=  $items['close_time'];
	$days[$items['day']]['status'] 		=  $items['status'];
	
	

}

$company_settings = $db->getCompanySettings($companyId);

	
$assigned_number = $company_settings->assigned_number;
$voicemail = $company_settings->voicemail;
$international = $company_settings->international;
$voicemail_closed = $company_settings->voicemail_closed;
$international_closed = $company_settings->international_closed;
$opt_hours = $company_settings->opt_hours;
$close_number = $company_settings->close_number;
$send_transcript = $company_settings->send_transcript;
$transcriptEmail = $company_settings->transcriptEmail;
$ring_count = $company_settings->ring_count;
if (empty($ring_count)) $ring_count = 60;

if (isset($_GET['act'])) {
  if ($_GET['act'] == "GET_RECORDED_AUDIO") {
    $content = $company_settings->voicemail_content;
    Util::generateFlashAudioPlayer($content,"sm");
    exit();
  }
}

?>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



<link href="../css/style.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/ivr2.css" />

<link href="css/jqcss/jquery-ui-1.10.3.custom.css" rel="stylesheet">

<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="../js/jquery.ui.timepicker.addon.js?1380796534"></script>
<script type="text/javascript" src="../jsmain/user.js"></script>

<link href="../player/skin/jplayer-black-and-blue.css<?php echo "?" . $build_number; ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../player/jquery.jplayer.min.js<?php echo "?" . $build_number; ?>"></script>

<script type="text/javascript" >

$(document).ready(function () {
  <?php if($opt_hours == 0) { ?>
      $("#opt_hours_table tr:eq(3)").css('height', '318px');
      $('#ring1numberIframe', window.parent.document).attr('height', '530');
  <?php
  }
  ?>

	$('.date_picker').timepicker()

  <?php
  if(@$lcl < 2) {
  ?>
      $('#ring1numberIframe', window.parent.document).attr('height', '180');
  <?php
  }
  ?>
});
</script>
<style >

.block form input.text {
    width: 410px;
    background: #fefefe;
    border: 1px solid #bbb;
    font-family: "Lucida Grande", Verdana, sans-serif;
    font-size: 14px;
    color: #333;
    padding: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    outline: none;
    vertical-align: middle;
}

.block {

    border: 0px solid #ccc;
    padding: 0;
	margin:0px;
}
.block .block_content {
    overflow: hidden;
    background: #fff;
    border-left: 0px solid #ccc;
    border-right: 0px solid #ccc;
    padding: 0;
	margin:0px;
}
.block table tr td,
.block table tr th {
    border-bottom: 0px solid #ddd;
    padding: 5px;
    line-height: normal;
    text-align: left;
}
.block form input.radio,
.block form input.checkbox {
    vertical-align: top;
	text-align:left;
	margin-left:10px!important; margin-top:3px !important;
}


</style>
</head>

<body>
<?php if(isset($_GET['saved']) && $_GET['saved']==1){ ?>
<script type="text/javascript">
    parent.window.infoMsgDialog("Settings saved!");
</script>
<?php } ?>

<div class="block" style="width:580px;" >
<div class="block_content">
<form onsubmit="return beforeSubmit();" name="frm1" action="" method="post">
    <input type="hidden" name="saved" value="1"/>
<table id="opt_hours_table" width="580" border="0"  >
    <?php
    if(@$lcl>=2){
    ?>
  <tr>
    <td colspan="5" style="padding-top: 10px; padding-bottom: 10px;">
      <label style="width: 147px; display: inline-block;" for="assigned_number">Specify Hours:</label>
      <select name="opt_hours" onchange="selectOptHours(this);">
        <option value="0">No</option>
        <option value="1" <?php  if ( $opt_hours =='1' ) echo ' selected="selected" '?>>Yes</option>
      </select>
    </td>
  </tr>
  <?php }
  else {
    ?>
      <input type="hidden" name="opt_hours" value="0" />
    <?php
  } ?>
  <tr>
    <td colspan="5" style="border-top: solid 1px #CCCCCC; padding-top: 10px; padding-bottom: 10px;">
      <label style="width: 147px; display: inline-block;" for="assigned_number">Phone number open:</label>
      <input class="text tiny " type="text"  name="assigned_number" id="assigned_number" style="display:inline; width:315px; padding:6px; margin:0px !important;" value="<?php echo $assigned_number ?>"/>
      
      <div style="padding-left: 146px; padding-top: 5px;">
        <label style="display: inline-block; width: 234px;"><input type="checkbox" id="international" name="international" <?php if ($international==1) {echo  "checked=checked ";} ?>  /> International number</label>
        <label><input type="checkbox" id="voicemail" name="voicemail" <?php if ($voicemail ==1) {echo  "checked=checked ";} ?> style="margin-left: 0px;" /> Voicemail</label>
      </div>

    </td>
  </tr>

  <?php
  if(@$lcl>=2){
  ?>
    <tr <?php if($opt_hours == 0) { ?>style="display: none;"<?php } ?>>
      <td colspan="5" style="border-top: solid 1px #CCCCCC; padding-top: 10px; padding-bottom: 10px;"><label for="close_number" style="width: 147px; display: inline-block;">When closed:</label>
        <label><input type="checkbox" id="forward_number" style="margin-left: 0px;" name="forward_number" onclick="if ($(this).is(':checked')) { $('#close_number').prop('disabled', false); $('#close_number').focus(); } else { $('#close_number').prop('disabled', true); }" <?php if (!empty($close_number)) { ?>checked="checked"<?php } ?> /> Forward number: </label> <input class="text tiny " type="text"  name="close_number" id="close_number"  style="display:inline; width:167px; padding:6px; margin:0px !important;" value="<?php echo $close_number; ?>" <?php if (empty($close_number)) { ?>disabled="disabled"<?php } ?> />

        <div style="padding-left: 146px; padding-top: 5px;">
          <label style="display: inline-block; width: 234px;"><input type="checkbox" id="international_closed" name="international_closed" <?php if ($international_closed==1) {echo  "checked=checked ";} ?> /> International number</label>
          <label><input type="checkbox" id="voicemail_closed" name="voicemail_closed" <?php if ($voicemail_closed ==1) {echo  "checked=checked ";} ?> style="margin-left: 0px;" /> Voicemail</label>
        </div>
      </td>
    </tr>

    <tr>
      <td colspan="5" style="border-top: solid 1px #CCCCCC; padding-top: 10px; padding-bottom: 10px; height: 112px; vertical-align: top;">
        <label style="margin-right: 54px;">Voicemail Settings:</label>
        <fieldset class="ivr-Menu ivr2-input-container" style="clear: both; margin-top: 10px; position: absolute; background: #FFFFFF; width: 565px;">
          <div class="ivr-Menu-selector" style="display: block">
              <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                <?php if ($company_settings->voicemail_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($company_settings->voicemail_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
              </div>
              <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                <?php if ($company_settings->voicemail_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($company_settings->voicemail_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
              </div>
              <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                <?php if ($company_settings->voicemail_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($company_settings->voicemail_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
              </div>
              <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                <?php if ($company_settings->voicemail_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($company_settings->voicemail_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
              </div>
            </div>
          <div class="ivr-Menu-editor ">

            <div class="ivr-Menu-editor-padding" style="padding: 10px;">
              <div class="ivr-Menu-read-text" style="display: none;">
                <div class="title-bar"> <span class="editor-label">Text to speak</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                <br>
                <div>
                  <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                    <label class="field-label">
                      <form action="" method="post" onsubmit="return false;">
                        <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="margin-bottom: 5px;"><?php echo (($company_settings->voicemail_type == 'Text')? $company_settings->voicemail_content:''); ?></textarea>

                        <?php
                        $voice = $company_settings->voicemail_text_voice;
                        $language = $company_settings->voicemail_text_language;
                        ?>
                        <label class="field-label-left" style="width: 55px;">Voice: </label>
                        <select class="styled" id="voice" onchange="var language = $(this).parents('div.block').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                          <?php
                          echo Util::getTwilioVoices($voice);
                          ?>
                        </select>

                        <br clear="all" />

                        <label class="field-label-left" style="width: 55px;">Dialect: </label>
                        <select class="styled" id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                          <?php
                          echo Util::getTwilioLanguages($voice, $language);
                          ?>
                        </select>

                        <br clear="all" /><br />

                        <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('div.block').find('#voice').val(), $(this).parents('div.block').find('#language').val(), $(this).parents('div.block').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline;" />

                        <script type="text/javascript">
                          $(document).ready(function() {
                            $("#voice").trigger("change");
                          });
                        </script>

                        <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px;" />
                      </form>
                    </label>
                  </fieldset>
                </div>
                <br>
                <br>
              </div>
              <div class="ivr-audio-upload" style="display: none;">
                <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                <div class="swfupload-container">
                  <div class="explanation"> <br>
                    
                    <span class="title" <?php if ( $company_settings->voicemail_type != 'Audio' ) echo ' style="display:none" ' ?>  id="voicefilename" >Voice to play: <strong>
                    <?php 
              echo (($company_settings->voicemail_type == 'Audio')? $company_settings->voicemail_content:''); ?>
                    </strong></span> <br>
                    
                    
                    <span class="title">Click to select a file:  <input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></span>
                     <span class="title" id="statusUpload">&nbsp;</span>
                  </div>
                
                
                </div>
              </div>
              <div class="ivr-mp3-url" style="display: none;">
                <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                <div class="swfupload-container">
                  <div class="explanation"> <br>           
                    
                    <span class="title">
                      <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($company_settings->voicemail_type == 'MP3_URL')? $company_settings->voicemail_content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                      <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                    </span>

                    <br /><br />

                    <span class="title" <?php if ( $company_settings->voicemail_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                    <?php 
              echo (($company_settings->voicemail_type == 'MP3_URL')? $company_settings->voicemail_content:''); ?>
                    </strong></span>

                  </div>
                
                
                </div>
              </div>
              <div class="ivr-record-audio" style="display: none;">
                <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                <div class="swfupload-container">
                  <div class="explanation"> <br>           
                    
                    Caller ID:

                    <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                      <option value="">Select number</option>
                      <?php
                      $numbers = $db->getNumbersOfCompany($_GET['cId']);
                      foreach ($numbers as $number) {
                        ?>
                          <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                        <?php
                      }
                      ?>
                    </select>

                    Your phone number:

                    <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 156px;" />

                    <br /><br />

                    <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio(this);" style="margin-left: 211px;" />

                    <br /><br />

                    <span class="title" <?php if ( $company_settings->voicemail_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSaved" >
                      <?php if ($company_settings->voicemail_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($company_settings->voicemail_content, 'sm'); } ?>
                    </span>

                  </div>
                
                
                </div>
              </div>
            </div>

          </div>
        </fieldset>

        <div style="clear: both;"></div>

        <fieldset class="ivr2-input-container" style="margin-bottom:15px; padding-top: 90px;">
          <label><input type="checkbox" style="margin-top:10px; margin-left:0px;" id="send_transcript" name="send_transcript" <?php if (@$send_transcript) { ?> checked="checked" <?php } ?> /> Send Transcript</label>

          <span style="padding-left: 66px;">
            <label for="transcriptEmail"><strong>To Email:</strong> </label>
            <input class="text medium" type="text" id="transcriptEmail" name="transcriptEmail"  value="<?php echo @$transcriptEmail; ?>" style="display:inline; width: 276px;" />
          </span>

        </fieldset>

        <div>
          <label style="width: 170px; display: inline-block;" for="assigned_number">Ring Count to Voicemail:</label>
          <input class="text tiny " type="number"  name="ring_count" id="ring_count" style="display:inline; width:60px; padding:6px; margin:0px !important; margin-bottom: 5px !important;" value="<?php echo $ring_count ?>" min="1" max="500" /> Seconds
        </div>

      </td>
    </tr>

    <tr <?php if($opt_hours == 0) { ?>style="display: none;"<?php } ?>>
      <td colspan="5" style="border-top: solid 1px #CCCCCC; padding-top: 10px; padding-bottom: 10px;">
        <?php
        $date = new DateTime(@$access[0], new DateTimeZone("UTC"));
        $date->setTimezone(new DateTimeZone($TIMEZONE));
        $date_return = $date->format("h:iA m/d/Y");
        ?>
        Local Time: <?php echo $date_return; ?>
      </td>
    </tr>

    <?php   foreach (	$days  as $k => $day ) {?>
      <tr <?php if($opt_hours == 0) { ?>style="display: none;"<?php } ?>>
        <td width="100"><label><?php echo  $day['name']; ?></label></td>
        <input type="hidden" value="<?php echo  $k; ?>" name="weekdays[<?php echo $k?>]" />
        <?php
        $dbOpenTime=substr($day['open_time'],0,5);
       $dbCloseTime=substr($day['close_time'],0,5);

       $dbOpenTimeExploded = explode(":", $dbOpenTime);
       if (!$dbOpenTimeExploded[0]) $dbOpenTimeExploded[0] = 8;
       $open_am_pm = "am";
       if ($dbOpenTimeExploded[0] > 11) {
         $open_am_pm = "pm";
       }
       if ($dbOpenTimeExploded[0] == '00') {
         $dbOpenTimeExploded[0] = 12;
       }

       $dbCloseTimeExploded = explode(":", $dbCloseTime);
       if (!$dbCloseTimeExploded[0]) $dbCloseTimeExploded[0] = 17;
       $closed_am_pm = "am";
       if ($dbCloseTimeExploded[0] > 11) {
         $closed_am_pm = "pm";
       }
       if ($dbCloseTimeExploded[0] == '00') {
         $dbCloseTimeExploded[0] = 12;
       }
    	
    	?>
        <td width="160">
          
          <select name="hour_open[<?php echo $k?>]" style="width: 46px; text-align: center;">
            <?php for ($i=1; $i<=12; $i++) { 
              $value = ($i < 10) ? "0".$i : $i;
              ?> 
              <option value="<?php echo $value; ?>" <?php if (@$dbOpenTimeExploded[0] == $value || $dbOpenTimeExploded[0] == ($value + 12)) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
            <?php } ?>
          </select>

          <select name="minute_open[<?php echo $k?>]" style="width: 46px; text-align: center;">
            <?php for ($i=0; $i<=59; $i++) { 
              $value = ($i < 10) ? "0".$i : $i;
              ?> 
              <option value="<?php echo $value; ?>" <?php if (@$dbOpenTimeExploded[1] == $value) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
            <?php } ?>
          </select>

          <select name="ampm_open[<?php echo $k?>]" style="width: 49px; text-align: center;">
              <option value="am">AM</option>
              <option value="pm" <?php if ($open_am_pm == "pm") { ?>selected="selected"<?php } ?>>PM</option>
          </select>
        </td>

        <td>-</td>
          
        <td width="150">
          <select name="hour_close[<?php echo $k?>]" style="width: 46px; text-align: center;">
            <?php for ($i=1; $i<=12; $i++) { 
              $value = ($i < 10) ? "0".$i : $i;
              ?> 
              <option value="<?php echo $value; ?>" <?php if (@$dbCloseTimeExploded[0] == $value || $dbCloseTimeExploded[0] == ($value + 12)) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
            <?php } ?>
          </select>

          <select name="minute_close[<?php echo $k?>]" style="width: 46px; text-align: center;">
            <?php for ($i=0; $i<=59; $i++) { 
              $value = ($i < 10) ? "0".$i : $i;
              ?> 
              <option value="<?php echo $value; ?>" <?php if (@$dbCloseTimeExploded[1] == $value) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
            <?php } ?>
          </select>

          <select name="ampm_close[<?php echo $k?>]" style="width: 49px; text-align: center;">
              <option value="am">AM</option>
              <option value="pm" <?php if ($closed_am_pm == "pm") { ?>selected="selected"<?php } ?>>PM</option>
          </select>
        </td>
      
        <td><label><input type="checkbox" name="chk[<?php echo $k?>]" <?php if ($day['status']==1) {echo  "checked=checked ";} ?> /> Closed</label></td>

      </tr>
    <?php  }?>

<?php  }?>  
 
  <tr>
   <td colspan="5" style="text-align: center; padding-top: 17px;"><input type="submit" class="submit mid" value="Save" name="save" /></td>
  </tr>
 
  
</table>


</form>

<br /><br />

</div>

</div>

<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>

<script type="text/javascript">
    function beforeSubmit(){
        console.log("b4smt");
        if ($("#assigned_number").val().length != 10 && !$("input[name='international']").is(":checked") && $("#assigned_number").val() != "") {
            parent.window.errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
            return false;
        }
        if ($("#close_number").val().length != 10 && !$("input[name='international']").is(":checked") && $("#close_number").val() != "") {
            parent.window.errMsgDialog("Please enter the 10 digit number without punctuation or spaces. If this is an international number (not US or CA), please check the box.");
            return false;
        }
        return true;
    }

    function selectOptHours(el) {
      el = $(el);

      if (el.val() == 1) {
        for (var i = 2; i <= 11; i++) {
          $("#opt_hours_table tr:eq(" + i + ")").show();
        }
        $('#ring1numberIframe', window.parent.document).attr('height', '700');
        $("#opt_hours_table tr:eq(3)").css('height', 'auto');
      }
      else {
        for (var i = 2; i <= 11; i++) {
          $("#opt_hours_table tr:eq(" + i + ")").hide();
        }
        $("#opt_hours_table tr:eq(3)").show();
        $("#opt_hours_table tr:eq(3)").css('height', '318px');
        $("#opt_hours_table tr:eq(3)").css('vertical-align', 'top');
        $('#ring1numberIframe', window.parent.document).attr('height', '530');
      }
    }

    function showAudioText(obj)
    {
      
      
      var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
      audioChoice.hide();
      audioChoice.parent().children('.ivr-Menu-editor').show();
      var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');
      
      if ( obj.id  == 'txt' ) 
      {   
        
        subDiv.children('.ivr-Menu-read-text').show();
        
        //////////////// only to avoid  file button clickable in text area    
        $('[name="uploadfile"]').css('z-index','-1');   
      }
      else if ( obj.id  == 'upload_mp3' ) {
        $('[name="uploadfile"]').css('z-index','2147483583');
        
        subDiv.children('.ivr-audio-upload').show();  
        SubObj = subDiv.children('.ivr-audio-upload').find('#uploadFileButton');    
        UploadFile(SubObj); 
      }
      else if ( obj.id  == 'mp3_url' ) {
        subDiv.children('.ivr-mp3-url').show();
      }
      else if ( obj.id  == 'record_audio' ) {
        subDiv.children('.ivr-record-audio').show();
      }
      
    }

    var closeBtnHtml = '<a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a>';

    function removeSelectedOption(obj) {
        parent.window.promptMsg('Are you sure ?', function() {
          SaveContent(obj, 'EMPTY_WIDGET');
          var thisBlock = $(obj).closest('.ivr-Menu');
          $(thisBlock).find(".ttsMwCloseBtn").remove();

          $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
          $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');

          $(thisBlock).find('.content').val('');
          $(thisBlock).find('.type').val('');
          $(thisBlock).find('.voice').val('');
          $(thisBlock).find('.language').val('');
        });
    }

    function SaveContent(obj, content_type) {
      var thisBlock = $(obj).closest('.block');
      var blockId = $(obj).closest('.block').prop('id');
      blockId = blockId.replace(/blockObj-/g, '');


      switch (content_type) {
        case "Text_mail":
            {
                var dho = $(thisBlock).find('#drop_here_obj');
                var voice_text = $(thisBlock).find('#readtxt_mail').val();
                var voice = $(thisBlock).find('#voice').val();
                var language = $(thisBlock).find('#language').val();
                $(thisBlock).find('#txt').addClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                var wId = $(dho).data("wid");
                var pId = $(dho).data("pid");
                var nId = $(dho).data("nid");

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#txt').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                //alert(voice_text + wId );
                /*console.log(dho);

                    */
                //////
                $.ajax({
                  type: "POST",
                  url: "opt_hours.php",
                  dataType: "html",
                  data: {
                      voice_text: voice_text,
                      voice: voice,
                      language: language,
                      cId: '<?php echo $companyId; ?>'
                  }
                })
                .done(function (msg) {
                    $('.ivr-Menu-close-button').click();
                    parent.infoMsgDialog("Text Saved Successfully");
                });
                break;
            }
            case "MP3_URL":
            {
                var dho = $(thisBlock).find('#drop_here_obj');
                var mp3_url = $(thisBlock).find('#mp3_url_text').val();
                $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                $(thisBlock).find('#mp3_url').addClass('ivr-Menu-Selected');
                $(thisBlock).find('#record_audio').removeClass('ivr-Menu-Selected');
                var wId = $(dho).data("wid");
                var pId = $(dho).data("pid");
                var nId = $(dho).data("nid");

                $(thisBlock).find(".ttsMwCloseBtn").remove();
                $(thisBlock).find('#mp3_url').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
                //alert(voice_text + wId );
                /*console.log(dho);

                    */
                //////
                $.ajax({
                    type: "POST",
                    url: "opt_hours.php",
                    dataType: "html",
                    data: {
                        cId: '<?php echo $companyId; ?>',
                        mp3_url: mp3_url
                    }
                })
                .done(function (msg) {
                      $('.ivr-Menu-close-button').click();
                      parent.infoMsgDialog("MP3 URL Saved Successfully");

                      $(obj).closest('.explanation').find('#mp3UrlSaved').css('display' ,'block');
                      $(obj).closest('.explanation').find('#mp3UrlSaved').html('MP3 to play: <strong>' + mp3_url + '</strong>');
                 
                });
                break;
            }
            case "EMPTY_WIDGET":
            {
                $.ajax({
                    type: "POST",
                    url: "opt_hours.php",
                    dataType: "html",
                    data: {
                        cId: '<?php echo $companyId; ?>',
                        EMPTY_WIDGET: 'EMPTY_WIDGET'
                    }
                })
                .done(function (msg) {
                });
                break;
            }
        }
    }

    function UploadFile(obj)
      {

      new AjaxUpload(obj, {
          
          
          action: 'opt_hours.php?cId=<?php echo $companyId; ?>',
          name: 'uploadfile',
          onSubmit: function(file, ext){
             if (! (ext && /^(mp3|wma)$/.test(ext))){ 
                        // extension is not allowed 
              status.text('Only MP3 files are allowed');
              return false;
            }
            $("#uploadFileButton").val('Uploading...');
          },
          onComplete: function(file, response){
            //On completion clear the status
            
            //Add uploaded file to list
            
            if(response!="error"){
              $('.ivr-Menu-close-button').click();
              parent.msgDialog("File Uploaded Successfully");
              $('#txt').removeClass('ivr-Menu-Selected');
              $('#upload_mp3').addClass('ivr-Menu-Selected');
              $('#mp3_url').removeClass('ivr-Menu-Selected');
              $('#record_audio').removeClass('ivr-Menu-Selected');
              $("#uploadFileButton").val('Upload');

              $('#voicefilename').html(response);
              $('#voicefilename').show();

              $(thisBlock).find(".ttsMwCloseBtn").remove();
              $('#upload_mp3').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);
            } else{
              parent.errMsgDialog("There was a problem uploading, please try again.");
            }
            $("#uploadFileButton").val('Uploading');
          }
        });
          
      }

      function CloseButton(obj)
      {
        
        
        
        var audioChoice = $(obj).closest('.ivr-Menu');
        
        var audioChoiceEditor = audioChoice.children('.ivr-Menu-editor');
        var audioChoiceSelector = audioChoice.children('.ivr-Menu-selector');
        
        var subDiv  = audioChoiceEditor.children('.ivr-Menu-editor-padding');
        
        audioChoiceSelector.show();
        audioChoiceEditor.hide();
        subDiv.children('.ivr-audio-upload').hide();  
        subDiv.children('.ivr-Menu-read-text').hide();
        subDiv.children('.ivr-mp3-url').hide();
        subDiv.children('.ivr-record-audio').hide();
        

      }

      function testVoice(voice, language, text) {
        if ($("#test_voice_iframe_wrapper").length == 0) {
          $("body").append('<div id="test_voice_iframe_wrapper" style="display: none;">' +
                    '<form id="test_voice_form" action="../test_voice.php" method="post" target="test_voice_iframe">' +
                      '<input type="hidden" name="voice" id="voice" />' +
                      '<input type="hidden" name="language" id="language" />' +
                      '<input type="hidden" name="text" id="text" />' +
                      '<input type="submit" name="submit" id="submit" value="Submit" />' +
                    '</form>' +
                    '<iframe id="test_voice_iframe" name="test_voice_iframe"></iframe>' +
                  '</form>'
          );
        }

        $("#test_voice_iframe_wrapper #voice").val(voice);
        $("#test_voice_iframe_wrapper #language").val(language);
        $("#test_voice_iframe_wrapper #text").val(text);
        $('#test_voice_iframe_wrapper #submit').click()
       }

       function recordAudio(obj) {
        companyId = '<?php echo $companyId; ?>';

        if ($(obj).val() == "Call Me") {
          var record_from = $(obj).parents('div.block').find('#record_from').val();
          var record_to = $(obj).parents('div.block').find('#record_to').val();

          if (record_from == "") {
            parent.errMsgDialog("Please select Caller ID.");
            return false;
          }

          if (record_to == "") {
            parent.errMsgDialog("Please enter your phone number.")
            return false;
          }

          $(obj).val('Stop');

          if ($("#record_audio_iframe_wrapper").length == 0) {
            $("body").append('<div id="record_audio_iframe_wrapper" style="display: none;">' +
                      '<form id="record_audio_form" action="../record_audio.php" method="post" target="record_audio_iframe">' +
                        '<input type="hidden" name="companyId" id="companyId" />' +
                        '<input type="hidden" name="record_from" id="record_from" />' +
                        '<input type="hidden" name="record_to" id="record_to" />' +
                        '<input type="submit" name="submit" id="submit" value="Submit" />' +
                      '</form>' +
                      '<iframe id="record_audio_iframe" name="record_audio_iframe"></iframe>' +
                    '</form>'
            );
          }

          $("#record_audio_iframe_wrapper #companyId").val(companyId);
          $("#record_audio_iframe_wrapper #record_from").val(record_from);
          $("#record_audio_iframe_wrapper #record_to").val(record_to);
          $('#record_audio_iframe_wrapper #submit').click()
        }
        else {
          $(obj).val('Please wait...');
          $("#record_audio_iframe").contents().find("#disconnectBtn").click();

          setTimeout(function() {
            $.get("opt_hours.php?act=GET_RECORDED_AUDIO&cId=" + companyId, function(response) {
              var thisBlock = $(obj).closest('.block');

              $(thisBlock).find('#recordedAudioSaved').css('display' ,'block');
                    $(thisBlock).find('#recordedAudioSaved').html(response);

                    $('.ivr-Menu-close-button').click();
                    parent.infoMsgDialog("Audio recording saved!");
                    
                    $(thisBlock).find('#txt').removeClass('ivr-Menu-Selected');
                    $(thisBlock).find('#upload_mp3').removeClass('ivr-Menu-Selected');
                    $(thisBlock).find('#mp3_url').removeClass('ivr-Menu-Selected');
                    $(thisBlock).find('#record_audio').addClass('ivr-Menu-Selected');

                    $(thisBlock).find(".ttsMwCloseBtn").remove();
                    $(thisBlock).find('#record_audio').parents('div.ivr-Menu-selector-item-wrapper').append(closeBtnHtml);

                    $(obj).val('Call Me');
            });
          }, 2000);
        }
       }
</script>
</body>
</html>