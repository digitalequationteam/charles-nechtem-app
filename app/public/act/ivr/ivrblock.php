<?php 

$blockTitle = array('Greetings'=>'Greetings',
'Menu'=>'Menu',
'Dial'=>'Dial',
'RoundRobin'=>'Round Robin',
'MultipleNumbers'=>'Multiple Numbers',
'SMS'=>'SMS',
'Voicemail'=>'Voicemail',
'VoicemailWithoutRing'=>'Voicemail',
'Hangup'=>'Hangup',
'Drop here'=>'Drop here',
'OptIn'=>'Opt-In',
'SingleAgent'=>'Agent',
'AgentIVR'=>'Agent IVR',
'BusinessHours'=>'Business Hours'
);

function genRoundRobinBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
	global $blockTitle;	
  global $con;

 	
	if ( $wId) {	 $dialData =         getDetail($wId); }
 ?>
<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>"  data-myflowtype="RoundRobin">
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>Round Robin  </h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
 
  <div class="block_content ivr2-input-container">
    <input type="checkbox" style="margin-top:10px; margin-left:0px;" onclick="SaveRoundRobin(this)"	 id="international_number" name="international_number" <?php echo (( $content =='1') ? ' checked="checked" ': ''); ?>>
      <label for="option_International"> <strong>International Number</strong> </label>
      
      
     <Br /> <Br />
       <label><strong>Phone Number:</strong> </label>
      <input class="text medium" type="text" id="PhoneNumber"  value=""  style="display:inline; " >
      <Br />
      <br />
    
        <input type="button"  class="submit mid"  value="Add" onClick="SaveRoundRobinNumber(this)" style="margin-left:300px;">
      </label>
    
    <table style="width: 100%;" id="number_t"><thead><tr><th align="left" width="80%">Phone Number</th><th align="left">Delete</th></tr></thead><tbody>
    <?php 
	if ( $wId) 
	{
		$multiRS = mysqli_query($con, " SELECT * FROM  `call_ivr_round_robin` where  wId = $wId order by idx ");
		if (mysqli_num_rows($multiRS)>0) 
		{
			while($multiRow= mysqli_fetch_assoc($multiRS))
			{
		?>
    <tr><td><?php echo $multiRow['number'] ?></td><td><a href="javascript:void(0);" onclick="DeleteRoundRobinNumber(this,'<?php echo $multiRow['idx'] ?>')" ><img src="images/delete.gif"></a></td></tr>
    <?php } 
		} 
	}
	?>
    </tbody></table>
    
    <br/>
    <span>
    <h2>What do you want to do next?</h2>
    </span> <a  href="javascript:void(0)" onClick="showContent(this)"   
      data-flowtype="<?php echo $flowtype ?>" 
      data-pid="<?php echo $pId ?>" 
      data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
      data-wid="<?php echo $wId ?>"   class="drp"><?php echo $blockTitle[$nextflowtype] ?></a>  </div>
  <!-- .block_content ends -->
  
  <div class="bendl"></div>
  <div class="bendr"></div>
</div>

	<?php	
}

function genMultipleNumbersBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
	global $blockTitle;	
  global $con;

	?>
    <div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>"  data-myflowtype="MultiNumber">
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>Dial Multiple Numbers  </h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
 
  <div class="block_content ivr2-input-container">
    <input type="checkbox" style="margin-top:10px; margin-left:0px;" onclick="SaveMultiNumberinternational(this)"	 id="international_number" name="international_number" <?php echo (( $content =='1') ? ' checked="checked" ': ''); ?>>
      <label for="option_International"> <strong>International Number</strong> </label>
      <Br />
      <br />
       <label><strong>Phone Number:</strong> </label>
      <input class="text medium" type="text" id="PhoneNumber"  value=""  style="display:inline; " >
      <Br />
      <br />
     
        <input type="button"  class="submit mid"  value="Add" onClick="SaveMultiNumber(this)" style="margin-left:300px;">
      </label>
    
    <table style="width: 100%;" id="number_t"><thead><tr><th align="left" width="80%">Phone Number</th><th align="left">Delete</th></tr></thead><tbody>
    <?php 
	if ( $wId) 
	{
		$multiRS = mysqli_query($con, " SELECT * FROM  `call_ivr_multiple_numbers` where  wId = $wId order by idx ");
		if (mysqli_num_rows($multiRS)>0) 
		{
			while($multiRow= mysqli_fetch_assoc($multiRS))
			{
		?>
    <tr><td><?php echo $multiRow['number'] ?></td><td><a href="javascript:void(0);" onclick="DeleteMultiNumber(this,'<?php echo $multiRow['idx'] ?>')" ==""><img src="images/delete.gif"></a></td></tr>
    <?php } 
		} 
	}
	?>
    </tbody></table>
    
    <br/>
    <span>
    <h2>What do you want to do next?</h2>
    </span> <a  href="javascript:void(0)" onClick="showContent(this)"   
      data-flowtype="<?php echo $flowtype ?>" 
      data-pid="<?php echo $pId ?>" 
      data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
      data-wid="<?php echo $wId ?>"   class="drp"><?php echo $blockTitle[$nextflowtype] ?></a>  </div>
  <!-- .block_content ends -->
  
  <div class="bendl"></div>
  <div class="bendr"></div>
</div>
<?php    
}


function  genDialBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
global $blockTitle; 
global $con;

global $db;
global $TIMEZONE;

$days = array();

$days['mon'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Monday');
$days['tue'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Tuesday' );
$days['wed'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Wednesday' );
$days['thu'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Thursday' );
$days['fri'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Friday' );
$days['sat'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Saturday' );
$days['sun'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Sunday' );

$dial_details = $content;
if (!empty($dial_details)) {
  $dial_details = json_decode($dial_details, true);

 foreach ($dial_details['call_hours'] as $item)
 {  
   $days[$item['day']]['open_time'] = $item['open_time'];
   $days[$item['day']]['close_time'] = $item['close_time'];
   $days[$item['day']]['status'] = $item['status'];
 }
}
?>

<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>"  data-myflowtype="Dial">
 <div class="block_head">
   <div class="bheadl"></div>
   <div class="bheadr"></div>
   <h2>Dial a Number </h2>
   <div style="display:inline-block; float: right;">
     <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
   </div>
 </div>
 <!-- .block_head ends -->
 
 <div class="block_content ivr2-input-container">
   <form onsubmit="return false;" name="frm1" action="" method="post">
     <table id="opt_hours_table" width="554" border="0"  >
       <tr>
         <td colspan="5" style="padding-top: 10px; padding-bottom: 10px;">
           <label style="margin-right: 57px; display: inline-block;" for="assigned_number">Specify Hours:</label>
           <select name="opt_hours" onchange="selectOptHours(this);" style="border: solid 1px gray; padding: 0px; height: 20px; display: inline-block;">
             <option value="0">No</option>
             <option value="1" <?php if (@$dial_details['specify_hours'] == '1') echo ' selected="selected" '?>>Yes</option>
           </select>

           <script type="text/javascript">
             $(document).ready(function() {
               $('select[name="opt_hours"]').trigger('change');
             });
           </script>
         </td>
       </tr>

       <tr>
         <td colspan="5" style="border-top: solid 1px #CCCCCC; padding-top: 10px; padding-bottom: 10px;">
           <label style="margin-right: 19px;" for="assigned_number">Phone number open:</label>
           <input class="text tiny " type="text"  name="assigned_number" id="assigned_number" style="display:inline; width:315px; padding: 6px; height: 20px; margin:0px !important;" value="<?php echo @$dial_details['open_number'] ?>"/>
           
           <div style="padding-left: 147px; padding-top: 5px;">
             <label><input type="checkbox" id="international" name="international" <?php if (@$dial_details['open_number_int']==1) {echo  "checked=checked ";} ?> /> International number</label>
             <label>
              <?php
              $ovw_count = (empty($wId) ? 0 : mysqli_num_rows(mysqli_query($con, "SELECT wId FROM call_ivr_widget WHERE pId = '".$wId."' AND data = 'open'")));
              $dho_text = ($ovw_count == 0) ? "Drop Voicemail" : "Voicemail";
              ?>
             <a  href="javascript:void(0)" onClick="showContent(this)"  
                          data-flowtype="Voicemail" 
                          data-submenu="yes" 
                          data-voicemail-only="yes" 
                          data-number="open" 
                          id="drop_here_obj"  
                          data-wid="<?php echo $wId ?>"   class="drp" data-drp-location="opt-hours"><?php echo $dho_text; ?></a>
             </label>
           </div>

         </td>
       </tr>

       <tr>
         <td colspan="5" style="border-top: solid 1px #CCCCCC; padding-top: 10px; padding-bottom: 10px;"><label for="close_number" style="margin-right: 54px;">When closed:</label>
           <label><input type="checkbox" id="forward_number" name="forward_number" onclick="if ($(this).is(':checked')) { $('#close_number').prop('disabled', false); $('#close_number').focus(); } else { $('#close_number').prop('disabled', true); }" <?php if (!empty($dial_details['close_number'])) { ?>checked="checked"<?php } ?> /> Forward number: </label> <input class="text tiny " type="text"  name="close_number" id="close_number"  style="display:inline; width: 136px; padding: 6px; height: 20px; margin:0px !important;" value="<?php echo @$dial_details['close_number']; ?>" <?php if (empty($dial_details['close_number'])) { ?>disabled="disabled"<?php } ?> />

           <div style="padding-left: 147px; padding-top: 5px;">
             <label><input type="checkbox" id="international_closed" name="international_closed" <?php if (@$dial_details['close_number_int']==1) {echo  "checked=checked ";} ?> /> International number</label>
              <?php
              $cvw_count = (empty($wId) ? 0 : mysqli_num_rows(mysqli_query($con, "SELECT wId FROM call_ivr_widget WHERE pId = '".$wId."' AND data = 'closed'")));
              $dho_text = ($cvw_count == 0) ? "Drop Voicemail" : "Voicemail";
              ?>
             <a  href="javascript:void(0)" onClick="showContent(this)"  
                          data-flowtype="Voicemail" 
                          data-submenu="yes" 
                          data-voicemail-only="yes" 
                          data-number="closed" 
                          id="drop_here_obj"  
                          data-wid="<?php echo $wId ?>"   class="drp" data-drp-location="opt-hours"><?php echo $dho_text; ?></a>
           </div>
         </td>
       </tr>
         <tr>
           <td colspan="5" style="border-top: solid 1px #CCCCCC; padding-top: 10px; padding-bottom: 10px;">
             <?php
             $date = new DateTime($access[0], new DateTimeZone("UTC"));
             $date->setTimezone(new DateTimeZone($TIMEZONE));
             $date_return = $date->format("h:iA m/d/Y");
             ?>
             Local Time: <?php echo $date_return; ?>
           </td>
         </tr>

         <?php   foreach ( $days  as $k => $day ) {?>
           <tr>
             <td width="70"><label><?php echo  $day['name']; ?></label></td>
             <input type="hidden" value="<?php echo  $k; ?>" class="weekday_val" name="weekdays[<?php echo $k?>]" />
             <?php
             $dbOpenTime=substr($day['open_time'],0,5);
             $dbCloseTime=substr($day['close_time'],0,5);

             $dbOpenTimeExploded = explode(":", $dbOpenTime);
             if (!$dbOpenTimeExploded[0]) $dbOpenTimeExploded[0] = 8;
             $open_am_pm = "am";
             if ($dbOpenTimeExploded[0] > 11) {
               $open_am_pm = "pm";
             }
             if ($dbOpenTimeExploded[0] == '00') {
               $dbOpenTimeExploded[0] = 12;
             }

             $dbCloseTimeExploded = explode(":", $dbCloseTime);
             if (!$dbCloseTimeExploded[0]) $dbCloseTimeExploded[0] = 17;
             $closed_am_pm = "am";
             if ($dbCloseTimeExploded[0] > 11) {
               $closed_am_pm = "pm";
             }
             if ($dbCloseTimeExploded[0] == '00') {
               $dbCloseTimeExploded[0] = 12;
             }
           
           ?>
             <td width="200" style="padding: 5px;">
               
               <select class="hour_open_<?php echo $k?>" name="hour_open[<?php echo $k?>]" style="width: 48px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                 <?php for ($i=1; $i<=12; $i++) { 
                   $value = ($i < 10) ? "0".$i : $i;
                   ?> 
                   <option value="<?php echo $value; ?>" <?php if ($dbOpenTimeExploded[0] == $value || $dbOpenTimeExploded[0] == ($value + 12)) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                 <?php } ?>
               </select>

               <select class="minute_open_<?php echo $k?>" name="minute_open[<?php echo $k?>]" style="width: 48px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                 <?php for ($i=0; $i<=59; $i++) { 
                   $value = ($i < 10) ? "0".$i : $i;
                   ?> 
                   <option value="<?php echo $value; ?>" <?php if ($dbOpenTimeExploded[1] == $value) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                 <?php } ?>
               </select>

               <select class="ampm_open_<?php echo $k?>" name="ampm_open[<?php echo $k?>]" style="width: 60px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                   <option value="am">AM</option>
                   <option value="pm" <?php if ($open_am_pm == "pm") { ?>selected="selected"<?php } ?>>PM</option>
               </select>
             </td>

             <td style="padding: 5px;">-</td>
               
             <td width="200" style="padding: 5px;">
               <select class="hour_close_<?php echo $k?>" name="hour_close[<?php echo $k?>]" style="width: 48px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                 <?php for ($i=1; $i<=12; $i++) { 
                   $value = ($i < 10) ? "0".$i : $i;
                   ?> 
                   <option value="<?php echo $value; ?>" <?php if ($dbCloseTimeExploded[0] == $value || $dbCloseTimeExploded[0] == ($value + 12)) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                 <?php } ?>
               </select>

               <select class="minute_close_<?php echo $k?>" name="minute_close[<?php echo $k?>]" style="width: 48px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                 <?php for ($i=0; $i<=59; $i++) { 
                   $value = ($i < 10) ? "0".$i : $i;
                   ?> 
                   <option value="<?php echo $value; ?>" <?php if ($dbCloseTimeExploded[1] == $value) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                 <?php } ?>
               </select>

               <select class="ampm_close_<?php echo $k?>" name="ampm_close[<?php echo $k?>]" style="width: 60px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                   <option value="am">AM</option>
                   <option value="pm" <?php if ($closed_am_pm == "pm") { ?>selected="selected"<?php } ?>>PM</option>
               </select>
             </td>
           
             <td style="padding: 5px;"><label><input class="chk_<?php echo $k?>" type="checkbox" name="chk[<?php echo $k?>]" <?php if ($day['status']==1) {echo  "checked=checked ";} ?> /> Closed</label></td>

           </tr>

 <?php } ?>
  
   
   </table>
 </form>

   <p>
     <label>
       <input type="button"  class="submit mid"  value="Save" onClick="SaveContent(this,'Dail')" style="margin-left: 413px;">
     </label>
   </p>
   <br/>
   <span>
   <h2>What you want to do next ?</h2>
   </span> <a  href="javascript:void(0)" onClick="showContent(this)"   
     data-flowtype="<?php echo $flowtype ?>" 
     data-pid="<?php echo $pId ?>" 
     data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
     data-wid="<?php echo $wId ?>"   class="drp"><?php echo $blockTitle[$nextflowtype] ?></a>  </div>
 <!-- .block_content ends -->
 
 <div class="bendl"></div>
 <div class="bendr"></div>
</div>
<?php }

function genSMSBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
	global $blockTitle;	
  global $con;

?>
<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>" data-myflowtype="SMS">
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>SMS Reply Content </h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
  
  <div class="block_content"> <span> </span><br/>
    <textarea name="sms_content" id="sms_content" style="width:550px; height:80px;" ><?php echo $content ?></textarea>
    <bR>
    <?php
    $sms_delay = getMetaDetail($wId, "sms_delay");
    ?>
    <label class="field-label-left" style="width: 55px;">Delay: </label>
    <select class="styled" id="sms_delay" style="display:inline; width: 50px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
      <option value="0">0</span>
      <option value="5" <?php if ($sms_delay == 5) { ?>selected="selected"<?php } ?>>5</span>
      <option value="10" <?php if ($sms_delay == 10) { ?>selected="selected"<?php } ?>>10</span>
      <option value="15" <?php if ($sms_delay == 15) { ?>selected="selected"<?php } ?>>15</span>
      <option value="30" <?php if ($sms_delay == 30) { ?>selected="selected"<?php } ?>>30<span>
      <option value="60" <?php if ($sms_delay == 60) { ?>selected="selected"<?php } ?>>60</span>
    </select>
    &nbsp;&nbsp;
    <label>minute(s)</label>

    <input  type="button"  class="submit mid" id="save_sms" value="Save"  style="margin-left:438px; margin-top:5px" onClick="SaveContent(this,'SMS')">
    <br/>
    <br/>
    <span>
    <h2>What do you want to do next?</h2>
    </span> <a  href="javascript:void(0)" onClick="showContent(this)"   
      data-flowtype="<?php echo $flowtype ?>" 
      data-pid="<?php echo $pId ?>" 
      data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
      data-wid="<?php echo $wId ?>"   class="drp"><?php echo $blockTitle[$nextflowtype] ?></a>  </div>
  <!-- .block_content ends -->
  
  <div class="bendl"></div>
  <div class="bendr"></div>
</div>
<?php }

function genVoicemailBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
  global $db;
  global $con;

  global $blockTitle; 
//echo  'wId:'. $wId.'---pId:'. $pId.'---nId:'. $nId .'---flowtype:'. $flowtype .'---content_type:'.$content_type .'---content:'.$content.'---data:'.$data.'---nextflowtype:'. $nextflowtype;

  $ring_count = getMetaDetail($wId, "ring_count");
  if (empty($ring_count)) $ring_count = 60;
?>
<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>" data-myflowtype="Voicemail" <?php if ($data == 'open' || $data == 'closed') { ?>data-data="<?php echo $data; ?>"<?php } ?>>
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>Voice Mail </h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
  
  <div class="block_content">
    <label style="width: 170px; display: inline-block;" for="assigned_number">Ring Count to Voicemail:</label>
    <input class="text tiny " type="number"  name="ring_count" id="ring_count" style="display:inline; width:60px; padding:6px; margin:0px !important; margin-bottom: 5px !important;" value="<?php echo $ring_count ?>" min="1" max="500" onchange="saveVoicemailRingCount(this);" /> Seconds
    <br />

    <span>Let  the user know about voice mail</span>
    <fieldset class="ivr-Menu ivr2-input-container">
      <div class="ivr-Menu-selector" style="display: block">
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($content_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($content_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($content_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($content_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
        </div>
      </div>
      <div class="ivr-Menu-editor ">
        <div class="ivr-Menu-editor-padding" style="padding: 10px;">
          <div class="ivr-Menu-read-text" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <br>
            <div>
              <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                <label class="field-label">
                  <form action="" method="post" onsubmit="return false;">
                    <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="width: 526px;"><?php echo (($content_type == 'Text')? $content:''); ?></textarea>

                    <?php
                    $voice = getMetaDetail($wId, "voice");
                    $language = getMetaDetail($wId, "language");
                    ?>
                    <label class="field-label-left" style="width: 55px;">Voice: </label>
                    <select class="styled" id="voice" onchange="var language = $(this).parents('div.block').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                      <?php
                      echo Util::getTwilioVoices($voice);
                      ?>
                    </select>

                    <br clear="all" />

                    <label class="field-label-left" style="width: 55px;">Dialect: </label>
                    <select class="styled" id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                      <?php
                      echo Util::getTwilioLanguages($voice, $language);
                      ?>
                    </select>

                    <br clear="all" /><br />

                    <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('div.block').find('#voice').val(), $(this).parents('div.block').find('#language').val(), $(this).parents('div.block').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline;" />
                    <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px;" />
                  </form>
                </label>
              </fieldset>
            </div>
            <br>
            <br>
          </div>
          <div class="ivr-audio-upload" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>
                
                <span class="title" <?php if ( $content_type != 'Audio' ) echo ' style="display:none" ' ?>  id="voicefilename" >Voice to play: <strong>
                <?php 
          echo (($content_type == 'Audio')? $content:''); ?>
                </strong></span> <br>
                
                
                <span class="title">Click to select a file:  <input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></span>
                 <span class="title" id="statusUpload">&nbsp;</span>
              </div>
            
            
            </div>
          </div>
          <div class="ivr-mp3-url" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>           
                
                <span class="title">
                  <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($content_type == 'MP3_URL')? $content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                  <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                </span>

                <br /><br />

                <span class="title" <?php if ( $content_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                <?php 
          echo (($content_type == 'MP3_URL')? $content:''); ?>
                </strong></span>

              </div>
            
            
            </div>
          </div>
          <div class="ivr-record-audio" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>           
                
                Caller ID:

                <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                  <option value="">Select number</option>
                  <?php
                  $numbers = $db->getNumbersOfCompany($_GET['cId']);
                  foreach ($numbers as $number) {
                    ?>
                      <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                    <?php
                  }
                  ?>
                </select>

                Your phone number:

                <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px;" />

                <br /><br />

                <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio('<?php echo $wId; ?>', this);" style="margin-left: 211px;" />

                <br /><br />

                <span class="title" <?php if ( $content_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSaved" >
                  <?php if ($content_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($content, 'sm'); } ?>
                </span>

              </div>
            
            
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <span>
    <br />

    <fieldset class="ivr2-input-container" style="margin-bottom:15px;">
      <label><input type="checkbox" style="margin-top:10px; margin-left:0px;" onclick="saveTranscriptDetails(this);" id="send_transcript" name="send_transcript" <?php if (getMetaDetail($wId, 'send_transcript') == 1) { ?> checked="checked" <?php } ?> /> Send Transcript</label>

      <span style="padding-left: 66px;">
        <label for="TranscriptEmail"><strong>To Email:</strong> </label>
        <input class="text medium" type="text" id="TranscriptEmail" name="TranscriptEmail"  value="<?php echo getMetaDetail($wId, 'TranscriptEmail'); ?>" style="display:inline;" onkeyup="saveTranscriptDetails(this);" />
      </span>

    </fieldset>

    </div>
  <!-- .block_content ends -->

  <div style="display: none;">
    <a href="javascript:void(0)" onClick="showContent(this)"
        data-flowtype="<?php echo $flowtype ?>" 
        data-pid="<?php echo $pId ?>" 
        data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
        data-wid="<?php echo $wId ?>"   class="drp">Drop Here</a>
  </div>
  
  <div class="bendl"></div>
  <div class="bendr"></div>
</div>
<?php }

function genVoicemailWithoutRingBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
  global $db;
  global $con;

  global $blockTitle; 
//echo  'wId:'. $wId.'---pId:'. $pId.'---nId:'. $nId .'---flowtype:'. $flowtype .'---content_type:'.$content_type .'---content:'.$content.'---data:'.$data.'---nextflowtype:'. $nextflowtype;

  $ring_count = getMetaDetail($wId, "ring_count");
  if (empty($ring_count)) $ring_count = 60;
?>
<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>" data-myflowtype="Voicemail" <?php if ($data == 'open' || $data == 'closed') { ?>data-data="<?php echo $data; ?>"<?php } ?>>
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>Voice Mail </h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
  
  <div class="block_content">

    <span>Let  the user know about voice mail</span>
    <fieldset class="ivr-Menu ivr2-input-container">
      <div class="ivr-Menu-selector" style="display: block">
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($content_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($content_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($content_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($content_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
        </div>
      </div>
      <div class="ivr-Menu-editor ">
        <div class="ivr-Menu-editor-padding" style="padding: 10px;">
          <div class="ivr-Menu-read-text" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <br>
            <div>
              <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                <label class="field-label">
                  <form action="" method="post" onsubmit="return false;">
                    <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="width: 526px;"><?php echo (($content_type == 'Text')? $content:''); ?></textarea>

                    <?php
                    $voice = getMetaDetail($wId, "voice");
                    $language = getMetaDetail($wId, "language");
                    ?>
                    <label class="field-label-left" style="width: 55px;">Voice: </label>
                    <select class="styled" id="voice" onchange="var language = $(this).parents('div.block').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                      <?php
                      echo Util::getTwilioVoices($voice);
                      ?>
                    </select>

                    <br clear="all" />

                    <label class="field-label-left" style="width: 55px;">Dialect: </label>
                    <select class="styled" id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                      <?php
                      echo Util::getTwilioLanguages($voice, $language);
                      ?>
                    </select>

                    <br clear="all" /><br />

                    <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('div.block').find('#voice').val(), $(this).parents('div.block').find('#language').val(), $(this).parents('div.block').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline;" />
                    <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px;" />
                  </form>
                </label>
              </fieldset>
            </div>
            <br>
            <br>
          </div>
          <div class="ivr-audio-upload" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>
                
                <span class="title" <?php if ( $content_type != 'Audio' ) echo ' style="display:none" ' ?>  id="voicefilename" >Voice to play: <strong>
                <?php 
          echo (($content_type == 'Audio')? $content:''); ?>
                </strong></span> <br>
                
                
                <span class="title">Click to select a file:  <input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></span>
                 <span class="title" id="statusUpload">&nbsp;</span>
              </div>
            
            
            </div>
          </div>
          <div class="ivr-mp3-url" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>           
                
                <span class="title">
                  <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($content_type == 'MP3_URL')? $content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                  <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                </span>

                <br /><br />

                <span class="title" <?php if ( $content_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                <?php 
          echo (($content_type == 'MP3_URL')? $content:''); ?>
                </strong></span>

              </div>
            
            
            </div>
          </div>
          <div class="ivr-record-audio" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>           
                
                Caller ID:

                <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                  <option value="">Select number</option>
                  <?php
                  $numbers = $db->getNumbersOfCompany($_GET['cId']);
                  foreach ($numbers as $number) {
                    ?>
                      <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                    <?php
                  }
                  ?>
                </select>

                Your phone number:

                <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px;" />

                <br /><br />

                <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio('<?php echo $wId; ?>', this);" style="margin-left: 211px;" />

                <br /><br />

                <span class="title" <?php if ( $content_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSaved" >
                  <?php if ($content_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($content, 'sm'); } ?>
                </span>

              </div>
            
            
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <span>
    <br />

    <fieldset class="ivr2-input-container" style="margin-bottom:15px;">
      <label><input type="checkbox" style="margin-top:10px; margin-left:0px;" onclick="saveTranscriptDetails(this);" id="send_transcript" name="send_transcript" <?php if (getMetaDetail($wId, 'send_transcript') == 1) { ?> checked="checked" <?php } ?> /> Send Transcript</label>

      <span style="padding-left: 66px;">
        <label for="TranscriptEmail"><strong>To Email:</strong> </label>
        <input class="text medium" type="text" id="TranscriptEmail" name="TranscriptEmail"  value="<?php echo getMetaDetail($wId, 'TranscriptEmail'); ?>" style="display:inline;" onkeyup="saveTranscriptDetails(this);" />
      </span>

    </fieldset>

    </div>
  <!-- .block_content ends -->

  <div style="display: none;">
    <a href="javascript:void(0)" onClick="showContent(this)"
        data-flowtype="<?php echo $flowtype ?>" 
        data-pid="<?php echo $pId ?>" 
        data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
        data-wid="<?php echo $wId ?>"   class="drp">Drop Here</a>
  </div>
  
  <div class="bendl"></div>
  <div class="bendr"></div>
</div>
<?php }

function genGreetingsBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
	global $blockTitle;	
  global $con;

  global $db;
?>
<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>" data-myflowtype="Greetings">
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>Greetings </h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
  
  <div class="block_content">
    <fieldset class="ivr-Menu ivr2-input-container">
      <div class="ivr-Menu-selector" style="display: block">
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($content_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($content_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($content_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
        </div>
        <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
          <?php if ($content_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
          <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($content_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
        </div>
      </div>
      <div class="ivr-Menu-editor ">
        <div class="ivr-Menu-editor-padding" style="padding: 10px;">
          <div class="ivr-Menu-read-text" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <br>
            <div>
              <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                <label class="field-label">
                  <form action="" method="post" onsubmit="return false;">
                    <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="width: 526px;"><?php echo (($content_type == 'Text')? $content:''); ?></textarea>

                    <?php
                    $voice = getMetaDetail($wId, "voice");
                    $language = getMetaDetail($wId, "language");
                    ?>
                    <label class="field-label-left" style="width: 55px;">Voice: </label>
                    <select class="styled" id="voice" onchange="var language = $(this).parents('div.block').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                      <?php
                      echo Util::getTwilioVoices($voice);
                      ?>
                    </select>

                    <br clear="all" />

                    <label class="field-label-left" style="width: 55px;">Dialect: </label>
                    <select class="styled" id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                      <?php
                      echo Util::getTwilioLanguages($voice, $language);
                      ?>
                    </select>

                    <br clear="all" /><br />

                    <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('div.block').find('#voice').val(), $(this).parents('div.block').find('#language').val(), $(this).parents('div.block').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline;" />
                    <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px;" />
                  </form>
                </label>
              </fieldset>
            </div>
            <br>
            <br>
          </div>
          <div class="ivr-audio-upload" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>
                
                <span class="title" <?php if ( $content_type != 'Audio' ) echo ' style="display:none" ' ?>  id="voicefilename" >Voice to play: <strong>
                <?php 
          echo (($content_type == 'Audio')? $content:''); ?>
                </strong></span> <br>
                
                
                <span class="title">Click to select a file:  <input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></span>
                 <span class="title" id="statusUpload">&nbsp;</span>
              </div>
            
            
            </div>
          </div>
          <div class="ivr-mp3-url" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>           
                
                <span class="title">
                  <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($content_type == 'MP3_URL')? $content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                  <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                </span>

                <br /><br />

                <span class="title" <?php if ( $content_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                <?php 
          echo (($content_type == 'MP3_URL')? $content:''); ?>
                </strong></span>

              </div>
            
            
            </div>
          </div>
          <div class="ivr-record-audio" style="display: none;">
            <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
            <div class="swfupload-container">
              <div class="explanation"> <br>           
                
                Caller ID:

                <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                  <option value="">Select number</option>
                  <?php
                  $numbers = $db->getNumbersOfCompany($_GET['cId']);
                  foreach ($numbers as $number) {
                    ?>
                      <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                    <?php
                  }
                  ?>
                </select>

                Your phone number:

                <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px;" />

                <br /><br />

                <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio('<?php echo $wId; ?>', this);" style="margin-left: 211px;" />

                <br /><br />

                <span class="title" <?php if ( $content_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSaved" >
                  <?php if ($content_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($content, 'sm'); } ?>
                </span>

              </div>
            
            
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <br>
    <br/>
    <span>
    <h2>What do you want to do next?</h2>
    </span> <a  href="javascript:void(0)" onClick="showContent(this)"   
      data-flowtype="<?php echo $flowtype ?>" 
      data-pid="<?php echo $pId ?>" 
      data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
      data-wid="<?php echo $wId ?>"   class="drp"><?php echo $blockTitle[$nextflowtype] ?></a>  </div>
  <!-- .block_content ends -->
  
  <div class="bendl"></div>
  <div class="bendr"></div>
</div>
<?php } 
function genMenuBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
	global $blockTitle;
  global $con;

  global $db;
?>
<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>" data-myflowtype="Menu">
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>IVR MENU </h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
  
  <div class="block_content">
    <form action="process_ivr.php" method="post" name="add_readtxt" enctype="multipart/form-data">
      <input type="hidden" value="1" name="callFlow">
      <input type="hidden" value="<?php echo $company_id ?>" name="company">
      <div class="settings-panel ivrMenu">
        <?php if ( isset ($_SESSION['success']) )
                                  { ?>
        <div align="center" style="color:#0C0"> <?php echo $_SESSION['success']  ?> </div>
        <?php  }
                                  
                                  unset ($_SESSION['success']);
                                  ?>
        <div class="ivrMenu">
          <div class="menu-prompt">
            <fieldset class="ivr-Menu ivr2-input-container">
              <div class="ivr-Menu-selector" style="display: block">
                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                  <?php if ($content_type == 'Text') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                  <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item <?php echo (($content_type == 'Text')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speech</span></a> </div>
                </div>
                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                  <?php if ($content_type == 'Audio') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                  <div class="padding-and-border"> <a id="upload_mp3" class="ivr-Menu-selector-item <?php echo (($content_type == 'Audio')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Upload MP3</span></a></div>
                </div>
                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                  <?php if ($content_type == 'MP3_URL') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                  <div class="padding-and-border"> <a id="mp3_url" class="ivr-Menu-selector-item <?php echo (($content_type == 'MP3_URL')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Enter MP3 URL</span></a></div>
                </div>
                <div class="ivr-Menu-selector-item-wrapper" style="width: 25%;">
                  <?php if ($content_type == 'RECORD_AUDIO') { ?><a href="javascript: void(0);" class="ttsMwCloseBtn" onclick="removeSelectedOption(this);" style="display: block; position: absolute; right: 7px; top: 6px; color: red; z-index: 9; font-weight: bold; cursor: pointer;"><img src="images/delete.gif" style="border: 0px;"></a><?php } ?>
                  <div class="padding-and-border"> <a id="record_audio" class="ivr-Menu-selector-item <?php echo (($content_type == 'RECORD_AUDIO')? ' ivr-Menu-Selected ':''); ?>" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Record Audio</span></a></div>
                </div>
              </div>
              <div class="ivr-Menu-editor ">
                <div class="ivr-Menu-editor-padding" style="padding: 10px;">
                  <div class="ivr-Menu-read-text" style="display: none;">
                    <div class="title-bar"> <span class="editor-label">Text To Speech</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                    <br>
                    <div>
                      <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                        <label class="field-label">
                          <form action="" method="post" onsubmit="return false;">
                            <textarea class="voicemail-text" name="readtxt_mail" id="readtxt_mail" style="width: 526px;"><?php echo (($content_type == 'Text')? $content:''); ?></textarea>

                            <?php
                            $voice = getMetaDetail($wId, "voice");
                            $language = getMetaDetail($wId, "language");
                            ?>
                            <label class="field-label-left" style="width: 55px;">Voice: </label>
                            <select class="styled" id="voice" onchange="var language = $(this).parents('div.block').find('#language'); language.find('option').hide().prop('disabled', true); language.find('option[data-voice=' + this.value + ']').show().prop('disabled', false); if (language.find('option:selected').attr('data-voice') != this.value) { language.find('option').removeAttr('selected', 'selected'); language.find('option:visible').first().attr('selected', 'selected'); }" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                              <?php
                              echo Util::getTwilioVoices($voice);
                              ?>
                            </select>

                            <br clear="all" />

                            <label class="field-label-left" style="width: 55px;">Dialect: </label>
                            <select class="styled" id="language" style="display:inline; width: 200px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px;">
                              <?php
                              echo Util::getTwilioLanguages($voice, $language);
                              ?>
                            </select>

                            <br clear="all" /><br />

                            <input type="button" class="submit mid" id="test_voice_text" value="Test" onclick="testVoice($(this).parents('div.block').find('#voice').val(), $(this).parents('div.block').find('#language').val(), $(this).parents('div.block').find('#readtxt_mail').val());" style="margin-left: 0px; display: inline;" />
                            <input type="button"  class="submit mid" id="save_voicetext" value="Save" onClick="SaveContent(this,'Text_mail')" style="float: right; margin-left: 0px; margin-bottom: 5px;" />
                          </form>
                        </label>
                      </fieldset>
                    </div>
                    <br>
                    <br>
                  </div>
                  <div class="ivr-audio-upload" style="display: none;">
                    <div class="title-bar"> <span class="editor-label">Upload an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                    <div class="swfupload-container">
                      <div class="explanation"> <br>
                        
                        <span class="title" <?php if ( $content_type != 'Audio' ) echo ' style="display:none" ' ?>  id="voicefilename" >Voice to play: <strong>
                        <?php 
                  echo (($content_type == 'Audio')? $content:''); ?>
                        </strong></span> <br>
                        
                        
                        <span class="title">Click to select a file:  <input type="button"   class="submit mid fileupload" id="uploadFileButton"   value="Upload" ></span>
                         <span class="title" id="statusUpload">&nbsp;</span>
                      </div>
                    
                    
                    </div>
                  </div>
                  <div class="ivr-mp3-url" style="display: none;">
                    <div class="title-bar"> <span class="editor-label">Enter the URL to an MP3 file</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                    <div class="swfupload-container">
                      <div class="explanation"> <br>           
                        
                        <span class="title">
                          <input type="text" name="mp3_url_text" id="mp3_url_text" value="<?php echo (($content_type == 'MP3_URL')? $content:''); ?>" class="text ui-widget-content ui-corner-all" style="width: 100%; height: 24px; padding: 2px; margin-left: 0px; margin-bottom: 5px;" />
                          <input type="button" class="submit mid" value="Save" style="margin-left: 0 !important;" onClick="SaveContent(this,'MP3_URL')" />
                        </span>

                        <br /><br />

                        <span class="title" <?php if ( $content_type != 'MP3_URL' ) echo ' style="display:none" ' ?>  id="mp3UrlSaved" >MP3 to play: <strong>
                        <?php 
                  echo (($content_type == 'MP3_URL')? $content:''); ?>
                        </strong></span>

                      </div>
                    
                    
                    </div>
                  </div>
                  <div class="ivr-record-audio" style="display: none;">
                    <div class="title-bar"> <span class="editor-label">Have ACT call you and record your own audio</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
                    <div class="swfupload-container">
                      <div class="explanation"> <br>           
                        
                        Caller ID:

                        <select id="record_from" style="display:inline; width: 135px; border: solid 1px #CCCCCC; padding: 3px; margin:0px !important; height: 27px; margin-right: 50px !important;">
                          <option value="">Select number</option>
                          <?php
                          $numbers = $db->getNumbersOfCompany($_GET['cId']);
                          foreach ($numbers as $number) {
                            ?>
                              <option value="<?php echo $number->number; ?>"><?php echo $number->number; ?></option>
                            <?php
                          }
                          ?>
                        </select>

                        Your phone number:

                        <input type="text" id="record_to" class="text ui-widget-content ui-corner-all" style="height: 23px; padding: 2px; width: 146px" />

                        <br /><br />

                        <input type="button"  class="submit mid" id="call_me_record" value="Call Me" onclick="recordAudio('<?php echo $wId; ?>', this);" style="margin-left: 211px;" />

                        <br /><br />

                        <span class="title" <?php if ( $content_type != 'RECORD_AUDIO' ) echo ' style="display:none" ' ?>  id="recordedAudioSaved" >
                          <?php if ($content_type == 'RECORD_AUDIO') { echo Util::generateFlashAudioPlayer($content, 'sm'); } ?>
                        </span>

                      </div>
                    
                    
                    </div>
                  </div>
                </div>
            </fieldset>
          </div>
          <?php if ( isset ($_SESSION['callFlow_err']) )
                                  { ?>
          <div align="center" style="color:#F00"> <?php echo @$_SESSION['callFlow_err']['upload_mp3'];  ?> </div>
          <?php  }?>
          <?php if ( isset ($_SESSION['callFlow_err']) )
                                  { ?>
          <div align="center" style="color:#F00"> <?php echo @$_SESSION['callFlow_err']['readtxt'];  ?> </div>
          <?php  }?>
          <br>
          <h2>Menu Options</h2>
          <table class="ivr2-menu-grid options-table addRemoveRows" id="mytable">
            <thead>
              <tr>
                <td width="15%">Keypress</td>
                <td width="20%">&nbsp;</td>
                <td width="50%">Dial Phone# / Voice Mail</td>
                <td width="5%"></td>
              </tr>
            </thead>
            <?php  $data = getRows($wId); ?>
          </table>
          <?php 
		   $repeat =3;
		   if ( $wId) 
{
	
	 $menuData =         getDetail($wId);
	 if  (!empty($menuData))	$repeat=  $menuData['meta_data'];
}?>
          <fieldset class="ivr2-input-complex ivr2-input-container">
            
            <label class="field-label-left" style="width: 55px;">Repeat: </label>
            <input class="text tiny" type="text"  onkeyup="saveRepeat(this)" onkeypress='validate(event)'   maxlength="1"
                style="display:inline; width:40px; padding:1px 1px 1px 20px; margin:0px !important;   " value="<?php echo $repeat ?>" >
            &nbsp;&nbsp;
            <label>time(s)</label>

            <br />
            <br />
            
            <?php
            $delay = getMetaDetail($wId, "menu_delay");
            ?>
            <label class="field-label-left" style="width: 55px;">Delay: </label>
            <input class="text tiny" type="text"  onkeyup="saveDelay(this)" onkeypress='validate(event)'   maxlength="1"
                style="display:inline; width:40px; padding:1px 1px 1px 20px; margin:0px !important;   " value="<?php echo $delay ?>" >
            &nbsp;&nbsp;
            <label>second(s)</label>

          </fieldset>
        </div>
      </div>
    </form>
    <span>
    <h2>If caller does not press key:</h2>
    </span> <a  href="javascript:void(0)" onClick="showContent(this)"  
      data-menu="next" 
      data-flowtype="<?php echo $flowtype ?>" 
      data-pid="<?php echo $pId ?>" 
      data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
      data-wid="<?php echo $wId ?>"   class="drp"><?php echo $blockTitle[$nextflowtype] ?></a> </div>
  <!-- .block_content ends -->
  
  <div class="bendl"></div>
  <div class="bendr"></div>
</div>
<?php
return $data;
   } 


function genOptInBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here')
{
  global $con;

global $blockTitle; 
global $db;
global $TIMEZONE;

require_once '../include/ad_auto_dialer_files/lib/ad_lib_funcs.php';

$ad_contact_lists = ad_advb_cl_get_contact_lists(1, 0, true);
?>

<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>"  data-myflowtype="Dial">
 <div class="block_head">
   <div class="bheadl"></div>
   <div class="bheadr"></div>
   <h2>Opt-In</h2>
   <div style="display:inline-block; float: right;">
     <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
   </div>
 </div>
 <!-- .block_head ends -->
 
 <div class="block_content ivr2-input-container">
    <form method="post" onsubmit="return false;">
     <label style="margin-right: 57px; display: inline-block;" for="list_name">Add Caller to:</label>
     <select id="list_name" name="list_name" onchange="var display = ($(this).val() == 'new_list') ? 'block' : 'none'; $(this).parents('.block').find('#new_list_wrapper').css('display', display);" class='styled' style="border: solid 1px gray; padding: 0px; height: 20px; display: inline-block; width: 150px;">
       <option value="">Select</option>
       <?php foreach ($ad_contact_lists as $single_list_data) { ?>
        <option value="<?php echo Util::escapeString($single_list_data['list_name']); ?>" <?php if (Util::escapeString($content) == Util::escapeString($single_list_data['list_name'])) echo ' selected="selected" '?>><?php echo Util::escapeString($single_list_data['list_name']); ?></option>
       <?php } ?>
       <option value="new_list">New List</option>
     </select>
     <br />
     <div id="new_list_wrapper" style="display: none; padding-top: 5px; ">
        <label style="margin-right: 49px; display: inline-block;" for="new_list_name">New list name:</label>
        <input class="text tiny" type="text" name="new_list_name" id="new_list_name" style="display:inline; width:315px; padding: 6px; height: 20px; margin:0px !important;" value=""/>
     </div>
   </form>
   <p>
     <label>
       <input type="button"  class="submit mid"  value="Save" onClick="SaveContent(this,'OptIn')" style="margin-left: 413px;">
     </label>
   </p>
   <br/>
   <span>
   <h2>What you want to do next ?</h2>
   </span> <a  href="javascript:void(0)" onClick="showContent(this)"   
     data-flowtype="<?php echo $flowtype ?>" 
     data-pid="<?php echo $pId ?>" 
     data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
     data-wid="<?php echo $wId ?>"   class="drp"><?php echo $blockTitle[$nextflowtype] ?></a>  </div>
 <!-- .block_content ends -->
 
 <div class="bendl"></div>
 <div class="bendr"></div>
</div>
<?php
}

function genAgentIVRBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
  global $blockTitle;
  global $con;

  global $db;

  $data = (empty($content)) ? array() : json_decode($content, true);
  //var_dump($data);
?>
<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>" data-myflowtype="AgentIVR">
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>Agent IVR</h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
  
  <div class="block_content">
    <label style="width: 180px; display: inline-block;" for="applet_name">Name:</label>
    <input class="text tiny" type="text" name="applet_name" id="applet_name" style="display:inline; width: 120px; padding: 2px; height: 14px; margin:2px !important;" value="<?php echo (isset($data['applet_name']) ? $data['applet_name'] : ""); ?>" /> (Optional)
    <br clear="all" />
    
    <form action="process_ivr.php" method="post" name="add_readtxt" enctype="multipart/form-data">
      <input type="hidden" value="1" name="callFlow">
      <input type="hidden" value="<?php echo $company_id ?>" name="company">
      
      <?php echo Util::genTTSAudioPlugin(array("name" => "agentIVRgreeting_", "location" => "call_flow", "content" => $data['greeting']['content'], "content_type" => $data['greeting']['type'], "voice" => $data['greeting']['voice'], "language" => $data['greeting']['language'], "company_id" => $_GET['cId'])); ?>
    
      <label style="width: 180px; display: inline-block;" for="flow_type">Flow Type:</label>
       <select id="flow_type" name="flow_type" class='styled' style="border: solid 1px gray; padding: 0px; height: 20px; display: inline-block; width: 150px;">
         <option value="first_available" <?php if ($data['flow_type'] == "first_available") { ?>selected="selected"<?php } ?>>First Available</option>
         <option value="multiple_agents" <?php if ($data['flow_type'] == "multiple_agents") { ?>selected="selected"<?php } ?>>Multiple Agents</option>
         <option value="agent_round_robin" <?php if ($data['flow_type'] == "agent_round_robin") { ?>selected="selected"<?php } ?>>Agent Round Robin</option>
       </select>

      <br />

      <div style="padding-bottom: 0px; padding-top: 10px;">Hold Music:</div>

      <?php echo Util::genTTSAudioPlugin(array("name" => "agentIVRHoldMusic_", "location" => "call_flow", "content" => $data['hold_music']['content'], "content_type" => $data['hold_music']['type'], "voice" => $data['hold_music']['voice'], "language" => $data['hold_music']['language'], "company_id" => $_GET['cId'])); ?>
      
      <label style="width: 180px; display: inline-block;" for="prompt_message">Prompt Message:</label>
      <select id="prompt_message" name="prompt_message" class='styled' style="border: solid 1px gray; padding: 0px; height: 20px; display: inline-block; width: 180px;" onchange="changePMType(this);">
        <option value="none" <?php if ($data['none_type'] == "none") { ?>selected="selected"<?php } ?>>None</option>
          <option value="prompt_message" <?php if ($data['prompt_message_type'] == "prompt_message") { ?>selected="selected"<?php } ?>>Prompt Message</option>
          <option value="call_queue_notification" <?php if ($data['prompt_message_type'] == "call_queue_notification") { ?>selected="selected"<?php } ?>>Call Queue Notification</option>
      </select>

      <script type="text/javascript">
        $(document).ready(function() {
          $("#prompt_message").trigger('change');
        });
      </script>

      <div class="prompt_template" style="display: none;">
          <div class="pm_template" style="margin-bottom: -15px;">
              <div style="position: absolute; right: -85px; margin-top: 25px; z-index: 999;width: 100px;">
                  <a class="remove action deleteThisRow" href="javascript:void(0)" onclick="removePromptMessage(this);" style="margin-bottom: 5px;display: inline-block;float: left;height: 28px;margin-right: 5px;"><span class="replace">Remove</span></a>
                  <div style="padding-left: 2px;" class="controls">
                      <a href="javascript:void(0)" onclick="movePMUp(this);"><img src="../images/arrow-up.png" style="opacity: 0.7;" /></a>
                      <br />
                      <a href="javascript:void(0)" onclick="movePMDown(this);"><img src="../images/arrow-down.png" style="opacity: 0.7;" /></a>
                  </div>
              </div>
              <?php echo Util::genTTSAudioPlugin(array("name" => "agentIVRPromtMessage_", "location" => "call_flow", "content" => $prompt_message['content'], "content_type" => $prompt_message['content_type'], "voice" => $prompt_message['voice'], "language" => $prompt_message['language'], "company_id" => $_GET['cId'])); ?>
          </div>
      </div>

      <div id="prompt_messages" style="position: relative; padding-right: 20px; width: 470px;">
        <?php
        if (!isset($data['prompt_messages']))
          $data['prompt_messages'] = array(
            array()
          );

        foreach ($data['prompt_messages'] as $key => $prompt_message) {
          ?>
          <div class="pm_template" style="margin-bottom: -15px;">
            <div style="position: absolute; right: -85px; margin-top: 25px; z-index: 999;width: 100px;">
              <a class="remove action deleteThisRow" href="javascript:void(0)" onclick="removePromptMessage(this);" style="margin-bottom: 5px;display: inline-block;float: left;height: 28px;margin-right: 5px;"><span class="replace">Remove</span></a>
              <div style="padding-left: 2px;" class="controls">
                <a href="javascript:void(0)" onclick="movePMUp(this);"><img src="../images/arrow-up.png" style="opacity: 0.7;" /></a>
                <br />
                <a href="javascript:void(0)" onclick="movePMDown(this);"><img src="../images/arrow-down.png" style="opacity: 0.7;" /></a>
              </div>
            </div>
            <?php echo Util::genTTSAudioPlugin(array("name" => "agentIVRPromtMessage".$key."_", "location" => "call_flow", "content" => $prompt_message['content'], "content_type" => $prompt_message['content_type'], "voice" => $prompt_message['voice'], "language" => $prompt_message['language'], "company_id" => $_GET['cId'])); ?>
          </div>
          <?php
        }
        ?>
      </div>

      <div style="float: right; margin-right: 5px; position: relative;top: -48px;z-index: 9999;">
        <a class="add action btnAddMore btnAddMorePrompts" href="javascript:void(0)" onclick="addPromptMessage(this);"><span class="replace">Add</span></a>
      </div>

      <br clear="all" />
      <br clear="all" />

      <label style="width: 180px; display: inline-block;" for="agents_use_extensions">Use extensions:</label>
      <input type="checkbox" name="agents_use_extensions" id="agents_use_extensions" <?php if (@$data['agents_use_extensions'] == 1) { ?>checked="checked"<?php } ?> style="margin-left: 2px;" />
      <br clear="all" />

      <label style="width: 180px; display: inline-block;" for="agents_extension_length">Agents extension length:</label>
      <input class="text tiny" type="number" name="agents_extension_length" id="agents_extension_length" style="display:inline; width: 36px; padding: 2px; height: 14px; margin:2px !important;" value="<?php echo (isset($data['agents_extension_length']) ? $data['agents_extension_length'] : 4); ?>" /> Characters
      <br clear="all" />

      <label style="width: 180px; display: inline-block;" for="max_queue_size">Max Queue Size:</label>
      <input class="text tiny" type="number" name="max_queue_size" id="max_queue_size" style="display:inline; width: 36px; padding: 2px; height: 14px; margin:2px !important;" value="<?php echo (isset($data['max_queue_size']) ? $data['max_queue_size'] : 5); ?>" /> Callers
      <br clear="all" />

      <label style="width: 180px; display: inline-block;" for="repeat">Repeat:</label>
      <input class="text tiny" type="number" name="repeat" id="repeat" style="display:inline; width: 36px; padding: 2px; height: 14px; margin:2px !important;" value="<?php echo (isset($data['repeat']) ? $data['repeat'] : 1); ?>" /> Times
      <br clear="all" />

      <br>
      <h2>Agent Options</h2>
      <table class="ivr2-menu-grid options-table addRemoveRows" id="ivrAgentTable" style="width: 100%;">
        <thead>
          <tr>
            <td style="text-align: center;">Number</td>
            <td>&nbsp;</td>
            <td style="text-align: center;">Extension</td>
            <td style="text-align: center;">Agent</td>
            <td></td>
          </tr>
        </thead>
        <?php 
        //var_dump($data['agents']);
        if (!isset($data['agents']))
          $data['agents'] = array(
            array()
          );

        if (count($data['agents']) == 0)
          $data['agents'] = array(
            array()
          );

        $rowCount = 1;
        foreach ($data['agents'] as $k=>$v)
        {
          $thisBlockTitle = (empty($v['wId']) ? "Drop here" : "Agent");
        ?>
        <tr class = "row">
          <td style="text-align: center;" class="number"><?php echo $rowCount; ?></td>
          <td>
            <a href="javascript:void(0)" onclick="moveAgent(this, 'up');"><img src="../images/arrow-up.png" style="opacity: 0.7;" /></a>
            <br />
            <a href="javascript:void(0)" onclick="moveAgent(this, 'down');"><img src="../images/arrow-down.png" style="opacity: 0.7;" /></a>
          </td>
          <td style="text-align: center;">
            <fieldset class="ivr2-input-container">
              <input class="extension tiny" type="text" autocomplete="off" class="extension" value="<?php echo $v['extension']; ?>" style="margin-left: 48px;" />
            </fieldset>
          </td>
          <td style="text-align: center;"><a  href="javascript:void(0)" onClick="showContent(this)"  
                                  data-flowtype="<?php echo $v['flowtype']  ?>" 
                                  data-agentivr-only="yes" 
                                  data-pid="<?php echo $wId ?>" 
                                  data-nid="<?php echo $v['wId'] ?>"  id="drop_here_obj"  
                                  data-wid="<?php echo $v['wId'] ?>"   class="drp"><?php echo $thisBlockTitle;  ?></a></td>
          <td style="margin-left:10px" align="right"><?php if ( $rowCount ==1) { ?>
            <a class="add action btnAddMore" href="javascript:void(0)" id="btnAddMore" onClick="addMoreAgentRow(this)"><span class="replace">Add</span> </a>
            <?php } else{?>
            <a class="remove action deleteThisRow" href="javascript:void(0)" onclick="deleteAgentRow(this)"> <span class="replace" >Remove</span> </a>
            <?php }
            $rowCount++;
            ?></td>
        </tr>
        <?php } ?>
      </table>

      <br />

      <label style="width: 180px; display: inline-block;">Master Voicemail:</label>

      <?php
      $dho_text = (empty($data['master_voicemail_wId'])) ? "Drop Voicemail" : "Voicemail";
      ?>

      <a  href="javascript:void(0)" onClick="showContent(this)"  
                  data-flowtype="Voicemail" 
                  data-submenu="yes" 
                  data-voicemail-only="yes" 
                  data-agent-ivr-voicemail="yes" 
                  id="drop_here_obj"  
                  data-wid="<?php echo $wId; ?>"
                  data-nid="<?php echo $data['master_voicemail_wId']; ?>" class="drp" data-drp-location="agent-ivr"><?php echo $dho_text; ?></a>

      <br clear="all" />  

      <div style="display: none;">
        <a href="javascript:void(0)" onClick="showContent(this)"
            data-menu="next" 
            data-flowtype="<?php echo $flowtype ?>" 
            data-pid="<?php echo $pId ?>" 
            data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
            data-wid="<?php echo $wId ?>"   class="drp">Drop Here</a>
      </div>   

    </form>
  </div>
  <!-- .block_content ends -->
  
  <div class="bendl"></div>
  <div class="bendr"></div>
</div>
<?php
foreach ($data['agents'] as $k=>$v)
{
  if (!empty($v['wId'])) genSingleAgentBlock($v['wId'], $wId, '', "SingleAgent", '', json_encode($v), $data, 'Drop here');
}

if (!empty($data['master_voicemail_wId'])) {
  getWidget($data['master_voicemail_wId'], "");
}

return array();
   } 

function genSingleAgentBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
  global $db;
  global $con;

  global $blockTitle; 
//echo  'wId:'. $wId.'---pId:'. $pId.'---nId:'. $nId .'---flowtype:'. $flowtype .'---content_type:'.$content_type .'---content:'.$content.'---data:'.$data.'---nextflowtype:'. $nextflowtype;
$content = json_decode($content, true);
?>
<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>" data-myflowtype="SingleAgent" <?php if ($data == 'open' || $data == 'closed') { ?>data-data="<?php echo $data; ?>"<?php } ?>>
  <div class="block_head">
    <div class="bheadl"></div>
    <div class="bheadr"></div>
    <h2>Agent</h2>
    <div style="display:inline-block; float: right;">
      <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
    </div>
  </div>
  <!-- .block_head ends -->
  
  <div class="block_content"> 
    <?php
    $addons_list = Addons::getAddonList();
    $users = array();
    $sth = $db->customExecute("SELECT * FROM users u WHERE 
        (SELECT COUNT(*) FROM user_company uc WHERE u.idx = uc.user_id AND uc.company_id = '".$_GET['cId']."') > 0 AND
        (
          (SELECT COUNT(*) FROM user_addon_access uaa WHERE u.idx = uaa.user_id AND uaa.addon_id = '".$addons_list["ACT_BROWSER_SOFTPHONE"]["id"]."') > 0 OR
          u.access_lvl >= 1
        )
      ");
    $sth->execute();

    $result = $sth->fetchAll(PDO::FETCH_ASSOC);
    $users = array();
    foreach($result as $key => $row)
    {
        $users[] = array("user_data" => $row);
    }
    ?>
    <label style="width: 180px; display: inline-block;" for="flow_type">Select User:</label>
    <select id="agent_user" name="agent_user" class='styled' style="border: solid 1px gray; padding: 0px; height: 20px; display: inline-block; width: 150px;">
      <option value="0">None</option>
      <?php foreach ($users as $user) {
        if (!empty($user['user_data']['full_name'])) { ?>
          <option value="<?php echo $user['user_data']['idx']; ?>" <?php if ($content['user_id'] == $user['user_data']['idx']) { ?>selected="selected"<?php } ?>><?php echo Util::escapeString($user['user_data']['full_name']); ?> (<?php echo $user['user_data']['username']; ?>)</option>
      <?php }
      } ?>
    </select>

    <br />
    <br />

    <span>Voicemail</span>
    <?php echo Util::genTTSAudioPlugin(array("name" => "agentVoicemail_", "location" => "call_flow", "content" => $content['voicemail']['content'], "content_type" => $content['voicemail']['type'], "voice" => $content['voicemail']['voice'], "language" => $content['voicemail']['language'], "company_id" => $_GET['cId'])); ?>

    <br />

    <fieldset class="ivr2-input-container" style="margin-bottom:15px;">
      <label><input type="checkbox" style="margin-top:10px; margin-left:0px;" onclick="saveTranscriptDetails(this);" id="send_transcript" name="send_transcript" <?php if ($content['send_transcript'] == 1) { ?> checked="checked" <?php } ?> /> Send Transcript</label>

      <span style="padding-left: 66px;">
        <label for="TranscriptEmail"><strong>To Email:</strong> </label>
        <input class="text medium" type="text" id="TranscriptEmail" name="TranscriptEmail"  value="<?php echo @$content['transcript_email']; ?>" style="display:inline;" onkeyup="saveTranscriptDetails(this);" />
      </span>

    </fieldset>

  </div>
  <!-- .block_content ends -->

  <div style="display: none;">
    <a href="javascript:void(0)" onClick="showContent(this)"
        data-flowtype="<?php echo $flowtype ?>" 
        data-pid="<?php echo $pId ?>" 
        data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
        data-wid="<?php echo $wId ?>"   class="drp">Drop Here</a>
  </div>

  <div class="bendl"></div>
  <div class="bendr"></div>
</div>
<?php }

function  genBusinessHoursBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){
global $blockTitle; 
global $con;

global $db;
global $TIMEZONE;

$days = array();

$days['mon'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Monday');
$days['tue'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Tuesday' );
$days['wed'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Wednesday' );
$days['thu'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Thursday' );
$days['fri'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Friday' );
$days['sat'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Saturday' );
$days['sun'] = array('open_time'=> '0' ,'close_time'=> '0' ,'status'=> '0', 'name' => 'Sunday' );

$dial_details = $content;
if (!empty($dial_details)) {
  $dial_details = json_decode($dial_details, true);

 foreach ($dial_details['call_hours'] as $item)
 {  
   $days[$item['day']]['open_time'] = $item['open_time'];
   $days[$item['day']]['close_time'] = $item['close_time'];
   $days[$item['day']]['status'] = $item['status'];
 }
}
?>

<div class="block" id="blockObj<?php echo (( $wId =='') ? '': '-'); echo $wId; ?>"  data-myflowtype="Dial">
 <div class="block_head">
   <div class="bheadl"></div>
   <div class="bheadr"></div>
   <h2>Business Hours</h2>
   <div style="display:inline-block; float: right;">
     <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
   </div>
 </div>
 <!-- .block_head ends -->
 
 <div class="block_content ivr2-input-container">
   <form onsubmit="return false;" name="frm1" action="" method="post">
     <table id="opt_hours_table" width="554" border="0"  >
       <tr>
         <td colspan="5" style="padding-top: 10px; padding-bottom: 10px;">
           <label style="margin-right: 10px; display: block; float: left; padding-top: 24px;" for="assigned_number">When open:</label>
           
           <div style="float: left;">
              <?php
              $bhoNext = (empty($wId) ? array() : mysqli_fetch_array(mysqli_query($con, "SELECT wId, flowtype FROM call_ivr_widget WHERE pId = '".$wId."' AND data = 'open'")));
              $dho_text = (empty($bhoNext['wId'])) ? "Drop here" : $blockTitle[$bhoNext['flowtype']];
              ?>
              <a  href="javascript:void(0)" onClick="showContent(this)"  
                          data-flowtype="BusinessHours" 
                          data-submenu="yes" 
                          data-business-hours-only="yes" 
                          data-number="open" 
                          id="drop_here_obj"  
                          data-wid="<?php echo $wId ?>" data-nid="<?php echo $bhoNext['wId'] ?>" class="drp" data-drp-location="business-hours"><?php echo $dho_text; ?></a>
             </label>
           </div>

           <label style="margin-right: 10px; display: block; float: left; padding-top: 24px; margin-left: 20px;" for="assigned_number">When closed:</label>
           
           <div style="float: left;">
              <?php
              $bhoNext = (empty($wId) ? array() : mysqli_fetch_array(mysqli_query($con, "SELECT wId, flowtype FROM call_ivr_widget WHERE pId = '".$wId."' AND data = 'closed'")));
              $dho_text = (empty($bhoNext['wId'])) ? "Drop here" : $blockTitle[$bhoNext['flowtype']];
              ?>
              <a  href="javascript:void(0)" onClick="showContent(this)"  
                          data-flowtype="BusinessHours" 
                          data-submenu="yes" 
                          data-business-hours-only="yes" 
                          data-number="closed" 
                          id="drop_here_obj"  
                          data-wid="<?php echo $wId; ?>" data-nid="<?php echo $bhoNext['wId']; ?>" class="drp" data-drp-location="business-hours"><?php echo $dho_text; ?></a>
             </label>
           </div>

           <div style="clear: both;"></div>

         </td>
       </tr>

         <tr>
           <td colspan="5" style="border-top: solid 1px #CCCCCC; padding-top: 10px; padding-bottom: 10px;">
             <?php
             $date = new DateTime($access[0], new DateTimeZone("UTC"));
             $date->setTimezone(new DateTimeZone($TIMEZONE));
             $date_return = $date->format("h:iA m/d/Y T");
             ?>
             <?php echo $date_return." (GMT ".(date_offset_get($date)/3600).")"; ?>
           </td>
         </tr>

         <?php   foreach ( $days  as $k => $day ) {?>
           <tr>
             <td width="70" style="padding: 5px;"><label><?php echo  $day['name']; ?></label></td>
             <input type="hidden" value="<?php echo  $k; ?>" class="weekday_val" name="weekdays[<?php echo $k?>]" />
             <?php
             $dbOpenTime=substr($day['open_time'],0,5);
             $dbCloseTime=substr($day['close_time'],0,5);

             $dbOpenTimeExploded = explode(":", $dbOpenTime);
             if (!$dbOpenTimeExploded[0]) $dbOpenTimeExploded[0] = 8;
             $open_am_pm = "am";
             if ($dbOpenTimeExploded[0] > 11) {
               $open_am_pm = "pm";
             }
             if ($dbOpenTimeExploded[0] == '00') {
               $dbOpenTimeExploded[0] = 12;
             }

             $dbCloseTimeExploded = explode(":", $dbCloseTime);
             if (!$dbCloseTimeExploded[0]) $dbCloseTimeExploded[0] = 17;
             $closed_am_pm = "am";
             if ($dbCloseTimeExploded[0] > 11) {
               $closed_am_pm = "pm";
             }
             if ($dbCloseTimeExploded[0] == '00') {
               $dbCloseTimeExploded[0] = 12;
             }
           
           ?>
             <td width="200" style="padding: 5px;">
               
               <select class="hour_open_<?php echo $k?>" name="hour_open[<?php echo $k?>]" style="width: 48px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                 <?php for ($i=1; $i<=12; $i++) { 
                   $value = ($i < 10) ? "0".$i : $i;
                   ?> 
                   <option value="<?php echo $value; ?>" <?php if ($dbOpenTimeExploded[0] == $value || $dbOpenTimeExploded[0] == ($value + 12)) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                 <?php } ?>
               </select>

               <select class="minute_open_<?php echo $k?>" name="minute_open[<?php echo $k?>]" style="width: 48px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                 <?php for ($i=0; $i<=59; $i++) { 
                   $value = ($i < 10) ? "0".$i : $i;
                   ?> 
                   <option value="<?php echo $value; ?>" <?php if ($dbOpenTimeExploded[1] == $value) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                 <?php } ?>
               </select>

               <select class="ampm_open_<?php echo $k?>" name="ampm_open[<?php echo $k?>]" style="width: 60px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                   <option value="am">AM</option>
                   <option value="pm" <?php if ($open_am_pm == "pm") { ?>selected="selected"<?php } ?>>PM</option>
               </select>
             </td>

             <td style="padding: 5px;">-</td>
               
             <td width="200" style="padding: 5px;">
               <select class="hour_close_<?php echo $k?>" name="hour_close[<?php echo $k?>]" style="width: 48px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                 <?php for ($i=1; $i<=12; $i++) { 
                   $value = ($i < 10) ? "0".$i : $i;
                   ?> 
                   <option value="<?php echo $value; ?>" <?php if ($dbCloseTimeExploded[0] == $value || $dbCloseTimeExploded[0] == ($value + 12)) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                 <?php } ?>
               </select>

               <select class="minute_close_<?php echo $k?>" name="minute_close[<?php echo $k?>]" style="width: 48px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                 <?php for ($i=0; $i<=59; $i++) { 
                   $value = ($i < 10) ? "0".$i : $i;
                   ?> 
                   <option value="<?php echo $value; ?>" <?php if ($dbCloseTimeExploded[1] == $value) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                 <?php } ?>
               </select>

               <select class="ampm_close_<?php echo $k?>" name="ampm_close[<?php echo $k?>]" style="width: 60px; text-align: center; display: inline-block; border: solid 1px gray; padding: 0px; height: 20px;">
                   <option value="am">AM</option>
                   <option value="pm" <?php if ($closed_am_pm == "pm") { ?>selected="selected"<?php } ?>>PM</option>
               </select>
             </td>
           
             <td><label><input class="chk_<?php echo $k?>" type="checkbox" name="chk[<?php echo $k?>]" <?php if ($day['status']==1) {echo  "checked=checked ";} ?> /> Closed</label></td>

           </tr>

 <?php } ?>
  
   
   </table>
 </form>

    <p>
     <label>
       <input type="button"  class="submit mid"  value="Save" onClick="SaveContent(this,'BusinessHours')" style="margin-left: 413px;">
     </label>
    </p>
    <br/>

    <div style="display: none;">
      <h2>What you want to do next ?</h2>
      <a  href="javascript:void(0)" onClick="showContent(this)"   
       data-flowtype="<?php echo $flowtype ?>" 
       data-pid="<?php echo $pId ?>" 
       data-nid="<?php echo $nId ?>"  id="drop_here_obj"  
       data-wid="<?php echo $wId ?>"   class="drp"><?php echo $blockTitle[$nextflowtype] ?></a>  </div>
    </div>
 <!-- .block_content ends -->
 
 <div class="bendl"></div>
 <div class="bendr"></div>
</div>
<?php }
  
function genHangupBlock($wId='', $pId='', $nId='' , $flowtype='' ,$content_type ='',$content='',$data='', $nextflowtype='Drop here'){}

global $con;


function getRows($wId)
{
  global $con;

    global $blockTitle; 
  if ( $wId)  $data = getMenuItems($wId);

  if ( !empty($data)) 
  {
    $rowCount =1;
    foreach ($data as $k=>$v)
    {
    ?>
<tr class = "row">
  <td><fieldset class="ivr2-input-container">
      <input class="keypress tiny" type="text" autocomplete="off"  maxlength="1" onkeyup="SaveDigit(this)" onkeypress='validate(event)' onblur="checkSimilarKeypresses(this);" data-rownr="<?php echo $rowCount; ?>"
                                  value="<?php echo $v['keypress']  ?>" name="keys[]" id="keypress">
    </fieldset></td>
  <td >&nbsp;</td>
  <td><a  href="javascript:void(0)" onClick="showContent(this)"  
                          data-flowtype="<?php echo $v['flowtype']  ?>" 
                          data-submenu="yes" 
                          data-pid="<?php echo $wId ?>" 
                          data-nid="<?php echo $v['wId'] ?>"  id="drop_here_obj"  
                          data-wid="<?php echo $v['wId'] ?>"   class="drp"><?php 
              
              echo $blockTitle[$v['flowtype']]  ?></a></td>
  <td style="margin-left:10px" align="right"><?php if ( $rowCount ==1) { ?>
    <a class="add action btnAddMore" href="javascript:void(0)" id="btnAddMore" onClick="addMoreRow(this)"><span class="replace">Add</span> </a>
    <?php } else{?>
    <a class="remove action deleteThisRow" href="javascript:void(0)" onclick="deleteRow(this)"> <span class="replace" >Remove</span> </a>
    <?php }
           $rowCount++;
          ?></td>
</tr>
<?php   }
  }else{ ?>
<tr class = "row">
  <td><fieldset class="ivr2-input-container">
      <input class="keypress tiny" type="text" autocomplete="off" 
                                  value="1" name="keys[]" id="keypress" onblur="checkSimilarKeypresses(this);" data-rownr="1" />
    </fieldset></td>
  <td >&nbsp;</td>
  <td><a  href="javascript:void(0)" onClick="showContent(this)" 
                          data-flowtype="empty" 
                          data-submenu="yes" data-pid="<?php echo  $wId ?>"
                          data-nid=""
                          id="drop_here_obj"  
                          data-wid=""   class="drp">Drop here</a></td>
  <td style="margin-left:10px" align="right"><a class="add action btnAddMore" href="javascript:void(0)" id="btnAddMore" onClick="addMoreRow(this)"><span class="replace">Add</span> </a></td>
</tr>
<?php } 
  return $data;
}
  function getMenuItems($pId)
  {
    global $con;

    
    $data= array();
    $sql_dt = "SELECT * FROM call_ivr_widget WHERE keypress != '-' and pId = $pId";
    $res = mysqli_query($con, $sql_dt);
    if (mysqli_num_rows($res) >0 )
    {   
      while ( $row = mysqli_fetch_assoc($res))
      {
        $data[$row['wId']]  =$row;
      }
    }
    
    return $data;
  }

function getDetail($wId)
  {
    global $con;

    
    $row= array();
    $sql_dt = "SELECT * FROM call_ivr_widget WHERE wId= $wId";
    $res = mysqli_query($con, $sql_dt);
    if (mysqli_num_rows($res) >0 )
    {   
      $row = mysqli_fetch_assoc($res);
    }
    
    return $row;
  }

  function getMetaDetail($wId, $meta_key)
  {
    global $con;


    if (empty($wId)) {
      return "";
    }
    
    $row= array();
    $sql_dt = "SELECT * FROM call_ivr_data WHERE wId= $wId AND meta_key = '".$meta_key."'";
    $res = mysqli_query($con, $sql_dt);
    if (mysqli_num_rows($res) >0 )
    {   
      $row = mysqli_fetch_assoc($res);
      return $row['meta_data'];
    }
    else {
      return "";
    }
  }

	function getStartId($cId)
	{
    global $con;

		$wId ='';
		$flowtype ='Drop here';
		$sql_dt = "SELECT * FROM call_ivr_widget WHERE companyId = $cId AND pId = '0' AND temporary = '1'";

		$res = mysqli_query($con, $sql_dt)or die(mysqli_error($con));
		
			if (mysqli_num_rows($res) >0 )
			{
				$rs = mysqli_fetch_array($res);
				$wId = $rs['wId'];
				$flowtype = $rs['flowtype'];
			}
		
		return array ( 'wId' =>$wId ,'flowtype'=> $flowtype);
	}
	
	function getWidget($wId, $keypress)
	{
    global $con;

		
		$sql_dt = "SELECT * FROM call_ivr_widget WHERE wId = '$wId' and keypress ='$keypress'";

		$res = mysqli_query($con, $sql_dt)or die(mysqli_error($con));
		
		if (mysqli_num_rows($res) >0 )
		{ 
			$widget = mysqli_fetch_assoc($res);
			
			$nextWidget = getNextWidget($wId);
			
			$widget['nextflowtype'] = $nextWidget['flowtype'];
			$widget['nId'] 			= $nextWidget['wId'];

      if ($widget['flowtype'] == "Agent") $widget['flowtype'] = "SingleAgent";
			$fData =call_user_func('gen'.$widget['flowtype'].'Block'  ,
					$widget['wId'], $widget['pId'], $widget['nId'], $widget['flowtype'] ,
					$widget['content_type'] ,$widget['content'],$widget['data'], $widget['nextflowtype']);

      if ($widget['flowtype'] == "Dial" || $widget['flowtype'] == "BusinessHours") {
        //Check Voicemail for Open number
        $cvw_details = mysqli_fetch_array(mysqli_query($con, "SELECT wId FROM call_ivr_widget WHERE pId = '".$widget['wId']."' AND data = 'open'"));
        if (!empty($cvw_details['wId'])) {
          getWidget($cvw_details['wId'],'');
        }

        //Check Voicemail for Closed number
        $cvw_details = mysqli_fetch_array(mysqli_query($con, "SELECT wId FROM call_ivr_widget WHERE pId = '".$widget['wId']."' AND data = 'closed'"));
        if (!empty($cvw_details['wId'])) {
          getWidget($cvw_details['wId'],'');
        }
      }
			
			
			if ( !empty($fData) ){
				foreach ( $fData as $k=>$v){ 

				getWidget($v['wId'],$v['keypress']);
				
				}
			}
			
			if ( $widget['nId'] != '0') getWidget($widget['nId'],'-');
			
		}
			
					/*$widget['wId'] = $rs_nxt['wId'];
					$widget['pId'] = $rs_nxt['pId'];
					$widget['flowtype'] = $rs_nxt['flowtype'];
					$widget['content_type'] = $rs_nxt['content_type'];
					$widget['content'] = $rs_nxt['content'];
					$widget['nId'] = $rs_nxt['nId'];
					$widget['data'] = $rs_nxt['data']
					$widget['keypress'] = $rs_nxt['keypress'];*/	
			
	}
	
	function getNextWidget($wId)
	{
    global $con;

		$sql_dt = "SELECT * FROM call_ivr_widget WHERE pId = '$wId' and keypress ='-'";
		$res = mysqli_query($con, $sql_dt);
		if (mysqli_num_rows($res) >0 )
		{ 	$rs = mysqli_fetch_assoc($res);
			return array('wId'=>$rs['wId'],'flowtype'=> $rs['flowtype']);
		}else{
			return array('wId'=>0,'flowtype'=> 'Drop here');

		}
	}
	
	
	
	

	
	function get_data_simple($cId)
	{
    global $con;

		
		$sql_dt = "SELECT * FROM call_ivr_widget WHERE companyId = $cId AND pId = '0'";
		$res = mysqli_query($con, $sql_dt)or die(mysqli_error($con));
		if (mysqli_num_rows($res) >0 ) $rs = mysqli_fetch_array($res);
		$wId = $rs['wId'];
		$nId = $rs['nId'];
		
		
		
		if ($wId)
		do {
			$sql_next = "SELECT * FROM call_ivr_widget WHERE wId = $wId";
			$res_nxt = mysqli_query($con, $sql_next);
				
				$nId=0;
				if (mysqli_num_rows($res_nxt) >0 ) 
				{
					$rs_nxt = mysqli_fetch_array($res_nxt);
					$nId = $rs_nxt['nId'];
				
					$callDat[$wId]['wId'] = $rs_nxt['wId'];
					$callDat[$wId]['pId'] = $rs_nxt['pId'];
					$callDat[$wId]['flowtype'] = $rs_nxt['flowtype'];
					$callDat[$wId]['content_type'] = $rs_nxt['content_type'];
					$callDat[$wId]['content'] = $rs_nxt['content'];
					$callDat[$wId]['nId'] = $rs_nxt['nId'];
					$callDat[$wId]['data'] = $rs_nxt['data'];
					$wId = $nId;
				}
			}while($nId !=0 );
			
			// echo "<pre>";			print_r();			echo "</pre>";
		
		return $callDat;
		
		}
	
	

  ?>
