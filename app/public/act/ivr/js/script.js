;(function() {
	
	window.jsPlumbDemo = {
			
		init : function() {			

			

			jsPlumb.importDefaults({
				// notice the 'curviness' argument to this Bezier curve.  the curves on this page are far smoother
				// than the curves on the first demo, which use the default curviness value.			
				Connector : [ "Bezier", { curviness:50 } ],
				DragOptions : { cursor: "pointer", zIndex:2000 },
				PaintStyle : { strokeStyle:color, lineWidth:2 },
				EndpointStyle : { radius:9, fillStyle:color },
				HoverPaintStyle : {strokeStyle:"#ec9f2e" },
				EndpointHoverStyle : {fillStyle:"#ec9f2e" },			
				Anchors :  [ "BottomLeft", "TopCenter" ]
			});
			
				
			
		
/*			jsPlumb.connect({source:"window3", target:"window6", overlays:overlays, detachable:true, reattach:true});
			jsPlumb.connect({source:"window1", target:"window2", overlays:overlays});
			jsPlumb.connect({source:"window1", target:"window3", overlays:overlays});
			jsPlumb.connect({source:"window2", target:"window4", overlays:overlays});
			
			
			// you do not need to use this method. You can use your library's selector method.
			// the jsPlumb demos use it so that the code can be shared between all three libraries.
			jsPlumb.draggable(jsPlumb.getSelector(".blockRight"));
			jsPlumb.draggable(jsPlumb.getSelector(".blockLeft"));*/
		}
	};
	
})();

	var color = "gray";	
	var arrowCommon = { foldback:0.7, fillStyle:color, width:14 },
				// use three-arg spec to create two different arrows with the common values:
				overlays = [
					[ "Arrow", { location:0.2, direction:1}, arrowCommon ],
					[ "Arrow", { location:0.7, direction:1 }, arrowCommon ]
				];		
$(document).ready(function()
{
	// declare some common values:
			
	jsPlumbDemo.init();
	
});


function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

var myInterval = null;

var ShowConnection = false;
function showFlow(auto)
{
	if (typeof(auto) == "undefined") {
		clearInterval(myInterval);
		myInterval = null;
	}

	var ShowConnectionx = ShowConnection;

	if ( ShowConnection ) {
		RemoveShowFlow()
		
	}else{
		$('#blockLeft .drp').each(function( index, element ) 
		{
			var el = $(this);
			if (!el.attr('data-flow-shown')) {
				el.attr('data-flow-shown', 'yes');
				if (
					($(element).data('nid') != '' && $(element).html() != 'Hangup' && $(element).data('drp-location') != 'opt-hours')
					||
					($(element).data('wid') != '' && $(element).html() != 'Hangup' && $(element).data('drp-location') == 'opt-hours')
					)
				{
					var strAchor ='<a href="#" class="startAnchor" id="startAchor-" >&nbsp;</a>';

					if ($(element).data('drp-location') == 'opt-hours') {
						strAchor= strAchor.replace(/startAchor-/,'startAchor-'+($(element).data('wid') + '-' + $(element).data('number')));
						$(element).after(strAchor);
						var color =get_random_color();

						var target_element = $('.block[data-data="' + $(element).data('number') + '"]');
						if (target_element.length > 0) {
							var source = 'startAchor-'+($(element).data('nid') || ($(element).data('wid') + '-' + $(element).data('number')))
							jsPlumb.connect({source: source, target: target_element.attr('id'), 
							endpointStyle:{ fillStyle:color },
							paintStyle:{ strokeStyle:color, lineWidth:2 },
							overlays:overlays});
							ShowConnection= true;
						}
					}
					else {
						strAchor= strAchor.replace(/startAchor-/,'startAchor-'+$(element).data('nid'));
						$(element).after(strAchor);
						var color =get_random_color();

						if ($("#blockObj-"+$(element).data('nid')).length > 0) {
							jsPlumb.connect({source:'startAchor-'+$(element).data('nid') , target:"blockObj-"+$(element).data('nid'), 
							endpointStyle:{ fillStyle:color },
							paintStyle:{ strokeStyle:color, lineWidth:2 },
							overlays:overlays});
							ShowConnection= true;
						}
					}
				}
			}
		});
	
		if ( ShowConnectionx== false  && ShowConnection== false )
		{
			parent.infoMsgDialog("Please drag few items");
		}
		else {
			if (myInterval == null) {
				myInterval = setInterval(function() {
					jsPlumb.repaintEverything();
				}, 100);
			}
		}
	}

	$("path, svg").each(function() {
		$(this).css('z-index', '9999 !important');
	});
}

function RemoveShowFlow(auto)
{
	if (typeof(auto) == "undefined") {
		clearInterval(myInterval);
		myInterval = null;
	}

	$('#blockLeft .drp').each(function( index, element ) 
	{
		var el = $(this);
		el.removeAttr('data-flow-shown');
	});

	if ( ShowConnection ) {
		
		ShowConnection= false;
		try {
			jsPlumb.detachEveryConnection();		
		}
		catch(e) {

		}
		$('.startAnchor').each(function( index, element ) 
		{$(element).remove();});
	}
}

	function showContent(obj){		
		//alert('  flowtype = '+$(obj).data('flowtype') + '  next Id = '+$(obj).data('nid') + '   my Id = '+$(obj).data('wid') + ' 		 parent Id = '+$(obj).data('pid'));
	}

    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }

    function validateSendDigits(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /[0-9w]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }