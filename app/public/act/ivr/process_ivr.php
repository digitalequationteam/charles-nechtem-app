<?php


	include("../include/config.php");
	include("../include/util.php");

header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() - 86400));

$con = mysqli_connect($DB_HOST,$DB_USER,$DB_PASS);
mysqli_select_db($con, $DB_NAME );

$wId = $_REQUEST ['wId'];
$pId = $_REQUEST['pId'];
$nId = $_REQUEST['nId'];
$companyId = $_REQUEST['companyId'];
$flowtype = $_REQUEST['flowtype'];
$act = $_REQUEST['act'];


if (  $act == 'save' )
{
	$previousId =  '';
	if ( $wId !='' )
	{
		if (!isset($_REQUEST['agent-applet']))
			$previousId  = ", pId  = '$wId' ";
	}
	
	$keypress='';
	
	if ( isset($_REQUEST['submenu']) )
	{
		$keypress = ', keypress="'.$_REQUEST['keypress'].'" ';
		$previousId  = ", pId  = '$pId' ";
	}

	if (isset($_REQUEST['replace'])) {
		$qry =  "UPDATE call_ivr_widget SET flowtype='$flowtype', content_type = '', content = '' WHERE wId = $wId"; 
		mysqli_query($con, $qry) or die(mysqli_error($con));
	}
	else {
		$number = '';
		if ($_REQUEST['voicemail_only'] == "yes" || $_REQUEST['business_hours_only'] == "yes") {
			$pId = $wId;
			$previousId = ", pId  = '$wId' ";
			$number = (isset($_REQUEST['number']) ? $_REQUEST['number'] : "");
			$keypress = ', keypress="" ';
		}

		if ($_REQUEST['voicemail_only'] == "yes" && isset($_REQUEST['data_agent_ivr_voicemail']))
			$meta_data = ", meta_data  = 'agent_ivr_master_voicemail' ";

		$qry =  "INSERT INTO call_ivr_widget SET companyId=$companyId, flowtype='$flowtype', data = '".$number."', temporary = 1 $keypress $previousId $meta_data";

		mysqli_query($con, $qry) or die(mysqli_error($con));
		$widgetId = mysqli_insert_id($con);
		if ( $wId != '' && $keypress=='')
		{
			mysqli_query($con, " update call_ivr_widget set nId = $widgetId  where wId = $wId" ) or die(mysqli_error($con));
		}
	}

	$post_data = array('wId' => $widgetId,	'pId' => $wId);		

	if ( isset($_REQUEST['submenu']) || isset($_REQUEST['agent-applet']) ) $post_data = array('wId' => $widgetId,	'pId' => $pId);		
	
	if (isset($_REQUEST['agent-applet'])) {
		mysqli_query($con, "DELETE FROM call_ivr_widget WHERE wid = '".$widgetId."'") or die(mysqli_error($con));
	}
	$data = json_encode((object)array('data' => $post_data));
	
	print_r($data);

}



if (  $act == 'saveRepeat' )
{
	if ( isset($_REQUEST['digit']) && is_numeric($_REQUEST['digit']) )
	{
		$repeat = ' meta_data ="'.$_REQUEST['digit'].'" ';
		
	
		$qry = " update call_ivr_widget set $repeat  where wId = $wId" ;

		if(  mysqli_query($con,  $qry ))
		{
			echo '1';
		}else{
			echo '0';
		}
	}
}

if (  $act == 'saveDelay' )
{
	$digit = addslashes( $_REQUEST['digit'] );

	mysqli_query($con, "REPLACE INTO call_ivr_data(wId, meta_key, meta_data) VALUES('".$wId."', 'menu_delay', '".$digit."')") or die(mysqli_error($con));
}

if (  $act == 'saveDigit' )
{
	if ( isset($_REQUEST['digit']) && is_numeric($_REQUEST['digit']) )
	{
		$keypress = ' keypress="'.$_REQUEST['digit'].'" ';
		
	
		$qry = " update call_ivr_widget set $keypress  where wId = $wId" ;

		if(  mysqli_query($con,  $qry ))
		{
			echo '1';
		}else{
			echo '0';
		}
	}
}
if (  $act == 'DeleteMultiNumber' )
{
	$idx = addslashes( $_REQUEST['idx'] );
	

	if(  mysqli_query($con, " delete from  call_ivr_multiple_numbers   where idx = $idx" 	))
	{
		echo '1';
		
	}else{
		echo '0';
	}
}
if (  $act == 'DeleteRoundRobinNumber' )
{
	$idx = addslashes( $_REQUEST['idx'] );
	

	if(  mysqli_query($con, " delete from  call_ivr_round_robin   where idx = $idx" 	))
	{
		echo '1';
		
	}else{
		echo '0';
	}
}


if (  $act == 'MultiNumberInternational' )
{
	
	$international_number = addslashes( $_REQUEST['international_number'] );

	if(  mysqli_query($con, " update call_ivr_widget set content_type='MultiNumber', content ='$international_number'  where wId = $wId" 	))
	{
		
		echo '1';
		
	}else{
		echo '0';
	}
}

if (  $act == 'MultiNumber' )
{
	$PhoneNumber = addslashes( $_REQUEST['PhoneNumber'] );
	$international_number = addslashes( $_REQUEST['international_number'] );

	if(  mysqli_query($con, " update call_ivr_widget set content_type='MultiNumber', content ='$international_number'  where wId = $wId" 	))
	{
		mysqli_query($con, " insert into  call_ivr_multiple_numbers  set  wId = $wId, number ='$PhoneNumber'  ") or die(mysqli_error($con));
		$idx = mysqli_insert_id($con);
		echo $idx;
		
	}else{
		echo '0';
	}
}
if (  $act == 'RoundRobinNumber' )
{
	$PhoneNumber = addslashes( $_REQUEST['PhoneNumber'] );
	
	$forward_number = addslashes( $_REQUEST['forward_number'] );
	$forward_sec = addslashes( $_REQUEST['forward_sec'] );
	
	$international_number = addslashes( $_REQUEST['international_number'] );
	
	if(  mysqli_query($con, " update call_ivr_widget set content_type='RoundRobin', content ='$international_number', data='$forward_number', meta_data='$forward_sec' where wId = $wId" 	))
	{
		mysqli_query($con, " insert into  call_ivr_round_robin  set  wId = $wId, number ='$PhoneNumber'  ") or die(mysqli_error($con));
		$idx = mysqli_insert_id($con);
		
		echo $idx;
		
	}else{
		echo '0';
	}
}
if (  $act == 'SaveRoundRobin' )
{
	$forward_number = addslashes( $_REQUEST['forward_number'] );
	$forward_sec = addslashes( $_REQUEST['forward_sec'] );
	
	$international_number = addslashes( $_REQUEST['international_number'] );

	if(  mysqli_query($con, "update call_ivr_widget set content_type='RoundRobin', content ='$international_number' or die(mysqli_error($con)) 
	,data='$forward_number', meta_data='$forward_sec'	 where wId = $wId" 	))
	{
		
		
		echo '1';
		
	}else{
		echo '0';
	}
}

if (  $act == 'VoicemailTranscript' )
{
	$send_transcript = addslashes( $_REQUEST['send_transcript'] );
	$TranscriptEmail = addslashes( $_REQUEST['TranscriptEmail'] );

	mysqli_query($con, "REPLACE INTO call_ivr_data(wId, meta_key, meta_data) VALUES('".$wId."', 'send_transcript', '".$send_transcript."')") or die(mysqli_error($con));
	mysqli_query($con, "REPLACE INTO call_ivr_data(wId, meta_key, meta_data) VALUES('".$wId."', 'TranscriptEmail', '".$TranscriptEmail."')") or die(mysqli_error($con));
}

if (  $act == 'VoicemailRingCount' )
{
	$ring_count = addslashes( $_REQUEST['ring_count'] );

	mysqli_query($con, "REPLACE INTO call_ivr_data(wId, meta_key, meta_data) VALUES('".$wId."', 'ring_count', '".$ring_count."')") or die(mysqli_error($con));
}


if (  $act == 'SMS' )
{
	$sms_content = addslashes( $_REQUEST['sms_content'] );

	if(  mysqli_query($con, " update call_ivr_widget set content_type='SMS', content ='$sms_content'  where wId = $wId")
		&&
		mysqli_query($con, "REPLACE INTO call_ivr_data(wId, meta_key, meta_data) VALUES('".$wId."', 'sms_delay', '".addslashes( $_REQUEST['sms_delay'] )."')")
	)
	{
		echo '1';
	}else{
		echo '0';
	}
}


if (  $act == 'Dail' )
{
	$all_data = $_POST['all_data'];
	$all_data['call_hours'] = array();

	foreach ($_POST['call_hours'] as $k => $call_hour) {
		$open = date("H:i:s", strtotime(date("Y-m-d")." ".$call_hour['hour_open'].":".$call_hour['minute_open'].":00 ".$call_hour['ampm_open']));
		$close = date("H:i:s", strtotime(date("Y-m-d")." ".$call_hour['hour_close'].":".$call_hour['minute_close'].":00 ".$call_hour['ampm_close']));
		$chk = $call_hour['chk'];

		$all_data['call_hours'][$k] = array(
			"day" => $call_hour['name'],
			"open_time" => $open,
			"close_time" => $close,
			"status" => $chk
		);
	}
	
	$data = " , data = '$dail_inter'";
	
	if(  mysqli_query($con, " update call_ivr_widget set content_type='Dail', content ='".json_encode($all_data)."',meta_data='$timeOut' $data  where wId = $wId")
         &&
         mysqli_query($con, "REPLACE INTO call_ivr_data(wId, meta_key, meta_data) VALUES('".$wId."','send_digits','".$sendDigits."')")
    )
	{
		echo '1';
	}else{
		echo '0';
	}
}

if (  $act == 'BusinessHours' )
{
	$all_data = array();
	$all_data['call_hours'] = array();

	foreach ($_POST['call_hours'] as $k => $call_hour) {
		$open = date("H:i:s", strtotime(date("Y-m-d")." ".$call_hour['hour_open'].":".$call_hour['minute_open'].":00 ".$call_hour['ampm_open']));
		$close = date("H:i:s", strtotime(date("Y-m-d")." ".$call_hour['hour_close'].":".$call_hour['minute_close'].":00 ".$call_hour['ampm_close']));
		$chk = $call_hour['chk'];

		$all_data['call_hours'][$k] = array(
			"day" => $call_hour['name'],
			"open_time" => $open,
			"close_time" => $close,
			"status" => $chk
		);
	}
	
	if(  mysqli_query($con, " update call_ivr_widget set content_type='BusinessHours', content ='".json_encode($all_data)."'  where wId = $wId" ))
	{
		echo '1';
	}else{
		echo '0';
	}
}

if (  $act == 'Text_mail' )
{
	$text_content = addslashes( $_REQUEST['text_content'] );
	
	if (
	  	mysqli_query($con, " update call_ivr_widget set content_type='Text', content ='$text_content'  where wId = $wId")
		&&
		mysqli_query($con, "REPLACE INTO call_ivr_data(wId, meta_key, meta_data) VALUES('".$wId."', 'language', '".addslashes( $_REQUEST['language'] )."')")
		&&
		mysqli_query($con, "REPLACE INTO call_ivr_data(wId, meta_key, meta_data) VALUES('".$wId."', 'voice', '".addslashes( $_REQUEST['voice'] )."')")
	)
	{
		echo '1';
	}else{
		echo '0';
	}
}

if (  $act == 'EMPTY_WIDGET' )
{
	$text_content = addslashes( $_REQUEST['text_content'] );
	
	if (
	  	mysqli_query($con, " update call_ivr_widget set content_type='', content =''  where wId = $wId")
	)
	{
		echo '1';
	}else{
		echo '0';
	}
}

if (  $act == 'OptIn' )
{
	require_once '../include/ad_auto_dialer_files/lib/ad_lib_funcs.php';
	$list_name = addslashes( $_REQUEST['list_name'] );

	if (empty($list_name))
		die('0');

	ad_advb_cl_add_cl($list_name);

	if(mysqli_query($con, " update call_ivr_widget set content_type='OptIn', content ='$list_name'  where wId = $wId" ))
	{
		echo '1';
	}else{
		echo '0';
	}
}


if (  $act == 'Text_greet' )
{
	$text_content = addslashes( $_REQUEST['text_content'] );
	
	if(  mysqli_query($con, " update call_ivr_widget set content_type='Text', content ='$text_content'  where wId = $wId" ))
	{
		echo '1';
	}else{
		echo '0';
	}
}


if (  $act == 'audioUpdate' )
{	
	$uploaddir = '../audio/'.$companyId.'/';
	
	@mkdir ('../audio/'.$companyId.'/',0777, true);
	@chmod ('../audio/'.$companyId.'/',0777);
	
	@mkdir ($uploaddir,0777, true);
	@chmod($uploaddir,0777);
	
	$file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name'])); 
	 
	if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
	{ 
		$filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
		
		mysqli_query($con, " update call_ivr_widget set content_type='Audio', content ='$filename'  where wId = $wId" ) or die(mysqli_error($con));
	
	  	echo "Voice to play: <strong>$filename</strong>"; 
	
	} else {
		echo "error";
	}

}

if (  $act == 'uploadFile' )
{	
	$uploaddir = '../audio/'.$companyId.'/';
	
	@mkdir ('../audio/'.$companyId.'/',0777, true);
	@chmod ('../audio/'.$companyId.'/',0777);
	
	@mkdir ($uploaddir,0777, true);
	@chmod($uploaddir,0777);
	
	$file = $uploaddir . str_replace(" ", "_", basename($_FILES['uploadfile']['name'])); 
	 
	if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
	{ 
		$filename = str_replace(" ", "_", basename($_FILES['uploadfile']['name']));
	
	  	echo $filename;
	
	} else {
		echo "error";
	}
	exit();
}


if (  $act == 'MP3_URL' )
{	
	$mp3_url = addslashes( $_REQUEST['mp3_url'] );
	
	if (
	  	mysqli_query($con, " update call_ivr_widget set content_type='MP3_URL', content ='$mp3_url'  where wId = $wId")
	  	)
	{
		echo '1';
	}else{
		echo '0';
	}
}


if (  $act == 'REMOVE' )
{


	mysqli_query($con, " delete  from call_ivr_widget  where wId = $wId" ) or die(mysqli_error($con));
	mysqli_query($con, " delete  from call_ivr_data  where wId = $wId" ) or die(mysqli_error($con));
	mysqli_query($con, " delete  from call_ivr_multiple_numbers  where wId = $wId" ) or die(mysqli_error($con));
	mysqli_query($con, " delete  from call_ivr_round_robin  where wId = $wId" ) or die(mysqli_error($con));
	
	
	if ( $nId != '' )
	{
		if ( $pId != '' ) 
		{
			mysqli_query($con, " update    call_ivr_widget set nId =$nId   where wId = $pId" ) or die(mysqli_error($con));
			mysqli_query($con, " update    call_ivr_widget set pId =$pId   where wId = $nId" ) or die(mysqli_error($con));
			
		}else{
				mysqli_query($con, " update    call_ivr_widget set pId=0   where wId = $nId" ) or die(mysqli_error($con));
		}
	}else{
		if ( $pId != '' ) 
		{
			mysqli_query($con, " update    call_ivr_widget set nId = 0   where wId = $pId" ) or die(mysqli_error($con));
		}
	}

}

if (  $act == 'REMOVE-Hangup' )
{
	if (empty($wId)) $wId = 0;
	
	mysqli_query($con, " delete  from call_ivr_widget  where (wId = $wId) OR (companyId = '".$companyId."' AND flowtype = 'Hangup')" ) or die(mysqli_error($con));
	
}


if (  $act == 'saveCallFlow' )
{
	$company_id = addslashes($_REQUEST['company_id']);
	
	mysqli_query($con, "DELETE FROM call_ivr_widget WHERE temporary = 0 AND companyId = '".$company_id."'") or die(mysqli_error($con));
	mysqli_query($con, "UPDATE call_ivr_widget SET temporary = 0 WHERE temporary = 1 AND companyId = '".$company_id."'") or die(mysqli_error($con));

	$unsavedChanges = json_decode(Util::escapeString($_POST['unsavedChanges']), true);

	$advanced_och = json_decode(Util::escapeString($_POST['advanced_och']), true);

	if (count($unsavedChanges) > 0) {
		foreach ($unsavedChanges as $unsavedChange) {
			mysqli_query($con, "UPDATE call_ivr_widget SET content = '".json_encode($unsavedChange)."' WHERE wId = ".$unsavedChange['wId']) or die(mysqli_error($con));
		}
	}

	foreach ($advanced_och['weekdays'] as $key => $weekday) {
		$open = date("H:i:s", strtotime(date("Y-m-d")." ".$weekday['hour_open'].":".$weekday['minute_open'].":00 ".$weekday['ampm_open']));
        $close = date("H:i:s", strtotime(date("Y-m-d")." ".$weekday['hour_close'].":".$weekday['minute_close'].":00 ".$weekday['ampm_close']));

        $advanced_och['weekdays'][$key] = array(
        	"open_time" => $open,
        	"close_time" => $close,
        	"status" => $weekday['closed']
        );
	}

	mysqli_query($con, "UPDATE companies SET advanced_och = '".json_encode($advanced_och)."' WHERE idx = ".$company_id) or die(mysqli_error($con));
}

if ($act == 'GET_RECORDED_AUDIO') {
	$sql_dt = "SELECT content FROM call_ivr_widget WHERE wId = '$wId'";
	$widget_data = mysqli_fetch_array(mysqli_query($con, $sql_dt)) or die(mysqli_error($con));

	$content = $widget_data['content'];

    $content = Util::generateFlashAudioPlayer($content,"sm");

    echo $content;
}
?>