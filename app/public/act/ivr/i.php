<?php


	//Check for config..
	if (!file_exists("../include/config.php")) {
		die("<b>There is an error. Config file not found. Please re-install or contact support.</b>");
	}
	session_start();
	require_once('../include/util.php');
	
	
	$page = "adminpage";
	$db = new DB();	
	//Database settings
	$con = mysql_connect($DB_HOST,$DB_USER,$DB_PASS);
	mysql_select_db($DB_NAME );


	//Pre-load Checks
	if (!isset($_SESSION['user_id'])) {		header("Location: ../login.php");		exit;	}
	$cId='0';	
	require_once('ivrblock.php');
	if (isset($_GET['cId']))
	{
		$cId=$_GET['cId'];			
	}

	
	
	
?>
<!doctype html>
<html lang="us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>IVR -  Customized</title>
<link href="css/jqcss/jquery-ui-1.10.3.custom.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="../css/ivr2.css" />
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui-1.10.3.custom.js"></script>
<!--<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>
-->
<script type="text/javascript" >
	/*$(function(){
		var btnUpload = $('.upload');
		var status 	  = $('#status');
		new AjaxUpload(btnUpload, {
			action: 'process_ivr.php?act=voice_mail',
			name: 'uploadfile',
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('Only JPG, PNG or GIF files are allowed');
					return false;
				}
				status.text('Uploading...');
			},
			onComplete: function(file, response){
				//On completion clear the status
				status.text('done');
				//Add uploaded file to list
				if(response=="success"){
					
					//alert ("image stored");
					//$('<li></li>').appendTo('#files').html('<img src="./uploads/'+file+'" alt="" /><br />'+file).addClass('success');
				} else{
					//$('<li></li>').appendTo('#files').text(file).addClass('error');
				}
			}
		});
		
	});*/
</script>
<style  type="text/css">
body {
	text-align: left !important
}
.blockRight {
	width: 200px;
	float: right;
	text-align: center;
}
#flotingDiv {
	position: relative;
}
.blockLeft {
	width: 600px;
	float: left;
}
.drg {
	-moz-box-shadow: 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: 0px 1px 0px 0px #ffffff;
	box-shadow: 0px 1px 0px 0px #ffffff;
	background-color: transparent;
	-webkit-border-top-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	border-top-left-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-bottom-left-radius: 10px;
	text-indent: 0px;
	border: 2px solid #dcdcdc;
	display: inline-block;
	color: #777777;
	font-family: Arial;
	font-size: 17px;
	font-weight: bold;
	font-style: normal;
	height: 50px;
	line-height: 50px;
	width: 120px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	margin: 5px;
}
.drg:active {
	position: relative;/* top: 1px; */
}
.drp {
	-moz-box-shadow: 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: 0px 1px 0px 0px #ffffff;
	box-shadow: 0px 1px 0px 0px #ffffff;
	background-color: transparent;
	-webkit-border-top-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	border-top-left-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-bottom-left-radius: 10px;
	text-indent: 0px;
	border: 2px solid #dcdcdc;
	display: inline-block;
	color: #777777;
	font-family: Arial;
	font-size: 17px;
	font-weight: bold;
	font-style: normal;
	height: 50px;
	line-height: 50px;
	width: 120px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	margin: 5px;
}
.drp2 {
	-moz-box-shadow: 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: 0px 1px 0px 0px #ffffff;
	box-shadow: 0px 1px 0px 0px #ffffff;
	background-color: transparent;
	-webkit-border-top-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	border-top-left-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-bottom-left-radius: 10px;
	text-indent: 0px;
	border: 2px solid #dcdcdc;
	display: inline-block;
	color: #00F;
	font-family: Arial;
	font-size: 17px;
	font-weight: bold;
	font-style: normal;
	height: 50px;
	line-height: 50px;
	width: 120px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	margin: 5px;
}
.droping {
	border: 2px solid #666666 !important;
	color: #333;
}
</style>
</head>
<body>
<div id="hld">
  <div class="wrapper">
    <div class="block" style="width:850px;">
      <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>
        <h2> Customize your IVR call flow </h2>
      </div>
      <!-- .block_head ends -->
      
      <div class="block_content">
        <div
        	class="message success" id="flow_msg_parent" style="width: 93%; margin: 0px 0px 11px; display:none"> </div>
        <?php /*?><div class="message warning" style="width: 93%;margin: 0 auto;margin-top: 8px;"><p>Please select a company to view.</p></div><?php */?>
        <span>Lets Start with Greeting.</span><br/>
        <br/>
        <div class="blockLeft" id="blockLeft">
          <div class="block" id="blockMain">
            <div class="block_head">
              <div class="bheadl"></div>
              <div class="bheadr"></div>
              <h2>Your Call Flow </h2>
              <div style="display:inline-block; float: right;">
                <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="showFlow()">Show Flow</a></h2>
              </div>
            </div>
            <!-- .block_head ends -->
            
            <div class="block_content"> <span>Drag any option to call flow.</span><br/>
              <br/>
              <?php $mainBlock = getStartId($cId); $wId = $mainBlock['wId'];  ?>
              <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="empty" data-pid="" data-nid=""  id="drop_here_obj"  data-wid=""   class="drp"><?php echo  $mainBlock['flowtype'] ?></a> 
              
              <!-- .block_content ends --> 
            </div>
            <div class="bendl"></div>
            <div class="bendr"></div>
          </div>
          <?php  /////////////Sub Blocks Starts //////// 
          	
		  	
		  	getWidget($wId,'-');
		  
           /////////////Sub Blocks Ends //////// ?>
        </div>
        <div class="blockRight" id="blockRight">
          <div class="block" id="flotingDiv">
            <div class="block_head">
              <div class="bheadl"></div>
              <div class="bheadr"></div>
              <h2>MENU OPTIONS </h2>
            </div>
            <!-- .block_head ends -->
            
            <div class="block_content">
              <?php /*?><div class="message warning" style="width: 93%;margin: 0 auto;margin-top: 8px;"><p>Please select a company to view.</p></div><?php */?>
              <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="Greetings"  class="drg" >Greetings</a> <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="Menu"  class="drg" >Menu</a> <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="Dial"  class="drg" >Dial</a> <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="SMS"  class="drg" >SMS</a> <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="Voicemail"  class="drg" >Voicemail</a> <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="Hangup"  class="drg" >Hangup</a> </div>
            <!-- .block_content ends -->
            
            <div class="bendl"></div>
            <div class="bendr"></div>
          </div>
        </div>
      </div>
      <!-- .block_content ends -->
      
      <div class="bendl"></div>
      <div class="bendr"></div>
    </div>
  </div>
</div>
<script type="text/javascript" >

	var companyId = '33';
	var drpOptions ='';
	
	function showContent(obj){		
		alert('  flowtype = '+$(obj).data('flowtype') + '  next Id = '+$(obj).data('nid') + '   my Id = '+$(obj).data('wid') + '  parent Id = '+$(obj).data('pid'));
	}
var ShowConnection = false;
function showFlow()
{
	var ShowConnectionx = ShowConnection;
	
	if ( ShowConnection ) {
		RemoveShowFlow()
		
	}else{
		$('.drp').each(function( index, element ) 
		{
			
			
			if (  $(element).data('nid') != '' && $(element).html() != 'Hangup' )
			{
				var strAchor ='<a href="#" class="startAnchor" id="startAchor-" >-</a>';
				strAchor= strAchor.replace(/startAchor-/,'startAchor-'+$(element).data('nid') );
				$(element).after(strAchor);
				
				jsPlumb.connect({source:'startAchor-'+$(element).data('nid') , target:"blockObj-"+$(element).data('nid'), overlays:overlays});	
				ShowConnection= true;
			}

		});
	}
	
	if ( ShowConnectionx== false  && ShowConnection== false )
	{
		alert("please drag few items");
	}
}

function RemoveShowFlow()
{
	if ( ShowConnection ) {
		
		ShowConnection= false;
		jsPlumb.detachEveryConnection();		
		$('.startAnchor').each(function( index, element ) 
		{$(element).remove();});
	}
}

					
$(document).ready(function()
{
	///////////// hide show audio choices ////////////////
	$(".ivr-Menu-editor").hide();
	$(".ivr-audio-upload").hide();
	$(".ivr-Menu-read-text").hide();
	///////////// hide show audio choices ////////////////
		

	/////////////////// drag options ////////
    $( ".drg" ).draggable({ 
							scope: "flow_opts",						
							helper: 'clone',
							appendTo: 'body',
							cursor: "move",
							revert: true });
    
	/////////////////// drop options ////////
	 drpOptions = {
		scope: "flow_opts",
		activeClass : "droping",
		drop: function( event, ui )
			{
				RemoveShowFlow();
				var $drag = $(ui.draggable);
				dataRep = true;				
				
				if (  $(this).html() == "Hangup")
				{
					var wId= $(this).data("wid");
					var pId= $(this).data("pid");
					var nId= $(this).data("nid")
					var flowtype= $(this).data("flowtype")
					
						$.ajax({
						  type: "POST",
						  url: "process_ivr.php",
						   dataType: "html",
						  data:{wId: nId,pId:pId,nId:nId , flowtype:flowtype ,act:'REMOVE-Hangup' }
						})
						dataRep = true;
						$(this).html('Drop here');
				}
				
				if ($(this).html() != "Drop here")
				{
					//dataRep	= confirm('Do you want to replace previous item?'); //$( "#dialog" ).dialog( "open" );
					alert('Please remove item attached 1st to replace previous item?');
					dataRep = false;
				}
				
				if (dataRep) 
				{					
					
					$(this).html($drag.html());
										
					var flowtype= $drag.data("flowtype");
					
					$(this).data('flowtype' ,flowtype);							
					
					var MainContainer = $(this).closest('.blockLeft');					
					var thisBlock = $(this).closest('.block');										
					
					
					//$(MainContainer).append($('#'+flowtype +'-content').html());
					//data-flowtype="empty" data-pid="" data-nid=""  id="drop_here_obj"  data-wid=""  
					
					var wId= $(this).data("wid");
					var pId= $(this).data("pid");
					var nId= $(this).data("nid");
					var PreviousObj= $(this);
					var submenu= $(this).data("submenu");
					
					var SubMenuQuery='';
					if ( submenu == 'yes' ) SubMenuQuery ='&submenu=yes&keypress='+$(this).closest('.row').find('#keypress').val();					

					var htmlData = $('#'+flowtype +'-content').clone(true,true).html();
					
					$.getJSON("process_ivr.php?companyId="+companyId+'&wId='+wId+'&pId='+pId+'&nId='+nId+'&flowtype='+flowtype+'&act=save'+SubMenuQuery, 
					function(jsdata) 
					{

						htmlData= htmlData.replace(/data-submenu="yes" data-pid=""/g,' data-submenu="yes" data-pid="'+jsdata.data['wId']+'"');
						htmlData= htmlData.replace(/data-pid=""/g,'data-pid="'+jsdata.data['pId']+'"');
						htmlData= htmlData.replace(/data-wid=""/g,'data-wid="'+jsdata.data['wId']+'"');
						htmlData= htmlData.replace(/myId/g,jsdata.data['wId']);											
						htmlData= htmlData.replace('="blockObj"',' ="blockObj-'+jsdata.data['wId']+'"');
						
						
						
						
						
						
						if ( flowtype !='Hangup' ) 		$(thisBlock).after(htmlData);
		

						
						//$( ".block" ).draggable({containment:  "#flowContainer",axis: "y"});
						
						
						if (jsdata.data['pId'] != '' )
						{
							$(PreviousObj).data('nid',jsdata.data['wId']);
							
						}	
						
							
							$(".drp" ).each(function(index, element) {
							  //$(element).removeClass('.drp');
							  $(element).droppable(drpOptions);//$(element).bind('click', function (){alert('x')});
							  
							});
					});
					
							
					
				}
      }};
		
	$(".drp" ).droppable(drpOptions	);
	/////////////////////////  Drop Function ends////////////////
		

	
 
 
	});
		
		$(window).scroll(function () {
 
         //after window scroll fire it will add define pixel added to that element.

                    scrollAmount = $(document).scrollTop()+"px";
					sc = parseInt(scrollAmount.replace('px',''));
        //this is the jQuery animate function to fixed the div position after scrolling.
						if( sc > 110) 
						scrollAmount = ( sc -110) + 'px';
						
                    	$('#flotingDiv').animate({top:scrollAmount},{duration:1000,queue:false});
						
						
					
					
					

                });
				
	function addMoreRow(obj){
		RemoveShowFlow()
		MyTable = $(obj).closest('#mytable');
		
		clonedRow= $(obj).closest('.row').clone(true).html();

 
 //This line wraps the clonedRow and wraps it <tr> tags since cloning ignores those tags
  appendRow = '<tr class = "row">' + clonedRow + '</tr>'; 

appendRow= appendRow.replace('<a class="add action btnAddMore" href="javascript:void(0)" id="btnAddMore" onclick="addMoreRow(this)"><span class="replace">Add</span> </a>', '<a class="remove action deleteThisRow" href="javascript:void(0)" onclick="deleteRow(this)"> <span class="replace" >Remove</span> </a>');

		
		appendRow= appendRow.replace(/data-wid=/g, ' data-wid="" ');
		appendRow= appendRow.replace(/data-flowtype=/g, ' data-flowtype="" ');
		
		
		appendRow= appendRow.replace(/Dial/g, 'Drop here');
		appendRow= appendRow.replace(/Greetings/g, 'Drop here');
		appendRow= appendRow.replace(/Menu/g, 'Drop here');
		appendRow= appendRow.replace(/SMS/g, 'Drop here');
		appendRow= appendRow.replace(/Hangup/g, 'Drop here');
		appendRow= appendRow.replace(/Voicemail/g, 'Drop here');

		
	
		
		$(MyTable).append(appendRow);
		
		$(".drp" ).each(function(index, element)
		{			
			$(element).droppable(drpOptions);
		});	
	}
	 function deleteRow(currentNode)
	 {
		RemoveShowFlow() 
		var dho = $(currentNode).closest('.row').find('#drop_here_obj');

		var myNextId= $(dho).data("nid");
		
		if ( myNextId !='' )
		{
			
			alert('please remove call flow attacthed 1st then remove menu item');		
			return;
		}else	$(currentNode).parent().parent().remove();

			
	  	
	  
	 }

var	appendRow='';



 
  //$('.ivr-Menu-close-button').click(function(){});
function showHideNumBox(obj){


if( $(obj).val() == 'dial' )
{
	$(obj).siblings('#phoneNumber').show();
}else{
	$(obj).siblings('#phoneNumber').hide();	
}

}

function CloseButton(obj)
{
	
	
	
	var audioChoice = $(obj).closest('.ivr-Menu');
	
	var audioChoiceEditor	= audioChoice.children('.ivr-Menu-editor');
	var audioChoiceSelector	= audioChoice.children('.ivr-Menu-selector');
	
	var subDiv	= audioChoiceEditor.children('.ivr-Menu-editor-padding');
	
	audioChoiceSelector.show();
	audioChoiceEditor.hide();
	subDiv.children('.ivr-audio-upload').hide();	
	subDiv.children('.ivr-Menu-read-text').hide();
	

}

function showAudioText(obj)
{
	
	
	var audioChoice = $(obj).closest('.ivr-Menu-selector'); 
	audioChoice.hide();
	audioChoice.parent().children('.ivr-Menu-editor').show();
	var subDiv= audioChoice.parent().children('.ivr-Menu-editor').children('.ivr-Menu-editor-padding');

	if ( obj.id  == 'txt' ) 
	{		
		
		subDiv.children('.ivr-Menu-read-text').show();
	
	}else{
		
		subDiv.children('.ivr-audio-upload').show();		
	}
	
}


function RemoveBlock(obj)
{
	
	var thisBlock = $(obj).closest('.block');
	var myflowtype = $(thisBlock).data('myflowtype');

	var dho = $(thisBlock).find('#drop_here_obj')
	
	var flowtype= $(dho).data("flowtype");

	var nId= $(dho).data("nid");
	//if ( nId != '' )
	
	if ( myflowtype != 'Menu')
	{
		var BlockH2 = $(thisBlock).find('h2').html();
			removeConfirm	= confirm('Are you sure, you want to remove "'+ BlockH2 +'" from call flow?');
		if ( removeConfirm )	
	
			RemoveSubBlock(obj);				
	}else
		
			RemoveMenu(obj);
	
}

function RemoveMenu(obj)
{

	var thisBlock = $(obj).closest('.block');
	var BlockH2 = $(thisBlock).find('h2').html();
	var nextId ='';
	var ParentId='';
	var SubItems = false;
	$( thisBlock).find('#drop_here_obj').each(function( index, element ) 
	{
		if ( $(element).data('menu') != 'next' && $(element).data("flowtype")  != 'Hangup'  && $(element).data('nid') != '')
		{
			
			SubItems= true;
			
			
		}
		
		/*if ( $(element).data('menu') == 'next' )
		{
			 NextId = $(element).data('nid');
			 ParentId= $(element).data('pid');
		}*/
		
	});
	
	if ( SubItems){ 
		alert('Please remove menu options 1st');
		return;
	}
	
	removeConfirm	= confirm('Are you sure, you want to remove "'+ BlockH2 +'" from call flow?');
	if ( removeConfirm )
	{
		$( thisBlock).find('#drop_here_obj').each(function( index, element ) 
		{
		
			if ( $(element).data('flowtype') == 'Hangup' )
			{
					$.ajax({
						  type: "POST",
						  url: "process_ivr.php",
						   dataType: "html",
						  data:{wId:  $(element).data('nId'),pId: $(element).data('pid'),nId: $(element).data('nid') , flowtype: $(element).data('flowtype') ,act:'REMOVE-Hangup' }
						});
						
			}
			if ( $(element).data('menu') != 'next')
			{
				$(element).remove();
			}
			/*else{
				 $(element).data('pid',NextId);
				 $(element).data('nid',ParentId);
			}*/
			
		});
		
		RemoveSubBlock(obj);		
	}
}


function RemoveSubBlock(obj){
	var thisBlock = $(obj).closest('.block');
	var dho = $(thisBlock).find('#drop_here_obj')
	var wId= $(dho).data("wid");
	var pId= $(dho).data("pid");
	var nId= $(dho).data("nid");
	var flowtype= $(dho).data("flowtype");
	RemoveShowFlow();
	if ( flowtype == 'Hangup' )
	{
			$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				   dataType: "html",
				  data:{wId: wId,pId:pId,nId:nId , flowtype:flowtype ,act:'REMOVE-Hangup' }
				})
	}
	
	if ( nId != '' )
	{
		if ( pId == '' ){ 
			
			$('#blockMain').find('#drop_here_obj').html($( '#blockObj-'+ wId).find('#drop_here_obj').html());
			$('#blockMain').find('#drop_here_obj').data('nid',nId);
			$( '#blockObj-'+ nId).find('#drop_here_obj').data('pid','');
			
		}else{
			
			$( '#blockObj-'+ pId).find('#drop_here_obj').each(function( index, element ) 
			{
				if ( $(element).data('nid') == wId)
				{
					$(element).html($( '#blockObj-'+ wId).find('#drop_here_obj').html());
					$(element).data('nid',nId);
				}
			});
			
			
			$( '#blockObj-'+ nId).find('#drop_here_obj').data('pid',pId);
			
		}
		
	}else{
		
		if ( pId == '' )
		{
			$('#blockMain').find('#drop_here_obj').html('Drop here');
			$('#blockMain').find('#drop_here_obj').data('nid','');

		}else{
			
			$( '#blockObj-'+ pId).find('#drop_here_obj').each(function( index, element ) 
			{
				if ( $(element).data('nid') == wId)
				{
					$(element).html('Drop here');
					$(element).data('nid','');
				}
			});
			/*
			$( '#blockObj-'+ pId).find('#drop_here_obj').html('Drop here');
			$( '#blockObj-'+ pId).find('#drop_here_obj').data('nid','')
			*/
		}		
	}
	
	
	
	var flowtype= $(dho).data("flowtype");
	
	//////
				$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				   dataType: "html",
				  data:{wId: wId,pId:pId,nId:nId , flowtype:flowtype ,act:'REMOVE' }
				})
				  .done(function( msg ) {
					//alert( "Option Removed " + msg );
					
					$('#blockObj-'+ wId).hide('slow',function() {	$( '#blockObj-'+ wId ).remove();  });
					
				  });
		
}

function update_msg (msg)
{
	
	$("#flow_msg_parent").css("display","block");
	$("#flow_msg_parent").html(msg);
	
	}

function SaveContent(obj,content_type)
{

	var thisBlock = $(obj).closest('.block');
	switch (content_type) {
            case "SMS":
            {
				
				var dho = $(thisBlock).find('#drop_here_obj');
				var sms_content = $(thisBlock).find('#sms_content').val();	
					
				var wId= $(dho).data("wid");
				var pId= $(dho).data("pid");
				var nId= $(dho).data("nid");
				/*console.log(dho);
				alert(sms_content + text);*/
				
				
				//////
				$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				   dataType: "html",
				  data:{wId: wId , sms_content:sms_content ,act:'SMS' }
				})
				  .done(function( msg ) {
					  if (msg == '1')
					  {
						update_msg ("SMS Text Saved Successfully");	
				  		
					  }
				  });
  
				  		
				break;
			
				
				
			
			}
			
			case "Dail":
            {
				
				var dho = $(thisBlock).find('#drop_here_obj');
				var dail_number = $(thisBlock).find('#postal_code').val();	
				
				if ( $(thisBlock).find('#option_International').is(":checked"))
				{ 
				
				var dail_international = 'Y';
					
					} else 
					{
						
				var dail_international = 'N';
						
						}
				
			
				
				var wId = $(dho).data("wid");
				var pId = $(dho).data("pid");
				var nId = $(dho).data("nid");
				
				
				/*console.log(dho);
				alert(dail_number + wId + dail_international + nId);
				*/
				//////
				$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				  dataType: "html",
				  data:{wId: wId , dail_number:dail_number, dail_inter:dail_international , act:'Dail' }
				})
				  .done(function( msg ) {
					 if (msg == '1')
					  {
						update_msg ("Dail Number Saved Successfully");	
				  		
					  }
				  });
  
				  		
				break;
			}
			
			case "Text_mail":
            {
				
				var dho = $(thisBlock).find('#drop_here_obj');
				var voice_text = $(thisBlock).find('#readtxt_mail').val();	
				
				var wId = $(dho).data("wid");
				var pId = $(dho).data("pid");
				var nId = $(dho).data("nid");
				
				//alert(voice_text + wId );
				/*console.log(dho);
				
				*/
				//////
				$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				  dataType: "html",
				  data:{wId: wId , text_content:voice_text, act:'Text_mail' }
				})
				  .done(function( msg ) {
					 if (msg == '1')
					  {
						update_msg ("Text Saved Successfully");	
				  		
					  }
				  });
  
				  		
				break;
			}
			
			case "Text_greeting":
            {
				
				var dho = $(thisBlock).find('#drop_here_obj');
				var greeting_text = $(thisBlock).find('#readtxt_greetext').val();	
				
				var wId = $(dho).data("wid");
				var pId = $(dho).data("pid");
				var nId = $(dho).data("nid");
				
				//alert(voice_text + wId );
				/*console.log(dho);
				
				*/
				//////
				$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				  dataType: "html",
				  data:{wId: wId , text_content:greeting_text, act:'Text_greet' }
				})
				  .done(function( msg ) {
					 if (msg == '1')
					  {
						update_msg ("Greeting Text Saved Successfully");	
				  		
					  }
				  });
  
				  		
				break;
			}
			
			case "Manue":
            {
				alert ("manue here");
				var dho = $(thisBlock).find('#drop_here_obj');
				var greeting_text = $(thisBlock).find('#readtxt_greetext').val();	
				
				var wId = $(dho).data("wid");
				var pId = $(dho).data("pid");
				var nId = $(dho).data("nid");
				
				//alert(voice_text + wId );
				/*console.log(dho);
				
				*/
				//////
				$.ajax({
				  type: "POST",
				  url: "process_ivr.php",
				  dataType: "html",
				  data:{wId: wId , text_content:greeting_text, act:'Text_greet' }
				})
				  .done(function( msg ) {
					  
					update_msg ("SMS Text Saved Successfully");	
				 
				  });
  
				  		
				break;
			}
			
			
			
		}
		
		
}

 </script>
<div id="Hangup-content"   style="display:none">
  <div class="block" id="blockObj" data-myflowtype="Hangup"></div>
</div>
<div id="Greetings-content"   style="display:none">
  <div class="block" id="blockObj" data-myflowtype="Greetings">
    <div class="block_head">
      <div class="bheadl"></div>
      <div class="bheadr"></div>
      <h2>Greetings </h2>
      <div style="display:inline-block; float: right;">
        <h2 style="font-size: 16px;"><a href="javascript:void(0);" onClick="RemoveBlock(this)">Remove</a></h2>
      </div>
    </div>
    <!-- .block_head ends -->
    
    <div class="block_content">
      <fieldset class="ivr-Menu ivr2-input-container">
        <div class="ivr-Menu-selector" style="display: block">
          <div class="ivr-Menu-selector-item-wrapper">
            <div class="padding-and-border"> <a id="txt" class="ivr-Menu-selector-item" href="javascript:void(0)" onclick="showAudioText(this)"> <span class="title">Text To Speak</span></a> </div>
          </div>
          <div class="ivr-Menu-selector-item-wrapper">
            <div class="padding-and-border"> <a id="mp3" class="ivr-Menu-selector-item" href="javascript:void(0)" onclick="showAudioText(this)" > <span class="title">Greeting Audio</span></a></div>
          </div>
        </div>
        <div class="ivr-Menu-editor ">
          <div class="ivr-Menu-editor-padding" style="padding: 10px;">
            <div class="ivr-Menu-read-text" style="display: none;">
              <div class="title-bar"> <span class="editor-label">Text to speak</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
              <br>
              <div>
                <fieldset class="ivr2-input-complex ivr2-input-container" style="align: center;">
                  <label class="field-label">
                    <textarea class="voicemail-text" name="readtxt_greetext" id="readtxt_greetext"><?php echo $readtxt ?></textarea>
                    <input type="button"  class="submit mid" id="save_greetext" value="Save" onClick="SaveContent(this,'Text_greeting')">
                  </label>
                </fieldset>
              </div>
              <br>
              <br>
            </div>
            <div class="ivr-audio-upload" style="display: none;">
              <div class="title-bar"> <span class="editor-label">Upload an MP3 file...</span> <a class="action close ivr-Menu-close-button" href="javascript:void(0)" onclick="CloseButton(this)"> <span class="replace">close</span> </a> </div>
              <div style="height: 68px;">
                <div class="swfupload-container">
                  <div class="explanation"> <br>
                    <?php if ( $upload_mp3!= '' ) {?>
                    <span class="title">Current File: <strong><?php echo $upload_mp3 ?></strong></span> <br>
                    <?php } ?>
                    <span class="title">Click to select a file</span>
                    <input name="upload_mp3" type="file"  id="upload_mp3"/>
                    <br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </fieldset>
      <br>
      <br/>
      <span>
      <h2>What you want to do next ?</h2>
      </span> <a  href="javascript:void(0)" onClick="showContent(this)"   data-flowtype="empty" data-pid="" data-nid=""  id="drop_here_obj"  data-wid=""   class="drp">Drop here</a> </div>
    <!-- .block_content ends -->
    
    <div class="bendl"></div>
    <div class="bendr"></div>
  </div>
</div>
<div id="Greetings-content" style="display:none" >
  <?php	genGreetingsBlock(); 	?>
</div>
<div id="Voicemail-content" style="display:none" >
  <?php	genVoicemailBlock(); 	?>
</div>
<div id="SMS-content" style="display:none" >
  <?php	genSMSBlock(); 			?>
</div>
<div id="Dial-content" style="display:none">
  <?php	genDialBlock();  		?>
</div>
<div id="Menu-content" style="display:none">
  <?php	genMenuBlock();  		?>
</div>
<script src="js/jquery.jsPlumb-1.5.2-min.js"></script> 
<script src="js/script.js"></script>
</body>
</html>