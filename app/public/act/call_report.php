<?php
session_start();
$page = "call_report";
require_once('include/util.php');
require_once('include/Pagination.php');

$db = new DB();
global $RECORDINGS;
if(@$_SESSION['permission'] < 1)
    $companies = $db->getAllCompaniesForUser($_SESSION['user_id']);
else
    $companies = $db->getAllCompanies();

//Page Variables
$user_id = $_SESSION['user_id'];
$display_report = false;

//Pre-load Checks
if(!isset($_SESSION['user_id']))
{
    header("Location: login.php?last_url=".urlencode(s8_get_current_webpage_uri()));
    exit;
}
if(@$_GET['sd']!="" && (@$_GET['ed']=="" || @$_GET['cm']==""))
{
    ?>
<script type="text/javascript">
    alert('An error has occured. Redirecting.');
    window.location = "call_report.php";
</script>
<?php
    exit;
}

global $AccountSid, $AuthToken, $ApiVersion;
$client = new Services_Twilio($AccountSid,$AuthToken);

//Post Function
if(@$_GET['sd']!="" && @$_GET['ed']!="" && @$_GET['cm']!="")
{
    //Declare shore-hand variables
    global $TIMEZONE;
    $tz = new DateTimeZone($TIMEZONE);

    if (strlen($_GET['sd']) < 10) $_GET['sd'] = substr($_GET['sd'], 4, 4)."-".substr($_GET['sd'], 0, 2)."-".substr($_GET['sd'], 2, 2)." 00:00:00";
    if (strlen($_GET['ed']) < 10) $_GET['ed'] = substr($_GET['ed'], 4, 4)."-".substr($_GET['ed'], 0, 2)."-".substr($_GET['ed'], 2, 2)." 00:00:00";

    $start_date = new DateTime($_GET['sd']);
    $year = $start_date->format("Y");
    $month = $start_date->format("n");
    $day = $start_date->format("j");
    $d1 = strtotime($start_date->format("Y-m-d H:i:s"));
    $start_date->setTimezone($tz);
    $d2 = strtotime($start_date->format("Y-m-d H:i:s"));
    $seconds_difference = abs($d2 - $d1);
    $start_date->setDate($year, $month, $day);
    $start_date->setTime(0, 0, 0);

    $end_date = new DateTime($_GET['ed']);
    $year = $end_date->format("Y");
    $month = $end_date->format("n");
    $day = $end_date->format("j");
    $end_date->setTimezone($tz);
    $end_date->setDate($year, $month, $day);
    $end_date->setTime(23, 59, 59);

    $date_start = $start_date;
    $date_end = $end_date;

    $start_date = strtotime($start_date->format("Y-m-d H:i:s")) + $seconds_difference;
    $end_date = strtotime($end_date->format("Y-m-d H:i:s")) + $seconds_difference;

    $company         = $_GET['cm'];
    $selected_phone_code      = $_GET['pc'];
    $call_result     = $_GET['cr'];
    $report_type     = $_GET['rt'];
	
	$agent           = $_GET['agent'];
    if (!empty($_GET['company'])) $company = $db->getCompanyId($_GET['company']);

    $order_by = (empty($_REQUEST['order_by']) ? "DateCreated" : $_REQUEST['order_by']);
    $order_type = (empty($_REQUEST['order_type']) ? "DESC" : $_REQUEST['order_type']);

    if(isset($_GET['on']) && $_GET['on']!="")
        $outgoing_number = $_GET['on'];
    else
        $outgoing_number = "";

    if(!isset($report_type) || $report_type==null)
        $report_type = "default";

    $report_campaign     = (empty($_GET['ca']) ? "" : $_GET['ca']);

    //Check if user belongs to company
    if(!$db->isUserInCompany($company,$user_id) and $_SESSION['permission'] < 1)
    {
        ?>
    <script type="text/javascript">
        alert('An error has occured. You don\'t belong to that company! Redirecting.');
        window.location = "call_report.php";
    </script>
    <?php
        exit;
    }

    if($report_type=="default")
    {
        $cpage = 1;
        // we get the current page from $_GET
        if (isset($_GET['cpage'])){
            $cpage = (int) $_GET['cpage'];
        }

        // create the pagination class
        $pagination = new Pagination();
        if(strpos($_SERVER['REQUEST_URI'],"cpage") === false)
        {
            $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $pagination->setLink($url."&cpage=%s");
        }else{
            $url="http://".$_SERVER['HTTP_HOST'].substr($_SERVER['REQUEST_URI'], 0, -strlen($_REQUEST['cpage']));
            $pagination->setLink($url."%s");
        }
        $pagination->setPage($cpage);
        $pagination->setSize(20);

        //Generate Report from DB
        $result = $db->getCallReport($_SESSION['user_id'],$start_date,$end_date,$company,$call_result,$selected_phone_code,$pagination,$outgoing_number,@$report_campaign,$agent,$order_by,$order_type);
        
        //echo "<!-- ";
        //print_r($result);
        //echo " -->";

        $call_count = $result[1];
        $total_talk_time_inbound = $result[2];

    }
    $display_report = true;
}

if ($display_report) {
    $numbers = $db->getNumbersOfCompany($company, true);

    $campaigns = array();
    foreach ($client->account->incoming_phone_numbers as $number) {
        $this_phone_number = $db->format_phone_db($number->phone_number);
        foreach ($numbers as $number2) {
            if ($number2->number == $this_phone_number) {
                $number->friendly_name = Util::escapeString($number->friendly_name);
                if (!isset($campaigns[$number->friendly_name]))
                    $campaigns[$number->friendly_name] = array("numbers" => array(), "calls_total" => 0, "calls_percent" => 0);

                $campaigns[$number->friendly_name]["numbers"][] = $this_phone_number;
                break;
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo $title; ?></title>

    <?php include "include/css.php"; ?>

    <!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <style type="text/css">
        input.submit {
            width: 165px !important;
            background: url(images/btnb.gif) top center no-repeat !important;
            height: 30px !important;
            line-height: 30px !important;
            border: 0;
            font-family: "Titillium800", "Trebuchet MS", Arial, sans-serif !important;
            font-size: 14px !important;
            font-weight: normal !important;
            text-transform: uppercase !important;
            color: white !important;
            text-shadow: 1px 1px 0 #0A5482;
            cursor: pointer !important;
            margin-right: 10px !important;
            vertical-align: middle !important;
        }
        .ui-state-highlight {
            background:#eee !important;
        }
        table { font-size: 12px !important;}
        <?php if(isset($report_type) && $report_type!="default") { ?>
        #report_logo {
            float:left !important;
            margin-bottom:10px;
            height: 75px;
        }
        <?php } ?>
        #company_info {
            float:right;
        }
        #company_info pre {
            font: 15px 'Helvetica' !important;
            line-height: 1.3em !important;
            margin-top: 5px;
        }
        #stats_table {
            height:285px;
            font: 16px 'Helvetica' !important;
            font-weight: bold !important;
        }
        #stats_table td {
            padding: 0 !important;
        }
        #stats_table td:nth-child(2) {
            font-weight: normal !important;
            text-align: left;
        }
        #header #nav, #header #nav *, #header #nav * * { z-index: 10000; }
    </style>

</head>





<body>

<div id="hld">

<div class="wrapper"<?php if(isset($report_type)) echo " style=\"width:960px\"";?>>     <!-- wrapper begins -->


<?php
if(!isset($report_type) || $report_type=="default")
    include('include/nav.php');
?>
<!-- #header ends -->

<?php
if(isset($report_type) && $report_type!="default") {
    $logoURL = "";
    if($db->getVar("company_logo"))
        $logoURL = $db->getVar("company_logo");

    $info = "";
    if($db->getVar("company_info"))
        $info = $db->getVar("company_info");

?>
    <div id="report_logo"><?php if($logoURL!="") echo "<img src=\"".$logoURL."\" />"; ?></div>

    <div id="company_info"><pre><?php echo $info; ?></pre></div>
<?php } ?>

<?php if(!$display_report) { ?>
        <div class="block" style="width:520px; margin:0 auto;margin-bottom: 25px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Generate Call Report
                </h2>
            </div>

            <div class="block_content">

                <table class="filter-calls" style="width:480px !important; max-width:480px !important; margin:0 auto;">
                    <thead>
                    <tr>
                        <th colspan="2"><center><b>Report Details</b></center></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2" style="border:none;">
                            <center>
                                <label for="start_date">Start:</label> <input type="text" id="start_date" name="start_date" style="width:120px; display: inline-block !important;" />
                                <label for="end_date" style="margin-left:20px;">End:</label> <input type="text" id="end_date" name="end_date" style="width:120px; display: inline-block !important;" />
                            </center>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="border:none; text-align: right;"><label for="call_result">Call Result:</label></td>
                        <td style="border:none;">
                            <select id="call_result">
                                <option value="all">Show ALL calls</option>
                                <option value="missed">Only show missed calls</option>
                                <option value="answered">Only show answered calls</option>
                                <option value="30">&gt; 30 seconds</option>
                                <option value="45">&gt; 45 seconds</option>
                                <option value="60">&gt; 60 seconds</option>
                                <option value="90">&gt; 90 seconds</option>
                                <option value="120">&gt; 2 minutes</option>
                                <option value="180">&gt; 3 minutes</option>
                                <option value="240">&gt; 4 minutes</option>
                                <option value="300">&gt; 5 minutes</option>
                                <option value="360">&gt; 6 minutes</option>
                                <option value="420">&gt; 7 minutes</option>
                                <option value="480">&gt; 8 minutes</option>
                                <option value="540">&gt; 9 minutes</option>
                                <option value="600">&gt; 10 minutes</option>
                                <option value="1200">&gt; 20 minutes</option>
                                <option value="1800">&gt; 30 minutes</option>
                                <option value="2400">&gt; 40 minutes</option>
                            </select>

                        </td>
                    </tr>

                    <tr>
                        <td style="border:none; text-align: right;"><label for="report_type">Report Type:</label></td>
                        <td style="border:none;">
                            <select id="report_type" onchange="if (this.value == 'campaign_rep') { $('.filter-calls tr:eq(7)').hide(); } else { $('.filter-calls tr:eq(7)').show(); }">
                                <option value="default">Call Detail Report</option>
                                <option value="ans_unans">Answered vs Unanswered</option>
                                <option value="daily_trends">Daily Trends</option>
                                <option value="phone_code_rep">Phone Code Report</option>
                                <option value="campaign_rep">Campaign Report</option>
                            </select>

                        </td>
                    </tr>
                    
                    <tr>
                        <td style="border:none; text-align: right; width:140px;"><label for="company_select">Company:</label></td>
                        <td style="border:none;">
                            <select id="company_select" onchange="companySelected(this.options[this.selectedIndex].value)">
                                <option value="none">Select a company</option>
                                <?php
                                if($db->isUserAdmin($_SESSION['user_id']))
                                {
                                    ?><option value="-1">ALL</option><?php echo "\n";
                                }
                                foreach($companies as $company)
                                {
                                    ?>                            <option value="<?php echo $company['idx']?>"><?php echo $company['company_name']?></option><?php echo "\n";
                                }
                                ?>
                            </select>

                        </td>
                    </tr>

                    <tr>
                        <td style="border:none; text-align: right;"><label for="phone_code">Phone Code:</label></td>
                        <td style="border:none;">
                            <select id="phone_code" disabled="true">
                                <option value="false">Select a company first</option>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td style="border:none; text-align: right;"><label for="report_type">Outgoing Number:</label></td>
                        <td style="border:none;">
                            <select id="outgoing_number" name="outgoing_number" disabled="true">
                                <option value="false">Select a company first</option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="border:none; text-align: right; width:140px;"><label for="campaign">Campaign</label></td>
                        <td style="border:none;">
                            <select id="campaign" name="campaign" disabled="true">
                                <option value="false">Select a company first</option>
                            </select>

                        </td>
                    </tr>

                    <tr>
                        <td style="border-spacing:0px; padding:0px; padding-top:10px" class="no_style_td" colspan="2">
                            <table id="rounded-corner" width="100%" height="100%">
                                <tr>
                                    <td style="border:none; width: 316px;" class="no_style_td button_space"></td>
                                    <td style="padding:0px; border:none;" class="no_style_td"><input id="submit_btn" class="submit" type="submit" value="Generate Report"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>      <!-- .block_content ends -->

                <div class="bendl"></div>
                <div class="bendr"></div>
            </div>      <!-- .block ends -->
<?php }else{ ?>
        <input type="hidden" id="company_id" value="<?php echo $company; ?>" />
        <input type="hidden" id="sd" value="<?php echo $start_date; ?>" />
        <input type="hidden" id="ed" value="<?php echo $end_date; ?>" />
        <input type="hidden" id="pc" value="<?php echo @$_GET['pc']; ?>" />
        <input type="hidden" id="cr" value="<?php echo $call_result; ?>" />
        <input type="hidden" id="rt" value="<?php echo $report_type; ?>" />
        <input type="hidden" id="on" value="<?php echo $outgoing_number; ?>" />
        <input type="hidden" id="ca" value="<?php echo $report_campaign; ?>" />
        <input type="hidden" id="agent" value="<?php echo $agent; ?>" />
        <script type="text/javascript">
            $(document).ready(function() {
                drawChart();
            });
                function drawChart() {
                    var report_type = $("#rt").val();

                    <?php if($report_type=="ans_unans") { ?>
                        Web1Graphs.graphTypes.answeredToUnanswered.draw_graph(
                            "ans_unans_pie",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),
                            $("#on").val(),
                            false,
                            '',
                            $("#ca").val());

                        Web1Graphs.graphTypes.callByTheHour.draw_graph(
                            "hourly",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),
                            'all',
                            $("#on").val(),
                            false,
                            '',
                            $("#ca").val());

                        Web1Graphs.graphTypes.callByTheWeek.draw_graph(
                            "day_of_week",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),
                            $("#on").val(),
                            false,
                            $("#ca").val());
                    <?php } ?>
                    <?php if($report_type=="daily_trends") { ?>
                        Web1Graphs.graphTypes.callByTheWeek.draw_graph(
                            "graph",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),
                            $("#on").val(),
                            false,
                            $("#ca").val());
                        Web1Graphs.graphTypes.callByTheHour.draw_graph(
                            "monday_g",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),"monday",$("#on").val(),
                            false,
                            '',
                            $("#ca").val());
                        Web1Graphs.graphTypes.callByTheHour.draw_graph(
                            "tuesday_g",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),"tuesday",$("#on").val(),
                            false,
                            '',
                            $("#ca").val());
                        Web1Graphs.graphTypes.callByTheHour.draw_graph(
                            "wednesday_g",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),"wednesday",$("#on").val(),
                            false,
                            '',
                            $("#ca").val());
                        Web1Graphs.graphTypes.callByTheHour.draw_graph(
                            "thursday_g",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),"thursday",$("#on").val(),
                            false,
                            '',
                            $("#ca").val());
                        Web1Graphs.graphTypes.callByTheHour.draw_graph(
                            "friday_g",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),"friday",$("#on").val(),
                            false,
                            '',
                            $("#ca").val());
                        Web1Graphs.graphTypes.callByTheHour.draw_graph(
                            "saturday_g",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),"saturday",$("#on").val(),
                            false,
                            '',
                            $("#ca").val());
                        Web1Graphs.graphTypes.callByTheHour.draw_graph(
                            "sunday_g",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),"sunday",$("#on").val(),
                            false,
                            '',
                            $("#ca").val());
                    <?php } ?>

                    <?php if($report_type=="phone_code_rep") { ?>
                        Web1Graphs.graphTypes.callByTheWeek.draw_graph(
                                "graph",
                                $("#company_id").val(),
                                $("#sd").val(),
                                $("#ed").val(),
                                $("#pc").val(),
                                $("#cr").val(),
                                $("#on").val(),
                                false,
                                $("#ca").val());
                    <?php } ?>

                    <?php
                    if($report_type=="campaign_rep") { 
                    ?>
                        Web1Graphs.graphTypes.campaignDailyTrends.draw_graph(
                            "graph",
                            $("#company_id").val(),
                            $("#sd").val(),
                            $("#ed").val(),
                            $("#pc").val(),
                            $("#cr").val(),
                            $("#on").val());

                        <?php foreach ($campaigns as $name=>$campaign) { ?>

                            Web1Graphs.graphTypes.campaignPhoneCodes.draw_graph(
                                "<?php echo str_replace(" ", "_", strtolower($name)); ?>",
                                $("#company_id").val(),
                                $("#sd").val(),
                                $("#ed").val(),
                                $("#pc").val(),
                                $("#cr").val(),
                                $("#on").val(),
                                "<?php echo $name; ?>");
                        <?php } ?>
                    <?php 
                    }
                    ?>

                    <?php if($report_type=="ans_unans" || $report_type=="default") { ?>
                    // Top graph / Default graph
                    Web1Graphs.graphTypes.customDate.draw_graph(
                        "graph",
                        report_type,
                        $("#company_id").val(),
                        $("#sd").val(),
                        $("#ed").val(),
                        $("#pc").val(),
                        $("#cr").val(),
                        $("#on").val(),
                        false,
                        '',
                        $("#ca").val(),
                        $("#agent").val());
                    <?php } ?>


                    $("#graph_types").change(function(){
                        $("#charts").block({
                            message: '<h1 style="color:#fff">Loading...</h1>',
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .7,
                                color: '#fff !important'
                            }
                        });
                        var selectedVal = $('#graph_types option:selected').attr('value');

                        if(selectedVal=="customDate")
                        {
                            Web1Graphs.graphTypes.customDate.draw_graph(
                                "graph",
                                report_type,
                                $("#company_id").val(),
                                $("#sd").val(),
                                $("#ed").val(),
                                $("#pc").val(),
                                $("#cr").val(),
                                $("#on").val(),
                                false,
                                '',
                                $("#ca").val(),
                                $("#agent").val());
                        }
                        if(selectedVal == 'callByTheHour')
                        {
                            Web1Graphs.graphTypes[selectedVal].draw_graph("graph",
                                    $("#company_id").val(),
                                    $("#sd").val(),
                                    $("#ed").val(),
                                    $("#pc").val(),
                                    $("#cr").val(),'all',
                                    $("#on").val(),
                                    false,
                                    '',
                                    $("#ca").val(),
                                    $("#agent").val());
                        }
                        else{
                            Web1Graphs.graphTypes[selectedVal].draw_graph("graph",
                                $("#company_id").val(),
                                $("#sd").val(),
                                $("#ed").val(),
                                $("#pc").val(),
                                $("#cr").val(),
                                $("#on").val(),
                                false,
                                '',
                                $("#ca").val(),
                                $("#agent").val());
                        }
                    });
                }
        </script>
        <div class="block" id="charts" style="margin-bottom: 12px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>
                <?php
                    switch($report_type)
                    {
                        case "ans_unans":
                            $report_title = "Answered vs Unanswered";
                            break;
                        case "daily_trends":
                            $report_title = "Daily Trends";
                            break;
                        case "phone_code_rep":
                            $report_title = "Phone Code Report";
                            break;
                        case "campaign_rep":
                            $report_title = "Campaign Report";
                            break;
                        default:
                            $report_title = "Call Detail Report";
                            break;
                    }
                $company_name = "";

                if($company==-1)
                    $company_name = "ALL COMPANIES";
                else
                    $company_name = $db->getCompanyName($company);
                ?>
                <h2><?php echo $report_title; ?> for:  <span style="color:#777; text-transform: none;"><?php echo $company_name; ?></span></h2>


                <?php if($report_type=="default"){ ?>
                <form style="padding:12px 0;">
                    <select class="styled" id="graph_types">
                        <option value="customDate" selected="selected">Call History</option>
                        <option value="answeredToUnanswered">Answered vs Unanswered</option>
                        <option value="callByTheHour">Calls By Hour</option>
                    </select>
                </form>
                <?php } ?>

            </div>      <!-- .block_head ends -->



            <div class="block_content" id="days" style="padding: 10px 5px 0;">

                <div id="graph" style="width: 960px; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->

<?php if($report_type=="daily_trends") { ?>
    <div style="float: left; width: 49%;">
        <div class="block" style="width: 100%; display: inline-block; height: 364px; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Statistics
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">
                <?php
                $calls      = $db->getCallsInRange($company,$start_date,$end_date,$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                $calls_at   = $db->getCallsInRangeCount($company,strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400),$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                $answered   = 0;
                $unanswered = 0;
                $unique     = $db->getCallsInRangeCount($company,$start_date,$end_date,$selected_phone_code,$call_result,true,$outgoing_number,false,"",$report_campaign,$agent);
                $unique_at  = $db->getCallsInRangeCount($company,strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400),$selected_phone_code,$call_result,true,$outgoing_number,false,"",$report_campaign,$agent);
                $failed     = 0;
                $busy       = 0;
                $seconds    = 0;

                foreach ($calls as $call)
                {
                    switch($call['DialCallStatus'])
                    {
                        case "no-answer":
                            $unanswered++;
                            break;
                        case "busy":
                            $busy++;
                            break;
                        case "completed":
                            $answered++;
                            break;
                        default:
                            $failed++;
                            break;
                    }

                    if($call['DialCallDuration']!=NULL)
                        $seconds = $call['DialCallDuration'] + $seconds;
                }
                
                ?>


                <table cellpadding="0" cellspacing="0" width="100%" class="sortable" id="stats_table">
                    <tr style="border-bottom: 1px solid #ddd;">
                        <td>Date Range: </td><td><?php echo $date_start->format("n\/j\/Y"); ?> To <?php echo $date_end->format("n\/j\/Y"); ?></td>
                    </tr>
                    <?php if($selected_phone_code!=0) { ?>
                    <tr>
                        <td>Phone Code: </td><td><?php echo $db->getPhoneCodeName($selected_phone_code); ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td>Total Calls: </td><td><?php echo count($calls); ?></td>
                    </tr>
                    <tr>
                        <td>Total Calls All Time: </td><td><?php echo $calls_at; ?></td>
                    </tr>
                    <tr>
                        <td>Answered: </td><td><?php echo $answered; ?> (<?php echo Util::percent($answered,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unanswered: </td><td><?php echo $unanswered; ?> (<?php echo Util::percent($unanswered,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Busy: </td><td><?php echo $busy; ?> (<?php echo Util::percent($busy,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unique: </td><td><?php echo $unique; ?> (<?php echo Util::percent($unique,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unique All Time: </td><td><?php echo $unique_at; ?> (<?php echo Util::percent($unique_at,$calls_at); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Failed Calls: </td><td><?php echo $failed; ?> (<?php echo Util::percent($failed,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Total Minutes: </td><td><?php if($seconds!=0) echo round($seconds/60); else echo 0; ?> minute(s)</td>
                    </tr>
                    <tr>
                        <td>Avg. Duration: </td><td><?php if($seconds!=0) echo round(($seconds/60)/count($calls),1); else echo 0; ?> minute(s)</td>
                    </tr>
                </table>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="float: right; width: 49%;">
        <div class="block" style="width: 100%; height: 364px; float:right; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Monday
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="monday_g" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="clear: both;"></div>

    <div style="float: left; width: 49%;">
        <div class="block" style="width: 100%; display: inline-block; height: 364px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Tuesday
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="tuesday_g" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="float: right; width: 49%;">
        <div class="block" style="width: 100%; height: 364px; float:right; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Wednesday
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="wednesday_g" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="clear: both;"></div>

    <div style="float: left; width: 49%;">
        <div class="block" style="width: 100%;display: inline-block; height: 364px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Thursday
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="thursday_g" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="float: right; width: 49%;">
        <div class="block" style="width: 100%; height: 364px; float:right; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Friday
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="friday_g" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="clear: both;"></div>

    <div style="float: left; width: 49%;">
        <div class="block" style="width: 100%; display: inline-block; height: 364px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Saturday
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="saturday_g" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="float: right; width: 49%;">
        <div class="block" style="width: 100%; height: 364px; float:right; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Sunday
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="sunday_g" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>
<?php } ?>

<?php if($report_type=="phone_code_rep") { ?>
    <div style="float: left; width: 49%;">
        <div class="block" style="width: 100%; display: inline-block; height: 364px; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Statistics
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">
                <?php
                $calls      = $db->getCallsInRange($company,$start_date,$end_date,$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                $calls_at   = $db->getCallsInRangeCount($company,strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400),$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                $answered   = 0;
                $unanswered = 0;
                $unique     = $db->getCallsInRangeCount($company,$start_date,$end_date,$selected_phone_code,$call_result,true,$outgoing_number,false,"",$report_campaign,$agent);
                $unique_at  = $db->getCallsInRangeCount($company,strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400),$selected_phone_code,$call_result,true,$outgoing_number,false,"",$report_campaign,$agent);
                $failed     = 0;
                $busy       = 0;
                $seconds    = 0;

                foreach ($calls as $call)
                {
                    switch($call['DialCallStatus'])
                    {
                        case "no-answer":
                            $unanswered++;
                            break;
                        case "busy":
                            $busy++;
                            break;
                        case "completed":
                            $answered++;
                            break;
                        default:
                            $failed++;
                            break;
                    }

                    if($call['DialCallDuration']!=NULL)
                        $seconds = $call['DialCallDuration'] + $seconds;
                }
                
                ?>


                <table cellpadding="0" cellspacing="0" width="100%" class="sortable" id="stats_table">
                    <tr style="border-bottom: 1px solid #ddd;">
                        <td>Date Range: </td><td><?php echo $date_start->format("n\/j\/Y"); ?> To <?php echo $date_end->format("n\/j\/Y"); ?></td>
                    </tr>
                    <?php if($selected_phone_code!=0) { ?>
                    <tr>
                        <td>Phone Code: </td><td><?php echo $db->getPhoneCodeName($selected_phone_code); ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td>Total Calls: </td><td><?php echo count($calls); ?></td>
                    </tr>
                    <tr>
                        <td>Total Calls All Time: </td><td><?php echo $calls_at; ?></td>
                    </tr>
                    <tr>
                        <td>Answered: </td><td><?php echo $answered; ?> (<?php echo Util::percent($answered,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unanswered: </td><td><?php echo $unanswered; ?> (<?php echo Util::percent($unanswered,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Busy: </td><td><?php echo $busy; ?> (<?php echo Util::percent($busy,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unique: </td><td><?php echo $unique; ?> (<?php echo Util::percent($unique,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unique All Time: </td><td><?php echo $unique_at; ?> (<?php echo Util::percent($unique_at,$calls_at); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Failed Calls: </td><td><?php echo $failed; ?> (<?php echo Util::percent($failed,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Total Minutes: </td><td><?php if($seconds!=0) echo round($seconds/60); else echo 0; ?> minute(s)</td>
                    </tr>
                    <tr>
                        <td>Avg. Duration: </td><td><?php if($seconds!=0) echo round(($seconds/60)/count($calls),1); else echo 0; ?> minute(s)</td>
                    </tr>
                </table>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="float: right; width: 49%;">
        <div class="block" style="width: 100%; height: 364px; float:right; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Phone Codes
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content" style="">

                <div id="phone_codes" style="width: 100%; height: 300px; min-height: 300px; position: relative; overflow: auto;">
                   <?php
                        $selected_phone_code_list = array(
                            0 => (object)array('count'=>0,'name'=>'No Phone Code')
                        );
                        if(@$_GET['cm']!=-1){
                            $selected_phone_codes = $db->getPhoneCodes($_GET['cm']);
                        }else{
                            $selected_phone_codes = $db->customExecute("SELECT * FROM company_phone_code;");
                            $selected_phone_codes->execute();
                            $selected_phone_codes = $selected_phone_codes->fetchAll(PDO::FETCH_OBJ);
                        }

                        if ($selected_phone_codes) {
                            foreach($selected_phone_codes as $selected_phone_code)
                            {
                                $selected_phone_code->count = 0;
                                $selected_phone_code_list[$selected_phone_code->idx] = $selected_phone_code;
                            }
                        }

                        foreach($calls as $call)
                        {
                            $selected_phone_code_list[$call['PhoneCode']]->count = $selected_phone_code_list[$call['PhoneCode']]->count+1;
                        }

                        $total_count = 0;
                        foreach ($selected_phone_code_list as $pc) {
                            $total_count = $total_count + $pc->count;
                        }

                   ?>
                    <table cellpadding="0" cellspacing="0" width="100%" class="sortable" id="phone_code_table">
                        <tbody>
                            <?php foreach($selected_phone_code_list as $pc) {
                                $percent = ($total_count == 0) ? 0 : ($pc->count/$total_count * 100);
                            ?>
                            <tr>
                                <td><?php echo $pc->name; ?></td><td><?php echo $pc->count; ?> (<?php echo number_format($percent, '2', '.', ','); ?>%)</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>
<?php } ?>

<?php if($report_type=="ans_unans") { ?>

    <div style="float: left; width: 49%;">
        <div class="block" style="width: 100%; display: inline-block; height: 364px; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Statistics
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">
                <?php
                $calls      = $db->getCallsInRange($company,$start_date,$end_date,$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                $calls_at   = $db->getCallsInRangeCount($company,strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400),$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                $answered   = 0;
                $unanswered = 0;
                $unique     = $db->getCallsInRangeCount($company,$start_date,$end_date,$selected_phone_code,$call_result,true,$outgoing_number,false,"",$report_campaign,$agent);
                $unique_at  = $db->getCallsInRangeCount($company,strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400),$selected_phone_code,$call_result,true,$outgoing_number,false,"",$report_campaign,$agent);
                $failed     = 0;
                $busy       = 0;
                $seconds    = 0;

                foreach ($calls as $call)
                {
                    switch($call['DialCallStatus'])
                    {
                        case "no-answer":
                            $unanswered++;
                            break;
                        case "busy":
                            $busy++;
                            break;
                        case "completed":
                            $answered++;
                            break;
                        default:
                            $failed++;
                            break;
                    }

                    if($call['DialCallDuration']!=NULL)
                        $seconds = $call['DialCallDuration'] + $seconds;
                }
                
                ?>


                <table cellpadding="0" cellspacing="0" width="100%" class="sortable" id="stats_table">
                    <tr style="border-bottom: 1px solid #ddd;">
                        <td>Date Range: </td><td><?php echo $date_start->format("n\/j\/Y"); ?> To <?php echo $date_end->format("n\/j\/Y"); ?></td>
                    </tr>
                    <?php if($selected_phone_code!=0) { ?>
                    <tr>
                        <td>Phone Code: </td><td><?php echo $db->getPhoneCodeName($selected_phone_code); ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td>Total Calls: </td><td><?php echo count($calls); ?></td>
                    </tr>
                    <tr>
                        <td>Total Calls All Time: </td><td><?php echo $calls_at; ?></td>
                    </tr>
                    <tr>
                        <td>Answered: </td><td><?php echo $answered; ?> (<?php echo Util::percent($answered,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unanswered: </td><td><?php echo $unanswered; ?> (<?php echo Util::percent($unanswered,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Busy: </td><td><?php echo $busy; ?> (<?php echo Util::percent($busy,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unique: </td><td><?php echo $unique; ?> (<?php echo Util::percent($unique,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unique All Time: </td><td><?php echo $unique_at; ?> (<?php echo Util::percent($unique_at,$calls_at); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Failed Calls: </td><td><?php echo $failed; ?> (<?php echo Util::percent($failed,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Total Minutes: </td><td><?php if($seconds!=0) echo round($seconds/60); else echo 0; ?> minute(s)</td>
                    </tr>
                    <tr>
                        <td>Avg. Duration: </td><td><?php if($seconds!=0) echo round(($seconds/60)/count($calls),1); else echo 0; ?> minute(s)</td>
                    </tr>
                </table>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>


    <div style="float: right; width: 49%;">
        <div class="block" style="width: 100%; height: 364px; float:right; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Comparison of Calls
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="ans_unans_pie" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="clear: both;"></div>

    <div style="float: left; width: 49%;">
        <div class="block" style="width: 100%;display: inline-block; height: 364px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Day of Week
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="day_of_week" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>


    <div style="float: right; width: 49%;">
        <div class="block" style="width: 100%; height: 364px; float:right;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Hourly
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <div id="hourly" style="width: 100%; height: 300px; position: relative; "></div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

<?php } ?>

<?php if($report_type=="campaign_rep") { ?>
    <div style="float: left; width: 49%;">
        <div class="block" style="width: 100%; display: inline-block; height: 364px; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Statistics
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">
                <?php
                $calls      = $db->getCallsInRange($company,$start_date,$end_date,$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                $calls_at   = $db->getCallsInRangeCount($company,strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400),$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                $answered   = 0;
                $unanswered = 0;
                $unique     = $db->getCallsInRangeCount($company,$start_date,$end_date,$selected_phone_code,$call_result,true,$outgoing_number,false,"",$report_campaign,$agent);
                $unique_at  = $db->getCallsInRangeCount($company,strtotime(date("2000-m-d H:i:s")),(strtotime("now") + 86400),$selected_phone_code,$call_result,true,$outgoing_number,false,"",$report_campaign,$agent);
                $failed     = 0;
                $busy       = 0;
                $seconds    = 0;

                foreach ($calls as $call)
                {
                    switch($call['DialCallStatus'])
                    {
                        case "no-answer":
                            $unanswered++;
                            break;
                        case "busy":
                            $busy++;
                            break;
                        case "completed":
                            $answered++;
                            break;
                        default:
                            $failed++;
                            break;
                    }

                    if($call['DialCallDuration']!=NULL)
                        $seconds = $call['DialCallDuration'] + $seconds;
                }
                
                ?>


                <table cellpadding="0" cellspacing="0" width="100%" class="sortable" id="stats_table">
                    <tr style="border-bottom: 1px solid #ddd;">
                        <td>Date Range: </td><td><?php echo $date_start->format("n\/j\/Y"); ?> To <?php echo $date_end->format("n\/j\/Y"); ?></td>
                    </tr>
                    <?php if($selected_phone_code!=0) { ?>
                    <tr>
                        <td>Phone Code: </td><td><?php echo $db->getPhoneCodeName($selected_phone_code); ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td>Total Calls: </td><td><?php echo count($calls); ?></td>
                    </tr>
                    <tr>
                        <td>Total Calls All Time: </td><td><?php echo $calls_at; ?></td>
                    </tr>
                    <tr>
                        <td>Answered: </td><td><?php echo $answered; ?> (<?php echo Util::percent($answered,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unanswered: </td><td><?php echo $unanswered; ?> (<?php echo Util::percent($unanswered,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Busy: </td><td><?php echo $busy; ?> (<?php echo Util::percent($busy,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unique: </td><td><?php echo $unique; ?> (<?php echo Util::percent($unique,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Unique All Time: </td><td><?php echo $unique_at; ?> (<?php echo Util::percent($unique_at,$calls_at); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Failed Calls: </td><td><?php echo $failed; ?> (<?php echo Util::percent($failed,count($calls)); ?>%)</td>
                    </tr>
                    <tr>
                        <td>Total Minutes: </td><td><?php if($seconds!=0) echo round($seconds/60); else echo 0; ?> minute(s)</td>
                    </tr>
                    <tr>
                        <td>Avg. Duration: </td><td><?php if($seconds!=0) echo round(($seconds/60)/count($calls),1); else echo 0; ?> minute(s)</td>
                    </tr>
                </table>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->
    </div>

    <div style="float: right; width: 49%;">
        <div class="block" style="width: 100%; height: 364px; float:right; margin-bottom: 20px;">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>

                <h2>
                    Overview
                </h2>
            </div>      <!-- .block_head ends -->

            <div class="block_content">

                <?php
                $calls = $db->getCallsInRange($company,$start_date,$end_date,$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);

                $total_calls = 0;

                if( is_array($calls) || is_object($calls) ) {
                    foreach($calls as $call)
                    {
                        $callTo = $db->format_phone_db($call['CallTo']);

                        foreach ($campaigns as $name=>$campaign) {
                            if (in_array($callTo, $campaign["numbers"])) {
                                $campaigns[$name]["calls_total"] = $campaigns[$name]["calls_total"] + 1;
                                $total_calls++;
                            }
                        }
                    }
                }
                ?>

                <div style="height: 300px; overflow-y: auto;">
                    <table cellpadding="0" cellspacing="0" width="100%" class="sortable" id="stats_table">
                        <?php foreach ($campaigns as $name=>$campaign) { 
                            $calls_total = $campaign["calls_total"];
                            $calls_percent = @($calls_total/$total_calls) * 100;
                            ?>
                            <tr>
                                <td><?php echo $name; ?></td>
                                <td><?php echo $calls_total; ?> (<?php echo number_format($calls_percent, '2', '.', ','); ?>%)</td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div> 
    </div>

    <div style="clear: both;"></div>

    <?php
    $i=0;
    foreach ($campaigns as $name=>$campaign) {
        $i++;
        ?>

        <div style="<?php if ($i%2 == 1) echo "float: left;"; else echo "float: right;"; ?> width: 49%;">
            <div class="block" style="width: 100%; height: 364px; margin-bottom: 20px;">

                <div class="block_head">
                    <div class="bheadl"></div>
                    <div class="bheadr"></div>

                    <h2>
                        <?php echo $name; ?>
                    </h2>
                </div>      <!-- .block_head ends -->

                <div class="block_content">
                    
                    <div id="<?php echo str_replace(" ", "_", strtolower($name)); ?>" style="width: 100%; height: 300px; position: relative; "></div>

                </div>      <!-- .block_content ends -->

                <div class="bendl"></div>
                <div class="bendr"></div>
            </div> 
        </div>

        <?php if ($i%2 == 0) { ?>
            <div style="clear: both;"></div>
        <?php } ?>

    <?php } ?>

    
<?php } ?>

<?php if($report_type=="default") { ?>
        <div class="block">

            <div class="block_head">
                <div class="bheadl"></div>
                <div class="bheadr"></div>
                <?php
                
                ?>

                <h2>
                  <?php echo $call_count; ?> Calls from '<a href="#"><?php echo $date_start->format("D n\/j Y"); ?></a>' to '<a href="#"><?php echo $date_end->format("D n\/j Y"); ?></a>'.
                </h2>
                <div style="float:right;"><a href="#" id="export_btn"><img src="images/excel_img.png" width="50" alt="Export as CSV" title="Export as CSV" /></a></div>
            </div>      <!-- .block_head ends -->

            <div class="block_content">
                <iframe id="export" src="" style="display:none;"></iframe>

                <?php if (!empty($agent)) {
                    $result2 = $db->getOutgoingCallReport($_SESSION['user_id'],$start_date,$end_date,$company,$call_result,$selected_phone_code,$pagination,$user,$report_campaign, $agent);
                    $total_talk_time_outbound = $result2[2];
                    ?>
                    <div style="padding: 5px; border-bottom: solid 1px #CCCCCC;">
                        Agent: <strong><?php echo $agent; ?></strong>
                        
                        &nbsp;&nbsp;

                        <?php
                        $inbound_calls = $db->getCallsInRangeCount($company,$start_date,$end_date,$selected_phone_code,$call_result,false,$outgoing_number,false,"",$report_campaign,$agent);
                        $outbound_calls = $db->getCallsInRangeCount($company,$start_date,$end_date,$selected_phone_code,$call_result,false,$outgoing_number,true,"",$report_campaign,$agent);
                        $total_calls = $outbound_calls + $inbound_calls;
                        ?>
                        Total Calls: <strong><?php echo $total_calls; ?></strong>

                        |

                        Inbound: <strong><?php echo $inbound_calls; ?></strong>

                        |

                        Outbound: <strong><?php echo $outbound_calls; ?></strong>

                        &nbsp;&nbsp;

                        Total Talk Time: <strong><?php echo Util::formatTime(($total_talk_time_inbound + $total_talk_time_outbound)); ?></strong>

                        |

                        Inbound: <strong><?php echo Util::formatTime($total_talk_time_inbound); ?></strong>

                        |

                        Outbound: <strong><?php echo Util::formatTime($total_talk_time_outbound); ?></strong>
                    </div>
                <?php } ?>

                <table cellpadding="0" cellspacing="0" width="100%">

                    <thead>
                    <tr>
                        <?php
                        $columns = array(
                            "DateCreated" => "Date",
                            "Campaign" => "Campaign",
                            "CallFrom" => "From",
                            "CallTo" => "To",
                            "CityState" => "City, State",
                            "DialCallDuration" => "Duration",
                            "DialCallStatus" => "Status",
                        );

                        foreach ($columns as $key => $value) {
                            if ($order_by == $key) { if ($order_type == "ASC") { $next_sort_type = "DESC"; $class = "headerSortDown"; } else { $next_sort_type = "ASC"; $class = "headerSortUp"; } } else { $next_sort_type = "ASC"; $class = ""; }

                            $url = Util::curPageURL();
                            $parsed_url = parse_url($url);
                            parse_str($parsed_url["query"], $output);
                            $output["order_by"] = $key;
                            $output["order_type"] = $next_sort_type;

                            $url = $parsed_url['scheme']."://".$parsed_url["host"].$parsed_url['path']."?".http_build_query($output);
                            if ($key == "Campaign") {
                                ?>
                                    <th><?php echo $value; ?></div></td>
                                <?php
                            }
                            else {
                                ?>
                                    <th style="cursor: pointer;" onclick="window.location = '<?php echo $url;?>';" class="header <?php echo $class; ?>"><?php echo $value; ?></div></td>
                                <?php
                            }
                        }
                        ?>
                        <?php if($RECORDINGS) { ?>
                        <th>Recording</th>
                        <?php } ?>
                    </tr>
                    </thead>

                    <tbody>

                        <?php
                        $twilio_numbers=Util::get_all_twilio_numbers();

                        $all_phone_codes = array();

                        $pagination->setTotalRecords($result[1]);
                        if( is_array($result[0]) || is_object($result[0]) )
                            foreach ($result[0] as $call) {
                                if(!Util::isNumberIntl($call['CallTo']))
                                {
                                    $company_id = $db->getCompanyOfNumber($db->format_phone_db($call['CallTo']));
                                }else{
                                    $company_id = $db->getCompanyOfNumber(substr($call['CallTo'],1,(strlen($call['CallTo'])-1)));
                                }
                                $company_name = $db->getCompanyName($company_id);
                                $all_phone_codes[$company_id] = $db->getPhoneCodes($company_id);

                                if (array_key_exists(format_phone($call['CallTo']), $twilio_numbers))
                                    $campaign = $twilio_numbers[format_phone($call['CallTo'])];
                                else
                                    $campaign = "";

                                $tz = new DateTimeZone($TIMEZONE);
                                $date = new DateTime($call['DateCreated']);
                                $date->setTimezone($tz);

                                if ($db->getVar("mask_recordings") == "true") {
                                    $call['RecordingUrl'] = Util::maskRecordingURL($call['RecordingUrl']);
                                }

                                //echo "<!-- ".var_dump($report_campaign). " -->";
                            ?>
                        <tr>
                            <td><span style="display:none;"><?php echo $date->format("U"); ?></span><?php echo "<a href=\"call_detail.php?id=".$call['CallSid']."\" title='Click for call details.'>" . $date->format("D n\/j Y g\:iA"). "</a>"; ?>
                                <?php if(@$_REQUEST['cm']=="-1") { ?>
                                <br><span style="font: 11px Verdana bold;"><?php echo $company_name; ?></span>
                                <?php } ?>
                            </td>
                            <td><?php echo $campaign == "callTracking"? "Pooled Call" : $campaign; ?></td>
                            <td style="white-space: nowrap;"<?php if($db->isNumberBlocked($company_id,$call['CallFrom'])) echo " style=\"text-decoration: line-through;\" title=\"This number has been blacklisted.\""; ?>><?php echo format_phone($db->format_phone_db($call['CallFrom'])); ?></td>
                            <td style="white-space: nowrap;"><?php echo format_phone($db->format_phone_db($call['CallTo'])); ?></td>
                            <td>
                                <?php
                                if($call['FromCity'] == "" && $call['FromState'] == "")
                                    echo "N/A";
                                elseif($call['FromCity']=="")
                                    echo $call['FromState'];
                                else
                                    echo $call['FromCity'].", ".$call['FromState'];
                                ?>
                            </td>
                            <td><?php echo Util::formatTime($call['DialCallDuration']); ?></td>
                            <td><?php echo $call['DialCallStatus']; ?></td>
                            <?php if($RECORDINGS && $db->isCompanyRecordingDisabled($company_id)==false) { ?>
                                <?php if ($call['RecordingUrl']!="") { ?>
                                <td><?php echo Util::generateFlashAudioPlayer($call['RecordingUrl'],"sm"); ?>
                                    <?php if($_SESSION['permission'] > 0 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])) { ?>
                                    <select style="margin-top: 3px;width: 120px;" class="sid_<?php echo $call['CallSid']; ?>" onchange="$(this).attr('disabled','true'); changePhoneCode('<?php echo $call['CallSid']; ?>',this.options[this.selectedIndex].value,(function(){$('.sid_<?php echo $call['CallSid']; ?>').removeAttr('disabled');}))">
                                        <?php
                                        if($call['PhoneCode']==0)
                                            echo "<option value='0' selected>None</option>";
                                        else
                                            echo "<option value='0'>None</option>";

                                        if(is_array($all_phone_codes[$company_id]) || is_object($all_phone_codes[$company_id]))
                                            foreach($all_phone_codes[$company_id] as $code)
                                            {
                                                if($call['PhoneCode']==$code->idx)
                                                    echo "<option value='$code->idx' selected>$code->name</option>";
                                                else
                                                    echo "<option value='$code->idx'>$code->name</option>";
                                            }
                                        ?>
                                    </select>
                                        <?php }else{ ?>
                                        <strong>
                                            <?php
                                            if($call['PhoneCode']==0)
                                                echo "None";
                                            else{
                                                foreach($all_phone_codes[$company_id] as $selected_phone_code){
                                                    if($call['PhoneCode']==$selected_phone_code->idx)
                                                        echo $selected_phone_code->name;
                                                }
                                            }
                                            ?>
                                        </strong>
                                        <?php } ?>
                                    <div style="display: inline-block !important;"><a href="#" data-companyid="<?php echo $company_id; ?>" data-params="<?php echo $call['CallFrom']; ?>" id="USR_ADD_NUM_TO_BLACKLIST"><img style="vertical-align: text-top;" src="images/blacklist_btn.png" alt="Add to blacklist" title="Add number to Blacklist"></a>
                                        <a href="#" id="USR_ADD_NOTE_FROM_LIST" title="Add Note" data-callsid="<?php echo $call['CallSid']; ?>"><img src="images/comment-edit-icon.png" style="width:16px; vertical-align: text-top;"></a></div>
                                </td>
                                <?php }else{ ?>
                                <td>
                                    <?php if($_SESSION['permission'] > 0 || $db->isUserAbleToSetPhoneCodes($_SESSION['user_id'])) { ?>
                                    <select title="Select a Phone Code" style="margin-top: 3px; width: 120px;" class="sid_<?php echo $call['CallSid']; ?>" onchange="$(this).attr('disabled','true'); changePhoneCode('<?php echo $call['CallSid']; ?>',this.options[this.selectedIndex].value,(function(){$('.sid_<?php echo $call['CallSid']; ?>').removeAttr('disabled');}))">
                                        <?php
                                        if($call['PhoneCode']==0)
                                            echo "<option value='0' selected>None</option>";
                                        else
                                            echo "<option value='0'>None</option>";
                                        foreach($all_phone_codes[$company_id] as $code)
                                        {
                                            if($call['PhoneCode']==$code->idx)
                                                echo "<option value='$code->idx' selected>$code->name</option>";
                                            else
                                                echo "<option value='$code->idx'>$code->name</option>";
                                        }
                                        ?>
                                    </select>
                                    <?php }else{ ?>
                                    <strong>
                                        <?php
                                        if($call['PhoneCode']==0)
                                            echo "None";
                                        else{
                                            foreach($all_phone_codes[$company_id] as $selected_phone_code){
                                                if($call['PhoneCode']==$selected_phone_code->idx)
                                                    echo $selected_phone_code->name;
                                            }
                                        }
                                        ?>
                                    </strong>
                                    <?php } ?>
                                    <div style="display: inline-block !important;"><a href="#" data-companyid="<?php echo $company_id; ?>" data-params="<?php echo $call['CallFrom']; ?>" id="USR_ADD_NUM_TO_BLACKLIST"><img style="vertical-align: text-top;" src="images/blacklist_btn.png" alt="Add to blacklist" title="Add number to Blacklist"></a>
                                        <a href="#" id="USR_ADD_NOTE_FROM_LIST" title="Add Note" data-callsid="<?php echo $call['CallSid']; ?>"><img src="images/comment-edit-icon.png" style="width:16px; vertical-align: text-top;"></a></div>
                                </td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                            <?php  ?>
                        <?php } ?>
                </table>

                <?php $navigation = $pagination->create_links();
                echo $navigation; ?>

            </div>      <!-- .block_content ends -->

            <div class="bendl"></div>
            <div class="bendr"></div>
        </div>      <!-- .block ends -->

<?php } ?>


<?php } ?>


<?php include "include/footer.php"; ?>
    <script type="text/javascript">
        $(document).ready(function(e){
            $("#export_btn").click(function(e){
                $("#export").attr("src","include/excel_export_record.php"+window.location.search);
            });

            <?php
                if($display_report&&$report_type=="default")
                {
                    ?>
                    //$("th.header:first").trigger("click").trigger("click");
                    <?php
                }
            ?>

            <?php if(!$display_report) { ?>
            if($("#company_select").val() != "none")
            {
                companySelected($("#company_select").val(),function(){
                    var phone_code = getCookie2("selectedPhoneCode");
                    var outgoing_number = getCookie2("selectedOutgoingNumber");
                    var campaign = getCookie2("selectedCampaign");
                    if(!phone_code)
                        phone_code = 0;
                    else{
                        //console.log(phone_code);
                        $("#phone_code").find("option[value='"+phone_code+"']").attr("selected","selected");
                        if(outgoing_number)
                            $("#outgoing_number").find("option[value='"+outgoing_number+"']").attr("selected","selected");
                        if(campaign)
                            $("#campaign").find("option[value='"+campaign+"']").attr("selected","selected");
                    }
                });
            }
            <?php } ?>

            $("#charts").block({
                message: '<h1 style="color:#fff">Loading...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .7,
                    color: '#fff !important'
                }
            });

        });
    </script>
<script type="text/javascript">
    setCookie("selectedPhoneCode","<?php echo @$_GET['pc']; ?>",1);
    setCookie("selectedOutgoingNumber","<?php echo @$_GET['on']; ?>",1);
    setCookie("selectedCampaign","<?php echo @$_GET['ca']; ?>",1);
</script>
</body>
</html>