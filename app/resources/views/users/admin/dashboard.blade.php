@extends('layouts.main')
@section('content')
	<ol class="breadcrumb">
		<li><i class="fa fa-home"></i></li>
	</ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Dashboard</strong></h3>
	</div>

    <div class="pull-right">
        <a class="btn btn-{{($only_me) ? 'default' : 'success'}} m-t-10 tooltips" data-original-title="Group Metrics" href="/dashboard"><i class="fa fa-users"></i></a>
        <a class="btn btn-{{($only_me) ? 'success' : 'default'}} m-t-10 tooltips" data-original-title="Individual Metrics" href="/dashboard/me"><i class="fa fa-user"></i></a>
    </div>

	<div class="clearfix"></div>

	<div class="row m-t-20">
        <div class="col-md-3 col-sm-12">
            <div class="panel no-bd bd-3 panel-stat">
                <div class="panel-body bg-blue p-15">
                    <div class="row m-b-10">
                        <div class="col-xs-3">
                            <i class="fa fa-briefcase" style="font-size: 45px;"></i>
                        </div>
                        <div class="col-xs-9">
                            <div class="" data-direction="vertical" data-delay="3500" data-height="56" style="height: 56px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <small class="stat-title">Clients</small>
                                    <?php
                                    $clients = ($only_me) ? \Client::where("user_id", \Auth::user()->id)->count() : \Client::count();
                                    ?>
                                    <h1 class="m-0 w-300">{{$clients}}</h1>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $this_month = \Client::where("created_at", ">=", date("Y-m-d H:i:s", strtotime("first day of this month")));
                    if ($only_me) $this_month = $this_month->where("user_id", \Auth::user()->id);
                    $this_month = $this_month->count();
                    $percentage = ($clients == 0) ? 0.00 : number_format($this_month/$clients * 100, '2', '.', ',');
                    ?>
                    <div class="row">
                        <div class="col-xs-6">
                            <small class="stat-title">This Month</small>
                            <div class="" data-direction="vertical" data-delay="3500" data-height="30" style="height: 30px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <h3 class="m-0 w-300">{{$this_month}}</h3>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <small class="stat-title">Increase</small>
                            <div class="" data-direction="vertical" data-delay="3500" data-height="30" style="height: 30px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <h3 class="m-0 w-500">{{$percentage}}%</h3>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12">
            <div class="panel no-bd bd-3 panel-stat">
                <div class="panel-body bg-red p-15">
                    <div class="row m-b-10">
                        <div class="col-xs-3">
                            <i class="fa fa-plus-square-o" style="font-size: 45px;"></i>
                        </div>
                        <div class="col-xs-9">
                            <div class="" data-direction="vertical" data-delay="3500" data-height="56" style="height: 56px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <small class="stat-title">Providers</small>
                                    <?php
                                    $providers = ($only_me) ? \Provider::where("user_id", \Auth::user()->id)->count() : \Provider::count();
                                    ?>
                                    <h1 class="m-0 w-300">{{$providers}}</h1>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $this_month = \Provider::where("created_at", ">=", date("Y-m-d H:i:s", strtotime("first day of this month")));
                    if ($only_me) $this_month = $this_month->where("user_id", \Auth::user()->id);
                    $this_month = $this_month->count();
                    $percentage = ($providers == 0) ? 0.00 : number_format($this_month/$providers * 100, '2', '.', ',');
                    ?>
                    <div class="row">
                        <div class="col-xs-6">
                            <small class="stat-title">This Month</small>
                            <div class="" data-direction="vertical" data-delay="3500" data-height="30" style="height: 30px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <h3 class="m-0 w-300">{{$this_month}}</h3>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <small class="stat-title">Increase</small>
                            <div class="" data-direction="vertical" data-delay="3500" data-height="30" style="height: 30px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <h3 class="m-0 w-500">{{$percentage}}%</h3>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12">
            <div class="panel no-bd bd-3 panel-stat">
                <div class="panel-body bg-green p-15">
                    <div class="row m-b-10">
                        <div class="col-xs-3">
                            <i class="fa fa-file-archive-o" style="font-size: 45px;"></i>
                        </div>
                        <div class="col-xs-9">
                            <div class="" data-direction="vertical" data-delay="3500" data-height="56" style="height: 56px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <small class="stat-title">Cases</small>
                                    <?php
                                    $cases = ($only_me) ? \ECase::where("user_id", \Auth::user()->id)->count() : \ECase::count();
                                    ?>
                                    <h1 class="m-0 w-300">{{$cases}}</h1>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $this_month = \ECase::where("created_at", ">=", date("Y-m-d H:i:s", strtotime("first day of this month")));
                    if ($only_me) $this_month = $this_month->where("user_id", \Auth::user()->id);
                    $this_month = $this_month->count();
                    $percentage = ($cases == 0) ? 0.00 : number_format($this_month/$cases * 100, '2', '.', ',');
                    ?>
                    <div class="row">
                        <div class="col-xs-6">
                            <small class="stat-title">This Month</small>
                            <div class="" data-direction="vertical" data-delay="3500" data-height="30" style="height: 30px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <h3 class="m-0 w-300">{{$this_month}}</h3>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <small class="stat-title">Increase</small>
                            <div class="" data-direction="vertical" data-delay="3500" data-height="30" style="height: 30px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <h3 class="m-0 w-500">{{$percentage}}%</h3>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12">
            <div class="panel no-bd bd-3 panel-stat">
                <div class="panel-body bg-dark p-15">
                    <div class="row m-b-10">
                        <div class="col-xs-3">
                            <i class="fa fa-archive" style="font-size: 45px;"></i>
                        </div>
                        <div class="col-xs-9">
                            <div class="" data-direction="vertical" data-delay="3500" data-height="56" style="height: 56px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <small class="stat-title">Claims</small>
                                    <?php
                                    $claims = ($only_me) ? \Claim::where("user_id", \Auth::user()->id)->count() : \Claim::count();
                                    ?>
                                    <h1 class="m-0 w-300">{{$claims}}</h1>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $this_month = \Claim::where("created_at", ">=", date("Y-m-d H:i:s", strtotime("first day of this month")));
                    if ($only_me) $this_month = $this_month->where("user_id", \Auth::user()->id);
                    $this_month = $this_month->count();
                    $percentage = ($claims == 0) ? 0.00 : number_format($this_month/$claims * 100, '2', '.', ',');
                    ?>
                    <div class="row">
                        <div class="col-xs-6">
                            <small class="stat-title">This Month</small>
                            <div class="" data-direction="vertical" data-delay="3500" data-height="30" style="height: 30px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <h3 class="m-0 w-300">{{$this_month}}</h3>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <small class="stat-title">Increase</small>
                            <div class="" data-direction="vertical" data-delay="3500" data-height="30" style="height: 30px;">
                                <div class="" style="transition-property: transform; transition-duration: 500ms; transition-timing-function: ease; transform: translate(0%, 0%) translateZ(0px);">
                                    <h3 class="m-0 w-500">{{$percentage}}%</h3>
                                <a class="" href="#" style="left: 0%;"></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="graph-wrapper">
                                <div class="graph-info p-r-10">
                                    <a href="javascript:void(0)" class="btn bg-blue">Clients</a>
                                    <a href="javascript:void(0)" class="btn bg-red">Providers</a>
                                    <a href="javascript:void(0)" class="btn bg-green">Cases</a>
                                    <a href="javascript:void(0)" class="btn bg-dark">Claims</a>
                                </div>
                                <div class="">
                                    <div class="" id="graph-lines" style="width:100%; height: 336px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="margin-top: 0px;">Clients &amp; Providers Map</h3>
                            <div id="map" style="width: 100%; height: 346px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <h3>Quick Links</h3>

                    <br /><br />
					<div class="text-center">
	                    <a href="/clients/add-client" class="btn btn-success"><i class="fa fa-plus"></i> Add Client</a>
	                    &nbsp;&nbsp;&nbsp;
	                    <a href="/providers/add-provider" class="btn btn-success"><i class="fa fa-plus"></i> Add Provider</a>
	                    &nbsp;&nbsp;&nbsp;
	                    <a href="/cases/add-case" class="btn btn-success"><i class="fa fa-plus"></i> Add Case</a>
	                    &nbsp;&nbsp;&nbsp;
	                    <a href="/claims/add-claim" class="btn btn-success"><i class="fa fa-plus"></i> Add Claim</a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="/clients/add-employee" class="btn btn-success"><i class="fa fa-plus"></i> Add Enrollee</a>
	                </div>
	                <br /><br />
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        <?php
        $ticks2 = array();
        for ($i=0; $i<12; $i++) {
            $ticks_nr = ($only_me)
                                ? \Client::where("user_id", \Auth::user()->id)->where("created_at", "LIKE", date("Y-m", strtotime("now -".(11-$i)."month"))."%")->count()
                                : \Client::where("created_at", "LIKE", date("Y-m", strtotime("now -".(11-$i)."month"))."%")->count();
            $ticks2[] = array($i, $ticks_nr);
        }
        ?>

        var d2 = {!! json_encode($ticks2) !!};

        <?php
        $ticks3 = array();
        for ($i=0; $i<12; $i++) {
            $ticks_nr = ($only_me)
                                ? \Provider::where("user_id", \Auth::user()->id)->where("created_at", "LIKE", date("Y-m", strtotime("now -".(11-$i)."month"))."%")->count()
                                : \Provider::where("created_at", "LIKE", date("Y-m", strtotime("now -".(11-$i)."month"))."%")->count();
            $ticks3[] = array($i, $ticks_nr);
        }
        ?>

        var d3 = {!! json_encode($ticks3) !!};

        <?php
        $ticks4 = array();
        for ($i=0; $i<12; $i++) {
            $ticks_nr = ($only_me)
                                ? \ECase::where("user_id", \Auth::user()->id)->where("created_at", "LIKE", date("Y-m", strtotime("now -".(11-$i)."month"))."%")->count()
                                : \ECase::where("created_at", "LIKE", date("Y-m", strtotime("now -".(11-$i)."month"))."%")->count();
            $ticks4[] = array($i, $ticks_nr);
        }
        ?>

        var d4 = {!! json_encode($ticks4) !!};

        <?php
        $ticks5 = array();
        for ($i=0; $i<12; $i++) {
            $ticks_nr = ($only_me)
                                ? \Claim::where("user_id", \Auth::user()->id)->where("created_at", "LIKE", date("Y-m", strtotime("now -".(11-$i)."month"))."%")->count()
                                : \Claim::where("created_at", "LIKE", date("Y-m", strtotime("now -".(11-$i)."month"))."%")->count();
            $ticks5[] = array($i, $ticks_nr);
        }
        ?>

        var d5 = {!! json_encode($ticks5) !!};

        $(document).ready(function() {
            buildChart();
        });

        $(window).resize(function() {
            buildChart();
        });

        function buildChart() {
            $("#graph-lines").html('');
            /****  Line Chart  ****/
                var graph_lines = [
                {
                    label: "Clients",
                    data: d2,
                    lines: {
                        lineWidth: 2,
                    },
                    animator: {
                        steps: 300,
                        duration: 1000,
                        start: 0
                    },
                    color: '#0090D9'
                }, {
                    label: "Clients",
                    data: d2,
                    points: {
                        show: true,
                        fill: true,
                        radius: 6,
                        fillColor: "#0090D9",
                        lineWidth: 3
                    },
                    color: '#fff'
                },

                {
                    label: "Providers",
                    data: d3,
                    lines: {
                        lineWidth: 2,
                    },
                    animator: {
                        steps: 300,
                        duration: 1000,
                        start: 0
                    },
                    color: '#C75757'
                }, {
                    label: "Providers",
                    data: d3,
                    points: {
                        show: true,
                        fill: true,
                        radius: 6,
                        fillColor: "#C75757",
                        lineWidth: 3
                    },
                    color: '#fff'
                },

                {
                    label: "Cases",
                    data: d4,
                    lines: {
                        lineWidth: 2,
                    },
                    animator: {
                        steps: 300,
                        duration: 1000,
                        start: 0
                    },
                    color: '#18a689'
                }, {
                    label: "Cases",
                    data: d4,
                    points: {
                        show: true,
                        fill: true,
                        radius: 6,
                        fillColor: "#18a689",
                        lineWidth: 3
                    },
                    color: '#fff'
                },

                {
                    label: "Claims",
                    data: d5,
                    lines: {
                        lineWidth: 2,
                    },
                    animator: {
                        steps: 300,
                        duration: 1000,
                        start: 0
                    },
                    color: '#2B2E33'
                }, {
                    label: "Claims",
                    data: d5,
                    points: {
                        show: true,
                        fill: true,
                        radius: 6,
                        fillColor: "#2B2E33",
                        lineWidth: 3
                    },
                    color: '#fff'
                }, ];

            <?php
            $ticks = array();
            for ($i=0; $i<12; $i++) {
                $ticks[] = array($i, date("M Y", strtotime("now -".(11-$i)."month")));
            }
            ?>

            var ticks = {!! json_encode($ticks) !!};

            var line_chart = $.plotAnimator($('#graph-lines'), graph_lines, {
                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    min: 0,
                    ticks: ticks,
                    font: {
                        lineHeight: 12,
                        weight: "bold",
                        family: "Open sans",
                        color: "#8D8D8D"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f3f3f3",
                    font: {
                        lineHeight: 13,
                        weight: "bold",
                        family: "Open sans",
                        color: "#8D8D8D"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderColor: "transparent",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 15,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 4
                },
                legend: {
                    show: false
                }
            });
        }

        $("#graph-lines").bind("plothover", function (event, pos, item) {
            console.log(item);
                $("#x").text(pos.x.toFixed(0));
                $("#y").text(pos.y.toFixed(0));
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $("#flot-tooltip").remove();
                        var x = item.datapoint[0].toFixed(0),
                            y = item.datapoint[1].toFixed(0);
                        showTooltip(item.pageX, item.pageY, ticks[item.dataIndex][1] + ": " + y + " " + item.series.label);
                    }
                } else {
                    $("#flot-tooltip").remove();
                    previousPoint = null;
                }
            });

        function showTooltip(x, y, contents) {
            $('<div id="flot-tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y + 5,
                left: x + 5,
                color: '#fff',
                padding: '2px 5px',
                'background-color': '#717171',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
        };
	</script>



    <style type="text/css">
        a[href^="http://maps.google.com/maps"]{display:none !important}
        a[href^="https://maps.google.com/maps"]{display:none !important}

        .gmnoprint a, .gmnoprint span, .gm-style-cc {
            display:none;
        }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZWqW7aJrwxQ4MeN10zXxap5a3lHl4iAI&sensor=true"></script>
    <script src="/js/markerclusterer.js"></script>

    <script>
        jQuery(document).ready(function () {
            var map;
            var geocoder;

            var openIW;

            geocoder = new google.maps.Geocoder();

             var customMapType = new google.maps.StyledMapType([
                  {
                    elementType: 'labels',
                    stylers: [{"hue":"#0066ff"},{"saturation":74},{"lightness":100}],
                  }
                ], {
                  name: 'Custom Style'
              });
              var customMapTypeId = 'custom_style';

            var centerPosition = new google.maps.LatLng(38.850033, -97.6500523);

            var options = {
                zoom: 3,
                center: centerPosition,
                styles: [
                    {"featureType":"water","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},
                    {"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},
                    {"featureType":"road","stylers":[{"saturation":-100},
                    {"lightness":45}]},
                    {"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},
                    {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
                    {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
                    {"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},
                    {"featureType":"administrative", "elementType": "labels","stylers":[{"visibility":"off"}]}
                ],
                 mapTypeId: google.maps.MapTypeId.ROADMAP,
                 mapTypeControl: false
            };

            map = new google.maps.Map($('#map')[0], options);

            /* Clients */
            var markers = [];
            <?php
            if ($only_me)
                $clients = \Client::where("user_id", \Auth::user()->id)->get();
            else
                $clients = \Client::all();

            foreach ($clients as $key => $client)
                $clients[$key]->address = $client->getPrimaryAddress();

            $lats = array();
            ?>

            @foreach ($clients as $key => $client)
                @if (!empty($client->lat))
                    <?php
                    if (in_array($client->lat, $lats))
                        continue;
                    ?>
                    var latLng = new google.maps.LatLng({{$client->lat}}, {{$client->lng}});
                    var marker_{{$key}} = new google.maps.Marker({
                        position: latLng,
                        icon: {
                            url: "{{URL::to('/')}}/images/icons/briefcase.png"
                        }
                    });

                      var infowindow_{{$key}} = new google.maps.InfoWindow({
                        content: '<strong>{{$client->name}}</strong><br />\
                            Location: {{$client->getPrimaryAddress()}}'
                      });
                      marker_{{$key}}.addListener('click', function() {
                        if (typeof(openIW) != "undefined")
                            openIW.close();

                        infowindow_{{$key}}.open(map, marker_{{$key}});
                        openIW = infowindow_{{$key}};
                      });
                    markers.push(marker_{{$key}});

                    <?php $lats[] = $client->lat; ?>
                @endif
            @endforeach

            var clients = {!! json_encode($clients) !!};

            var client_nr = 0;
            setTimeout(function() {
                getAddress(clients[client_nr].address);
            }, 1000);

            var infowindows = [];

            function getAddress(address) {
                if (clients[client_nr].lat != "") {
                    client_nr++;
                    if (typeof(clients[client_nr]) == "undefined") return;

                    getAddress(clients[client_nr].address);
                }
                else {
                    geocoder.geocode( { 'address': address}, function(results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                        $.post("/clients/save-lat-lng", {
                            client_nr: client_nr,
                            lat: results[0].geometry.location.lat(),
                            lng: results[0].geometry.location.lng(),
                            only_me: {{($only_me) ? 1 : 0}}
                        }, function() {}, "json");

                        client_nr++;
                        if (typeof(clients[client_nr]) == "undefined") return;

                        setTimeout(function() {
                            getAddress(clients[client_nr].address);
                        }, 1000);
                      } else {
                        if (status != "OVER_QUERY_LIMIT")
                            getAddress(address);
                        else {
                            client_nr++;
                            if (typeof(clients[client_nr]) == "undefined") return;

                            setTimeout(function() {
                                getAddress(clients[client_nr].address);
                            }, 1000);
                        }
                        //alert('Geocode was not successful for the following reason: ' + status);
                      }
                    });
                }
              }

            var markerCluster = new MarkerClusterer(map, markers, {
                averageCenter: true,
                // gridSize: 30
            });

            google.maps.event.addListener(markerCluster, "click", function (cluster) {
                console.log("Cluster click");
            });
            /* Clients */
            

            /* Providers */
            var markers_p = [];
            <?php
            if ($only_me)
                $providers = \Provider::where("user_id", \Auth::user()->id)->get();
            else
                $providers = \Provider::all();

            foreach ($providers as $key => $provider)
                $providers[$key]->address = $provider->getPrimaryAddress();

            $lats = array();
            ?>

            @foreach ($providers as $key => $provider)
                @if (!empty($provider->lat))
                    <?php
                    if (in_array($provider->lat, $lats))
                        continue;
                    ?>
                    var latLng = new google.maps.LatLng({{$provider->lat}}, {{$provider->lng}});
                    var marker_p_{{$key}} = new google.maps.Marker({
                        position: latLng,
                        icon: {
                            url: "{{URL::to('/')}}/images/icons/medplus.png"
                        }
                    });

                      var infowindow_p_{{$key}} = new google.maps.InfoWindow({
                        content: '<strong>{{$provider->name}}</strong><br />\
                            Location: {{$provider->getPrimaryAddress()}}'
                      });
                      marker_p_{{$key}}.addListener('click', function() {
                        if (typeof(openIW) != "undefined")
                            openIW.close();

                        infowindow_p_{{$key}}.open(map, marker_p_{{$key}});
                        openIW = infowindow_p_{{$key}};
                      });
                    markers_p.push(marker_p_{{$key}});

                    <?php $lats[] = $provider->lat; ?>
                @endif
            @endforeach

            var providers = {!! json_encode($providers) !!};

            var provider_nr = 0;
            setTimeout(function() {
                getAddressP(providers[provider_nr].address);
            }, 1000);

            var infowindows = [];

            function getAddressP(address) {
                if (providers[provider_nr].lat != "") {
                    provider_nr++;
                    if (typeof(providers[provider_nr]) == "undefined") return;

                    getAddressP(providers[provider_nr].address);
                }
                else {
                    geocoder.geocode( { 'address': address}, function(results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                        $.post("/providers/save-lat-lng", {
                            provider_nr: provider_nr,
                            lat: results[0].geometry.location.lat(),
                            lng: results[0].geometry.location.lng(),
                            only_me: {{($only_me) ? 1 : 0}}
                        }, function() {}, "json");

                        provider_nr++;
                        if (typeof(providers[provider_nr]) == "undefined") return;

                        setTimeout(function() {
                            getAddressP(providers[provider_nr].address);
                        }, 1000);
                      } else {
                        if (status != "OVER_QUERY_LIMIT")
                            getAddressP(address);
                        else {
                            provider_nr++;
                            if (typeof(providers[provider_nr]) == "undefined") return;

                            setTimeout(function() {
                                getAddressP(providers[provider_nr].address);
                            }, 1000);
                        }
                        //alert('Geocode was not successful for the following reason: ' + status);
                      }
                    });
                }
              }

            var markerClusterP = new MarkerClusterer(map, markers_p, {
                averageCenter: true,
                // styles: [{
                //    textColor: "#FF0000",
                //     height: 27,
                //     width: 35
                // }],
                // gridSize: 30
            });

            google.maps.event.addListener(markerClusterP, "click", function (cluster) {
                console.log("Cluster click");
            });
            /* Providers */
        });
    </script>
@stop