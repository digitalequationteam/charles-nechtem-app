<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head></head>
    <body>
    	Dear <?php echo $first_name; ?>,
    	<br />
    	<br />
    	You have requested to reset your password. To do so, please click on the link below and you will be able to create a new password for your account:
    	<br />
    	<br />
    	<a href="<?php echo $link; ?>">Click Here</a>
    	<br />
    	<br />
    	Thank you,
    	<br />
    	<?php echo $signature; ?>
    </body>
</html>