<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head></head>
    <body>
    	Dear <?php echo $first_name; ?>,
    	<br />
    	<br />
    	The admin has set a temporary password on your account. When you are ready, click the link below to reset the password with your chosen one.
    	<br />
    	<br />
    	<a href="<?php echo $link; ?>">Click Here</a>
    	<br />
    	<br />
    	Thank you,
    	<br />
    	<?php echo $signature; ?>
    </body>
</html>