<?php
$path = explode(SERVER_HOST, Request::url())[1];
?>
@foreach (\Menu::where("user_type", Auth::user()->Owner()->type)->where("hidden", 0)->where("category", "menu")->where("parent_id", 0)->orderBy("order", "ASC")->get() as $key => $menu_item)
    <?php
    $submenu_items = \Menu::where("user_type", Auth::user()->Owner()->type)->where("hidden", 0)->where("category", "menu")->where("parent_id", $menu_item->id)->orderBy("order", "ASC");
    ?>
    @if (Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key) || !$menu_item->require_permission)
    <li class="@if (strpos($path, $menu_item->getSidebarURL()) !== false) current active @endif @if ($submenu_items->count() > 0) mm-dropdown @endif">
        <a @if ($menu_item->key == "support") href="#live_help" data-toggle="modal" @elseif ($submenu_items->count() > 0) href="javascript: void(0);" @else href="{{$menu_item->getSidebarURL()}}" @endif><i class="menu-icon fa {{$menu_item->icon_class}}"></i><span class="ng-scope">{{$menu_item->name}}</span></a>
        @if ($submenu_items->count() > 0)
        <ul>
            @foreach ($submenu_items->get() as $key2 => $sub_menu_item)
                @if (Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key."_".$sub_menu_item->key) || !$sub_menu_item->require_permission)
                    <li @if ($path == $sub_menu_item->getSidebarURL()) class="active" @endif>

                        <?php
                        $sub_submenu_items = \Menu::where("user_type", Auth::user()->Owner()->type)->where("hidden", 0)->where("category", "menu")->where("parent_id", $sub_menu_item->id)->orderBy("order", "ASC");
                        ?>

                        @if ($menu_item->key == "frontend" && $sub_menu_item->key == "view_site")
                            <a tabindex="-1" href="{{Auth::user()->Owner()->getSiteURL()}}" target="view_front_site"><span class="ng-scope">View</span></a>
                        @else
                            <a tabindex="-1" href="{{($sub_submenu_items->count() > 0) ? 'javascript: void(0);' : $sub_menu_item->getSidebarURL()}}"><span class="ng-scope">{{$sub_menu_item->name}}</span>@if ($sub_submenu_items->count() > 0)&nbsp;<i class="fa fa-caret-right"></i>@endif</a>
                        @endif
                        @if ($sub_submenu_items->count() > 0)
                        <ul>
                            @foreach ($sub_submenu_items->get() as $key2 => $sub_sub_menu_item)
                                @if (Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key."_".$sub_menu_item->key."_".$sub_sub_menu_item->key) || Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key."_".$sub_menu_item->key."_".$sub_sub_menu_item->key."_access") || Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key."_".$sub_menu_item->key."_".$sub_sub_menu_item->key."_users") || !$menu_item->require_permission || Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$sub_sub_menu_item->key) || Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$sub_sub_menu_item->key."_access"))
                                    <li @if ($path == $sub_sub_menu_item->getSidebarURL()) class="active" @endif>
                                            <a tabindex="-1" href="{{$sub_sub_menu_item->getSidebarURL()}}"><span class="ng-scope">{{$sub_sub_menu_item->name}}</span></a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        @endif
                    </li>
                @endif
            @endforeach
        </ul>
        @if ($submenu_items->count() > 0)<i class="fa fa-caret-down icon-has-ul-h"></i><i class="fa fa-caret-right icon-has-ul"></i>@endif
        @endif
    </li>
    @endif
@endforeach

@if (Auth::user()->hasRole("super_admin") && Auth::user()->type == "admin")
    <li class="mm-dropdown">
        <a href="javascript: void(0);"><i class="menu-icon fa fa-sort-amount-desc"></i><span class="ng-scope">Interface</span></a>
        <ul>
            <li @if ($path == "/interface/menu") class="active" @endif>
                <a href="/interface/menu"><span class="ng-scope">Menu</span></a>
            </li>
            <li @if ($path == "/interface/pages") class="active" @endif>
                <a href="/interface/pages"><span class="ng-scope">Pages</span></a>
            </li>
        </ul>
        <i class="fa fa-caret-down icon-has-ul-h"></i><i class="fa fa-caret-right icon-has-ul"></i>
    </li>
@endif