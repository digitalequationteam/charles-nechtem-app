<?php
$path = explode(SERVER_HOST, Request::url())[1];
?>
@foreach (\Menu::where("user_type", Auth::user()->Owner()->type)->where("hidden", 0)->where("category", "menu")->where("parent_id", 0)->orderBy("order", "ASC")->get() as $key => $menu_item)
    <?php
    $submenu_items = \Menu::where("user_type", Auth::user()->Owner()->type)->where("hidden", 0)->where("category", "menu")->where("parent_id", $menu_item->id)->orderBy("order", "ASC");
    ?>
    @if (Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key) || !$menu_item->require_permission)
    <li class="@if ($path == $menu_item->getSidebarURL()) current active @endif @if ($submenu_items->count() > 0) mm-dropdown @endif">
        <a @if ($menu_item->key == "support") href="#live_help" data-toggle="modal" @else href="{{$menu_item->getSidebarURL()}}" @endif><i class="menu-icon fa {{$menu_item->icon_class}}"></i><span class="mm-text">{{$menu_item->name}}</span>@if ($submenu_items->count() > 0)<span class="fa arrow"></span>@endif</a>
        @if ($submenu_items->count() > 0)
        <ul>
            @foreach ($submenu_items->get() as $key2 => $sub_menu_item)
                @if (Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key."_".$sub_menu_item->key) || !$sub_menu_item->require_permission)
                    <li @if ($path == $sub_menu_item->getSidebarURL()) class="active" @endif>
                        <?php
                        $sub_submenu_items = \Menu::where("user_type", Auth::user()->Owner()->type)->where("hidden", 0)->where("category", "menu")->where("parent_id", $sub_menu_item->id)->orderBy("order", "ASC");
                        ?>

                        @if ($menu_item->key == "frontend" && $sub_menu_item->key == "view_site")
                            <a tabindex="-1" href="{{Auth::user()->Owner()->getSiteURL()}}" target="view_front_site"><span class="mm-text">View</span></a>
                        @else
                            <a tabindex="-1" href="{{$sub_menu_item->getSidebarURL()}}"><span class="mm-text">{{$sub_menu_item->name}}</span>@if ($sub_submenu_items->count() > 0)<span class="fa arrow"></span>@endif</a>
                        @endif
                        @if ($sub_submenu_items->count() > 0)
                        <ul>
                            @foreach ($sub_submenu_items->get() as $key2 => $sub_sub_menu_item)
                                @if (Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key."_".$sub_menu_item->key."_".$sub_sub_menu_item->key) || Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key."_".$sub_menu_item->key."_".$sub_sub_menu_item->key."_access") || Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$menu_item->key."_".$sub_menu_item->key."_".$sub_sub_menu_item->key."_users") || !$menu_item->require_permission || Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$sub_sub_menu_item->key) || Auth::user()->hasPermission(Auth::user()->Owner()->type."_".$sub_sub_menu_item->key."_access"))
                                    <li @if ($path == $sub_sub_menu_item->getSidebarURL()) class="active" @endif>
                                            <a tabindex="-1" href="{{$sub_sub_menu_item->getSidebarURL()}}"><span class="mm-text">{{$sub_sub_menu_item->name}}</span></a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        @endif
                    </li>
                @endif
            @endforeach
        </ul>
        @endif
    </li>
    @endif
@endforeach

@if (Auth::user()->hasRole("super_admin") && Auth::user()->type == "admin")
    <li class="mm-dropdown">
        <a href="javascript: void(0);"><i class="menu-icon fa fa-sort-amount-desc"></i><span class="mm-text">Interface</span><span class="fa arrow"></span></a>
        <ul>
            <li @if ($path == "/interface/menu") class="active" @endif>
                <a href="/interface/menu"><span class="mm-text">Menu</span></a>
            </li>
            <li @if ($path == "/interface/pages") class="active" @endif>
                <a href="/interface/pages"><span class="mm-text">Pages</span></a>
            </li>
        </ul>
    </li>
@endif