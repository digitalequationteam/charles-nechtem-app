<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"><!--<![endif]-->

<?php
if (\Auth::user()->getSetting('color'))
    $userToCheck = \Auth::user();
else
    $userToCheck = \Auth::user()->Owner();

\Auth::user()->last_activity = date("Y-m-d H:i:s");
\Auth::user()->save();
?>

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title><?php echo Config::get("app.name"); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="/css/icons.min.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/plugins.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="{{$userToCheck->getSetting('color') ? '/css/colors/color-'.$userToCheck->getSetting('color').'.css' : '#'}}" rel="stylesheet" id="theme-color">
    <!-- END  MANDATORY STYLE -->
hide
    <link href="/css/fontawesome-iconpicker.min.css" rel="stylesheet">

    <link rel="stylesheet" href="/css/dataTables.css">
    <link rel="stylesheet" href="/css/application.css">
    <link rel="stylesheet" href="/css/jquery.qtip.css">

    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.css">

    <script src="/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <script type="text/javascript">
        document.domain = '{{MAIN_DOMAIN}}';
    </script>

    <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>

<body class="{{$userToCheck->getSetting('layout_type')}">
    <!-- BEGIN TOP MENU -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid" style="padding: 0px;">
            <div class="navbar-header" style="position: relative;">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="menu-medium" class="sidebar-toggle tooltips" @if ($userToCheck->getSetting('menu_type') == 'horizontal') style="display: none;" @endif>
                    <i class="fa fa-outdent"></i>
                </a>
                <?php /*<a class="navbar-brand @if ($userToCheck->getSetting('menu_type') == 'horizontal') for-top @endif" href="/"><?php if (!empty(Auth::user()->Owner()->logo)) { ?><img src="/uploads/<?php echo Auth::user()->Owner()->logo; ?>" style="max-height: 42px;" /><?php } else { ?>CNA Business App<?php } ?></a>*/?>
                <a class="navbar-brand @if ($userToCheck->getSetting('menu_type') == 'horizontal') for-top @endif" href="/" style="margin-left: 0px;"><img src="/images/logo.png" /></a>
            </div>
            <div class="navbar-collapse collapse">
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-left header-menu" style="padding-left: 150px;">
                    <li>
                        <?php
                        $width = \Menu::where("user_type", Auth::user()->Owner()->type)->where("hidden", 0)->where("category", "menu")->where("parent_id", 0)->orderBy("order", "ASC")->count() * 120;
                        ?>
                        <div class="top-nav-wrapper" @if ($userToCheck->getSetting('menu_type') != 'horizontal') style="display: none;" @endif>
                            <aside data-ng-include=" 'views/sidebar.html' " id="nav-container" class="nav-container ng-scope nav-fixed nav-horizontal nav-fixed" data-ng-class=" {'nav-fixed': admin.fixedSidebar, 
                                                            'nav-horizontal': admin.menu === 'horizontal', 
                                                            'nav-vertical': admin.menu === 'vertical'
                                           }" style="">        
                                <div class="nav-wrapper ng-scope">
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><ul id="nav" class="nav" data-slim-scroll="" data-collapse-nav="" data-highlight-active="" style="overflow: hidden; width: auto; height: 100%;">
                                        @include('layouts.menu.display_horizontal')
                                    </ul><div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 558px; background: rgb(0, 0, 0);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
                                </div></aside>
                        </div>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right header-menu">

                    @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_settings_appearance"))
                        <li class="dropdown" id="settings-header" style="cursor: default;">
                            <a href="#" class="c-white @if ($userToCheck->getSetting('menu_type') == 'horizontal') for-top @endif" onclick="if ($('#appearance-dropdown').is(':visible')) { $('#appearance-dropdown').hide(); } else { $('#appearance-dropdown').show(); }">
                                <i class="fa fa-cog"></i>
                            </a>
                            <div id="appearance-dropdown" class="dropdown-menu" style="padding: 10px; width: 400px; max-width: none !important;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Menu Style</p>
                                        <label class=""><input name="menu" type="radio" value="vertical" ng-model="admin.menu" class="ng-untouched ng-valid ng-dirty" style="" @if ($userToCheck->getSetting('menu_type') != 'horizontal') checked @endif><span>Vertical</span></label>
                                        <label class=""><input name="menu" type="radio" value="horizontal" ng-model="admin.menu" class="ng-untouched ng-valid ng-dirty ng-valid-parse" style="" @if ($userToCheck->getSetting('menu_type') == 'horizontal') checked @endif><span>Horizontal</span></label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Layout Style</p>
                                        <label class=""><input name="layout" type="radio" value="wide" ng-model="admin.layout" class="ng-untouched ng-valid ng-dirty" style="" @if ($userToCheck->getSetting('layout_type') != 'horizontal') checked @endif><span>Wide</span></label>
                                        <label class=""><input name="layout" type="radio" value="boxed" ng-model="admin.layout" class="ng-untouched ng-valid ng-dirty ng-valid-parse" style="" @if ($userToCheck->getSetting('layout_type') == 'boxed') checked @endif><span>Boxed</span></label>
                                    </div>
                                </div>

                                <p>Color Theme</p>
                                    <a href="#" data-style="dark" class="btn theme-color bg-dark" style="display: inline-block; width: 32%; margin-bottom: 5px;"><span class="sidebar-text">Dark Skin</span></a>

                                    <a href="#" data-style="light" class="btn theme-color btn-default" style="display: inline-block; width: 32%; margin-bottom: 5px;"><span class="sidebar-text">Light Skin</span></a>

                                    <a href="#" data-style="cafe" class="btn theme-color bg-cafe" style="display: inline-block; width: 32%; margin-bottom: 5px; background-color: #31251e; color: #FFFFFF;"><span class="sidebar-text">Cafe Skin</span></a>

                                    <a href="#" data-style="blue" class="btn theme-color bg-blue" style="display: inline-block; width: 32%; margin-bottom: 5px;"><span class="sidebar-text">Blue Skin</span></a>

                                    <a href="#" data-style="red" class="btn theme-color bg-red" style="display: inline-block; width: 32%; margin-bottom: 5px;"><span class="sidebar-text">Red Skin</span></a>

                                    <a href="#" data-style="green" class="btn theme-color bg-green" style="display: inline-block; width: 32%; margin-bottom: 5px;"><span class="sidebar-text">Green Skin</span></a>
                                
                                <p>Login Page Background Image</p>

                                <?php
                                $image = !empty(\Common::getAdminUser()->login_background) ? '/uploads/'.\Common::getAdminUser()->login_background : '/images/signin-bg-1.jpg';
                                ?>
                                <div id="login-background-image-wrapper">
                                    <img id="login-background-image" src="{{$image}}" style="width: 100%;" />
                                    <div id="login-background-image-upload" style="vertical-align: middle; width: 100%; height: 400px; text-align: center; background-image: url({{$image}}); background-size: 100%; display: none;">
                                        <form id="login-background-image-form" action="/settings/upload-new-login-background" method="post" enctype="multipart/form-data" target="lbIframe">
                                            <div style="padding: 15px; background: #FFFFFF; width: 250px; margin: auto;">
                                                Upload new image: <input type="file" name="lb-file" id="lb-file" placeholder="Upload new image" style="display: none;" />

                                                <label id="login-background-image-label" for="lb-file" class="btn btn-success">Upload New Image</label>

                                                <div id="login-background-image-loading" class="text-center" style="padding-top: 15px; display: none;">
                                                    <img src="https://www.hereofamily.com/wp-content/themes/HereO/images/ajax-loader.gif" style="width: 30px;" />
                                                </div>
                                            </div>
                                        </form>

                                        <iframe id="lbIframe" name="lbIframe" style="display: none;"></iframe>
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    var lb_uploading = false;
                                    $(document).ready(function() {
                                        $("#login-background-image-wrapper").mouseover(function() {
                                            var height = $("#login-background-image").height();
                                            var padding = height/2 - 80;
                                            $("#login-background-image").hide();
                                            $("#login-background-image-upload").show().css("height", height + "px");
                                            $("#login-background-image-upload").show().css("padding-top", padding + "px");
                                        });

                                        $("#login-background-image-wrapper").mouseout(function() {
                                            if (!lb_uploading) {
                                                $("#login-background-image").show();
                                                $("#login-background-image-upload").hide();
                                            }
                                        });

                                        $("#login-background-image-label").click(function() {
                                            $("#appearance-dropdown").addClass("visible");
                                        });

                                        $("#lb-file").change(function() {
                                            $("#login-background-image-form").submit();
                                            $("#appearance-dropdown").addClass("visible");
                                            $("#login-background-image").hide();
                                            $("#login-background-image-upload").show();
                                            $("#login-background-image-loading").show();
                                            lb_uploading = true;
                                        });

                                        $("body").click(function() {
                                            $("#appearance-dropdown").removeClass("visible");
                                        });
                                    });

                                    function completeUpload(image_url) {
                                        $("#login-background-image").attr("src", image_url).show();
                                        $("#login-background-image-upload").css("background-image", 'url(' + image_url + ')').hide();
                                        lb_uploading = false;
                                        $("#login-background-image-loading").hide();
                                    }
                                </script>

                            </div>
                        </li>
                    @endif

                    <!-- BEGIN USER DROPDOWN -->
                    <li class="dropdown" id="user-header">
                        <a href="#" class="dropdown-toggle c-white @if ($userToCheck->getSetting('menu_type') == 'horizontal') for-top @endif" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username">{{Auth::user()->first_name}}</span>
                            <i class="fa fa-angle-down p-r-10"></i>
                        </a>
                        <ul class="dropdown-menu">
                            @if (Auth::user()->hasPermission("admin_dashboard_view"))
                                <li>
                                    <a href="/"><i class="fa fa-home text-info"></i>&nbsp;&nbsp;Dashboard</a>
                                </li>
                            @endif
                            <li><a href="/settings/profile"><i class="dropdown-icon fa fa-user text-warning"></i>&nbsp;&nbsp;Account</a></li>
                            <li class="dropdown-footer clearfix">
                                <a href="javascript:;" class="toggle_fullscreen" title="Fullscreen">
                                    <i class="glyph-icon flaticon-fullscreen3"></i>
                                </a>
                                <a href="/settings/profile" title="Lock Screen">
                                    <i class="fa fa-key"></i>
                                </a>
                                <a href="{{ action('\App\Modules\Auth\Http\Controllers\AuthController@getLogout') }}" title="Logout">
                                    <i class="fa fa-power-off"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
    </nav>
    <!-- END TOP MENU -->
    <!-- BEGIN WRAPPER -->

    <div id="wrapper" @if ($userToCheck->getSetting('menu_type') == 'horizontal') class="full-wrapper" @endif>
        <!-- BEGIN MAIN SIDEBAR -->
        <nav id="sidebar" class="mCustomScrollbar" @if ($userToCheck->getSetting('menu_type') == 'horizontal') style="display: none;" @endif>
            <div id="main-menu">
                <ul class="sidebar-nav" style="padding-top: 15px;">
                    @include('layouts.menu.display')
                </ul> <!-- / .navigation -->
            </div>
            <div class="footer-widget">
                <div class="footer-gradient"></div>
                <div id="sidebar-charts">
                    <div class="sidebar-charts-inner">
                        <div class="sidebar-charts-left">
                            <div class="sidebar-chart-title">Clients</div>
                            <div class="sidebar-chart-number">{{\Client::count()}}</div>
                        </div>
                        <div class="sidebar-charts-right" data-type="bar" data-color="theme">
                        </div>
                    </div>
                    <hr class="divider">
                    <div class="sidebar-charts-inner">
                        <div class="sidebar-charts-left">
                            <div class="sidebar-chart-title">Providers</div>
                            <div class="sidebar-chart-number">{{\Provider::count()}}</div>
                        </div>
                        <div class="sidebar-charts-right" data-type="bar" data-color="theme">
                        </div>
                    </div>
                </div>
                <div class="sidebar-footer clearfix">
                    <a class="pull-left" href="/settings/profile" data-rel="tooltip" data-placement="top" data-original-title="Settings"><i class="glyph-icon flaticon-settings21"></i></a>
                    <a class="pull-left" href="/settings/profile" data-rel="tooltip" data-placement="top" data-original-title="Change Password"><i class="fa fa-key"></i></a>
                    <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen"><i class="glyph-icon flaticon-fullscreen3"></i></a>
                    <a class="pull-left" href="/auth/logout" data-rel="tooltip" data-placement="top" data-original-title="Logout"><i class="fa fa-power-off"></i></a>
                </div> 
            </div>
        </nav>
        <!-- END MAIN SIDEBAR -->

        <!-- BEGIN MAIN CONTENT -->
        <div id="main-content" class="dashboard">
            <div class="m-b-20 clearfix">
                @yield('content')
            </div>
        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END WRAPPER -->

<div class="modal fade" id="live_help" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="text-align: left;">Get Help</h4>
            </div>
            <div class="modal-body" style="max-height: 600px; overflow-y: auto; text-align: left; font-weight: normal;">
                <form id="support_form" action="/api/send-support-email" method="post" class="form-horizontal" target="support_iframe">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Subject <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" required name="subject" id="subject" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Message <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <textarea class="form-control" required name="message" id="message" style="height: 120px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn bg-success">Send <i class="icon-arrow-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>

                <iframe src="" name="support_iframe" id="support_iframe" style="display: none;
                "></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-info" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#support_form").submit(function() {
            $(this).prepend('<div class="loading-message " style="display: block; margin: auto;"><div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
            var that = this;
            setTimeout(function() {
                $(that).find(".loading-message").remove();
                $(that).prepend('<div class="alert alert-success">\
                                <button class="close" data-close="alert"></button>\
                                <span>\
                                    Support Request Sent. Thank you we will reply as quickly as possible!\
                                </span>\
                            </div>');

                $(that).find("input, textarea").val('');
            }, 1000);
        });

        $("body").removeClass("hide");

        //updateUserActivity();
    });

    function updateUserActivity() {
        $.post("/api/update-user-activity", {}, function() {
            setTimeout(updateUserActivity, 5000);
        }, "json");
    }
</script>

    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="/js/mandatoryJs.min.js"></script>
    <!-- END MANDATORY SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/js/metrojs.min.js"></script>
    <script src='/js/moment.min.js'></script>
    <script src='/js/fullcalendar.min.js'></script>
    <script src="/js/jquery.simpleWeather.min.js"></script>
    <script src="/js/gmaps.js"></script>
    <script src="/js/jquery.flot.js"></script>
    <script src="/js/jquery.flot.animator.min.js"></script>
    <script src="/js/jquery.flot.resize.js"></script>
    <script src="/js/jquery.flot.time.min.js"></script>
    <script src="/js/raphael.min.js"></script>
    <script src="/js/morris.min.js"></script>
    <script src="/js/jquery.ui.touch-punch.min.js"></script>
    <script src="/js/calendar.js"></script>
    <!-- END  PAGE LEVEL SCRIPTS -->
    <script src="/js/application.js"></script>
    <script src="/js/fontawesome-iconpicker.js"></script>

    <script src="/js/custom.js"></script>

    <script src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables.bootstrap.js"></script>
    <script src="/js/dataTables.tableTools.js"></script>

    <script src="/js/jquery-sortable.js"></script>
    <script type="text/javascript" src="/js/jquery.imgareaselect.pack.js"></script>
    <script type="text/javascript" src="/js/imageMapResizer.min.js"></script>
    <!--<script type="text/javascript" src="/js/jquery.mapify.min.js"></script>-->
    
    <script type="text/javascript" src="/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="/js/jquery.maphilight.js"></script>
    <script type="text/javascript" src="/js/jquery.qtip.min.js"></script>

    <script src="/js/aam-ui.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <script src="/js/editable/contact.js"></script>
    <script src="/js/editable/address.js"></script>
    <script src="/js/editable/dependent.js"></script>
    <script src="/js/editable/license.js"></script>

    <script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.js"></script>
</body>

</html>