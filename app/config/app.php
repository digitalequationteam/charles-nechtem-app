<?php

if (SERVER_ENVIRONMENT == "local") {
    define("SERVER_HOST", "charlesnechtem.local");
    define("MAIN_DOMAIN", "charlesnechtem.local");
    $admin_subdomain = "admin";
}
elseif (SERVER_ENVIRONMENT == "staging") {
    define("SERVER_HOST", "staging.charlesnechtem.net");
    define("MAIN_DOMAIN", "charlesnechtem.net");
    $admin_subdomain = "admin";
}
elseif (SERVER_ENVIRONMENT == "live") {
    define("SERVER_HOST", "charlesnechtem.net");
    define("MAIN_DOMAIN", "charlesnechtem.net");
    $admin_subdomain = "cp";
}
else {
    define("SERVER_HOST", "charlesnechtem.net");
    define("MAIN_DOMAIN", "charlesnechtem.net");
    $admin_subdomain = "admin";
}

return [

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => true,

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => 'https://' . SERVER_HOST . '/',

    'domain' => SERVER_HOST,

    'main_domain' => MAIN_DOMAIN,

    'global_hosts' => array(SERVER_HOST, 'www.'.SERVER_HOST),

    'admin_subdomain' => $admin_subdomain.'.'.SERVER_HOST,

    'name' => 'CNA Business App',

    'email' => 'info@'.SERVER_HOST,

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY', 'SomeRandomString'),

    'cipher' => (1) ? 'AES-128-CBC' : 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => 'single',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Foundation\Providers\ArtisanServiceProvider::class,
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Routing\ControllerServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        'MicheleAngioni\Entrust\EntrustServiceProvider',
        'anlutro\LaravelSettings\ServiceProvider',

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        Caffeinated\Modules\ModulesServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App'       => Illuminate\Support\Facades\App::class,
        'Artisan'   => Illuminate\Support\Facades\Artisan::class,
        'Auth'      => Illuminate\Support\Facades\Auth::class,
        'Blade'     => Illuminate\Support\Facades\Blade::class,
        'Bus'       => Illuminate\Support\Facades\Bus::class,
        'Cache'     => Illuminate\Support\Facades\Cache::class,
        'Config'    => Illuminate\Support\Facades\Config::class,
        'Cookie'    => Illuminate\Support\Facades\Cookie::class,
        'Crypt'     => Illuminate\Support\Facades\Crypt::class,
        'DB'        => Illuminate\Support\Facades\DB::class,
        'Eloquent'  => Illuminate\Database\Eloquent\Model::class,
        'Event'     => Illuminate\Support\Facades\Event::class,
        'File'      => Illuminate\Support\Facades\File::class,
        'Hash'      => Illuminate\Support\Facades\Hash::class,
        'Input'     => Illuminate\Support\Facades\Input::class,
        'Inspiring' => Illuminate\Foundation\Inspiring::class,
        'Lang'      => Illuminate\Support\Facades\Lang::class,
        'Log'       => Illuminate\Support\Facades\Log::class,
        'Mail'      => Illuminate\Support\Facades\Mail::class,
        'Password'  => Illuminate\Support\Facades\Password::class,
        'Queue'     => Illuminate\Support\Facades\Queue::class,
        'Redirect'  => Illuminate\Support\Facades\Redirect::class,
        'Redis'     => Illuminate\Support\Facades\Redis::class,
        'Request'   => Illuminate\Support\Facades\Request::class,
        'Response'  => Illuminate\Support\Facades\Response::class,
        'Route'     => Illuminate\Support\Facades\Route::class,
        'Schema'    => Illuminate\Support\Facades\Schema::class,
        'Session'   => Illuminate\Support\Facades\Session::class,
        'Storage'   => Illuminate\Support\Facades\Storage::class,
        'URL'       => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View'      => Illuminate\Support\Facades\View::class,
        'Entrust' => 'MicheleAngioni\Entrust\EntrustFacade',
        'Setting' => 'anlutro\LaravelSettings\Facade',

        'Module' => Caffeinated\Modules\Facades\Module::class,

        'Common' => App\Common::class,
        'StaticData' => App\StaticData::class,

        'Casper' => App\Helpers\Casper::class,
        'ZipCode' => App\Helpers\ZipCode::class,


        /* ----- Models&Helpers ----- */

            /* ----- Auth Module ----- */
                'User' => App\Modules\Auth\Models\User::class,
            /* ----- Auth Module ----- */

            /* ----- InterfaceMenu Module ----- */
                'Menu' => App\Modules\InterfaceMenu\Models\Menu::class,
                'CustomPageLink' => App\Modules\InterfaceMenu\Models\CustomPageLink::class,
            /* ----- InterfaceMenu Module ----- */

            /* ----- UsersManagement Module ----- */
                'Role' => App\Modules\UsersManagement\Models\Role::class,
                'Permission' => App\Modules\UsersManagement\Models\Permission::class,
            /* ----- UsersManagement Module ----- */

            /* ----- Settings Module ----- */
                'DictionaryValue' => App\Modules\Settings\Models\DictionaryValue::class,
                'ValuesDictionary' => App\Modules\Settings\Helpers\ValuesDictionary::class,
            /* ----- Settings Module ----- */

            /* ----- Clients Module ----- */
                'Client' => App\Modules\Clients\Models\Client::class,
                'Contact' => App\Modules\Clients\Models\Contact::class,
                'ClientPlan' => App\Modules\Clients\Models\ClientPlan::class,
                'ClientPlanEdition' => App\Modules\Clients\Models\ClientPlanEdition::class,
                'ClientEmployee' => App\Modules\Clients\Models\ClientEmployee::class,
                'ClientDocument' => App\Modules\Clients\Models\ClientDocument::class,
            /* ----- Clients Module ----- */

            /* ----- Providers Module ----- */
                'Provider' => App\Modules\Providers\Models\Provider::class,
                'ProviderLicense' => App\Modules\Providers\Models\ProviderLicense::class,
                'ProviderStaff' => App\Modules\Providers\Models\ProviderStaff::class,
                'ProviderCost' => App\Modules\Providers\Models\ProviderCost::class,
            /* ----- Providers Module ----- */

            /* ----- Cases Module ----- */
                'ECase' => App\Modules\Cases\Models\ECase::class,
                'CaseAuthorizationPage' => App\Modules\Cases\Models\CaseAuthorizationPage::class,
            /* ----- Cases Module ----- */

            /* ----- Cases Module ----- */
                'Claim' => App\Modules\Claims\Models\Claim::class,
                'ClaimService' => App\Modules\Claims\Models\ClaimService::class,
            /* ----- Cases Module ----- */

            /* ----- Comments Module ----- */
                'Comment' => App\Modules\Comments\Models\Comment::class,
            /* ----- Comments Module ----- */

            /* ----- Security Module ----- */
                'SecurityLog' => App\Modules\Security\Models\SecurityLog::class,
                'IpToAddress' => App\Modules\Security\Models\IpToAddress::class,
            /* ----- Security Module ----- */

        /* ----- Models ----- */

    ],

];
