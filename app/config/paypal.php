<?php

return [
	'domain' => (SERVER_ENVIRONMENT == "local" || SERVER_ENVIRONMENT == "staging") ? 'sandbox.paypal.com' : 'paypal.com',
];