<?php

namespace App;

class Common
{
	static public function slugify($text)
	{ 
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	  // trim
	  $text = trim($text, '-');

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // lowercase
	  $text = strtolower($text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  if (empty($text))
	  {
	    return 'n-a';
	  }

	  return $text;
	}
	static public function unslugify($text)
	{ 
	  return ucwords(strtolower(str_replace(array("-", "_"), array(" ", " "), $text)));
	}

	static public function replaceVariables($content, $company = null, $customer = null, $feedback = null)
	{
		$company->phone = Common::formatPhone(@$company->phone);
		if ($customer)
			$customer->phone = Common::formatPhone(@$customer->phone);
		$content = str_replace("{signature}", @$company->getSignature(), $content);
		$content = str_replace("{company_first_name}", @$company->first_name, $content);
		$content = str_replace("{company_last_name}", @$company->last_name, $content);
		$content = str_replace("{company_name}", @$company->company_name, $content);
		$content = str_replace("{company_phone}", @$company->phone, $content);
		$content = str_replace("{company_web_address}", @$company->web_address, $content);
		$content = str_replace("{company_email}", @$company->email, $content);
		$content = str_replace("{company_address}", @$company->address, $content);
		$content = str_replace("{company_state}", @$company->state, $content);
		$content = str_replace("{company_city}", @$company->city, $content);
		$content = str_replace("{company_zip}", @$company->zip, $content);
		$content = str_replace("{company_country}", @$company->country, $content);
		$content = str_replace("{company_full_address}", @$company->getLocation(), $content);
		if (!empty(@$feedback->customer_name)) {
			$content = str_replace("{customer_first_name}", @$feedback->customer_name, $content);
			$content = str_replace("{customer_email}", "-", $content);
			$content = str_replace("{customer_phone}", '-', $content);
			$content = str_replace("{customer_address}", '-', $content);
			$content = str_replace("{customer_state}", '-', $content);
			$content = str_replace("{customer_city}", '-', $content);
			$content = str_replace("{customer_zip}", '-', $content);
			$content = str_replace("{customer_profile_url}", @$feedback->customer_profile_url, $content);
		}
		else {
			$content = str_replace("{customer_first_name}", @$customer->first_name, $content);
			$content = str_replace("{customer_last_name}", @$customer->last_name, $content);
			$content = str_replace("{customer_email}", @$customer->email, $content);
			$content = str_replace("{customer_phone}", @$customer->phone, $content);
			$content = str_replace("{customer_address}", @$customer->address, $content);
			$content = str_replace("{customer_state}", @$customer->state, $content);
			$content = str_replace("{customer_city}", @$customer->city, $content);
			$content = str_replace("{customer_zip}", @$customer->zip, $content);
			$content = str_replace("{customer_profile_url}", "-", $content);
		}
		$content = str_replace("{date_review}", date("Y-m-d H:i:s"), $content);
		$content = str_replace("{review_text}", @$feedback->review, $content);
		$content = str_replace("{rating_review}", @$feedback->rating, $content);
		$content = str_replace("{review_type}", @$feedback->type, $content);
		
		return $content;
	}

	public static function formatPhone($phone = "") {
		if (!$phone) $phone = "";

		return ((is_numeric($phone) && strlen($phone) == 10) ? sprintf("(%s) %s-%s",
              substr($phone, 0, 3),
              substr($phone, 3, 3),
              substr($phone, 6, 4)) : $phone);
	}

	public static function getAllCountries() {
		$countries = array();

		foreach (\StaticData::$countries_and_states["countries"] as $key => $value) {
			$countries[$key] = $value["name"];
		}

		return $countries;
	}

	public static function getStates($country_code) {
		$states = @\StaticData::$countries_and_states["countries"][$country_code]["states"];
		if (empty($states))
			return array();
		else
			return $states;
	}

	public static function getCountryName($country_code) {
		return @\StaticData::$countries_and_states["countries"][$country_code]["name"];
	}

	public static function getCountryCode($country_name) {
		$country_name = \Common::unslugify($country_name);
		$code = "";

		foreach (@\StaticData::$countries_and_states["countries"] as $key => $country) {
			if ($country["name"] == $country_name)
				$code = $key;
		}

		return $code;
	}

	public static function getStateCode($state_name) {
		$state_name = \Common::unslugify($state_name);
		$code = "";

		foreach (@\StaticData::$countries_and_states["countries"] as $key => $country) {
			foreach ($country["states"] as $key2 => $state) {
				if ($state == $state_name)
					$code = $key2;
			}
		}

		return $code;
	}

	public static function getAdminUser() {
		return \User::where("type", "admin")->orderBy("created_at", "DESC")->get()->first();
	}

	public static function checkFrontSite($site_folder) {
		function recurse_copy($src,$dst) { 
            $dir = opendir($src); 
            @mkdir($dst); 
            while(false !== ( $file = readdir($dir)) ) { 
                if (( $file != '.' ) && ( $file != '..' )) { 
                    if ( is_dir($src . '/' . $file) ) { 
                        recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                    } 
                    else { 
                        copy($src . '/' . $file,$dst . '/' . $file); 
                    } 
                } 
            } 
            closedir($dir); 
        }

        $exploded = explode("/app", __FILE__);
        $folder = $exploded[0]."/public/";

        if (!is_dir($folder.'static_front_sites/'.$site_folder.'/')) {
            mkdir($folder.'static_front_sites/'.$site_folder.'/', 0755);

            recurse_copy($folder.'static_front_sites/default/', $folder.'static_front_sites/'.$site_folder.'/');
        }
	}

	public static function checkIPValid($ip, $array) {
		$exploded = explode(".", $ip);

		$variant1 = $ip;
		$variant2 = $exploded[0].".x.x.x";
		$variant3 = $exploded[0].".".$exploded[1].".x.x";
		$variant4 = $exploded[0].".".$exploded[1].".".$exploded[2].".x";

		return (
			in_array($variant1, $array) ||
			in_array($variant2, $array) ||
			in_array($variant3, $array) ||
			in_array($variant4, $array)
		);
	}

	// ms2time( (microtime(true) - ( time() - rand(0,1000000) ) ) );
    // return array
    public static function  ms2time($ms){
        $return = array();
        // ms
        $return['ms'] = (int) number_format( ($ms - (int) $ms), 2, '', '');
        $seconds = (int) $ms;
        unset($ms);

        if ($seconds%60 > 0){
            $return['s'] = $seconds%60;
        } else {
            $return['s'] = 0;
        }

        if ( ($minutes = intval($seconds/60))){
            $return['m'] = $minutes;
        }

        if (isset($return['m'])){
            $return['h'] = intval($return['m'] / 60);
            $return['m']  = $return['m'] % 60; 
        }


        if (isset($return['h'])){
            $return['d'] = intval($return['h'] / 24);
            $return['h']  = $return['h'] % 24; 
        }

        if (isset($return['d']))
            $return['mo'] = intval($return['d'] / 30);

        foreach($return as $k=>$v){
            if ($v == 0)
                unset($return[$k]);
        }

        return $return;
    }

    // ms2time2string( (microtime(true) - ( time() - rand(0,1000000) ) ) );
    // return array     
    public static function  secondsToTime($ms){
        $array = array(
            'ms' => 'ms',
            's'  => 'seconds',
            'm'  => 'minutes',
            'h'  => 'hours',
            'd'  => 'days',
            'mo' => 'month',
        );


        if ( ( $return = \Common::ms2time($ms) )  && count($ms) > 0){

            foreach($return as $key=>$data){
                $return[$key] = $data .' '.$array[$key];
            }

        }
        return implode(" ", array_reverse($return));
    }
}