<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$is_artisan_command = false;
if (empty($_SERVER["HTTP_HOST"])) {
	$_SERVER["HTTP_HOST"] = \Config::get("app.global_hosts")[0];
	$_SERVER["REQUEST_URI"] = "";
	$is_artisan_command = true;
}

Route::controller('/data-import', 'DataImportController');

/* Check if live admin has set any IP restrictions to the staging site - START. */
if (SERVER_ENVIRONMENT == "staging" && (strpos($_SERVER["REQUEST_URI"], "api/") === false) && (strpos($_SERVER["REQUEST_URI"], "prices.html") === false) && !$is_artisan_command) {
	$ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, str_replace("staging.", "", URL::to("/api/staging-restriction-settings")));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE );
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE );

    $result = (array)json_decode(curl_exec($ch), true);

    if (@$result["staging"]["access"] && !Common::checkIPValid($_SERVER["REMOTE_ADDR"], explode("\r\n", @$result["staging"]["ips"]["allowed"]))) {
        echo view('errors.403');
       	exit();
    }

    if (!@$result["staging"]["access"] && Common::checkIPValid($_SERVER["REMOTE_ADDR"], explode("\r\n", @$result["staging"]["ips"]["excluded"]))) {
        echo view('errors.403');
       	exit();
    }
}
/* Check if live admin has set any IP restrictions to the staging site. - END */

foreach (Module::enabled() as $module){
    $file = base_path('app/Modules/'.$module['name'].'/Http/routes.php');
    if (file_exists($file))
        require_once($file);
}

if (!$is_artisan_command) {
	Route::controller('api', 'ApiController');

	if ($_SERVER["HTTP_HOST"] == \Config::get("app.admin_subdomain")) {
		Route::controller('dashboard', 'DashboardController');

		define("SITE_ENVIRONMENT", "admin");

		Route::filter('auth', function()
		{
			if (Auth::guest()) return Redirect::action( '\App\Modules\Auth\Http\Controllers\AuthController@getIndex' );

			return \Permission::validate($_SERVER["REQUEST_URI"]);
		});

		Route::get('/', function () {
			if (Auth::check())
		    	return Redirect::action( 'DashboardController@getIndex' );
		    else
		    	return Redirect::action( '\App\Modules\Auth\Http\Controllers\AuthController@getIndex' );
		});
	}
	elseif (in_array($_SERVER["HTTP_HOST"], \Config::get("app.global_hosts"))) {
		$admin = \Common::getAdminUser();
		define("SITE_ENVIRONMENT", "company");
		define("FRONT_FOLDER", (($admin->type == "admin") ? "global" : $admin->username));

		Route::any('{slug}', [
		    'as' => 'front-site-page', 'uses' => '\App\Modules\Frontend\Http\Controllers\FrontSiteController@getIndex'
		])->where('slug', '([A-z\d-\/_.]+)?');
	}
}
