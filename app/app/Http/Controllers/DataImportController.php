<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class DataImportController extends Controller
{
    public function getImportValues()
    {
        for ($i=0; $i<=0; $i++) {
        	$offset = $i * 12 + 1;
        	$html = $this->extractDataFromURL("http://216.15.147.121:7070/administrator/settings/dictionaries/icd-codes/pager?list.offset=".($offset));

	        /*$exploded = explode('<div style="float: left; width: 535px;" class="name-container">', $html);

	        $count = 0;
	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		$exploded2 = explode('</div>', $item);
	        		$name = trim($exploded2[0]);
	        		
	        		$value = new \DictionaryValue;
		            $value->name = $name;
		            $value->category = "pos_codes";
		            $value->save();
	        	}
	        	$count++;
	        }
	        
	        $exploded = explode('<div class="title strechable" style="width: 70px;">', $html);
	        $count = 0;

	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		$exploded2 = explode('</div>', $item);
	        		$code = trim($exploded2[0]);

	        		$exploded2 = explode('<div class="strechable" style="width: 530px;">', $item);
	        		$exploded3 = explode('</div>', $exploded2[1]);
	        		$name = $exploded3[0];
	        		
	        		$value = new \DictionaryValue;
		            $value->name = $name;
		            $value->code = $code;
		            $value->category = "icd_codes";
		            $value->save();
	        	}
	        	$count++;
	        }
	        */
	    }
    }
    public function getImportClients()
    {
	    $clients_urls = array();

        for ($i=0; $i<=5; $i++) {
        	$offset = $i * 15 + 1;
        	$html = $this->extractDataFromURL("http://216.15.147.121:7070/administrator/clients/pager?list.offset=".($offset)."&sort=UID&sortOrder=ASCENDING");

	        $exploded = explode("location.href='", $html);
	        $count = 0;

	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		$exploded2 = explode("'", $item);
	        		$url = "http://216.15.147.121:7070".trim($exploded2[0]);

	        		//$exploded2 = explode('<div class="title" style="width: 610px;">', $item);
	        		//$exploded3 = explode('</div>', $exploded2[1]);

	        		//$url .= " ".trim($exploded3[0]);

	        		$clients_urls[] = $url;

	        	}
	        	$count++;
	        }
	    }

	    //echo "<pre>";
	    //var_dump($clients_urls);
	    //exit();

	    $countClient = 0;
        foreach ($clients_urls as $url) {
        	$client = array();

        	$edit_url = str_replace("profile", "edit", $url);
        	$html = $this->extractDataFromURL($edit_url);

        	$contract_type_identifier = $this->extractElementValue($html, '<input name="contract.contractType"', 'value="', '"');

	        $client = array(
	        	"name" => $this->extractElementValue($html, '<label for="name">', 'value="', '"'),
	            "tax_id" => $this->extractElementValue($html, '<label for="taxID">', 'value="', '"'),
	            "memo" => $this->extractElementValue($html, '<label for="memo">', 'name="memo">', '</textarea>'),

	            "primary_street" => $this->extractElementValue($html, '<label for="primaryAddress.streetAddress">', 'value="', '"'),
	            "primary_city" => $this->extractElementValue($html, '<label for="primaryAddress.city">', 'value="', '"'),
	            "primary_country" => \Common::getCountryCode($this->extractElementValue($html, '<label for="primaryAddress.country">', 'selected="selected">', '</option>')),
	            "primary_state" => \Common::getStateCode($this->extractElementValue($html, 'name="primaryAddress.state"', 'value="', '"')),
	            "primary_zip" => $this->extractElementValue($html, '<label for="primaryAddress.zipCode">', 'value="', '"'),

	            "secondary_address_available" => (!empty($this->extractElementValue($html, '<label for="secondaryAddress.streetAddress">', 'value="', '"'))) ? 1 : 0,
	            "secondary_street" => $this->extractElementValue($html, '<label for="secondaryAddress.streetAddress">', 'value="', '"'),
	            "secondary_city" => $this->extractElementValue($html, '<label for="secondaryAddress.city">', 'value="', '"'),
	            "secondary_country" => \Common::getCountryCode($this->extractElementValue($html, '<label for="secondaryAddress.country">', 'selected="selected">', '</option>')),
	            "secondary_state" => \Common::getStateCode($this->extractElementValue($html, 'name="secondaryAddress.state"', 'value="', '"')),
	            "secondary_zip" => $this->extractElementValue($html, '<label for="secondaryAddress.zipCode">', 'value="', '"'),

	            "mailing_same_as_primary" => (empty($this->extractElementValue($html, '<label for="mailingAddress.streetAddress">', 'value="', '"'))) ? 1 : 0,
	            "mailing_street" => $this->extractElementValue($html, '<label for="mailingAddress.streetAddress">', 'value="', '"'),
	            "mailing_city" => $this->extractElementValue($html, '<label for="mailingAddress.city">', 'value="', '"'),
	            "mailing_country" => \Common::getCountryCode($this->extractElementValue($html, '<label for="mailingAddress.country">', 'selected="selected">', '</option>')),
	            "mailing_state" => \Common::getStateCode($this->extractElementValue($html, 'name="mailingAddress.state"', 'value="', '"')),
	            "mailing_zip" => $this->extractElementValue($html, '<label for="mailingAddress.zipCode">', 'value="', '"'),

	            "contract_type" => $this->extractElementValue($item, $contract_type_identifier.'">', '</option>'),
	            "contract_type_custom" => $this->extractElementValue($html, '<label for="contract.contractType">', 'value="', '"'),
	            "contract_period_from" => $this->extractElementValue($html, '<label for="contract.contractPeriod.startDate">', 'value="', '"'),
	            "contract_period_to" => $this->extractElementValue($html, '<label for="contract.contractPeriod.endDate">', 'value="', '"'),
	            "renewal_period_from" => $this->extractElementValue($html, '<label for="contract.renewalPeriod.startDate">', 'value="', '"'),
	            "renewal_period_to" => $this->extractElementValue($html, '<label for="contract.renewalPeriod.endDate">', 'value="', '"'),
	            "employee_head_count" => $this->extractElementValue($html, '<label for="contract.employeeHeadCount">', 'value="', '"'),
	            "invoice_type" => $this->extractElementValue($html, '<label for="contract.invoiceType">', 'selected="selected">', '</option>'),
	            "employee_fee" => $this->extractElementValue($html, '<label for="contract.employeeFee">', 'value="', '"'),

	            "status" => "active",

	            "contacts" => array(),
	            "plans" => array(),
	            "employees" => array(),
	            "documents" => array()
	        );

			/* EXTRACT CONTACTS */
			$contacts_section = $this->extractElementValue($html, 'Contact Information</a></h3>', 'Contract Information</a></h3>');

			$exploded = explode('<div class="list-input-element"', $contacts_section);
			$count = 0;

	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		$title_identifier = $this->extractElementValue($item, '.title" class="min-width-field"', 'value="', '"');

	        		$client["contacts"][] = array(
	        			"title" => $this->extractElementValue($item, $title_identifier.'">', '</option>'),
	        			"first_name" => $this->extractElementValue($item, 'firstName">', 'value="', '"'),
	        			"last_name" => $this->extractElementValue($item, 'lastName">', 'value="', '"'),
	        			"email" => $this->extractElementValue($item, 'email">', 'value="', '"'),
	        			"phone" => $this->extractElementValue($item, 'phone">', 'value="', '"'),
	        			"cell_phone" => $this->extractElementValue($item, 'cellPhone">', 'value="', '"'),
	        			"additional_info" => $this->extractElementValue($item, '<textarea', 'additionalInfo">', '</textarea>'),
	        		);
	        	}
	        	$count++;
	        }
	        /* EXTRACT CONTACTS */

	        /* EXTRACT PLANS */
	        $plans_url = str_replace("profile", "plans", $url);
	        $html = $this->extractDataFromURL($plans_url);

	        $plans_urls = array();

	        $html2 = $this->extractElementValue($html, '<div id="list" class="pager">', '<div id="bottom">');
	        $exploded = explode("location.href='", $html2);
	        $count = 0;

	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		$exploded2 = explode("'", $item);
	        		$plans_urls[] = "http://216.15.147.121:7070".trim($exploded2[0]);
	        	}
	        	$count++;
	        }

			foreach ($plans_urls as $plan_url) {
				$html = $this->extractDataFromURL($plan_url."/edit");

		        $plan = array(
		        	"type" => $this->extractElementValue($html, 'name="planType"', 'value="', '"'),
		        	"name" => $this->extractElementValue($html, '<label for="name">', 'value="', '"'),
		        	"editions" => array()
		        );

		        $html = $this->extractDataFromURL($plan_url);

				$html2 = $this->extractElementValue($html, '<div id="list" class="pager">', '<div id="bottom">');
		        $exploded = explode('<div class="info-block action-block">', $html2);
		        $count = 0;

		        foreach ($exploded as $item) {
		        	if ($count > 0) {
		        		if ($plan["type"] == "A") {
		        			$edition = array(
		        				"inin_copay_day" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Inpatient Co-Pay</label>$', 'per day')),
					            "inin_copay_admission" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Inpatient Co-Pay</label>$', 'per day up to a maximum $', 'per admission')),
		        				"inin_php_copay_day" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Inpatient PHP Co-Pay</label>$', 'per day')),
					            "inin_php_copay_admission" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Inpatient PHP Co-Pay</label>$', 'per day up to a maximum $', 'per admission')),
					            "inou_copay" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Outpatient Co-Pay</label>$', 'per visit')),
					            "inouopm_individual" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Outpatient Out of Pocket Max</label>$', '/individual')),
					            "inouopm_family" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Outpatient Out of Pocket Max</label>$', 'or $', '/family')),
					            "inot_copay" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Other Co-Pay</label>', '%</div>')),
					            "onde_individual" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Deductible</label>$', '/individual')),
					            "onde_family" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Deductible</label>', 'or $', '/family')),
					            "onin_copay" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Inpatient Co-Pay</label>', '%<br>')),
					            "onin_php_copay" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Inpatient PHP Co-Pay</label>', '%<br>')),
					            "onou_copay" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Outpatient Co-Pay</label>', '%<br>')),
					            "onouopm_individual" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Outpatient Out of Pocket Max</label>$', '/individual')),
					            "onouopm_family" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Outpatient Out of Pocket Max</label>', 'or $', '/family')),
					            "onot_copay" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Other Co-Pay</label>', '%</div>')),
		        			);
		        		}
		        		elseif ($plan["type"] == "B") {
		        			$edition = array(
					            "incp_copay" => (float)str_replace(",", "", $this->extractElementValue($item, 'In-Network', '<label>Co-Pay</label>', '%<br>')),
					            "inde_single" => (float)str_replace(",", "", $this->extractElementValue($item, 'In-Network', '<label>Single Employee Deductible</label>$', '<br>')),
					            "inde_employee_1" => (float)str_replace(",", "", $this->extractElementValue($item, 'In-Network', '<label>Employee + 1 Deductible</label>$', '<br>')),
					            "inde_employee_2" => (float)str_replace(",", "", $this->extractElementValue($item, 'In-Network', '<label>Employee + 2 or more Deductible</label>$', '<br>')),
					            "inou_single" => (float)str_replace(",", "", $this->extractElementValue($item, 'In-Network', '<label>Single Employee Out of Pocket Max</label>$', '<br>')),
					            "inou_employee_1" => (float)str_replace(",", "", $this->extractElementValue($item, 'In-Network', '<label>Employee + 1 Out of Pocket Max</label>$', '<br>')),
					            "inou_employee_2" => (float)str_replace(",", "", $this->extractElementValue($item, 'In-Network', '<label>Employee + 2 or more Out of Pocket Max</label>$', '<br>')),
					            "oncp_copay" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Co-Pay</label>', '%<br>')),
					            "onde_single" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Single Employee Deductible</label>$', '<br>')),
					            "onde_employee_1" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Employee + 1 Deductible</label>$', '<br>')),
					            "onde_employee_2" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Employee + 2 or more Deductible</label>$', '<br>')),
					            "onou_single" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Single Employee Out of Pocket Max</label>$', '<br>')),
					            "onou_employee_1" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Employee + 1 Out of Pocket Max</label>$', '<br>')),
					            "onou_employee_2" => (float)str_replace(",", "", $this->extractElementValue($item, 'Out-Network', '<label>Employee + 2 or more Out of Pocket Max</label>$', '<br>'))
		        			);
		        		}

		        		$plan["editions"][] = $edition;
		        	}
		        	$count++;
		        }

		        $client["plans"][] = $plan;
			}
			/* EXTRACT PLANS */

			/* EXTRACT EMPLOYEES */
			$hasEmployees = true;
		    $employees_urls = array();

			$i=0;
			while ($hasEmployees) {
				$offset = $i * 15 + 1;
		    	$employees_url = str_replace("profile", "enrollees", $url);
				$html = $this->extractDataFromURL($employees_url."?list.offset=".$offset);

		        $html2 = $this->extractElementValue($html, '<div id="list" class="pager">', '<div id="bottom">');
		        $exploded = explode("location.href='", $html2);
		        $count = 0;

		        $prev_count = count($employees_urls);
		        foreach ($exploded as $item) {
		        	if ($count > 0) {
		        		$exploded2 = explode("'", $item);
		        		if (strpos($exploded2[0], "employee-profile") !== false && !in_array("http://216.15.147.121:7070".trim($exploded2[0]), $employees_urls))
		        			$employees_urls[] = "http://216.15.147.121:7070".trim($exploded2[0]);
		        	}
		        	$count++;
		        }

		        if ($prev_count == count($employees_urls))
		        	$hasEmployees = false;

		        $i++;
	        }
			/* EXTRACT EMPLOYEES */

	        foreach ($employees_urls as $employee_url) {
	        	$html = $this->extractDataFromURL($employee_url);

		        $address_section = $this->extractElementValue($html, '<div class="address-info">', '</div>');
		        $exploded = explode("</span>", $address_section);

		        $employee = array(
        			"first_name" => $this->extractElementValue($html, '<label>First Name</label>', '<br'),
        			"last_name" => $this->extractElementValue($html, '<label>Last Name</label>', '<br'),
        			"gender" => $this->extractElementValue($html, '<label>Gender</label>', '<br'),
        			"ssn" => $this->extractElementValue($html, '<label>SSN</label>', '<br'),
        			"cell_phone" => $this->extractElementValue($html, '<label>Cell Phone<</label>', '<br'),
        			"home_phone" => $this->extractElementValue($html, '<label>Home Phone</label>', '<br'),
        			"work_phone" => $this->extractElementValue($html, '<label>Work Phone</label>', '<br'),
        			"birthday" => $this->extractElementValue($html, '<label>Birthday</label>', '<br'),
        			"plan" => $this->extractElementValue($html, '<label>Plan</label>', '<br'),
        			"insurance_network" => $this->extractElementValue($html, '<label>Insurance Network</label>', '<br'),
        			"insurance_type" => $this->extractElementValue($html, '<label>Insurance Type</label>', '<br'),
        			"street" => trim(strip_tags(@$exploded[0])),
        			"city" => trim(strip_tags(@$exploded[1])),
        			"state" => trim(strip_tags(@$exploded[2])),
        			"zip" => trim(strip_tags(@$exploded[3])),
        			"country" => trim(strip_tags(@$exploded[4])),
        			"dependants" => array()
		        );

				$dependants_section = $this->extractElementValue($html, '<span class="title">Dependent Persons</span>', '<span class="title">Deductible Info</span>');

				$exploded = explode('<div class="dependant-container"', $dependants_section);

				if (count($exploded) > 1) {
					foreach ($exploded as $item) {
			        	if ($count > 0) {
					        $address_section = $this->extractElementValue($item, '<div class="address-info">', '</div>');
					        $exploded2 = explode("</span>", $address_section);

			        		$dependent = array(
		        				"first_name" => $this->extractElementValue($item, '<label>First Name</label>', '<br'),
			        			"last_name" => $this->extractElementValue($item, '<label>Last Name</label>', '<br'),
			        			"gender" => $this->extractElementValue($item, '<label>Gender</label>', '<br'),
			        			"ssn" => $this->extractElementValue($item, '<label>SSN</label>', '<br'),
			        			"cell_phone" => $this->extractElementValue($item, '<label>Cell Phone<</label>', '<br'),
			        			"home_phone" => $this->extractElementValue($item, '<label>Home Phone</label>', '<br'),
			        			"work_phone" => $this->extractElementValue($item, '<label>Work Phone</label>', '<br'),
			        			"birthday" => $this->extractElementValue($item, '<label>Birthday</label>', '<br'),
			        			"dependent_status" => $this->extractElementValue($item, '<label>Relation</label>', '<br'),
			        			"street" => trim(strip_tags(@$exploded2[0])),
			        			"city" => trim(strip_tags(@$exploded2[1])),
			        			"state" => trim(strip_tags(@$exploded2[2])),
			        			"zip" => trim(strip_tags(@$exploded2[3])),
			        			"country" => trim(strip_tags(@$exploded2[4])),
		        			);
			        		
			        		if (!empty($dependent["first_name"])) {
				        		$employee["dependants"][] = $dependent;
			        		}
			        	}
			        	$count++;
			        }
		        }

		        $client["employees"][] = $employee;
		    }

		    /*
		    echo "<pre>";
		    print_r($client);
		    exit();
		    */

		    $db_client = new \Client;
		    $db_client->name = $client["name"];
            $db_client->tax_id = $client["tax_id"];
            $db_client->memo = $client["memo"];

            $db_client->primary_street = $client["primary_street"];
            $db_client->primary_city = $client["primary_city"];
            $db_client->primary_country = $client["primary_country"];
            $db_client->primary_state = $client["primary_state"];
            $db_client->primary_zip = $client["primary_zip"];

            $db_client->secondary_address_available = $client["secondary_address_available"];
            $db_client->secondary_street = $client["secondary_street"];
            $db_client->secondary_city = $client["secondary_city"];
            $db_client->secondary_country = $client["secondary_country"];
            $db_client->secondary_state = $client["secondary_state"];
            $db_client->secondary_zip = $client["secondary_zip"];

            $db_client->mailing_same_as_primary = $client["mailing_same_as_primary"];
            $db_client->mailing_street = $client["mailing_street"];
            $db_client->mailing_city = $client["mailing_city"];
            $db_client->mailing_country = $client["mailing_country"];
            $db_client->mailing_state = $client["mailing_state"];
            $db_client->mailing_zip = $client["mailing_zip"];

            $db_client->contract_type = $client["contract_type"];
            $db_client->contract_type_custom = $client["contract_type_custom"];
            $db_client->contract_period_from = $client["contract_period_from"];
            $db_client->contract_period_to = $client["contract_period_to"];
            $db_client->renewal_period_from = $client["renewal_period_from"];
            $db_client->renewal_period_to = $client["renewal_period_to"];
            $db_client->employee_head_count = $client["employee_head_count"];
            $db_client->invoice_type = $client["invoice_type"];
            $db_client->employee_fee = $client["employee_fee"];

            $db_client->status = $client["status"];
            $db_client->save();

            foreach ($client["contacts"] as $contact) {
            	$db_contact = new \Contact;
            	$db_contact->owner_id = $db_client->id;
            	$db_contact->title = $contact["title"];
    			$db_contact->first_name = $contact["first_name"];
    			$db_contact->last_name = $contact["last_name"];
    			$db_contact->email = $contact["email"];
    			$db_contact->phone = $contact["phone"];
    			$db_contact->cell_phone = $contact["cell_phone"];
    			$db_contact->additional_info = $contact["additional_info"];
    			$db_contact->save();
            }

            foreach ($client["plans"] as $plan) {
            	$db_plan = new \ClientPlan;
            	$db_plan->client_id = $db_client->id;
            	$db_plan->type = $plan["type"];
    			$db_plan->name = $plan["name"];
    			$db_plan->save();

		        foreach ($plan["editions"] as $edition) {
		        	$db_edition = new \ClientPlanEdition;
            		$db_edition->client_plan_id = $db_plan->id;

		        	if ($plan["type"] == "A") {
		                $db_edition->inin_copay_day = $edition["inin_copay_day"];
		                $db_edition->inin_copay_admission = $edition["inin_copay_admission"];
		                $db_edition->inin_php_copay_day = $edition["inin_php_copay_day"];
		                $db_edition->inin_php_copay_admission = $edition["inin_php_copay_admission"];
		                $db_edition->inou_copay = $edition["inou_copay"];
		                $db_edition->inouopm_individual = $edition["inouopm_individual"];
		                $db_edition->inouopm_family = $edition["inouopm_family"];
		                $db_edition->inot_copay = $edition["inot_copay"];
		                $db_edition->onde_individual = $edition["onde_individual"];
		                $db_edition->onde_family = $edition["onde_family"];
		                $db_edition->onin_copay = $edition["onin_copay"];
		                $db_edition->onin_php_copay = $edition["onin_php_copay"];
		                $db_edition->onou_copay = $edition["onou_copay"];
		                $db_edition->onouopm_individual = $edition["onouopm_individual"];
		                $db_edition->onouopm_family = $edition["onouopm_family"];
		                $db_edition->onot_copay = $edition["onot_copay"];
		            }
		            else {
		                $db_edition->incp_copay = $edition["incp_copay"];
		                $db_edition->inde_single = $edition["inde_single"];
		                $db_edition->inde_employee_1 = $edition["inde_employee_1"];
		                $db_edition->inde_employee_2 = $edition["inde_employee_2"];
		                $db_edition->inou_single = $edition["inou_single"];
		                $db_edition->inou_employee_1 = $edition["inou_employee_1"];
		                $db_edition->inou_employee_2 = $edition["inou_employee_2"];
		                $db_edition->oncp_copay = $edition["oncp_copay"];
		                $db_edition->onde_single = $edition["onde_single"];
		                $db_edition->onde_employee_1 = $edition["onde_employee_1"];
		                $db_edition->onde_employee_2 = $edition["onde_employee_2"];
		                $db_edition->onou_single = $edition["onou_single"];
		                $db_edition->onou_employee_1 = $edition["onou_employee_1"];
		                $db_edition->onou_employee_2 = $edition["onou_employee_2"];
		            }
		            $db_edition->save();
		        }
            }

            foreach ($client["employees"] as $employee) {
            	$db_employee = new \ClientEmployee;
            	$db_employee->client_id = $db_client->id;

            	$db_employee->first_name = $employee['first_name'];
	            $db_employee->last_name = $employee['last_name'];
	            $db_employee->gender = $employee['gender'];
	            $db_employee->cell_phone = $employee['cell_phone'];
	            $db_employee->home_phone = $employee['home_phone'];
	            $db_employee->work_phone = $employee['work_phone'];
	            $db_employee->birthday = $employee['birthday'];

	            $db_employee->street = $employee['street'];
	            $db_employee->city = $employee['city'];
	            $db_employee->country = $employee['country'];
	            $db_employee->state = $employee['state'];
	            $db_employee->zip = $employee['zip'];

	            $db_employee->ssn = $employee['ssn'];
	            $this_plan = \ClientPlan::where("name", $employee["plan"])->get()->first();
	            $db_employee->client_plan_id = @$this_plan->id;
	            $this_insurance_network = \DictionaryValue::where("name", $employee["insurance_network"])->get()->first();
	            $db_employee->insurance_network_id = @$this_insurance_network->id;
	            $db_employee->insurance_type = $employee['insurance_type'];
	            $db_employee->plan_coverage_termination_date = @$employee['plan_coverage_termination_date'];
	            $db_employee->work_department = @$employee['work_department'];

    			$db_employee->save();

		        foreach ($employee["dependants"] as $dependant) {
		        	$db_dependent = new \ClientEmployee;
	            	$db_dependent->client_id = $db_client->id;
	            	$db_dependent->parent_id = $db_employee->id;

	            	$db_dependent->type = "dependant";

	            	$db_dependent->first_name = $dependant['first_name'];
		            $db_dependent->last_name = $dependant['last_name'];
		            $db_dependent->gender = $dependant['gender'];
		            $db_dependent->cell_phone = $dependant['cell_phone'];
		            $db_dependent->home_phone = $dependant['home_phone'];
		            $db_dependent->work_phone = $dependant['work_phone'];
		            $db_dependent->birthday = $dependant['birthday'];

		            $db_dependent->street = $dependant['street'];
		            $db_dependent->city = $dependant['city'];
		            $db_dependent->country = $dependant['country'];
		            $db_dependent->state = $dependant['state'];
		            $db_dependent->zip = $dependant['zip'];

		            $db_dependent->dependent_status = $dependant['dependent_status'];
		            $db_dependent->save();
		        }
            }
            $countClient++;
        }
    }

    public function getImportStaff()
    {
	    $staff_urls = array();

        for ($i=0; $i<=3; $i++) {
        	$offset = $i * 4 + 1;
        	$html = $this->extractDataFromURL("http://216.15.147.121:7070/administrator/managers/pager?list.offset=".($offset));

	        $exploded = explode('<div class="actions">       <a href="/administrator/managers/', $html);
	        $count = 0;

	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		$exploded2 = explode('"', $item);
	        		$staff_urls[] = "http://216.15.147.121:7070/administrator/managers/".trim($exploded2[0]);
	        	}
	        	$count++;
	        }
	    }

        foreach ($staff_urls as $url) {
        	$client = array();
        	$html = $this->extractDataFromURL($url);

	        $staff_member = array(
	        	"email" => $this->extractElementValue($html, '<label for="email">', 'value="', '"'),
	            "first_name" => $this->extractElementValue($html, '<label for="firstName">', 'value="', '"'),
	            "last_name" => $this->extractElementValue($html, '<label for="lastName">', 'value="', '"'),
	            "initials" => $this->extractElementValue($html, '<label for="initials">', 'value="', '"'),
	            "phone" => $this->extractElementValue($html, '<label for="phone">', 'value="', '"'),
	            "cell_phone" => $this->extractElementValue($html, '<label for="cellPhone">', 'value="', '"'),
	            "personal_email" => $this->extractElementValue($html, '<label for="personalEmail">', 'value="', '"'),
	            "birthday" => date("Y-m-d", strtotime($this->extractElementValue($html, '<label for="birthday">', 'value="', '"'))),
	            "ssn" => $this->extractElementValue($html, '<label for="socialSecurityNumber">', 'value="', '"'),
	            "hire_date" => date("Y-m-d", strtotime($this->extractElementValue($html, '<label for="hireDate">', 'value="', '"'))),

	            "street" => $this->extractElementValue($html, '<label for="address.streetAddress">', 'value="', '"'),
	            "city" => $this->extractElementValue($html, '<label for="address.city">', 'value="', '"'),
	            "country" => \Common::getCountryCode(trim(strip_tags($this->extractElementValue($html, '<label for="address.country">', 'selected="selected">', '</option>')))),
	            "state" => \Common::getStateCode($this->extractElementValue($html, 'name="address.state"', 'value="', '"')),
	            "zip" => $this->extractElementValue($html, '<label for="address.zipCode">', 'value="', '"'),

	            "bank_name" => $this->extractElementValue($html, '<label for="bankAccount.bankName">', 'value="', '"'),
	            "bank_routing" => $this->extractElementValue($html, '<label for="bankAccount.bankRoutingNumber">', 'value="', '"'),
	            "bank_account_number" => $this->extractElementValue($html, '<label for="bankAccount.accountNumber">', 'value="', '"'),
	            "bank_termination_date" => $this->extractElementValue($html, '<label for="bankAccount.terminationDate">', 'value="', '"')
	        );

		    $db_user = new \User;
		    $db_user->type = "user";
            $db_user->parent_id = 1;
            $db_user->password = \Hash::make("temporary_pass");

		    $db_user->email = $staff_member["email"];
            $db_user->first_name = $staff_member["first_name"];
            $db_user->last_name = $staff_member["last_name"];
            $db_user->initials = $staff_member["initials"];
            $db_user->phone = $staff_member["phone"];
            $db_user->cell_phone = $staff_member["cell_phone"];
            $db_user->personal_email = $staff_member["personal_email"];
            $db_user->birthday = $staff_member["birthday"];
            $db_user->ssn = $staff_member["ssn"];
            $db_user->hire_date = $staff_member["hire_date"];

            $db_user->street = $staff_member["street"];
            $db_user->city = $staff_member["city"];
            $db_user->country = $staff_member["country"];
            $db_user->state = $staff_member["state"];
            $db_user->zip = $staff_member["zip"];

            $db_user->bank_name = $staff_member["bank_name"];
            $db_user->bank_routing = $staff_member["bank_routing"];
            $db_user->bank_account_number = $staff_member["bank_account_number"];
            $db_user->bank_termination_date = $staff_member["bank_termination_date"];

            $db_user->save();

            $role_id = 8;

            $db_user->roles()->attach($role_id);

            $role = \Role::where("id", $role_id)->get()->first();

            foreach ($role->perms()->get() as $permission)
                $db_user->attachPermission($permission);
        }
    }

    public function getImportProviders()
    {
	    $providers_urls = array();

        for ($i=67; $i<=201; $i++) {
        //for ($i=0; $i<=1; $i++) {
        	$offset = $i * 15 + 1;
        	$html = $this->extractDataFromURL("http://216.15.147.121:7070/administrator/providers/pager?list.offset=".($offset)."&sort=UID&sortOrder=ASCENDING");

	        $exploded = explode("location.href='", $html);
	        $count = 0;

	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		$exploded2 = explode("'", $item);
	        		$providers_urls[] = "http://216.15.147.121:7070".trim($exploded2[0]);
	        	}
	        	$count++;
	        }
	    }

        foreach ($providers_urls as $key => $url) {
        	//if ($key < 3) continue;
        	$client = array();

        	$edit_url = str_replace("profile", "edit", $url);
        	$html = $this->extractDataFromURL($edit_url);

	        $type = (strpos($html, '<label for="gender">Gender</label>') === false) ? "Company" : "Person";
	        $provider = array(
	        	"type" => $type,
	        	
	        	"name" => $this->extractElementValue($html, '<label for="name">', 'value="', '"'),
	            "pos_code" => $this->extractElementValue($html, 'name="posCode"', 'selected="selected">', '</option>'),
	            "tax_id" => $this->extractElementValue($html, '<label for="taxID">', 'value="', '"'),
	            "npi" => $this->extractElementValue($html, '<label for="nationalProviderIdentifier">', 'value="', '"'),
	            "gender" => $this->extractElementValue($html, 'name="gender"', 'selected="selected">', '</option>'),
	            "birthday" => $this->extractElementValue($html, '<label for="birthday">', 'value="', '"'),
	            "memo" => $this->extractElementValue($html, '<label for="memo">', 'name="memo">', '</textarea>'),

	            "primary_street" => $this->extractElementValue($html, '<label for="primaryAddress.streetAddress">', 'value="', '"'),
	            "primary_city" => $this->extractElementValue($html, '<label for="primaryAddress.city">', 'value="', '"'),
	            "primary_country" => \Common::getCountryCode($this->extractElementValue($html, '<label for="primaryAddress.country">', 'selected="selected">', '</option>')),
	            "primary_state" => \Common::getStateCode($this->extractElementValue($html, 'name="primaryAddress.state"', 'value="', '"')),
	            "primary_zip" => $this->extractElementValue($html, '<label for="primaryAddress.zipCode">', 'value="', '"'),

	            "secondary_address_available" => (!empty($this->extractElementValue($html, '<label for="secondaryAddress.streetAddress">', 'value="', '"'))) ? 1 : 0,
	            "secondary_street" => $this->extractElementValue($html, '<label for="secondaryAddress.streetAddress">', 'value="', '"'),
	            "secondary_city" => $this->extractElementValue($html, '<label for="secondaryAddress.city">', 'value="', '"'),
	            "secondary_country" => \Common::getCountryCode($this->extractElementValue($html, '<label for="secondaryAddress.country">', 'selected="selected">', '</option>')),
	            "secondary_state" => \Common::getStateCode($this->extractElementValue($html, 'name="secondaryAddress.state"', 'value="', '"')),
	            "secondary_zip" => $this->extractElementValue($html, '<label for="secondaryAddress.zipCode">', 'value="', '"'),

	            "mailing_same_as_primary" => (empty($this->extractElementValue($html, '<label for="mailingAddress.streetAddress">', 'value="', '"'))) ? 1 : 0,
	            "mailing_street" => $this->extractElementValue($html, '<label for="mailingAddress.streetAddress">', 'value="', '"'),
	            "mailing_city" => $this->extractElementValue($html, '<label for="mailingAddress.city">', 'value="', '"'),
	            "mailing_country" => \Common::getCountryCode($this->extractElementValue($html, '<label for="mailingAddress.country">', 'selected="selected">', '</option>')),
	            "mailing_state" => \Common::getStateCode($this->extractElementValue($html, 'name="mailingAddress.state"', 'value="', '"')),
	            "mailing_zip" => $this->extractElementValue($html, '<label for="mailingAddress.zipCode">', 'value="', '"'),

	            "contacts" => array(),
	            "specialities" => "",
	            "insurance_networks" => "",
	            "licenses" => array(),
	            "languages" => "",
	            "staff" => array(),
	            "costs" => array()
	        );

			/* EXTRACT CONTACTS */
			$contacts_section = $this->extractElementValue($html, 'Contact Information</a></h3>', 'Specialities</a></h3>');

			$exploded = explode('<div class="list-input-element"', $contacts_section);

			$count = 0;

	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		$title_identifier = $this->extractElementValue($item, '.title" class="min-width-field"', 'value="', '"');

	        		$provider["contacts"][] = array(
	        			"title" => $this->extractElementValue($item, $title_identifier.'">', '</option>'),
	        			"first_name" => $this->extractElementValue($item, 'firstName">', 'value="', '"'),
	        			"last_name" => $this->extractElementValue($item, 'lastName">', 'value="', '"'),
	        			"email" => $this->extractElementValue($item, 'email">', 'value="', '"'),
	        			"phone" => $this->extractElementValue($item, 'phone">', 'value="', '"'),
	        			"fax" => $this->extractElementValue($item, 'fax">', 'value="', '"'),
	        			"cell_phone" => $this->extractElementValue($item, 'cellPhone">', 'value="', '"'),
	        			"additional_info" => $this->extractElementValue($item, '<textarea', 'additionalInfo">', '</textarea>'),
	        		);
	        	}
	        	$count++;
	        }
	        /* EXTRACT CONTACTS */

			/* EXTRACT SPECIALITIES */
			$specialities_section = $this->extractElementValue($html, 'Specialities</a></h3>', 'Insurance Networks</a></h3>');

			$exploded = explode('<input id="specialities', $specialities_section);
			$count = 0;

			$specialities = array();
	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		if (strpos($item, "checked") !== false) {
	        			$specialities[] = $this->extractElementValue($item, '<label for="', '">', '</label>');
	        		}
	        	}
	        	$count++;
	        }

	        $provider["specialities"] = implode(", ", $specialities);
	        /* EXTRACT SPECIALITIES */

			/* EXTRACT INSURANCE NETWORKS */
			$insurance_networks_section = $this->extractElementValue($html, 'Insurance Networks</a></h3>', 'Licenses</a></h3>');

			$exploded = explode('<input id="insuranceNetworks', $insurance_networks_section);
			$count = 0;

			$insurance_networks = array();
	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		if (strpos($item, "checked") !== false) {
	        			$insurance_networks[] = $this->extractElementValue($item, '<label for="', '">', '</label>');
	        		}
	        	}
	        	$count++;
	        }

	        $provider["insurance_networks"] = implode(", ", $insurance_networks);
	        /* EXTRACT INSURANCE NETWORKS */

			/* EXTRACT LICENSES */
			$licenses_section = $this->extractElementValue($html, 'Licenses</a></h3>', 'Spoken Languages</a></h3>');

			$exploded = explode('<div class="list-input-element">', $licenses_section);
			$count = 0;

	        foreach ($exploded as $item) {
	        	if ($count > 0) {
        			$license = array(
        				"number" => $this->extractElementValue($item, '.number">', 'value="', '"'),
        				"title" => $this->extractElementValue($item, '<input name="licenses[', 'value="', '"'),
        				"expiration_date" => $this->extractElementValue($item, '.expirationDate">', 'value="', '"')
        			);

        			$provider["licenses"][] = $license;
	        	}
	        	$count++;
	        }

	        /* EXTRACT LICENSES */

			/* EXTRACT LANGUAGES */
			$languages_section = $this->extractElementValue($html, 'Spoken Languages</a></h3>', '<div id="page-footer">');

			$exploded = explode('<input id="spokenLanguages', $languages_section);
			$count = 0;

			$languages = array();
	        foreach ($exploded as $item) {
	        	if ($count > 0) {
	        		if (strpos($item, "checked") !== false) {
	        			$languages[] = $this->extractElementValue($item, '<label for="', '">', '</label>');
	        		}
	        	}
	        	$count++;
	        }

	        $provider["languages"] = implode(", ", $languages);
	        /* EXTRACT LANGUAGES */

	        /* EXTRACT STAFF */
	        $hasStaff = true;
	        $staffCount = 0;
	        $staff_urls = array();
	        while ($hasStaff) {
		    	$offset = $staffCount * 15 + 1;
		    	$staff_url = str_replace("profile", "staff", $url);
	        	$html = $this->extractDataFromURL($staff_url);

		        $html2 = $this->extractElementValue($html, '<div id="list" class="pager">', '<div id="bottom">');
		        $exploded = explode("location.href='", $html2);

		        $prev_count = count($staff_urls);

		        $count = 0;
		        foreach ($exploded as $item) {
		        	if ($count > 0) {
		        		$exploded2 = explode("'", $item);
		        		$this_url = "http://216.15.147.121:7070".trim($exploded2[0]);
		        		if (!in_array($this_url, $staff_urls))
		        			$staff_urls[] = $this_url;
		        	}
		        	$count++;
		        }

		        if ($prev_count == count($staff_urls))
		        	$hasStaff = false;

	        	$staffCount++;
	        }

	        foreach ($staff_urls as $staff_url) {
	        	$html = $this->extractDataFromURL($staff_url);

		        $staff = array(
		        	"first_name" => $this->extractElementValue($html, '<label for="firstName">', 'value="', '"'),
		        	"last_name" => $this->extractElementValue($html, '<label for="lastName">', 'value="', '"'),
		        	"memo" => $this->extractElementValue($html, '<label for="memo">', 'name="memo">', '</textarea>')
		        );

		        $provider["staff"][] = $staff;
	        }
			/* EXTRACT STAFF */

	        /* EXTRACT COSTS */
	        $rates_url = str_replace("profile", "rates", $url);
	        $html = $this->extractDataFromURL($rates_url);

	        $html2 = $this->extractElementValue($html, '<div id="list" class="pager">', '<div id="bottom">');
	        $exploded = explode('<div class="info-block action-block">', $html2);

	        $count = 0;
	        foreach ($exploded as $item) {
	        	if ($count > 0) {
		        	$type = $this->extractElementValue($item, '<label>Type</label>', '<br>');
		        	$dn = ($type == "EAP")
		        			? $this->extractElementValue($item, '<label>EAP Service</label>', '<br>')
		        			: $this->extractElementValue($item, ' - ', '<br>');

		        	$dv = \DictionaryValue::where("name", $dn)->get()->first();

			        $cost = array(
			        	"name" => $this->extractElementValue($item, '<span class="title">', '</span>'),
			        	"type" => $type,
			        	"dictionary_value_id" => (int)@$dv->id,
			        	"representative" => $this->extractElementValue($item, '<label>CNA Representative</label>', '<br>'),
			        	"cost" => (float)str_replace(",", "", $this->extractElementValue($item, '<label>Cost</label>$', '<br>')),
			        	"signing_date" => $this->extractElementValue($item, '<label>Signing Date</label>', '<br>')
			        );

			        $provider["costs"][] = $cost;
			    }
		        $count++;
	        }
			/* EXTRACT COSTS */

			//echo "<pre>";
			//var_dump($provider);
			//exit();

		    $db_provider = new \Provider;
		    $db_provider->type = $provider["type"];
		    $db_provider->name = $provider["name"];
		    $this_pos_code = \DictionaryValue::where("name", $provider["pos_code"])->get()->first();

		    if ($this_pos_code) {
		        $db_provider->pos_code_id = @$this_pos_code->id;
	            $db_provider->tax_id = $provider["tax_id"];
	            $db_provider->npi = $provider["npi"];
	            if ($provider["type"] == "Person") {
		            $db_provider->gender = $provider["gender"];
		            $db_provider->birthday = $provider["birthday"];
	            }
	            $db_provider->memo = $provider["memo"];

	            $db_provider->primary_street = $provider["primary_street"];
	            $db_provider->primary_city = $provider["primary_city"];
	            $db_provider->primary_country = $provider["primary_country"];
	            $db_provider->primary_state = $provider["primary_state"];
	            $db_provider->primary_zip = $provider["primary_zip"];

	            $db_provider->secondary_address_available = $provider["secondary_address_available"];
	            $db_provider->secondary_street = $provider["secondary_street"];
	            $db_provider->secondary_city = $provider["secondary_city"];
	            $db_provider->secondary_country = $provider["secondary_country"];
	            $db_provider->secondary_state = $provider["secondary_state"];
	            $db_provider->secondary_zip = $provider["secondary_zip"];

	            $db_provider->mailing_same_as_primary = $provider["mailing_same_as_primary"];
	            $db_provider->mailing_street = $provider["mailing_street"];
	            $db_provider->mailing_city = $provider["mailing_city"];
	            $db_provider->mailing_country = $provider["mailing_country"];
	            $db_provider->mailing_state = $provider["mailing_state"];
	            $db_provider->mailing_zip = $provider["mailing_zip"];

	            $db_provider->specialities = $provider["specialities"];
	            $db_provider->insurance_networks = $provider["insurance_networks"];
	            $db_provider->languages = $provider["languages"];

	            $db_provider->save();

	            foreach ($provider["contacts"] as $contact) {
	            	$db_contact = new \Contact;
	            	$db_contact->owner_id = $db_provider->id;
	            	$db_contact->type = "provider";
	            	$db_contact->title = $contact["title"];
	    			$db_contact->first_name = $contact["first_name"];
	    			$db_contact->last_name = $contact["last_name"];
	    			$db_contact->email = $contact["email"];
	    			$db_contact->phone = $contact["phone"];
	    			$db_contact->fax = $contact["fax"];
	    			$db_contact->cell_phone = $contact["cell_phone"];
	    			$db_contact->additional_info = $contact["additional_info"];
	    			$db_contact->save();
	            }

	            foreach ($provider["licenses"] as $license) {
	            	if (!empty($license["title"])) {
		            	$db_license = new \ProviderLicense;
		            	$db_license->provider_id = $db_provider->id;
		            	$db_license->number = $license["number"];
		    			$db_license->title = $license["title"];
		    			$db_license->expiration_date = $license["expiration_date"];
		    			$db_license->save();
	    			}
	            }

	            foreach ($provider["staff"] as $staff_member) {
	            	if (!empty($staff_member["first_name"])) {
		            	$db_staff_member = new \ProviderStaff;
		            	$db_staff_member->provider_id = $db_provider->id;
		            	$db_staff_member->first_name = $staff_member["first_name"];
		    			$db_staff_member->last_name = $staff_member["last_name"];
		    			$db_staff_member->memo = $staff_member["memo"];
		    			$db_staff_member->save();
	    			}
	            }

	            foreach ($provider["costs"] as $cost) {
	            	$db_cost = new \ProviderCost;
	            	$db_cost->provider_id = $db_provider->id;
	            	$db_cost->name = $cost["name"];
	            	$db_cost->type = $cost["type"];
	    			$db_cost->dictionary_value_id = $cost["dictionary_value_id"];
	    			$db_cost->representative = $cost["representative"];
	    			$db_cost->cost = $cost["cost"];
	    			$db_cost->signing_date = $cost["signing_date"];
	    			$db_cost->save();
	            }
	        }
        }
    }

    public function getImportCases()
    {
	    $types = array("OMC", "EAP");

	    foreach ($types as $type) {
	    	$length = ($type == "EAP") ? 359 : 0;

	    	$cases_urls = array();

	        for ($i=0; $i<=$length; $i++) {
	        	$offset = $i * 15 + 1;
	        	$html = $this->extractDataFromURL("http://216.15.147.121:7070/administrator/call-cases/pager?list.offset=".($offset)."&sort=CALL_START_TIME&sortOrder=ASCENDING&callCaseType=".$type);

		        $exploded = explode("location.href='", $html);
		        $count = 0;

		        foreach ($exploded as $item) {
		        	if ($count > 0) {
		        		$exploded2 = explode("'", $item);
		        		$cases_urls[] = "http://216.15.147.121:7070".trim($exploded2[0]);
		        	}
		        	$count++;
		        }
		    }

	        foreach ($cases_urls as $key => $url) {
	        	//if ($key < 3) continue;

	        	$edit_url = str_replace("profile", "edit", $url);
	        	$html = $this->extractDataFromURL($url);

		        $enrollee_data = $this->extractElementValue($html, 'Enrollee</h2>', '</div><br>');
		        $employee_data = $this->extractElementValue($html, 'Employee</h2>', '</div><br>');

		        $comments_section = $this->extractElementValue($html, '<span class="title">Notes</span>', '<div id="bottomLeft">');

		        $case = array(
		        	"type" => $type,
		        	
		        	"UID" => $this->extractElementValue($html, '<label>UID</label>', '<br>'),
		            "creator_name" => $this->extractElementValue($html, '<label>Creator</label>', '</div>'),
		            "caller_name" => $this->extractElementValue($enrollee_data, '<label>First Name</label>', '<br>')." ".$this->extractElementValue($enrollee_data, '<label>Last Name</label>', '<br>'),
		            "company_name" => $this->extractElementValue($enrollee_data, '<label>Company</label>', '</div>'),
		            "enrollee_data" => json_encode(array(
		                "enrollee" => array(
		                    "first_name" => $this->extractElementValue($enrollee_data, '<label>First Name</label>', '<br>'),
		                    "last_name" => $this->extractElementValue($enrollee_data, '<label>Last Name</label>', '<br>'),
		                    "gender" => $this->extractElementValue($enrollee_data, '<label>Gender</label>', '<br>'),
		                    "cell_phone" => $this->extractElementValue($enrollee_data, '<label>Cell Phone</label>', '<br>'),
		                    "home_phone" => $this->extractElementValue($enrollee_data, '<label>Home Phone</label>', '<br>'),
		                    "work_phone" => $this->extractElementValue($enrollee_data, '<label>Work Phone</label>', '<br>'),
		                    "birthday" => $this->extractElementValue($enrollee_data, '<label>Birthday</label>', '<br>'),
		                    "plan" => $this->extractElementValue($enrollee_data, '<label>Plan</label>', '<br>'),
		                    "insurance_network" => $this->extractElementValue($enrollee_data, '<label>Insurance Network</label>', '<br>'),
		                    "insurance_type" => $this->extractElementValue($enrollee_data, '<label>Insurance Type</label>', '<br>'),
		                    "client" => $this->extractElementValue($enrollee_data, '<label>Company</label>', '</div>'),
		                    "address" => trim(strip_tags($this->extractElementValue($enrollee_data, '<label>Address</label>', '</div></div>'))),
		                ),
		                "employee" => array(
		                    "first_name" => $this->extractElementValue($employee_data, '<label>First Name</label>', '<br>'),
		                    "last_name" => $this->extractElementValue($employee_data, '<label>Last Name</label>', '<br>'),
		                    "gender" => $this->extractElementValue($employee_data, '<label>Gender</label>', '<br>'),
		                    "cell_phone" => $this->extractElementValue($employee_data, '<label>Cell Phone</label>', '<br>'),
		                    "home_phone" => $this->extractElementValue($employee_data, '<label>Home Phone</label>', '<br>'),
		                    "work_phone" => $this->extractElementValue($employee_data, '<label>Work Phone</label>', '<br>'),
		                    "birthday" => $this->extractElementValue($employee_data, '<label>Birthday</label>', '<br>'),
		                    "plan" => $this->extractElementValue($employee_data, '<label>Plan</label>', '<br>'),
		                    "insurance_network" => $this->extractElementValue($employee_data, '<label>Insurance Network</label>', '<br>'),
		                    "insurance_type" => $this->extractElementValue($employee_data, '<label>Insurance Type</label>', '<br>'),
		                    "client" => $this->extractElementValue($employee_data, '<label>Company</label>', '</div>'),
		                    "address" => trim(strip_tags($this->extractElementValue($employee_data, '<label>Address</label>', '</div></div>'))),
		                )
		            )),
		            "problems" => $this->extractElementValue($html, '<label>Presenting Problems</label>', '<br>'),
		            "preliminary_diagnosis" => trim(strip_tags($this->extractElementValue($html, '<label>Preliminary Diagnosis</label>', '<br>'))),
		            "referral_type" => $this->extractElementValue($html, '<label>Referral Type</label>', '<br>'),
		            "call_start" => date("Y-m-d H:i:s", strtotime($this->extractElementValue($html, '<label>Call Start</label>', '<br>'))),
		            "call_duration" => $this->extractElementValue($html, '<label>Call Duration</label>', '<br>'),
		            "call_center_number" => $this->extractElementValue($html, '<label>Call Center Number</label>', '</div>'),

		            "authorization_pages" => array(),
		            "comments" => array()
		        );

		        /* EXTRACT AUTHORIZATION PAGES */
		        $apages_data = $this->extractElementValue($html, 'Add Authorization Page', '<span class="title">Notes</span>');

		        $count = 0;
		        $exploded = explode('<div style="overflow: hidden;">', $apages_data);

		        foreach ($exploded as $item) {
		        	if ($count > 0) {
		        		$exploded2 = explode('"', $item);

				        $apage = array(
				            "UID" =>  $this->extractElementValue($item, '<label>UID</label>', '</div>'),
				            "creator_name" =>  $this->extractElementValue($item, '<label>Creator</label>', '</div>')
				        );

				        $case["authorization_pages"][] = $apage;
		        	}
		        	$count++;
		        }

		        $apages_urls = array();
		        $count = 0;
		        $exploded = explode('<a href="', $apages_data);

		        foreach ($exploded as $item) {
		        	if ($count > 0) {
		        		$exploded2 = explode('"', $item);
		        		$apages_urls[] = "http://216.15.147.121:7070".trim($exploded2[0]);
		        	}
		        	$count++;
		        }

		        $count = 0;
		        foreach ($apages_urls as $apage_url) {
		        	$html = $this->extractDataFromURL($apage_url);

		        	$apage = $case["authorization_pages"][$count];
			        $this_apage = array(
			        	"created_at" => date("Y-m-d H:i:s", strtotime($this->extractElementValue($html, '<label>Creation Date</label>', '</div>'))),
			        	"provider_data" => json_encode(array(
			                "name" => $this->extractElementValue($html, '<label>Provider Name</label>', '<br>'),
			                "address" => trim(strip_tags($this->extractElementValue($html, '<label>Address</label>', '</div></div>'))),
			                "pos_code" =>  $this->extractElementValue($html, '<label>POS Code</label>', '<br>')
			            )),
			            "date_from" =>  date("Y-m-d", strtotime($this->extractElementValue($html, '<label>Date Period</label>', ' -'))),
			            "date_to" =>  date("Y-m-d", strtotime($this->extractElementValue($html, '<label>Date Period</label>', ' - ', '<br>'))),
			            "units" =>  $this->extractElementValue($html, '<label>Units</label>', '</div>'),
			            "max_payment" =>  $this->extractElementValue($html, '<label>Maximum Payment</label>', '<br>'),
			            "patient_copay" =>  $this->extractElementValue($html, '<label>Patient Co-pay</label>', '<br>'),
			            "remarks" =>  trim(strip_tags($this->extractElementValue($html, '<label>Remarks</label>', '</div>')))
			        );

					if ($type == "EAP") {
						$dictionary_value = $this->extractElementValue($html, '<label>EAP Service</label>', '<br>');
						$this_dictionary = \DictionaryValue::where("name", $dictionary_value)->get()->first();
					}
					else {
						$dictionary_value = $this->extractElementValue($html, '<label>CPT Code</label>', ' -');
						$this_dictionary = \DictionaryValue::where("code", $dictionary_value)->get()->first();
					}

					$apage["dictionary_value_id"] = @$this_dictionary->id;

			        $case["authorization_pages"][$count] = array_merge($apage, $this_apage);
			        $count++;
		        }
				/* EXTRACT AUTHORIZATION PAGES */

				//if (!$apage["dictionary_value_id"]) continue;

				/* EXTRACT COMMENTS */
				$exploded = explode('<div style="overflow: hidden; margin-bottom: 10px;">', $comments_section);

				$count = 0;

		        foreach ($exploded as $item) {
		        	if ($count > 0) {
		        		$case["comments"][] = array(
		        			"created_at" => date("Y-m-d H:i:s", strtotime($this->extractElementValue($item, '<div style="float: left;">', ', by'))),
		        			"comment" => $this->extractElementValue($item, '<div>', '</div>'),
		        			"user_name" => $this->extractElementValue($item, ', by ', '</div>')
		        		);
		        	}
		        	$count++;
		        }
		        /* EXTRACT COMMENTS */

		        /*
				echo "<pre>";
				var_dump($case);
				exit();
				*/

			    $db_case = new \ECase;
			    $db_case->type = $case["type"];
			    $db_case->UID = $case["UID"];
	            $db_case->creator_name = $case["creator_name"];
	            $db_case->caller_name = $case["caller_name"];
	            $db_case->company_name = $case["company_name"];
	            $db_case->enrollee_data = $case["enrollee_data"];
	            $db_case->problems = $case["problems"];
	            $db_case->preliminary_diagnosis = $case["preliminary_diagnosis"];
	            $db_case->referral_type = $case["referral_type"];
	            $db_case->call_start = $case["call_start"];
	            $db_case->call_duration = $case["call_duration"];
	            $db_case->call_center_number = $case["call_center_number"];

	            $db_case->save();

	            foreach ($case["authorization_pages"] as $apage) {
	            	$db_apage = new \CaseAuthorizationPage;
	            	$db_apage->case_id = $db_case->id;
	            	$db_apage->creator_name = $apage["creator_name"];
	            	$db_apage->provider_data = $apage["provider_data"];
			        $db_apage->dictionary_value_id = $apage["dictionary_value_id"];
		            $db_apage->date_from = $apage["date_from"];
		            $db_apage->date_to = $apage["date_to"];
		            $db_apage->units = $apage["units"];
		            $db_apage->max_payment = $apage["max_payment"];
		            $db_apage->patient_copay = $apage["patient_copay"];
		            $db_apage->remarks = $apage["remarks"];
		            $db_apage->UID = $apage["UID"];
	    			$db_apage->save();

			        $db_apage->created_at = $apage["created_at"];
	    			$db_apage->save();
	            }

	            foreach ($case["comments"] as $comment) {
	            	$db_comment = new \Comment;
	            	$db_comment->owner_id = $db_case->id;
	            	$db_comment->type = "case";
	            	$db_comment->user_name = $comment["user_name"];
	            	$db_comment->comment = $comment["comment"];
	    			$db_comment->save();

			        $db_comment->created_at = $comment["created_at"];
	    			$db_comment->save();
	    		}
	        }
	    }
    }

    private function extractElementValue($html, $par1 = null, $par2 = null, $par3 = null, $par4 = null) {
    	$result = "";
    	if ($par1) {
    		$exploded = explode($par1, $html);
    		if (count($exploded) > 1) {
	    		if ($par2) {
	    			$exploded2 = explode($par2, $exploded[1]);
	    			if (count($exploded2) > 1) {
		    			if ($par3) {
			    			$exploded3 = explode($par3, $exploded2[1]);
			    			if (count($exploded3) > 1) {
				    			if ($par4) {
					    			$exploded4 = explode($par4, $exploded3[1]);

					    			if (count($exploded4) > 1)
					    				$result = trim($exploded4[0]);
					    		}
					    		else
				    				$result = trim($exploded3[0]);
					    	}
			    			
			    		}
			    		else
			    			$result = trim($exploded2[0]);
			    	}
	    		}
	    		else
	    			$result = trim($exploded[0]);
	    	}
    	}

    	return trim($result);
    }

    private function extractDataFromURL($url) {
    	$casper = new \Casper();
    	$casper->setOptions(array(
		      'ignore-ssl-errors' => 'yes',
		      'ssl-protocol' => 'any'
	    ));
	    $casper->start("http://216.15.147.121:7070/sign-in");
	    $casper->waitForSelector('form.quick-sign-in-form');
    	$casper->sendKeys('form.quick-sign-in-form input[name="j_username"]', "admin@admin.com");
    	$casper->sendKeys('form.quick-sign-in-form input[name="j_password"]', "admin@admin.com");
    	$casper->click('input[type="submit"]');
    	$casper->thenOpen($url);
	    $casper->run();

	    $output = json_encode($casper->getOutput());
	    $exploded = explode('[CURRENT_HTML]', $output);
        $html = stripslashes($exploded[1]);
        $html = str_replace('","', " ", $html);

        return $html;
    }
}