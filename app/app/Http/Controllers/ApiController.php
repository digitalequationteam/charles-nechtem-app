<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ApiController extends Controller
{
    public function postGetStates($country_code) {
        return response()->json(array('states' => \Common::getStates($country_code)));
    }

    public function getCheckUserActive() {
        return response()->json(array('activated' => \Auth::user()->active));
    }

    public function getFetchCountriesAndStates() {
        $all_data = StaticData::$countries_and_states;
        foreach (\Common::getAllCountries() as $code => $name) {
            //if ($code == "US") {
                $url = 'http://reputation.thinkbigenterprises.com/default/business-owner-registration/get-states';
                $fields = array(
                    'country' => urlencode($code)
                );

                $fields_string = "";
                //url-ify the data for the POST
                foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                rtrim($fields_string, '&');

                //open connection
                $ch = curl_init();

                //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, count($fields));
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

                //execute post
                $result = curl_exec($ch);

                $states = array();

                $exploded = explode('</option><option label="', $result);

                $count = 0;
                foreach ($exploded as $item) {
                    if ($count > 1) {
                        $exploded2 = explode('"', $item);
                        $state = $exploded2[0];

                        $states[] = $state;
                    }
                    $count++;
                }

                $all_data["countries"][$code]["states"] = $states;
            //}
        }

        var_export($all_data);
        exit();
    }

    public function getStagingRestrictionSettings() {
        return response()->json(\Common::getAdminUser()->getSettings());
    }

    public function postSaveMenuType() {
        \Auth::user()->saveSetting("menu_type", \Input::get("menu_type"));
        \Auth::user()->saveSetting("layout_type", \Input::get("layout_type"));

        if (\Input::get("color"))
            \Auth::user()->saveSetting("color", \Input::get("color"));

        return response()->json(array('status' => "ok"));
    }

    public function postUpdateUserActivity() {
        $login_session = \Auth::user()->login_sessions()->where("id", \Session::get("login_session_id"))->get()->first();

        $login_session->last_activity = date("Y-m-d H:i:s");
        $login_session->save();

        return response()->json(array('status' => "ok"));
    }
}