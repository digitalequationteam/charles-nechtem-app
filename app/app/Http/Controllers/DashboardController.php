<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class DashboardController extends Controller
{
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

    public function getIndex($only_me = false)
    {
        return view('users.admin.dashboard', array("only_me" => $only_me));
    }

    public function getMe()
    {
        return $this->getIndex(true);
    }
}