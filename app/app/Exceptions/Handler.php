<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Setting;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        /*
        if (SERVER_ENVIRONMENT == "live") {
            $exploded = explode(".", $_SERVER["REMOTE_ADDR"]);
            $array = explode("\r\n", @Setting::get("admin.debugging.ips"));

            $variant1 = $_SERVER["REMOTE_ADDR"];
            $variant2 = $exploded[0].".x.x.x";
            $variant3 = $exploded[0].".".$exploded[1].".x.x";
            $variant4 = $exploded[0].".".$exploded[1].".".$exploded[2].".x";

            $exists = (
                in_array($variant1, $array) ||
                in_array($variant2, $array) ||
                in_array($variant3, $array) ||
                in_array($variant4, $array)
            );

            if (!$exists) {
                echo view("errors.520");
                exit();
            }
        }
        */
        return parent::render($request, $e);
    }
}
