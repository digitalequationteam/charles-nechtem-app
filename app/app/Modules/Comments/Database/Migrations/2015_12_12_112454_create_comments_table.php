<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('comments', function($table)
        {
            $table->increments('id');
            $table->integer('owner_id')->default(0);
            $table->string('type')->default('case'); //case/claim/etc...
            $table->integer('user_id')->default(0);
            $table->string('user_name', 255);
            $table->string('comment', 1000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}