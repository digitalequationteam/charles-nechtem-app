<?php

namespace App\Modules\Comments\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class CommentsController extends Controller
{	
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

    public function postAddComment()
    {
        if (empty(trim(\Input::get("comment"))))
            return response()->json(array('status' => "error"));

        if (\Input::get("comment_id")) {
            $comment = \Comment::where("id", \Input::get("comment_id"))->get()->first();

            if ($comment->type == "case") {
                $case = \ECase::where("id", $comment->owner_id)->get()->first();
                \SecurityLog::addNew("updated_case_comment", \Auth::user()->getName()." updated comment for case <strong>".$case->uid."</strong>.");
            }
            elseif ($comment->type == "claim") {
                $claim = \Claim::where("id", $comment->owner_id)->get()->first();
                \SecurityLog::addNew("updated_claim_comment", \Auth::user()->getName()." updated comment for claim <strong>".$claim->UID."</strong>.");
            }
        }
        else {
            $comment = new \Comment;
            $comment->owner_id = \Input::get("owner_id");
            $comment->type = \Input::get("owner_type");
            $comment->user_id = \Auth::user()->id;
            $comment->user_name = \Auth::user()->first_name." ".\Auth::user()->last_name;

            if ($comment->type == "case") {
                $case = \ECase::where("id", $comment->owner_id)->get()->first();
                \SecurityLog::addNew("added_case_comment", \Auth::user()->getName()." added comment for case <strong>".$case->uid."</strong>.");
            }
            elseif ($comment->type == "claim") {
                $claim = \Claim::where("id", $comment->owner_id)->get()->first();
                \SecurityLog::addNew("added_claim_comment", \Auth::user()->getName()." added comment for claim <strong>".$claim->UID."</strong>.");
            }
        }
        $comment->comment = \Input::get("comment");
        $comment->save();

        //if $comment->type

        return response()->json(array('status' => "ok", "html" => (string)\View::make('comments::widgets.comments_single', ['comment' => $comment])));
    }

    public function postGetComments() {
        $html = '';
        //(string)\View::make('comments::widgets.comments_single', ['comment' => $comment]))

        foreach (\Comment::where("owner_id", \Input::get("owner_id"))->where("type", \Input::get("owner_type"))->orderBy("created_at", \Input::get("order"))->get() as $comment) {
            $html .= (string)\View::make('comments::widgets.comments_single', ['comment' => $comment]);
        }

        return response()->json(array('status' => "ok", "html" => $html));
    }

    public function postDeleteComment()
    {
        $comment = \Comment::where("id", \Input::get("comment_id"))->get()->first();

        if ($comment->type == "case") {
            $case = \ECase::where("id", $comment->owner_id)->get()->first();
            \SecurityLog::addNew("deleted_case_comment", \Auth::user()->getName()." deleted comment from case <strong>".$case->uid."</strong>.");
        }
        elseif ($comment->type == "claim") {
            $claim = \Claim::where("id", $comment->owner_id)->get()->first();
            \SecurityLog::addNew("deleted_claim_comment", \Auth::user()->getName()." deleted comment from claim <strong>".$claim->UID."</strong>.");
        }

        $comment->delete();

        return response()->json(array('status' => "ok", "html" => (string)\View::make('comments::widgets.comments_single', ['comment' => $comment])));
    }
}