<?php
namespace App\Modules\Comments\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class CommentsServiceProvider extends ServiceProvider
{
	/**
	 * Register the Comments module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Comments\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Comments module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('comments', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('comments', base_path('resources/views/vendor/comments'));
		View::addNamespace('comments', realpath(__DIR__.'/../Resources/Views'));
	}
}
