<div class="panel fade active in">
    <div class="panel-body no-padding-hr">
        <div class="row p-20">
            <div class="col-md-12">
                <h3 class="m-t-0">Comments</h3>                    
            </div>
        </div>

        <div class="row p-20">
	        <textarea id="comment-text" class="form-control" style="height: 100px; margin-bottom: 10px;" placeholder="Add Comment..."></textarea>
	        <button class="btn btn-success" onclick="addComment();">Add</button>
	    </div>

	    <div class="pull-right">
        	<select id="sort-type-dd">
        		<option value="ASC">Oldest on top</option>
        		<option value="DESC">Newest on top</option>
        	</select>
	    </div>

	    <!-- <div class="pull-right" style="padding-right: 5px; padding-top: 5px;">
        	Comments order: 
	    </div> -->

        <div id="comments-wrapper">
	        @foreach ($comments_owner->comments()->orderBy("created_at", "ASC")->get() as $comment)
	        	@include('comments::widgets.comments_single')
	        @endforeach
	    </div>
    </div>
</div>

<script type="text/javascript">
	function addComment() {
		var comment = $("#comment-text").val();
		$.post("/comments/add-comment", {
			owner_id: {{$comments_owner->id}},
			owner_type: "{{$owner_type}}",
			comment: comment
		}, function(response) {
			if (response.status == "ok") {
				if ($("#sort-type-dd").val() == "ASC")
					$("#comments-wrapper").append(response.html);
				else
					$("#comments-wrapper").prepend(response.html);
			}
			else
				$("#comment-text").focus();

			$("#comment-text").val('');
		}, "json");
	}

	$(document).ready(function() {
		$("#sort-type-dd").change(function() {
			$("#comments-wrapper").css("opacity", "0.3");
			var comment = $("#comment-text").val();
			$.post("/comments/get-comments", {
				owner_id: {{$comments_owner->id}},
				owner_type: "{{$owner_type}}",
				order: $("#sort-type-dd").val()
			}, function(response) {
				$("#comments-wrapper").html(response.html);
				$("#comments-wrapper").css("opacity", "1");
			}, "json");
		});

		$(".edit-comment-btn").live("click", function(e) {
			e.preventDefault();
			var parent = $(this).parents("#comment-wrapper");
			parent.find(".comment-text").hide();
			parent.find(".comment-editable").show().focus();
			parent.find(".comment-footer").hide();
		});

		$(".btn-cancel-edit-comment").live("click", function(e) {
			e.preventDefault();
			var parent = $(this).parents("#comment-wrapper");
			parent.find(".comment-text").show();
			parent.find(".comment-editable").hide().focus();
			parent.find(".comment-footer").show();
		});

		$(".btn-save-edit-comment").live("click", function(e) {
			e.preventDefault();
			var parent = $(this).parents("#comment-wrapper");
			var comment = parent.find("textarea").val();
			$.post("/comments/add-comment", {
				comment_id: parent.attr("data-id"),
				comment: comment
			}, function(response) {
				if (response.status == "ok")
					parent.replaceWith(response.html);
				else
					parent.find("textarea").focus();
			}, "json");
		});

		$(".delete-comment-btn").live("click", function(e) {
			if (confirm('Are you sure you want to delete this comment?')) {
				var parent = $(this).parents("#comment-wrapper");
				parent.slideUp("fast", function() {
					parent.remove();
				});
				$.post("/comments/delete-comment", {
					comment_id: parent.attr("data-id")
				}, function(response) {}, "json");
			}
			return;
		});
	});
</script>