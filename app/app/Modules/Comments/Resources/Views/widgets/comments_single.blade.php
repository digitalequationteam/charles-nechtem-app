<div id="comment-wrapper" data-id="{{$comment->id}}">
    <div class="col-md-12 p-30 comment m-t-0">
        <div class="row">
            <div class="comment-content">
                <div class="comment-header f-16 clearfix">
                    <div class="pull-left">
                        <a href="javascript: void(0);">{{$comment->user_name}}</a>
                    </div>
                    <div class="pull-right f-12">
                        <?php
                        $date = new DateTime($comment->created_at, new DateTimeZone('UTC'));
                        $date->setTimezone(new DateTimeZone('America/New_York'));
                        ?>

                       <span class="c-gray">{{$date->format("m/d/Y H:i A")}}</span>
                    </div>
                </div>
                <p class="comment-text">
                    {!! nl2br($comment->comment) !!}
                </p>
                <p class="comment-editable" style="display: none;">
                    <textarea class="form-control" style="height: 100px; margin-bottom: 10px;">{{$comment->comment}}</textarea>
                    <button class="btn btn-success btn-xs btn-save-edit-comment">Save</button>
                    <button class="btn btn-default btn-xs btn-cancel-edit-comment">Cancel</button>
                </p>
                <div class="comment-footer">
                    <div>
                    	<a class="btn btn-success btn-xs tooltips edit-comment-btn" href="#" data-original-title="Edit Comment"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-danger btn-xs tooltips delete-comment-btn ui-bootbox-alert" href="javascript: void(0);" data-original-title="Delete Comment" data-title="Delete Comment"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix div-separator"></div>
</div>