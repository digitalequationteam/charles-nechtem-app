<?php

namespace App\Modules\InterfaceMenu\Models;

use Illuminate\Database\Eloquent\Model;

class CustomPageLink extends Model
{
    protected $table = 'custom_page_links';

	protected $hidden = array();

    public function menu() {
        return $this->belongsTo( '\Menu' );
    }
}