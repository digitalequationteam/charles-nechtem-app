<?php

namespace App\Modules\InterfaceMenu\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

	protected $hidden = array();

    public static $saveRules = array(
        'name'=>'required|min:2'
    );

    public function parent() {
        return $this->belongsTo( '\Menu', 'parent_id');
    }

    public function children() {
        return $this->hasMany( '\Menu', 'parent_id');
    }

    public function links() {
        return $this->hasMany( '\CustomPageLink');
    }

    public function getSidebarURL() {
        $url = "";
        if ($this->link_type != "url") {
            if ($this->parent_id == 0)
                $url .= '/';
            else {
                $parent = $this->parent()->get()->first();

                if ($parent->parent_id > 0)
                    $url .= '/'.$parent->parent()->first()->key;

                $url .= '/'.$parent->key.'/';
            }

            if ($this->type == 'custom')
                $url = '/page/'.$this->key;
            else
                $url .= $this->key;
        }
        else {
            $url = $this->url;
        }

        return $url;
    }
}