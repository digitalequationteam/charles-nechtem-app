@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/interface/pages">Interface</a></li>
        <li class="active">Pages</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Manage Pages</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getAddPage', $user_type) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span> Add Page</a>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

					    <span class="hidden">
							Choose area:
							<select onchange="window.location = '{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getPages')}}/' + this.value" style="width: 100px;">
								<option value="admin">Global Admin</option>
								<option value="company" @if ($user_type == "company") selected="selected" @endif>Company Account</option>
								<option value="location" @if ($user_type == "location") selected="selected" @endif>Location Account</option>
							</select>
						</span>

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
							<table class="table table-tools table-hover" id="menu_table">
							<thead>
							<tr>
								<th>
									 Page
								</th>
								<th>
									 URL Path
								</th>
								<th style="text-align: center;">
									 Actions
								</th>
							</tr>
							</thead>
							<tbody>
								<?php
								$count = 0;
								foreach ($menu_items->where("parent_id", 0)->get() as $menu_item) {?>
									<tr class="odd gradeX">
										<td>
											 {{$menu_item->name}}
										</td>
										<td>
											/{{$user_type}}/page/{{$menu_item->key}}
										</td>
										<td style="text-align: center;">
											<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getEditPage', array($menu_item->id)) }}" class="btn bg-success tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Menu Item"><i class="fa fa-pencil"></i></a>
											<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getDeletePage', array($menu_item->id)) }}" class="btn bg-danger tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Menu Item"><i class="fa fa-times"></i></a>
										</td>
									</tr>
								<?php 
									$count++;
									foreach (Menu::where("parent_id", $menu_item->id)->get() as $sub_menu_item) {?>
										<tr class="odd gradeX">
											<td>
												 {{$sub_menu_item->name}}
											</td>
											<td>
												 @if ($sub_menu_item->parent_id > 0)
												 	{{Menu::where("id", $sub_menu_item->parent_id)->get()->first()->name}}
												 @else
												 	-
												 @endif
											</td>
											<td style="text-align: center;">
												<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getEditMenu', array($sub_menu_item->id)) }}" class="btn bg-success tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Menu Item"><i class="fa fa-pencil"></i></a>
												<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getDeleteMenu', array($sub_menu_item->id)) }}" class="btn bg-danger tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Menu Item"><i class="fa fa-times"></i></a>
											</td>
										</tr>
									<?php 
										$count++;
										}
									}
								?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
@stop