@extends('layouts.main')
@section('content')
	<script type="text/javascript">
		document.domain = '{{MAIN_DOMAIN}}';
	</script>

	<iframe id="myIframe" onload="$(this).show(); $('#iframe_loading').hide();" src="?scrape=true" style="width: 100%; border: 0px; padding: 0px; margin: 0px; display: none;"></iframe>

	<div id="iframe_loading" style="position: width: 100%; height: 100%; text-align: center; min-height: 600px; background-image: url(https://www.hereofamily.com/wp-content/themes/HereO/images/ajax-loader.gif); background-repeat: no-repeat; background-position: 50% 50%;"></div>

	<script type="text/javascript">
		if (typeof(init) != "undefined") {
			init.push(function() {
				doThis();
			});
		}
		else {
			$(document).ready(function() {
				doThis();
			});
		}
		
		function doThis() {
			if ($("#main-menu-bg").length == 0)
				$("#myIframe").css('height', ($("#sidebar").height() - 38) + "px");
			else {
				$("#myIframe").css('height', ($("#main-menu-bg").height() - 38) + "px");
				$("#myIframe").css('position', 'relative');
				$("#myIframe").css('margin-left', '0px');
				$("#myIframe").css('margin-top', '0px');
			}
		}
	</script>

	<style type="text/css">
		html, body {
			overflow: hidden !important;
		}
		#main-content {
			padding: 0px;
			margin: 0px;
		}

		::-webkit-scrollbar {width:10px;}  
		::-webkit-scrollbar-track {background-color: #eaeaea;border-left: 1px solid #c1c1c1;}
		::-webkit-scrollbar-thumb {background-color: #c1c1c1;}  
		::-webkit-scrollbar-thumb:hover { background-color: #aaa; }  
		::-webkit-scrollbar-track {border-radius: 0;box-shadow: none;border: 0;}
		::-webkit-scrollbar-thumb {border-radius: 0;box-shadow: none;border: 0;} 
	</style>
@stop