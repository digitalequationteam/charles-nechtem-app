@extends('layouts.main')
@section('content')

	@if (!@$edit || ($edit && !Auth::user()->hasRole("super_admin")))
		<style type="text/css">
			#main-content {
				padding: 0px !important;
			}
		</style>

		<div><img id="actual_photo" src="/uploads/{{$menu_item->image}}" style="width: 100%;" usemap="#links" /></div>

		<?php 
		$links = $menu_item->links()->get();

		if (count($links) > 0) {
			$sizes = json_decode($links[0]->sizes, true);
			?>
			<input type="hidden" id="image_width" value="600"  />
			<input type="hidden" id="image_height" value="{{$sizes['height']}}" />
			<map name="links">
				@foreach ($links as $link)
					<?php
					$coordinates = json_decode($link->coordinates, true);
					$coordinates = join($coordinates, ",");
					?>
			  		<area shape="rect" coords="{{$coordinates}}" href="{{$link->url}}" alt="Sun">
			  	@endforeach
			</map>
		<?php } ?>

		<script type="text/javascript">
			$(document).ready(function () {
				imageMapResize();
				//$("img#photo").mapify();
			});
		</script>
	@else
		<link rel="stylesheet" type="text/css" href="/css/imgareaselect-animated.css" />
		<link rel="stylesheet" type="text/css" href="/css/imgareaselect-default.css" />


		<div class="panel">
			<div class="panel-heading">
				<div class="pull-right">
					<a href="{{$menu_item->getSidebarURL()}}" class="btn btn-success">Go to the Page</a>
					@if ($menu_item->category == "menu")
						<a href="{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getEditMenu', $menu_item->id)}}" class="btn btn-success">Manage Page</a>
					@elseif ($menu_item->category == "page")
						<a href="{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getEditPage', $menu_item->id)}}" class="btn btn-success">Manage Page</a>
					@endif
				</div>
				<span class="panel-title">Add a Link</span>
			</div>
			<div class="panel-body">
				  <div style="float: left; width: 50%;">
				 
				    <div class="frame" style="width: 600px; height: auto;">
				      <img id="photo" src="/uploads/{{$menu_item->image}}" style="width: 600px;" usemap="#links" />
				    </div>
					<?php 
					$links = $menu_item->links()->get();

					if (count($links) > 0) {
						$sizes = json_decode($links[0]->sizes, true);
						?>
						<input type="hidden" id="image_width" value="600"  />
						<input type="hidden" id="image_height" value="{{$sizes['height']}}" />
						<map id="links" name="links">
							@foreach ($links as $this_link)
								<?php
								$coordinates = json_decode($this_link->coordinates, true);
								$coordinates = join($coordinates, ",");
								?>
						  		<area shape="rect" class="map_tooltips" data-original-title="" coords="{{$coordinates}}" href="/page/{{$menu_item->key}}/edit/{{$this_link->id}}" alt="{{$this_link->name}}" title="{{$this_link->name}}">
						  	@endforeach
						</map>
					<?php } ?>

					<script type="text/javascript">
						$(document).ready(function () {
							imageMapResize();
							$('#photo').maphilight({alwaysOn: true});
							//$("img#photo").mapify();
						});
					</script>

				    <p class="instructions">
				      Click and drag on the image to select an area. 
				    </p>
				  </div>
				 
				  <div style="width: 100%; padding-left: 20px;">
				  	<div style="display: none;">
					    <p style="font-size: 110%; font-weight: bold; padding-left: 0.1em;">
					      Selection Preview
					    </p>
					  
					    <div class="frame" 
					      style="margin: 0 1em;width: 200px; height: auto;">
					      <div id="preview" style="width: 200px; height: auto; overflow: hidden;">
					        <img src="/uploads/{{$menu_item->image}}" style="width: 200px; height: auto;" />
					      </div>
					    </div>
					</div>

				    <table class="table" style="margin-top: 1em; max-width: 600px;">
				      <thead>
				        <tr style="display: none;">
				          <th colspan="2" style="font-size: 110%; font-weight: bold; text-align: left; padding-left: 0.1em;">
				            Coordinates
				          </th>
				          <th colspan="2" style="font-size: 110%; font-weight: bold; text-align: left; padding-left: 0.1em;">
				            Dimensions
				          </th>
				        </tr>
				      </thead>
				      <tbody>
				        <tr style="display: none;">
				          <td style="width: 10%;"><b>X<sub>1</sub>:</b></td>
				 		      <td style="width: 30%;"><input type="text" class="form-control" id="x1" value="-" disabled /></td>
				 		      <td style="width: 20%; padding-left: 10px;"><b>Width:</b></td>
				   		    <td><input type="text" class="form-control" value="-" disabled id="w" /></td>
				        </tr>
				        <tr style="display: none;">
				          <td><b>Y<sub>1</sub>:</b></td>
				          <td><input type="text" class="form-control" id="y1" value="-" disabled /></td>
				          <td style="padding-left: 10px;"><b>Height:</b></td>
				          <td><input type="text" class="form-control" id="h" value="-" disabled /></td>
				        </tr>
				        <tr style="display: none;">
				          <td><b>X<sub>2</sub>:</b></td>
				          <td><input type="text" class="form-control" id="x2" value="-" disabled /></td>
				          <td><b>Y<sub>2</sub>:</b></td>
				          <td><input type="text" class="form-control" id="y2" value="" /></td>
				        </tr>
				        <tr id="saveLinkArea" style="display: none;">
				          <td><b>Name:</b></td>
				          <td><input type="text" class="form-control" id="name" value="{{@$link->name}}" /></td>
				          <td><b>URL:</b></td>
				          <td><input type="text" class="form-control" id="url" value="{{@$link->url}}" /></td>
				          <td><b>&nbsp;</b></td>
				          <td><button class="btn btn-success" onclick="saveImageLink();">Save</button></td>
				        </tr>
				      </tbody>
				    </table>
				  </div>
				</div>
			</div>
		</div>

		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title">Current Links</span>
			</div>
			<div class="panel-body">
				@if(Session::has('message'))
			        <div class="alert alert-success">
			            <button type="button" class="close" data-dismiss="alert">×</button>
			            <span>
			                {{ Session::get('message') }}
			            </span>
				    </div>
			    @endif
				<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
					<table class="table table-tools table-hover" id="companies_table">
					<thead>
					<tr>
						<th>
							 Link Name
						</th>
						<th>
							 URL
						</th>
						<th style="text-align: center;">
							 Actions
						</th>
					</tr>
					</thead>
					<tbody>
						<?php
						$count = 0;
						foreach ($menu_item->links()->get() as $this_link) {?>
							<tr class="odd gradeX">
								<td>
									 <?php echo $this_link->name; ?>
								</td>
								<td>
									 <?php echo $this_link->url; ?>
								</td>
								<td style="text-align: center;">
									<a href="/page/{{$menu_item->key}}/edit/{{$this_link->id}}" class="btn bg-success tooltips" data-toggle="tooltip" title="" data-original-title="Edit Link"><i class="fa fa-pencil"></i></a>
									<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getDeleteMenuLink', array($this_link->id)) }}" class="btn bg-danger tooltips" data-toggle="tooltip" title="" data-original-title="Delete Link"><i class="fa fa-times"></i></a>
								</td>
							</tr>
						<?php 
							$count++;
							}
						?>
					</tbody>
					</table>
				</div>
			</div>
		</div>

	    <script type="text/javascript">
	    	var ias;
			$(document).ready(function () {
			    ias = $('img#photo').imgAreaSelect({
			        handles: true,
			        onSelectChange: preview,
			        onInit: preview,
			        aspectRatio: 'auto',
			        instance: true
			    });

			    @if ($link)
			    	ias.setSelection({{join(json_decode($link->coordinates, true), ",")}}, true);
					ias.setOptions({ show: true });
					ias.update();
			    @endif

			    $(".map_tooltips").qtip({
				    content: {
				        attr: 'alt'
				    },
				    position: {
				        target: 'mouse', // Position it where the click was...
				        adjust: { mouse: false } // ...but don't follow the mouse
				    },
				    style: { classes: 'qtip-dark' }
				});
			});

			function preview(img, selection) {
			    if (!selection.width || !selection.height)
			        return;
			    
			    var scaleX = 100 / selection.width;
			    var scaleY = 100 / selection.height;

			    $('#preview img').css({
			        width: Math.round(scaleX * $("#photo").width()),
			        height: Math.round(scaleY * $("#photo").width()),
			        marginLeft: -Math.round(scaleX * selection.x1),
			        marginTop: -Math.round(scaleY * selection.y1)
			    });

			    $('#x1').val(selection.x1);
			    $('#y1').val(selection.y1);
			    $('#x2').val(selection.x2);
			    $('#y2').val(selection.y2);
			    $('#w').val(selection.width);
			    $('#h').val(selection.height);    

			    $("#saveLinkArea").show();
			}

			function saveImageLink() {
				if ($("#name").val() == "") {
					$("#name").focus();
					return false;
				}

				if ($("#url").val() == "") {
					$("#url").focus();
					return false;
				}

				$.post("/interface/save-image-link", {
					menu_id: {{$menu_item->id}},
					name: $("#name").val(),
					url: $("#url").val(),
					coordinates: {
						x1: $('#x1').val(),
						y1: $('#y1').val(),
						x2: $('#x2').val(),
						y2: $('#y2').val()
					},
					sizes: {
						width: $('img#photo').width(),
						height: $('img#photo').height()
					},
					@if ($link)
						link_id: {{$link->id}}
					@endif
				}, function(response) {
					//alert('Image saved! You will be taken to the page to see your link in action.');
					window.location = '/page/{{$menu_item->key}}/edit/';
				});
			}
		</script>
	@endif
@stop