@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/interface/menu">Interface</a></li>
        <li class="active">Menu</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Manage Menu</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getAddMenu', $user_type) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span> Add Menu Item</a>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						@if($errors->all())
					        <div class="alert alert-danger">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            @foreach($errors->all() as $error_msg)
					        
					                <span>
					                 {{ $error_msg }}
					                </span><br/>
					            @endforeach
					        </div>
					    @endif

					    <span class="hidden">
							Choose area:
							<select onchange="window.location = '{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu')}}/' + this.value" class="form-control" style="width: 200px;">
								<option value="admin">Global Admin</option>
								<option value="company" @if ($user_type == "company") selected="selected" @endif>Company Account</option>
								<option value="location" @if ($user_type == "location") selected="selected" @endif>Location Account</option>
							</select>
						</span>

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
							<strong>Menu Items:</strong>
							<ol class='default vertical'>
								@foreach ($menu_items->where("parent_id", 0)->orderBy("order", "ASC")->get() as $menu_item)
									<li data-id="{{$menu_item->id}}">
										<div class="pull-left">
											{{$menu_item->name}}
											({{($menu_item->type == "actual") ? 'Actual' : 'Demo'}})
										</div>

										<?php
										$sub_items = \Menu::where("parent_id", $menu_item->id)->orderBy("order", "ASC")->get();
										?>

										<div class="pull-right">
											<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getEditMenu', array($menu_item->id)) }}" class="btn btn-sm bg-success tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Menu Item"><i class="fa fa-pencil"></i></a>
											@if ($menu_item->type != "actual")
												<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getDeleteMenu', array($menu_item->id)) }}" class="btn btn-sm bg-danger tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Menu Item"><i class="fa fa-times"></i></a>
											@endif
										</div>

										<div class="pull-right">
											<input type="checkbox" class="toggle-hide-menu switch" data-size="small" @if (!$menu_item->hidden) checked @endif data-id="{{$menu_item->id}}" />
										</div>

										<div class="clearfix"></div>

											<ol>
										@if (count($sub_items) > 0)
												@foreach ($sub_items as $sub_menu_item)
												<li data-id="{{$sub_menu_item->id}}">
													<div class="pull-left">
														{{$sub_menu_item->name}}
														({{($sub_menu_item->type == "actual") ? 'Actual' : 'Demo'}})
													</div>

													<?php
													$sub_sub_items = \Menu::where("parent_id", $sub_menu_item->id)->orderBy("order", "ASC")->get();
													?>

													<div class="pull-right">
														<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getEditMenu', array($sub_menu_item->id)) }}" class="btn btn-sm bg-success tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Menu Item"><i class="fa fa-pencil"></i></a>

														@if ($sub_menu_item->type != "actual")
															<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getDeleteMenu', array($sub_menu_item->id)) }}" class="btn btn-sm bg-danger tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Menu Item"><i class="fa fa-times"></i></a>
														@endif
													</div>

													<div class="pull-right">
														<input type="checkbox" class="toggle-hide-menu switch" data-size="small" @if (!$sub_menu_item->hidden) checked @endif data-id="{{$sub_menu_item->id}}" />
													</div>

													<div class="clearfix"></div>

													<ol>
													@if (count($sub_sub_items) > 0)
															@foreach ($sub_sub_items as $sub_sub_menu_item)
															<li data-id="{{$sub_sub_menu_item->id}}">
																<div class="pull-left">
																	{{$sub_sub_menu_item->name}}
																	({{($sub_sub_menu_item->type == "actual") ? 'Actual' : 'Demo'}})
																</div>

																<div class="pull-right">
																	<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getEditMenu', array($sub_sub_menu_item->id)) }}" class="btn btn-sm bg-success tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Menu Item"><i class="fa fa-pencil"></i></a>

																	@if ($sub_sub_menu_item->type != "actual")
																		<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getDeleteMenu', array($sub_sub_menu_item->id)) }}" class="btn btn-sm bg-danger tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Menu Item"><i class="fa fa-times"></i></a>
																	@endif
																</div>

																<div class="pull-right">
																	<input type="checkbox" class="toggle-hide-menu switch" data-size="small" @if (!$sub_sub_menu_item->hidden) checked @endif data-id="{{$sub_sub_menu_item->id}}" />
																</div>

																<div class="clearfix"></div>
															</li>
															@endforeach
													@endif
														</ol>
												</li>
												@endforeach
										@endif
											</ol>
									</li>
								@endforeach
						    </ol>
						</div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

    <script type="text/javascript">
        $(function  () {
			var group = $("ol.default").sortable({
				group: 'default',
				delay: 500,
				isValidTarget: function($item, container) {
					var isValid = true;

					if ($item.find("li").length > 0) {
						if (container.target.parents("li").length > 0)
							isValid = false;
					}

					return isValid;
				},
				onDrop: function ($item, container, _super) {
					var data = group.sortable("serialize").get();

				    var jsonString = JSON.stringify(data, null, ' ');

				    $('#serialize_output2').text(jsonString);
				    _super($item, container);

				    $.post("{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@postSaveMenuOrder')}}", {
				    	user_type: '{{$user_type}}',
				    	menu_items: $.parseJSON(jsonString)
				    }, function(response) {}, "json");
				}
			});
		});

		$(document).ready(function() {
			$(".switch").bootstrapSwitch('size', 'small');

			$('.switch').on('switchChange.bootstrapSwitch', function (event, state) {
			    console.log(state.el);
			    console.log();
			    $.post("{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@postToggleHideMenu')}}", {
					id: state.el.attr("data-id"),
					hidden: !state.value
				}, function(response){}, "json");
			});
		});
    </script>
@stop