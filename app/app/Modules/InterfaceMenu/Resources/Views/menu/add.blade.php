@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/interface/menu">Interface</a></li>
        <li><a href="/interface/menu">Menu</a></li>
        <li class="active">Add Menu Item</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Add Menu Item</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span> Back</a>
	</div>
	-->

	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@postSaveMenu')}}" class="panel form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<input type="hidden" name="user_type" value="{{$user_type}}" />			
		<div class="row">
			<div class="col-md-12">
				@if($errors->all())
			        <div class="alert alert-danger">
			            <button type="button" class="close" data-dismiss="alert">×</button>
			            @foreach($errors->all() as $error_msg)
			        
			                <span>
			                 {{ $error_msg }}
			                </span><br/>
			            @endforeach
			        </div>
			    @endif

				<div class="panel-body no-padding-hr">
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Title <span class="text-danger">*</span></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" maxlength="100" name="name" id="name" value="{{ old('name') }}" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Order #</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" maxlength="100" name="order" id="order" value="<?php echo (old()) ? Input::old('order') : (\Menu::where("user_type", $user_type)->where("parent_id", 0)->count() + 1); ?>" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Parent Item</label>
						<div class="col-sm-4">
							<select class="form-control" name="parent_id">
								<option value="">No Parent</option>
								@foreach (\Menu::where("user_type", $user_type)->where("parent_id", 0)->orderBy("order", "ASC")->get() as $menu_item)
									<option value="{{$menu_item->id}}" @if (Input::old("parent_id") == $menu_item->id) selected="selected" @endif>{{@$menu_item->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Icon</label>
						<div class="col-sm-4">
							<div class="input-group">
                                <input data-placement="bottomRight" name="icon_class" id="icon_class" class="form-control icp icp-auto" value="{{ (old()) ? old('icon_class') : 'fa-anchor' }}" type="text" />
                                <span class="input-group-addon"></span>
                            </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Type</label>
						<div class="col-sm-4">
							<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type4" value="image_page" <?php if (!old() || Input::old("link_type") == "image_page") { ?>checked<?php } ?> onclick="$('.for-image').show(); $('.for-url').hide();" /> Image </label>
							<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type5" value="url" <?php if (Input::old("link_type") == "url") { ?>checked<?php } ?> onclick="$('.for-image').hide(); $('.for-url').show();" /> Link to Page </label>
							<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type5" value="iframe" <?php if (Input::old("link_type") == "iframe") { ?>checked<?php } ?> onclick="$('.for-image').hide(); $('.for-url').show();" /> Iframe </label>
							<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type5" value="wp_page" <?php if (Input::old("link_type") == "wp_page") { ?>checked<?php } ?> onclick="$('.for-image').hide(); $('.for-url').show();" /> WP Admin Page </label>
							<!--<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type5" value="scrape" <?php if (Input::old("link_type") == "scrape") { ?>checked<?php } ?> onclick="$('.for-image').hide(); $('.for-url').show();" /> Scrape </label>-->
						</div>
					</div>
					<div class="form-group for-image no-margin-hr panel-padding-h" <@if (old() && Input::old("link_type") != "image_page") style="display: none;" @endif>
						<label class="control-label col-sm-4">Image</label>
						<div class="col-sm-4">
							<div class="input-group">
                                <input type="file" name="image" />
                            </div>
						</div>
					</div>
					<div class="form-group for-url no-margin-hr panel-padding-h" <@if (Input::old("link_type") != "url" && Input::old("link_type") != "iframe" && Input::old("link_type") != "scrape") style="display: none;" @endif>
						<label class="control-label col-sm-4">URL</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" maxlength="100" name="url" id="url" value="{{ old('url') }}" />
						</div>
					</div>

					<div class="panel-group panel-accordion col-sm-offset-4 col-sm-4" id="accordion" style="padding: 10px;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed">
                                    Advanced
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
									<div class="form-group no-margin-hr panel-padding-h">
										<label class="control-label">Which Roles Can View This Page</label>
										<div class="">
											@foreach (\Auth::user()->Owner()->my_roles()->where("level", ">", 0)->get() as $role)
												<label>
													<input type="checkbox" name="role_{{$role->id}}" checked /> {{$role->display_name}}
												</label>
												<br />
											@endforeach
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-sm-offset-4 col-sm-4 text-center">
							<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span> Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

    <script>
        $(function() {
            $('.icp-auto').iconpicker();
        });
    </script>
@stop