@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/interface/menu">Interface</a></li>
        <li><a href="/interface/menu">Menu</a></li>
        <li class="active">Edit Menu Item</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Edit Item</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu')}}" class="btn btn-success m-t-10">Manage Menu Items</a>
	</div>
	-->

	<!--
	<div class="pull-right" style="padding-right: 10px;">
		<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span> Back</a>
	</div>
	-->

	<div class="clearfix"></div>

	<form id="menu-item-form" method="post" action="{{action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@postSaveMenu', $menu_item->id)}}" class="panel form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	
		<div class="row">
			<div class="col-md-12">
				@if($errors->all())
			        <div class="alert alert-danger">
			            <button type="button" class="close" data-dismiss="alert">×</button>
			            @foreach($errors->all() as $error_msg)
			        
			                <span>
			                 {{ $error_msg }}
			                </span><br/>
			            @endforeach
			        </div>
			    @endif

				@if(Session::has('message'))
			        <div class="alert alert-success">
			            <button type="button" class="close" data-dismiss="alert">×</button>
			            <span>
			                {{ Session::get('message') }}
			            </span>
				    </div>
			    @endif
				<div class="panel-body no-padding-hr">
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Menu Item Type</label>
						<div class="col-sm-4">
							<button class="btn btn-info" maxlength="100" disabled style="font-weight: bold;">@if ($menu_item->type == 'actual') Actual @else Demo @endif</button>
						</div>
					</div>
					@if ($menu_item->type == "actual")
						<div class="form-group no-margin-hr panel-padding-h">
							<label class="control-label col-sm-4">Original Title</label>
							<div class="col-sm-4">
								<button class="btn btn-info" maxlength="100" disabled style="font-weight: bold;">{{$menu_item->original_name}}</button>
							</div>
						</div>
					@endif
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Title <span class="text-danger">*</span></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" maxlength="100" name="name" id="name" value="{{ (old()) ? old('name') : $menu_item->name }}" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Order #</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" maxlength="100" name="order" id="order" value="<?php echo (old()) ? Input::old('order') : $menu_item->order; ?>" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Parent Item</label>
						<div class="col-sm-4">
							<select class="form-control" name="parent_id">
								<option value="">No Parent</option>
								@foreach (Menu::where("user_type", $menu_item->user_type)->where("parent_id", 0)->where("id", "!=", $menu_item->id)->orderBy("order", "ASC")->get() as $menu_item2)
									<option value="{{$menu_item2->id}}" @if ((old() && Input::old("parent_id")== $menu_item->id) || (!old() && $menu_item2->id == $menu_item->parent_id)) selected="selected" @endif>{{@$menu_item2->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Icon</label>
						<div class="col-sm-4">
							<div class="input-group">
                                <input data-placement="bottomRight" name="icon_class" id="icon_class" class="form-control icp icp-auto" value="{{ (old()) ? old('icon_class') : str_replace('fa fa-', 'fa-', $menu_item->icon_class) }}" type="text" />
                                <span class="input-group-addon"></span>
                            </div>
						</div>
					</div>
					@if ($menu_item->type == "custom")
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Link Type</label>
						<div class="col-sm-4">
							<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type4" value="image_page" <?php if ((!old() && $menu_item->link_type == "image_page") || (old() && Input::old("link_type") == "image_page")) { ?>checked<?php } ?> onclick="$('.for-image').show(); $('.for-url').hide();" /> Image </label>
							<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type5" value="url" <?php if ((!old() && $menu_item->link_type == "url") || (old() && Input::old("link_type") == "url")) { ?>checked<?php } ?> onclick="$('.for-image').hide(); $('.for-url').show();" /> URL </label>
							<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type5" value="iframe" <?php if ((!old() && $menu_item->link_type == "iframe") || (old() && Input::old("link_type") == "url")) { ?>checked<?php } ?> onclick="$('.for-image').hide(); $('.for-url').show();" /> Iframe </label>
							<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type5" value="wp_page" <?php if ((!old() && $menu_item->link_type == "wp_page") || (old() && Input::old("link_type") == "url")) { ?>checked<?php } ?> onclick="$('.for-image').hide(); $('.for-url').show();" /> WP Admin Page </label>
							<!--<label class="radio-inline">
							<input type="radio" name="link_type" id="link_type5" value="scrape" <?php if ((!old() && $menu_item->link_type == "scrape") || (old() && Input::old("link_type") == "url")) { ?>checked<?php } ?> onclick="$('.for-image').hide(); $('.for-url').show();" /> Scrape </label>-->
						</div>
					</div>
					<div class="form-group for-image no-margin-hr panel-padding-h" <@if ((old() && Input::old("link_type") != "image_page") || (!old() && $menu_item->link_type != "image_page")) style="display: none;" @endif>
						<label class="control-label col-sm-4">Image</label>
						<div class="col-sm-4">
							<div class="input-group">
                                <input type="file" name="image" onchange="$('#saveImageBtn').show();" />

                                @if (!empty($menu_item->image))
                                	<div id="current-image">
	                                	<br />
	                                	Current Image:
	                                	<br />
	                                	<img src="/uploads/{{$menu_item->image}}" style="width: 100%;" />

	                                	<div style="padding-top: 10px;">
	                                		<a href="/page/{{$menu_item->key}}/edit" class="btn btn-success"><i class="fa fa-pencil"></i>&nbsp;Edit Image Links</a>
	                                	</div>

	                                	<div style="padding-top: 10px;">
	                                		<a href="javascript: removeImage();" class="btn btn-danger delete-btn" data-original-title="Remove Image"><i class="fa fa-times"></i>&nbsp;Remove Image</a>
	                                	</div>
	                                </div>
                                @endif

                            	<div id="saveImageBtn" style="padding-top: 10px; display: none;">
                            		<button name="saveImage" class="btn btn-success"><i class="fa fa-arrow-right"></i>&nbsp;@if (!empty($menu_item->image)) Update Image @else Add Image @endif</button>
                            	</div>
                            </div>
						</div>
					</div>
					<div class="form-group for-url no-margin-hr panel-padding-h" <@if ((old() && Input::old("link_type") == "image_page") || (!old() && $menu_item->link_type == "image_page")) style="display: none;" @endif>
						<label class="control-label col-sm-4">URL</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" maxlength="100" name="url" id="url" value="{{ (old()) ? old('url') : $menu_item->url }}" />
						</div>
					</div>
					@endif

					<div class="panel-group panel-accordion col-sm-offset-4 col-sm-4" id="accordion" style="padding: 10px;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed">
                                    Advanced
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
									<div class="form-group no-margin-hr panel-padding-h">
										<label class="control-label">Which Roles Can View This Page</label>
										<div class="">
											@foreach (\Auth::user()->Owner()->my_roles()->where("level", ">", 0)->get() as $role)
												<label>
													<input type="checkbox" name="role_{{$role->id}}" @if ($role->hasPermission("admin_".$menu_item->key) || $role->hasPermission("admin_".$menu_item->key."_access")) checked @endif /> {{$role->display_name}}
												</label>
												<br />
											@endforeach
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-sm-offset-4 col-sm-4 text-center">
							<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span> Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

    <script>
        $(function() {
            $('.icp-auto').iconpicker();
        });

        function removeImage() {
        	$("#menu-item-form").append('<input type="hidden" name="remove_image" value="1" />');
        	$("#current-image").remove();
        }
    </script>
@stop