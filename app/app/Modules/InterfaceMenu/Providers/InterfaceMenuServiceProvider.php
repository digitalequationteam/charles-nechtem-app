<?php
namespace App\Modules\InterfaceMenu\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class InterfaceMenuServiceProvider extends ServiceProvider
{
	/**
	 * Register the InterfaceMenu module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\InterfaceMenu\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the InterfaceMenu module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('interface-menu', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('interface-menu', base_path('resources/views/vendor/interface-menu'));
		View::addNamespace('interface-menu', realpath(__DIR__.'/../Resources/Views'));
	}
}
