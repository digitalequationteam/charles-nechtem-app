<?php
namespace App\Modules\InterfaceMenu\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InterfaceMenuDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		//Get admin user.
        $admin = \User::where("type", "admin")->get()->first();

        //Init menu items.
        $default_menu_items = array(
        	"dashboard" => array(
        		"name" => "Dashboard",
        		"type" => "actual",
        		"icon_class" => "fa-dashboard",
                "default_page" => 1,
                "link_type" => "url",
                "url" => "/dashboard"
        	),
            "staff" => array(
                "name" => "Staff",
                "type" => "actual",
                "icon_class" => "fa-users",
                "sub_items" => array(
                    "users" => array(
                        "name" => "Users",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/staff/users"
                    ),
                    "roles-permissions" => array(
                        "name" => "Roles Permissions",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/staff/roles-permissions"
                    )
                )
            ),
            "clients" => array(
                "name" => "Clients",
                "type" => "actual",
                "icon_class" => "fa-briefcase",
                "link_type" => "url",
                "url" => "/clients"
            ),
            "providers" => array(
                "name" => "Providers",
                "type" => "actual",
                "icon_class" => "fa-plus-square-o",
                "link_type" => "url",
                "url" => "/providers"
            ),
            "cases" => array(
                "name" => "Cases",
                "type" => "actual",
                "icon_class" => "fa-file-archive-o",
                "link_type" => "url",
                "url" => "/cases"
            ),
            "claims" => array(
                "name" => "Claims",
                "type" => "actual",
                "icon_class" => "fa-briefcase",
                "link_type" => "url",
                "url" => "/claims"
            ),
            "reports" => array(
                "name" => "Reports",
                "type" => "actual",
                "icon_class" => "fa-bars",
                "link_type" => "url",
                "url" => "/reports"
            ),
            "frontend" => array(
                "name" => "Frontend",
                "type" => "actual",
                "icon_class" => "fa-laptop",
                "sub_items" => array(
                    "view_site" => array(
                        "name" => "View",
                        "type" => "actual"
                    ),
                    "default" => array(
                        "name" => "Default",
                        "type" => "custom",
                        "link_type" => "wp_page",
                        "url" => "customize.php"
                    ),
                    "edit" => array(
                        "name" => "Edit",
                        "type" => "actual",
                        "sub_items" => array(
                            "basic" => array(
                                "name" => "Basic",
                                "type" => "actual",
                                "link_type" => "url",
                                "url" => "/frontend/edit/basic"
                            ),
                            "advanced" => array(
                                "name" => "Advanced",
                                "type" => "actual",
                                "link_type" => "url",
                                "url" => "/frontend/edit/advanced"
                            ),
                        )
                    ),
                    "content" => array(
                        "name" => "Content",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "javascript: void(0);",
                        "sub_items" => array(
                            "posts" => array(
                                "name" => "Posts",
                                "type" => "custom",
                                "link_type" => "wp_page",
                                "url" => "edit.php"
                            )
                        )
                    ),
                    "backup" => array(
                        "name" => "Backup",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/frontend/backup"
                    ),
                    "reset" => array(
                        "name" => "Reset",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/frontend/reset"
                    ),
                    "info" => array(
                        "name" => "Info",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/frontend/info"
                    )
                )
            ),
        	"settings" => array(
        		"name" => "Settings",
        		"type" => "actual",
        		"icon_class" => "fa-cog",
        		"sub_items" => array(
                    "profile" => array(
                        "name" => "Edit Profile",
                        "type" => "actual",
                        "no_permissions" => true,
                        "link_type" => "url",
                        "url" => "/settings/profile"
                    ),
                    "values" => array(
                        "name" => "Manage Values",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/settings/values"
                    ),
                    "import-export" => array(
                        "name" => "Import/Export",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/settings/import-export"
                    ),
                    "ip-access" => array(
                        "name" => "IP Access",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/settings/ip-access"
                    )
        		),
        		"no_permissions" => true
        	),
            "support" => array(
                "name" => "Support",
                "type" => "actual",
                "icon_class" => "fa-exclamation-circle",
                "no_permissions" => true
            ),
            "security" => array(
                "name" => "Security",
                "type" => "actual",
                "icon_class" => "fa-user",
                "sub_items" => array(
                    "log" => array(
                        "name" => "Log",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/security/log"
                    ),
                    "map" => array(
                        "name" => "Map",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/security/map"
                    ),
                    "map-wide" => array(
                        "name" => "Map Wide",
                        "type" => "actual",
                        "link_type" => "url",
                        "url" => "/security/map-wide"
                    )
                )
            )
        );

		//Save default menu items to the database.
		$order = 1;
		foreach ($default_menu_items as $key => $item) {
			$menu_item = \Menu::where("key", $key)->get()->first();

			if (!$menu_item) {
				$menu_item = new \Menu;
	            $menu_item->parent_id = 0;
	            $menu_item->user_type = "admin";
	            $menu_item->type = $item["type"];
	            $menu_item->key = $key;
                $menu_item->name = $item["name"];
                $menu_item->original_name = $item["name"];
	            $menu_item->icon_class = $item["icon_class"];
                $menu_item->order = (isset($item["order"]) ? $item["order"] : $order);
                if (isset($item["link_type"])) {
                    $menu_item->link_type = $item["link_type"];
                    $menu_item->url = $item["url"];
                }
            	if (isset($item["no_permissions"]))
            		$menu_item->require_permission = 0;
                $menu_item->default_page = (int)@$item["default_page"];
	            $menu_item->save();

                /*
                if ($menu_item->type == "custom") {
                    $menu_item->key = $menu_item->key."-".$menu_item->id;
                    $menu_item->save();
                }
                */
			}

			$order2 = 1;
			foreach ((array)@$item["sub_items"] as $key2 => $item2) {
				$sub_menu_item = \Menu::where("key", $key2)->where("parent_id", $menu_item->id)->get()->first();

				if (!$sub_menu_item) {
					$sub_menu_item = new \Menu;
		            $sub_menu_item->parent_id = $menu_item->id;
		            $sub_menu_item->user_type = "admin";
		            $sub_menu_item->type = $item2["type"];
		            $sub_menu_item->key = $key2;
		            $sub_menu_item->name = $item2["name"];
                    $sub_menu_item->original_name = $item2["name"];
                    $sub_menu_item->order = (isset($item2["order"]) ? $item2["order"] : $order2);
                    if (isset($item2["link_type"])) {
                        $sub_menu_item->link_type = $item2["link_type"];
                        $sub_menu_item->url = $item2["url"];
                    }
	            	if (isset($item2["no_permissions"]))
	            		$sub_menu_item->require_permission = 0;
                    $sub_menu_item->default_page = (int)@$item2["default_page"];
		            $sub_menu_item->save();
				}

                if (isset($item2["sub_items"])) {
                    $order3 = 1;
                    foreach ((array)@$item2["sub_items"] as $key3 => $item3) {
                        $sub_sub_menu_item = \Menu::where("key", $key3)->where("parent_id", $sub_menu_item->id)->get()->first();

                        if (!$sub_sub_menu_item) {
                            $sub_sub_menu_item = new \Menu;
                            $sub_sub_menu_item->parent_id = $sub_menu_item->id;
                            $sub_sub_menu_item->user_type = "admin";
                            $sub_sub_menu_item->type = $item3["type"];
                            $sub_sub_menu_item->key = $key3;
                            $sub_sub_menu_item->name = $item3["name"];
                            $sub_sub_menu_item->original_name = $item3["name"];
                            $sub_sub_menu_item->order = (isset($item3["order"]) ? $item3["order"] : $order3);
                            if (isset($item3["link_type"])) {
                                $sub_sub_menu_item->link_type = $item3["link_type"];
                                $sub_sub_menu_item->url = $item3["url"];
                            }
                            if (isset($item3["no_permissions"]))
                                $sub_sub_menu_item->require_permission = 0;
                            $sub_sub_menu_item->default_page = (int)@$item3["default_page"];
                            $sub_sub_menu_item->save();
                        }
                        $order3++;
                    }
                }
				$order2++;
			}
			$order++;
		}
	}

}
