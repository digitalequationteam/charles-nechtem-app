<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function($table)
        {
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->string('user_type', 20); // admin, company or location
            $table->string('type', 255);
            $table->string('key', 255);
            $table->string('name', 255);
            $table->string('original_name', 255);
            $table->string('icon_class', 255)->nullable();
            $table->float('order')->default(0);
            $table->string('image', 255)->nullable();
            $table->boolean('require_permission')->default(1);
            $table->boolean('hidden')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu');
    }
}
