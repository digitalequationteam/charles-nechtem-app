<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'interface'], function() {
	Route::controller('/', '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController');
});

Route::get('page/{key}', [
    'as' => 'page', 'uses' => '\App\Modules\InterfaceMenu\Http\Controllers\PageController@getIndex'
]);

Route::get('page/{key}/{edit}', [
    'as' => 'page', 'uses' => '\App\Modules\InterfaceMenu\Http\Controllers\PageController@getIndex'
]);

Route::get('page/{key}/{edit}/{link_id}', [
    'as' => 'page', 'uses' => '\App\Modules\InterfaceMenu\Http\Controllers\PageController@getIndex'
]);