<?php

namespace App\Modules\InterfaceMenu\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class PageController extends Controller
{
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

    public function getIndex($key, $edit = false, $link_id = null) {
        $menu_item = \Menu::where("key", $key)->get()->first();
        $link = $menu_item->links()->where("id", $link_id)->get()->first();

        if (isset($_GET['scrape'])) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $menu_item->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $output = curl_exec($ch);

            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = strtoupper(substr($output, 0, $header_size));
            $body = substr($output, $header_size);

            $parsed_url = parse_url($menu_item->url);

            $body = str_replace('src="/web', 'src="https://web.archive.org/web', $body);
            $body = str_replace('src="//', 'src="IGNORE_ME', $body);
            $body = str_replace('src="/', 'src="http://'.$parsed_url["host"].'/', $body);
            //$body = str_replace('src="', 'src="'.$page->getCloakedURL()."?u=", $body);
            $body = str_replace('src="IGNORE_ME', 'src="//', $body);

            $body = str_replace('href="/web', 'href="https://web.archive.org/web', $body);
            $body = str_replace('href="//', 'href="IGNORE_ME', $body);
            $body = str_replace('href="/', 'href="http://'.$parsed_url["host"].'/', $body);
            //$body = str_replace('href="', 'href="'.$page->getCloakedURL()."?u=", $body);
            $body = str_replace('href="IGNORE_ME', 'href="//', $body);
            $body = str_replace('href="_css', 'href="http://'.$parsed_url["host"].'/_css', $body);
            $body = str_replace('src="_img', 'src="http://'.$parsed_url["host"].'/_img', $body);
            $body = str_replace('href="p7pmm', 'href="http://'.$parsed_url["host"].'/p7pmm', $body);
            $body = str_replace('src="p7pmm', 'src="http://'.$parsed_url["host"].'/p7pmm', $body);
            $body = str_replace('src="social', 'src="http://'.$parsed_url["host"].'/p7pmm', $body);

            echo $body;
            exit();
        }

        switch ($menu_item->link_type) {
            case 'image_page':
                return view('interface-menu::page.image', array('menu_item' => $menu_item, "edit" => $edit, "link" => $link));
            break;

            case 'iframe':
                return view('interface-menu::page.iframe', array('menu_item' => $menu_item));
            break;

            case 'wp_page':
                return view('interface-menu::page.wp_page', array('menu_item' => $menu_item));
            break;

            case 'scrape':
                return view('interface-menu::page.scrape', array('menu_item' => $menu_item));
            break;
        }
    }
}