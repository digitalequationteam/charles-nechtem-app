<?php

namespace App\Modules\InterfaceMenu\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class InterfaceMenuController extends Controller
{	
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

    public function getRunSeeder() {
        \Artisan::call('db:seed', array('--class'=> "\App\Modules\InterfaceMenu\Database\Seeds\InterfaceMenuDatabaseSeeder"));
    }

	public function getIndex() {
		return $this->getMenu();
	}

    public function getMenu($user_type = "admin")
    {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        $menu_items = \Menu::where("user_type", $user_type)->where("category", "menu");

        return view('interface-menu::menu.index', array("menu_items" => $menu_items, "user_type" => $user_type));
    }

    public function getAddMenu($user_type = "admin")
    {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        return view('interface-menu::menu.add', array("user_type" => $user_type));
    }

    public function getEditMenu($id = null)
    {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        $menu_item = \Menu::where('id', $id)->get()->first();

        return view('interface-menu::menu.edit', array( 'menu_item' => $menu_item ));
    }

    public function postSaveMenu($id = null)
    {
        $validator = \Validator::make( \Input::all(), \Menu::$saveRules );

        if ( $validator->passes() ) {
            if ($id == null)
                $menu_item = new \Menu;
            else
                $menu_item = \Menu::find($id);

            if (isset($_POST["saveImage"])) {
                if (\Input::file('image')) {
                    $fileName = "";
                    $destinationPath = 'uploads'; // upload path
                    $extension = \Input::file('image')->getClientOriginalExtension();
                    $fileName = rand(111111111,999999999).'.'.$extension;
                    \Input::file('image')->move($destinationPath, $fileName);
                    $menu_item->image = $fileName;

                    foreach ($menu_item->links()->get() as $link)
                        $link->delete();
                }
                $menu_item->save();
                return Redirect::back()->with( 'message', 'Image saved!' );
            }

            if (\Input::get("parent_id")) {
                $menu_item->parent_id = \Input::get("parent_id");
                $parent_menu_item = \Menu::where("id", \Input::get("parent_id"))->get()->first();
            }
            else
                $menu_item->parent_id = 0;

            if ($id == null) {
                $menu_item->user_type = \Input::get("user_type");
                $menu_item->type = "custom";
            }
            
            if ($menu_item->type == "custom") {
                if ($id == null)
                    $menu_item->key = \Common::slugify(\Input::get( 'name' ));

                $menu_item->link_type = \Input::get( 'link_type' );
                $menu_item->url = \Input::get( 'url' );
            }

            $menu_item->category = "menu";
            $menu_item->name = \Input::get( 'name' );
            $menu_item->icon_class = \Input::get( 'icon_class' );
            $menu_item->order = \Input::get( 'order' );

            if (\Input::get("remove_image")) {
                $menu_item->image = "";
            }

            if (\Input::file('image')) {
                $fileName = "";
                $destinationPath = 'uploads'; // upload path
                $extension = \Input::file('image')->getClientOriginalExtension();
                $fileName = rand(111111111,999999999).'.'.$extension;
                \Input::file('image')->move($destinationPath, $fileName);
                $menu_item->image = $fileName;

                foreach ($menu_item->links()->get() as $link)
                    $link->delete();
            }

            $menu_item->save();

            if ($id == null) {
                $menu_item->key = $menu_item->key."-".$menu_item->id;
                $menu_item->save();
            }

            if (\Input::get("parent_id")) {
                if ($parent_menu_item->parent_id > 0) {
                    $permission_name = $menu_item->user_type."_".$parent_parent_menu_item->key."_".$parent_menu_item->key."_".$menu_item->key;
                }
                else
                    $permission_name = $menu_item->user_type."_".$parent_menu_item->key."_".$menu_item->key;
            }                
            else {
                $permission_name = $menu_item->user_type."_".$menu_item->key;
            }

            $permission_display_name = $menu_item->name;

            $permission = \Permission::where("name", $permission_name)->get()->first();

            if (!$permission) {
                $permission = new \Permission;
                $permission->name         = $permission_name;
                $permission->display_name = $permission_display_name;
                $permission->save();
            }

            $users = \User::all();

            foreach (\Auth::user()->Owner()->my_roles()->get() as $role) {
                if (isset($_POST["role_".$role->id]) || $role->level == 0) {
                    if (!$role->hasPermission($permission->name))
                        $role->attachPermission($permission);

                    foreach ($users as $user) {
                        if (@$user->getMyRole()->id == @$role->id)
                            if (!$user->hasPermission($permission->name))
                                $user->attachPermission($permission);
                    }
                }
                else {
                    if ($role->hasPermission($permission->name))
                        $role->detachPermission($permission);

                    foreach ($users as $user) {
                        if (@$user->getMyRole()->id == @$role->id)
                            if ($user->hasPermission($permission->name))
                                $user->detachPermission($permission);
                    }
                }
            }

            if ($id == null) {
                \SecurityLog::addNew("add_menu_item", \Auth::user()->getName()." added a new menu item.");
                return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu', $menu_item->user_type )->with( 'message', 'New \menu item saved!' );
            }
            else {
                \SecurityLog::addNew("update_menu_item", \Auth::user()->getName()." updated menu item <strong>".$menu_item->name."</strong>.");
                return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu', $menu_item->user_type )->with( 'message', 'Menu item details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function getDeleteMenu($id = null)
    {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        $menu_item = \Menu::where('id', $id)->where("type", "!=", "actual")->get()->first();
        if ($menu_item) {
            if (\Menu::where("parent_id", $menu_item->id)->where("type", "actual")->orderBy("order", "ASC")->count() > 0) 
                return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu' )->withErrors(["That item could not be deleted because it has actual child items."]);

            \SecurityLog::addNew("delete_menu_item", \Auth::user()->getName()." deleted menu item <strong>".$menu_item->name."</strong>.");

            $menu_item->delete();

            return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu' )->with( 'message', 'Menu item deleted!' );
        }
        else {
            return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu' );
        }
    }

    public function getPages($user_type = "admin")
    {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        $menu_items = \Menu::where("user_type", $user_type)->where("category", "page");

        return view('interface-menu::pages.index', array("menu_items" => $menu_items, "user_type" => $user_type));
    }

    public function getAddPage($user_type = "admin")
    {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        return view('interface-menu::pages.add', array("user_type" => $user_type));
    }

    public function getEditPage($id = null)
    {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        $menu_item = \Menu::where('id', $id)->get()->first();

        return view('interface-menu::pages.edit', array( 'menu_item' => $menu_item ));
    }

    public function postSavePage($id = null)
    {
        $validator = \Validator::make( \Input::all(), \Menu::$saveRules );

        if ( $validator->passes() ) {
            if ($id == null)
                $menu_item = new \Menu;
            else
                $menu_item = \Menu::find($id);

            if (isset($_POST["saveImage"])) {
                if (\Input::file('image')) {
                    $fileName = "";
                    $destinationPath = 'uploads'; // upload path
                    $extension = \Input::file('image')->getClientOriginalExtension();
                    $fileName = rand(111111111,999999999).'.'.$extension;
                    \Input::file('image')->move($destinationPath, $fileName);
                    $menu_item->image = $fileName;

                    foreach ($menu_item->links()->get() as $link)
                        $link->delete();
                }
                $menu_item->save();
                return Redirect::back()->with( 'message', 'Image saved!' );
            }    

            $menu_item->parent_id = 0;

            if ($id == null) {
                $menu_item->user_type = \Input::get("user_type");
                $menu_item->type = "custom";
            }

            $menu_item->key = \Common::slugify(\Input::get( 'name' ))."-".rand(1000000, 9999999);

            $menu_item->category = "page";
            $menu_item->name = \Input::get( 'name' );

            $menu_item->link_type = \Input::get( 'link_type' );
            $menu_item->url = \Input::get( 'url' );

            if (\Input::get("remove_image")) {
                $menu_item->image = "";
            }

            if (\Input::file('image')) {
                $fileName = "";
                $destinationPath = 'uploads'; // upload path
                $extension = \Input::file('image')->getClientOriginalExtension();
                $fileName = rand(111111111,999999999).'.'.$extension;
                \Input::file('image')->move($destinationPath, $fileName);
                $menu_item->image = $fileName;

                foreach ($menu_item->links()->get() as $link)
                    $link->delete();
            }

            $menu_item->save();

            $permission_name = $menu_item->user_type."_".$menu_item->key;

            $permission_display_name = $menu_item->name;

            $permission = \Permission::where("name", $permission_name)->get()->first();

            if (!$permission) {
                $permission = new \Permission;
                $permission->name         = $permission_name;
                $permission->display_name = $permission_display_name;
                $permission->save();
            }

            $users = \User::all();

            foreach (\Auth::user()->Owner()->my_roles()->get() as $role) {
                if (isset($_POST["role_".$role->id]) || $role->level == 0) {
                    if (!$role->hasPermission($permission->name))
                        $role->attachPermission($permission);

                    foreach ($users as $user) {
                        if ($user->getMyRole()->id == $role->id)
                            if (!$user->hasPermission($permission->name))
                                $user->attachPermission($permission);
                    }
                }
                else {
                    if ($role->hasPermission($permission->name))
                        $role->detachPermission($permission);

                    foreach ($users as $user) {
                        if ($user->getMyRole()->id == $role->id)
                            if ($user->hasPermission($permission->name))
                                $user->detachPermission($permission);
                    }
                }
            }


            if ($id == null) {
                \SecurityLog::addNew("add_page", \Auth::user()->getName()." added a new page.");
                return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getPages', $menu_item->user_type )->with( 'message', 'New page saved!' );
            }
            else {
                \SecurityLog::addNew("update_page", \Auth::user()->getName()." updated page <strong>".$menu_item->name."</strong>.");
                return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getPages', $menu_item->user_type )->with( 'message', 'Page details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function getDeletePage($id = null)
    {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        $menu_item = \Menu::where('id', $id)->get()->first();
        if ($menu_item) {
            \SecurityLog::addNew("delete_page", \Auth::user()->getName()." deleted page <strong>".$menu_item->name."</strong>.");

            $menu_item->delete();

            return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getPages' )->with( 'message', 'Page deleted!' );
        }
        else {
            return Redirect::action( '\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getPages' );
        }
    }

    public function getDeleteMenuLink($id) {
        if (!$this->me->hasRole("super_admin")) return view("errors.403");

        $link = \CustomPageLink::where('id', $id)->get()->first();
        $menu_item = $link->menu()->get()->first();
        if ($link) {
            \SecurityLog::addNew("delete_menu_link", \Auth::user()->getName()." deleted image link from menu item <strong>".$menu_item->name."</strong>.");

            $link->delete();

            return redirect("/page/".$menu_item->key."/edit")->with( 'message', 'Link deleted!' );
        }
        else {
            return redirect("/page/".$menu_item->key."/edit");
        }
    }

    public function postSaveMenuOrder() {
        $user_type = \Input::get("user_type");
        $menu_items = \Input::get("menu_items");

        foreach ($menu_items[0] as $key => $value) {
            $menu_item = \Menu::where("id", $value["id"])->get()->first();
            $menu_item->parent_id = 0;
            $menu_item->order = ($key + 1);
            $menu_item->save();

            if (isset($value["children"])) {
                foreach ($value["children"][0] as $key2 => $value2) {
                    $sub_menu_item = \Menu::where("id", $value2["id"])->get()->first();
                    $sub_menu_item->parent_id = $value["id"];
                    if ($sub_menu_item->type == "custom") {
                        //$sub_menu_item->key = \Common::slugify($menu_item->key."-".$sub_menu_item->name );
                    }
                    $sub_menu_item->order = ($key2 + 1);
                    $sub_menu_item->save();

                    $permission_name = $sub_menu_item->user_type."_".$sub_menu_item->key;

                    $permission_display_name = $menu_item->name;

                    $permission = \Permission::where("name", $permission_name)->get()->first();

                    if (!$permission) {
                        $permission = new \Permission;
                        $permission->name         = $permission_name;
                        $permission->display_name = $permission_display_name;
                        $permission->save();
                    }

                    if ($sub_menu_item->type == "custom") {
                        foreach (\Auth::user()->Owner()->my_roles()->get() as $role) {
                            if (!$role->hasPermission($permission->name))
                                $role->attachPermission($permission);
                            
                            foreach (\User::all() as $user) {
                                if ($user->getMyRole()->id == $role->id && !$user->hasPermission($permission->name))
                                    $user->attachPermission($permission);
                            }
                        }
                    }

                    if (isset($value2["children"])) {
                        foreach ($value2["children"][0] as $key3 => $value3) {
                            $sub_sub_menu_item = \Menu::where("id", $value3["id"])->get()->first();
                            $sub_sub_menu_item->parent_id = $value2["id"];
                            if ($sub_sub_menu_item->type == "custom") {
                                //$sub_sub_menu_item->key = \Common::slugify($menu_item->key."_".$sub_menu_item->key."_".$sub_sub_menu_item->name );
                            }
                            $sub_sub_menu_item->order = ($key3 + 1);
                            $sub_sub_menu_item->save();

                            $permission_name = $sub_sub_menu_item->user_type."_".$sub_sub_menu_item->key;

                            $permission_display_name = $sub_sub_menu_item->name;

                            $permission = \Permission::where("name", $permission_name)->get()->first();

                            if (!$permission) {
                                $permission = new \Permission;
                                $permission->name         = $permission_name;
                                $permission->display_name = $permission_display_name;
                                $permission->save();
                            }

                            if ($sub_sub_menu_item->type == "custom") {
                                foreach (\Auth::user()->Owner()->my_roles()->get() as $role) {
                                    if (!$role->hasPermission($permission->name))
                                        $role->attachPermission($permission);
                                    
                                    foreach (\User::all() as $user) {
                                        if (@$user->getMyRole()->id == @$role->id && !@$user->hasPermission($permission->name))
                                            $user->attachPermission($permission);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        \SecurityLog::addNew("menu_order_change", \Auth::user()->getName()." changed menu order.");

        return response()->json(array('status' => "ok"));
    }

    public function postSaveImageLink() {
        if (\Input::get("link_id"))
            $link = \CustomPageLink::where("id", \Input::get("link_id"))->get()->first();
        else
            $link = new \CustomPageLink;

        $link->menu_id = \Input::get("menu_id");
        $link->name = \Input::get("name");
        $link->url = \Input::get("url");
        $link->coordinates = json_encode(\Input::get("coordinates"));
        $link->sizes = json_encode(\Input::get("sizes"));
        $link->save();

        $menu_item = $link->menu()->get()->first();

        \SecurityLog::addNew("add_menu_link", \Auth::user()->getName()." added image link for menu item <strong>".$menu_item->name."</strong>.");

        return response()->json(array('status' => "ok"));
    }

    function postToggleHideMenu() {
        $menu_item = \Menu::where("id", \Input::get("id"))->get()->first();
        $menu_item->hidden = (\Input::get("hidden") == "true") ? 1 : 0;
        $menu_item->save();

        \SecurityLog::addNew("hide_menu_itme", \Auth::user()->getName()." hid menu item <strong>".$menu_item->name."</strong>.");

        return response()->json(array('status' => "ok"));
    }
}