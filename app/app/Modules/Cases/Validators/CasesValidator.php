<?php

namespace App\Modules\Cases\Validators;

class CasesValidator
{
	public static $addRules = array(
        'name'=>'required',
        'contract_type'=>'required',
        'primary_address' => 'required'
    );
}