<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('cases', function($table)
        {
            $table->increments('id');
            $table->string('type', 255)->default("EAP"); //EAP or OMC
            $table->string('creator_name', 255);
            $table->string('caller_name', 255);
            $table->string('company_name', 255);
            $table->string('UID', 255); // Creator Initials + incrementing number
            $table->longtext('enrollee_data')->nullable(); //array of employee data and enrollee data
            $table->integer('creator_id')->nullable();
            $table->string('problems', 1000)->nullable();

            $table->string('preliminary_diagnosis', 255);
            $table->string('referral_type', 255);
            $table->datetime('call_start');
            $table->string('call_duration', 255);
            $table->string('call_center_number', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cases');
    }
}