<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseAuthorizationPagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('case_authorization_pages', function($table)
        {
            $table->increments('id');
            $table->string('creator_name', 255);
            $table->integer('case_id')->unsigned();
            $table->foreign('case_id')->references('id')->on('cases')->onUpdate('cascade')->onDelete('cascade');
            $table->longtext('provider_data')->nullable(); //array of provider data
            $table->string('UID', 255); // 6 letter or more string
            $table->integer('dictionary_value_id')->default(0);
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->integer('units')->default(0);
            $table->decimal('max_payment', 10, 2)->default(0.00);
            $table->decimal('patient_copay', 10, 2)->default(0.00);
            $table->string('remarks', 1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('case_authorization_pages');
    }
}