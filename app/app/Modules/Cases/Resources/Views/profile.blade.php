@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/cases">Cases</a></li>
        <li class="active">{{$case->getEnrolleeData()->enrollee->first_name}} {{$case->getEnrolleeData()->enrollee->last_name}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>{{$case->getEnrolleeData()->enrollee->first_name}} {{$case->getEnrolleeData()->enrollee->last_name}} - Case</strong></h3>
	</div> <!-- / .page-header -->

    <div class="pull-right" style="padding-left: 10px;">
        <a class="btn btn-success m-t-10" href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getEditCase', array($case->id)) }}"><i class="fa fa-pencil"></i> Edit Case</a>
    </div>

    <div class="pull-right" style="padding-left: 10px;">
        <a class="btn btn-default m-t-10" href="javascript: void(0);" onclick="window.print();"><i class="fa fa-print"></i> Print</a>
    </div>

	<div class="clearfix"></div>

    <?php //if (!empty($case->email)) ?>
        <div class="panel fade active in" id="profile">
            <div class="panel-body no-padding-hr">
                <div class="row p-20">
                    <div class="col-md-6">
                        <h3 class="m-t-0">Contact Info</h3>
                        <strong>Email Address: </strong>{{(empty($case->getEmployee()->email) ? @$case->email : $case->getEmployee()->email)}}<br>
                        <strong>Phone Number: </strong>{{@$case->getEnrolleeData()->enrollee->cell_phone}}<br>
                    </div>
                </div>

            </div>
        </div>
    <?php //@endif ?>

    <div class="panel fade active in" id="profile">
        <div class="panel-body no-padding-hr">
            <div class="row p-20">

                <div class="col-md-6">
                    <h3 class="m-t-0">Enrollee</h3>
                    <strong>First Name: </strong>{{$case->getEnrolleeData()->enrollee->first_name}}<br>
                    <strong>Last Name: </strong>{{$case->getEnrolleeData()->enrollee->last_name}}<br>
                    <strong>Gender: </strong>{{$case->getEnrolleeData()->enrollee->gender}}<br>
                    <strong>Cell Phone: </strong>{{$case->getEnrolleeData()->enrollee->cell_phone}}<br>
                    <strong>Home Phone: </strong>{{$case->getEnrolleeData()->enrollee->home_phone}}<br>
                    <strong>Work Phone: </strong>{{$case->getEnrolleeData()->enrollee->work_phone}}<br>
                    <strong>Birthday: </strong>{{$case->getEnrolleeData()->enrollee->birthday}}<br>
                </div>
                <div class="col-md-6">
                <div class="pull-right" style="padding-left: 10px;">
                    <a class="btn btn-success m-t-10 edit-enrollee-btn" href="#edit-enrollee-modal" data-toggle="modal"><i class="fa fa-pencil"></i> Edit Enrollee</a>
                </div>
                    <?php
                    $e_data = (!empty($case->getEnrolleeData()->enrollee->insurance_network) ? $case->getEnrolleeData()->enrollee : $case->getEnrolleeData()->employee);
                    ?>
                    <h3 class="m-t-0">&nbsp;</h3>
                    <strong>Plan: </strong>{{@$e_data->plan}}<br>
                    <strong>Insurance Network: </strong>{{@$e_data->insurance_network}}<br>
                    <strong>Insurance Type: </strong>{{$e_data->insurance_type}}<br>
                    <strong>Company: </strong>{{$e_data->client}}

                    <div class="address-info">
                        <strong>Address</strong>
                            {!! $case->getEmployee()->getPrimaryAddress(2) !!}
                      </div>
                </div>
            </div>

            <div class="div-separator"></div>
            <div class="row p-20">
                <div class="col-md-6">
                    <h3 class="m-t-0">Case</h3>
                    <strong>UID: </strong>{{$case->UID}}<br>
                    <strong>Presenting Problems: </strong>{{$case->problems}}<br>
                    <strong>Preliminary Diagnosis: </strong>{{$case->preliminary_diagnosis}}<br>
                    <strong>Referral Type: </strong>{{strip_tags($case->referral_type)}}<br>
                </div>
                <div class="col-md-6">
                    <h3 class="m-t-0">&nbsp;</h3>
                    <strong>Call Start: </strong> 
                    <?php
                    $date = new DateTime($case->call_start, new DateTimeZone('UTC'));
                    $date->setTimezone(new DateTimeZone('America/New_York'));
                    ?>
                    {{$date->format("m/d/Y g:i:s A")}}
                    <br>
                    <strong>Call Duration: </strong>{{strip_tags($case->call_duration)}}<br>
                    <strong>Call Center Number: </strong>{{$case->call_center_number}}<br>
                    <strong>Creator: </strong>{{$case->creator_name}}<br>
                </div>
            </div>

        </div>
    </div>

    <div class="panel fade active in">
        <div class="panel-body no-padding-hr">
            <div class="row p-20">
                <div class="col-md-12">
                    <div class="pull-right">
                        @if ($case->authorization_pages()->count() == 0)
                            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_cases_authorization-pages"))
                                <a class="btn btn-success" href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getAddAuthorizationPage', array($case->id)) }}"><i class="fa fa-plus"></i> Add Authorization Page</a>
                            @endif
                        @endif
                    </div>
                    <h3 class="m-t-0">Authorization Pages</h3>                    
                </div>
            </div>

            @foreach ($case->authorization_pages()->get() as $apage)
                <div class="apage-wrapper">
                    <div class="row p-20">
                        <div class="col-md-6">
                            <strong>UID: </strong>{{$apage->UID}}

                            <a class="btn btn-primary btn-xs tooltips" href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getAuthorizationPageProfile', array($apage->id)) }}" data-original-title="View Page"><i class="fa fa-eye"></i></a>
                            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_cases_authorization-pages"))
                                <a class="btn btn-success btn-xs tooltips" href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getEditAuthorizationPage', array($apage->id)) }}" data-original-title="Edit Page"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-danger btn-xs tooltips delete-apage" data-id="{{$apage->id}}" href="#" data-original-title="Delete Page"><i class="fa fa-times"></i></a>
                            @endif

                            <br><br>
                            <strong>Provider: </strong>{{$apage->getProviderData()["name"]}}<br>
                            <strong>Date Period: </strong>{{date("m/d/Y", strtotime($apage->date_from))}} - {{date("m/d/Y", strtotime($apage->date_to))}}<br>
                            <strong>Units: </strong>{{$apage->units}}<br>
                        </div>
                        <div class="col-md-6">
                            <h3 class="m-t-0">&nbsp;</h3>
                            <strong>Creation Date: </strong>{{date("m/d/Y", strtotime($apage->created_at))}}<br>
                            <strong>Creator: </strong>{{$apage->creator_name}}<br>
                            <strong>Maximum Payment: </strong>{{$apage->max_payment}}<br>
                            <strong>Patient Co-Pay: </strong>{{$apage->patient_copay}}<br>
                        </div>
                    </div>
                    <div class="div-separator"></div>
                </div>
            @endforeach
        </div>
    </div>

    <?php
    $comments_owner = $case;
    $owner_type = "case";
    ?>

    @include('comments::widgets.comments_section')

    <script type="text/javascript">
        $(document).ready(function() {
            $('.delete-apage').live('click', function (e) {
                e.preventDefault();
                if (confirm("Are you sure to delete this authorization page?") == false) {
                    return;
                }
                var that = $(this);
                var ids = [that.attr("data-id")];

                that.parents(".apage-wrapper").slideUp("fast", function() {
                    that.parents(".apage-wrapper").remove();
                });

                $.post("{{ action('\App\Modules\Cases\Http\Controllers\CasesController@postDeleteAuthorizationPages') }}", {
                    ids: ids
                }, function(response) {
                    window.location = window.location.href;
                }, "json");
            });

            $('a[href="#edit-enrollee-modal"]').click(function() {
                var new_height = $(window).height() - 250;
                $("#edit-enrollee-form").css('height', new_height + 'px');
                $("#edit-enrollee-form #iframe-here").html('<iframe onload="$(\'#iframe-here\').show(); $(\'#iframe_loading\').hide();" src="{{URL::to('/clients/edit-employee/'.@$employee->id)}}" style="width: 100%; height: 100%; border: 0px; overflow-x: hidden;"></iframe>');
            });
        });

        function finishEditEmployee() {
            window.location = window.location.href;
        }

        function submitIframeForm() {
            $("#edit-enrollee-form iframe").contents().find("form").submit();
        }
    </script>

    <div class="modal fade" id="edit-enrollee-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding: 15px;">
                        <script type="text/javascript">
                            var exists = true;
                        </script>
                        <div class="col-md-12" id="edit-enrollee-form" style="margin-top: -30px;">
                            <div id="iframe-here" style="height: 100%; display: none;"></div>
                            <div id="iframe_loading" style="width: 100%; height: 100%; text-align: center; background-image: url(https://www.hereofamily.com/wp-content/themes/HereO/images/ajax-loader.gif); background-repeat: no-repeat; background-position: 50% 50%; background-size: 10%;"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-success" onclick="submitIframeForm();"><i class="fa fa-arrow-right"></i> Save</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
@stop