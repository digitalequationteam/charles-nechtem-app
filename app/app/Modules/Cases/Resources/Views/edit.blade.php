@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/cases">Cases</a></li>
        <li><a href="/cases/case-profile/{{$case->id}}">{{$case->getEnrolleeData()->enrollee->first_name}} {{$case->getEnrolleeData()->enrollee->last_name}}</a></li>
        <li class="active">Edit</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Edit Case</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<!--
		<a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getIndex') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
		-->
	</div>

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Cases\Http\Controllers\CasesController@postSaveCase', $case->id)}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			
			<?php
			$enrollee_data = $case->getEnrolleeData();
			?>
			@include('clients::employees.widgets.select_employee')

			<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 8px;">
						<label class="control-label col-sm-offset-4 col-sm-4" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Contact Info</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">Email Address</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" maxlength="100" name="email" id="email" value="<?php echo (old()) ? Input::old('email') : $case->email; ?>" />
						</div>
					</div>
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label col-sm-offset-4 col-sm-4" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Preliminary Diagnosis</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Presenting Problems</label>
					<div class="col-sm-4">
						<a href="#" id="problem_types_editable" data-type="checklist" data-value="<?php echo (old()) ? Input::old('problem_types') : $case->problems; ?>" data-title="Select Problems"></a>
						<input type="hidden" name="problem_types" id="problem_types" value="<?php echo (old()) ? Input::old('problem_types') : $case->problems; ?>" />
					</div>
				</div>

				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Preliminary Diagnosis</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" maxlength="100" name="preliminary_diagnosis" id="preliminary_diagnosis" value="<?php echo (old()) ? Input::old('preliminary_diagnosis') : $case->preliminary_diagnosis; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Referral Type</label>
					<div class="col-sm-4">
						<select id="referral_type" name="referral_type" class="form-control">
							<option value="">None</option>
							<option value="Phone Counseling" @if ((old() && \Input::old("referral_type") == "Phone Counseling") || (!old() && $case->referral_type == "Phone Counseling")) selected @endif>Phone Counseling</option>
							<option value="Face to Face Counseling" @if ((old() && \Input::old("referral_type") == "Face to Face Counseling") || (!old() && $case->referral_type == "Face to Face Counseling")) selected @endif>Face to Face Counseling</option>
							<option value="Dignity Phone Counseling" @if ((old() && \Input::old("referral_type") == "Dignity Phone Counseling") || (!old() && $case->referral_type == "Dignity Phone Counseling")) selected @endif>Dignity Phone Counseling</option>
							<option value="Dignity Face to Face Counseling" @if ((old() && \Input::old("referral_type") == "Dignity Face to Face Counseling") || (!old() && $case->referral_type == "Dignity Face to Face Counseling")) selected @endif>Dignity Face to Face Counseling</option>
							<option value="Legal Referral" @if ((old() && \Input::old("referral_type") == "Legal Referral") || (!old() && $case->referral_type == "Legal Referral")) selected @endif>Legal Referral</option>
							<option value="Financial Referral" @if ((old() && \Input::old("referral_type") == "Financial Referral") || (!old() && $case->referral_type == "Financial Referral")) selected @endif>Financial Referral</option>
							<option value="Resources" @if ((old() && \Input::old("referral_type") == "Resources") || (!old() && $case->referral_type == "Resources")) selected @endif>Resources</option>
							<option value="E-Counseling" @if ((old() && \Input::old("referral_type") == "E-Counseling") || (!old() && $case->referral_type == "E-Counseling")) selected @endif>E-Counseling</option>
							<option value="Child Elder Care Referral" @if ((old() && \Input::old("referral_type") == "Child Elder Care Referral") || (!old() && $case->referral_type == "Child Elder Care Referral")) selected @endif>Child Elder Care Referral</option>
						</select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Call Start</label>
					<div class="col-sm-4">
						<input type="text" class="form-control date" maxlength="100" name="call_start1" id="call_start1" value="<?php echo (old()) ? Input::old('call_start1') : date("Y-m-d", strtotime($case->call_start." -4hour")); ?>" style="display: inline-block; width: 49%;" />
						<input type="time" class="form-control" maxlength="100" name="call_start2" id="call_start2" value="<?php echo (old()) ? Input::old('call_start2') : date("H:i:s", strtotime($case->call_start." -4hour")); ?>" style="display: inline-block; width: 49%;" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Duration</label>
					<div class="col-sm-4">
						<input type="number" class="form-control" maxlength="100" name="duration" id="duration" value="<?php echo (old()) ? Input::old('duration') : $case->call_duration; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Call Center Number</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" maxlength="100" name="call_center_number" id="call_center_number" value="<?php echo (old()) ? Input::old('call_center_number') : $case->call_center_number; ?>" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#first_name, #last_name").keyup(function() {
				$("#initials").val($("#first_name").val().substr(0,1) + $("#last_name").val().substr(0,1));
			});
			$(".switch.sw1").bootstrapSwitch({
				onText: "ON",
				offText: "OFF"
			});
			$(".switch.sw2").bootstrapSwitch({
				onText: "YES",
				offText: "NO"
			});

			$('.switch.sw1').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (state.value)
			    	$(".for-secondary").show();
			    else
			    	$(".for-secondary").hide();
			});

			$('.switch.sw2').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (!state.value)
			    	$(".for-mailing").show();
			    else
			    	$(".for-mailing").hide();
			});

			$("#add-new-contact").click(function() {
				var key = $("#contacts>.contact-item").length + 1;
				$("#contacts").append('<div class="contact-item form-group no-margin-hr panel-padding-h">\
					<label class="control-label col-sm-1">&nbsp;</label>\
					<div class="col-sm-8">\
						<a href="#" id="contact_' + key + '" data-type="contact" data-pk="1" data-title="Contact Info" style="display: block;"></a>\
						<input type="hidden" class="title" name="contact[' + key + '][title]" />\
						<input type="hidden" class="first_name" name="contact[' + key + '][first_name]" />\
						<input type="hidden" class="last_name" name="contact[' + key + '][last_name]" />\
						<input type="hidden" class="email" name="contact[' + key + '][email]" />\
						<input type="hidden" class="phone" name="contact[' + key + '][phone]" />\
						<input type="hidden" class="cell_phone" name="contact[' + key + '][cell_phone]" />\
						<input type="hidden" class="fax" name="contact[' + key + '][fax]" />\
						<input type="hidden" class="additional_info" name="contact[' + key + '][additional_info]" />\
					</div>\
				</div>');

				$("#contact_" + key).editable({
			        validate: function(value) {
			            if(value.first_name == '') return 'First name is required!'; 
			        },
			        placement: 'left',
		        	emptytext: 'Add Contact Info'
			    });

				setTimeout(function() {
			    	$("#contact_" + key).click();
			    }, 100);
			});

			$("#primary_address, #secondary_address, #mailing_address").editable({
		        validate: function(value) {
		            if(value.street == '') return 'Street is required!'; 
		            if(value.city == '') return 'City is required!'; 
		            if(value.country == '') return 'Country is required!';
		            if(value.state == '') return 'State is required!'; 
		            if(value.zip == '') return 'Zip Code is required!'; 
		        },
		        placement: 'left',
		        emptytext: 'Add Address'
		    });

		    $("#problem_types_editable").editable({
		        placement: 'left',
		        emptytext: 'No Problems',
		        source: {!!json_encode(\DictionaryValue::getAll("problem_types"))!!},
                success: function() {
                    setTimeout(function() {
                        $('input[name="problem_types"]').val($('#problem_types_editable').html().replace(/<br>/g, ", "));
                    }, 100);
                }
		    });
		});

		var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};

		var contact_titles = ['Administrator, Benefits/Risk', 'Administrator, HRS-Benefits services', 'Benefits Administrator', 'Benefits Contact Person', 'Benefits Coordinator', 'Benefits Manager', 'Benefits Specialist', 'CEO & President', 'Chief Operating Officer', 'Director, Compensation & Benefits', 'Director, Human resources', 'Director of Administrator Services', 'Director of Benefits', 'Director of Social Services', 'Director of Workers Compensation & Employee Health', 'Division of Labor-Management Relations', 'Employees Relations', 'Executive Director', 'Financial Director', 'Financial Officer', 'Group Business Manager', 'Human Resources Manager', 'Lead Manager, HR Department', 'Office Manager', 'President', 'Senior President', 'Training Director', 'Vice President', 'Vice President & Chief Financial Officer', 'Vice President of Human Resources', 'Vice President of Life Operations'];
	</script>
@stop