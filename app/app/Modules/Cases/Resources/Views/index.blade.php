@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li class="active">Cases</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Cases</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_cases_add"))
			<a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getAddCase', $type) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span> Add Case</a>
		@endif
	</div>

	<div class="clearfix"></div>

	<div class="tabcordion">
	    <ul id="myTab" class="nav nav-tabs">
	        <li @if ($type == "EAP") class="active" @endif><a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getIndex') }}/index/EAP">EAP</a></li>
	        <li @if ($type == "OMC") class="active" @endif><a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getIndex') }}/index/OMC">OMC</a></li>
	    </ul>
	    <div class="tab-content">
			<div class="row users">
				<div class="col-md-12">
					<div class="panel panel-default">
		                <div class="panel-body">
				        
		                    <div class="row">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
								@if(Session::has('message'))
							        <div class="alert alert-success">
							            <button type="button" class="close" data-dismiss="alert">×</button>
							            <span>
							                {{ Session::get('message') }}
							            </span>
								    </div>
							    @endif

								<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
				                    <table id="cases-table" class="table table-tools table-striped">
				                        <thead>
				                            <tr>
				                                <th style="min-width:50px">
				                                	<div class="pull-left">
				                                		@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_cases_delete"))
				                                    		<input type="checkbox" class="check_all"/>
				                                    	@endif
				                                    </div>
				                                    <div class="pull-left" style="padding-left: 30px; margin-top: -12px;">
				                                    	@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_cases_delete"))
															<a href="#" class="btn btn-danger btn-delete btn-xs" style="margin: 0px;">Delete</a>
														@endif
													</div>
				                                </th>
				                                <th></th>
				                                <th class="text-right" style="width: 300px; padding-top: 6px; font-size: 12px;">
				                                	Sort:
				                                	<select id="sort-by-dd">
				                                		<option value="call_start" @if (\Input::get("sort_by") == "call_start") selected @endif>Call Start Time</option>
				                                		<option value="caller_name" @if (\Input::get("sort_by") == "caller_name") selected @endif>Caller Name</option>
				                                		<option value="company_name" @if (\Input::get("sort_by") == "company_name") selected @endif>Company Name</option>
				                                	</select>

				                                	<select id="sort-type-dd">
				                                		<option value="ASC" @if (\Input::get("sort_type") == "ASC") selected @endif>Ascending</option>
				                                		<option value="DESC" @if (!\Input::get("sort_type") || \Input::get("sort_type") == "DESC") selected @endif>Descending</option>
				                                	</select>
				                                </th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                            <tr class="bg-gray-light filters-tr">
				                                <td colspan="3" style="padding-top: 6px;">
				                                	<div class="pull-left" style="padding-left: 8px;">
				                                		<button id="toggle-filters-btn" class="btn btn-danger btn-xm" onclick="toggleFilters();"><i class="fa fa-arrow-down"></i> Search Filters</button>
				                                	</div>

				                                	<div class="clearfix"></div>

				                                	<div id="filters-wrapper" style="padding-top: 10px; padding-bottom: 10px; @if (!\Input::get('applyFilters')) display: none; @endif">
				                                		<form action="" method="get">
				                                			<input type="hidden" name="sort_by" value="{{\Input::get('sort_by')}}" />
				                                			<input type="hidden" name="sort_type" value="{{\Input::get('sort_type')}}" />
		                                					<input type="hidden" name="page_size" value="{{\Input::get('page_size')}}" />
					                                		<div class="row">
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Enrollee Name</label>
																		<div>
																			<input class="form-control" type="text" name="name" value="{{\Input::get('name')}}" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label><input type="radio" name="nst" value="creator" @if (\Input::get('nst') == 'creator' || !\Input::get('nst')) checked @endif onclick="$('.cn').val('').focus();" /> Creator</label>
																		<label><input type="radio" name="nst" value="initials" @if (\Input::get('nst') == 'initials') checked @endif onclick="$('.cn').val('').focus();" /> Initials</label>
																		<div>
																			<input class="form-control cn" type="text" name="creator_name" value="{{\Input::get('creator_name')}}" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-2">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Street</label>
																		<div>
																			<input type="text" class="form-control" maxlength="100" name="street" id="street" value="<?php echo Input::get('street'); ?>" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-2">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>City</label>
																		<div>
																			<input type="text" class="form-control" maxlength="100" name="city" id="city" value="<?php echo Input::get('city'); ?>" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-2">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Country</label>
																		<div>
																			<select class="form-control" name="country" id="country">

																				<?php foreach (\Common::getAllCountries() as $code => $name) {
																					?>
																						<option value="<?php echo $code; ?>" <?php if (\Input::get("country") == $code) { ?>selected="selected"<?php } ?>><?php echo $name; ?></option>
																					<?php
																				}
																				?>
																			</select>
																		</div>
																	</div>
						                                		</div>
						                                	</div>

						                                	<div class="row">
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Referral Type</label>
																		<div>
																			<select id="referral_type" name="referral_type" class="form-control">
																				<option value="">None</option>
																				<option value="Phone Counseling" @if (\Input::get("referral_type") == "Phone Counseling") selected @endif>Phone Counseling</option>
																				<option value="Face to Face Counseling" @if (\Input::get("referral_type") == "Face to Face Counseling") selected @endif>Face to Face Counseling</option>
																				<option value="Dignity Phone Counseling" @if (\Input::get("referral_type") == "Dignity Phone Counseling") selected @endif>Dignity Phone Counseling</option>
																				<option value="Dignity Face to Face Counseling" @if (\Input::get("referral_type") == "Dignity Face to Face Counseling") selected @endif>Dignity Face to Face Counseling</option>
																				<option value="Legal Referral" @if (\Input::get("referral_type") == "Legal Referral") selected @endif>Legal Referral</option>
																				<option value="Financial Referral" @if (\Input::get("referral_type") == "Financial Referral") selected @endif>Financial Referral</option>
																				<option value="Resources" @if (\Input::get("referral_type") == "Resources") selected @endif>Resources</option>
																				<option value="E-Counseling" @if (\Input::get("referral_type") == "E-Counseling") selected @endif>E-Counseling</option>
																				<option value="Child Elder Care Referral" @if (\Input::get("referral_type") == "Child Elder Care Referral") selected @endif>Child Elder Care Referral</option>
																			</select>
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h col-md-6" style="padding-left: 0px;">
																		<label>Case UID</label>
																		<div>
																			<input class="form-control" type="text" name="case_uid" value="{{\Input::get('case_uid')}}" />
																		</div>
																	</div>
																	<div class="form-group no-margin-hr panel-padding-h col-md-6">
																		<label>Auth UID</label>
																		<div>
																			<input class="form-control" type="text" name="authorization_uid" value="{{\Input::get('authorization_uid')}}" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>State</label>
																		<div>
																			<select class="form-control" name="state" id="state">
																				<option value="">Select state...</option>
																				<?php if (Input::get("country")) { ?>
																					<?php foreach (\Common::getStates(Input::get("country")) as $state) {
																						?>
																						<option value="<?php echo $state; ?>" <?php if ($state == Input::get("state")) { ?>selected="selected"<?php } ?>><?php echo $state; ?></option>
																						<?php
																					}
																					?>
																				<?php } ?>
																			</select>
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Zip Code</label>
																		<div>
																			<input type="text" class="form-control" maxlength="100" name="zip" id="zip" value="<?php echo Input::get('zip'); ?>" />
																		</div>
																	</div>
						                                		</div>
						                                	</div>
						                                	<button class="btn btn-default" name="applyFilters" value="Yes">Apply Filters</button>
						                                	<button onclick="window.location = '/cases/index/{{$type}}'; return false;" class="btn btn-default">Reset</button>
						                                </form>
				                                	</div>
				                                </td>
				                            </tr>
				                        	<?php
											foreach ($cases as $case) { ?>
												<tr class="odd gradeX">
													<td>
														<div class="pull-left" style="padding-top: 10px;">
															@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_cases_delete"))
																<input type="checkbox" class="batch_id" value="{{$case->id}}" />
															@endif
														</div>
														<div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
															 <a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getCaseProfile', array($case->id)) }}"><strong>{{$case->getEnrolleeData()->enrollee->first_name}} {{$case->getEnrolleeData()->enrollee->last_name}}</strong></a>
															 <br />
															 {{$case->getEnrolleeData()->enrollee->client}}
															 <?php
															 $apage = $case->authorization_pages()->get()->last();
															 ?>
															 @if ($apage)
															 <br />
															 <a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getAuthorizationPageProfile', array($apage->id)) }}">{{$apage->UID}}</a>
															 @endif
														</div>
													</td>
													<td>
														<?php
								                        $date = new DateTime($case->call_start, new DateTimeZone('UTC'));
								                        $date->setTimezone(new DateTimeZone('America/New_York'));
								                        ?>
                        								{{$date->format("m/d/Y g:i:s A")}}
													</td>
													<td style="text-align: center;">
														<a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getCaseProfile', array($case->id)) }}" class="btn bg-blue btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Profile"><i class="fa fa-eye"></i></a>
														@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_cases_edit"))
															<a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getEditCase', array($case->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Case"><i class="fa fa-pencil"></i></a>
														@endif
														@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_cases_delete"))
															<a href="#" class="btn bg-red btn-delete-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Case"><i class="fa fa-times"></i></a>
														@endif
													</td>
												</tr>
											<?php 
												}
											?>
				                        </tbody>
				                    </table>

				                    @if ($cases->count() == 0)
				                    	No cases found.
				                    @endif

				                    <div class="pull-left" style="padding-right: 10px; padding-top: 20px;">
				                    	Per page:
				                    </div>

				                    <div class="pull-left" style="padding-top: 15px;">
		                            	<select id="page-size-dd" class="form-control">
		                            		<option value="10" @if (\Input::get("page_size") == "10") selected @endif>10</option>
		                            		<option value="25" @if (\Input::get("page_size") == "25") selected @endif>25</option>
		                            		<option value="100" @if (\Input::get("page_size") == "100") selected @endif>100</option>
		                            		<option value="All" @if (\Input::get("page_size") == "All") selected @endif>All</option>
		                            	</select>
				                    </div>

				                    <div class="pull-right">
				                    	@if ($cases_paginator)
				                    		{!! $cases_paginator->render() !!}
				                    	@endif
				                    </div>
				                </div>
				            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
	        /* Delete a product */
	        $('#cases-table a.btn-delete-single').live('click', function (e) {
	            e.preventDefault();
	            if (confirm("Are you sure to delete this case?") == false) {
	                return;
	            }

	            var ids = [$(this).parents("tr").find('.batch_id').val()];

	            $.post("{{ action('\App\Modules\Cases\Http\Controllers\CasesController@postDeleteCases') }}", {
	        		ids: ids
	        	}, function() {
	        		window.location = window.location.href;
	        	});
	        });

	        $(".btn-delete").click(function(e) {
	            e.preventDefault();
	        	var ids = [];

	        	$(".batch_id:checked").each(function() {
	        		ids.push($(this).val());
	        	});

	        	if (ids.length == 0) {
	        		alert('No cases were selected.');
	        		return;
	        	}

	        	if (confirm('Are you sure you want to delete the selected cases?')) {
		        	$.post("{{ action('\App\Modules\Cases\Http\Controllers\CasesController@postDeleteCases') }}", {
		        		ids: ids
		        	}, function() {
		        		window.location = window.location.href;
		        	});
	        	}
	        });

			$("#sort-by-dd, #sort-type-dd, #page-size-dd").change(function() {
				$('input[name="sort_by"]').val($("#sort-by-dd").val());
				$('input[name="sort_type"]').val($("#sort-type-dd").val());
				$('input[name="page_size"]').val($("#page-size-dd").val());
				$("#filters-wrapper form").submit();
			});
	    });

		function toggleFilters() {
			if ($('#filters-wrapper').is(":visible")) {
				$('#filters-wrapper').slideUp();
				$("#toggle-filters-btn i").removeClass("fa-arrow-up").addClass("fa-arrow-down");
			}
			else {
				$('#filters-wrapper').slideDown();
				$("#toggle-filters-btn i").removeClass("fa-arrow-down").addClass("fa-arrow-up");
			}
		}
    </script>
@stop