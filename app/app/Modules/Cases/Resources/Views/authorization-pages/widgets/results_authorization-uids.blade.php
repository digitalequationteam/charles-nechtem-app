@if (count($authorization_uids) == 0)
	Nothing found.
@endif

@foreach ($authorization_uids as $authorization_uid)
	<div class="row" style="border-bottom: solid 1px #CCCCCC; padding: 5px;">
		<div class="pull-right">
			<a href="#" data-id="{{$authorization_uid->UID}}" class="btn btn-success tooltips btn-xs select-auth-uid-btn" data-original-title="Use {{$authorization_uid->UID}}"><i class="fa fa-arrow-right"></i></a>
		</div>
		<strong>{{$authorization_uid->UID}} - {{$authorization_uid->ecase()->get()->first()->caller_name}}</strong>
		{{$authorization_uid->ecase()->get()->first()->company_name}}
		-
		{{date("m/d/Y", strtotime($authorization_uid->created_at))}}
	</div>
@endforeach