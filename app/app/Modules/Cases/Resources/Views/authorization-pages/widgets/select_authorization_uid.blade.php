<br />
<a href="#select-auth-uid-modal" data-toggle="modal" class="btn btn-success"><i class="fa fa-search"></i> Search Authorization UID</a>

<div class="modal fade" id="select-auth-uid-modal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><strong>Search</strong> Authorization UID</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding: 15px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" id="search-auth-uid-field" placeholder="Search Enrollee name...">
                        </div>

                        <div class="form-group">
                        	<div id="authorization-uid-results" style="max-height: 200px; overflow-y: auto; padding: 20px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-employee-dependent-modal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><strong>Add</strong> Dependent</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding: 15px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" id="search-auth-uid-field" placeholder="Search Enrollee...">
                        </div>

                        <div class="form-group">
                        	<div id="authorization-uid-results" style="max-height: 200px; overflow-y: auto; padding: 20px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var employee_searching = false;
		$("#search-auth-uid-field").keyup(function() {
			var search_text = $(this).val();
			if (search_text.length <= 1)
				return;

			//if (employee_searching)
				//return false;

			employee_searching = true;
			$.post("{{action('\App\Modules\Cases\Http\Controllers\CasesController@postWidgetSearchAuthorizationUid')}}", {
				search_text: search_text
			}, function(response) {
				$("#authorization-uid-results").html(response);
				employee_searching = false;
				$(".tooltips").tooltip();
			});
		});

		$(".select-auth-uid-btn").live("click", function() {
			selectAuthorizationUID($(this).attr("data-id"));
		});

		@if (\Input::old("auth-uid_id"))
			selectAuthorizationUID({{\Input::old("auth-uid_id")}});
		@endif
	});

	function selectAuthorizationUID(id) {
		$('button[data-dismiss="modal"]').trigger("click");
		$("#authorization_uid").val(id);
	}
</script>