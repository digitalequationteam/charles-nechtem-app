@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/cases">Cases</a></li>
        <li><a href="/cases/case-profile/{{$apage->ecase()->get()->first()->id}}">{{$apage->ecase()->get()->first()->getEnrolleeData()->enrollee->first_name}} {{$apage->ecase()->get()->first()->getEnrolleeData()->enrollee->last_name}}</a></li>
        <li><a href="/cases/authorization-page-profile/{{$apage->id}}">Authorization Page</a></li>
        <li class="active">Edit Authorization Page</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Edit Authorization Page</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getCaseProfile', $apage->case_id) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Cases\Http\Controllers\CasesController@postSaveAuthorizationPage', $apage->id)}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<input type="hidden" name="case_id" value="{{$apage->case_id}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			
			<?php
			$provider_data = $apage->getProviderData();
			?>
			@include('providers::widgets.select_provider')

			<div class="col-md-12">
				<br />
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">EAP Service <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<div id="eap-service-wrapper">{{$apage->eap_service()->get()->first()->name}}</div>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Date Period From</label>
					<div class="col-sm-4">
						<input type="text" class="form-control date" maxlength="100" name="date_from" id="date_from" value="<?php echo (old() ? Input::old('date_from') : $apage->date_from); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Date Period To</label>
					<div class="col-sm-4">
						<input type="text" class="form-control date" maxlength="100" name="date_to" id="date_to" value="<?php echo (old() ? Input::old('date_to') : $apage->date_to); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Units</label>
					<div class="col-sm-4">
						<input type="number" class="form-control" maxlength="100" name="units" id="units" value="<?php echo (old() ? Input::old('units') : $apage->units); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Maximum Payment</label>
					<div class="col-sm-4">
						<div class="input-group transparent">
                            <span class="input-group-addon bg-grey">
                              <i class="fa fa-dollar"></i>
                            </span>
                            <input type="number" class="form-control" maxlength="100" name="max_payment" id="max_payment" value="<?php echo (old() ? Input::old('max_payment') : $apage->max_payment); ?>" />
                        </div>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Patient Co-pay</label>
					<div class="col-sm-4">
						<div class="input-group transparent">
                            <span class="input-group-addon bg-grey">
                              <i class="fa fa-dollar"></i>
                            </span>
                            <input type="number" class="form-control" maxlength="100" name="patient_copay" id="patient_copay" value="<?php echo (old() ? Input::old('patient_copay') : $apage->patient_copay); ?>" />
                        </div>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Remarks</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="remarks" id="remarks"><?php echo (old() ? Input::old('remarks') : $apage->remarks); ?></textarea>
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>
@stop