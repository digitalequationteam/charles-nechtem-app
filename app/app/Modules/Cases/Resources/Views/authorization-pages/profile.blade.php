@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/cases">Cases</a></li>
        <li><a href="/cases/case-profile/{{$case->id}}">{{$case->getEnrolleeData()->enrollee->first_name}} {{$case->getEnrolleeData()->enrollee->last_name}}</a></li>
        <li class="active">Authorization Page - {{$apage->UID}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Authorization Page</strong></h3>
	</div> <!-- / .page-header -->

    <div class="pull-right" style="padding-left: 10px;">
        <a class="btn btn-success m-t-10" href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getEditAuthorizationPage', array($apage->id)) }}"><i class="fa fa-pencil"></i> Edit Page</a>
    </div>

    <div class="pull-right" style="padding-left: 10px;">
        <a class="btn btn-default m-t-10" href="javascript: void(0);" onclick="window.print();"><i class="fa fa-print"></i> Print</a>
    </div>

    <!--
    <div class="pull-right">
        <a href="{{ action('\App\Modules\Cases\Http\Controllers\CasesController@getCaseProfile', $case->id) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
    </div>
    -->

	<div class="clearfix"></div>

    <div class="panel fade active in" id="profile">
        <div class="panel-body no-padding-hr">
            <div class="row p-20">
                <div class="col-md-12">
                    <h3 class="m-t-0">Cover</h3>
                    <strong>Creation Date: </strong>{{date("m/d/Y", strtotime($apage->created_at))}}<br>
                </div>
            </div>

            <div class="div-separator"></div>

            <div class="row p-20">
                <div class="col-md-12">
                    <h3 class="m-t-0">Fund</h3>
                    <strong>Company Name: </strong>{{$case->getEnrolleeData()->employee->client}}<br>
                </div>
            </div>

            <div class="div-separator"></div>

            <div class="row p-20">
                <div class="col-md-12">
                    <h3 class="m-t-0">Physician Name</h3>
                    <strong>Provider Name: </strong>{{$apage->getProviderData()["name"]}}

                    <div class="address-info">
                        <strong>Address</strong>
                            {!! $apage->getProviderData()["address"] !!}
                      </div>
                </div>
            </div>

            <div class="div-separator"></div>

            <div class="row p-20">
                <div class="col-md-6">
                    <h3 class="m-t-0">Subscriber (Employee)</h3>
                    <strong>First Name: </strong>{{$case->getEnrolleeData()->employee->first_name}}<br>
                    <strong>Last Name: </strong>{{$case->getEnrolleeData()->employee->last_name}}<br>
                    <strong>Gender: </strong>{{$case->getEnrolleeData()->employee->gender}}<br>
                    <strong>Cell Phone: </strong>{{$case->getEnrolleeData()->employee->cell_phone}}<br>
                    <strong>Home Phone: </strong>{{$case->getEnrolleeData()->employee->home_phone}}<br>
                    <strong>Work Phone: </strong>{{$case->getEnrolleeData()->employee->work_phone}}<br>
                    <strong>Birthday: </strong>{{$case->getEnrolleeData()->employee->birthday}}<br>
                    <strong>Plan: </strong>{{@$case->getEnrolleeData()->employee->plan}}<br>
                    <strong>Insurance Network: </strong>{{@$case->getEnrolleeData()->employee->insurance_network}}<br>
                    <strong>Insurance Type: </strong>{{$case->getEnrolleeData()->employee->insurance_type}}<br>
                    <strong>Company: </strong>{{$case->getEnrolleeData()->employee->client}}

                    <div class="address-info">
                        <strong>Address</strong>
                            {!! $case->getEnrolleeData()->employee->address !!}
                      </div>
                </div>
                <div class="col-md-6">
                    <h3 class="m-t-0">Claim for</h3>
                    <strong>First Name: </strong>{{$case->getEnrolleeData()->enrollee->first_name}}<br>
                    <strong>Last Name: </strong>{{$case->getEnrolleeData()->enrollee->last_name}}<br>
                    <strong>Gender: </strong>{{$case->getEnrolleeData()->enrollee->gender}}<br>
                    <strong>Cell Phone: </strong>{{$case->getEnrolleeData()->enrollee->cell_phone}}<br>
                    <strong>Home Phone: </strong>{{$case->getEnrolleeData()->enrollee->home_phone}}<br>
                    <strong>Work Phone: </strong>{{$case->getEnrolleeData()->enrollee->work_phone}}<br>
                    <strong>Birthday: </strong>{{$case->getEnrolleeData()->enrollee->birthday}}<br>
                    <strong>Plan: </strong>{{@$case->getEnrolleeData()->employee->plan}}<br>
                    <strong>Insurance Network: </strong>{{@$case->getEnrolleeData()->employee->insurance_network}}<br>
                    <strong>Insurance Type: </strong>{{$case->getEnrolleeData()->employee->insurance_type}}<br>
                    <strong>Company: </strong>{{$case->getEnrolleeData()->employee->client}}

                    <div class="address-info">
                        <strong>Address</strong>
                            {!! $case->getEnrolleeData()->enrollee->address !!}
                      </div>
                </div>
            </div>

            <div class="div-separator"></div>
            <div class="row p-20">
                <div class="col-md-6">
                    <h3 class="m-t-0">Authorization Terms</h3>
                    <strong>UID: </strong>{{$apage->UID}}<br>
                    <strong>POS Code: </strong>{{$apage->getProviderData()["pos_code"]}}<br>
                    <strong>EAP Service: </strong>{{$apage->eap_service()->get()->first()->name}}<br>
                    <strong>Units: </strong>{{$apage->units}}<br>
                    <strong>Remarks: </strong>{{$apage->remarks}}<br>
                </div>
                <div class="col-md-6">
                    <h3 class="m-t-0">&nbsp;</h3>
                    <strong>Maximum Payment: </strong>{{$apage->max_payment}}<br>
                    <strong>Patient Co-Pay: </strong>{{$apage->patient_copay}}<br>
                </div>
            </div>
        </div>
    </div>
@stop