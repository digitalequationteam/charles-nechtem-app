<?php

namespace App\Modules\Cases\Models;

use Illuminate\Database\Eloquent\Model;

class CaseAuthorizationPage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'case_authorization_pages';

    public function ecase() {
        return $this->belongsTo("\ECase", "case_id");
    }

    public function eap_service() {
        return $this->belongsTo("\DictionaryValue", "dictionary_value_id");
    }

    public function getProviderData() {
    	return json_decode($this->provider_data, true);
    }
}