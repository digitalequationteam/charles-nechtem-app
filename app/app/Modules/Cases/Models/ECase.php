<?php

namespace App\Modules\Cases\Models;

use Illuminate\Database\Eloquent\Model;

class ECase extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cases';

    public function comments() {
        return $this->hasMany("\Comment", "owner_id")->where("type", "case");
    }

    public function contacts() {
        return $this->hasMany("\Contact", "owner_id")->where("type", "case");
    }

    public function authorization_pages() {
        return $this->hasMany("\CaseAuthorizationPage", "case_id");
    }

    public function getEnrolleeData() {
        return json_decode($this->enrollee_data);
    }

    public function getEnrolleeDataArray() {
        return json_decode($this->enrollee_data, true);
    }

    public function getEmployee() {
        $enrollee_data = $this->getEnrolleeData();

        if (!empty($enrollee_data->enrollee->id))
            $enrollee = \App\Modules\Clients\Models\ClientEmployee::where("id", $enrollee_data->enrollee->id)->get()->first();
        else
            $enrollee = \App\Modules\Clients\Models\ClientEmployee::where("first_name", $enrollee_data->enrollee->first_name)->where("last_name", $enrollee_data->enrollee->last_name)->get()->first();
        
        return $enrollee;
    }
}