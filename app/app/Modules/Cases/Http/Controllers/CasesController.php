<?php

namespace App\Modules\Cases\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class CasesController extends Controller
{	
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

	public function getIndex($type = "EAP") {
        $cases = \ECase::where("type", $type);

        $appends = array();
        foreach ($_GET as $key => $value) {
            if ($key != "applyFilters" && !empty($value)) {
                $appends[$key] = $value;

                switch ($key) {
                    case 'name';
                        $cases = $cases->where("enrollee_data", 'LIKE', '%'.$value.'%');
                    break;
                    case 'creator_name';
                        if (\Input::get("nst") == "creator") {
                            $cases = $cases->where("creator_name", 'LIKE', '%'.$value.'%');
                        }
                        elseif (\Input::get("nst") == "initials") {
                            $names = array();
                            $users = \User::where("initials", "LIKE", '%'.$value.'%')->get();
                            foreach ($users as $user)
                                $names[] = $user->first_name." ".$user->last_name;

                            $cases = $cases->whereIn("creator_name", $names);
                        }
                    break;
                    case 'referral_type';
                        $cases = $cases->where("referral_type", 'LIKE', '%'.$value.'%');
                    break;
                    case 'street';
                        $cases = $cases->where("enrollee_data", 'LIKE', '%'.$value.'%');
                    break;
                    case 'city';
                        $cases = $cases->where("enrollee_data", 'LIKE', '%'.$value.'%');
                    break;
                    case 'country';
                        if ($value != "US")
                            $cases = $cases->where("enrollee_data", 'LIKE', '%'.\Common::getCountryName($value).'%');
                    break;
                    case 'state';
                        $cases = $cases->where("enrollee_data", 'LIKE', '%'.\Common::getStateCode($value).'%');
                    break;
                    case 'zip';
                        $cases = $cases->where("enrollee_data", 'LIKE', '%'.$value.'%');
                    break;
                    case 'authorization_uid';
                        $cases = $cases->whereHas('authorization_pages', function ($q) {
                            return $q->where('UID', \Input::get("authorization_uid"));
                        });
                    break;
                    case 'case_uid';
                        $cases = $cases->where('UID', \Input::get("case_uid"));
                    break;
                    case 'contract_type';
                        $cases = $cases->whereIn("contract_type", $value);
                    break;
                }
            }
        }

        if (\Input::get("sort_by"))
            $cases = $cases->orderBy(\Input::get("sort_by"), \Input::get("sort_type"));
        else
            $cases = $cases->orderBy("call_start", "DESC");

        $page_size = (\Input::get("page_size")) ? \Input::get("page_size") : 10;
        if ($page_size != "All") {
            $cases = $cases->paginate($page_size);
            $cases_paginator = $cases;
            $cases_paginator = $cases_paginator->appends($appends);
        }
        else {
            $cases = $cases->get();
        }

        if (\Input::get("page") && $cases->count() == 0)
            return redirect($cases_paginator->url($cases_paginator->lastPage()));

        return view('cases::index', array('cases' => $cases, "type" => $type, "cases_paginator" => @$cases_paginator));
	}

	public function getAddCase($type = null) {
        return view('cases::add', array("type" => $type));
	}

    public function getEditCase($id = null)
    {
        $case = \ECase::where('id', $id)->get()->first();

        $contacts = array();

        foreach ($case->contacts()->get() as $contact) {
            $contacts[] = array(
                "id" => $contact->id,
                "title" => $contact->title,
                "first_name" => $contact->first_name,
                "last_name" => $contact->last_name,
                "email" => $contact->email,
                "phone" => $contact->phone,
                "cell_phone" => $contact->cell_phone,
                "fax" => $contact->fax,
                "additional_info" => $contact->additional_info
            );
        }

        return view('cases::edit', array( 'case' => $case, 'contacts' => $contacts ));
    }

    public function getCaseProfile($id = null)
    {
        $case = \ECase::where('id', $id)->get()->first();
        $employee = $case->getEmployee();

        return view('cases::profile', array( 'case' => $case, 'employee' => $employee));
    }

    public function postSaveCase($id = null)
    {
        if (!\Input::get("enrollee_id") && $id == null)
            return Redirect::back()->withErrors(["No enrollee selected."])->withInput();

        //$rules = \App\Modules\Cases\Validators\ECasesValidator::$addRules;
        $rules = array();

        $validator = \Validator::make( \Input::all(), $rules );

        if ( $validator->passes() ) {
            if ($id == null) {
                $case = new \ECase;
                $case->type = \Input::get( 'type' );

                $increment = \Auth::user()->cases()->count() + 1;
                $length = strlen($increment);

                while (strlen($increment) < 4)
                    $increment = "0".$increment;

                $case->uid = \Auth::user()->initials.date("y").$increment;
                $case->creator_id = \Auth::user()->id;
                $case->user_id = \Auth::user()->id;
                $case->creator_name = \Auth::user()->first_name." ".\Auth::user()->last_name;
            }
            else {
                $case = \ECase::find($id);
            }

            $enrollee = \ClientEmployee::where("id", \Input::get("enrollee_id"))->get()->first();

            if (\Input::get("enrollee_id")) {
                if ($enrollee->parent_id == 0)
                    $employee = $enrollee;
                else
                    $employee = $enrollee->parent()->get()->first();

                $case->enrollee_data = json_encode(array(
                    "enrollee" => array(
                        "id" => $enrollee->id,
                        "first_name" => $enrollee->first_name,
                        "last_name" => $enrollee->last_name,
                        "gender" => $enrollee->gender,
                        "ssn" => $enrollee->ssn,
                        "cell_phone" => $enrollee->cell_phone,
                        "home_phone" => $enrollee->home_phone,
                        "work_phone" => $enrollee->work_phone,
                        "birthday" => $enrollee->birthday,
                        "plan" => @$employee->plan()->get()->first()->name,
                        "insurance_network" => @$employee->insurance_network()->get()->first()->name,
                        "insurance_type" => $employee->insurance_type,
                        "client" => $employee->client()->get()->first()->name,
                        "address" =>$enrollee->getPrimaryAddress(2)
                    ),
                    "employee" => array(
                        "first_name" => $employee->first_name,
                        "last_name" => $employee->last_name,
                        "gender" => $employee->gender,
                        "ssn" => $employee->ssn,
                        "cell_phone" => $employee->cell_phone,
                        "home_phone" => $employee->home_phone,
                        "work_phone" => $employee->work_phone,
                        "birthday" => $employee->birthday,
                        "plan" => @$employee->plan()->get()->first()->name,
                        "insurance_network" => @$employee->insurance_network()->get()->first()->name,
                        "insurance_type" => $employee->insurance_type,
                        "client" => $employee->client()->get()->first()->name,
                        "address" =>$employee->getPrimaryAddress(2)
                    )
                ));
            
                $case->caller_name = $enrollee->first_name." ".$enrollee->last_name;
                $case->company_name = $employee->client()->get()->first()->name;
            }
            //$case->email = (string)\Input::get( 'email' );
            $case->problems = (string)\Input::get( 'problem_types' );
            $case->preliminary_diagnosis = (string)\Input::get( 'preliminary_diagnosis' );
            $case->referral_type = (string)\Input::get( 'referral_type' );
            if (\Input::get( 'call_start2' )) {
                $case->call_start = date("Y-m-d H:i:s", strtotime(\Input::get( 'call_start1' ) . " " . \Input::get( 'call_start2' )." +4hour"));
            }
            else {
                $case->call_start = date("Y-m-d H:i:s");
            }
            $case->call_duration = (int)\Input::get( 'duration' );
            $case->call_center_number = (int)\Input::get( 'call_center_number' );

            $case->save();

            if (!empty(\Input::get("comment"))) {
                $comment = new \Comment;
                $comment->owner_id = $case->id;
                $comment->type = "case";
                $comment->user_id = \Auth::user()->id;
                $comment->user_name = \Auth::user()->first_name." ".\Auth::user()->last_name;
                $comment->comment = \Input::get("comment");
                $comment->save();
            }

            if ($id == null) {
                \SecurityLog::addNew("add_case", \Auth::user()->getName()." created a case.");
                return Redirect::action( '\App\Modules\Cases\Http\Controllers\CasesController@getCaseProfile', $case->id )->with( 'message', 'New case saved!' );
            }
            else {
                \SecurityLog::addNew("update_case", \Auth::user()->getName()." updated case <strong>".$case->uid."</strong>.");
                return Redirect::action( '\App\Modules\Cases\Http\Controllers\CasesController@getCaseProfile', $case->id )->with( 'message', 'Case details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteCases()
    {
        foreach (\Input::get("ids") as $id) {
            $case = \ECase::where('id', $id)->get()->first();
            if ($case) {
                \SecurityLog::addNew("delete_case", \Auth::user()->getName()." deleted case <strong>".$case->uid."</strong>.");
                $case->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getAddAuthorizationPage($case_id) {
        if (!$this->me->hasPermission($this->owner->type."_cases_authorization-pages")) return view("errors.403");

        $case = \ECase::where("id", $case_id)->get()->first();

        if ($case->authorization_pages()->count() > 0)
            return Redirect::action( '\App\Modules\Cases\Http\Controllers\CasesController@getCaseProfile', $case->id )->with( 'message', 'This case already has an authorization page!' );

        return view('cases::authorization-pages.add', array("case" => $case));
    }

    public function getEditAuthorizationPage($id = null)
    {
        if (!$this->me->hasPermission($this->owner->type."_cases_authorization-pages")) return view("errors.403");

        $apage = \CaseAuthorizationPage::where('id', $id)->get()->first();

        return view('cases::authorization-pages.edit', array( 'apage' => $apage));
    }

    public function getAuthorizationPageProfile($id = null)
    {
        if (!$this->me->hasPermission($this->owner->type."_cases_authorization-pages")) return view("errors.403");

        $apage = \CaseAuthorizationPage::where('id', $id)->get()->first();
        $case = $apage->ecase()->get()->first();

        return view('cases::authorization-pages.profile', array( 'apage' => $apage, 'case' => $case));
    }

    public function postSaveAuthorizationPage($id = null)
    {
        if (!$this->me->hasPermission($this->owner->type."_cases_authorization-pages")) return view("errors.403");

        if ($id == null)
            $validator = \Validator::make( \Input::all(), \App\Modules\Cases\Validators\AuthorizationPagesValidator::$addRules );
        else
            $validator = \Validator::make( \Input::all(), \App\Modules\Cases\Validators\AuthorizationPagesValidator::$editRules );

        if ( $validator->passes() ) {
            if ($id == null)
                $authorization_page = new \CaseAuthorizationPage;
            else
                $authorization_page = \CaseAuthorizationPage::find($id);

            if ($id == null) {
                $authorization_page->creator_name = \Auth::user()->first_name." ".\Auth::user()->last_name;
                $authorization_page->case_id = \Input::get( 'case_id' );
            }

            if (\Input::get("provider")) {
                $provider = \Provider::where("id", \Input::get("provider"))->get()->first();

                $authorization_page->provider_data = json_encode(array(
                    "id" => $provider->id,
                    "name" => $provider->name,
                    "address" => $provider->getPrimaryAddress(3),
                    "pos_code" => $provider->pos_code()->get()->first()->code." - ".$provider->pos_code()->get()->first()->name
                ));
            }
            if (\Input::get("eap_service"))
                $authorization_page->dictionary_value_id = \Input::get("eap_service");

            $authorization_page->date_from = date("Y-m-d", strtotime(\Input::get( 'date_from' )));
            $authorization_page->date_to = date("Y-m-d", strtotime(\Input::get( 'date_to' )));
            $authorization_page->units = \Input::get("units");
            $authorization_page->max_payment = \Input::get("max_payment");
            $authorization_page->patient_copay = (int)\Input::get("patient_copay");
            $authorization_page->remarks = (string)\Input::get("remarks");
            $authorization_page->save();

            // $uid = $authorization_page->id;
            // $length = strlen($uid);

            // while (strlen($uid) < 6)
            //     $uid = "0".$uid;

            $case = \ECase::where("id", \Input::get( 'case_id' ))->get()->first();

            $authorization_page->UID = $case->UID;
            $authorization_page->save();

            if ($id == null) {
                \SecurityLog::addNew("add_apage", \Auth::user()->getName()." created an authorization page.");
                return Redirect::action( '\App\Modules\Cases\Http\Controllers\CasesController@getAuthorizationPageProfile', $authorization_page->id )->with( 'message', 'New authorization page saved!' );
            }
            else {
                return Redirect::action( '\App\Modules\Cases\Http\Controllers\CasesController@getAuthorizationPageProfile', $authorization_page->id )->with( 'message', 'Authorization page details saved!' );
                \SecurityLog::addNew("update_apage", \Auth::user()->getName()." updated authorization page <strong>".$authorization_page->UID."</strong>.");
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteAuthorizationPages()
    {
        if (!$this->me->hasPermission($this->owner->type."_cases_authorization-pages")) return view("errors.403");

        foreach (\Input::get("ids") as $id) {
            $apage = \CaseAuthorizationPage::where('id', $id)->get()->first();
            if ($apage) {
                \SecurityLog::addNew("delete_apage", \Auth::user()->getName()." deleted authorization page <strong>".$apage->UID."</strong>.");
                $apage->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function postWidgetSearchAuthorizationUid() {
        $authorization_uids = \CaseAuthorizationPage::whereHas("ecase", function ($q) {
                           $q->where('enrollee_data', 'LIKE', '%'.\Input::get("search_text").'%')
                            ->where("type", "OMC")
                           ;
                        })->get();

        return view('cases::authorization-pages.widgets.results_authorization-uids', array("authorization_uids" => $authorization_uids));
    }
}