<?php
namespace App\Modules\Cases\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class CasesServiceProvider extends ServiceProvider
{
	/**
	 * Register the Cases module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Cases\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Cases module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('cases', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('cases', base_path('resources/views/vendor/cases'));
		View::addNamespace('cases', realpath(__DIR__.'/../Resources/Views'));
	}
}
