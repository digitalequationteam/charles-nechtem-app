<?php

namespace App\Modules\UsersManagement\Validators;

class StaffValidator
{
	public static $addRules = array(
        'email'=>'required|email|unique:users',
        'password'=>'required|between:4,12|confirmed',
        'password_confirmation'=>'required|between:4,12',
        'first_name'=>'required|min:2',
        'last_name'=>'required|min:2',
        'initials'=>'required|min:3|max:3',
        'phone'=>'required|min:10',
        'personal_email'=>'email',
        'birthday'=>'required|date',
        'hire_date'=>'required|date',
        'street'=>'required|min:2',
        'city'=>'required|min:2',
        'state'=>'required|min:2',
        'zip'=>'required|min:2'
    );

    public static $saveTemporaryPassRules = array(
        'temporary_password'=>'required|between:4,12'
    );
}