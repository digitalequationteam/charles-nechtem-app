<?php
namespace App\Modules\UsersManagement\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class UsersManagementServiceProvider extends ServiceProvider
{
	/**
	 * Register the UsersManagement module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\UsersManagement\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the UsersManagement module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('users-management', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('users-management', base_path('resources/views/vendor/users-management'));
		View::addNamespace('users-management', realpath(__DIR__.'/../Resources/Views'));
	}
}
