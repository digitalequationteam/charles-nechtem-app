<?php

namespace App\Modules\UsersManagement\Models;
use MicheleAngioni\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
	public static $list = array(
		"admin" => array(
			"dashboard" => array(
				"name" => "Dashboard",
				"roles" => array("all")
			),
			"staff" => array(
				"name" => "Staff",
				"roles" => array("super_admin", "administrator"),
				"sub_items" => array(
					"users" => array("name" => "Users", "roles" => array("super_admin", "administrator")),
					"set_temporary" => array("name" => "Reset Password", "roles" => array("super_admin")),
					"roles-permissions" => array("name" => "Roles/Permissions", "roles" => array("super_admin"))
				),
			),
			"clients" => array(
				"name" => "Clients",
				"roles" => array("super_admin", "administrator", "manager", "editor", "viewer"),
				"sub_items" => array(
					"add" => array("name" => "Add", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"edit" => array("name" => "Edit", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"delete" => array("name" => "Delete", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"plans" => array("name" => "Manage Plans", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"enrollees" => array("name" => "Manage Enrollees", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"documents" => array("name" => "Manage Documents", "roles" => array("super_admin", "administrator", "manager", "editor"))
				)
			),
			"providers" => array(
				"name" => "Providers",
				"roles" => array("super_admin", "administrator", "manager", "editor", "viewer"),
				"sub_items" => array(
					"add" => array("name" => "Add", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"edit" => array("name" => "Edit", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"delete" => array("name" => "Delete", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"costs" => array("name" => "Manage Costs", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"staff" => array("name" => "Manage Staff", "roles" => array("super_admin", "administrator", "manager", "editor"))
				)
			),
			"cases" => array(
				"name" => "Cases",
				"roles" => array("super_admin", "administrator", "manager", "editor", "viewer"),
				"sub_items" => array(
					"add" => array("name" => "Add", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"edit" => array("name" => "Edit", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"delete" => array("name" => "Delete", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"authorization-pages" => array("name" => "Authorization Pages", "roles" => array("super_admin", "administrator", "manager", "editor"))
				)
			),
			"claims" => array(
				"name" => "Claims",
				"roles" => array("super_admin", "administrator", "manager", "editor", "viewer"),
				"sub_items" => array(
					"add" => array("name" => "Add", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"edit" => array("name" => "Edit", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"delete" => array("name" => "Delete", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"services" => array("name" => "Services", "roles" => array("super_admin", "administrator", "manager", "editor"))
				)
			),
			"reports" => array(
				"name" => "Reports",
				"roles" => array("super_admin", "administrator", "manager", "editor", "viewer"),
			),
			"frontend" => array(
				"name" => "Frontend",
				"roles" => array("super_admin", "administrator", "manager", "editor", "viewer"),
				"sub_items" => array(
					"view_site" => array("name" => "View", "roles" => array("all")),
					"default" => array("name" => "Default", "roles" => array("super_admin")),
					"edit" => array(
						"name" => "Edit",
						"roles" => array("super_admin"),
						"sub_items" => array(
							"basic" => array("name" => "Basic", "roles" => array("super_admin")),
							"advanced" => array("name" => "Advanced", "roles" => array("super_admin")),
						)
					),
					"content" => array(
						"name" => "Content", 
						"roles" => array("super_admin", "administrator", "manager", "editor"),
						"sub_items" => array(
							"posts" => array("name" => "Posts", "roles" => array("super_admin", "administrator", "manager", "editor"))
						)
					),
					"backup" => array("name" => "Backup", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"reset" => array("name" => "Reset", "roles" => array("super_admin")),
					"info" => array("name" => "Info", "roles" => array("super_admin"))
				)
			),
			"settings" => array(
				"name" => "Settings",
				"roles" => array("all"),
				"sub_items" => array(
					"values" => array("name" => "Manage Values", "roles" => array("super_admin", "administrator", "manager", "editor")),
					"import-export" => array("name" => "Import/Export", "roles" => array("super_admin")),
					"ip-access" => array("name" => "IP Access", "roles" => array("super_admin")),
					"appearance" => array("name" => "Appearance", "roles" => array("super_admin", "administrator"))
				)
			),
			"security" => array(
				"name" => "Security Log",
				"roles" => array("super_admin"),
				"sub_items" => array(
					"log" => array("name" => "Log", "roles" => array("super_admin")),
					"map" => array("name" => "Map", "roles" => array("super_admin")),
					"map-wide" => array("name" => "Map Wide", "roles" => array("super_admin"))
				)
			),
			"support" => array(
				"name" => "Support",
				"roles" => array("all")
			)
		)
	);

	public static function getDynamicList($user_type) {
		$list = array();

		foreach (\Menu::where("user_type", $user_type)->where("parent_id", 0)->where("category", "menu")->orderBy("order", "ASC")->get() as $menu_item) {
			if ($menu_item->require_permission || (!$menu_item->require_permission && \Menu::where("parent_id", $menu_item->id)->orderBy("order", "ASC")->count() > 0)) {
				$list[$menu_item->key] = array(
					"name" => $menu_item->name,
					"sub_items" => array()
				);

				foreach (\Menu::where("parent_id", $menu_item->id)->orderBy("order", "ASC")->get() as $sub_menu_item) {
					if ($sub_menu_item->require_permission) {
						$list[$menu_item->key]["sub_items"][$sub_menu_item->key] = array(
							"name" => $sub_menu_item->name,
							"sub_items" => array()
						);
					}

					foreach (\Menu::where("parent_id", $sub_menu_item->id)->orderBy("order", "ASC")->get() as $sub_sub_menu_item) {
						if ($sub_sub_menu_item->require_permission) {
							$list[$menu_item->key]["sub_items"][$sub_menu_item->key]["sub_items"][$sub_sub_menu_item->key] = array(
								"name" => $sub_menu_item->name." ".$sub_sub_menu_item->name
							);
						}
					}
				}

				if (isset(\Permission::$list[$user_type][$menu_item->key])) {
					foreach ((array)@\Permission::$list[$user_type][$menu_item->key]["sub_items"] as $key2 => $sub_item) {
						if (!isset($list[$menu_item->key]["sub_items"][$key2])) {
							$list[$menu_item->key]["sub_items"][$key2] = array(
								"name" => $sub_item["name"]
							);
						}
					}
				}
			}
		}

		return $list;
	}

	public static function getByName($name = null, $display_name = null) {
		$permission = Permission::where("name", $name)->get()->first();
		if (!$permission) {
            $permission = new Permission;
            $permission->name         = $name;
            $permission->display_name = $display_name;
            $permission->save();
		}

		return $permission;
	}

	public static function validate() {
		$request_uri = substr(str_replace("/page", "", $_SERVER["REQUEST_URI"]), 1);

		$request_uri = str_replace("/", "_", $request_uri);

		$exploded = explode("/", $request_uri);

		$perm_name = "";
		foreach ($exploded as $item) {
			$perm_name .= $item;
			$permission_names[] = \Auth::user()->Owner()->type."_".$perm_name;
			$exploded2 = explode("-", $perm_name);

			$perm_name2 = "";
			if (count($exploded2) > 1) {
				foreach ($exploded2 as $item2) {
					$perm_name2 .= "_".$item2;
					$permission_names[] = \Auth::user()->Owner()->type.$perm_name2;
				}
			}
		}

		foreach ($permission_names as $permission_name) {
			$permission = \Permission::where("name", $permission_name)->get()->first();

			if ($permission)
				if (!\Auth::user()->hasPermission($permission_name))
					return view("errors.403");
		}
	}
}