<?php

namespace App\Modules\UsersManagement\Models;

use MicheleAngioni\Entrust\EntrustRole;

class Role extends EntrustRole
{
	public function hasPermission($permission_name) {
		return ($this->perms()->where("name", $permission_name)->count() > 0);
	}
}