<?php
namespace App\Modules\UsersManagement\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersManagementDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		//Get admin user.
        $admin = \User::where("type", "admin")->get()->first();

		//Init default levels.
		$default_levels = array(
			"0" => "super_admin",
			"1" => "administrator",
			"2" => "manager",
			"3" => "editor",
			"4" => "viewer"
		);

		//Init default admin roles.
		$default_roles = array(
			array(
				"name" => "super_admin",
				"display_name" => "System",
				"level" => "0"
			),
			array(
				"name" => "administrator",
				"display_name" => "Admin",
				"level" => "1"
			),
			array(
				"name" => "manager",
				"display_name" => "Manager",
				"level" => "2"
			),
			array(
				"name" => "editor",
				"display_name" => "Editor",
				"level" => "3"
			),
			array(
				"name" => "viewer",
				"display_name" => "Viewer",
				"level" => "4"
			)
		);

		//Save default roles to the database.
		foreach ($default_roles as $role) {
			$check_role = $admin->my_roles()->where("name", $role["name"])->get()->first();

			if (!$check_role) {
				$new_role = new \Role;
	            $new_role->user_id = $admin->id;
	            $new_role->name = $role["name"];
	            $new_role->display_name = $role["display_name"];
	            $new_role->level = $role["level"];
	            $new_role->save();
			}
		}

        //Init default permissions.
        $available_permissions = \Permission::$list["admin"];

		//Save default permissions to the database.
        foreach ($available_permissions as $key => $permission_array) {
        	$permission_name = $admin->type."_".$key;
            $permission_display_name = ucfirst($admin->type)." ".$permission_array["name"];

            $permission = \Permission::where("name", $permission_name)->get()->first();

            if (!$permission) {
                $permission = new \Permission;
                $permission->name         = $permission_name;
                $permission->display_name = $permission_display_name;
                $permission->save();
            }

            //Assign permission to the the proper roles.
            foreach ($admin->my_roles()->get() as $role_obj) {
            	if (in_array("all", $permission_array["roles"]) || in_array($role_obj->name, $permission_array["roles"])) {
            		if ($role_obj->perms()->where("name", $permission->name)->count() == 0)
                    	$role_obj->attachPermission($permission);

                	foreach (\User::all() as $user) {
                		if (@$user->getMyRole()->id == @$role_obj->id)
                			if (!$user->hasPermission($permission_name))
                				$user->attachPermission($permission);
                	}
            	}
            }

            foreach ((array)@$permission_array["sub_items"] as $key2 => $sub_item) {
                $permission_name = $admin->type."_".$key."_".$key2;
                $permission_display_name = ucfirst($admin->type)." ".$permission_array["name"]." ".$sub_item["name"];

                $permission = \Permission::where("name", $permission_name)->get()->first();

                if (!$permission) {
                    $permission = new \Permission;
                    $permission->name         = $permission_name;
                    $permission->display_name = $permission_display_name;
                    $permission->save();
                }

                //Assign permission to the the proper roles.
                foreach ($admin->my_roles()->get() as $role_obj) {
                	if (in_array("all", $sub_item["roles"]) || in_array($role_obj->name, $sub_item["roles"])) {
                		if ($role_obj->perms()->where("name", $permission->name)->count() == 0)
	                    	$role_obj->attachPermission($permission);

	                	foreach (\User::all() as $user) {
	                		if (@$user->getMyRole()->id == @$role_obj->id)
	                			if (!$user->hasPermission($permission_name))
	                				$user->attachPermission($permission);
	                	}
                	}
                }

                if (isset($sub_item["sub_items"])) {
	                foreach ((array)$sub_item["sub_items"] as $key3 => $sub_sub_item) {
		                $permission_name = $admin->type."_".$key."_".$key2."_".$key3;
		                $permission_display_name = ucfirst($admin->type)." ".$permission_array["name"]." ".$sub_item["name"]." ".$sub_sub_item["name"];

		                $permission = \Permission::where("name", $permission_name)->get()->first();

		                if (!$permission) {
		                    $permission = new \Permission;
		                    $permission->name         = $permission_name;
		                    $permission->display_name = $permission_display_name;
		                    $permission->save();
		                }

		                //Assign permission to the the proper roles.
		                foreach ($admin->my_roles()->get() as $role_obj) {
		                	if (in_array("all", $sub_sub_item["roles"]) || in_array($role_obj->name, $sub_sub_item["roles"])) {
		                		if ($role_obj->perms()->where("name", $permission->name)->count() == 0)
			                    	$role_obj->attachPermission($permission);

			                	foreach (\User::all() as $user) {
			                		if (@$user->getMyRole()->id == @$role_obj->id)
			                			if (!$user->hasPermission($permission_name))
			                				$user->attachPermission($permission);
			                	}
		                	}
		                }
		            }
	            }
            }
        }

        //Attach the super admin role to the admin user.
        $admin_role = $admin->my_roles()->get()->first();
        if (!$admin->hasRole("super_admin")) {
            $admin->attachRole($admin->my_roles()->where("name", "super_admin")->get()->first());

            foreach ($admin_role->perms()->get() as $permission) {
            	if (!$admin->hasPermission($permission->name))
                	$admin->attachPermission($permission);
            }
        }
	}

}
