<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersStaffFields extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('users',function($table){
            $table->string('personal_email', 255)->nullable();
            $table->date('birthday')->nullable();
            $table->string('ssn', 255)->nullable();
            $table->string('url', 255)->nullable();
            $table->date('hire_date')->nullable();
            $table->string('street', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->string('state', 255)->nullable();
            $table->string('zip', 255)->nullable();
            $table->string('bank_name', 255)->nullable();
            $table->string('bank_routing', 255)->nullable();
            $table->string('bank_account_number', 255)->nullable();
            $table->date('bank_termination_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function($table){
            $table->dropColumn('personal_email');
            $table->dropColumn('birthday');
            $table->dropColumn('ssn');
            $table->dropColumn('url');
            $table->dropColumn('hire_date');
            $table->dropColumn('street');
            $table->dropColumn('city');
            $table->dropColumn('country');
            $table->dropColumn('state');
            $table->dropColumn('zip');
            $table->dropColumn('bank_name');
            $table->dropColumn('bank_routing');
            $table->dropColumn('bank_account_number');
            $table->dropColumn('bank_termination_date');
        });
    }
}