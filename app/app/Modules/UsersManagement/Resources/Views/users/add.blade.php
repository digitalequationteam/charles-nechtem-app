@extends('layouts.main')
@section('content')
<?php
use App\Common;
?>
	<ol class="breadcrumb">
		<li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
		<li><a href="/staff/users">Staff</a></li>
		<li class="active">Register Staff Member</li>
	</ol>
	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Register Staff Member</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getUsers') }}" class="btn btn-primary btn-labeled m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>
	-->

	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@postSaveUser')}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Account Properties</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Email <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="email" id="email" value="<?php echo Input::old('email'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Password <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="password" class="form-control" maxlength="100" name="password" id="password" value="<?php echo Input::old('password'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Confirm Password <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="password" class="form-control" maxlength="100" name="password_confirmation" id="password_confirmation" value="<?php echo Input::old('password_confirmation'); ?>" />
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Personal Data</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">First Name <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="first_name" id="first_name" value="{{ old('first_name') }}" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Last Name <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="last_name" id="last_name" value="<?php echo Input::old('last_name'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Initials <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="3" name="initials" id="initials" value="<?php echo Input::old('initials'); ?>" style="width: 60px;" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Phone <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="phone" id="phone" value="<?php echo Input::old('phone'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Cell Phone</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="cell_phone" id="cell_phone" value="<?php echo Input::old('cell_phone'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Personal Email</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="personal_email" id="personal_email" value="<?php echo Input::old('personal_email'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Birthday <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="text" class="form-control date" maxlength="100" name="birthday" id="birthday" value="<?php echo Input::old('birthday'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">SSN</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="ssn" id="ssn" value="<?php echo Input::old('ssn'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Hire Date <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="text" class="form-control date" maxlength="100" name="hire_date" id="hire_date" value="<?php echo Input::old('hire_date'); ?>" />
					</div>
				</div>
			</div>

			<div class="col-md-6 address_holder">
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Address Information</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Street <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<input type="text" class="form-control" maxlength="100" name="street" id="street" value="<?php echo Input::old('street'); ?>" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">City <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<input type="text" class="form-control" maxlength="100" name="city" id="city" value="<?php echo Input::old('city'); ?>" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">State <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<select class="form-control" name="state" id="state">
								<?php if (Input::old("country")) { ?>
									<?php foreach (\Common::getStates(Input::old("country")) as $state) {
										?>
										<option value="<?php echo $state; ?>" <?php if ($state == Input::old("state")) { ?>selected="selected"<?php } ?>><?php echo $state; ?></option>
										<?php
									}
									?>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Zip Code <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<input type="text" class="form-control" maxlength="100" name="zip" id="zip" value="<?php echo Input::old('zip'); ?>" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Country <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<?php $country = "US"; if (Input::old('country')) $country = Input::old('country'); ?>
							<select class="form-control" name="country" id="country">
								<?php foreach (\Common::getAllCountries() as $code => $name) {
									?>
									<option value="<?php echo $code; ?>" <?php if ($country == $code) { ?>selected="selected"<?php } ?>><?php echo $name; ?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Bank Information</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Bank Name</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" maxlength="100" name="bank_name" id="bank_name" value="{{ old('bank_name') }}" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Bank Routing Number</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" maxlength="100" name="bank_routing" id="bank_routing" value="{{ old('bank_routing') }}" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Account Number</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" maxlength="100" name="bank_account_number" id="bank_account_number" value="{{ old('bank_account_number') }}" />
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Termination Date</label>
						<div class="col-sm-7">
							<input type="text" class="form-control date" maxlength="100" name="bank_termination_date" id="bank_termination_date" value="<?php echo Input::old('bank_termination_date'); ?>" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Security</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Assign Role</label>
						<div class="col-sm-7">
							<select name="role_id" id="role_id" class="form-control">
								@foreach (Auth::user()->Owner()->getSubRoles(Auth::user()) as $role)
									<option value="{{$role->id}}">{{$role->display_name}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#first_name, #last_name").keyup(function() {
				$("#initials").val(($("#first_name").val().substr(0,1) + $("#last_name").val().substr(0,2)).toUpperCase());
			});
		});
	</script>
@stop