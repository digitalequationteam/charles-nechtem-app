@extends('layouts.main')
@section('content')
	<ol class="breadcrumb">
		<li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
		<li class="active">Staff</li>
	</ol>
	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Staff</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getAddUser') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span> Register Staff Member</a>
	</div>

	<div class="clearfix"></div>

	<div class="row users">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
		                    <table id="users-table" class="table table-tools table-striped">
		                        <thead>
		                            <tr>
		                                <th style="min-width:50px">
		                                	<div class="pull-left">
		                                    	<input type="checkbox" class="check_all"/>
		                                    </div>
		                                    <div class="pull-left" style="padding-left: 30px; margin-top: -12px;">
												<a href="#" class="btn btn-danger btn-delete btn-xs" style="margin: 0px;">Delete</a>
											</div>
		                                </th>
		                                <th></th>
		                                <th class="text-right" style="width: 300px; padding-top: 6px; font-size: 12px;">
		                                	Sort:
		                                	<select id="sort-by-dd">
		                                		<option value="name" @if (\Input::get("sort_by") == "name") selected @endif>Name</option>
		                                		<option value="created_at" @if (\Input::get("sort_by") == "created_at") selected @endif>Registration Date</option>
		                                	</select>

		                                	<select id="sort-type-dd">
		                                		<option value="ASC" @if (\Input::get("sort_type") == "ASC") selected @endif>Ascending</option>
		                                		<option value="DESC" @if (\Input::get("sort_type") == "DESC") selected @endif>Descending</option>
		                                	</select>
		                                </th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <tr class="bg-gray-light filters-tr">
		                                <td colspan="3" style="padding-top: 6px;">
		                                	<div class="pull-left" style="padding-left: 8px;">
		                                		<button id="toggle-filters-btn" class="btn btn-danger btn-xm" onclick="toggleFilters();"><i class="fa fa-arrow-down"></i> Search Filters</button>
		                                	</div>

		                                	<div class="clearfix"></div>

		                                	<div id="filters-wrapper" style="padding-top: 10px; padding-bottom: 10px; @if (!\Input::get('applyFilters')) display: none; @endif">
		                                		<form action="" method="get">
		                                			<input type="hidden" name="sort_by" value="{{\Input::get('sort_by')}}" />
		                                			<input type="hidden" name="sort_type" value="{{\Input::get('sort_type')}}" />
		                                			<input type="hidden" name="page_size" value="{{\Input::get('page_size')}}" />
			                                		<div class="row">
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Name</label>
																<div>
																	<input class="form-control" type="text" name="name" value="{{\Input::get('name')}}" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Street</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="street" id="street" value="<?php echo Input::get('street'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>City</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="city" id="city" value="<?php echo Input::get('city'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Country</label>
																<div>
																	<select class="form-control" name="country" id="country">
																		<?php foreach (\Common::getAllCountries() as $code => $name) {
																			?>
																			<option value="<?php echo $code; ?>" <?php if (\Input::get("country") == $code) { ?>selected="selected"<?php } ?>><?php echo $name; ?></option>
																			<?php
																		}
																		?>
																	</select>
																</div>
															</div>
				                                		</div>
				                                	</div>
				                                	<div class="row">
				                                		<div class="col-md-4">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>State</label>
																<div>
																	<select class="form-control" name="state" id="state">
																		<option value="">Select state...</option>
																		<?php if (Input::get("country")) { ?>
																			<?php foreach (\Common::getStates(Input::get("country")) as $state) {
																				?>
																				<option value="<?php echo $state; ?>" <?php if ($state == Input::get("state")) { ?>selected="selected"<?php } ?>><?php echo $state; ?></option>
																				<?php
																			}
																			?>
																		<?php } ?>
																	</select>
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-4">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Zip Code</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="zip" id="zip" value="<?php echo Input::get('zip'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-4">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Role</label>
																<div>
																	<select class="form-control" name="role" id="role">
																		<option value="">Select role...</option>
																			<?php foreach (\Auth::user()->getSubRoles(\Auth::user()) as $role) {
																				?>
																				<option value="<?php echo $role->id; ?>" <?php if ($role->id == Input::get("role")) { ?>selected="selected"<?php } ?>><?php echo $role->display_name; ?></option>
																				<?php
																			}
																			?>
																	</select>
																</div>
															</div>
				                                		</div>
				                                	</div>
				                                	<button class="btn btn-default" name="applyFilters" value="Yes">Apply Filters</button>
				                                	<button onclick="window.location = '/staff/users'; return false;" class="btn btn-default">Reset</button>
				                                </form>
		                                	</div>
		                                </td>
		                            </tr>
		                        	<?php
									foreach ($users as $user) { ?>
										<tr class="odd gradeX">
											<td>
												<div class="pull-left" style="padding-top: 10px;">
													<input type="checkbox" class="batch_id" value="{{$user->id}}" />
												</div>
												<div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
													 <a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getUserProfile', array($user->id)) }}"><strong><?php echo $user->first_name; ?> <?php echo $user->last_name; ?></strong></a>
													 <br />
													 {{$user->street}}
													 <br />
													 {{$user->city}}, 
													 {{$user->state}}, 
													 {{$user->zip}}
												</div>
											</td>
											<td>
												Role: {{$user->getMyRole()->display_name}}<br />
												Email: {{$user->email}}<br />
												@if (!empty($user->personal_email))
													Alt. Email {{$user->personal_email}}
													<br />
												@endif
												Phone: {{$user->phone}}
												<br />
												@if (!empty($user->cell_phone))
													Cell Phone: {{$user->cell_phone}}
												@endif
											</td>
											<td style="text-align: center;">
												<a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getUserProfile', array($user->id)) }}" class="btn bg-blue btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Profile"><i class="fa fa-eye"></i></a>
												@if (Auth::user()->getMyLevel() < $user->getMyLevel())
													<a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getEditUser', array($user->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Staff Member"><i class="fa fa-pencil"></i></a>
													<a href="#" class="btn bg-red btn-delete-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Staff Member"><i class="fa fa-times"></i></a>
												@endif
											</td>
										</tr>
									<?php 
										}
									?>
		                        </tbody>
		                    </table>

		                    @if ($users->count() == 0)
		                    	<br clear="all" />
		                    	No users found.
		                    @endif

		                    <div class="pull-left" style="padding-right: 10px; padding-top: 20px;">
		                    	Per page:
		                    </div>

		                    <div class="pull-left" style="padding-top: 15px;">
                            	<select id="page-size-dd" class="form-control">
                            		<option value="10" @if (\Input::get("page_size") == "10") selected @endif>10</option>
                            		<option value="25" @if (\Input::get("page_size") == "25") selected @endif>25</option>
                            		<option value="100" @if (\Input::get("page_size") == "100") selected @endif>100</option>
                            		<option value="All" @if (\Input::get("page_size") == "All") selected @endif>All</option>
                            	</select>
		                    </div>

		                    <div class="pull-right">
		                    	@if ($users_paginator)
		                    		{!! $users_paginator->render() !!}
		                    	@endif
		                    </div>
		                </div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
	        /* Delete a product */
	        $('#users-table a.btn-delete-single').live('click', function (e) {
	            e.preventDefault();
	            if (confirm("Are you sure to delete this staff member?") == false) {
	                return;
	            }

	            var ids = [$(this).parents("tr").find('.batch_id').val()];

	            $.post("{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@postDeleteUsers') }}", {
	        		ids: ids
	        	}, function() {
	        		window.location = window.location.href;
	        	});
	        });

	        $(".btn-delete").click(function(e) {
	            e.preventDefault();
	        	var ids = [];

	        	$(".batch_id:checked").each(function() {
	        		ids.push($(this).val());
	        	});

	        	if (ids.length == 0) {
	        		alert('No staff members were selected.');
	        		return;
	        	}

	        	if (confirm('Are you sure you want to delete the selected staff members?')) {
		        	$.post("{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@postDeleteUsers') }}", {
		        		ids: ids
		        	}, function() {
		        		window.location = window.location.href;
		        	});
	        	}
	        });

			$("#sort-by-dd, #sort-type-dd, #page-size-dd").change(function() {
				$('input[name="sort_by"]').val($("#sort-by-dd").val());
				$('input[name="sort_type"]').val($("#sort-type-dd").val());
				$('input[name="page_size"]').val($("#page-size-dd").val());
				$("#filters-wrapper form").submit();
			});
	    });

		function toggleFilters() {
			if ($('#filters-wrapper').is(":visible")) {
				$('#filters-wrapper').slideUp();
				$("#toggle-filters-btn i").removeClass("fa-arrow-up").addClass("fa-arrow-down");
			}
			else {
				$('#filters-wrapper').slideDown();
				$("#toggle-filters-btn i").removeClass("fa-arrow-down").addClass("fa-arrow-up");
			}
		}
    </script>
@stop