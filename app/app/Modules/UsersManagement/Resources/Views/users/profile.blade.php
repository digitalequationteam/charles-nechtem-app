@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
		<li><a href="/staff/users">Staff</a></li>
        <li class="active">{{$user->first_name}} {{$user->last_name}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>User Profile</strong></h3>
	</div> <!-- / .page-header -->

    <div class="pull-right" style="padding-left: 10px;">
        @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_staff_set_temporary"))
            <a class="btn btn-primary m-t-10" href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getSetTemporaryPass', array($user->id)) }}"><i class="fa fa-key"></i> Temporary Password</a>
        @endif
        @if (Auth::user()->getMyLevel() < $user->getMyLevel())
            <a class="btn btn-success m-t-10" href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getEditUser', array($user->id)) }}"><i class="fa fa-pencil"></i> Edit User</a>
        @endif
    </div>

	<div class="clearfix"></div>

    @if(Session::has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>
                {{ Session::get('message') }}
            </span>
        </div>
    @endif

    <div class="panel fade active in" id="profile">
        <div class="panel-body no-padding-hr">
            <div class="row p-20">
                <div class="col-md-4">
                    <h3 class="m-t-0">Profile Details</h3>
                    <strong>Role: </strong>{{$user->getMyRole()->display_name}}<br>
                    <strong>First Name: </strong>{{$user->first_name}}<br>
                    <strong>Last Name: </strong>{{$user->last_name}}<br>
                    <strong>Initials: </strong>{{$user->initials}}<br>
                    <strong>Hire Date: </strong>{{date("F s, Y", strtotime($user->hire_date))}}<br>
                </div>
                <div class="col-md-4">
                    <h3 class="m-t-0">Contact Info</h3>
                    <strong>Email: </strong>{{$user->email}}<br>
                    <strong>Personal Email: </strong>{{$user->personal_email}}<br>
                    <strong>Phone: </strong>{{$user->phone}}<br>
                    <strong>Cell Phone: </strong>{{$user->cell_phone}}<br>
                </div>
                <div class="col-md-4">
                    <h3 class="m-t-0">Address</h3>
                    <strong>Street: </strong>{{$user->street}}<br>
                    <strong>City: </strong>{{$user->city}}<br>
                    <strong>State: </strong>{{$user->state}}<br>
                    <strong>Zip Code: </strong>{{$user->zip}}<br>
                    <strong>Country: </strong>{{$user->country}}<br>
                </div>
            </div>
        </div>
    </div>
@stop