@extends('layouts.main')
@section('content')
	<ol class="breadcrumb">
		<li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
		<li><a href="/staff/users">Staff</a></li>
		<li><a href="/staff/user-profile/{{$user->id}}">{{$user->getName()}}</a></li>
		<li class="active">Create Temporary Password</li>
	</ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Create Temporary Password</strong></h3>
	</div> <!-- / .page-header -->

	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@postSaveTemporaryPass', $user->id)}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif

			<div class="col-md-12">
				<br />
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Temporary Password</label>
					<div class="col-sm-4">
						<div class="input-group">
							<input type="text" class="form-control" name="temporary_password" id="temporary_password" value="{{old('password')}}" style="height: 37px;" />
							<span class="input-group-btn">
								<button id="randomIdentifier" class="btn btn-info" type="button">Random</button>
							</span>
						</div>
						<br />
						<div class="random-text text-danger"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#randomIdentifier").click(function() {
			    var text = "";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			    var nr = Math.floor((Math.random() * 4) + 5);

			    for( var i=0; i < nr; i++ )
			        text += possible.charAt(Math.floor(Math.random() * possible.length));

				$("#temporary_password").val(text);

				$(".random-text").html('Make sure you copy the password in a safe place!');
			});
		});
	</script>
@stop