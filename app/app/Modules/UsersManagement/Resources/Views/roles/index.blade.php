@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li class="active">Roles/Permissions</li>
    </ol>
    <div class="page-title pull-left">
        <h3 class="pull-left"><strong>Roles</strong></h3>
    </div> <!-- / .page-header -->

    <div class="pull-right">
        <a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getUsers') }}" class="btn btn-primary btn-labeled" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
    </div>

    <div class="clearfix"></div>

	@if(Session::has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>
                {{ Session::get('message') }}
            </span>
	    </div>
    @endif

	@if($errors->all())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach($errors->all() as $error_msg)
        
                <span>
                 {{ $error_msg }}
                </span><br/>
            @endforeach
        </div>
    @endif

	<div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel">
                <div class="postbox">
                    <div class="panel-heading">
                    	<h3 class="panel-title">
	                        <span>Access Manager</span></small>
	                        <span class="aam-help-menu" data-target="#access-manager-inside"><i class="fa  fa-help-circled"></i></span>
	                    </h3>
	                </div>
                    <div class="panel-body" id="access-manager-inside">
                        <div class="aam-feature active" id="admin_menu-content">
    <div class="row">
        <div class="col-xs-12">
            <p class="aam-info">
                Manage permissions for: {{ucfirst($item_type)}} <strong>@if ($item_type == "role") {{$item->display_name}} @else {{$item->first_name}} {{$item->last_name}} @endif</strong>
            </p>
        </div>
    </div>
    <div class="panel-group" id="admin-menu" role="tablist" aria-multiselectable="true">
    	<form action="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@postSavePermissions') }}" method="post">
    		<input type="hidden" name="item_type" value="{{$item_type}}" />
    		<input type="hidden" name="item_id" value="{{$item->id}}" />

            @foreach (\Permission::getDynamicList("admin") as $key => $permission)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="menu-{{$key}}-heading" style="padding: 10px;">
                        <div class="pull-right">
                            <?php
                            $permission_name = \Auth::user()->Owner()->type."_".$key;
                            $permission_obj = \Permission::where("name", $permission_name)->get()->first();

                            $permission_available = true;
                            if ($item_type == "user") {
                                if ($item->getMyRole()->perms()->where("name", $permission_obj->name)->count() == 0)
                                    $permission_available = false;
                            }
                            ?>
                            <div class="col-xs-12 col-md-6 aam-submenu-item" style="padding-right: 30px;">
                                <input data-name="{{$permission_obj->name}}" type="checkbox" class="aam-checkbox-danger" name="permission[{{$permission_obj->id}}]" value="{{$permission_obj->id}}" @if ($item->hasPermission($permission_obj->name)) checked="checked" @endif @if (!$permission_available) disabled="disabled" @endif />
                            </div>
                        </div>
                        <h3 class="panel-title">
                            <a role="button" class='collapsed' data-toggle="collapse" data-parent="#admin-menu" href="#menu-{{$key}}" aria-controls="menu-{{$key}}">
                                {{$permission["name"]}}
                            </a>
                        </h3>
                    </div>

                    <div id="menu-{{$key}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="menu-{{$key}}-heading">
                        <div class="panel-body">
                            <div class="row aam-bordered">
                                @if (count((array)@$permission["sub_items"]) == 0)
                                    There are no sub items.
                                @endif
                                @foreach ((array)@$permission["sub_items"] as $key2 => $sub_permission)
                                <?php
                                $permission_name = \Auth::user()->Owner()->type."_".$key."_".$key2;

                                $permission = \Permission::where("name", $permission_name)->get()->first();

                                $permission_available = true;
                                if ($item_type == "user") {
                                    if ($item->getMyRole()->perms()->where("name", @$permission->name)->count() == 0)
                                        $permission_available = false;
                                }

                                if (!$permission)
                                    continue;
                                ?>
                                <div class="col-xs-12 col-md-6 aam-submenu-item">
                                    <input data-name="{{$permission->name}}" type="checkbox" class="aam-checkbox-danger" name="permission[{{$permission->id}}]" value="{{$permission->id}}" @if ($item->hasPermission($permission->name)) checked="checked" @endif @if (!$permission_available) disabled="disabled" @endif />
                                    <label for="menu-item-00" style="padding-left: 30px; font-size: 13px; font-weight: 300;">{{@$sub_permission["name"]}}</label>                                   
                                </div>

                                    @foreach ((array)@$sub_permission["sub_items"] as $key3 => $sub_sub_permission)
                                    <?php
                                    $permission_name = \Auth::user()->Owner()->type."_".$key."_".$key2."_".$key3;
                                    $permission = \Permission::where("name", $permission_name)->get()->first();

                                    $permission_available = true;
                                    if ($item_type == "user") {
                                        if ($item->getMyRole()->perms()->where("name", @$permission->name)->count() == 0)
                                            $permission_available = false;
                                    }

                                    if (!$permission)
                                        continue;
                                    ?>
                                    <div class="col-xs-12 col-md-6 aam-submenu-item">
                                        <input data-name="{{$permission->name}}" type="checkbox" class="aam-checkbox-danger" name="permission[{{$permission->id}}]" value="{{$permission->id}}" @if ($item->hasPermission($permission->name)) checked="checked" @endif @if (!$permission_available) disabled="disabled" @endif />
                                        <label for="menu-item-00" style="padding-left: 30px; font-size: 13px; font-weight: 300;">{{@$sub_sub_permission["name"]}}</label>                                   
                                    </div>
                                    @endforeach

                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
		    	<br />
		    	<button name="Save" class="btn btn-success">Save</button>
		</form>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>

        <div class="col-xs-12 col-md-6">
            <div class="panel">
              	<input type="hidden" id="create_user_url" value="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getAddUser') }}" />  
                <div class="postbox">
                    <div class="panel-heading">
                    	<h3 class="panel-title">
	                        <span>User/Role Manager</span>
	                        <span class="aam-help-menu" data-target="#user-role-manager-inside"><i class="fa  fa-help-circled"></i></span>
	                    </h3>
	                </div>
                    <div class="panel-body" id="user-role-manager-inside">
                        <div class="aam-help-context">
                            <p class="aam-info aam-hint">
                                Manage access for your users and roles.
                            </p>
                        </div>
                        <div class="aam-postbox-inside">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#roles" aria-controls="roles" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-users"></i> Roles</a></li>
                                <li role="presentation" class=""><a href="#users" aria-controls="users" role="tab" data-toggle="tab" aria-expanded="false"><i class="fa  fa-user"></i> Users</a></li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="roles">
                                    <a href="#add-role-modal" data-toggle="modal" class="btn btn-info pull-right"><i class="fa fa-plus"></i>&nbsp;Add Role</a>
                                    <div class="clearfix"></div>
                                    <br />

                                    <table id="role-list" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Role</th>
                                                <th>Level</th>
                                                <th>Users</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	@foreach (\Auth::user()->Owner()->my_roles()->where("level", ">", 0)->orderBy(DB::raw('CAST(level AS DECIMAL(2,1))'), "ASC")->get() as $role)
	                                            <tr>
                                                    <td>{{$role->display_name}}</td>
                                                    <td>{{$role->level}}</td>
	                                                <td>{{$role->users()->count()}}</td>
	                                                <th>
	                                                	<a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getRolesPermissions', array($role->id, 'role')) }}" class="btn bg-primary btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Manage Role Permissions"><i class="fa fa-cog"></i></a>
	                                                	@if ($role->name != "administrator" && $role->name != "super_admin")
															<a href="#edit-role-modal" onclick="$('#edit_role_id').val('{{$role->id}}'); $('#edit-role-name').val('{{$role->display_name}}'); $('#edit-role-level').val('{{$role->level}}');" data-toggle="modal" class="btn bg-success btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Role"><i class="fa fa-pencil"></i></a>
															<a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getDeleteRole', array($role->id)) }}" class="btn bg-danger btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Role"><i class="fa fa-times"></i></a>
														@endif
	                                                </th>
	                                            </tr>
                                        	@endforeach
                                        </tbody>
                                    </table>

                                    <div class="modal fade" id="add-role-modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Add New Role</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="new-role-name">Role Name<span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" id="new-role-name" placeholder="Enter Role Name" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="new-role-name">Level<span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" id="new-role-level" placeholder="Enter Role Level" value="10" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inherit-role-list">Inherit Capabilities</label>
														<select name="inherit-role-list" id="inherit-role-list" class="form-control">
															@foreach (\Auth::user()->Owner()->my_roles()->get() as $role)
																<option value="{{$role->id}}">{{$role->display_name}}</option>
															@endforeach
														</select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" id="add-role-btn">Add Role</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="edit-role-modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Edit Role</h4>
                                                </div>
                                                <div class="modal-body">
                                                	<input type="hidden" id="edit_role_id" />
                                                    <div class="form-group">
                                                        <label for="new-role-name">Role Name</label>
                                                        <input type="text" class="form-control" id="edit-role-name" placeholder="Enter Role Name" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="new-role-name">Level<span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" id="edit-role-level" placeholder="Enter Role Level" value="10" />
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" id="edit-role-btn">Update Role</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="delete-role-modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Delete Role</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="text-center aam-confirm-message" data-message="Are you sure that you want to delete %s role?"></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" id="delete-role-btn">Delete Role</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="role-notification-modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Notification</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="text-center aam-notification-message">You are not allowed to delete this role because there is at least one user assigned to it.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="users">
                                    <a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getAddUser')}}" class="btn btn-info pull-right"><i class="fa fa-plus"></i>&nbsp;Add User</a>
                                    <div class="clearfix"></div>
                                    <br />

                                    <table id="user-list" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	@foreach (\Auth::user()->Owner()->children()->where("type", "user")->orderBy("created_at", "ASC")->get() as $user)
	                                            <tr>
	                                                <td>
                                                        {{$user->first_name}} {{$user->last_name}}
                                                        <input type="hidden" class="batch_id" value="{{$user->id}}" />
                                                    </td>
	                                                <td>{{@$user->roles()->get()->first()->display_name}}</td>
	                                                <th>
	                                                	<a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getRolesPermissions', array($user->id, 'user')) }}" class="btn bg-primary btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Manage User Permissions"><i class="fa fa-cog"></i></a>

	                                                	@if (\Auth::user()->id == $user->id)
															<a href="/settings/profile" class="btn bg-success btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit User"><i class="fa fa-pencil"></i></a>
														@else
															<a href="{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getEditUser', array($user->id)) }}" class="btn bg-success btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit User"><i class="fa fa-pencil"></i></a>
                                                            <a href="#" class="btn bg-danger btn-xs btn-delete-single tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete User"><i class="fa fa-times"></i></a>
														@endif
	                                                </th>
	                                            </tr>
                                        	@endforeach
                                        </tbody>
                                    </table>

                                    <div class="modal fade" id="user-notification-modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Notification</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="text-center aam-notification-message">You are not allowed to block yourself.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            /* Delete a product */
            $('.btn-delete-single').live('click', function (e) {
                e.preventDefault();
                if (confirm("Are you sure to delete this staff member?") == false) {
                    return;
                }

                var tr = $(this).parents("tr");

                var ids = [tr.find('.batch_id').val()];

                tr.slideUp("fast");

                $.post("{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@postDeleteUsers') }}", {
                    ids: ids
                }, function() {});
            });
        });
    </script>
@stop