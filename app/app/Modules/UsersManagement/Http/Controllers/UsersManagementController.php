<?php

namespace App\Modules\UsersManagement\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class UsersManagementController extends Controller
{	
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

    public function getRunSeeder() {
        \Artisan::call('db:seed', array('--class'=> "\App\Modules\UsersManagement\Database\Seeds\UsersManagementDatabaseSeeder"));
    }

	public function getUsers() {
        $users = $this->owner->children()
                    ->where("type", "user")
                    ->whereHas('roles', function ($q) {
                       $q->where('level', ">=", $this->me->getMyLevel());
                    });

        $appends = array();
        foreach ($_GET as $key => $value) {
            if ($key != "applyFilters" && !empty($value)) {
                $appends[$key] = $value;

                switch ($key) {
                    case 'name';
                        $users = $users->where(\DB::raw("CONCAT(first_name,' ',last_name)"), 'LIKE', '%'.$value.'%');
                    break;
                    case 'street';
                        $users = $users->where("street", 'LIKE', '%'.$value.'%');
                    break;
                    case 'city';
                        $users = $users->where("city", 'LIKE', '%'.$value.'%');
                    break;
                    case 'country';
                        $users = $users->where("country", 'LIKE', '%'.$value.'%');
                    break;
                    case 'state';
                        $users = $users->where("state", 'LIKE', '%'.\Common::getStateCode($value).'%');
                    break;
                    case 'zip';
                        $users = $users->where("zip", 'LIKE', '%'.$value.'%');
                    break;
                    case 'role';
                        $users = $users->whereHas("roles", function($query) {
                            $query->where("id", $_GET['role']);
                        });
                    break;
                }
            }
        }

        $sort_by = (\Input::get("sort_by")) ? \Input::get("sort_by") : "name";
        $sort_type = (\Input::get("sort_type")) ? \Input::get("sort_type") : "ASC";

        if ($sort_by == "name") $sort_by = \DB::raw("CONCAT(first_name,' ',last_name)");

        $users = $users->orderBy($sort_by, $sort_type);

        $page_size = (\Input::get("page_size")) ? \Input::get("page_size") : 10;
        if ($page_size != "All") {
            $users = $users->paginate($page_size);
            $users_paginator = $users;
            $users_paginator = $users_paginator->appends($appends);
        }
        else {
            $users = $users->get();
        }

        if (\Input::get("page") && $users->count() == 0)
            return redirect($users_paginator->url($users_paginator->lastPage()));

        return view('users-management::users.index', array('users' => $users, 'users_paginator' => @$users_paginator));
	}

	public function getAddUser() {
        return view('users-management::users.add');
	}

    public function getEditUser($id = null)
    {
        $user = $this->owner->children()->where("type", "user")->where('id', $id)->get()->first();

        return view('users-management::users.edit', array( 'user' => $user ));
    }

    public function getSetTemporaryPass($id = null)
    {
        $user = $this->owner->children()->where("type", "user")->where('id', $id)->get()->first();

        return view('users-management::users.temporary-pass', array( 'user' => $user ));
    }

    public function postSaveTemporaryPass($id = null)
    {
        $validator = \Validator::make( \Input::all(), \App\Modules\UsersManagement\Validators\StaffValidator::$saveTemporaryPassRules );

        if ( $validator->passes() ) {
            $user = \User::find($id);
            $user->password = \Hash::make( \Input::get( 'temporary_password' ) );
            $user->save();

            $agency = $user;
            $login_url = \URL::to("/auth/reset-password/".base64_encode($user->email."|SEPARATOR|".$user->password));

            $data = array(
                "first_name" => $user->first_name,
                "last_name" => $user->last_name,
                "email" => $user->email,
                "link" => $login_url,
                "signature" => \Config::get("app.name")
            );

            \Mail::send('emails.temporary_password', $data, function($message) use ($user, $agency) {
                $message->to($user->email, $user->first_name." ".$user->last_name)->from($agency->email, $agency->first_name)->subject("Change temporary password.");
            }); 

            \SecurityLog::addNew("temporary_password", \Auth::user()->getName()." changed <strong>".$user->first_name." ".$user->last_name."</strong>'s password to a temporary password.");
            return Redirect::action( '\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getUserProfile', $user->id )->with( 'message', 'Temporary password saved! An email has been sent to the user.' );
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function getUserProfile($id = null)
    {
        $user = $this->owner->children()->where('id', $id)->get()->first();

        return view('users-management::users.profile', array( 'user' => $user));
    }

    public function postSaveUser($id = null)
    {
        if ($id == null) {
            $validator = \Validator::make( \Input::all(), \App\Modules\UsersManagement\Validators\StaffValidator::$addRules );
        }
        else {
            \App\Modules\UsersManagement\Validators\StaffValidator::$addRules['email'] = 'required|email|unique:users,email,' . $id;
            \App\Modules\UsersManagement\Validators\StaffValidator::$addRules['password'] = 'between:4,12|confirmed';
            \App\Modules\UsersManagement\Validators\StaffValidator::$addRules['password_confirmation'] = 'between:4,12';

            $validator = \Validator::make( \Input::all(), \App\Modules\UsersManagement\Validators\StaffValidator::$addRules );
        }

        if ( $validator->passes() ) {
            if ($id == null)
                $user = new \User;
            else
                $user = \User::find($id);

            $user->parent_id = $this->owner->id;
            $user->type = "user";

            $user->email = \Input::get( 'email' );

            if (!empty(\Input::get( 'password' )))
                $user->password = \Hash::make( \Input::get( 'password' ) );

            $user->first_name = \Input::get( 'first_name' );
            $user->last_name = \Input::get( 'last_name' );
            $user->initials = \User::generateUniqueInitials(\Input::get( 'initials' ));
            $user->phone = \Input::get( 'phone' );
            $user->cell_phone = (string)\Input::get( 'cell_phone' );
            $user->personal_email = \Input::get( 'personal_email' );
            $user->birthday = (\Input::get( 'birthday' )) ? date("Y-m-d", strtotime(\Input::get( 'birthday' ))) : null;;
            $user->ssn = \Input::get( 'ssn' );
            $user->hire_date = date("Y-m-d", strtotime(\Input::get( 'hire_date' )));
            $user->street = \Input::get( 'street' );
            $user->city = \Input::get( 'city' );
            $user->country = "US";
            $user->state = \Input::get( 'state' );
            $user->zip = \Input::get( 'zip' );
            $user->bank_name = \Input::get( 'bank_name' );
            $user->bank_routing = \Input::get( 'bank_routing' );
            $user->bank_account_number = \Input::get( 'bank_account_number' );
            $user->hire_date = date("Y-m-d", strtotime(\Input::get( 'hire_date' )));

            //$user->active = 1;
            $user->save();

            $role = \Role::where("id", \Input::get("role_id"))->get()->first();
            //echo "<pre>";
            //if (@$role->id != @$user->getMyRole()->id) {
                foreach ($user->roles()->get() as $role)
                    $user->roles()->detach($role->id);

                foreach ($user->perms()->get() as $perm)
                    $user->perms()->detach($perm->id);

                $user->roles()->attach(\Input::get("role_id"));

                //var_dump($role->perms()->get()); exit();

                foreach ($role->perms()->get() as $permission) {
                    $user->attachPermission($permission);
                }
            //}

            if ($id == null) {
                \SecurityLog::addNew("add_user", \Auth::user()->getName()." added staff member <strong>".$user->first_name." ".$user->last_name."</strong>.");
                return Redirect::action( '\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getUsers' )->with( 'message', 'New staff member saved!' );
            }
            else {
                \SecurityLog::addNew("update_user", \Auth::user()->getName()." updated staff member <strong>".$user->first_name." ".$user->last_name."</strong>.");
                return Redirect::action( '\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getUsers' )->with( 'message', 'Staff Member details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteUsers()
    {
        foreach (\Input::get("ids") as $id) {
            $user = \User::where('parent_id', $this->owner->id)->where('id', $id)->get()->first();
            if ($user) {
                \SecurityLog::addNew("delete_user", \Auth::user()->getName()." deleted staff member <strong>".$user->first_name." ".$user->last_name."</strong>.");
                $user->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getRolesPermissions($item_id = null, $item_type = "role") {
        if ($item_type == "role") {
            if ($item_id == null)
                $item_id = $this->owner->my_roles()->get()->first()->id;

            $item = $this->owner->my_roles()->where("id", $item_id)->get()->first();
        }
        elseif ($item_type == "user") {
            $item = $this->owner->children()->where("id", $item_id)->get()->first();
            if (!$item)
                $item = $this->owner;
        }

        return view('users-management::roles.index', array("item_type" => $item_type, "item" => $item));
    }

    public function getDeleteRole($id = null) {
        $role = $this->owner->my_roles()->where("id", $id)->get()->first();

        if ($role->users()->count() > 0)
            return Redirect::back()->withErrors(["You are not allowed to delete this role because there is at least one user assigned to it."])->withInput();

        if ($role)
            $role->delete();

        return Redirect::action( '\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@getRolesPermissions' )->with( 'message', 'Role removed.' );
    }

    public function postSavePermissions() {
        $item_type = \Input::get("item_type");
        $item_id = \Input::get("item_id");

        if ($item_type == "role")
            $item = $this->owner->my_roles()->where("id", $item_id)->get()->first();
        elseif ($item_type == "user") {
            $item = $this->owner->children()->where("id", $item_id)->get()->first();
            if (!$item)
                $item = $this->owner;
        }

        foreach ($item->perms()->get() as $perm)
            $item->perms()->detach($perm->id);

        foreach (@\Input::get("permission") as $permission_id) {
            $permission = \Permission::where("id", $permission_id)->get()->first();

            if (!$item->hasPermission($permission->name))
                $item->attachPermission($permission);
        }

        if ($item_type == "role") {
            foreach ($item->users()->get() as $user) {
                foreach ($user->perms()->get() as $perm)
                    $user->perms()->detach($perm->id);

                foreach (\Input::get("permission") as $permission_id) {
                    $permission = \Permission::where("id", $permission_id)->get()->first();

                    $user->attachPermission($permission);
                }
            }
        }

        if ($item_type == "user")
            $name = $item->first_name." ".$item->last_name;
        elseif ($item_type == "role")
            $name = $item->display_name;

        \SecurityLog::addNew("update_".$item_type."_permissions", \Auth::user()->getName()." update permissions for ".$item_type." <strong>".$name."</strong>.");

        return Redirect::back()->with( 'message', 'Permissions saved.' );
    }

    public function postSaveRole($id = null) {
        if ($id == null)
            $role = new \Role;
        else
            $role = \Auth::user()->Owner()->my_roles()->where("id", $id)->get()->first();

        $role->user_id = \Auth::user()->Owner()->id;
        $role->name = \Common::slugify(\Input::get("name"));
        $role->display_name = \Input::get("name");
        $role->level = \Input::get("level");
        if (\Input::get("level")) $role->level = \Input::get("level");
        $role->save();

        if (\Input::get("inherit_id")) {
            $inherit_role = \Auth::user()->Owner()->my_roles()->where("id", \Input::get("inherit_id"))->get()->first();

            foreach ($inherit_role->perms()->get() as $perm) {
                $role->attachPermission($perm);
            }
        }

        \SecurityLog::addNew("add_role", \Auth::user()->getName()." added new role <strong>".$role->display_name."</strong>.");

        return response()->json(array('status' => "ok"));
    }
}