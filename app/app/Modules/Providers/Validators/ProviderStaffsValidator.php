<?php

namespace App\Modules\Providers\Validators;

class ProviderStaffsValidator
{
	public static $addRules = array(
        'first_name'=>'required',
        'last_name'=>'required'
    );
}