<?php

namespace App\Modules\Providers\Validators;

class ProviderCostsValidator
{
	public static $addRules = array(
        'name'=>'required',
        'cost'=>'required|numeric'
    );
}