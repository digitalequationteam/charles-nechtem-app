<?php

namespace App\Modules\Providers\Validators;

class ProvidersValidator
{
	public static $addRules = array(
        'name'=>'required',
        'primary_address' => 'required'
    );
}