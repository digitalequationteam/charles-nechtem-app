<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('providers', function($table)
        {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('type', 255);
            $table->string('gender', 255);
            $table->date('birthday')->nullable();
            $table->integer('pos_code_id');
            $table->string('npi', 255)->nullable();
            $table->string('tax_id', 255)->nullable();
            $table->string('memo', 1000)->nullable();

            $table->string('primary_street', 255);
            $table->string('primary_city', 255);
            $table->string('primary_country', 255);
            $table->string('primary_state', 255);
            $table->string('primary_zip', 255);

            $table->boolean('secondary_address_available')->default(0);
            $table->string('secondary_street', 255)->nullable();
            $table->string('secondary_city', 255)->nullable();
            $table->string('secondary_country', 255)->nullable();
            $table->string('secondary_state', 255)->nullable();
            $table->string('secondary_zip', 255)->nullable();

            $table->boolean('mailing_same_as_primary')->default(1);
            $table->string('mailing_street', 255)->nullable();
            $table->string('mailing_city', 255)->nullable();
            $table->string('mailing_country', 255)->nullable();
            $table->string('mailing_state', 255)->nullable();
            $table->string('mailing_zip', 255)->nullable();

            $table->string('specialities', 1000)->nullable();
            $table->string('insurance_networks', 1000)->nullable();
            $table->string('languages', 1000)->nullable();

            $table->string('status', 20)->nullable(); //active/inactive/cancelled/archived
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('providers');
    }
}