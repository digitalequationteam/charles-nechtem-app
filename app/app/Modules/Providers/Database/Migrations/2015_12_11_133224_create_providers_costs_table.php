<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersCostsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('provider_costs', function($table)
        {
            $table->increments('id');
            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on('providers')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name', 255)->nullable();
            $table->string('type', 255)->nullable();
            $table->string('dictionary_value_id', 255)->nullable();
            $table->decimal('cost', 10, 2)->nullable();
            $table->string('representative', 255)->nullable();
            $table->date('signing_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('provider_costs');
    }
}