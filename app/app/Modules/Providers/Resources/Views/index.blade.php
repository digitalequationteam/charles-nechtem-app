@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li class="active">Providers</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Providers</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_add"))
			<a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getAddProvider') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span> Register Provider</a>
		@endif
	</div>

	<div class="clearfix"></div>

	<div class="row users">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
		                    <table id="providers-table" class="table table-tools table-striped">
		                        <thead>
		                            <tr>
		                                <th style="min-width:50px">
		                                	<div class="pull-left">
		                                		@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_delete"))
		                                    		<input type="checkbox" class="check_all"/>
		                                    	@endif
		                                    </div>
		                                    <div class="pull-left" style="padding-left: 30px; margin-top: -12px;">
		                                    	@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_delete"))
													<a href="#" class="btn btn-danger btn-delete btn-xs" style="margin: 0px;">Delete</a>
												@endif
											</div>
		                                </th>
		                                <th></th>
		                                <th class="text-right" style="width: 300px; padding-top: 6px; font-size: 12px;">
		                                	Sort:
		                                	<select id="sort-by-dd">
		                                		<option value="id" @if (\Input::get("sort_by") == "id") selected @endif>UID</option>
		                                		<option value="name" @if (\Input::get("sort_by") == "name") selected @endif>Name</option>
		                                		<option value="primary_city" @if (\Input::get("sort_by") == "primary_city") selected @endif>City</option>
		                                		<option value="primary_state" @if (\Input::get("sort_by") == "primary_state") selected @endif>State</option>
		                                		<option value="pos_code_id" @if (\Input::get("sort_by") == "pos_code") selected @endif>POS Code</option>
		                                		<option value="created_at" @if (\Input::get("sort_by") == "created_at") selected @endif>Registration Date</option>
		                                	</select>

		                                	<select id="sort-type-dd">
		                                		<option value="ASC" @if (\Input::get("sort_type") == "ASC") selected @endif>Ascending</option>
		                                		<option value="DESC" @if (\Input::get("sort_type") == "DESC") selected @endif>Descending</option>
		                                	</select>
		                                </th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <tr class="bg-gray-light filters-tr">
		                                <td colspan="3" style="padding-top: 6px;">
		                                	<div class="pull-left" style="padding-left: 8px;">
		                                		<button id="toggle-filters-btn" class="btn btn-danger btn-xm" onclick="toggleFilters();"><i class="fa fa-arrow-down"></i> Search Filters</button>
		                                	</div>

		                                	<div class="pull-right" style="padding-left: 8px;">
		                                		<button id="download-csv-btn" class="btn btn-success btn-xm" onclick="downloadCSV();"><i class="fa fa-download"></i> Export</button>
		                                	</div>

		                                	<div class="clearfix"></div>

		                                	<div id="filters-wrapper" style="padding-top: 10px; padding-bottom: 10px; @if (!\Input::get('applyFilters')) display: none; @endif">
		                                		<form action="" method="get">
		                                			<input type="hidden" name="sort_by" value="{{\Input::get('sort_by')}}" />
		                                			<input type="hidden" name="sort_type" value="{{\Input::get('sort_type')}}" />
		                                			<input type="hidden" name="page_size" value="{{\Input::get('page_size')}}" />
		                                			<input type="hidden" name="export" value="0" />

			                                		<div class="row">
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Name</label>
																<div>
																	<input class="form-control" type="text" name="name" value="{{\Input::get('name')}}" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>License Title</label>
																<div>
																	<select name="license_title[]" id="license_title" multiple="multiple" style="width: 100%;">
																	    <option value="APRN" @if (in_array("APRN", (array)\Input::get("license_title"))) selected @endif>APRN</option>
																	    <option value="EdD" @if (in_array("EdD", (array)\Input::get("license_title"))) selected @endif>EdD</option>
																	    <option value="LCSW" @if (in_array("LCSW", (array)\Input::get("license_title"))) selected @endif>LCSW</option>
																	    <option value="LICSW" @if (in_array("LICSW", (array)\Input::get("license_title"))) selected @endif>LICSW</option>
																	    <option value="LMFT" @if (in_array("LMFT", (array)\Input::get("license_title"))) selected @endif>LMFT</option>
																	    <option value="LMHC" @if (in_array("LMHC", (array)\Input::get("license_title"))) selected @endif>LMHC</option>
																	    <option value="LMSW" @if (in_array("LMSW", (array)\Input::get("license_title"))) selected @endif>LMSW</option>
																	    <option value="LPC" @if (in_array("LPC", (array)\Input::get("license_title"))) selected @endif>LPC</option>
																	    <option value="MD" @if (in_array("MD", (array)\Input::get("license_title"))) selected @endif>MD</option>
																	    <option value="PhD" @if (in_array("PhD", (array)\Input::get("license_title"))) selected @endif>PhD</option>
																	    <option value="PMHNP" @if (in_array("PMHNP", (array)\Input::get("license_title"))) selected @endif>PMHNP</option>
																	    <option value="PsyD" @if (in_array("PsyD", (array)\Input::get("license_title"))) selected @endif>PsyD</option>
																	    <option value="Other" @if (in_array("Other", (array)\Input::get("license_title"))) selected @endif>Other</option>
																	</select>
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Street</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="street" id="street" value="<?php echo Input::get('street'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>City</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="city" id="city" value="<?php echo Input::get('city'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Country</label>
																<div>
																	<select class="form-control" name="country" id="country">
																		<?php foreach (\Common::getAllCountries() as $code => $name) {
																			?>
																			<option value="<?php echo $code; ?>" <?php if (\Input::get("country") == $code) { ?>selected="selected"<?php } ?>><?php echo $name; ?></option>
																			<?php
																		}
																		?>
																	</select>
																</div>
															</div>
				                                		</div>
				                                	</div>

				                                	<div class="row">
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>POS Code</label>
																<div>
																	<select name="pos_code_id[]" id="pos_code" class="form-control" multiple="multiple" style="width: 100%;">
																		@foreach (\DictionaryValue::where("category", "pos_codes")->get() as $pos_code)
																	    	<option value="{{$pos_code->id}}" @if (in_array($pos_code->id, (array)\Input::get("pos_code_id"))) selected @endif>{{$pos_code->name}}</option>
																	    @endforeach
																	</select>
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Tax ID</label>
																<div>
																	<input class="form-control" type="text" name="tax_id" value="{{\Input::get('tax_id')}}" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>State</label>
																<div>
																	<select class="form-control" name="state" id="state">
																		<option value="">Select state...</option>
																		<?php
																		$country = (Input::get("country")) ? Input::get("country") : "US";
																		if ($country) { ?>
																			<?php foreach (\Common::getStates($country) as $key => $state) {
																				?>
																				<option value="<?php echo $key; ?>" <?php if ($key == Input::get("state")) { ?>selected="selected"<?php } ?>><?php echo $state; ?></option>
																				<?php
																			}
																			?>
																		<?php } ?>
																	</select>
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Zip Code</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="zip" id="zip" value="<?php echo Input::get('zip'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Miles Range</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="range" id="range" value="<?php echo Input::get('range'); ?>" />
																</div>
															</div>
				                                		</div>
				                                	</div>
				                                	<button class="btn btn-default" name="applyFilters" value="Yes">Apply Filters</button>
				                                	<button onclick="window.location = '/providers'; return false;" class="btn btn-default">Reset</button>
				                                </form>
		                                	</div>
		                                </td>
		                            </tr>
		                        	<?php
									foreach ($providers as $provider) { ?>
										<tr class="odd gradeX">
											<td>
												<div class="pull-left" style="padding-top: 10px;">
													@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_delete"))
														<input type="checkbox" class="batch_id" value="{{$provider->id}}" />
													@endif
												</div>
												<div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
													 <a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', array($provider->id)) }}"><strong><?php echo $provider->name; ?></strong></a>
													 <br />
													 {{$provider->getPrimaryAddress()}}
													 @if (
												 		(\Input::get("street") && $provider->secondary_street && strpos($provider->secondary_street, \Input::get("street")) !== false) || 
												 		(\Input::get("city") && $provider->secondary_street && strpos($provider->secondary_city, \Input::get("city")) !== false) || 
												 		(\Input::get("state") && $provider->secondary_street && strpos($provider->secondary_state, \Input::get("state")) !== false) || 
												 		(\Input::get("zip") && $provider->secondary_street && strpos($provider->secondary_zip, \Input::get("zip")) !== false) || 
												 		(\Input::get("country") && $provider->secondary_street && strpos($provider->secondary_country, \Input::get("country")) !== false)
													 )
													 	<br />
													 	{{$provider->getSecondaryAddress()}}
													 @endif
												</div>
											</td>
											<td>
												 <?php echo $provider->getMainContact(); ?>
											</td>
											<td style="text-align: center;">
												<a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', array($provider->id)) }}" class="btn bg-blue btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Profile"><i class="fa fa-eye"></i></a>
												@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_edit"))
													<a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getEditProvider', array($provider->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Provider"><i class="fa fa-pencil"></i></a>
												@endif
												@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_delete"))
													<a href="#" class="btn bg-red btn-delete-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Provider"><i class="fa fa-times"></i></a>
												@endif
											</td>
										</tr>
									<?php 
										}
									?>
		                        </tbody>
		                    </table>

		                    @if ($providers->count() == 0)
		                    	No providers found.
		                    @endif

		                    <div class="pull-left" style="padding-right: 10px; padding-top: 20px;">
		                    	Per page:
		                    </div>

		                    <div class="pull-left" style="padding-top: 15px;">
                            	<select id="page-size-dd" class="form-control">
                            		<option value="10" @if (\Input::get("page_size") == "10") selected @endif>10</option>
                            		<option value="25" @if (\Input::get("page_size") == "25") selected @endif>25</option>
                            		<option value="100" @if (\Input::get("page_size") == "100") selected @endif>100</option>
                            		<option value="All" @if (\Input::get("page_size") == "All") selected @endif>All</option>
                            	</select>
		                    </div>

		                    <div class="pull-right">
		                    	@if ($providers_paginator)
		                    		{!! $providers_paginator->render() !!}
		                    	@endif
		                    </div>
		                </div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
	        /* Delete a product */
	        $('#providers-table a.btn-delete-single').live('click', function (e) {
	            e.preventDefault();
	            if (confirm("Are you sure to delete this provider?") == false) {
	                return;
	            }

	            var ids = [$(this).parents("tr").find('.batch_id').val()];

	            $.post("{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@postDeleteProviders') }}", {
	        		ids: ids
	        	}, function() {
	        		window.location = window.location.href;
	        	});
	        });

	        $(".btn-delete").click(function(e) {
	        	e.preventDefault();
	        	var ids = [];

	        	var nRow = $(this).parents('tr')[0];

	        	$(".batch_id:checked").each(function() {
	        		ids.push($(this).val());
	        	});

	        	if (ids.length == 0) {
	        		alert('No providers were selected.');
	        		return;
	        	}

	        	if (confirm('Are you sure you want to delete the selected providers?')) {
		        	$.post("{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@postDeleteProviders') }}", {
		        		ids: ids
		        	}, function() {
		        		window.location = window.location.href;
		        	});
	        	}
	        });

	        var ct_select = $("#pos_code").select2({
				allowClear: true,
				placeholder: "Choose..."
			});

	        var ct_select = $("#license_title").select2({
				allowClear: true,
				placeholder: "Choose..."
			});

			$("#sort-by-dd, #sort-type-dd, #page-size-dd").change(function() {
				$('input[name="sort_by"]').val($("#sort-by-dd").val());
				$('input[name="sort_type"]').val($("#sort-type-dd").val());
				$('input[name="page_size"]').val($("#page-size-dd").val());
				$("#filters-wrapper form").submit();
			});
	    });

		function toggleFilters() {
			if ($('#filters-wrapper').is(":visible")) {
				$('#filters-wrapper').slideUp();
				$("#toggle-filters-btn i").removeClass("fa-arrow-up").addClass("fa-arrow-down");
			}
			else {
				$('#filters-wrapper').slideDown();
				$("#toggle-filters-btn i").removeClass("fa-arrow-down").addClass("fa-arrow-up");
			}
		}

		function downloadCSV() {
			$('input[name="export"]').val(1);
			$("#filters-wrapper form").submit();
		}
    </script>
@stop