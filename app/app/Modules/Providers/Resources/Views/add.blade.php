@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/providers">Providers</a></li>
        <li class="active">Register Provider</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Register Provider</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<!--
		<a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getIndex') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
		-->
	</div>

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@postSaveProvider')}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-3">
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Basic Info</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">Name <span class="text-danger">*</span></label>
					<div class="">
						<input type="text" class="form-control" maxlength="100" name="name" id="name" value="<?php echo Input::old('name'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">Type <span class="text-danger">*</span></label>
					<div class="">
						<select name="type" class="form-control" onchange="if (this.value == 'Person') { $('.for-person-only').show(); } else { $('.for-person-only').hide(); }">
						    <option value="Company" @if (old('type') == 'Company') selected @endif>Company</option>
						    <option value="Person" @if (old('type') == 'Person') selected @endif>Person</option>
						  </select>
					</div>
				</div>
				<div class="for-person-only form-group no-margin-hr panel-padding-h" @if (old('type') != 'Other') style="display: none;" @endif>
					<label class="control-label">Gender</label>
					<div class="">
						<select name="gender" class="form-control">
						    <option value="Male" @if (old('gender') == 'Male') selected @endif>Male</option>
						    <option value="Female" @if (old('gender') == 'Female') selected @endif>Female</option>
						  </select>
					</div>
				</div>
				<div class="for-person-only form-group no-margin-hr panel-padding-h" @if (old('type') != 'Other') style="display: none;" @endif>
					<label class="control-label">Birthday</label>
					<div class="">
						<input type="text" class="form-control date" maxlength="100" name="birthday" id="birthday" value="<?php echo Input::old('birthday'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">POS Code</label>
					<div class="">
						<select name="pos_code_id" class="form-control">
							@foreach (\DictionaryValue::where("category", "pos_codes")->get() as $pos_code)
						    	<option value="{{$pos_code->id}}" @if (old('pos_code_id') == $pos_code->id) selected @endif>{{$pos_code->name}}</option>
						    @endforeach
						</select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">Tax ID</label>
					<div class="">
						<input type="text" class="form-control" maxlength="100" name="tax_id" id="tax_id" value="<?php echo Input::old('tax_id'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">NPI</label>
					<div class="">
						<input type="text" class="form-control" maxlength="100" name="npi" id="npi" value="<?php echo Input::old('npi'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">Memo</label>
					<div class="">
						<textarea class="form-control" name="memo" id="memo"><?php echo Input::old('memo'); ?></textarea>
					</div>
				</div>
			</div>

			<div class="col-md-3 col-sm-offset-1">
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Specialities</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						<a href="#" id="specialities_editable" data-type="checklist" data-value="<?php echo Input::old('specialities'); ?>" data-title="Select Specialities"></a>
						<input type="hidden" name="specialities" id="specialities" value="<?php echo Input::old('specialities'); ?>" />
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Insurance Networks</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						<a href="#" id="insurance_networks_editable" data-type="checklist" data-value="<?php echo Input::old('insurance_networks'); ?>" data-title="Select Insurance Networks"></a>
						<input type="hidden" name="insurance_networks" id="insurance_networks" value="<?php echo Input::old('insurance_networks'); ?>" />
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Languages</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						<a href="#" id="languages_editable" data-type="checklist" data-value="<?php echo Input::old('languages'); ?>" data-title="Select Languages"></a>
						<input type="hidden" name="languages" id="languages" value="<?php echo Input::old('languages'); ?>" />
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Licenses</strong></h3>
					</label>
				</div>

				<div id="licenses"></div>

				<div class="form-group">
					<label class="control-label" style="text-align: left;">
						<a href="#" id="add-new-license" class="btn btn-success"><i class="fa fa-plus"></i> Add License</a>
					</label>
				</div>
			</div>

			<div class="col-md-4 col-sm-offset-1">
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Contacts</strong></h3>
					</label>
				</div>

				<div id="contacts"></div>

				<div class="form-group">
					<label class="control-label" style="text-align: left;">
						<a href="#" id="add-new-contact" class="btn btn-success"><i class="fa fa-plus"></i> Add Contact</a>
					</label>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Primary Office Address</strong></h3>
					</label>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h">
					<div class="">
						<a href="#" id="primary_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="primary_street" value="{{old('primary_street')}}" />
						<input type="hidden" class="city" name="primary_city" value="{{old('primary_city')}}" />
						<input type="hidden" class="country" name="primary_country" value="{{old('primary_country')}}" />
						<input type="hidden" class="state" name="primary_state" value="{{old('primary_state')}}" />
						<input type="hidden" class="zip" name="primary_zip" value="{{old('primary_zip')}}" />
					</div>

					<script type="text/javascript">
						$(document).ready(function() {
							$("#primary_address").editable({
								@if (old("primary_street"))
									value: {
										street: '{{old("primary_street")}}',
										city: '{{old("primary_city")}}',
										country: '{{old("primary_country")}}',
										state: '{{old("primary_state")}}',
										zip: '{{old("primary_zip")}}'
									},
								@endif
						        validate: function(value) {
						            if(value.street == '') return 'Street is required!'; 
						            if(value.city == '') return 'City is required!'; 
						            if(value.country == '') return 'Country is required!';
						            if(value.state == '') return 'State is required!'; 
						            if(value.zip == '') return 'Zip Code is required!'; 
						        },
						        placement: 'left',
	        					emptytext: 'Add Address'
						    });
						});
					</script>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Secondary Office Address</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						<label>
							<input type="checkbox" class="switch sw1" maxlength="100" name="secondary_address_available" id="secondary_address_available" @if (Input::old('secondary_address_available')) checked @endif />
						</label>
					</div>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h for-secondary" @if ((!\old("secondary_address_available"))) style="display: none;" @endif>
					<div class="">
						<a href="#" id="secondary_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="secondary_street" value="{{old('secondary_street')}}" />
						<input type="hidden" class="city" name="secondary_city" value="{{old('secondary_city')}}" />
						<input type="hidden" class="country" name="secondary_country" value="{{old('secondary_country')}}" />
						<input type="hidden" class="state" name="secondary_state" value="{{old('secondary_state')}}" />
						<input type="hidden" class="zip" name="secondary_zip" value="{{old('secondary_zip')}}" />
					</div>

					<script type="text/javascript">
						$(document).ready(function() {
							$("#secondary_address").editable({
								@if (old("secondary_street"))
									value: {
										street: '{{old("secondary_street")}}',
										city: '{{old("secondary_city")}}',
										country: '{{old("secondary_country")}}',
										state: '{{old("secondary_state")}}',
										zip: '{{old("secondary_zip")}}'
									},
								@endif
						        validate: function(value) {
						            if(value.street == '') return 'Street is required!'; 
						            if(value.city == '') return 'City is required!'; 
						            if(value.country == '') return 'Country is required!';
						            if(value.state == '') return 'State is required!'; 
						            if(value.zip == '') return 'Zip Code is required!'; 
						        },
						        placement: 'left',
	        					emptytext: 'Add Address'
						    });
						});
					</script>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Mailing Address</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="">&nbsp;</label>
					<div class="col-sm-8">
						Same as primary:
						<label>
							<input type="checkbox" class="switch sw2" maxlength="100" name="mailing_same_as_primary" id="mailing_same_as_primary" @if (Input::old('mailing_same_as_primary') || !old()) checked @endif />
						</label>
					</div>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h for-mailing" @if ((\old("mailing_same_as_primary")) || !old()) style="display: none;" @endif>
					<div class="">
						<a href="#" id="mailing_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="mailing_street" value="{{old('mailing_street')}}" />
						<input type="hidden" class="city" name="mailing_city" value="{{old('mailing_city')}}" />
						<input type="hidden" class="country" name="mailing_country" value="{{old('mailing_country')}}" />
						<input type="hidden" class="state" name="mailing_state" value="{{old('mailing_state')}}" />
						<input type="hidden" class="zip" name="mailing_zip" value="{{old('mailing_zip')}}" />
					</div>

					<script type="text/javascript">
						$(document).ready(function() {
							$("#mailing_address").editable({
								@if (old("mailing_street"))
									value: {
										street: '{{old("mailing_street")}}',
										city: '{{old("mailing_city")}}',
										country: '{{old("mailing_country")}}',
										state: '{{old("mailing_state")}}',
										zip: '{{old("mailing_zip")}}'
									},
								@endif
						        validate: function(value) {
						            if(value.street == '') return 'Street is required!'; 
						            if(value.city == '') return 'City is required!'; 
						            if(value.country == '') return 'Country is required!';
						            if(value.state == '') return 'State is required!'; 
						            if(value.zip == '') return 'Zip Code is required!'; 
						        },
						        placement: 'left'
						    });
						});
					</script>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#first_name, #last_name").keyup(function() {
				$("#initials").val($("#first_name").val().substr(0,1) + $("#last_name").val().substr(0,1));
			});
			$(".switch.sw1").bootstrapSwitch({
				onText: "ON",
				offText: "OFF"
			});
			$(".switch.sw2").bootstrapSwitch({
				onText: "YES",
				offText: "NO"
			});

			$('.switch.sw1').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (state.value)
			    	$(".for-secondary").show();
			    else
			    	$(".for-secondary").hide();
			});

			$('.switch.sw2').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (!state.value)
			    	$(".for-mailing").show();
			    else
			    	$(".for-mailing").hide();
			});

			$("#add-new-contact").click(function(e) {
				e.preventDefault();
				var key = $("#contacts>.contact-item").length + 1;
				$("#contacts").append('<div class="contact-item form-group no-margin-hr panel-padding-h">\
					<div class="">\
						<a href="#" id="contact_' + key + '" data-type="contact" data-pk="1" data-title="Contact Info" style="display: block;"></a>\
						<input type="hidden" class="title" name="contact[' + key + '][title]" />\
						<input type="hidden" class="first_name" name="contact[' + key + '][first_name]" />\
						<input type="hidden" class="last_name" name="contact[' + key + '][last_name]" />\
						<input type="hidden" class="email" name="contact[' + key + '][email]" />\
						<input type="hidden" class="phone" name="contact[' + key + '][phone]" />\
						<input type="hidden" class="cell_phone" name="contact[' + key + '][cell_phone]" />\
						<input type="hidden" class="fax" name="contact[' + key + '][fax]" />\
						<input type="hidden" class="additional_info" name="contact[' + key + '][additional_info]" />\
					</div>\
				</div>');

				$("#contact_" + key).editable({
			        validate: function(value) {
			            if(value.first_name == '') return 'First name is required!'; 
			        },
			        placement: 'left',
		        	emptytext: 'Add Contact Info'
			    });

				setTimeout(function() {
			    	$("#contact_" + key).click();
			    }, 100);
			});

			$("#add-new-license").click(function(e) {
				e.preventDefault();
				var key = $("#licenses>.license-item").length + 1;
				$("#licenses").append('<div class="license-item form-group no-margin-hr panel-padding-h">\
					<div class="">\
						<a href="#" id="license_' + key + '" data-type="license" data-pk="1" data-title="License Info" style="display: block;"></a>\
						<input type="hidden" class="number" name="license[' + key + '][number]" />\
						<input type="hidden" class="title" name="license[' + key + '][title]" />\
						<input type="hidden" class="expiration_date" name="license[' + key + '][expiration_date]" />\
					</div>\
				</div>');

				$("#license_" + key).editable({
			        placement: 'left',
		        	emptytext: 'Add License'
			    });

				setTimeout(function() {
			    	$("#license_" + key).click();
			    }, 100);
			});

			$("#primary_address, #secondary_address, #mailing_address").editable({
		        validate: function(value) {
		            if(value.street == '') return 'Street is required!'; 
		            if(value.city == '') return 'City is required!'; 
		            if(value.country == '') return 'Country is required!';
		            if(value.state == '') return 'State is required!'; 
		            if(value.zip == '') return 'Zip Code is required!'; 
		        },
		        placement: 'left',
		        emptytext: 'Add Address'
		    });

		    $("#specialities_editable").editable({
		        placement: 'left',
		        emptytext: 'No Specialities',
		        source: {!!json_encode(\DictionaryValue::getAll("specialities"))!!},
                success: function() {
                    setTimeout(function() {
                        $('input[name="specialities"]').val($('#specialities_editable').html().replace(/<br>/g, ", "));
                    }, 100);
                }
		    });

		    $("#insurance_networks_editable").editable({
		        placement: 'left',
		        emptytext: 'No Insurance Networks',
		        source: {!!json_encode(\DictionaryValue::getAll("insurance_networks"))!!},
                success: function() {
                    setTimeout(function() {
                        $('input[name="insurance_networks"]').val($('#insurance_networks_editable').html().replace(/<br>/g, ", "));
                    }, 100);
                }
		    });

		    $("#languages_editable").editable({
		        placement: 'left',
		        emptytext: 'No languages',
		        source: {!!json_encode(\DictionaryValue::getAll("languages"))!!},
                success: function() {
                    setTimeout(function() {
                        $('input[name="languages"]').val($('#languages_editable').html().replace(/<br>/g, ", "));
                    }, 100);
                }
		    });
		});

		var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};

		var contact_titles = ['Administrator, Benefits/Risk', 'Administrator, HRS-Benefits services', 'Benefits Administrator', 'Benefits Contact Person', 'Benefits Coordinator', 'Benefits Manager', 'Benefits Specialist', 'CEO & President', 'Chief Operating Officer', 'Director, Compensation & Benefits', 'Director, Human resources', 'Director of Administrator Services', 'Director of Benefits', 'Director of Social Services', 'Director of Workers Compensation & Employee Health', 'Division of Labor-Management Relations', 'Employees Relations', 'Executive Director', 'Financial Director', 'Financial Officer', 'Group Business Manager', 'Human Resources Manager', 'Lead Manager, HR Department', 'Office Manager', 'President', 'Senior President', 'Training Director', 'Vice President', 'Vice President & Chief Financial Officer', 'Vice President of Human Resources', 'Vice President of Life Operations'];
	</script>
@stop