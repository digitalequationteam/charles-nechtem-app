@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/providers">Providers</a></li>
        <li><a href="/providers/edit-provider/{{$provider->id}}">{{$provider->name}}</a></li>
        <li class="active">Edit</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Edit Provider</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', $provider->id) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>
	-->

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@postSaveProvider', $provider->id)}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-3">
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Basic Info</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">Name <span class="text-danger">*</span></label>
					<div class="">
						<input type="text" class="form-control" maxlength="100" name="name" id="name" value="<?php echo (old()) ? Input::old('name') : $provider->name; ?>" />
					</div>
				</div>
				@if ($provider->type == "Person")
					<div class="for-person-only form-group no-margin-hr panel-padding-h">
						<label class="control-label">Gender</label>
						<div class="">
							<select name="gender" class="form-control">
							    <option value="Male" @if (old('gender') == 'Male') selected @endif>Male</option>
							    <option value="Female" @if (old('gender') == 'Female') selected @endif>Female</option>
							  </select>
						</div>
					</div>
					<div class="for-person-only form-group no-margin-hr panel-padding-h">
						<label class="control-label">Birthday</label>
						<div class="">
							<input type="text" class="form-control date" maxlength="100" name="birthday" id="birthday" value="" />

							<script type="text/javascript">
								$(document).ready(function() {
									setTimeout(function() {
										$("#birthday").val('<?php echo (old()) ? Input::old('birthday') : date("m/d/Y", strtotime($provider->birthday)); ?>');
									}, 1000);
								});
							</script>
						</div>
					</div>
				@endif
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">POS Code</label>
					<div class="">
						<select name="pos_code_id" class="form-control">
							@foreach (\DictionaryValue::where("category", "pos_codes")->get() as $pos_code)
						    	<option value="{{$pos_code->id}}" @if (old('pos_code_id') == $pos_code->id) selected @endif>{{$pos_code->name}}</option>
						    @endforeach
						</select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">Tax ID</label>
					<div class="">
						<input type="text" class="form-control" maxlength="100" name="tax_id" id="tax_id" value="<?php echo (old()) ? Input::old('tax_id') : $provider->tax_id; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">NPI</label>
					<div class="">
						<input type="text" class="form-control" maxlength="100" name="npi" id="npi" value="<?php echo (old()) ? Input::old('npi') : $provider->npi; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label">Memo</label>
					<div class="">
						<textarea class="form-control" name="memo" id="memo"><?php echo (old()) ? Input::old('memo') : $provider->memo; ?></textarea>
					</div>
				</div>
			</div>

			<div class="col-md-3 col-sm-offset-1">
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Specialities</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						<a href="#" id="specialities_editable" data-type="checklist" data-value="<?php echo (old()) ? Input::old('specialities') : $provider->specialities; ?>" data-title="Select Specialities"></a>
						<input type="hidden" name="specialities" id="specialities" value="<?php echo (old()) ? Input::old('specialities') : $provider->specialities; ?>" />
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Insurance Networks</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						<a href="#" id="insurance_networks_editable" data-type="checklist" data-value="<?php echo (old()) ? Input::old('insurance_networks') : $provider->insurance_networks; ?>" data-title="Select Insurance Networks"></a>
						<input type="hidden" name="insurance_networks" id="insurance_networks" value="<?php echo (old()) ? Input::old('insurance_networks') : $provider->insurance_networks; ?>" />
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Languages</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						<a href="#" id="languages_editable" data-type="checklist" data-value="<?php echo (old()) ? Input::old('languages') : $provider->languages; ?>" data-title="Select Languages"></a>
						<input type="hidden" name="languages" id="languages" value="<?php echo (old()) ? Input::old('languages') : $provider->languages; ?>" />
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Licenses</strong></h3>
					</label>
				</div>

				<div id="licenses">
					<?php $licenses = (old()) ? (array)old("license") : $licenses; ?>

					@foreach ($licenses as $key => $license)
						<div class="license-item form-group no-margin-hr panel-padding-h">
							<div class="">
								<a href="#" id="license_{{@$key}}" data-type="license" data-pk="1" data-title="License Info" style="display: block;"></a>
								@if (@$license["id"])
									<input type="hidden" class="id" name="license[{{@$key}}][id]" value="{{$license['id']}}" />
								@endif
								<input type="hidden" class="number" name="license[{{@$key}}][number]" value="{{$license['number']}}" />
								<input type="hidden" class="title" name="license[{{@$key}}][title]" value="{{$license['title']}}" />
								<input type="hidden" class="expiration_date" name="license[{{@$key}}][expiration_date]" value="{{$license['expiration_date']}}" />
							</div>
						</div>
						<script type="text/javascript">
							$(document).ready(function() {
								$("#license_{{$key}}").editable({
									value: {
										number: $("#license_{{$key}}").parents(".license-item").find('input.number').val(),
										title: $("#license_{{$key}}").parents(".license-item").find('input.title').val(),
										expiration_date: $("#license_{{$key}}").parents(".license-item").find('input.expiration_date').val(),
									},
							        placement: 'left',
		        					emptytext: 'Add License'
							    });
							});
						</script>
					@endforeach
				</div>

				<div class="form-group">
					<label class="control-label" style="text-align: left;">
						<a href="#" id="add-new-license" class="btn btn-success"><i class="fa fa-plus"></i> Add License</a>
					</label>
				</div>
			</div>

			<div class="col-md-4 col-sm-offset-1">
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Contacts</strong></h3>
					</label>
				</div>

				<div id="contacts">
					<?php $contacts = (old()) ? (array)old("contact") : $contacts; ?>

					@foreach ($contacts as $key => $contact)
						<div class="contact-item form-group no-margin-hr panel-padding-h">
							<div class="">
								<a href="#" id="contact_{{@$key}}" data-type="contact" data-pk="1" data-title="Contact Info" style="display: block;"></a>
								@if (@$contact["id"])
									<input type="hidden" class="id" name="contact[{{@$key}}][id]" value="{{$contact['id']}}" />
								@endif
								<input type="hidden" class="title" name="contact[{{@$key}}][title]" value="{{$contact['title']}}" />
								<input type="hidden" class="first_name" name="contact[{{@$key}}][first_name]" value="{{$contact['first_name']}}" />
								<input type="hidden" class="last_name" name="contact[{{@$key}}][last_name]" value="{{$contact['last_name']}}" />
								<input type="hidden" class="email" name="contact[{{@$key}}][email]" value="{{$contact['email']}}" />
								<input type="hidden" class="phone" name="contact[{{@$key}}][phone]" value="{{$contact['phone']}}" />
								<input type="hidden" class="cell_phone" name="contact[{{@$key}}][cell_phone]" value="{{$contact['cell_phone']}}" />
								<input type="hidden" class="fax" name="contact[{{@$key}}][fax]" value="{{$contact['fax']}}" />
								<input type="hidden" class="additional_info" name="contact[{{@$key}}][additional_info]" value="{{$contact['additional_info']}}" />
							</div>
						</div>
						<script type="text/javascript">
							$(document).ready(function() {
								$("#contact_{{$key}}").editable({
									value: {
										title: $("#contact_{{$key}}").parents(".contact-item").find('input.title').val(),
										first_name: $("#contact_{{$key}}").parents(".contact-item").find('input.first_name').val(),
										last_name: $("#contact_{{$key}}").parents(".contact-item").find('input.last_name').val(),
										email: $("#contact_{{$key}}").parents(".contact-item").find('input.email').val(),
										phone: $("#contact_{{$key}}").parents(".contact-item").find('input.phone').val(),
										cell_phone: $("#contact_{{$key}}").parents(".contact-item").find('input.cell_phone').val(),
										fax: $("#contact_{{$key}}").parents(".contact-item").find('input.fax').val(),
										additional_info: $("#contact_{{$key}}").parents(".contact-item").find('input.additional_info').val(),
									},
							        validate: function(value) {
							            if(value.first_name == '') return 'First name is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Contact Info'
							    });
							});
						</script>
					@endforeach
				</div>

				<div class="form-group">
					<label class="control-label" style="text-align: left;">
						<a href="#" id="add-new-contact" class="btn btn-success"><i class="fa fa-plus"></i> Add Contact</a>
					</label>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Primary Office Address</strong></h3>
					</label>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h">
					<div class="">
						<a href="#" id="primary_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="primary_street" value="{{(old()) ? old('primary_street') : $provider->primary_street}}" />
						<input type="hidden" class="city" name="primary_city" value="{{(old()) ? old('primary_city') : $provider->primary_city}}" />
						<input type="hidden" class="country" name="primary_country" value="{{(old()) ? old('primary_country') : $provider->primary_country}}" />
						<input type="hidden" class="state" name="primary_state" value="{{(old()) ? old('primary_state') : $provider->primary_state}}" />
						<input type="hidden" class="zip" name="primary_zip" value="{{(old()) ? old('primary_zip') : $provider->primary_zip}}" />
					</div>

					@if (old())
						<script type="text/javascript">
							$(document).ready(function() {
								$("#primary_address").editable({
									@if (old("primary_street"))
										value: {
											street: '{{old("primary_street")}}',
											city: '{{old("primary_city")}}',
											country: '{{old("primary_country")}}',
											state: '{{old("primary_state")}}',
											zip: '{{old("primary_zip")}}'
										},
									@endif
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@else
						<script type="text/javascript">
							$(document).ready(function() {
								$("#primary_address").editable({
									value: {
										street: '{{$provider->primary_street}}',
										city: '{{$provider->primary_city}}',
										country: '{{$provider->primary_country}}',
										state: '{{$provider->primary_state}}',
										zip: '{{$provider->primary_zip}}'
									},
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@endif
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Secondary Office Address</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						<label>
							<input type="checkbox" class="switch sw1" maxlength="100" name="secondary_address_available" id="secondary_address_available" @if ((old() && Input::old('secondary_address_available')) || (!old() && $provider->secondary_address_available)) checked @endif />
						</label>
					</div>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h for-secondary" @if ((old() && !\old("secondary_address_available")) || (!old() && !$provider->secondary_address_available)) style="display: none;" @endif>
					<div class="">
						<a href="#" id="secondary_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="secondary_street" value="{{(old()) ? old('secondary_street') : $provider->secondary_street}}" />
						<input type="hidden" class="city" name="secondary_city" value="{{(old()) ? old('secondary_city') : $provider->secondary_city}}" />
						<input type="hidden" class="country" name="secondary_country" value="{{(old()) ? old('secondary_country') : $provider->secondary_country}}" />
						<input type="hidden" class="state" name="secondary_state" value="{{(old()) ? old('secondary_state') : $provider->secondary_state}}" />
						<input type="hidden" class="zip" name="secondary_zip" value="{{(old()) ? old('secondary_zip') : $provider->secondary_zip}}" />
					</div>

					@if (old())
						<script type="text/javascript">
							$(document).ready(function() {
								$("#secondary_address").editable({
									@if (old("secondary_street"))
										value: {
											street: '{{old("secondary_street")}}',
											city: '{{old("secondary_city")}}',
											country: '{{old("secondary_country")}}',
											state: '{{old("secondary_state")}}',
											zip: '{{old("secondary_zip")}}'
										},
									@endif
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@else
						<script type="text/javascript">
							$(document).ready(function() {
								$("#secondary_address").editable({
									value: {
										street: '{{$provider->secondary_street}}',
										city: '{{$provider->secondary_city}}',
										country: '{{$provider->secondary_country}}',
										state: '{{$provider->secondary_state}}',
										zip: '{{$provider->secondary_zip}}'
									},
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@endif
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Mailing Address</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<div class="">
						Same as primary:
						<label>
							<input type="checkbox" class="switch sw2" maxlength="100" name="mailing_same_as_primary" id="mailing_same_as_primary" @if ((old() && Input::old('mailing_same_as_primary') || (!old() && $provider->mailing_same_as_primary))) checked @endif />
						</label>
					</div>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h for-mailing" @if ((old() && \old("mailing_same_as_primary")) || (!old() && $provider->mailing_same_as_primary)) style="display: none;" @endif>
					<div class="">
						<a href="#" id="mailing_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="mailing_street" value="{{(old()) ? old('mailing_street') : $provider->mailing_street}}" />
						<input type="hidden" class="city" name="mailing_city" value="{{(old()) ? old('mailing_city') : $provider->mailing_city}}" />
						<input type="hidden" class="country" name="mailing_country" value="{{(old()) ? old('mailing_country') : $provider->mailing_country}}" />
						<input type="hidden" class="state" name="mailing_state" value="{{(old()) ? old('mailing_state') : $provider->mailing_state}}" />
						<input type="hidden" class="zip" name="mailing_zip" value="{{(old()) ? old('mailing_zip') : $provider->mailing_zip}}" />
					</div>

					@if (old())
						<script type="text/javascript">
							$(document).ready(function() {
								$("#mailing_address").editable({
									@if (old("mailing_street"))
										value: {
											street: '{{old("mailing_street")}}',
											city: '{{old("mailing_city")}}',
											country: '{{old("mailing_country")}}',
											state: '{{old("mailing_state")}}',
											zip: '{{old("mailing_zip")}}'
										},
									@endif
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left'
							    });
							});
						</script>
					@else
						<script type="text/javascript">
							$(document).ready(function() {
								$("#mailing_address").editable({
									value: {
										street: '{{$provider->mailing_street}}',
										city: '{{$provider->mailing_city}}',
										country: '{{$provider->mailing_country}}',
										state: '{{$provider->mailing_state}}',
										zip: '{{$provider->mailing_zip}}'
									},
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@endif
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#first_name, #last_name").keyup(function() {
				$("#initials").val($("#first_name").val().substr(0,1) + $("#last_name").val().substr(0,1));
			});
			$(".switch.sw1").bootstrapSwitch({
				onText: "ON",
				offText: "OFF"
			});
			$(".switch.sw2").bootstrapSwitch({
				onText: "YES",
				offText: "NO"
			});

			$('.switch.sw1').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (state.value)
			    	$(".for-secondary").show();
			    else
			    	$(".for-secondary").hide();
			});

			$('.switch.sw2').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (!state.value)
			    	$(".for-mailing").show();
			    else
			    	$(".for-mailing").hide();
			});

			$("#add-new-contact").click(function(e) {
				e.preventDefault();
				var key = $("#contacts>.contact-item").length + 1;
				$("#contacts").append('<div class="contact-item form-group no-margin-hr panel-padding-h">\
					<div class="">\
						<a href="#" id="contact_' + key + '" data-type="contact" data-pk="1" data-title="Contact Info" style="display: block;"></a>\
						<input type="hidden" class="title" name="contact[' + key + '][title]" />\
						<input type="hidden" class="first_name" name="contact[' + key + '][first_name]" />\
						<input type="hidden" class="last_name" name="contact[' + key + '][last_name]" />\
						<input type="hidden" class="email" name="contact[' + key + '][email]" />\
						<input type="hidden" class="phone" name="contact[' + key + '][phone]" />\
						<input type="hidden" class="cell_phone" name="contact[' + key + '][cell_phone]" />\
						<input type="hidden" class="fax" name="contact[' + key + '][fax]" />\
						<input type="hidden" class="additional_info" name="contact[' + key + '][additional_info]" />\
					</div>\
				</div>');

				$("#contact_" + key).editable({
			        validate: function(value) {
			            if(value.first_name == '') return 'First name is required!'; 
			        },
			        placement: 'left',
		        	emptytext: 'Add Contact Info'
			    });

				setTimeout(function() {
			    	$("#contact_" + key).click();
			    }, 100);
			});

			$("#add-new-license").click(function(e) {
				e.preventDefault();
				var key = $("#licenses>.license-item").length + 1;
				$("#licenses").append('<div class="license-item form-group no-margin-hr panel-padding-h">\
					<div class="">\
						<a href="#" id="license_' + key + '" data-type="license" data-pk="1" data-title="License Info" style="display: block;"></a>\
						<input type="hidden" class="number" name="license[' + key + '][number]" />\
						<input type="hidden" class="title" name="license[' + key + '][title]" />\
						<input type="hidden" class="expiration_date" name="license[' + key + '][expiration_date]" />\
					</div>\
				</div>');

				$("#license_" + key).editable({
			        placement: 'left',
		        	emptytext: 'Add License'
			    });

				setTimeout(function() {
			    	$("#license_" + key).click();
			    }, 100);
			});

			$("#primary_address, #secondary_address, #mailing_address").editable({
		        validate: function(value) {
		            if(value.street == '') return 'Street is required!'; 
		            if(value.city == '') return 'City is required!'; 
		            if(value.country == '') return 'Country is required!';
		            if(value.state == '') return 'State is required!'; 
		            if(value.zip == '') return 'Zip Code is required!'; 
		        },
		        placement: 'left',
		        emptytext: 'Add Address'
		    });

		    $("#specialities_editable").editable({
		        placement: 'left',
		        emptytext: 'No Specialities',
		        source: {!!json_encode(\DictionaryValue::getAll("specialities"))!!},
                success: function() {
                    setTimeout(function() {
                        $('input[name="specialities"]').val($('#specialities_editable').html().replace(/<br>/g, ", "));
                    }, 100);
                }
		    });

		    $("#insurance_networks_editable").editable({
		        placement: 'left',
		        emptytext: 'No Insurance Networks',
		        source: {!!json_encode(\DictionaryValue::getAll("insurance_networks"))!!},
                success: function() {
                    setTimeout(function() {
                        $('input[name="insurance_networks"]').val($('#insurance_networks_editable').html().replace(/<br>/g, ", "));
                    }, 100);
                }
		    });

		    $("#languages_editable").editable({
		        placement: 'left',
		        emptytext: 'No languages',
		        source: {!!json_encode(\DictionaryValue::getAll("languages"))!!},
                success: function() {
                    setTimeout(function() {
                        $('input[name="languages"]').val($('#languages_editable').html().replace(/<br>/g, ", "));
                    }, 100);
                }
		    });
		});

		var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};

		var contact_titles = ['Administrator, Benefits/Risk', 'Administrator, HRS-Benefits services', 'Benefits Administrator', 'Benefits Contact Person', 'Benefits Coordinator', 'Benefits Manager', 'Benefits Specialist', 'CEO & President', 'Chief Operating Officer', 'Director, Compensation & Benefits', 'Director, Human resources', 'Director of Administrator Services', 'Director of Benefits', 'Director of Social Services', 'Director of Workers Compensation & Employee Health', 'Division of Labor-Management Relations', 'Employees Relations', 'Executive Director', 'Financial Director', 'Financial Officer', 'Group Business Manager', 'Human Resources Manager', 'Lead Manager, HR Department', 'Office Manager', 'President', 'Senior President', 'Training Director', 'Vice President', 'Vice President & Chief Financial Officer', 'Vice President of Human Resources', 'Vice President of Life Operations'];
	</script>
@stop