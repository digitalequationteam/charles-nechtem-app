@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/providers">Providers</a></li>
        <li class="active">{{$provider->name}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>{{$provider->name}} - Profile</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
        <!--
		<a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getIndex') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
        -->
        @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_edit"))
            <a class="btn btn-success m-t-10" href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getEditProvider', array($provider->id)) }}"><i class="fa fa-pencil"></i> Edit Provider</a>
        @endif
	</div>

	<div class="clearfix"></div>

	<div class="tabcordion">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
            @if ($provider->type == "Company")
                @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_staff"))
                    <li class=""><a href="#staff" data-toggle="tab">Staff</a></li>
                @endif
            @endif
            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_costs"))
                <li class=""><a href="#costs" data-toggle="tab">Costs</a></li>
            @endif
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="profile">
                <div class="row p-20" style="border-bottom: solid 1px #ececec;">
                    <div class="col-md-6">
                        <h3 class="m-t-0">Basic Information</h3>
                        <form class="form-horizontal p-20">
                            <div class="form-group">
                                <div class="col-sm-4">Name:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$provider->name}}</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">POS Code:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{@$provider->pos_code()->get()->first()->code}} - {{@$provider->pos_code()->get()->first()->name}}</strong>
                                </div>
                            </div>
                            @if ($provider->type == "Person")
                                <div class="form-group">
                                    <div class="col-sm-4">Gender:
                                    </div>
                                    <div class="col-sm-8">
                                        <strong>{{$provider->gender}}</strong>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">Birthday:
                                    </div>
                                    <div class="col-sm-8">
                                        <strong>{{date("m/d/Y", strtotime($provider->birthday))}}</strong>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-sm-4">Tax ID:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$provider->tax_id}}</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">Memo:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$provider->memo}}</strong>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h3 class="m-t-0">Address Information</h3>
                        <form class="form-horizontal p-20">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    Primary Office Address:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{!! $provider->getPrimaryAddress(2) !!}</strong>
                                </div>
                            </div>
                            @if ($provider->secondary_address_available)
                            <div class="form-group">
                                <div class="col-sm-4">
                                    Secondary Office Address:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{!! $provider->getSecondaryAddress(2) !!}</strong>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="col-sm-4">
                                    Mailing Address:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{!! $provider->getMailingAddress(2) !!}</strong>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row p-20" style="border-bottom: solid 1px #ececec;">
                    <div class="col-md-6">
                        <h3 class="m-t-0">Contact Information</h3>
                        @foreach ($provider->contacts()->get() as $contact)
                            {!! $contact->getDisplay() !!}
                            <br /><br />
                        @endforeach

                        @if ($provider->contacts()->count() == 0)
                            There are no contacts.
                        @endif
                    </div>
                    <div class="col-md-6">
                        <h3 class="m-t-0">License Information</h3>
                        @foreach ($provider->licenses()->get() as $license)
                            {!! $license->getDisplay() !!}
                            <br /><br />
                        @endforeach

                        @if ($provider->licenses()->count() == 0)
                            There are no licenses.
                        @endif
                    </div>
                </div>

                <div class="row p-20" style="border-bottom: solid 1px #ececec;">
                    <div class="col-md-4">
                        <h3 class="m-t-0">Specialities</h3>
                        {{$provider->specialities}}
                    </div>
                    <div class="col-md-4">
                        <h3 class="m-t-0">Insurance Networks</h3>
                        {{$provider->insurance_networks}}
                    </div>
                    <div class="col-md-4">
                        <h3 class="m-t-0">Languages</h3>
                        {{$provider->languages}}
                    </div>
                </div>

                <br />
            </div>
            @if ($provider->type == "Company")
                @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_staff"))
                    <div class="tab-pane fade users" id="staff">
                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <div class="filter-checkbox">
                                <a href="#" class="btn btn-danger btn-delete-staff">Delete</a>
                                <a href="{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@getAddStaff', array($provider->id))}}" class="btn btn-primary">Register Staff</a>
                            </div>
                            <table id="staff-table" class="table table-tools table-striped">
                                <thead>
                                    <tr>
                                        <th style="min-width:50px">
                                            <input type="checkbox" class="check_all"/>
                                        </th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($provider->staff()->get() as $cost) { ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <div class="pull-left" style="padding-top: 10px;">
                                                    <input type="checkbox" class="batch_id" value="{{$cost->id}}" />
                                                </div>
                                                <div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
                                                    {{$cost->first_name}} {{$cost->last_name}}
                                                </div>
                                            </td>
                                            <td style="text-align: right;">
                                                <a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getEditStaff', array($cost->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Staff"><i class="fa fa-pencil"></i></a>
                                                <a href="#" class="btn bg-red btn-delete-staff-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Staff"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
                                    <?php 
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>

                        <script type="text/javascript">
                            $(document).ready(function() {
                                var opt = {};
                                // Tools: export to Excel, CSV, PDF & Print
                                opt.sDom = "<'row m-t-10'<'col-md-6'><'col-md-6'Tf>r>t<'row'<'col-md-6'><'col-md-6 align-right'p>>",
                                opt.oLanguage = { "sSearch": "","sZeroRecords": "No staff found" } ,
                                opt.iDisplayLength = 15,
                                opt.oTableTools = {
                                    /*"sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",*/
                                    "aButtons": []
                                };
                                opt.bSort = false;

                                var oTable = $('#staff-table').dataTable(opt);
                                oTable.fnDraw();

                                $("#staff-table tr th:eq(0)").removeClass("sorting_asc").removeClass("sorting_desc");

                                /* Add a placeholder to searh input */
                                $('.dataTables_filter input').attr("placeholder", "Search...");

                                /* Delete a product */
                                $('#staff-table a.btn-delete-staff-single').live('click', function (e) {
                                    e.preventDefault();
                                    if (confirm("Are you sure you want to delete this staff member?") == false) {
                                        return;
                                    }
                                    var nRow = $(this).parents('tr')[0];
                                    oTable.fnDeleteRow(nRow);

                                    var ids = [$(this).parents("tr").find('.batch_id').val()];

                                    $.post("{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@postDeleteStaffs') }}", {
                                        ids: ids
                                    }, function() {
                                        for (var i in nRows)
                                            oTable.fnDeleteRow(nRows[i]);
                                    });
                                });

                                $(".btn-delete-staff").click(function(e) {
                                    e.preventDefault();
                                    var ids = [];
                                    var nRows = [];

                                    var nRow = $(this).parents('tr')[0];

                                    $(".batch_id:checked").each(function() {
                                        ids.push($(this).val());
                                        nRows.push($(this).parents('tr')[0]);
                                    });

                                    if (ids.length == 0) {
                                        alert('No staff were selected.');
                                        return;
                                    }

                                    if (confirm('Are you sure you want to delete the selected staff?')) {
                                        $.post("{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@postDeleteStaffs') }}", {
                                            ids: ids
                                        }, function() {
                                            for (var i in nRows)
                                                oTable.fnDeleteRow(nRows[i]);
                                        });
                                    }
                                });
                            });
                        </script>
                    </div>
                @endif
            @endif

            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_providers_costs"))
                <div class="tab-pane fade users" id="costs">
                    <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                        <div class="filter-checkbox">
                            <a href="#" class="btn btn-danger btn-delete-cost">Delete</a>
                            <a href="{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@getAddCost', array($provider->id, 'EAP'))}}" class="btn btn-primary">Create EAP Rate</a>
                            <a href="{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@getAddCost', array($provider->id, 'OMC'))}}" class="btn btn-primary">Create OMC Rate</a>
                        </div>
                        <table id="costs-table" class="table table-tools table-striped">
                            <thead>
                                <tr>
                                    <th style="min-width:50px">
                                        <input type="checkbox" class="check_all"/>
                                    </th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($provider->costs()->get() as $cost) { ?>
                                    <tr class="odd gradeX">
                                        <td>
                                            <div class="pull-left" style="padding-top: 10px;">
                                                <input type="checkbox" class="batch_id" value="{{$cost->id}}" />
                                            </div>
                                            <div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
                                                <strong>{{$cost->name}}</strong>
                                                <br />
                                                Type: {{$cost->type}}<br />
                                                @if ($cost->type == "EAP")
                                                    EAP Service: 
                                                @else
                                                    CPT Code: 
                                                @endif
                                                {{$cost->getDictionaryValue()}}<br />
                                                CNA Representative: {{$cost->getRepresentative()}}<br />
                                                Cost: ${{$cost->cost}}<br />
                                                Signing Date: {{$cost->signing_date}}
                                            </div>
                                        </td>
                                        <td style="text-align: right;">
                                            <a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getEditCost', array($cost->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Rate"><i class="fa fa-pencil"></i></a>
                                            <a href="#" class="btn bg-red btn-delete-cost-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Rate"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            var opt = {};
                            // Tools: export to Excel, CSV, PDF & Print
                            opt.sDom = "<'row m-t-10'<'col-md-6'><'col-md-6'Tf>r>t<'row'<'col-md-6'><'col-md-6 align-right'p>>",
                            opt.oLanguage = { "sSearch": "","sZeroRecords": "No rates found" } ,
                            opt.iDisplayLength = 15,
                            opt.oTableTools = {
                                /*"sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",*/
                                "aButtons": []
                            };
                            opt.bSort = false;

                            var oTable = $('#costs-table').dataTable(opt);
                            oTable.fnDraw();

                            $("#costs-table tr th:eq(0)").removeClass("sorting_asc").removeClass("sorting_desc");

                            /* Add a placeholder to searh input */
                            $('.dataTables_filter input').attr("placeholder", "Search...");

                            /* Delete a product */
                            $('#costs-table a.btn-delete-cost-single').live('click', function (e) {
                                e.preventDefault();
                                if (confirm("Are you sure you want to delete this rate?") == false) {
                                    return;
                                }
                                var nRow = $(this).parents('tr')[0];
                                oTable.fnDeleteRow(nRow);

                                var ids = [$(this).parents("tr").find('.batch_id').val()];

                                $.post("{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@postDeleteCosts') }}", {
                                    ids: ids
                                }, function() {
                                    for (var i in nRows)
                                        oTable.fnDeleteRow(nRows[i]);
                                });
                            });

                            $(".btn-delete-cost").click(function(e) {
                                e.preventDefault();
                                var ids = [];
                                var nRows = [];

                                var nRow = $(this).parents('tr')[0];

                                $(".batch_id:checked").each(function() {
                                    ids.push($(this).val());
                                    nRows.push($(this).parents('tr')[0]);
                                });

                                if (ids.length == 0) {
                                    alert('No costs were selected.');
                                    return;
                                }

                                if (confirm('Are you sure you want to delete the selected rates?')) {
                                    $.post("{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@postDeleteCosts') }}", {
                                        ids: ids
                                    }, function() {
                                        for (var i in nRows)
                                            oTable.fnDeleteRow(nRows[i]);
                                    });
                                }
                            });
                        });
                    </script>
                </div>
            @endif
        </div>
    </div>

    <script type="text/javascript">
    	$(document).ready(function() {
    		var exploded = window.location.href.split("#");
    		if (exploded.length > 1)
    			$('a[href="#' + exploded[1] + '"]').click();
    	});
    </script>
@stop