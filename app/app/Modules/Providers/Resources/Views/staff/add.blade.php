@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/providers">Providers</a></li>
        <li><a href="/providers/provider-profile/{{$provider->id}}">{{$provider->name}}</a></li>
        <li><a href="/providers/provider-profile/{{$provider->id}}#staff">Staff</a></li>
        <li class="active">Register</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Register Staff Member</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<!--<a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', array($provider->id, '#staff')) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>-->
	</div>

	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@postSaveStaff', $provider->id)}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-12">
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">First Name <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" maxlength="100" name="first_name" id="first_name" value="{{Input::old('first_name')}}" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Last Name <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" maxlength="100" name="last_name" id="last_name" value="{{Input::old('last_name')}}" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Memo</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="memo" id="memo">{{Input::old('memo')}}</textarea>
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>
@stop