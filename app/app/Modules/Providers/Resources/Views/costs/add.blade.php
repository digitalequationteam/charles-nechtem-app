@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/providers">Providers</a></li>
        <li><a href="/providers/provider-profile/{{$provider->id}}">{{$provider->name}}</a></li>
        <li><a href="/providers/provider-profile/{{$provider->id}}#costs">Rates</a></li>
        <li class="active">Add Rate</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Create Provider {{$type}} Rate</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<!--<a href="{{ action('\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', array($provider->id, '#costs')) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>-->
	</div>

	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@postSaveCost', array($provider->id))}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<input type="hidden" name="type" value="{{{ $type }}}" />

		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-12">
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Name <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" maxlength="100" name="name" id="name" value="<?php echo Input::old('name'); ?>" />
					</div>
				</div>

				@if ($type == "EAP")
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">EAP Service</label>
						<div class="col-sm-4">
							<select name="dictionary_value_id" class="form-control">
								@foreach (\DictionaryValue::where("category", "eap_services")->orderBy("name", "ASC")->get() as $eap_service)
							    	<option value="{{$eap_service->id}}" @if (old('dictionary_value_id') == $eap_service->id) selected @endif>{{$eap_service->name}}</option>
							    @endforeach
							</select>
						</div>
					</div>
				@elseif ($type == "OMC")
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-4">CPT Code</label>
						<div class="col-sm-4">
							<select name="dictionary_value_id" class="form-control">
								@foreach (\DictionaryValue::where("category", "cpt_codes")->orderBy("name", "ASC")->get() as $cpt_code)
							    	<option value="{{$cpt_code->id}}" @if (old('dictionary_value_id') == $cpt_code->id) selected @endif>{{$cpt_code->code}} - {{$cpt_code->name}}</option>
							    @endforeach
							</select>
						</div>
					</div>
				@endif

				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Cost <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<div class="input-group transparent">
                            <span class="input-group-addon bg-grey">
                              <i class="fa fa-dollar"></i>
                            </span>
                            <input type="number" class="form-control" maxlength="100" name="cost" id="cost" value="<?php echo Input::old('cost'); ?>" />
                        </div>
					</div>
				</div>

				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Representative</label>
					<div class="col-sm-4">
						<textarea class="form-control" name="representative" id="representative">{{Input::old('representative')}}</textarea>
					</div>
				</div>

				<div class="for-person-only form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Signing Date</label>
					<div class="col-sm-4">
						<input type="text" class="form-control date" maxlength="100" name="signing_date" id="signing_date" value="<?php echo Input::old('signing_date'); ?>" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>
@stop