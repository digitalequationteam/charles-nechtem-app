@if (count($providers) == 0)
	No Provider found.
@endif

@foreach ($providers as $provider)
	<div class="row" style="border-bottom: solid 1px #CCCCCC; padding: 5px;">
		<div class="pull-right">
			<a href="#" data-id="{{$provider->id}}" class="btn btn-success tooltips btn-xs select-employee-btn" data-original-title="Use {{$provider->first_name}} {{$provider->last_name}}"><i class="fa fa-arrow-right"></i></a>
		</div>
		<strong>{{$provider->name}}</strong>
	</div>
@endforeach