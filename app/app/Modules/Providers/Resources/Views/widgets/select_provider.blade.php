<div class="row text-center">
	<h3 class="panel-title" style="width: auto;"><strong>Provider</strong></h3>
	<br />
	<a href="#select-provider-modal" data-toggle="modal" class="btn btn-success"><i class="fa fa-plus"></i> Select Provider</a>

	<div class="form-group">
		<br />
		<div class="col-sm-offset-1 col-sm-10" id="provider-data">
			@if (@$provider_data)
				{{$provider_data["name"]}} ({{$provider_data["address"]}})
			@endif
		</div>
	</div>

	<div class="div-separator"></div>
</div>

<div class="modal fade" id="select-provider-modal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><strong>Select</strong> Provider <small></small></h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding: 15px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" id="search-provider-field" placeholder="Search Provider...">
                        </div>

                        <div class="form-group">
                        	<div id="providers-results" style="max-height: 200px; overflow-y: auto; padding: 20px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-employee-dependent-modal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><strong>Add</strong> Dependent</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding: 15px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" id="search-provider-field" placeholder="Search Provider...">
                        </div>

                        <div class="form-group">
                        	<div id="providers-results" style="max-height: 200px; overflow-y: auto; padding: 20px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var employee_searching = false;
		$("#search-provider-field").keyup(function() {
			var search_text = $(this).val();
			if (search_text.length <= 1)
				return;

			//if (employee_searching)
				//return false;

			employee_searching = true;
			$.post("{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@postWidgetSearchProvider')}}", {
				search_text: search_text
			}, function(response) {
				$("#providers-results").html(response);
				employee_searching = false;
				$(".tooltips").tooltip();
			});
		});

		$(".select-employee-btn").live("click", function() {
			selectProvider($(this).attr("data-id"));
			fetchEapCases($(this).attr("data-id"));
		});

		@if (!@$provider_data)
			fetchEapCases(false);
		@endif

		@if (old('provider'))
			selectProvider({{old('provider')}});
			fetchEapCases({{old('provider')}});
		@endif

		@if (@$selected_provider_id)
			selectProvider({{$selected_provider_id}});
			fetchEapCases({{$selected_provider_id}});
		@endif
	});

	function selectProvider(id) {
		$.post("{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@postGetProviderData')}}", {
			provider_id: id
		}, function(response) {
			$("#provider-data").html(response);
			$('button[data-dismiss="modal"]').trigger("click");
		});
	}

	function fetchEapCases(id) {
		if (!id) {
			$("#eap-service-wrapper").html('<span class="text-danger">Select a provider to make the list of EAP services available</span>');
			return;
		}

		$.post("{{action('\App\Modules\Providers\Http\Controllers\ProvidersController@postGetProviderEapServices')}}", {
			provider_id: id
		}, function(response) {
			$("#eap-service-wrapper").html(response);
		});
	}
</script>