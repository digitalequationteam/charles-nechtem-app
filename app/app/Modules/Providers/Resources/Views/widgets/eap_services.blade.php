@if (count($eap_services) == 0)
	<span class="text-danger">There are no EAP services assigned to the selected provider</span>
@else
	<select name="eap_service" id="eap_service" class="form-control">
		@foreach ($eap_services as $eap_service)
			<option value="{{$eap_service['id']}}">{{$eap_service["name"]}}</option>
		@endforeach
	</select>
@endif