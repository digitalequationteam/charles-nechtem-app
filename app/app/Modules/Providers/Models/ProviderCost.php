<?php

namespace App\Modules\Providers\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderCost extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_costs';

    public function provider() {
    	return $this->belongsTo("\Provider");
    }

    public function dictionary_value() {
    	return $this->belongsTo("\DictionaryValue");
    }

    public function getDictionaryValue() {
		$dv = $this->dictionary_value()->get()->first();
		if ($this->type == "EAP")
			return $dv->name;
		elseif ($this->type == "OMC")
			return $dv->code." - ".$dv->name;
    }

    public function getRepresentative() {
    	if (empty($this->representative))
    		return \Common::getAdminUser()->first_name." ".\Common::getAdminUser()->last_name;
    	else
    		return $this->representative;
    }
}