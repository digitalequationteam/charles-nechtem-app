<?php

namespace App\Modules\Providers\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderStaff extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_staff';

    public function provider() {
    	return $this->belongsTo("\Provider");
    }
}