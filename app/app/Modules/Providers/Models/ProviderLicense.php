<?php

namespace App\Modules\Providers\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderLicense extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_licenses';

    public function provider() {
    	return $this->belongsTo("\Provider");
    }

    public function getDisplay() {
    	$license_info = (empty($this->number)) ? "" : '#'.$this->number." ";
		$license_info .= "<strong>".$this->title."</strong>";

		if (!empty($this->expiration_date) && $this->expiration_date != "0000-00-00")
			$license_info .= " (expires: ".$this->expiration_date.')';

		return $license_info;
    }
}