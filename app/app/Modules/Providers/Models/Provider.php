<?php

namespace App\Modules\Providers\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'providers';

    public function contacts() {
    	return $this->hasMany("\Contact", "owner_id")->where("type", "provider");
    }

    public function licenses() {
        return $this->hasMany("\ProviderLicense");
    }

    public function staff() {
        return $this->hasMany("\ProviderStaff");
    }

    public function costs() {
        return $this->hasMany("\ProviderCost");
    }

    public function pos_code() {
        return $this->belongsTo("\DictionaryValue");
    }

    public function getPrimaryAddress($format = 1) {
    	if ($format == 1)
    		return $this->primary_street.", ".$this->primary_city.", ".$this->primary_state.", ".$this->primary_zip;
    	elseif ($format == 2)
    		return $this->primary_street."<br />".$this->primary_city.", ".$this->primary_state.", ".$this->primary_zip."<br />".\Common::getCountryName($this->primary_country);
        elseif ($format == 3)
            return $this->primary_street.", ".$this->primary_city.", ".$this->primary_state.", ".$this->primary_zip.", ".\Common::getCountryName($this->primary_country);;
    }

    public function getSecondaryAddress($format = 1) {
    	if ($format == 1)
    		return $this->secondary_street.", ".$this->secondary_city.", ".$this->secondary_state.", ".$this->secondary_zip;
    	elseif ($format == 2)
    		return $this->secondary_street."<br />".$this->secondary_city.", ".$this->secondary_state.", ".$this->secondary_zip."<br />".\Common::getCountryName($this->secondary_country);
    }

    public function getMailingAddress($format = 1) {
    	if ($this->mailing_same_as_primary)
    		return $this->getPrimaryAddress($format);
    	else
    		if ($format == 1)
	    		return $this->mailing_street.", ".$this->mailing_city.", ".$this->mailing_state.", ".$this->mailing_zip;
	    	elseif ($format == 2)
	    		return $this->mailing_street."<br />".$this->mailing_city.", ".$this->mailing_state.", ".$this->mailing_zip."<br />".\Common::getCountryName($this->mailing_country);
    }

    public function getAllAddresses() {
    	$addresses = $this->getPrimaryAddress();

    	if ($this->secondary_address_available)
    		$addresses .= "<br />".$this->getSecondaryAddress();

    	if (!$this->mailing_same_as_primary)
    		$addresses .= "<br />".$this->getMailingAddress();

    	return $addresses;
    }

    public function getMainContact() {
    	$contact = $this->contacts()->get()->first();
    	if ($contact) {
    		return $contact->getDisplay();
    	}
    	else
    		return "";
    }
}