<?php

namespace App\Modules\Providers\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ProvidersController extends Controller
{	
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

    public function getIndex() {
        $providers = \Provider::where("id", "!=", "0");

        if (\Input::get("page") && $providers->count() == 0)
            return redirect("/providers?page=".(\Input::get("page") - 1));

        $appends = array();
        foreach ($_GET as $key => $value) {
            if ($key != "applyFilters" && !empty($value)) {
                $appends[$key] = $value;

                switch ($key) {
                    case 'name';
                        $providers = $providers->where("name", 'LIKE', '%'.$value.'%');
                    break;
                    case 'street';
                        $providers = $providers->where("primary_street", 'LIKE', '%'.$value.'%');
                    break;
                    case 'city';
                        $providers = $providers->where("primary_city", 'LIKE', '%'.$value.'%');
                    break;
                    case 'country';
                        $providers = $providers->where("primary_country", 'LIKE', '%'.$value.'%');
                    break;
                    case 'state';
                        $providers = $providers->where("primary_state", 'LIKE', '%'.$value.'%');
                    break;
                    case 'zip';
                        if (!\Input::get("range"))
                            $providers = $providers->where("primary_zip", 'LIKE', '%'.$value.'%');
                    break;
                    case 'pos_code_id';
                        $providers = $providers->whereIn("pos_code_id", $value);
                    break;
                    case 'license_title';
                        $providers = $providers->whereHas('licenses', function ($q) {
                            global $value;
                           $q->whereIn('title', \Input::get("license_title"));
                        });
                    break;
                    case 'tax_id';
                        $providers = $providers->where("tax_id", 'LIKE', '%'.$value.'%');
                    break;
                    case 'range';
                        $z = new \ZipCode;
                        $zips_arr = array();
                        $zips = $z->get_zips_in_range(\Input::get("zip"), $value, _ZIPS_SORT_BY_DISTANCE_ASC, true); 

                        foreach ((array)$zips as $key3 => $zip)
                            $zips_arr[] = $key3;

                        $providers = $providers->whereIn("primary_zip", $zips_arr);
                    break;
                }
            }
        }

        if (\Input::get("sort_by")) {
            $providers = $providers->orderBy(\Input::get("sort_by"), \Input::get("sort_type"));
        }

        if (\Input::get("export")) {
            $output = implode(",", array('Provider Name', 'Office Address', 'Provider Phone Number')).",\n";

            foreach ($providers->get() as $provider) {
                $output .=  implode(",", array('"'.$provider->name.'"', '"'.$provider->getPrimaryAddress().'"', '"'.@$provider->contacts()->get()->first()->phone.'"')).",\n";
            }

            $headers = array(
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="providers.csv"',
            );

            return \Response::make(rtrim($output, "\n"), 200, $headers);
        }

        $page_size = (\Input::get("page_size")) ? \Input::get("page_size") : 10;
        if ($page_size != "All") {
            $providers = $providers->paginate($page_size);
            $providers_paginator = $providers;
            $providers_paginator = $providers_paginator->appends($appends);
        }
        else {
            $providers = $providers->get();
        }

        return view('providers::index', array('providers' => $providers, 'providers_paginator' => @$providers_paginator));
	}

	public function getAddProvider() {
        return view('providers::add');
	}

    public function getEditProvider($id = null)
    {
        $provider = \Provider::where('id', $id)->get()->first();

        $contacts = array();

        foreach ($provider->contacts()->get() as $contact) {
            if (!empty($contact->first_name)) {
                $contacts[] = array(
                    "id" => $contact->id,
                    "title" => $contact->title,
                    "first_name" => $contact->first_name,
                    "last_name" => $contact->last_name,
                    "email" => $contact->email,
                    "phone" => $contact->phone,
                    "cell_phone" => $contact->cell_phone,
                    "fax" => $contact->fax,
                    "additional_info" => $contact->additional_info
                );
            }
        }

        $licenses = array();

        foreach ($provider->licenses()->get() as $license) {
            if (!empty($license->title)) {
                $licenses[] = array(
                    "id" => $license->id,
                    "number" => $license->number,
                    "title" => $license->title,
                    "expiration_date" => $license->expiration_date
                );
            }
        }

        return view('providers::edit', array( 'provider' => $provider, 'contacts' => $contacts, 'licenses' => $licenses ));
    }

    public function getProviderProfile($id = null)
    {
        $provider = \Provider::where('id', $id)->get()->first();

        return view('providers::profile', array( 'provider' => $provider));
    }

    public function postSaveProvider($id = null)
    {
        $_POST["primary_address"] = \Input::get("primary_street").\Input::get("primary_city").\Input::get("primary_country").\Input::get("primary_state").\Input::get("primary_zip");
        \Input::replace($_POST);

        $rules = \App\Modules\Providers\Validators\ProvidersValidator::$addRules;

        if (\Input::get("secondary_address_available")) {
            $_POST["secondary_address"] = \Input::get("secondary_street").\Input::get("secondary_city").\Input::get("secondary_country").\Input::get("secondary_state").\Input::get("secondary_zip");
            \Input::replace($_POST);
            $rules["secondary_address"] = "required";
        }

        if (!\Input::get("mailing_same_as_primary")) {
            $_POST["mailing_address"] = \Input::get("mailing_street").\Input::get("mailing_city").\Input::get("mailing_country").\Input::get("mailing_state").\Input::get("mailing_zip");
            \Input::replace($_POST);
            $rules["mailing_address"] = "required";
        }

        $validator = \Validator::make( \Input::all(), $rules );

        if ( $validator->passes() ) {
            if ($id == null) {
                $provider = new \Provider;
                $provider->user_id = \Auth::user()->id;
            }
            else
                $provider = \Provider::find($id);

            $provider->name = \Input::get( 'name' );

            if ($id == null)
                $provider->type = \Input::get( 'type' );

            $provider->pos_code_id = \Input::get( 'pos_code_id' );

            if (@$provider->type == "Person") {
                $provider->gender = \Input::get( 'gender' );
                $provider->birthday = (\Input::get( 'birthday' )) ? date("Y-m-d", strtotime(\Input::get( 'birthday' ))) : null;
            }

            $provider->tax_id = \Input::get( 'tax_id' );
            $provider->npi = \Input::get( 'npi' );
            $provider->memo = urldecode(urldecode(\Input::get( 'memo' )));

            $provider->primary_street = \Input::get( 'primary_street' );
            $provider->primary_city = \Input::get( 'primary_city' );
            $provider->primary_country = \Input::get( 'primary_country' );
            $provider->primary_state = \Input::get( 'primary_state' );
            $provider->primary_zip = \Input::get( 'primary_zip' );

            if (\Input::get( 'secondary_address_available' ))
                $provider->secondary_address_available = 1;
            else
                $provider->secondary_address_available = 0;

            $provider->secondary_street = \Input::get( 'secondary_street' );
            $provider->secondary_city = \Input::get( 'secondary_city' );
            $provider->secondary_country = \Input::get( 'secondary_country' );
            $provider->secondary_state = \Input::get( 'secondary_state' );
            $provider->secondary_zip = \Input::get( 'secondary_zip' );

            if (\Input::get( 'mailing_same_as_primary' ))
                $provider->mailing_same_as_primary = 1;
            else
                $provider->mailing_same_as_primary = 0;

            $provider->mailing_street = \Input::get( 'mailing_street' );
            $provider->mailing_city = \Input::get( 'mailing_city' );
            $provider->mailing_country = \Input::get( 'mailing_country' );
            $provider->mailing_state = \Input::get( 'mailing_state' );
            $provider->mailing_zip = \Input::get( 'mailing_zip' );

            $provider->specialities = \Input::get( 'specialities' );
            $provider->insurance_networks = \Input::get( 'insurance_networks' );
            $provider->languages = \Input::get( 'languages' );

            $provider->save();

            foreach ($provider->contacts()->get() as $contact) {
                $contact_removed = true;

                foreach ((array)\Input::get("contact") as $contact2) {
                    if (@$contact2['id'] == $contact->id) {
                        $contact_removed = false;
                    }
                }

                if ($contact_removed) {
                    $contact->delete();
                }
            }

            foreach ((array)\Input::get("contact") as $contact) {
                if (!empty($contact['first_name'])) {
                    if (isset($contact['id']))
                        $new_contact = $provider->contacts()->where("id", $contact['id'])->get()->first();
                    else
                        $new_contact = new \Contact;

                    $new_contact->owner_id = $provider->id;
                    $new_contact->type = "provider";
                    $new_contact->title = @$contact['title'];
                    $new_contact->first_name = @$contact['first_name'];
                    $new_contact->last_name = @$contact['last_name'];
                    $new_contact->email = @$contact['email'];
                    $new_contact->phone = @$contact['phone'];
                    $new_contact->cell_phone = @$contact['cell_phone'];
                    $new_contact->fax = @$contact['fax'];
                    $new_contact->additional_info = @$contact['additional_info'];
                    $new_contact->save();
                }
            }

            foreach ($provider->licenses()->get() as $license) {
                $license_removed = true;

                foreach ((array)\Input::get("license") as $license2) {
                    if (@$license2['id'] == $license->id) {
                        $license_removed = false;
                    }
                }

                if ($license_removed) {
                    $license->delete();
                }
            }

            foreach ((array)\Input::get("license") as $license) {
                if (!empty($license['title'])) {
                    if (isset($license['id']))
                        $new_license = $provider->licenses()->where("id", $license['id'])->get()->first();
                    else
                        $new_license = new \ProviderLicense;

                    $new_license->provider_id = $provider->id;
                    $new_license->number = @$license['number'];
                    $new_license->title = @$license['title'];
                    $new_license->expiration_date = @$license['expiration_date'];
                    $new_license->save();
                }
            }

            if ($id == null) {
                \SecurityLog::addNew("add_provider", \Auth::user()->getName()." added new provider.");
                return Redirect::action( '\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', $provider->id )->with( 'message', 'New provider saved!' );
            }
            else {
                \SecurityLog::addNew("update_provider", \Auth::user()->getName()." updated provider <strong>".$provider->name."</strong>.");
                return Redirect::action( '\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', $provider->id )->with( 'message', 'Provider details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteProviders()
    {
        foreach (\Input::get("ids") as $id) {
            $provider = \Provider::where('id', $id)->get()->first();
            if ($provider) {
                \SecurityLog::addNew("delete_provider", \Auth::user()->getName()." deleted provider <strong>".$provider->name."</strong>.");
                $provider->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getAddStaff($provider_id) {
        $provider = \Provider::where("id", $provider_id)->get()->first();
        return view('providers::staff.add', array("provider" => $provider));
    }

    public function getEditStaff($id = null)
    {
        $staff = \ProviderStaff::where('id', $id)->get()->first();

        return view('providers::staff.edit', array( 'staff' => $staff ));
    }

    public function postSaveStaff($provider_id, $id = null)
    {
        $validator = \Validator::make( \Input::all(), \App\Modules\Providers\Validators\ProviderStaffsValidator::$addRules );

        if ( $validator->passes() ) {
            if ($id == null)
                $staff = new \ProviderStaff;
            else
                $staff = \ProviderStaff::find($id);

            $staff->provider_id = $provider_id;

            $staff->first_name = \Input::get( 'first_name' );
            $staff->last_name = \Input::get( 'last_name' );
            $staff->memo = \Input::get( 'memo' );
            $staff->save();

            $provider = $staff->provider()->get()->first();

            if ($id == null) {
                \SecurityLog::addNew("add_provider_staff", \Auth::user()->getName()." added staff member for provider <strong>".$provider->name."</strong>.");
                return Redirect::action( '\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', array($provider_id, "#staff") )->with( 'message', 'New staff saved!' );
            }
            else {
                \SecurityLog::addNew("update_provider_staff", \Auth::user()->getName()." updated staff member <strong>".$staff->first_name." ".$staff->last_name."</strong> for provider <strong>".$provider->name."</strong>.");
                return Redirect::action( '\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', array($provider_id, "#staff") )->with( 'message', 'Staff details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteStaffs()
    {
        foreach (\Input::get("ids") as $id) {
            $staff = \ProviderStaff::where('id', $id)->get()->first();
            if ($staff) {
                $provider = $staff->provider()->get()->first();
                \SecurityLog::addNew("delete_provider_staff", \Auth::user()->getName()." deleted staff member <strong>".$staff->first_name." ".$staff->last_name."</strong> from provider <strong>".$provider->name."</strong>.");
                $staff->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getAddCost($provider_id, $type = "EAP") {
        $provider = \Provider::where("id", $provider_id)->get()->first();
        return view('providers::costs.add', array("provider" => $provider, "type" => $type));
    }

    public function getEditCost($id = null)
    {
        $cost = \ProviderCost::where('id', $id)->get()->first();

        return view('providers::costs.edit', array( 'cost' => $cost ));
    }

    public function getCostProfile($id = null)
    {
        $cost = \ProviderCost::where('id', $id)->get()->first();

        return view('providers::costs.profile', array( 'cost' => $cost));
    }

    public function postSaveCost($provider_id, $id = null)
    {
        $validator = \Validator::make( \Input::all(), \App\Modules\Providers\Validators\ProviderCostsValidator::$addRules );

        if ( $validator->passes() ) {
            if ($id == null)
                $cost = new \ProviderCost;
            else
                $cost = \ProviderCost::find($id);

            $cost->provider_id = $provider_id;
            $cost->type = \Input::get( 'type' );
            $cost->name = \Input::get( 'name' );
            if ($id == null)
                $cost->dictionary_value_id = \Input::get( 'dictionary_value_id' );
            $cost->cost = \Input::get( 'cost' );
            $cost->representative = \Input::get( 'representative' );
            $cost->signing_date = (\Input::get( 'signing_date' )) ? date("Y-m-d", strtotime(\Input::get( 'signing_date' ))) : null;
            $cost->save();

            $provider = $cost->provider()->get()->first();

            if ($id == null) {
                \SecurityLog::addNew("add_provider_cost", \Auth::user()->getName()." added rate for provider <strong>".$provider->name."</strong>.");
                return Redirect::action( '\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', array($provider_id, "#costs") )->with( 'message', 'New cost saved!' );
            }
            else {
                \SecurityLog::addNew("update_provider_cost", \Auth::user()->getName()." updated rate <strong>".$cost->name."</strong> for provider <strong>".$provider->name."</strong>.");
                return Redirect::action( '\App\Modules\Providers\Http\Controllers\ProvidersController@getProviderProfile', array($provider_id, "#costs") )->with( 'message', 'Cost details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteCosts()
    {
        foreach (\Input::get("ids") as $id) {
            $cost = \ProviderCost::where('id', $id)->get()->first();
            if ($cost) {
                $provider = $cost->provider()->get()->first();
                \SecurityLog::addNew("delete_provider_cost", \Auth::user()->getName()." deleted rate <strong>".$cost->name."</strong> from provider <strong>".$provider->name."</strong>.");
                $cost->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function postWidgetSearchProvider() {
        $providers = \Provider::where("name", 'LIKE', '%'.\Input::get("search_text").'%')->get();

        return view('providers::widgets.results_provider', array("providers" => $providers));
    }

    public function postGetProviderData() {
        $provider = \Provider::where("id", \Input::get("provider_id"))->get()->first();

        return view('providers::widgets.provider_data', array("provider" => $provider));
    }

    public function postGetProviderEapServices() {
        $provider = \Provider::where("id", \Input::get("provider_id"))->get()->first();
        $eap_services = array();

        foreach ($provider->costs()->where("type", "EAP")->get() as $cost) {
            $eap_services[] = array(
                "id" => $cost->dictionary_value_id,
                "name" => $cost->dictionary_value()->get()->first()->name
            );
        }
        return view('providers::widgets.eap_services', array("eap_services" => $eap_services));
    }

    public function postSaveLatLng()
    {
        if (\Input::get("only_me"))
            $provider = \Provider::where("id", ">", 0)->where("user_id", \Auth::user()->id);
        else
            $provider = \Provider::where("id", ">", 0);

        $provider = $provider->skip(\Input::get("provider_nr"))->take(1)->get()->first();

        $provider->lat = \Input::get("lat");
        $provider->lng = \Input::get("lng");
        $provider->save();

        return response()->json(array('status' => "ok"));
    }
}