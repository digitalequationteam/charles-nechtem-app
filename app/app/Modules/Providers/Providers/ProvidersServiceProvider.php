<?php
namespace App\Modules\Providers\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ProvidersServiceProvider extends ServiceProvider
{
	/**
	 * Register the Providers module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Providers\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Providers module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('providers', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('providers', base_path('resources/views/vendor/providers'));
		View::addNamespace('providers', realpath(__DIR__.'/../Resources/Views'));
	}
}
