<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValuesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('dictionary_values', function($table)
        {
            $table->increments('id');
            $table->string('category', 20);
            $table->string('code', 255)->nullable();
            $table->string('name', 255);
            $table->string('treatment_type', 255)->nullable();
            $table->boolean('removable')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dictionary_values');
    }
}