<?php
namespace App\Modules\Settings\Helpers;

class ValuesDictionary {
	public static $categories = array(
		"specialities" => array(
			"title_sg" => "Speciality",
			"title_pl" => "Specialities"
		),
		"insurance_networks" => array(
			"title_sg" => "Insurance Network",
			"title_pl" => "Insurance Networks"
		),
		"pos_codes" => array(
			"title_sg" => "POS Code",
			"title_pl" => "POS Codes"
		),
		"cpt_codes" => array(
			"title_sg" => "CPT Code",
			"title_pl" => "CPT Codes"
		),
		"eap_services" => array(
			"title_sg" => "EAP Service",
			"title_pl" => "EAP Services"
		),
		"problem_types" => array(
			"title_sg" => "Problem Type",
			"title_pl" => "Problem Types"
		),
		"icd_codes" => array(
			"title_sg" => "ICD Code",
			"title_pl" => "ICD Codes"
		),
		"languages" => array(
			"title_sg" => "Language",
			"title_pl" => "Languages"
		)
	);
}