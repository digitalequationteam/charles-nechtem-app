<?php

namespace App\Modules\Settings\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Ifsnop\Mysqldump as IMysqldump;

class SettingsController extends Controller
{	
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

	public function getProfile() {
        return view('settings::profile', array('user' => $this->me));
	}

    public function postSaveProfile()
    {
        \App\Modules\UsersManagement\Validators\StaffValidator::$addRules['email'] = 'required|email|unique:users,email,' . $this->me->id;
        \App\Modules\UsersManagement\Validators\StaffValidator::$addRules['password'] = 'between:6,12|confirmed';
        \App\Modules\UsersManagement\Validators\StaffValidator::$addRules['password_confirmation'] = 'between:6,12';

        if ($this->me->type == "admin") {
        	unset(\App\Modules\UsersManagement\Validators\StaffValidator::$addRules['email']);
        	unset(\App\Modules\UsersManagement\Validators\StaffValidator::$addRules['personal_email']);
	        unset(\App\Modules\UsersManagement\Validators\StaffValidator::$addRules['birthday']);
	        unset(\App\Modules\UsersManagement\Validators\StaffValidator::$addRules['hire_date']);
	        unset(\App\Modules\UsersManagement\Validators\StaffValidator::$addRules['street']);
	        unset(\App\Modules\UsersManagement\Validators\StaffValidator::$addRules['city']);
	        unset(\App\Modules\UsersManagement\Validators\StaffValidator::$addRules['state']);
	        unset(\App\Modules\UsersManagement\Validators\StaffValidator::$addRules['zip']);
        }

        $validator = \Validator::make( \Input::all(), \App\Modules\UsersManagement\Validators\StaffValidator::$addRules );


        if ( $validator->passes() ) {
            $user = $this->me;
            $user->email = \Input::get( 'email' );

            if (!empty(\Input::get( 'password' )))
                $user->password = \Hash::make( \Input::get( 'password' ) );

            $user->first_name = \Input::get( 'first_name' );
            $user->last_name = \Input::get( 'last_name' );
            $user->initials = \Input::get( 'initials' );
            $user->phone = \Input::get( 'phone' );
            $user->cell_phone = \Input::get( 'cell_phone' );
            $user->personal_email = \Input::get( 'personal_email' );
            if ($user->type == "user") {
	            $user->birthday = (\Input::get( 'birthday' )) ? date("Y-m-d", strtotime(\Input::get( 'birthday' ))) : null;;
	            $user->ssn = \Input::get( 'ssn' );
	            $user->hire_date = date("Y-m-d", strtotime(\Input::get( 'hire_date' )));
	            $user->street = \Input::get( 'street' );
	            $user->city = \Input::get( 'city' );
	            $user->country = "US";
	            $user->state = \Input::get( 'state' );
	            $user->zip = \Input::get( 'zip' );
	            $user->bank_name = \Input::get( 'bank_name' );
	            $user->bank_routing = \Input::get( 'bank_routing' );
	            $user->bank_account_number = \Input::get( 'bank_account_number' );
	            $user->hire_date = date("Y-m-d", strtotime(\Input::get( 'bank_termination_date' )));
            }

            //$user->active = 1;
            $user->save();

            \SecurityLog::addNew("update_profile", \Auth::user()->getName()." updated profile.");

            return Redirect::action( '\App\Modules\Settings\Http\Controllers\SettingsController@getProfile' )->with( 'message', 'Profile settings saved!' );
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

	public function getValues($category = null) {
		switch ($category) {
			case null;
                return view('settings::values.index');
			break;
			default:
                $values = \DictionaryValue::where("category", $category)->get();
                return view('settings::values.list', array("category" => $category, "values" => $values));
			break;
		}
	}

    public function postSaveValue($id = null) {
        $validations = array(
            "name" => "required"
        );

        if (\Input::get( 'category' ) == "pos_codes") {
            $validations["code"] = "required";
            $validations["treatment_type"] = "required";
        }
        
        if (\Input::get( 'category' ) == "cpt_codes" || \Input::get( 'category' ) == "icd_codes") {
            $validations["code"] = "required";
        }

        $validator = \Validator::make( \Input::all(), $validations);

        if ( $validator->passes() ) {
            if ($id) {
                $value = \DictionaryValue::where("id", $id)->get()->first();
                $old_name = $value->name;
            }
            else
                $value = new \DictionaryValue;

            $value->name = \Input::get( 'name' );

            if ($id == null)
                $value->category = \Input::get( 'category' );

            if ($id == null)
                $value->removable = 1;

            if (\Input::get( 'category' ) == "pos_codes") {
                $value->code = \Input::get( 'code' );
                $value->treatment_type = \Input::get( 'treatment_type' );
            }

            if (\Input::get( 'category' ) == "cpt_codes" || \Input::get( 'category' ) == "icd_codes") {
                $value->code = \Input::get( 'code' );
            }

            $value->save();

            if (\Input::get("ajaxed"))
                return response()->json(array('status' => "ok"));

            if ($id == null) {
                \SecurityLog::addNew("add_value", \Auth::user()->getName()." added <strong>".\ValuesDictionary::$categories[\Input::get( 'category' )]['title_sg']."</strong>.");
                return Redirect::action( '\App\Modules\Settings\Http\Controllers\SettingsController@getValues', array(\Input::get( 'category' )) )->with( 'message', 'New '.\ValuesDictionary::$categories[\Input::get( 'category' )]['title_sg'].' saved!' );
            }
            else {
                \SecurityLog::addNew("update_value", \Auth::user()->getName()." updated <strong>".\ValuesDictionary::$categories[\Input::get( 'category' )]['title_sg']."</strong> from <strong>".$old_name."</strong> to <strong>".$value->name."</strong>.");
                return Redirect::action( '\App\Modules\Settings\Http\Controllers\SettingsController@getValues', array(\Input::get( 'category' )) )->with( 'message', ''.\ValuesDictionary::$categories[\Input::get( 'category' )]['title_sg'].' details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteValues()
    {
        foreach (\Input::get("ids") as $id) {
            $value = \DictionaryValue::where('id', $id)->get()->first();
            if ($value) {
                \SecurityLog::addNew("delete_value", \Auth::user()->getName()." deleted value <strong>".$value->name."</strong>.");
                $value->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getIpAccess()
    {
        return view('settings::ip-access');
    }

    public function postSaveIpAccessSettings() {
        if (\Auth::user()->hasRole("super_admin") && \Auth::user()->type == "admin") {
            if (SERVER_ENVIRONMENT == "live") {
                $this->owner->saveSetting("staging.access", \Input::get("staging_access"));
                $this->owner->saveSetting("staging.ips.allowed", \Input::get("staging_ips_allowed"));
                $this->owner->saveSetting("staging.ips.excluded", \Input::get("staging_ips_excluded"));

                $this->owner->saveSetting("debugging.ips", \Input::get("debugging_ips"));
            }
        }

        $this->owner->saveSetting("front_site.access", \Input::get("front_site_access"));
        $this->owner->saveSetting("front_site.ips.allowed", \Input::get("front_site_ips_allowed"));
        $this->owner->saveSetting("front_site.ips.excluded", \Input::get("front_site_ips_excluded"));

        $this->owner->saveSetting("map.ips", \Input::get("map_ips"));

        \SecurityLog::addNew("update_ip_settings", \Auth::user()->getName()." updated IP settings.");

        return Redirect::action( '\App\Modules\Settings\Http\Controllers\SettingsController@getIpAccess' )->with( 'message', 'Settings saved.' );
    }

    public function getImportExport()
    {
        return view('settings::import-export');
    }

    public function postSaveImportExport() {
        if (\Input::get("action") == 1) {
            $filename = 'dump-'.date("Y-m-d").'-'.rand(000000, 999999).'.sql';
            $file = base_path('public/uploads/'.$filename);
            $myfile = fopen($file, "w");
            chmod($file, 0777);
            
            try {
                $dump = new IMysqldump\Mysqldump('mysql:host=localhost;dbname=charles_nechtem', 'root', 'born4biz');
                $dump->start($file);
            } catch (\Exception $e) {
                echo 'mysqldump-php error: ' . $e->getMessage();
            }

            return \Response::download($file, $filename);

            \SecurityLog::addNew("export_db", \Auth::user()->getName()." exported the database.");

            exit(0);
        }
        else {
            if (\Input::file('file')) {
                $fileName = "";
                $destinationPath = 'uploads'; // upload path
                $extension = \Input::file('file')->getClientOriginalExtension();
                if ($extension != "sql")
                    return Redirect::back()->withErrors(["Please select a SQL file."])->withInput();

                //$fileName = rand(111111111,999999999).'.'.$extension;
                $fileName = \Input::file('file')->getClientOriginalName();
                \Input::file('file')->move($destinationPath, $fileName);

                $db = new \PDO('mysql:host=localhost;dbname=charles_nechtem;charset=utf8', 'root', 'born4biz');
                $db = \DB::connection()->getPdo()->exec( 'DROP DATABASE IF EXISTS charles_nechtem');
                $db = \DB::connection()->getPdo()->exec( 'CREATE DATABASE charles_nechtem');

                $filename = $destinationPath.'/'.$fileName;

                $db = new \PDO('mysql:host=localhost;dbname=charles_nechtem;charset=utf8', 'root', 'born4biz');

                // Temporary variable, used to store current query
                $templine = '';
                // Read in entire file
                $lines = file($filename);
                // Loop through each line
                foreach ($lines as $line)
                {
                    // Skip it if it's a comment
                    if (substr($line, 0, 2) == '--' || $line == '')
                        continue;

                    // Add this line to the current segment
                    $templine .= $line;
                    // If it has a semicolon at the end, it's the end of the query
                    if (substr(trim($line), -1, 1) == ';')
                    {
                        $db->query($templine);
                        $templine = '';
                    }
                }

                \SecurityLog::addNew("import_db", \Auth::user()->getName()." imported database.");

                return Redirect::back()->with( 'message', 'Data successfully imported.' );
            }
            else {
                return Redirect::back()->withErrors(["Please select a file."])->withInput();
            }
        }
    }

    public function postUploadNewLoginBackground() {
        if (\Input::file('lb-file')) {
            $fileName = "";
            $destinationPath = 'uploads'; // upload path
            $extension = \Input::file('lb-file')->getClientOriginalExtension();
            $fileName = rand(111111111,999999999).'.'.$extension;
            \Input::file('lb-file')->move($destinationPath, $fileName);
            $this->owner->login_background = $fileName;
            $this->owner->save();

            \SecurityLog::addNew("change_login_bg", \Auth::user()->getName()." changed login page background image.");
            ?>
                <script type="text/javascript">
                    document.domain = '<?php echo \Config::get("app.domain"); ?>';
                    parent.completeUpload('/uploads/<?php echo $fileName; ?>');
                </script>
            <?php
        }
    }
}