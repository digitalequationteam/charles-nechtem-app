<?php

namespace App\Modules\Settings\Models;
use Illuminate\Database\Eloquent\Model;

class DictionaryValue extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dictionary_values';

    public static function getAll($category) {
    	$result = array();

    	foreach (\DictionaryValue::where("category", $category)->get() as $item)
    		$result[] = $item->name;

    	return $result;
    }
}