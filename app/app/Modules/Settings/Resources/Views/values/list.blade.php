@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/settings/values">Settings</a></li>
        <li><a href="/settings/values">Manage Values</a></li>
        <li class="active">{{\ValuesDictionary::$categories[$category]['title_pl']}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>{{\ValuesDictionary::$categories[$category]['title_pl']}}</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\Settings\Http\Controllers\SettingsController@getValues') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>
	-->

	<div class="clearfix"></div>

	<div class="row users">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
		                    <div class="filter-checkbox">
		                        <a href="#add-new-modal" data-toggle="modal" id="table-edit_new" class="btn btn-danger">
                                    Add New <i class="fa fa-plus"></i>
                                </a>
                                &nbsp;
                                <a href="#" class="btn btn-danger btn-delete">Delete</a>
		                    </div>
		                    <table id="values-table" class="table table-tools table-striped">
		                        <thead>
		                            <tr>
		                                <th style="max-width: 20px">
		                                    <input type="checkbox" class="check_all"/>
		                                </th>

		                                <th>{{\ValuesDictionary::$categories[$category]['title_sg']}}</th>

		                                @if ($category == "pos_codes")
		                                	<th>Name</th>
		                                	<th>Treatment Type</th>
		                                @endif

		                                @if ($category == "cpt_codes" || $category == "icd_codes")
		                                	<th>Name</th>
		                                @endif

		                                <th class="text-center">Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<?php
									foreach ($values as $value) { ?>
										<tr class="odd gradeX">
											<td style="max-width: 20px"><input type="checkbox" class="batch_id" value="{{$value->id}}" /></td>
											@if ($category == "pos_codes" || $category == "cpt_codes" || $category == "icd_codes")
												<td>
													 <?php echo $value->code; ?>
												</td>
											@endif
											<td>
												 <?php echo $value->name; ?>
											</td>
											@if ($category == "pos_codes")
												<td>
													 <?php echo $value->treatment_type; ?>
												</td>
											@endif
											<td style="text-align: center;">
												<a href="#" class="btn bg-green tooltips edit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit {{\ValuesDictionary::$categories[$category]['title_sg']}}"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn bg-red btn-delete-single tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete {{\ValuesDictionary::$categories[$category]['title_sg']}}"><i class="fa fa-times"></i></a>
											</td>
										</tr>
									<?php 
										}
									?>
		                        </tbody>
		                    </table>
		                </div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

    <div class="modal fade" id="add-new-modal" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
            	<form action="{{ action('\App\Modules\Settings\Http\Controllers\SettingsController@postSaveValue') }}" method="post">
            		<input type="hidden" name="category" value="{{$category}}" />
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                    <h4 class="modal-title"><strong>Add </strong> {{\ValuesDictionary::$categories[$category]['title_sg']}}</h4>
	                </div>
	                <div class="modal-body">
	                	@if($errors->all())
					        <div class="alert alert-danger">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            @foreach($errors->all() as $error_msg)
					        
					                <span>
					                 {{ $error_msg }}
					                </span><br/>
					            @endforeach
					        </div>
					    @endif

					    @if ($category == "pos_codes")
		                    <div class="row">
		                        <div class="col-md-12">
		                            <div class="form-group">
		                                <label for="field-1" class="control-label">POS Code</label>
		                                <input type="text" class="form-control" id="code" name="code" placeholder="" required>
		                            </div>
		                        </div>
		                    </div>
	                    @endif

					    @if ($category == "cpt_codes" || $category == "icd_codes")
		                    <div class="row">
		                        <div class="col-md-12">
		                            <div class="form-group">
		                                <label for="field-1" class="control-label">{{\ValuesDictionary::$categories[$category]['title_sg']}}</label>
		                                <input type="text" class="form-control" id="code" name="code" placeholder="" required>
		                            </div>
		                        </div>
		                    </div>
	                    @endif

	                    <div class="row">
	                        <div class="col-md-12">
	                            <div class="form-group">
	                                <label for="field-1" class="control-label">Name</label>
	                                <input type="text" class="form-control" id="name" name="name" placeholder="" required>
	                            </div>
	                        </div>
	                    </div>

					    @if ($category == "pos_codes")
		                    <div class="row">
		                        <div class="col-md-12">
		                            <div class="form-group">
		                                <label for="field-1" class="control-label">Treatment Type</label>
		                                <select class="form-control" id="treatment_type" name="treatment_type" required>
		                                	<option value="Inpatient">Inpatient</option>
		                                	<option value="Inpatient (Partial Hospitalization)">Inpatient (Partial Hospitalization)</option>
		                                	<option value="Outpatient">Outpatient</option>
		                                	<option value="Other">Other</option>
		                                </select>
		                            </div>
		                        </div>
		                    </div>
	                    @endif
	                </div>
	                <div class="modal-footer text-center">
	                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
	                    <button class="btn btn-success"><i class="fa fa-check"></i> Save</button>
	                </div>
	            </form>
            </div>
        </div>
    </div>
    <div class="md-overlay"></div>

	<script type="text/javascript">
		function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = $(jqTds[0]).html();
            jqTds[1].innerHTML = '<input type="text" class="form-control small" value="' + aData[1] + '">';
            @if ($category == "pos_codes")
            	jqTds[2].innerHTML = '<input type="text" class="form-control small" value="' + aData[2] + '">';
            	jqTds[3].innerHTML = '<select class="form-control" id="treatment_type" name="treatment_type" required>\
		                                	<option value="Inpatient" ' + ((aData[3] == "Inpatient") ? "selected" : "") + '>Inpatient</option>\
		                                	<option value="Inpatient (Partial Hospitalization)" ' + ((aData[3] == "Inpatient (Partial Hospitalization)") ? "selected" : "") + '>Inpatient (Partial Hospitalization)</option>\
		                                	<option value="Outpatient" ' + ((aData[3] == "Outpatient") ? "selected" : "") + '>Outpatient</option>\
		                                	<option value="Other" ' + ((aData[3] == "Other") ? "selected" : "") + '>Other</option>\
		                                </select>\
		                             ';
            	jqTds[4].innerHTML = '<div class="text-center"><a class="edit btn btn-success" href="">Save</a> <a class="btn btn-danger btn-cancel-edit" href=""><i class="fa fa-times-circle"></i> Cancel</a></div>';
            @elseif ($category == "cpt_codes" || $category == "icd_codes")
            	jqTds[2].innerHTML = '<input type="text" class="form-control small" value="' + aData[2] + '">';
            	jqTds[3].innerHTML = '<div class="text-center"><a class="edit btn btn-success" href="">Save</a> <a class="btn btn-danger btn-cancel-edit" href=""><i class="fa fa-times-circle"></i> Cancel</a></div>';
            @else
            	jqTds[2].innerHTML = '<div class="text-center"><a class="edit btn btn-success" href="">Save</a> <a class="btn btn-danger btn-cancel-edit" href=""><i class="fa fa-times-circle"></i> Cancel</a></div>';
            @endif
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var jqTds = $('>td', nRow);
            var jqSelects = $('select', nRow);
            oTable.fnUpdate($(jqTds[0]).html(), nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            @if ($category == "pos_codes")
	            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
	            oTable.fnUpdate(jqSelects[0].value, nRow, 3, false);
            @elseif ($category == "cpt_codes" || $category == "icd_codes")
	            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            @endif
            oTable.fnDraw();

            $.post("{{action('\App\Modules\Settings\Http\Controllers\SettingsController@postSaveValue')}}/" + $(jqTds[0]).find("input").val(), {
            	@if ($category == "pos_codes")
            		code: jqInputs[1].value,
            		name: jqInputs[2].value,
            		treatment_type: jqSelects[0].value,
            	@elseif ($category == "cpt_codes" || $category == "icd_codes")
            		code: jqInputs[1].value,
            		name: jqInputs[2].value,
            	@else
            		name: jqInputs[1].value,
            	@endif
            	category: '{{$category}}',
            	ajaxed: true
            });
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var jqTds = $('>td', nRow);
            oTable.fnUpdate($(jqTds[0]).html(), nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnDraw();
        }

		$(document).ready(function() {
			var opt = {};
	        // Tools: export to Excel, CSV, PDF & Print
	        opt.sDom = "<'row m-t-10'<'col-md-6'><'col-md-6'Tf>r>t<'row'<'col-md-6'><'col-md-6 align-right'p>>",
	        opt.oLanguage = { "sSearch": "","sZeroRecords": "Nothing found" } ,
	        opt.iDisplayLength = 15,
	        opt.oTableTools = {
	            /*"sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",*/
	            "aButtons": []
	        };
	        opt.bSort = false;

	        var oTable = $('#values-table').dataTable(opt);
	        oTable.fnDraw();

	        var nEditing = null;

	        /* Add a placeholder to searh input */
	        $('.dataTables_filter input').attr("placeholder", "Search...");

	        $('#values-table a.edit').live('click', function (e) {
	            e.preventDefault();
	            /* Get the row as a parent of the link that was clicked on */
	            var nRow = $(this).parents('tr')[0];

	            if (nEditing !== null && nEditing != nRow) {
	                restoreRow(oTable, nEditing);
	                editRow(oTable, nRow);
	                nEditing = nRow;
	            } else if (nEditing == nRow && this.innerHTML == "Save") {
	                 /* This row is being edited and should be saved */
	                saveRow(oTable, nEditing);
	                nEditing = null;
	                // alert("Updated! Do not forget to do some ajax to sync with backend :)");
	            } else {
	                 /* No row currently being edited */
	                editRow(oTable, nRow);
	                nEditing = nRow;
	            }
	        });

	        $('#values-table a.btn-cancel-edit').live('click', function(e) {
	            e.preventDefault();
	        	var nRow = $(this).parents('tr')[0];
	        	cancelEditRow(oTable, nRow);
	        });

	        /* Delete a product */
	        $('#values-table a.btn-delete-single').live('click', function (e) {
	            e.preventDefault();
	            if (confirm("Are you sure to delete this {{\ValuesDictionary::$categories[$category]['title_sg']}}?") == false) {
	                return;
	            }
	            var nRow = $(this).parents('tr')[0];
	            oTable.fnDeleteRow(nRow);

	            var ids = [$(this).parents("tr").find('.batch_id').val()];

	            $.post("{{ action('\App\Modules\Settings\Http\Controllers\SettingsController@postDeleteValues') }}", {
	        		ids: ids
	        	}, function() {
	        		for (var i in nRows)
	            		oTable.fnDeleteRow(nRows[i]);
	        	});
	        });

	        $(".btn-delete").click(function() {
	        	var ids = [];
	        	var nRows = [];

	        	var nRow = $(this).parents('tr')[0];

	        	$(".batch_id:checked").each(function() {
	        		ids.push($(this).val());
	        		nRows.push($(this).parents('tr')[0]);
	        	});

	        	if (ids.length == 0) {
	        		alert('No {{\ValuesDictionary::$categories[$category]['title_pl']}} were selected.');
	        		return;
	        	}

	        	if (confirm('Are you sure you want to delete the selected {{\ValuesDictionary::$categories[$category]['title_pl']}}?')) {
		        	$.post("{{ action('\App\Modules\Settings\Http\Controllers\SettingsController@postDeleteValues') }}", {
		        		ids: ids
		        	}, function() {
		        		for (var i in nRows)
		            		oTable.fnDeleteRow(nRows[i]);
		        	});
	        	}
	        });

	        @if($errors->all())
	        	$('#table-edit_new').trigger("click");
	        @endif
	    });
    </script>
@stop