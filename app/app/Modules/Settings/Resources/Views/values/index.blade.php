@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/settings/values">Settings</a></li>
        <li class="active">Manage Values</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Manage Values</strong></h3>
	</div> <!-- / .page-header -->

	<div class="clearfix"></div>

	<div class="row users" style="max-width: 600px; margin: auto;">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
		                    <table id="users-table" class="table table-tools table-striped">
		                        <thead>
		                        </thead>
		                        <tbody>
		                        	@foreach (\ValuesDictionary::$categories as $slug => $category)
									<tr class="odd gradeX">
										<td>
											 {{$category['title_pl']}}
										</td>
										<td style="text-align: right;">
											<a href="{{ action('\App\Modules\Settings\Http\Controllers\SettingsController@getValues', array($slug)) }}" class="btn bg-green tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$category['title_pl']}}"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									@endforeach
		                        </tbody>
		                    </table>
		                </div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
	        /* Add a placeholder to searh input */
	        $('.dataTables_filter input').attr("placeholder", "Search users...");

	        /* Delete a product */
	        $('#users-table a.btn-delete-single').live('click', function (e) {
	            e.preventDefault();
	            if (confirm("Are you sure to delete this staff member?") == false) {
	                return;
	            }
	            var nRow = $(this).parents('tr')[0];
	            oTable.fnDeleteRow(nRow);

	            var ids = [$(this).parents("tr").find('.batch_id').val()];

	            $.post("{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@postDeleteUsers') }}", {
	        		ids: ids
	        	}, function() {
	        		for (var i in nRows)
	            		oTable.fnDeleteRow(nRows[i]);
	        	});
	        });

	        $(".btn-delete").click(function() {
	        	var ids = [];
	        	var nRows = [];

	        	var nRow = $(this).parents('tr')[0];

	        	$(".batch_id:checked").each(function() {
	        		ids.push($(this).val());
	        		nRows.push($(this).parents('tr')[0]);
	        	});

	        	if (ids.length == 0) {
	        		alert('No staff members were selected.');
	        		return;
	        	}

	        	if (confirm('Are you sure you want to delete the selected staff members?')) {
		        	$.post("{{ action('\App\Modules\UsersManagement\Http\Controllers\UsersManagementController@postDeleteUsers') }}", {
		        		ids: ids
		        	}, function() {
		        		for (var i in nRows)
		            		oTable.fnDeleteRow(nRows[i]);
		        	});
	        	}
	        });
	    });
    </script>
@stop