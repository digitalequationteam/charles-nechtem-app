@extends('layouts.main')
@section('content')

<?php
use App\Common;
?>
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/settings/import-export">Settings</a></li>
        <li class="active">Import/Export</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Import or Export Data</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		
	</div>

	<div class="clearfix"></div>

    <style type="text/css">
        #profile-tabs li {
            margin-bottom: 10px;
        }
    </style>

    @if(Session::has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>
                {{ Session::get('message') }}
            </span>
        </div>
    @endif
    @if($errors->all())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach($errors->all() as $error_msg)
        
                <span>
                 {{ $error_msg }}
                </span><br/>
            @endforeach
        </div>
    @endif

    <form action="{{ action('\App\Modules\Settings\Http\Controllers\SettingsController@postSaveImportExport') }}" method="post" enctype="multipart/form-data" class="panel form-horizontal form-bordered">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="panel-body no-padding-hr">
            <div class="form-group no-border-t no-padding-t">
                <label class="control-label col-md-4">Action</label>
				<div class="col-md-5">
					<label class="radio-inline">
					<input type="radio" name="action" id="access4" value="1" onclick="$('.import').hide();" checked /> Export </label>
					<label class="radio-inline">
					<input type="radio" name="action" id="access5" value="2" onclick="$('.import').show();" @if (old('action') == 2) checked @endif /> Import </label>
				</div>
            </div>
            <div class="form-group import no-border-t" style="@if (old('action') != 2) display: none; @endif">
                <label class="control-label col-md-4">Select File</label>
                <div class="col-md-5">
                	<input type="file" name="file" accept=".sql" />
                	<br />
                	<small>(must be a .CSV file that was previously exported from this system)</small>
                </div>
            </div>
        </div>

		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-offset-4 col-sm-5 text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Run</button>
				</div>
			</div>
		</div>
	</form>
@stop