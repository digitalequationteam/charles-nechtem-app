@extends('layouts.main')
@section('content')

<?php
use App\Common;
?>
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/settings/ip-access">Settings</a></li>
        <li class="active">IP Access</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Manage Access by IP</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		
	</div>

	<div class="clearfix"></div>

    <style type="text/css">
        #profile-tabs li {
            margin-bottom: 10px;
        }
    </style>

    @if(Session::has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>
                {{ Session::get('message') }}
            </span>
        </div>
    @endif
    @if($errors->all())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach($errors->all() as $error_msg)
        
                <span>
                 {{ $error_msg }}
                </span><br/>
            @endforeach
        </div>
    @endif

    <form action="{{ action('\App\Modules\Settings\Http\Controllers\SettingsController@postSaveIpAccessSettings') }}" method="post" class="panel form-horizontal form-bordered">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="panel-heading">
            <span class="panel-title">Settings</span>
        </div>

        <div class="panel-body no-padding-hr">
            @if (Auth::user()->hasRole("super_admin") && Auth::user()->type == "admin")
            	@if (SERVER_ENVIRONMENT == "live")
    			<div class="form-group">
                    <label class="control-label col-md-4">&nbsp;</label>
                    <div class="col-md-5">
                    	<h3>Staging Access <i class="fa fa-question-circle text-info tooltips" title="Restrict access by IP to the staging site."></i></h3>
                    </div>
                </div>
                <div class="form-group no-border-t no-padding-t">
                    <label class="control-label col-md-4">Restriction</label>
    				<div class="col-md-5">
    					<label class="radio-inline">
    					<input type="radio" name="staging_access" id="staging.access4" value="1" <?php if (Auth::user()->Owner()->getSetting("staging.access")) { ?>checked<?php } ?> onclick="$('.staging-access').show(); $('.staging-access2').hide();" /> Restrict all except trusted </label>
    					<label class="radio-inline">
    					<input type="radio" name="staging_access" id="staging.access5" value="0" <?php if (!Auth::user()->Owner()->getSetting("staging.access")) { ?>checked<?php } ?> onclick="$('.staging-access').hide(); $('.staging-access2').show();" /> Allow all except excluded </label>
    				</div>
                </div>
                <div class="form-group staging-access no-border-t" <?php if (!Auth::user()->Owner()->getSetting("staging.access")) { ?>style="display: none;"<?php } ?>>
                    <label class="control-label col-md-4">Allowed IPs</label>
                    <div class="col-md-5">
                    	<textarea class="form-control" name="staging_ips_allowed" id="staging_ips_allowed" style="height: 100px;">{{Auth::user()->Owner()->getSetting("staging.ips.allowed")}}</textarea>
                    	<small>(enter IP addresses, one per line)</small>
                    </div>
                </div>
                <div class="form-group staging-access2 no-border-t" <?php if (Auth::user()->Owner()->getSetting("staging.access")) { ?>style="display: none;"<?php } ?>>
                    <label class="control-label col-md-4">Excluded IPs</label>
                    <div class="col-md-5">
                    	<textarea class="form-control" name="staging_ips_excluded" id="staging_ips_excluded" style="height: 100px;">{{Auth::user()->Owner()->getSetting("staging.ips.excluded")}}</textarea>
                    	<small>(enter IP addresses, one per line)</small>
                    </div>
                </div>

    			<div class="form-group">
                    <label class="control-label col-md-4">&nbsp;</label>
                    <div class="col-md-5">
                    	<h3>Debugging Access <i class="fa fa-question-circle text-info tooltips" title="Enable debugging for the following IPs."></i></h3>
                    </div>
                </div>

                <div class="form-group no-border-t">
                    <label class="control-label col-md-4">Excluded IPs</label>
                    <div class="col-md-5">
                    	<textarea class="form-control" name="debugging_ips" id="debugging_ips" style="height: 100px;">{{Auth::user()->Owner()->getSetting("debugging.ips")}}</textarea>
                    	<small>(enter IP addresses, one per line)</small>
                    </div>
                </div>
                @endif
            @endif

			<div class="form-group">
                <label class="control-label col-md-4">&nbsp;</label>
                <div class="col-md-5">
                	<h3>Front Site Access <i class="fa fa-question-circle text-info tooltips" title="Restrict access by IP to the frontend site."></i></h3>
                </div>
            </div>
            <div class="form-group no-border-t no-padding-t">
                <label class="control-label col-md-4">Restriction</label>
				<div class="col-md-5">
					<label class="radio-inline">
					<input type="radio" name="front_site_access" id="front_site.access4" value="1" <?php if (Auth::user()->Owner()->getSetting("front_site.access")) { ?>checked<?php } ?> onclick="$('.front_site-access').show(); $('.front_site-access2').hide();" /> Restrict all except trusted </label>
					<label class="radio-inline">
					<input type="radio" name="front_site_access" id="front_site.access5" value="0" <?php if (!Auth::user()->Owner()->getSetting("front_site.access")) { ?>checked<?php } ?> onclick="$('.front_site-access').hide(); $('.front_site-access2').show();" /> Allow all except excluded </label>
				</div>
            </div>
            <div class="form-group front_site-access no-border-t" <?php if (!Auth::user()->Owner()->getSetting("front_site.access")) { ?>style="display: none;"<?php } ?>>
                <label class="control-label col-md-4">Allowed IPs</label>
                <div class="col-md-5">
                	<textarea class="form-control" name="front_site_ips_allowed" id="front_site_ips_allowed" style="height: 100px;">{{Auth::user()->Owner()->getSetting("front_site.ips.allowed")}}</textarea>
                	<small>(enter IP addresses, one per line)</small>
                </div>
            </div>
            <div class="form-group front_site-access2 no-border-t" <?php if (Auth::user()->Owner()->getSetting("front_site.access")) { ?>style="display: none;"<?php } ?>>
                <label class="control-label col-md-4">Excluded IPs</label>
                <div class="col-md-5">
                	<textarea class="form-control" name="front_site_ips_excluded" id="front_site_ips_excluded" style="height: 100px;">{{Auth::user()->Owner()->getSetting("front_site.ips.excluded")}}</textarea>
                	<small>(enter IP addresses, one per line)</small>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4">&nbsp;</label>
                <div class="col-md-5">
                    <h3>Map Access Restriction <i class="fa fa-question-circle text-info tooltips" title="Enter IPs that will be excluded from showing up on the security map."></i></h3>
                </div>
            </div>

            <div class="form-group no-border-t">
                <label class="control-label col-md-4">Excluded IPs</label>
                <div class="col-md-5">
                    <textarea class="form-control" name="map_ips" id="map_ips" style="height: 100px;">{{Auth::user()->Owner()->getSetting("map.ips")}}</textarea>
                    <small>(enter IP addresses, one per line)</small>
                </div>
            </div>
        </div>

		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-offset-4 col-sm-5 text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>
@stop