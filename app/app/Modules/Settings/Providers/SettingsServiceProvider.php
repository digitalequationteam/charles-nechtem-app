<?php
namespace App\Modules\Settings\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{
	/**
	 * Register the Settings module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Settings\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Settings module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('settings', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('settings', base_path('resources/views/vendor/settings'));
		View::addNamespace('settings', realpath(__DIR__.'/../Resources/Views'));
	}
}
