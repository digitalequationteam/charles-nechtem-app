@extends('auth::layout')
@section('content')
	<form action="{{ action('\App\Modules\Auth\Http\Controllers\AuthController@postForgotPassword') }}" method="post" id="password-reset-form_id">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<div class="signin-text">
			<span>Password reset</span>
		</div> <!-- / .signin-text -->

		@if(Session::has('error'))
	        <div class="alert alert-danger">
	                <button type="button" class="close" data-dismiss="alert">×</button>
	                <span>
	                 {{ Session::get('error') }}
	            </span>
	        </div>
	    @endif

		@if(Session::has('message'))
	        <div class="alert alert-success">
	            <button type="button" class="close" data-dismiss="alert">×</button>
	            <span>
	                {{ Session::get('message') }}
	            </span>
		    </div>
	    @endif

	    @if($errors->all())
	        <div class="alert alert-danger">
	            <button type="button" class="close" data-dismiss="alert">×</button>
	            @foreach($errors->all() as $error_msg)
	        
	                <span>
	                 {{ $error_msg }}
	                </span><br/>
	            @endforeach
	        </div>
    	@endif

		<div class="form-group w-icon">
			<input type="text" name="email" id="email" class="form-control input-lg" placeholder="Enter your email...">
			<span class="fa fa-envelope signin-form-icon"></span>
		</div> <!-- / Username -->

		<div class="form-actions">
			<input type="submit" value="SEND PASSWORD RESET LINK" class="signin-btn bg-primary">
			<a href="{{ action('\App\Modules\Auth\Http\Controllers\AuthController@getLogin') }}" class="forgot-password">Sign In</a>
		</div> <!-- / .form-actions -->
	</form>
@stop