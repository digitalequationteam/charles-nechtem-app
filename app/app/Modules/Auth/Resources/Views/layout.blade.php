<?php
$site_settings = \Common::getAdminUser()->first();

if (SITE_ENVIRONMENT == "company") {
	$agency = \User::where("id", AGENCY_ID)->get()->first();
}
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>{{Config::get('app.name')}}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<!-- Open Sans font from Google CDN -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

	<!-- Pixel Admin's stylesheets -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/css/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="/css/widgets.min.css" rel="stylesheet" type="text/css">
	<link href="/css/pages.min.css" rel="stylesheet" type="text/css">
	<link href="/css/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="/css/themes.min.css" rel="stylesheet" type="text/css">
	<link href="/css/custom.css" rel="stylesheet" type="text/css">

	<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">


	<!--[if lt IE 9]>
		<script src="assets/javascripts/ie.min.js"></script>
	<![endif]-->


<!-- $DEMO =========================================================================================

	Remove this section on production
-->
	<style>
		#signin-demo {
			position: fixed;
			right: 0;
			bottom: 0;
			z-index: 10000;
			background: rgba(0,0,0,.6);
			padding: 6px;
			border-radius: 3px;
		}
		#signin-demo img { cursor: pointer; height: 40px; }
		#signin-demo img:hover { opacity: .5; }
		#signin-demo div {
			color: #fff;
			font-size: 10px;
			font-weight: 600;
			padding-bottom: 6px;
		}
	</style>
<!-- / $DEMO -->

</head>


<!-- 1. $BODY ======================================================================================
	
	Body

	Classes:
	* 'theme-{THEME NAME}'
	* 'right-to-left'     - Sets text direction to right-to-left
-->
<body class="theme-default page-signin">

<script>
	var init = [];
	init.push(function () {
		var $div = $('<div id="signin-demo" class="hidden-xs"><div>PAGE BACKGROUND</div></div>'),
		    bgs  = [ '/images/signin-bg-1.jpg' ];
		for (var i=0, l=bgs.length; i < l; i++) $div.append($('<img src="' + bgs[i] + '">'));
		$div.find('img').click(function () {
			var img = new Image();
			img.onload = function () {
				$('#page-signin-bg > img').attr('src', img.src);
				$(window).resize();
			}
			img.src = $(this).attr('src');
		});
		$('body').append($div);
	});
</script>

<?php if (!empty($agency->theme_settings)) { ?>
	<script type="text/javascript">
		<?php
		$agency->theme_settings = str_replace("demo_", "", $agency->theme_settings);
		$agency->theme_settings = str_replace('"1"', "true", $agency->theme_settings);
		$agency->theme_settings = str_replace('"0"', "false", $agency->theme_settings);
		?>
		var main_demo_settings = <?php echo $agency->theme_settings; ?>;
	</script>
<?php }
	elseif (!empty($site_settings->theme_settings)) { ?>
	<script type="text/javascript">
		<?php
		$site_settings->theme_settings = str_replace("demo_", "", $site_settings->theme_settings);
		$site_settings->theme_settings = str_replace('"1"', "true", $site_settings->theme_settings);
		$site_settings->theme_settings = str_replace('"0"', "false", $site_settings->theme_settings);
		?>
		var main_demo_settings = <?php echo $site_settings->theme_settings; ?>;
	</script>
<?php } ?>

<!-- Demo script --> <script src="/js/demo.js"></script> <!-- / Demo script -->

	<!-- Page background -->
	<div id="page-signin-bg">
		<!-- Background overlay -->
		<div class="overlay"></div>
		<!-- Replace this with your bg image -->
		<?php if (!empty($agency->login_background)) { ?>
			<img src="/uploads/{{$agency->login_background}}" alt="">
		<?php } elseif (!empty(\Common::getAdminUser()->login_background)) { ?>
			<img src="/uploads/{{\Common::getAdminUser()->login_background}}" alt="">
		<?php } else { ?>
			<img src="/images/signin-bg-1.jpg" alt="">
		<?php } ?>
	</div>
	<!-- / Page background -->

	<!-- Container -->
	<div class="signin-container" @if (@$register) style="width: auto;"@endif>

		<!-- Left side -->
		<div class="signin-info" style="display: block; width: auto; background: #fff; border-bottom: solid 1px #eaeaea; background: #f7f7f7;">
			
			<a href="/" class="logo" style="color: #777;">
				<?php
				switch (SITE_ENVIRONMENT) {
					case 'admin':
					?>
						<?php if (!empty(\Common::getAdminUser()->logo)) { ?>
							<img src="/uploads/{{\Common::getAdminUser()->logo}}" style="max-height: 42px;" />
						<?php } else { ?>
							{{Config::get("app.name")}}
						<?php } ?>
					<?php
					break;
					case 'agency':
					?>
						<?php if (!empty(\Common::getAdminUser()->logo)) { ?>
							<img src="/uploads/{{\Common::getAdminUser()->logo}}" style="max-height: 42px;" />
						<?php } else { ?>
							{{Config::get("app.name")}}
						<?php } ?>
					<?php
					break;
					case 'company':
					case 'location':
					?>
						<?php if (!empty($agency->logo)) { ?>
							<img src="/uploads/{{$agency->logo}}" style="max-height: 42px;" />
						<?php }
							elseif (!empty(\Common::getAdminUser()->logo)) { ?>
							<img src="/uploads/{{\Common::getAdminUser()->logo}}" style="max-height: 42px;" />
						<?php } else { ?>
							{{Config::get("app.name")}}
						<?php } ?>
					<?php
					break;
				}
				?>
			</a> <!-- / .logo -->
		</div>
		<!-- / Left side -->

		<!-- Right side -->
		<div class="signin-form" style="display: block; width: auto;">

			@yield('content')

		</div>
		<!-- Right side -->
	</div>
	<!-- / Container -->

	<div class="not-a-member">
		2015 © {{\Config::get("app.name")}}
	</div>

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/pixel-admin.min.js"></script>

<script type="text/javascript">
	// Resize BG
	init.push(function () {
		var $ph  = $('#page-signin-bg'),
		    $img = $ph.find('> img');

		$(window).on('resize', function () {
			$img.attr('style', '');
			if ($img.height() < $ph.height()) {
				$img.css({
					height: '100%',
					width: 'auto'
				});
			}
		});
	});

	window.PixelAdmin.start(init);
</script>

<script src="/js/custom.js"></script>


</body>
</html>
