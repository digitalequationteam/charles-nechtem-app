@extends('auth::layout')
@section('content')
	<form action="{{ action('\App\Modules\Auth\Http\Controllers\AuthController@postResetPassword') }}" method="post" id="password-reset-form_id">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<input type="hidden" name="key" value="{{{ $key }}}" />
		<div class="signin-text">
			<span>Reset Your Password</span>
		</div> <!-- / .signin-text -->

		@if(Session::has('error'))
	        <div class="alert alert-danger">
	                <button type="button" class="close" data-dismiss="alert">×</button>
	                <span>
	                 {{ Session::get('error') }}
	            </span>
	        </div>
	    @endif

		@if(Session::has('message'))
	        <div class="alert alert-success">
	            <button type="button" class="close" data-dismiss="alert">×</button>
	            <span>
	                {{ Session::get('message') }}
	            </span>
		    </div>
	    @endif

	    @if($errors->all())
	        <div class="alert alert-danger">
	            <button type="button" class="close" data-dismiss="alert">×</button>
	            @foreach($errors->all() as $error_msg)
	        
	                <span>
	                 {{ $error_msg }}
	                </span><br/>
	            @endforeach
	        </div>
    	@endif


		<div class="form-group w-icon">
			<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Enter new password">
			<span class="fa fa-lock signin-form-icon"></span>
		</div>


		<div class="form-group w-icon">
			<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Password Confirmation">
			<span class="fa fa-lock signin-form-icon"></span>
		</div>

		<div class="form-actions">
			<input type="submit" value="RESET" class="signin-btn bg-primary">
			<a href="{{ action('\App\Modules\Auth\Http\Controllers\AuthController@getLogin') }}" class="forgot-password">Sign In</a>
		</div> <!-- / .form-actions -->
	</form>
@stop