@extends('auth::layout')
@section('content')
	<form action="{{ action('\App\Modules\Auth\Http\Controllers\AuthController@postLogin') }}" method="post" id="signin-form_id">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<div class="signin-text">
			<span>
				<?php
				switch (SITE_ENVIRONMENT) {
					case 'admin':
					?>
						Sign In to Administration
					<?php
					break;
					case 'agency':
					?>
						Sign In to Your Agency Account
					<?php
					break;
					case 'company':
					case 'location':
					?>
						Sign In to Your Account
					<?php
					break;
				}
				?>
			</span>
		</div> <!-- / .signin-text -->

		@if(Session::has('error'))
	        <div class="alert alert-danger">
	                <button type="button" class="close" data-dismiss="alert">×</button>
	                <span>
	                 {{ Session::get('error') }}
	            </span>
	        </div>
	    @endif

		@if(Session::has('message'))
	        <div class="alert alert-success">
	            <button type="button" class="close" data-dismiss="alert">×</button>
	            <span>
	                {{ Session::get('message') }}
	            </span>
		    </div>
	    @endif

	    @if($errors->all())
	        <div class="alert alert-danger">
	            <button type="button" class="close" data-dismiss="alert">×</button>
	            @foreach($errors->all() as $error_msg)
	        
	                <span>
	                 {{ $error_msg }}
	                </span><br/>
	            @endforeach
	        </div>
		@endif

		<div class="form-group w-icon">
			<input type="text" name="email" id="username_id" class="form-control input-lg" placeholder="Username or email">
			<span class="fa fa-user signin-form-icon"></span>
		</div> <!-- / Username -->

		<div class="form-group w-icon">
			<input type="password" name="password" id="password_id" class="form-control input-lg" placeholder="Password">
			<span class="fa fa-lock signin-form-icon"></span>
		</div> <!-- / Password -->

		<div class="form-actions">
			<input type="submit" value="SIGN IN" class="signin-btn bg-primary">
			<a href="{{ action('\App\Modules\Auth\Http\Controllers\AuthController@getForgotPassword') }}" class="forgot-password">Forgot your password?</a>
		</div> <!-- / .form-actions -->
	</form>
@stop