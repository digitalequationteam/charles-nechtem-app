<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function($table)
        {
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->string('type', 20); // admin or user
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('initials', 255);
            $table->string('phone', 255)->default('');
            $table->string('cell_phone', 255)->default('');
            $table->string('email', 255);
            $table->string('password', 255);
            $table->string('remember_token', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
