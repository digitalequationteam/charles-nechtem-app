<?php
namespace App\Modules\Auth\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AuthDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		//Create admin account if it doesn't exist.
        $admin = \User::where("type", "admin")->get()->first();
		if (!$admin) {
			$admin = new \User;
			$admin->type = "admin";
		    $admin->first_name = "Super";
		    $admin->last_name = "Admin";
		    $admin->email = "super_admin@".Config::get("app.domain");
		    $admin->password = \Hash::make("super_admin");
		    $admin->save();
		}
	}

}
