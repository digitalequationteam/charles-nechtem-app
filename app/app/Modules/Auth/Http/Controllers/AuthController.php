<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{
	public function getIndex() {
		return $this->getLogin();
	}

    public function getLogin($type = null)
    {
    	if (\Auth::check() ) {
    		if (\Auth::user()->type == "admin")
				return redirect('/');
		}

        return view('auth::login', array('type' => $type));
    }

    public function postLogin() {
		$attempt = \Auth::attempt( array( 'email' => \Input::get( 'email' ), 'password' => \Input::get( 'password'), 'type' => SITE_ENVIRONMENT ));

		if (!$attempt)
			$attempt = \Auth::attempt( array( 'email' => \Input::get( 'email' ), 'password' => \Input::get( 'password'), 'type' => "user" ));

		if ($attempt) {
			\SecurityLog::addNew("login", \Auth::user()->getName()." logged in.");

			return redirect("/");
		} else {
			$user = \User::where("email", \Input::get( 'email' ))->get()->first();
			if ($user)
				\SecurityLog::addNew("login_fail", $user->getName()." failed to login.", $user);
            else
                \SecurityLog::addNew("login_fail", "IP: ".$_SERVER["REMOTE_ADDR"]." failed to login.", $user);

			return Redirect::action( '\App\Modules\Auth\Http\Controllers\AuthController@getLogin' )
					->withErrors( 'Your username/password combination was incorrect' )
					->withInput();
		}
	}

    public function getLogout()
    {
        \Auth::logout();

        return Redirect::action( '\App\Modules\Auth\Http\Controllers\AuthController@getLogin' );
    }

    public function getUnsubscribe($email) {
    	$opt_out = new OptOut;
    	$opt_out->email = $email;
    	$opt_out->save();

    	\SecurityLog::addNew("email_optout", $email." opted out.");

    	echo "You have been successfully opted out. Thank you.";
    }

    public function getForgotPassword()
    {
        return view('auth::forgot');
    }

    public function postForgotPassword()
    {
    	if (\Auth::check() ) {
			return redirect(\Auth::user()->type.'/');
		}

		$user = \User::where("email", \Input::get("email"))->get()->first();

		if (!$user)
			return Redirect::back()->withErrors("Email not found.")->withInput();

		if ($user->type == "admin") {
			$agency = $user;
			$login_url = \URL::to("/auth/reset-password/".base64_encode($user->email."|SEPARATOR|".$user->password));
		}

		$data = array(
			"first_name" => $user->first_name,
			"last_name" => $user->last_name,
			"email" => $user->email,
			"link" => $login_url,
			"signature" => \Config::get("app.name")
		);

		\Mail::send('emails.reset_password', $data, function($message) use ($user, $agency) {
		    $message->to($user->email, $user->first_name." ".$user->last_name)->from($agency->email, $agency->first_name)->subject("Reset your password.");
		});	

        return Redirect::action( '\App\Modules\Auth\Http\Controllers\AuthController@getForgotPassword' )->with( 'message', 'Please check your email!' );
    }

    public function getResetPassword($key)
    {
    	$original_key = $key;
    	$key = base64_decode($key);
    	$exploded = explode("|SEPARATOR|", $key);
    	$email = @$exploded[0];
    	$password = @$exploded[1];

    	$user = \User::where("email", $email)->where("password", $password)->get()->first();

    	if (!$user)
    		return Redirect::action( '\App\Modules\Auth\Http\Controllers\AuthController@getForgotPassword' )->withErrors( 'URL not valid!' );

        return view('auth::reset', array("user" => $user, "key" => $original_key));
    }

    public function postResetPassword()
    {
    	$key = \Input::get("key");
    	$key = base64_decode($key);
    	$exploded = explode("|SEPARATOR|", $key);
    	$email = @$exploded[0];
    	$password = @$exploded[1];

    	$user = \User::where("email", $email)->where("password", $password)->get()->first();

    	if (!$user)
    		return Redirect::action( '\App\Modules\Auth\Http\Controllers\AuthController@getForgotPassword' )->withErrors( 'URL not valid!' );

		$validator = Validator::make( \Input::all(), \User::$resetPasswordRules);
		
		if ( $validator->passes() ) {
			$user->password = \Hash::make( \Input::get( 'password' ) );
			$user->save();

			\SecurityLog::addNew("password_reset", $user->getName()." reset password.", $user);

			return Redirect::action( '\App\Modules\Auth\Http\Controllers\AuthController@getLogin' )->with( 'message', 'Password has been reset. Please login.' );
		}

		return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postSendSupportEmail() {
    	if (SITE_ENVIRONMENT == "agency")
        	$parent = Common::getAdminUser();
        else
        	$parent = \User::where("id", \Auth::user()->parent_id)->get()->first();

        $data = array("content" => \Input::get("message"));

        Mail::send('emails.empty', $data, function($message) use ($parent) {
            $emails = explode(",", $parent->reputation_pro_email);
            foreach ($emails as $email_to) {
                $email_to = trim($email_to);
                $message->to($email_to, "")->from(\Auth::user()->email, \Auth::user()->first_name." ".\Auth::user()->last_name)->subject(\Input::get("subject"));
            }
        });

        return Redirect::back()->with( 'message', 'Email Sent!' );
    }
}
