<?php

namespace App\Modules\Auth\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use MicheleAngioni\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    use EntrustUserTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    public static $resetPasswordRules = array(
        'password'=>'required|between:6,12|confirmed',
        'password_confirmation'=>'required|between:6,12',
    );

    public function Owner() {
        if ($this->type == "user")
            return $this->parent()->get()->first();
        else
            return $this;
    }

    public function parent() {
        return $this->belongsTo( '\User', 'parent_id' );
    }

    public function children() {
        return $this->hasMany( '\User', 'parent_id' );
    }

    public function users() {
        return $this->children()->where("type", "user");
    }

    public function my_roles() {
        return $this->hasMany('\Role');
    }

    public function login_sessions() {
        return $this->hasMany('\SecurityLog')->where("type", "login");
    }

    public function cases() {
        return $this->hasMany( '\ECase', 'creator_id' );
    }

    public function hasPermission($permission_name) {
        if (!$this->getMyRole()->hasPermission($permission_name))
            return false;

        return $this->perms()->where("name", $permission_name)->count() > 0;
    }

    public function getSubUsers($logged_in_user) {
        $sub_users = $this->children()
                    ->where("type", "user")
                    ->orderBy('id', 'ASC')
                    ->get();

        foreach ($sub_users as $key => $sub_user) {
            if ($sub_user->getMyLevel() <= $logged_in_user->getMyLevel())
                unset($sub_users[$key]);
        }

        return $sub_users;
    }

    public function getName() {
        return $this->first_name." ".$this->last_name;
    }

    public function getSubRoles($logged_in_user) {
        return $this->my_roles()->where("level", ">", $logged_in_user->getMyLevel())->orderBy("level", "ASC")->get();
    }

    public function getMyRole() {
        return $this->roles()->get()->first();
    }

    public function getMyLevel() {
        return $this->getMyRole()->level;
    }

    public function getSetting($key) {
        return \Setting::get($this->id.".".$key);
    }

    public function saveSetting($key, $value) {
        \Setting::set($this->id.".".$key, $value);
        \Setting::save();
    }

    public function getSettings() {
        return \Setting::get($this->id);
    }

    public function checkWebsiteCreated($force_new = false, $force_folder = null) {
        $site_folder = ($force_folder) ? $force_folder : $this->getSiteFolder();

        $exploded = explode("/app", __FILE__);
        $folder = $exploded[0]."/public/";

        if ($force_new) {
            function rrmdir($dir) { 
               if (is_dir($dir)) { 
                 $objects = scandir($dir); 
                 foreach ($objects as $object) { 
                   if ($object != "." && $object != "..") { 
                     if (is_dir($dir."/".$object))
                       rrmdir($dir."/".$object);
                     else
                       unlink($dir."/".$object); 
                   } 
                 }
                 rmdir($dir); 
               } 
             }
            if (is_dir($folder.'static_front_sites/'.$site_folder.'/')) {
                rrmdir($folder.'static_front_sites/'.$site_folder.'/');

                $db = new \PDO('mysql:host=localhost;dbname=charles_nechtem_wp;charset=utf8', 'root', 'born4biz');
                $db = \DB::connection()->getPdo()->exec( 'DROP DATABASE IF EXISTS charles_nechtem_wp');
            }
        }

        if (!is_dir($folder.'static_front_sites/'.$site_folder.'/')) {
            $wordpress_data = array();

            $wordpress_data["external_home_url"] = $this->getSiteURL();
            $wordpress_data["external_admin_url"] = $this->getSiteURL().'/static_front_sites/'.$site_folder;
            $wordpress_data["username"] = "cna-admin";

            $wordpress_data["password"] = "GoBig@2015";

            if (!$force_folder) {
                $this->wordpress_data = json_encode($wordpress_data);
                $this->save();
            }

            mkdir($folder.'static_front_sites/'.$site_folder.'/', 0755);

            $this->recurse_copy($folder.'static_front_sites/default/', $folder.'static_front_sites/'.$site_folder.'/');

            $db = \DB::connection()->getPdo()->exec( 'CREATE DATABASE IF NOT EXISTS charles_nechtem_wp');

            function chmod_r($dir, $dirPermissions, $filePermissions) {
                  chmod($dir, $dirPermissions);
                  $dp = opendir($dir);
                   while($file = readdir($dp)) {
                     if (($file == ".") || ($file == ".."))
                        continue;

                    $fullPath = $dir."/".$file;

                     if(is_dir($fullPath)) {
                        //echo('DIR:' . $fullPath . "\n");
                        chmod($fullPath, $dirPermissions);
                        chmod_r($fullPath, $dirPermissions, $filePermissions);
                     } else {
                        //echo('FILE:' . $fullPath . "\n");
                        chmod($fullPath, $filePermissions);
                     }

                   }
                 closedir($dp);
            }

            chmod_r($folder.'static_front_sites/'.$site_folder."/wp-content/uploads/", 0777, 0777);

            $filename = $folder.'static_front_sites/'.$site_folder.'/dump.sql';

            $db = new \PDO('mysql:host=localhost;dbname=charles_nechtem_wp;charset=utf8', 'root', 'born4biz');

            // Temporary variable, used to store current query
            $templine = '';
            // Read in entire file
            $lines = file($filename);
            // Loop through each line
            foreach ($lines as $line)
            {
            // Skip it if it's a comment
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;

            // Add this line to the current segment
            $templine .= $line;
            // If it has a semicolon at the end, it's the end of the query
            if (substr(trim($line), -1, 1) == ';')
            {
                $db->query($templine);
                $templine = '';
            }
            }

            if ($this->type == "admin")
                $this->domain = \Config::get("app.domain");
            
            $stmt = $db->prepare("SELECT * FROM wp_options");
            $stmt->execute(array());
            $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($rows as $row) {
                $array = @unserialize($row["option_value"]);
                if ($array) {
                    if ($row["option_name"] == "theme_mods_altitude-pro") {
                        if (isset($array["header_image"]))
                            unset($array["header_image"]);

                        if (isset($array["header_image_data"]))
                            unset($array["header_image_data"]);
                    }
                    $row["option_value"] = serialize($array);
                }
                else {
                    if ($row["option_name"] == "siteurl" || $row["option_name"] == "home")
                        $row["option_value"] = "http://".$this->domain."/static_front_sites/".$site_folder."/";
                }

                //var_dump($row);

                $stmt2 = $db->prepare("UPDATE wp_options SET option_value = ? WHERE option_id = ?");
                $stmt2->execute(array($row["option_value"], $row["option_id"]));
            }

            $stmt = $db->prepare("SELECT * FROM wp_posts");
            $stmt->execute(array());
            $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($rows as $row) {
                $row["guid"] = str_replace("{{SITE_URL}}", "http://".$this->domain."/static_front_sites/".$site_folder."/", $row["guid"]);

                //var_dump($row);

                $stmt2 = $db->prepare("UPDATE wp_posts SET guid = ? WHERE ID = ?");
                $stmt2->execute(array($row["guid"], $row["ID"]));
            }
        }
    }

    public function getSiteFolder() {
        return ($this->type == "admin") ? "global" : $this->username;
    }

    public function getSiteURL() {
        switch ($this->type) {
            case 'admin':
                return \Config::get("app.url");
            break;
            default:
                return "http://".$this->domain."/";
            break;
        }
    }

    function recurse_copy($src,$dst) {
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
                } 
                else { 
                    copy($src . '/' . $file,$dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    }

    public function getWordpressData() {
        return (array)json_decode($this->wordpress_data, true);
    }

    public static function generateUniqueInitials($initials) {
        $find = \User::where("initials", $initials)->count();

        if ($find == 0)
            return $initials;
        else {
            $new_initials = substr($initials, 0, 2).rand(1,9);
            return \User::generateUniqueInitials($new_initials);
        }
    }
}