<?php

namespace App\Modules\Security\Models;

use Illuminate\Database\Eloquent\Model;

class IpToAddress extends Model
{
    protected $table = 'ip_to_address';

    public function log() {
        return $this->hasMany( '\SecurityLog', "IP", "IP");
    }

    public static function saveAddressForIP($ip) {
    	$ip_to_address = \IpToAddress::where("ip", $ip)->get()->first();
    	if (!$ip_to_address) {
    		$ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, "https://freegeoip.net/json/".$ip);
		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    $output = curl_exec($ch);
		    curl_close($ch);

		    $response = json_decode($output);

		    $ip_to_address = new \IpToAddress;
	    	$ip_to_address->IP = $ip;
            $ip_to_address->country_code = @$response->country_code;
            $ip_to_address->country_name = @$response->country_name;
            $ip_to_address->region_code = @$response->region_code;
            $ip_to_address->region_name = @$response->region_name;
            $ip_to_address->city = @$response->city;
            $ip_to_address->zip_code = @$response->zip_code;
            $ip_to_address->time_zone = @$response->time_zone;
            $ip_to_address->latitude = @$response->latitude;
            $ip_to_address->longitude = @$response->longitude;
            $ip_to_address->metro_code = @$response->metro_code;
	    	$ip_to_address->save();
    	}
    }

    public function getAddress() {
    	$elements = array();

    	if (!empty($this->city)) $elements[] = $this->city;
    	if (!empty($this->region_code)) $elements[] = $this->region_code;
    	if (!empty($this->zip)) $elements[] = $this->zip;
    	if (!empty($this->country_name)) $elements[] = $this->country_name;

    	return implode(", ", $elements);
    }
}