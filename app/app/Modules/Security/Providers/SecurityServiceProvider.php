<?php
namespace App\Modules\Security\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class SecurityServiceProvider extends ServiceProvider
{
	/**
	 * Register the Security module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Security\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Security module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('security', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('security', base_path('resources/views/vendor/security'));
		View::addNamespace('security', realpath(__DIR__.'/../Resources/Views'));
	}
}
