<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecurityLogTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('security_log', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('type', 20); // login, client_add, etc...
            $table->string('IP', 255);
            $table->string('message', 255);
            $table->datetime('last_activity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('security_log');
    }
}