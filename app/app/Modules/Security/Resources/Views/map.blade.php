@extends('layouts.main')
@section('content')
	<ol class="breadcrumb">
		<li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
		<li><a href="/security/map">Security</a></li>
		<li class="active">Map</li>
	</ol>
	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Login Map</strong></h3>
	</div> <!-- / .page-header -->

	<div class="clearfix"></div>

	<div class="row users">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
		                    <div id="map" style="width: 100%; height: 750px;"></div>
		                </div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

	<style type="text/css">
		a[href^="http://maps.google.com/maps"]{display:none !important}
		a[href^="https://maps.google.com/maps"]{display:none !important}

		.gmnoprint a, .gmnoprint span, .gm-style-cc {
		    display:none;
		}

		@if ($wide)
			#wrapper {
				width: 100% !important;
			}
		@endif
	</style>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZWqW7aJrwxQ4MeN10zXxap5a3lHl4iAI&sensor=true"></script>
    <script src="https://google-maps-utility-library-v3.googlecode.com/svn-history/r354/trunk/markerclustererplus/src/markerclusterer_packed.js"></script>

    <script>
        jQuery(document).ready(function () {
            var map;
            var locations = [];

            var openIW;


             var customMapType = new google.maps.StyledMapType([
			      {
			        elementType: 'labels',
			        stylers: [{"hue":"#0066ff"},{"saturation":74},{"lightness":100}],
			      }
			    ], {
			      name: 'Custom Style'
			  });
			  var customMapTypeId = 'custom_style';

            var centerPosition = new google.maps.LatLng(38.850033, {{($wide) ? -17.6500523 : -97.6500523}});
            var markers = [];

            var options = {
                zoom: {{($wide) ? 3 : 5}},
                center: centerPosition,
                styles: [
	                {"featureType":"water","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},
	                {"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},
	                {"featureType":"road","stylers":[{"saturation":-100},
	                {"lightness":45}]},
	                {"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},
	                {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
	                {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
	                {"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},
	                {"featureType":"administrative", "elementType": "labels","stylers":[{"visibility":"off"}]}
                ],
				 mapTypeId: google.maps.MapTypeId.ROADMAP,
				 mapTypeControl: false
            };

            map = new google.maps.Map($('#map')[0], options);

            //map.mapTypes.set(customMapTypeId, customMapType);
  			//map.setMapTypeId(customMapTypeId);

  			@foreach ($last_24_sessions as $key => $session)
	  			<?php
	  			$ipToAddress = $session->ipToAddress()->get()->first();
	  			$user = $session->user()->get()->first();

	  			$color = "grey";

	  			if ($session->type == "login_fail")
	  				$color = "red";
	  			elseif ($session->getStatus() == "online")
	  				$color = "green";
	  			elseif (time() - strtotime($session->last_activity) >= 900)
	  				$color = "yellow";
	  			?>

                var latLng = new google.maps.LatLng({{$ipToAddress->latitude}}, {{$ipToAddress->longitude}});
                var marker_{{$key}} = new google.maps.Marker({
                    position: latLng,
                    icon: {
                    	url: "{{URL::to('/')}}/images/icons/user-{{$color}}.svg",
                    	fill: "#FF0000"
                    }
                });

				  var infowindow_{{$key}} = new google.maps.InfoWindow({
				    content: '<strong>{{$user->first_name}} {{$user->last_name}}</strong><br />\
				    	Location: {{$ipToAddress->getAddress()}}<br />\
				    	@if ($session->type != "login_fail") Connected for: {{$session->getConnectedFor()}} @endif'
				  });
				  marker_{{$key}}.addListener('click', function() {
				  	if (typeof(openIW) != "undefined")
				  		openIW.close();

				    infowindow_{{$key}}.open(map, marker_{{$key}});
				    openIW = infowindow_{{$key}};
				  });
                markers.push(marker_{{$key}});
            @endforeach

            var markerCluster = new MarkerClusterer(map, markers, {
                averageCenter: true
            });

            google.maps.event.addListener(markerCluster, "click", function (cluster) {
                console.log("Cluster click");
            });

        });
    </script>

@stop