@extends('layouts.main')
@section('content')
	<ol class="breadcrumb">
		<li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
		<li><a href="/security/log">Security</a></li>
		<li class="active">Log</li>
	</ol>
	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Security Log</strong></h3>
	</div> <!-- / .page-header -->

	<div class="clearfix"></div>

	<div class="row users">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
		                    <table id="users-table" class="table table-tools table-striped">
		                        <thead>
		                            <tr>
		                                <th>User</th>
		                                <th>IP</th>
		                                <th style="width: 350px;">Action</th>
		                                <th>Date</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<?php
									foreach ($log as $log_item) { ?>
										<tr class="odd gradeX">
											<td>
												<?php
												$user = $log_item->user()->get()->first();
												?>
												@if ($user)
													{{$log_item->user()->get()->first()->first_name}}
													{{$log_item->user()->get()->first()->last_name}}
												@else
													Anonymous
												@endif
											</td>
											<td>
												{{$log_item->IP}}
											</td>
											<td>
												{!! $log_item->message !!}
											</td>
											<td>
												{{$log_item->created_at}}
											</td>
										</tr>
									<?php 
										}
									?>
		                        </tbody>
		                    </table>

		                    @if ($log->count() == 0)
		                    	No users found.
		                    @endif

		                    <div class="pull-right">
		                    	{!! $log_paginator->render() !!}
		                    </div>
		                </div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
@stop