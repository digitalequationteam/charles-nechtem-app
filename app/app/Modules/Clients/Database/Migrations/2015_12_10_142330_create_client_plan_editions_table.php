<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientPlanEditionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('client_plan_editions', function($table)
        {
            $table->increments('id');
            $table->integer('client_plan_id')->unsigned();
            $table->foreign('client_plan_id')->references('id')->on('client_plans')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('year')->default(0);

            //A-Type
            $table->decimal('inin_copay_day', 10, 2)->nullable();
            $table->decimal('inin_copay_admission', 10, 2)->nullable();
            $table->decimal('inin_php_copay_day', 10, 2)->nullable();
            $table->decimal('inin_php_copay_admission', 10, 2)->nullable();
            $table->decimal('inou_copay', 10, 2)->nullable();
            $table->decimal('inouopm_individual', 10, 2)->nullable();
            $table->decimal('inouopm_family', 10, 2)->nullable();
            $table->decimal('inot_copay', 10, 2)->nullable();
            $table->decimal('onde_individual', 10, 2)->nullable();
            $table->decimal('onde_family', 10, 2)->nullable();
            $table->decimal('onin_copay', 10, 2)->nullable();
            $table->decimal('onin_php_copay', 10, 2)->nullable();
            $table->decimal('onou_copay', 10, 2)->nullable();
            $table->decimal('onouopm_individual', 10, 2)->nullable();
            $table->decimal('onouopm_family', 10, 2)->nullable();
            $table->decimal('onot_copay', 10, 2)->nullable();

            //B-Type
            $table->decimal('incp_copay', 10, 2)->nullable();
            $table->decimal('inde_single', 10, 2)->nullable();
            $table->decimal('inde_employee_1', 10, 2)->nullable();
            $table->decimal('inde_employee_2', 10, 2)->nullable();
            $table->decimal('inou_single', 10, 2)->nullable();
            $table->decimal('inou_employee_1', 10, 2)->nullable();
            $table->decimal('inou_employee_2', 10, 2)->nullable();
            $table->decimal('oncp_copay', 10, 2)->nullable();
            $table->decimal('onde_single', 10, 2)->nullable();
            $table->decimal('onde_employee_1', 10, 2)->nullable();
            $table->decimal('onde_employee_2', 10, 2)->nullable();
            $table->decimal('onou_single', 10, 2)->nullable();
            $table->decimal('onou_employee_1', 10, 2)->nullable();
            $table->decimal('onou_employee_2', 10, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_plan_editions');
    }
}