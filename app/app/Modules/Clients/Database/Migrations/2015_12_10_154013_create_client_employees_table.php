<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientEmployeesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('client_employees', function($table)
        {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('parent_id')->default(0);

            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('gender', 255);
            $table->string('cell_phone', 255)->nullable();
            $table->string('home_phone', 255)->nullable();
            $table->string('work_phone', 255)->nullable();
            $table->string('ssn', 255)->nullable();
            $table->date('birthday')->nullable();

            $table->string('street', 255);
            $table->string('city', 255);
            $table->string('country', 255);
            $table->string('state', 255);
            $table->string('zip', 255);

            $table->integer('client_plan_id')->nullable();
            $table->integer('insurance_network_id')->nullable();
            $table->string('insurance_type', 255);
            $table->date('plan_coverage_termination_date')->nullable();

            $table->string('work_department')->nullable();

            $table->string('dependent_status', 255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_employees');
    }
}