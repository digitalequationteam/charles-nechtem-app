<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('clients', function($table)
        {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('tax_id', 255)->nullable();
            $table->string('memo', 1000)->nullable();

            $table->string('primary_street', 255);
            $table->string('primary_city', 255);
            $table->string('primary_country', 255);
            $table->string('primary_state', 255);
            $table->string('primary_zip', 255);

            $table->boolean('secondary_address_available')->default(0);
            $table->string('secondary_street', 255)->nullable();
            $table->string('secondary_city', 255)->nullable();
            $table->string('secondary_country', 255)->nullable();
            $table->string('secondary_state', 255)->nullable();
            $table->string('secondary_zip', 255)->nullable();

            $table->boolean('mailing_same_as_primary')->default(1);
            $table->string('mailing_street', 255)->nullable();
            $table->string('mailing_city', 255)->nullable();
            $table->string('mailing_country', 255)->nullable();
            $table->string('mailing_state', 255)->nullable();
            $table->string('mailing_zip', 255)->nullable();

            $table->string('contract_type', 255);
            $table->string('contract_type_custom', 255);
            $table->date('contract_period_from')->nullable();
            $table->date('contract_period_to')->nullable();
            $table->date('renewal_period_from')->nullable();
            $table->date('renewal_period_to')->nullable();
            $table->integer('employee_head_count')->nullable();
            $table->string('invoice_type', 255)->nullable();
            $table->string('employee_fee', 255)->nullable();

            $table->string('status', 20)->nullable(); //active/inactive/cancelled/archived
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}