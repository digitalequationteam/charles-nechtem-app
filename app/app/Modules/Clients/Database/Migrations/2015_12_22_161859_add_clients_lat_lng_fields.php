<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientsLatLngFields extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('clients',function($table){
            $table->string('lat', 100)->default('');
            $table->string('lng', 100)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients',function($table){
            $table->dropColumn('lat');
            $table->dropColumn('lng');
        });
    }
}