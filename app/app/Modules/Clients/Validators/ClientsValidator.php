<?php

namespace App\Modules\Clients\Validators;

class ClientsValidator
{
	public static $addRules = array(
        'name'=>'required',
        'contract_type'=>'required',
        'primary_address' => 'required'
    );
}