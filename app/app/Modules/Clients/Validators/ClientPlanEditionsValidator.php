<?php

namespace App\Modules\Clients\Validators;

class ClientPlanEditionsValidator
{
    public static $aRules = array(
        "year" => "required|numeric|min:0|unique:client_plan_editions",
        "inin_copay_day" => "required|numeric|min:0",
        "inin_copay_admission" => "required|numeric|min:0",
        "inin_php_copay_day" => "required|numeric|min:0",
        "inin_php_copay_admission" => "required|numeric|min:0",
        "inou_copay" => "required|numeric|min:0",
        "inouopm_individual" => "required|numeric|min:0",
        "inouopm_family" => "required|numeric|min:0",
        "inot_copay" => "required|numeric|min:0|max:100",
        "onde_individual" => "required|numeric|min:0",
        "onde_family" => "required|numeric|min:0",
        "onin_copay" => "required|numeric|min:0|max:100",
        "onin_php_copay" => "required|numeric|min:0|max:100",
        "onou_copay" => "required|numeric|min:0|max:100",
        "onouopm_individual" => "required|numeric|min:0",
        "onouopm_family" => "required|numeric|min:0",
        "onot_copay" => "required|numeric|min:0|max:100",
    );

    public static $bRules = array(
        "year" => "required|numeric|min:0|unique:client_plan_editions",
        "incp_copay" => "required|numeric|min:0|max:100",
        "inde_single" => "required|numeric|min:0",
        "inde_employee_1" => "required|numeric|min:0",
        "inde_employee_2" => "required|numeric|min:0",
        "inou_single" => "required|numeric|min:0",
        "inou_employee_1" => "required|numeric|min:0",
        "inou_employee_2" => "required|numeric|min:0",
        "oncp_copay" => "required|numeric|min:0|max:100",
        "onde_single" => "required|numeric|min:0",
        "onde_employee_1" => "required|numeric|min:0",
        "onde_employee_2" => "required|numeric|min:0",
        "onou_single" => "required|numeric|min:0",
        "onou_employee_1" => "required|numeric|min:0",
        "onou_employee_2" => "required|numeric|min:0"
    );
}