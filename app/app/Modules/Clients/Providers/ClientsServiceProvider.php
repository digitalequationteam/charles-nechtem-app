<?php
namespace App\Modules\Clients\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ClientsServiceProvider extends ServiceProvider
{
	/**
	 * Register the Clients module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Clients\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Clients module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('clients', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('clients', base_path('resources/views/vendor/clients'));
		View::addNamespace('clients', realpath(__DIR__.'/../Resources/Views'));
	}
}
