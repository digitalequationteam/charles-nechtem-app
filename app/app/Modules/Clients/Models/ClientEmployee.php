<?php

namespace App\Modules\Clients\Models;

use Illuminate\Database\Eloquent\Model;

class ClientEmployee extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_employees';

    public function client() {
    	return $this->belongsTo("\Client");
    }

    public function parent() {
    	return $this->belongsTo("\ClientEmployee", "parent_id");
    }

    public function dependents() {
        return $this->hasMany("\ClientEmployee", "parent_id");
    }

    public function plan() {
    	return $this->belongsTo("\ClientPlan", "client_plan_id");
    }

    public function insurance_network() {
    	return $this->belongsTo("\DictionaryValue", "insurance_network_id");
    }

    public function getPrimaryAddress($format = 1) {
    	if ($format == 1)
    		return $this->street.", ".$this->city.", ".$this->state.", ".$this->zip;
    	elseif ($format == 2)
    		return $this->street."<br />".$this->city.", ".$this->state.", ".$this->zip."<br />".\Common::getCountryName($this->country);
    }

    public function saveEverywhere() {
        $cases = \App\Modules\Cases\Models\ECase::where("enrollee_data", 'LIKE', '%{"enrollee":{"id":'.$this->id.'%')
                 ->orWhere(function($q) {
                    return $q->where("enrollee_data", 'LIKE', '%{"enrollee":{"first_name":"'.$this->first_name.'%"')->where("enrollee_data", 'LIKE', '%"last_name":"'.$this->last_name.'%"');
                 })->get();

        foreach ($cases as $case) {
            $current_enrollee_data = $case->getEnrolleeDataArray();
            $case->enrollee_data = json_encode(array(
                "enrollee" => array(
                    "id" => $this->id,
                    "first_name" => $this->first_name,
                    "last_name" => $this->last_name,
                    "gender" => $this->gender,
                    "ssn" => $this->ssn,
                    "cell_phone" => $this->cell_phone,
                    "home_phone" => $this->home_phone,
                    "work_phone" => $this->work_phone,
                    "birthday" => $this->birthday,
                    "plan" => @$this->plan()->get()->first()->name,
                    "insurance_network" => @$this->insurance_network()->get()->first()->name,
                    "insurance_type" => $this->insurance_type,
                    "client" => $this->client()->get()->first()->name,
                    "address" => $this->getPrimaryAddress(2)
                ),
                "employee" => array(
                    "first_name" => @$current_enrollee_data["employee"]["first_name"],
                    "last_name" => @$current_enrollee_data["employee"]["last_name"],
                    "gender" => @$current_enrollee_data["employee"]["gender"],
                    "ssn" => @$current_enrollee_data["employee"]["ssn"],
                    "cell_phone" => @$current_enrollee_data["employee"]["cell_phone"],
                    "home_phone" => @$current_enrollee_data["employee"]["home_phone"],
                    "work_phone" => @$current_enrollee_data["employee"]["work_phone"],
                    "birthday" => @$current_enrollee_data["employee"]["birthday"],
                    "plan" => @$current_enrollee_data["employee"]["plan"],
                    "insurance_network" => @$current_enrollee_data["employee"]["insurance_network"],
                    "insurance_type" => @$current_enrollee_data["employee"]["insurance_type"],
                    "client" => @$current_enrollee_data["employee"]["client"],
                    "address" => @$current_enrollee_data["employee"]["address"]
                )
            ));
            $case->save();
        }
    }
}