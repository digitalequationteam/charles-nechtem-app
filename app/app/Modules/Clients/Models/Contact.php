<?php

namespace App\Modules\Clients\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    public function client() {
        return $this->belongsTo("\Client", "owner_id");
    }

    public function provider() {
        return $this->belongsTo("\Provider", "owner_id");
    }

    public function getDisplay() {
    	$contact_info = $this->first_name." ".$this->last_name;
		if (!empty($this->title))
			$contact_info .= "<br />(".$this->title.")";

		if (!empty($this->email))
			$contact_info .= "<br />".$this->email;

		if (!empty($this->phone))
            $contact_info .= "<br />".$this->phone;

        if (!empty($this->fax))
            $contact_info .= "<br />Fax: ".$this->fax;

		return $contact_info;
    }
}