<?php

namespace App\Modules\Clients\Models;

use Illuminate\Database\Eloquent\Model;

class ClientDocument extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_documents';

    public function client() {
    	return $this->belongsTo("\Client");
    }
}