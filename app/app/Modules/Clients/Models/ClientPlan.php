<?php

namespace App\Modules\Clients\Models;

use Illuminate\Database\Eloquent\Model;

class ClientPlan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_plans';

    public function client() {
    	return $this->belongsTo("\Client");
    }

    public function editions() {
    	return $this->hasMany("\ClientPlanEdition");
    }
}