<?php

namespace App\Modules\Clients\Models;

use Illuminate\Database\Eloquent\Model;

class ClientPlanEdition extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_plan_editions';

    public function plan() {
    	return $this->belongsTo("\ClientPlan", "client_plan_id");
    }

    public function client() {
    	return $this->plan()->get()->first()->clients()->get()->first();
    }
}