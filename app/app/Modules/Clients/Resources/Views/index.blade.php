@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li class="active">Clients</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Clients</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_add"))
			<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getAddClient') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span> Register Client</a>
		@endif
	</div>

	<div class="clearfix"></div>

	<div class="row users">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
		                    <table id="clients-table" class="table table-tools table-striped">
		                        <thead>
		                            <tr>
		                                <th style="min-width:50px">
		                                	<div class="pull-left">
		                                		@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_delete"))
		                                    		<input type="checkbox" class="check_all"/>
		                                    	@endif
		                                    </div>
		                                    <div class="pull-left" style="padding-left: 30px; margin-top: -12px;">
		                                    	@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_delete"))
													<a href="#" class="btn btn-danger btn-delete btn-xs" style="margin: 0px;">Delete</a>
												@endif
											</div>
		                                </th>
		                                <th style="width: 300px;"></th>
		                                <th></th>
		                                <th class="text-right" style="width: 300px; padding-top: 6px; font-size: 12px;">
		                                	Sort:
		                                	<select id="sort-by-dd">
		                                		<option value="id" @if (\Input::get("sort_by") == "id") selected @endif>UID</option>
		                                		<option value="name" @if (\Input::get("sort_by") == "name") selected @endif>Name</option>
		                                		<option value="created_at" @if (\Input::get("sort_by") == "created_at") selected @endif>Registration Date</option>
		                                		<option value="primary_state" @if (\Input::get("sort_by") == "primary_state") selected @endif>State</option>
		                                		<option value="head_count" @if (\Input::get("sort_by") == "head_count") selected @endif>Head Count</option>
		                                	</select>

		                                	<select id="sort-type-dd">
		                                		<option value="ASC" @if (\Input::get("sort_type") == "ASC") selected @endif>Ascending</option>
		                                		<option value="DESC" @if (\Input::get("sort_type") == "DESC") selected @endif>Descending</option>
		                                	</select>
		                                </th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <tr class="bg-gray-light filters-tr">
		                                <td colspan="4" style="padding-top: 6px;">
		                                	<div class="pull-left" style="padding-left: 8px;">
		                                		<button id="toggle-filters-btn" class="btn btn-danger btn-xm" onclick="toggleFilters();"><i class="fa fa-arrow-down"></i> Search Filters</button>
		                                	</div>

		                                	<div class="clearfix"></div>

		                                	<div id="filters-wrapper" style="padding-top: 10px; padding-bottom: 10px; @if (!\Input::get('applyFilters')) display: none; @endif">
		                                		<form action="" method="get">
		                                			<input type="hidden" name="sort_by" value="{{\Input::get('sort_by')}}" />
		                                			<input type="hidden" name="sort_type" value="{{\Input::get('sort_type')}}" />
		                                			<input type="hidden" name="page_size" value="{{\Input::get('page_size')}}" />
			                                		<div class="row">
				                                		<div class="col-md-6">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Name</label>
																<div>
																	<input class="form-control" type="text" name="name" value="{{\Input::get('name')}}" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Street</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="street" id="street" value="<?php echo Input::get('street'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>City</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="city" id="city" value="<?php echo Input::get('city'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Country</label>
																<div>
																	<select class="form-control" name="country" id="country">
																		<?php foreach (\Common::getAllCountries() as $code => $name) {
																			?>
																			<option value="<?php echo $code; ?>" <?php if (\Input::get("country") == $code) { ?>selected="selected"<?php } ?>><?php echo $name; ?></option>
																			<?php
																		}
																		?>
																	</select>
																</div>
															</div>
				                                		</div>
				                                	</div>

				                                	<div class="row">
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Contract Types</label>
																<div>
																	<select name="contract_type[]" id="contract_type" class="form-control" multiple="multiple" style="width: 100%;">
																	    <option value="EAP" @if (in_array('EAP', (array)\Input::get("contract_type"))) selected @endif>EAP</option>
																	    <option value="EAP/OASIS" @if (in_array('EAP/OASIS', (array)\Input::get("contract_type"))) selected @endif>EAP/OASIS</option>
																	    <option value="OASIS" @if (in_array('OASIS', (array)\Input::get("contract_type"))) selected @endif>OASIS</option>
																	    <option value="Phone Only Supports" @if (in_array('Phone Only Supports', (array)\Input::get("contract_type"))) selected @endif>Phone Only Supports</option>
																	    <option value="Wellness Coaching" @if (in_array('Wellness Coaching', (array)\Input::get("contract_type"))) selected @endif>Wellness Coaching</option>
																	    <option value="Other" @if (in_array('Other', (array)\Input::get("contract_type"))) selected @endif>Other</option>
																	  </select>
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-3">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Status</label>
																<div>
																	<select name="statuses[]" id="status" class="form-control" multiple="multiple" style="width: 100%;">
																	    <option value="active" @if (in_array('active', (array)\Input::get("statuses")) || !isset($_GET["statuses"])) selected @endif>Active</option>
																	    <option value="cancelled" @if (in_array('cancelled', (array)\Input::get("statuses")) || !isset($_GET["statuses"])) selected @endif>Cancelled</option>
																	    <option value="archived" @if (in_array('archived', (array)\Input::get("statuses"))) selected @endif>Archived</option>
																	  </select>
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>State</label>
																<div>
																	<select class="form-control" name="state" id="state">
																		<option value="">Select state...</option>
																		<?php if (Input::get("country")) { ?>
																			<?php foreach (\Common::getStates(Input::get("country")) as $state) {
																				?>
																				<option value="<?php echo $state; ?>" <?php if ($state == Input::get("state")) { ?>selected="selected"<?php } ?>><?php echo $state; ?></option>
																				<?php
																			}
																			?>
																		<?php } ?>
																	</select>
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Zip Code</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="zip" id="zip" value="<?php echo Input::get('zip'); ?>" />
																</div>
															</div>
				                                		</div>
				                                		<div class="col-md-2">
															<div class="form-group no-margin-hr panel-padding-h">
																<label>Miles Range</label>
																<div>
																	<input type="text" class="form-control" maxlength="100" name="range" id="range" value="<?php echo Input::get('range'); ?>" />
																</div>
															</div>
				                                		</div>
				                                	</div>
				                                	<button class="btn btn-default" name="applyFilters" value="Yes">Apply Filters</button>
				                                	<button onclick="window.location = '/clients'; return false;" class="btn btn-default">Reset</button>
				                                </form>
		                                	</div>
		                                </td>
		                            </tr>
		                        	<?php
									foreach ($clients as $client) { ?>
										<tr class="odd gradeX">
											<td>
												<div class="pull-left" style="padding-top: 10px;">
													@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_delete"))
														<input type="checkbox" class="batch_id" value="{{$client->id}}" />
													@endif
												</div>
												<div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
													 <a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($client->id)) }}"><strong><?php echo $client->name; ?></strong></a>
													 <br />
													 <?php echo $client->getAllAddresses(); ?>
												</div>
											</td>
											<td>
												@if ($client->status == "active")
												 	<span class="label label-success tooltips" data-original-title="Active">&nbsp;&nbsp;&nbsp;</span>
												 @elseif ($client->status == "inactive")
												 	<span class="label label-warning tooltips" data-original-title="Inactive">&nbsp;&nbsp;&nbsp;</span>
												 @elseif ($client->status == "cancelled")
												 	<span class="label label-danger tooltips" data-original-title="Cancelled">&nbsp;&nbsp;&nbsp;</span>
												 @elseif ($client->status == "archived")
												 	<span class="label label-default tooltips" data-original-title="Archived">&nbsp;&nbsp;&nbsp;</span>
												 @endif
											</td>
											<td>
												 <?php echo $client->getMainContact(); ?>
											</td>
											<td style="text-align: center;">
												<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($client->id)) }}" class="btn bg-blue btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Profile"><i class="fa fa-eye"></i></a>
												@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_edit"))
													<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getEditClient', array($client->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Client"><i class="fa fa-pencil"></i></a>
												@endif
												@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_delete"))
													<a href="#" class="btn bg-red btn-delete-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Client"><i class="fa fa-times"></i></a>
												@endif
											</td>
										</tr>
									<?php 
										}
									?>
		                        </tbody>
		                    </table>

		                    @if ($clients->count() == 0)
		                    	No clients found.
		                    @endif

		                    <div class="clearfix"></div>

		                    <div class="pull-left" style="padding-right: 10px; padding-top: 20px;">
		                    	Per page:
		                    </div>

		                    <div class="pull-left" style="padding-top: 15px;">
                            	<select id="page-size-dd" class="form-control">
                            		<option value="10" @if (\Input::get("page_size") == "10") selected @endif>10</option>
                            		<option value="25" @if (\Input::get("page_size") == "25") selected @endif>25</option>
                            		<option value="100" @if (\Input::get("page_size") == "100") selected @endif>100</option>
                            		<option value="All" @if (\Input::get("page_size") == "All") selected @endif>All</option>
                            	</select>
		                    </div>

		                    <div class="pull-right">
		                    	@if ($clients_paginator)
		                    		{!! $clients_paginator->render() !!}
		                    	@endif
		                    </div>
		                </div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
	        /* Delete a product */
	        $('#clients-table a.btn-delete-single').live('click', function (e) {
	            e.preventDefault();
	            if (confirm("Are you sure to delete this client?") == false) {
	                return;
	            }

	            var ids = [$(this).parents("tr").find('.batch_id').val()];

	            $.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeleteClients') }}", {
	        		ids: ids
	        	}, function() {
	        		window.location = window.location.href;
	        	});
	        });

	        $(".btn-delete").click(function(e) {
	        	e.preventDefault();
	        	var ids = [];

	        	var nRow = $(this).parents('tr')[0];

	        	$(".batch_id:checked").each(function() {
	        		ids.push($(this).val());
	        	});

	        	if (ids.length == 0) {
	        		alert('No clients were selected.');
	        		return;
	        	}

	        	if (confirm('Are you sure you want to delete the selected clients?')) {
		        	$.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeleteClients') }}", {
		        		ids: ids
		        	}, function() {
		        		window.location = window.location.href;
		        	});
	        	}
	        });

	        var ct_select = $("#contract_type").select2({
				allowClear: true,
				placeholder: "Choose..."
			});

	        var ct_select2 = $("#status").select2({
				allowClear: true,
				placeholder: "Choose..."
			});

			$("#sort-by-dd, #sort-type-dd, #page-size-dd").change(function() {
				$('input[name="sort_by"]').val($("#sort-by-dd").val());
				$('input[name="sort_type"]').val($("#sort-type-dd").val());
				$('input[name="page_size"]').val($("#page-size-dd").val());
				$("#filters-wrapper form").submit();
			});
	    });

		function toggleFilters() {
			if ($('#filters-wrapper').is(":visible")) {
				$('#filters-wrapper').slideUp();
				$("#toggle-filters-btn i").removeClass("fa-arrow-up").addClass("fa-arrow-down");
			}
			else {
				$('#filters-wrapper').slideDown();
				$("#toggle-filters-btn i").removeClass("fa-arrow-down").addClass("fa-arrow-up");
			}
		}
    </script>
@stop