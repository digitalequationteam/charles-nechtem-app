@extends('layouts.main')
@section('content')
	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Upload Document</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', $client_id) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>

	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Clients\Http\Controllers\ClientsController@postSaveDocument', $client_id)}}" class="panel form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-12">
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">File <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="file" name="file" />
					</div>
				</div>

				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Description</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" maxlength="100" name="description" id="description" value="<?php echo Input::old('description'); ?>" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>
@stop