@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/clients">Clients</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}">{{$client->name}}</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}#plans">Plans</a></li>
        <li class="active">{{$plan->name}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>{{$plan->name}} - Plan Editions</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getAddEdition', array($plan->id)) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span> Create Edition</a>
	</div>

	<div class="pull-right" style="padding-right: 10px;">
		<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getEditPlan', array($plan->id)) }}" class="btn btn-success m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-pencil"></span> Edit Plan</a>
	</div>

	<div class="clearfix"></div>

	<div class="row users">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						@if(Session::has('message'))
					        <div class="alert alert-success">
					            <button type="button" class="close" data-dismiss="alert">×</button>
					            <span>
					                {{ Session::get('message') }}
					            </span>
						    </div>
					    @endif

						<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
		                    <div class="filter-checkbox">
		                        <a href="#" class="btn btn-danger btn-delete">Delete</a>
		                    </div>
		                    <table id="plan-editions-table" class="table table-tools table-striped">
		                        <thead>
		                            <tr>
		                                <th style="min-width:50px">
		                                    <input type="checkbox" class="check_all"/>
		                                </th>
		                                <th class="text-center"></th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<?php
									foreach ($plan->editions()->get() as $edition) { ?>
										<tr class="odd gradeX">
											<td>
												<div class="pull-left" style="padding-top: 10px;">
													<input type="checkbox" class="batch_id" value="{{$edition->id}}" />
												</div>
												<div class="pull-left" style="padding-left: 30px; padding-top: 0px;">
													<h3 style="margin-top: 0px; padding-top: 0px;">{{$edition->year}}</h3>
													@if ($plan->type == "A")
														 <h4>
												            In-Network (preauthorization, no deductible)
												          </h4>
												          <div class="section">
												            <label>Inpatient Co-Pay: </label>&nbsp;${{$edition->inin_copay_day}} 
												              per day up to a maximum ${{$edition->inin_copay_admission}} 
												              per admission<br>
												            <label>Inpatient PHP Co-Pay: </label>&nbsp;${{$edition->inin_php_copay_day}}  
												              per day up to a maximum ${{$edition->inin_php_copay_admission}}  
												              per admission<br>
												            <label>Outpatient Co-Pay: </label>&nbsp;${{$edition->inou_copay}}  
												              per visit<br>
												            <label>Outpatient Out of Pocket Max: </label>&nbsp;${{$edition->inouopm_individual}} /individual
												              or ${{$edition->inouopm_family}} /family<br>
												            <label>Other Co-Pay: </label>&nbsp;{{$edition->inot_copay}} %</div>
												          <h4>
												            Out-Network (no preauthorization, deductible)
												          </h4>
												          <div class="section">
												            <label>Deductible: </label>&nbsp;${{$edition->onde_individual}} /individual
												              or ${{$edition->onde_family}} /family<br>
												            <label>Inpatient Co-Pay: </label>&nbsp;{{$edition->onin_copay}} %<br>
												            <label>Inpatient PHP Co-Pay: </label>&nbsp;{{$edition->onin_php_copay}} %<br>
												            <label>Outpatient Co-Pay: </label>&nbsp;{{$edition->onou_copay}} %<br>
												            <label>Outpatient Out of Pocket Max: </label>&nbsp;${{$edition->onouopm_individual}} /individual
												              or ${{$edition->onouopm_family}} /family<br>
												            <label>Other Co-Pay: </label>&nbsp;{{$edition->onot_copay}} %</div>
												    @elseif ($plan->type == "B")
												    	<h4>In-Network</h4>
												          <div class="section">
												            <label>Co-Pay: </label>&nbsp;{{$edition->incp_copay}}%<br>
												            <label>Single Employee Deductible: </label>&nbsp;${{$edition->inde_single}}<br>
												            <label>Employee + 1 Deductible: </label>&nbsp;${{$edition->inde_employee_1}}<br>
												            <label>Employee + 2 or more Deductible: </label>&nbsp;${{$edition->inde_employee_2}}<br>
												            <label>Single Employee Out of Pocket Max: </label>&nbsp;${{$edition->inou_single}}<br>
												            <label>Employee + 1 Out of Pocket Max: </label>&nbsp;${{$edition->inou_employee_1}}<br>
												            <label>Employee + 2 or more Out of Pocket Max: </label>&nbsp;${{$edition->inou_employee_2}}<br>
												          </div>
												          <h4>Out-Network</h4>
												          <div class="section">
												            <label>Co-Pay: </label>&nbsp;{{$edition->oncp_copay}}%<br>
												            <label>Single Employee Deductible: </label>&nbsp;${{$edition->onde_single}}<br>
												            <label>Employee + 1 Deductible: </label>&nbsp;${{$edition->onde_employee_1}}<br>
												            <label>Employee + 2 or more Deductible: </label>&nbsp;${{$edition->onde_employee_2}}<br>
												            <label>Single Employee Out of Pocket Max: </label>&nbsp;${{$edition->onou_single}}<br>
												            <label>Employee + 1 Out of Pocket Max: </label>&nbsp;${{$edition->onou_employee_1}}<br>
												            <label>Employee + 2 or more Out of Pocket Max: </label>&nbsp;${{$edition->onou_employee_2}}<br>
												          </div>
												    @endif
												</div>
											</td>
											<td style="text-align: center; vertical-align: top;">
												<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getEditEdition', array($edition->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Edition"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn bg-red btn-delete-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Edition"><i class="fa fa-times"></i></a>
											</td>
										</tr>
									<?php 
										}
									?>
		                        </tbody>
		                    </table>
		                </div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			var opt = {};
	        // Tools: export to Excel, CSV, PDF & Print
	        opt.sDom = "<'row m-t-10'<'col-md-6'><'col-md-6'Tf>r>t<'row'<'col-md-6'><'col-md-6 align-right'p>>",
	        opt.oLanguage = { "sSearch": "","sZeroRecords": "No editions found" } ,
	        opt.iDisplayLength = 15,
	        opt.oTableTools = {
	            /*"sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",*/
	            "aButtons": []
	        };
	        opt.aaSorting = [[ 0, 'asc' ]];
	        opt.aoColumnDefs = [
	              { 'bSortable': false, 'aTargets': [0] }
	           ];

	        var oTable = $('#plan-editions-table').dataTable(opt);
	        oTable.fnDraw();

	        $("#plan-editions-table tr th:eq(0)").removeClass("sorting_asc").removeClass("sorting_desc");

	        /* Add a placeholder to searh input */
	        $('.dataTables_filter input').attr("placeholder", "Search editions...");

	        /* Delete a product */
	        $('#plan-editions-table a.btn-delete-single').live('click', function (e) {
	            e.preventDefault();
	            if (confirm("Are you sure to delete this edition?") == false) {
	                return;
	            }
	            var nRow = $(this).parents('tr')[0];
	            oTable.fnDeleteRow(nRow);

	            var ids = [$(this).parents("tr").find('.batch_id').val()];

	            $.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeleteEditions') }}", {
	        		ids: ids
	        	}, function() {
	        		for (var i in nRows)
	            		oTable.fnDeleteRow(nRows[i]);
	        	});
	        });

	        $(".btn-delete").click(function() {
	        	var ids = [];
	        	var nRows = [];

	        	var nRow = $(this).parents('tr')[0];

	        	$(".batch_id:checked").each(function() {
	        		ids.push($(this).val());
	        		nRows.push($(this).parents('tr')[0]);
	        	});

	        	if (ids.length == 0) {
	        		alert('No editions were selected.');
	        		return;
	        	}

	        	if (confirm('Are you sure you want to delete the selected editions?')) {
		        	$.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeleteEditions') }}", {
		        		ids: ids
		        	}, function() {
		        		for (var i in nRows)
		            		oTable.fnDeleteRow(nRows[i]);
		        	});
	        	}
	        });
	    });
    </script>
@stop