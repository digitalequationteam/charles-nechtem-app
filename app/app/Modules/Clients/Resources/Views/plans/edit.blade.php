@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/clients">Clients</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}">{{$client->name}}</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}#plans">Plans</a></li>
        <li><a href="/clients/plan-profile/{{$plan->id}}">{{$plan->name}}</a></li>
        <li class="active">Edit</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Edit Plan</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', $plan->client_id) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>
	-->

	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Clients\Http\Controllers\ClientsController@postSavePlan', array($plan->client_id, $plan->id))}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-12">
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Plan Type <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<select name="type" class="form-control" disabled>
							<option value="A">A</option>
							<option value="B" @if ((old() && old('type') == 'B') || (!old() && $plan->type == 'B')) selected @endif>B</option>
						</select>
					</div>
				</div>

				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Name <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" maxlength="100" name="name" id="name" value="{{(old() ? old('name') : $plan->name)}}" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>
@stop