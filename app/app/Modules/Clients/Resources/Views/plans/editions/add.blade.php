@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/clients">Clients</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}">{{$client->name}}</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}#plans">Plans</a></li>
        <li><a href="/clients/plan-profile/{{$plan->id}}">{{$plan->name}}</a></li>
        <li class="active">Create Edition</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Create Edition</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<!--
		<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getPlanProfile', $plan->id) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
		-->
	</div>

	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Clients\Http\Controllers\ClientsController@postSaveEdition', $plan->id)}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			
			@if ($plan->type == "A")
				<div class="col-md-6">
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Year <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<select name="year" class="form-control">
								@for ($i = 2000; $i <= 2100; $i++)
									<option value="{{$i}}" @if ((old() && old('year') == $i) || (!old() && $i == date("Y"))) selected @endif>{{$i}}</option>
								@endfor
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>In-Network Inpatient</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inin_copay_day" required value="{{old('inin_copay_day')}}" />
	                            <span class="input-group-addon bg-grey" style="width: 100px;">
	                              /day
	                            </span>
	                        </div>

							<div class="input-group transparent" style="padding-top: 10px;">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inin_copay_admission" required value="{{old('inin_copay_admission')}}" />
	                            <span class="input-group-addon bg-grey" style="width: 100px;">
	                              /admission
	                            </span>
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">PHP Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inin_php_copay_day" required value="{{old('inin_php_copay_day')}}" />
	                            <span class="input-group-addon bg-grey" style="width: 100px;">
	                              /day
	                            </span>
	                        </div>

							<div class="input-group transparent" style="padding-top: 10px;">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inin_php_copay_admission" required value="{{old('inin_php_copay_admission')}}" />
	                            <span class="input-group-addon bg-grey" style="width: 100px;">
	                              /admission
	                            </span>
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>In-Network Outpatient</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inou_copay" required value="{{old('inou_copay')}}" />
	                            <span class="input-group-addon bg-grey" style="width: 100px;">
	                              /visit
	                            </span>
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>In-Network Outpatient Out of Pocket Max</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Individual <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inouopm_individual" required value="{{old('inouopm_individual')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Family <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inouopm_family" required value="{{old('inouopm_family')}}" />
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>In-Network Other</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <input type="number" class="form-control" name="inot_copay" required value="{{old('inot_copay')}}" />
	                            <span class="input-group-addon bg-grey">
	                              %
	                            </span>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Out-Network Deductible</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Individual <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onde_individual" required value="{{old('onde_individual')}}" />

	                            <span class="input-group-addon bg-grey" style="width: 100px;">
	                              /year
	                            </span>
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Family <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onde_family" required value="{{old('onde_family')}}" />
	                            <span class="input-group-addon bg-grey" style="width: 100px;">
	                              /year
	                            </span>
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Out-Network Inpatient</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <input type="number" class="form-control" name="onin_copay" required value="{{old('onin_copay')}}" />
	                            <span class="input-group-addon bg-grey">
	                              %
	                            </span>
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">PHP Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <input type="number" class="form-control" name="onin_php_copay" required value="{{old('onin_php_copay')}}" />
	                            <span class="input-group-addon bg-grey">
	                              %
	                            </span>
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Out-Network Outpatient</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <input type="number" class="form-control" name="onou_copay" required value="{{old('onou_copay')}}" />
	                            <span class="input-group-addon bg-grey">
	                              %
	                            </span>
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Out-Network Outpatient Out of Pocket Max</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Individual <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onouopm_individual" required value="{{old('onouopm_individual')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Family <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onouopm_family" required value="{{old('onouopm_family')}}" />
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Out-Network Other</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <input type="number" class="form-control" name="onot_copay" required value="{{old('onot_copay')}}" />
	                            <span class="input-group-addon bg-grey">
	                              %
	                            </span>
	                        </div>
						</div>
					</div>

				</div>
			@elseif ($plan->type == "B")
				<div class="col-md-6">
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Year <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<select name="year" class="form-control">
								@for ($i = 2000; $i <= 2100; $i++)
									<option value="{{$i}}" @if ((old() && old('year') == $i) || (!old() && $i == date("Y"))) selected @endif>{{$i}}</option>
								@endfor
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>In-Network Co-Pay</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="incp_copay" required value="{{old('incp_copay')}}" />
	                            <span class="input-group-addon bg-grey">
	                              %
	                            </span>
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>In-Network Deductibles</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Single <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inde_single" required value="{{old('inde_single')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Employee + 1 <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inde_employee_1" required value="{{old('inde_employee_1')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Employee + 2 or more <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inde_employee_2" required value="{{old('inde_employee_2')}}" />
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>In-Network Out of Pocket Max</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Single <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inou_single" required value="{{old('inou_single')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Employee + 1 <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inou_employee_1" required value="{{old('inou_employee_1')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Employee + 2 or more <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="inou_employee_2" required value="{{old('inou_employee_2')}}" />
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-md-6">

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Out-Network Co-Pay</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Co-Pay <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="oncp_copay" required value="{{old('oncp_copay')}}" />
	                            <span class="input-group-addon bg-grey">
	                              %
	                            </span>
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Out-Network Deductibles</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Single <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onde_single" required value="{{old('onde_single')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Employee + 1 <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onde_employee_1" required value="{{old('onde_employee_1')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Employee + 2 or more <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onde_employee_2" required value="{{old('onde_employee_2')}}" />
	                        </div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Out-Network Out of Pocket Max</strong></h3>
						</label>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Single <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onou_single" required value="{{old('onou_single')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Employee + 1 <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onou_employee_1" required value="{{old('onou_employee_1')}}" />
	                        </div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h">
						<label class="control-label col-sm-5">Employee + 2 or more <span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="input-group transparent">
	                            <span class="input-group-addon bg-grey">
	                              <i class="fa fa-dollar"></i>
	                            </span>
	                            <input type="number" class="form-control" name="onou_employee_2" required value="{{old('onou_employee_2')}}" />
	                        </div>
						</div>
					</div>

				</div>
			@endif

		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>
@stop