@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/clients">Clients</a></li>
        <li class="active">Register Client Company</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Register Client Company</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getIndex') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>
	-->

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Clients\Http\Controllers\ClientsController@postSaveClient')}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Basic Info</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Name <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="name" id="name" value="<?php echo Input::old('name'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Tax ID</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="tax_id" id="tax_id" value="<?php echo Input::old('tax_id'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Memo</label>
					<div class="col-sm-7">
						<textarea class="form-control" name="memo" id="memo"><?php echo Input::old('memo'); ?></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-offset-5 col-sm-7" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Contract Info</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Contract Type <span class="text-danger">*</span></label>
					<div class="col-sm-7">
						<select name="contract_type" class="form-control" onchange="if (this.value == 'Other') { $('.for-contract-type-custom').show(); } else { $('.for-contract-type-custom').hide(); }">
						    <option value="EAP" @if (old('contract_type') == 'EAP') selected @endif>EAP</option>
						    <option value="EAP/OASIS" @if (old('contract_type') == 'EAP/OASIS') selected @endif>EAP/OASIS</option>
						    <option value="OASIS" @if (old('contract_type') == 'OASIS') selected @endif>OASIS</option>
						    <option value="Phone Only Supports" @if (old('contract_type') == 'Phone Only Supports') selected @endif>Phone Only Supports</option>
						    <option value="Wellness Coaching" @if (old('contract_type') == 'Wellness Coaching') selected @endif>Wellness Coaching</option>
						    <option value="Other" @if (old('contract_type') == 'Other') selected @endif>Other</option>
						  </select>
					</div>
				</div>
				<div class="for-contract-type-custom form-group no-margin-hr panel-padding-h" @if (old('contract_type') != 'Other') style="display: none;" @endif>
					<label class="control-label col-sm-5">&nbsp;</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="contract_type_custom" id="contract_type_custom" value="<?php echo Input::old('contract_type_custom'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Contract Period From</label>
					<div class="col-sm-7">
						<input type="text" class="form-control date" maxlength="100" name="contract_period_from" id="contract_period_from" value="<?php echo Input::old('contract_period_from'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Contract Period To</label>
					<div class="col-sm-7">
						<input type="text" class="form-control date" maxlength="100" name="contract_period_to" id="contract_period_to" value="<?php echo Input::old('contract_period_to'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Renewal Period From</label>
					<div class="col-sm-7">
						<input type="text" class="form-control date" maxlength="100" name="renewal_period_from" id="renewal_period_from" value="<?php echo Input::old('renewal_period_from'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Renewal Period To</label>
					<div class="col-sm-7">
						<input type="text" class="form-control date" maxlength="100" name="renewal_period_to" id="renewal_period_to" value="<?php echo Input::old('renewal_period_to'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Employee Head Count</label>
					<div class="col-sm-7">
						<input type="number" class="form-control" maxlength="100" name="employee_head_count" id="employee_head_count" value="<?php echo Input::old('employee_head_count'); ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Invoice Type</label>
					<div class="col-sm-7">
						<select class="form-control" maxlength="100" name="invoice_type" id="invoice_type">
							<option value="Monthly" @if (old('invoice_type') == 'Monthly') selected @endif>Monthly</option>
							<option value="Quarterly" @if (old('invoice_type') == 'Quarterly') selected @endif>Quarterly</option>
							<option value="Yearly" @if (old('invoice_type') == 'Yearly') selected @endif>Yearly</option>
						</select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-5">Employee Fee</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" maxlength="100" name="employee_fee" id="employee_fee" value="<?php echo Input::old('employee_fee'); ?>" />
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label col-sm-offset-1 col-sm-7" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Contacts</strong></h3>
					</label>
				</div>

				<div id="contacts">
					@foreach ((array)old("contact") as $key => $contact)
						<div class="contact-item form-group no-margin-hr panel-padding-h">
							<label class="control-label col-sm-1">&nbsp;</label>
							<div class="col-sm-8">
								<a href="#" id="contact_{{@$key}}" data-type="contact" data-pk="1" data-title="Contact Info" style="display: block;"></a>
								<input type="hidden" class="title" name="contact[{{@$key}}][title]" value="{{@$contact['title']}}" />
								<input type="hidden" class="first_name" name="contact[{{@$key}}][first_name]" value="{{@$contact['first_name']}}" />
								<input type="hidden" class="last_name" name="contact[{{@$key}}][last_name]" value="{{@$contact['last_name']}}" />
								<input type="hidden" class="email" name="contact[{{@$key}}][email]" value="{{@$contact['email']}}" />
								<input type="hidden" class="phone" name="contact[{{@$key}}][phone]" value="{{@$contact['phone']}}" />
								<input type="hidden" class="cell_phone" name="contact[{{@$key}}][cell_phone]" value="{{@$contact['cell_phone']}}" />
								<input type="hidden" class="fax" name="contact[{{@$key}}][fax]" value="{{@$contact['fax']}}" />
								<input type="hidden" class="additional_info" name="contact[{{@$key}}][additional_info]" value="{{@$contact['additional_info']}}" />
							</div>
						</div>
						<script type="text/javascript">
							$(document).ready(function() {
								$("#contact_{{$key}}").editable({
									value: {
										title: $("#contact_{{$key}}").parents(".contact-item").find('input.title').val(),
										first_name: $("#contact_{{$key}}").parents(".contact-item").find('input.first_name').val(),
										last_name: $("#contact_{{$key}}").parents(".contact-item").find('input.last_name').val(),
										email: $("#contact_{{$key}}").parents(".contact-item").find('input.email').val(),
										phone: $("#contact_{{$key}}").parents(".contact-item").find('input.phone').val(),
										cell_phone: $("#contact_{{$key}}").parents(".contact-item").find('input.cell_phone').val(),
										fax: $("#contact_{{$key}}").parents(".contact-item").find('input.fax').val(),
										additional_info: $("#contact_{{$key}}").parents(".contact-item").find('input.additional_info').val(),
									},
							        validate: function(value) {
							            if(value.first_name == '') return 'First name is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@endforeach
				</div>

				<div class="form-group">
					<label class="control-label col-sm-offset-1 col-sm-7" style="text-align: left;">
						<a href="#" id="add-new-contact" class="btn btn-success"><i class="fa fa-plus"></i> Add Contact</a>
					</label>
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label col-sm-offset-1 col-sm-7" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Primary Office Address</strong></h3>
					</label>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-1">&nbsp;</label>
					<div class="col-sm-8">
						<a href="#" id="primary_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="primary_street" value="{{old('primary_street')}}" />
						<input type="hidden" class="city" name="primary_city" value="{{old('primary_city')}}" />
						<input type="hidden" class="country" name="primary_country" value="{{old('primary_country')}}" />
						<input type="hidden" class="state" name="primary_state" value="{{old('primary_state')}}" />
						<input type="hidden" class="zip" name="primary_zip" value="{{old('primary_zip')}}" />
					</div>

					@if (old())
						<script type="text/javascript">
							$(document).ready(function() {
								$("#primary_address").editable({
									@if (old("primary_street"))
										value: {
											street: '{{old("primary_street")}}',
											city: '{{old("primary_city")}}',
											country: '{{old("primary_country")}}',
											state: '{{old("primary_state")}}',
											zip: '{{old("primary_zip")}}'
										},
									@endif
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@endif
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label col-sm-offset-1 col-sm-7" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Secondary Office Address</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-1">&nbsp;</label>
					<div class="col-sm-8">
						<label>
							<input type="checkbox" class="switch sw1" maxlength="100" name="secondary_address_available" id="secondary_address_available" @if (Input::old('secondary_address_available')) checked @endif />
						</label>
					</div>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h for-secondary" @if (!\old("secondary_address_available")) style="display: none;" @endif>
					<label class="control-label col-sm-1">&nbsp;</label>
					<div class="col-sm-8">
						<a href="#" id="secondary_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="secondary_street" value="{{old('secondary_street')}}" />
						<input type="hidden" class="city" name="secondary_city" value="{{old('secondary_city')}}" />
						<input type="hidden" class="country" name="secondary_country" value="{{old('secondary_country')}}" />
						<input type="hidden" class="state" name="secondary_state" value="{{old('secondary_state')}}" />
						<input type="hidden" class="zip" name="secondary_zip" value="{{old('secondary_zip')}}" />
					</div>

					@if (old())
						<script type="text/javascript">
							$(document).ready(function() {
								$("#secondary_address").editable({
									@if (old("secondary_street"))
										value: {
											street: '{{old("secondary_street")}}',
											city: '{{old("secondary_city")}}',
											country: '{{old("secondary_country")}}',
											state: '{{old("secondary_state")}}',
											zip: '{{old("secondary_zip")}}'
										},
									@endif
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@endif
				</div>

				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label col-sm-offset-1 col-sm-7" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Mailing Address</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-1">&nbsp;</label>
					<div class="col-sm-8">
						Same as primary:
						<label>
							<input type="checkbox" class="switch sw2" maxlength="100" name="mailing_same_as_primary" id="mailing_same_as_primary" @if (Input::old('mailing_same_as_primary') || !old()) checked @endif />
						</label>
					</div>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h for-mailing" @if (\old("mailing_same_as_primary") || !old()) style="display: none;" @endif>
					<label class="control-label col-sm-1">&nbsp;</label>
					<div class="col-sm-8">
						<a href="#" id="mailing_address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="mailing_street" value="{{old('mailing_street')}}" />
						<input type="hidden" class="city" name="mailing_city" value="{{old('mailing_city')}}" />
						<input type="hidden" class="country" name="mailing_country" value="{{old('mailing_country')}}" />
						<input type="hidden" class="state" name="mailing_state" value="{{old('mailing_state')}}" />
						<input type="hidden" class="zip" name="mailing_zip" value="{{old('mailing_zip')}}" />
					</div>

					@if (old())
						<script type="text/javascript">
							$(document).ready(function() {
								$("#mailing_address").editable({
									@if (old("mailing_street"))
										value: {
											street: '{{old("mailing_street")}}',
											city: '{{old("mailing_city")}}',
											country: '{{old("mailing_country")}}',
											state: '{{old("mailing_state")}}',
											zip: '{{old("mailing_zip")}}'
										},
									@endif
							        validate: function(value) {
							            if(value.street == '') return 'Street is required!'; 
							            if(value.city == '') return 'City is required!'; 
							            if(value.country == '') return 'Country is required!';
							            if(value.state == '') return 'State is required!'; 
							            if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'left'
							    });
							});
						</script>
					@endif
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#first_name, #last_name").keyup(function() {
				$("#initials").val($("#first_name").val().substr(0,1) + $("#last_name").val().substr(0,1));
			});
			$(".switch.sw1").bootstrapSwitch({
				onText: "ON",
				offText: "OFF"
			});
			$(".switch.sw2").bootstrapSwitch({
				onText: "YES",
				offText: "NO"
			});

			$('.switch.sw1').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (state.value)
			    	$(".for-secondary").show();
			    else
			    	$(".for-secondary").hide();
			});

			$('.switch.sw2').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (!state.value)
			    	$(".for-mailing").show();
			    else
			    	$(".for-mailing").hide();
			});

			$("#add-new-contact").click(function() {
				var key = $("#contacts>.contact-item").length + 1;
				$("#contacts").append('<div class="contact-item form-group no-margin-hr panel-padding-h">\
					<label class="control-label col-sm-1">&nbsp;</label>\
					<div class="col-sm-8">\
						<a href="#" id="contact_' + key + '" data-type="contact" data-pk="1" data-title="Contact Info" style="display: block;"></a>\
						<input type="hidden" class="title" name="contact[' + key + '][title]" />\
						<input type="hidden" class="first_name" name="contact[' + key + '][first_name]" />\
						<input type="hidden" class="last_name" name="contact[' + key + '][last_name]" />\
						<input type="hidden" class="email" name="contact[' + key + '][email]" />\
						<input type="hidden" class="phone" name="contact[' + key + '][phone]" />\
						<input type="hidden" class="cell_phone" name="contact[' + key + '][cell_phone]" />\
						<input type="hidden" class="fax" name="contact[' + key + '][fax]" />\
						<input type="hidden" class="additional_info" name="contact[' + key + '][additional_info]" />\
					</div>\
				</div>');

				$("#contact_" + key).editable({
			        validate: function(value) {
			            if(value.first_name == '') return 'First name is required!'; 
			        },
			        placement: 'left',
		        	emptytext: 'Add Contact Info'
			    });

				setTimeout(function() {
			    	$("#contact_" + key).click();
			    }, 100);
			});

			$("#primary_address, #secondary_address, #mailing_address").editable({
		        validate: function(value) {
		            if(value.street == '') return 'Street is required!'; 
		            if(value.city == '') return 'City is required!'; 
		            if(value.country == '') return 'Country is required!';
		            if(value.state == '') return 'State is required!'; 
		            if(value.zip == '') return 'Zip Code is required!'; 
		        },
		        placement: 'left',
		        emptytext: 'Add Address'
		    });
		});

		var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};

		var contact_titles = ['Administrator, Benefits/Risk', 'Administrator, HRS-Benefits services', 'Benefits Administrator', 'Benefits Contact Person', 'Benefits Coordinator', 'Benefits Manager', 'Benefits Specialist', 'CEO & President', 'Chief Operating Officer', 'Director, Compensation & Benefits', 'Director, Human resources', 'Director of Administrator Services', 'Director of Benefits', 'Director of Social Services', 'Director of Workers Compensation & Employee Health', 'Division of Labor-Management Relations', 'Employees Relations', 'Executive Director', 'Financial Director', 'Financial Officer', 'Group Business Manager', 'Human Resources Manager', 'Lead Manager, HR Department', 'Office Manager', 'President', 'Senior President', 'Training Director', 'Vice President', 'Vice President & Chief Financial Officer', 'Vice President of Human Resources', 'Vice President of Life Operations'];
	</script>
@stop