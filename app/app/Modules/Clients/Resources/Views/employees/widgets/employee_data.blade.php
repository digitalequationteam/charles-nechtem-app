<div class="row">
	<div class="col-md-6">
		<h3>Enrollee</h3>
	    <strong>First Name: </strong>{{$enrollee->first_name}}<br>
	    <strong>Last Name: </strong>{{$enrollee->last_name}}<br>
	    <strong>Gender: </strong>{{$enrollee->gender}}<br>
	    <strong>Email: </strong>{{$enrollee->email}}<br>
	    <!--<strong>SSN: </strong>{{$enrollee->ssn}}<br>-->
	    <strong>Cell Phone: </strong>{{$enrollee->cell_phone}}<br>
	    <strong>Home Phone: </strong>{{$enrollee->home_phone}}<br>
	    <strong>Work Phone: </strong>{{$enrollee->work_phone}}<br>
	    <strong>Birthday: </strong>{{$enrollee->birthday}}<br>
	</div>
	<div class="col-md-6">
		<h3>&nbsp;</h3>
	    <strong>Plan: </strong>{{@$employee->plan()->get()->first()->name}}<br>
	    <strong>Insurance Network: </strong>{{@$employee->insurance_network()->get()->first()->name}}<br>
	    <strong>Insurance Type: </strong>{{$employee->insurance_type}}<br>
	    <strong>Company: </strong>{{$employee->client()->get()->first()->name}}

	    <div class="address-info">
	    	<strong>Address</strong>
				{!! $enrollee->getPrimaryAddress(2) !!}
		  </div>
	</div>

	<input type="hidden" name="enrollee_id" value="{{$enrollee->id}}" />
</div>