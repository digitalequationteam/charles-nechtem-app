<div class="row">
	<div class="col-md-12">
		<div class="text-center">
			<h3 class="panel-title" style="width: auto;"><strong>Enrollee</strong></h3>
			<br />
			<a href="#select-enrollee-modal" data-toggle="modal" class="btn btn-success"><i class="fa fa-plus"></i> Select Enrollee</a>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-1 col-sm-10" id="enrollee-data">
				@if (@$enrollee_data)
					<div class="row">
		                <div class="col-md-6">
		                    <h3 class="m-t-0">Enrollee</h3>
		                    <strong>First Name: </strong>{{$enrollee_data->enrollee->first_name}}<br>
		                    <strong>Last Name: </strong>{{$enrollee_data->enrollee->last_name}}<br>
		                    <strong>Gender: </strong>{{$enrollee_data->enrollee->gender}}<br>
		                    <strong>Cell Phone: </strong>{{$enrollee_data->enrollee->cell_phone}}<br>
		                    <strong>Home Phone: </strong>{{$enrollee_data->enrollee->home_phone}}<br>
		                    <strong>Work Phone: </strong>{{$enrollee_data->enrollee->work_phone}}<br>
		                    <strong>Birthday: </strong>{{$enrollee_data->enrollee->birthday}}<br>
		                </div>
		                <div class="col-md-6">
		                    <h3 class="m-t-0">&nbsp;</h3>
		                    <strong>Plan: </strong>{{@$enrollee_data->employee->plan}}<br>
		                    <strong>Insurance Network: </strong>{{@$enrollee_data->employee->insurance_network}}<br>
		                    <strong>Insurance Type: </strong>{{$enrollee_data->employee->insurance_type}}<br>
		                    <strong>Company: </strong>{{$enrollee_data->employee->client}}

		                    <div class="address-info">
		                        <strong>Address</strong>
		                            {!! $enrollee_data->enrollee->address !!}
		                      </div>
		                </div>
		            </div>
				@endif
			</div>
		</div>

		<div class="div-separator"></div>
	</div>
</div>

<div class="modal fade" id="select-enrollee-modal" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><strong>Select</strong> Enrollee</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding: 15px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" id="search-enrollee-field" placeholder="Search Enrollee...">
                        </div>

                        <div class="form-group">
                        	<div id="enrollees-results" style="max-height: 200px; overflow-y: auto; padding: 20px;"></div>
                        </div>

                        <a href="#add-enrollee-modal" data-toggle="modal" class="btn btn-success"><i class="fa fa-plus"></i> Add New Enrollee</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-enrollee-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding: 15px;">
                	<script type="text/javascript">
                		var exists = true;
                	</script>
                    <div class="col-md-12" id="add-new-enrollee-form" style="margin-top: -30px;">
                    	<div id="iframe-here" style="height: 100%; display: none;"></div>
						<div id="iframe_loading" style="width: 100%; height: 100%; text-align: center; background-image: url(https://www.hereofamily.com/wp-content/themes/HereO/images/ajax-loader.gif); background-repeat: no-repeat; background-position: 50% 50%; background-size: 10%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-success" onclick="submitIframeForm();"><i class="fa fa-arrow-right"></i> Save</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var employee_searching = false;
		$("#search-enrollee-field").keyup(function() {
			var search_text = $(this).val();
			if (search_text.length <= 1)
				return;

			//if (employee_searching)
				//return false;

			employee_searching = true;
			$.post("{{action('\App\Modules\Clients\Http\Controllers\ClientsController@postWidgetSearchEnrollee')}}", {
				search_text: search_text
			}, function(response) {
				$("#enrollees-results").html(response);
				employee_searching = false;
				$(".tooltips").tooltip();
			});
		});

		$(".select-employee-btn").live("click", function() {
			selectEnrollee($(this).attr("data-id"));
		});

		@if (\Input::old("enrollee_id"))
			selectEnrollee({{\Input::old("enrollee_id")}});
		@endif

		$('a[href="#add-enrollee-modal"]').click(function() {
			var new_height = $(window).height() - 250;
			$("#add-new-enrollee-form").css('height', new_height + 'px');
			$("#add-new-enrollee-form #iframe-here").html('<iframe onload="$(\'#iframe-here\').show(); $(\'#iframe_loading\').hide();" src="{{URL::to('/clients/add-employee/')}}" style="width: 100%; height: 100%; border: 0px; overflow-x: hidden;"></iframe>');
		});
	});

	function selectEnrollee(id) {
		$.post("{{action('\App\Modules\Clients\Http\Controllers\ClientsController@postGetEnrolleeData')}}", {
			enrollee_id: id
		}, function(response) {
			$("#enrollee-data").html(response);
			$('button[data-dismiss="modal"]').trigger("click");
		});
	}

	function submitIframeForm() {
		$("#add-new-enrollee-form iframe").contents().find("form").submit();
	}
</script>