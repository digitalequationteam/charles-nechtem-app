@if (count($enrollees) == 0)
	No Enrollees found.
@endif

@foreach ($enrollees as $enrollee)
	<div class="row" style="border-bottom: solid 1px #CCCCCC; padding: 5px;">
		<div class="pull-right">
			<a href="#" data-id="{{$enrollee->id}}" class="btn btn-success tooltips btn-xs select-employee-btn" data-original-title="Use {{$enrollee->first_name}} {{$enrollee->last_name}}"><i class="fa fa-arrow-right"></i></a>
			@if ($enrollee->parent_id == 0)
				<!--<a href="#" class="btn btn-primary tooltips btn-xs" data-original-title="Add Dependent"><i class="fa fa-plus"></i></a>-->
			@endif
		</div>
		<strong>{{$enrollee->first_name}} {{$enrollee->last_name}}</strong>
		({{($enrollee->type == 'Employee') ? "employee" : "dependent"}})
	</div>
@endforeach