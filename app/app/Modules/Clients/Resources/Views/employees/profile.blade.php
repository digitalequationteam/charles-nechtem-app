@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/clients">Clients</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}">{{$client->name}}</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}#employees">Enrollees</a></li>
        <li class="active">{{$employee->first_name}} {{$employee->last_name}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>{{$client->name}} - {{$employee->first_name}} {{$employee->last_name}}</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right" style="padding-right: 10px;">
		<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($employee->client_id, '#employees')) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span> Back</a>
	</div>
	-->

	<div class="clearfix"></div>

	<div class="row users">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-body">
					<div class="row p-20">
	                    <div class="col-md-6">
	                        <h3 class="m-t-0">{{ucfirst($employee->type)}} Info</h3>
	                        <form class="form-horizontal p-20">
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">First Name:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->first_name}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Last Name:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->last_name}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Gender:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->gender}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">SSN:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->ssn}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Cell Phone:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->cell_phone}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Home Phone:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->home_phone}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Work Phone:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->work_phone}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Birthday:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->birthday}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Plan:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{@$employee->plan()->get()->first()->name}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Insurance Network:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{@$employee->insurance_network()->get()->first()->name}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Insurance Type:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{$employee->insurance_type}}</strong>
	                                </div>
	                            </div>
	                            <div class="form-group" style="margin-bottom: 2px;">
	                                <div class="col-sm-4">Company:
	                                </div>
	                                <div class="col-sm-8">
	                                    <strong>{{@$employee->client()->get()->first()->name}}</strong>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                    <div class="col-md-6">
	                    	@if ($employee->street)
		                        <h3 class="m-t-0">Address Information</h3>
		                        <form class="form-horizontal p-20">
		                            <div class="form-group" style="margin-bottom: 2px;">
		                                <div class="col-sm-4">
		                                    Address:
		                                </div>
		                                <div class="col-sm-8">
		                                    <strong>{!! $employee->getPrimaryAddress(2) !!}</strong>
		                                </div>
		                            </div>
		                        </form>
		                    @endif
	                    </div>
	                </div>

	                <div class="clearfix text-left">
	                    <a class="btn btn-success" href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getEditEmployee', array($employee->id)) }}">Edit {{ucfirst($employee->type)}}</a>
	                </div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
@stop