@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/clients">Clients</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}">{{$client->name}}</a></li>
        <li><a href="/clients/client-profile/{{$client->id}}#employees">Enrollees</a></li>
        <li><a href="/clients/employee-profile/{{$employee->id}}">{{$employee->first_name}} {{$employee->last_name}}</a></li>
        <li class="active">Edit</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Edit @if ($employee->parent_id == 0) Employee @else Dependant @endif</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		<!--
		<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($client->id, '#employees')) }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
		-->
	</div>

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" id="edit-client-form" action="{{action('\App\Modules\Clients\Http\Controllers\ClientsController@postSaveEmployee', array($client->id, $employee->id))}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-sm-offset-5 col-sm-5" style="text-align: left;">
							<h3 class="panel-title" style="width: auto;"><strong>Client</strong></h3>
						</label>
					</div>
					<label class="control-label col-sm-4">Select Client</label>
					<div class="col-sm-5">
						<select class="form-control" name="client_id" id="client_id">
							@foreach (\Client::orderBy("name")->get() as $client2)
						    	<option value="{{$client2->id}}" @if ($employee->client_id == $client2->id) selected @endif>{{$client2->name}}</option>
						    @endforeach
						</select>
					</div>
				<div class="form-group">
					<label class="control-label col-sm-offset-5 col-sm-5" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Personal Data</strong></h3>
					</label>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">First Name</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" maxlength="100" name="first_name" id="first_name" value="<?php echo (old()) ? Input::old('first_name') : $employee->first_name; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Last Name</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" maxlength="100" name="last_name" id="last_name" value="<?php echo (old()) ? Input::old('last_name') : $employee->last_name; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Email</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" maxlength="100" name="email" id="email" value="<?php echo (old()) ? Input::old('email') : $employee->email; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Gender</label>
					<div class="col-sm-5">
						<select name="gender" class="form-control">
						    <option value="Male" @if ((old() && old('gender') == 'Male') || (!old() && $employee->gender == "Male")) selected @endif>Male</option>
						    <option value="Female" @if ((old() && old('gender') == 'Female') || (!old() && $employee->gender == "Female")) selected @endif>Female</option>
						  </select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Cell Phone</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" maxlength="100" name="cell_phone" id="cell_phone" value="<?php echo (old()) ? Input::old('cell_phone') : $employee->cell_phone; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Home Phone</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" maxlength="100" name="home_phone" id="home_phone" value="<?php echo (old()) ? Input::old('home_phone') : $employee->home_phone; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Work Phone</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" maxlength="100" name="work_phone" id="work_phone" value="<?php echo (old()) ? Input::old('work_phone') : $employee->work_phone; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">SSN</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" maxlength="100" name="ssn" id="ssn" value="<?php echo (old()) ? Input::old('ssn') : $employee->ssn; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Birthday</label>
					<div class="col-sm-5">
						<input type="text" class="form-control date" maxlength="100" name="birthday" id="birthday" value="<?php echo (old()) ? Input::old('birthday') : $employee->birthday; ?>" />
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-offset-5 col-sm-5" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Insurance Info</strong></h3>
					</label>
				</div>

				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Plan</label>
					<div class="col-sm-5">
						<select name="client_plan_id" class="form-control">
							@foreach ($client->plans()->get() as $plan)
						    	<option value="{{$plan->id}}" @if ((old() && old('client_plan_id') == $plan->id) || (!old() && $employee->client_plan_id == $plan->id)) selected @endif>{{$plan->name}}</option>
						    @endforeach
						</select>
					</div>
				</div>

				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Insurance Network</label>
					<div class="col-sm-5">
						<select name="insurance_network_id" class="form-control">
							@foreach (\DictionaryValue::where("category", "insurance_networks")->get() as $insurance_network)
						    	<option value="{{$insurance_network->id}}" @if ((old() && old('insurance_network_id') == $insurance_network->id) || (!old() && $employee->insurance_network_id == $insurance_network->id)) selected @endif>{{$insurance_network->name}}</option>
						    @endforeach
						</select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Insurance Type</label>
					<div class="col-sm-5">
						<select name="insurance_type" class="form-control">
						    <option value="Full" @if ((old() && old('insurance_type') == 'Full') || (!old() && $employee->insurance_type == "Full")) selected @endif>Full</option>
						    <option value="Cobra" @if ((old() && old('insurance_type') == 'Cobra') || (!old() && $employee->insurance_type == "Cobra")) selected @endif>Cobra</option>
						  </select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Plan Coverage Termination Date</label>
					<div class="col-sm-5">
						<input type="text" class="form-control date" maxlength="100" name="plan_coverage_termination_date" id="plan_coverage_termination_date" value="<?php echo (old()) ? Input::old('plan_coverage_termination_date') : $employee->plan_coverage_termination_date; ?>" />
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-offset-5 col-sm-5" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Additional Info</strong></h3>
					</label>
				</div>

				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Work Department</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" maxlength="100" name="work_department" id="work_department" value="<?php echo (old()) ? Input::old('work_department') : $employee->work_department; ?>" />
					</div>
				</div>
			
				<div class="form-group" style="margin-bottom: 8px;">
					<label class="control-label col-sm-offset-4 col-sm-5" style="text-align: left;">
						<h3 class="panel-title" style="width: auto;"><strong>Address</strong></h3>
					</label>
				</div>
				<div class="address-item form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">&nbsp;</label>
					<div class="col-sm-5">
						<a href="#" id="address" data-type="address" data-pk="1" data-title="Address Info" style="display: block;"></a>
						<input type="hidden" class="street" name="street" value="{{(old()) ? old('street') : $employee->street}}" />
						<input type="hidden" class="city" name="city" value="{{(old()) ? old('city') : $employee->city}}" />
						<input type="hidden" class="country" name="country" value="{{(old()) ? old('country') : $employee->country}}" />
						<input type="hidden" class="state" name="state" value="{{(old()) ? old('state') : $employee->state}}" />
						<input type="hidden" class="zip" name="zip" value="{{(old()) ? old('zip') : $employee->zip}}" />
					</div>

					@if (old())
						<script type="text/javascript">
							$(document).ready(function() {
								$("#address").editable({
									@if (old("street"))
										value: {
											street: '{{old("street")}}',
											city: '{{old("city")}}',
											country: '{{old("country")}}',
											state: '{{old("state")}}',
											zip: '{{old("zip")}}'
										},
									@endif
							        validate: function(value) {
							            // if(value.street == '') return 'Street is required!'; 
							            // if(value.city == '') return 'City is required!'; 
							            // if(value.country == '') return 'Country is required!';
							            // if(value.state == '') return 'State is required!'; 
							            // if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'top',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@else
						<script type="text/javascript">
							$(document).ready(function() {
								$("#address").editable({
									@if ($employee->city)
										value: {
											street: '{{$employee->street}}',
											city: '{{$employee->city}}',
											country: '{{$employee->country}}',
											state: '{{$employee->state}}',
											zip: '{{$employee->zip}}'
										},
									@endif
							        validate: function(value) {
							            // if(value.street == '') return 'Street is required!'; 
							            // if(value.city == '') return 'City is required!'; 
							            // if(value.country == '') return 'Country is required!';
							            // if(value.state == '') return 'State is required!'; 
							            // if(value.zip == '') return 'Zip Code is required!'; 
							        },
							        placement: 'top',
		        					emptytext: 'Add Address'
							    });
							});
						</script>
					@endif
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#add-new-dependent").click(function() {
				var key = $("#dependents>.dependent-item").length + 1;
				$("#dependents").append('<div class="dependent-item form-group no-margin-hr panel-padding-h">\
					<label class="control-label col-sm-1">&nbsp;</label>\
					<div class="col-sm-8">\
						<a href="#" id="dependent_' + key + '" data-type="dependent" data-pk="1" data-title="Contact Info" style="display: block;"></a>\
						<input type="hidden" class="first_name" name="dependent[' + key + '][first_name]" />\
						<input type="hidden" class="last_name" name="dependent[' + key + '][last_name]" />\
						<input type="hidden" class="gender" name="dependent[' + key + '][gender]" />\
						<input type="hidden" class="cell_phone" name="dependent[' + key + '][cell_phone]" />\
						<input type="hidden" class="home_phone" name="dependent[' + key + '][home_phone]" />\
						<input type="hidden" class="work_phone" name="dependent[' + key + '][work_phone]" />\
						<input type="hidden" class="birthday" name="dependent[' + key + '][birthday]" />\
						<input type="hidden" class="dependent_status" name="dependent[' + key + '][dependent_status]" />\
						<input type="hidden" class="phone" name="dependent[' + key + '][phone]" />\
						<input type="hidden" class="cell_phone" name="dependent[' + key + '][cell_phone]" />\
						<input type="hidden" class="additional_info" name="dependent[' + key + '][additional_info]" />\
						<input type="hidden" class="street" name="dependent[' + key + '][street]" />\
						<input type="hidden" class="city" name="dependent[' + key + '][city]" />\
						<input type="hidden" class="country" name="dependent[' + key + '][country]" />\
						<input type="hidden" class="state" name="dependent[' + key + '][state]" />\
						<input type="hidden" class="zip" name="dependent[' + key + '][zip]" />\
					</div>\
				</div>');

				$("#dependent_" + key).editable({
			        validate: function(value) {
			            if(value.first_name == '') return 'First name is required!'; 
						if(value.last_name == '') return 'Last name is required!';
			        },
			        placement: 'left',
		        	emptytext: 'Add Dependent Info'
			    });

				setTimeout(function() {
			    	$("#dependent_" + key).click();
			    }, 100);
			});

			$("#address").editable({
		        validate: function(value) {
		            // if(value.street == '') return 'Street is required!'; 
		            // if(value.city == '') return 'City is required!'; 
		            // if(value.country == '') return 'Country is required!';
		            // if(value.state == '') return 'State is required!'; 
		            // if(value.zip == '') return 'Zip Code is required!'; 
		        },
		        placement: 'top',
		        emptytext: 'Add Address'
		    });

		    if (typeof(parent.exists) != "undefined") {
		    	$("nav.navbar, .breadcrumb").hide();
		    	$('form#edit-client-form').append('<input type="hidden" name="parent_exists" value="1" />');
		    	$("html, body").css("overflow-x", "hidden");
		    	$(".panel-footer").hide();
		    }
		});
	</script>
@stop