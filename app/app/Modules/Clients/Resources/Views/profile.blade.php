@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/clients">Clients</a></li>
        <li class="active">{{$client->name}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>{{$client->name}} - Profile</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
        @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_edit"))
		  <a class="btn btn-success m-t-10" href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getEditClient', array($client->id)) }}"><i class="fa fa-pencil"></i> Edit Client</a>
        @endif
	</div>

	<div class="clearfix"></div>

	<div class="tabcordion">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_plans"))
                <li class=""><a href="#plans" data-toggle="tab">Plans</a></li>
            @endif
            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_enrollees"))
                <li class=""><a href="#employees" data-toggle="tab">Enrolees</a></li>
            @endif
            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_documents"))
                <li class=""><a href="#documents" data-toggle="tab">Documents</a></li>
            @endif
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="profile">
                <div class="row p-20">
                    <div class="col-md-6">
                        <h3 class="m-t-0">Basic Information</h3>
                        <form class="form-horizontal p-20">
                            <div class="form-group">
                                <div class="col-sm-4">Name:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->name}}</strong>
                                    @if ($client->status == "active")
                                        <span class="label label-success">Active</span>
                                     @elseif ($client->status == "inactive")
                                        <span class="label label-warning">Inactive</span>
                                     @elseif ($client->status == "cancelled")
                                        <span class="label label-danger">Cancelled</span>
                                     @elseif ($client->status == "archived")
                                        <span class="label label-default">Archived</span>
                                     @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">Tax ID:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->tax_id}}</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">Memo:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->memo}}</strong>
                                </div>
                            </div>
                        </form>

                        <h3 class="m-t-0">Contract Information</h3>
                        <form class="form-horizontal p-20">
                            <div class="form-group">
                                <div class="col-sm-4">Contract Type:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->contract_type}}</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">Contract Period:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->contract_period_from}} - {{$client->contract_period_to}}</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">Renewal Period:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->renewal_period_from}} - {{$client->renewal_period_to}}</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">Employee Head Count:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->employee_head_count}}</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">Invoice Type:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->invoice_type}}</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">Employee Fee:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{{$client->employee_fee}}</strong>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h3 class="m-t-0">Address Information</h3>
                        <form class="form-horizontal p-20">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    Primary Office Address:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{!! $client->getPrimaryAddress(2) !!}</strong>
                                </div>
                            </div>
                            @if ($client->secondary_address_available)
                            <div class="form-group">
                                <div class="col-sm-4">
                                    Secondary Office Address:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{!! $client->getSecondaryAddress(2) !!}</strong>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="col-sm-4">
                                    Mailing Address:
                                </div>
                                <div class="col-sm-8">
                                    <strong>{!! $client->getMailingAddress(2) !!}</strong>
                                </div>
                            </div>
                        </form>
                        <h3 class="m-t-0">Contact Information</h3>
                        @foreach ($client->contacts()->get() as $contact)
                        	{!! $contact->getDisplay() !!}
                        	<br /><br />
                        @endforeach
                    </div>
                </div>
            </div>
            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_plans"))
                <div class="tab-pane fade users" id="plans">
                    <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                        <div class="filter-checkbox">
                            <a href="#" class="btn btn-danger btn-delete">Delete</a>
                            <a href="{{action('\App\Modules\Clients\Http\Controllers\ClientsController@getAddPlan', array($client->id))}}" class="btn btn-primary">Create Plan</a>
                        </div>
                        <table id="plans-table" class="table table-tools table-striped">
                            <thead>
                                <tr>
                                    <th style="min-width:50px">
                                        <input type="checkbox" class="check_all"/>
                                    </th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
    							foreach ($client->plans()->get() as $plan) { ?>
    								<tr class="odd gradeX">
    									<td>
    										<div class="pull-left" style="padding-top: 10px;">
    											<input type="checkbox" class="batch_id" value="{{$plan->id}}" />
    										</div>
    										<div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
    											<?php echo $plan->name; ?>
    										</div>
    									</td>
    									<td style="text-align: right;">
    										<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getPlanProfile', array($plan->id)) }}" class="btn bg-blue btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Editions"><i class="fa fa-eye"></i></a>
    										<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getEditPlan', array($plan->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Plan"><i class="fa fa-pencil"></i></a>
    										<a href="#" class="btn bg-red btn-delete-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Plan"><i class="fa fa-times"></i></a>
    									</td>
    								</tr>
    							<?php 
    								}
    							?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>

                    <script type="text/javascript">
    					$(document).ready(function() {
    						var opt = {};
    				        // Tools: export to Excel, CSV, PDF & Print
    				        opt.sDom = "<'row m-t-10'<'col-md-6'><'col-md-6'Tf>r>t<'row'<'col-md-6'><'col-md-6 align-right'p>>",
    				        opt.oLanguage = { "sSearch": "","sZeroRecords": "No plans found" } ,
    				        opt.iDisplayLength = 15,
    				        opt.oTableTools = {
    				            /*"sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",*/
    				            "aButtons": []
    				        };
                            opt.bSort = false;

    				        var oTable = $('#plans-table').dataTable(opt);
    				        oTable.fnDraw();

    				        $("#plans-table tr th:eq(0)").removeClass("sorting_asc").removeClass("sorting_desc");

    				        /* Add a placeholder to searh input */
    				        $('.dataTables_filter input').attr("placeholder", "Search...");

    				        /* Delete a product */
    				        $('#plans-table a.btn-delete-single').live('click', function (e) {
    				            e.preventDefault();
    				            if (confirm("Are you sure to delete this plan?") == false) {
    				                return;
    				            }
    				            var nRow = $(this).parents('tr')[0];
    				            oTable.fnDeleteRow(nRow);

    				            var ids = [$(this).parents("tr").find('.batch_id').val()];

    				            $.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeletePlans') }}", {
    				        		ids: ids
    				        	}, function() {
    				        		for (var i in nRows)
    				            		oTable.fnDeleteRow(nRows[i]);
    				        	});
    				        });

    				        $(".btn-delete").click(function(e) {
                                e.preventDefault();
    				        	var ids = [];
    				        	var nRows = [];

    				        	var nRow = $(this).parents('tr')[0];

    				        	$(".batch_id:checked").each(function() {
    				        		ids.push($(this).val());
    				        		nRows.push($(this).parents('tr')[0]);
    				        	});

    				        	if (ids.length == 0) {
    				        		alert('No plans were selected.');
    				        		return;
    				        	}

    				        	if (confirm('Are you sure you want to delete the selected plans?')) {
    					        	$.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeletePlans') }}", {
    					        		ids: ids
    					        	}, function() {
    					        		for (var i in nRows)
    					            		oTable.fnDeleteRow(nRows[i]);
    					        	});
    				        	}
    				        });
    				    });
    			    </script>
    			</div>
            @endif

            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_enrollees"))
                <div class="tab-pane fade users" id="employees">
                    <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                        <div class="filter-checkbox">
                            <a href="#" class="btn btn-danger btn-delete-employee">Delete</a>
                            <a href="{{action('\App\Modules\Clients\Http\Controllers\ClientsController@getAddEmployee', array($client->id))}}" class="btn btn-primary">Add Enrollee</a>
                        </div>
                        <table id="employees-table" class="table table-tools table-striped">
                            <thead>
                                <tr>
                                    <th style="min-width:50px">
                                        <input type="checkbox" class="check_all"/>
                                    </th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
    							foreach ($client->employees()->get() as $employee) { ?>
    								<tr class="odd gradeX">
    									<td>
    										<div class="pull-left" style="padding-top: 10px;">
    											<input type="checkbox" class="batch_id" value="{{$employee->id}}" />
    										</div>
    										<div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
    											<?php echo $employee->first_name; ?> <?php echo $employee->last_name; ?>
                                                ({{@$employee->type}})
    										</div>
    									</td>
    									<td style="text-align: right;">
    										<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getEmployeeProfile', array($employee->id)) }}" class="btn bg-blue btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="View  @if ($employee->type == 'employee') Employee @else Dependent @endif Profile"><i class="fa fa-eye"></i></a>
    										<a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getEditEmployee', array($employee->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
    										<a href="#" class="btn bg-red btn-delete-employee-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Employee"><i class="fa fa-times"></i></a>
    									</td>
    								</tr>
    							<?php 
    								}
    							?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>

                    <script type="text/javascript">
    					$(document).ready(function() {
    						var opt = {};
    				        // Tools: export to Excel, CSV, PDF & Print
    				        opt.sDom = "<'row m-t-10'<'col-md-6'><'col-md-6'Tf>r>t<'row'<'col-md-6'><'col-md-6 align-right'p>>",
    				        opt.oLanguage = { "sSearch": "","sZeroRecords": "No employees found" } ,
    				        opt.iDisplayLength = 15,
    				        opt.oTableTools = {
    				            /*"sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",*/
    				            "aButtons": []
    				        };
    				        opt.bSort = false;

    				        var oTable = $('#employees-table').dataTable(opt);
    				        oTable.fnDraw();

    				        $("#employees-table tr th:eq(0)").removeClass("sorting_asc").removeClass("sorting_desc");

    				        /* Add a placeholder to searh input */
    				        $('.dataTables_filter input').attr("placeholder", "Search...");

    				        /* Delete a product */
    				        $('#employees-table a.btn-delete-employee-single').live('click', function (e) {
    				            e.preventDefault();
    				            if (confirm("Are you sure to delete this employee?") == false) {
    				                return;
    				            }
    				            var nRow = $(this).parents('tr')[0];
    				            oTable.fnDeleteRow(nRow);

    				            var ids = [$(this).parents("tr").find('.batch_id').val()];

    				            $.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeleteEmployees') }}", {
    				        		ids: ids
    				        	}, function() {
    				        		for (var i in nRows)
    				            		oTable.fnDeleteRow(nRows[i]);
    				        	});
    				        });

    				        $(".btn-delete-employee").click(function(e) {
                                e.preventDefault();
    				        	var ids = [];
    				        	var nRows = [];

    				        	var nRow = $(this).parents('tr')[0];

    				        	$(".batch_id:checked").each(function() {
    				        		ids.push($(this).val());
    				        		nRows.push($(this).parents('tr')[0]);
    				        	});

    				        	if (ids.length == 0) {
    				        		alert('No employees were selected.');
    				        		return;
    				        	}

    				        	if (confirm('Are you sure you want to delete the selected employees?')) {
    					        	$.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeleteEmployees') }}", {
    					        		ids: ids
    					        	}, function() {
    					        		for (var i in nRows)
    					            		oTable.fnDeleteRow(nRows[i]);
    					        	});
    				        	}
    				        });
    				    });
    			    </script>
                </div>
            @endif

            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_clients_documents"))
                <div class="tab-pane fade users" id="documents">
                    <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                        <div class="filter-checkbox">
                            <a href="#" class="btn btn-danger btn-delete-document">Delete</a>
                            <a href="{{action('\App\Modules\Clients\Http\Controllers\ClientsController@getAddDocument', array($client->id))}}" class="btn btn-primary">Upload Document</a>
                        </div>
                        <table id="documents-table" class="table table-tools table-striped">
                            <thead>
                                <tr>
                                    <th style="min-width:50px">
                                        <input type="checkbox" class="check_all"/>
                                    </th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($client->documents()->get() as $document) { ?>
                                    <tr class="odd gradeX">
                                        <td>
                                            <div class="pull-left" style="padding-top: 10px;">
                                                <input type="checkbox" class="batch_id" value="{{$document->id}}" />
                                            </div>
                                            <div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
                                                <strong><?php echo $document->file; ?></strong>
                                                @if ($document->description)
                                                    <br />{{$document->description}}
                                                @endif
                                            </div>
                                        </td>
                                        <td style="text-align: right;">
                                            <a href="{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@getDownloadDocument', array($document->id)) }}" class="btn bg-blue btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download"><i class="fa fa-download"></i></a>
                                            <a href="#" class="btn bg-red btn-delete-document-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Document"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                <?php 
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            var opt = {};
                            // Tools: export to Excel, CSV, PDF & Print
                            opt.sDom = "<'row m-t-10'<'col-md-6'><'col-md-6'Tf>r>t<'row'<'col-md-6'><'col-md-6 align-right'p>>",
                            opt.oLanguage = { "sSearch": "","sZeroRecords": "No documents found" } ,
                            opt.iDisplayLength = 15,
                            opt.oTableTools = {
                                /*"sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",*/
                                "aButtons": []
                            };
                            opt.bSort = false;

                            var oTable = $('#documents-table').dataTable(opt);
                            oTable.fnDraw();

                            $("#documents-table tr th:eq(0)").removeClass("sorting_asc").removeClass("sorting_desc");

                            /* Add a placeholder to searh input */
                            $('.dataTables_filter input').attr("placeholder", "Search...");

                            /* Delete a product */
                            $('#documents-table a.btn-delete-document-single').live('click', function (e) {
                                e.preventDefault();
                                if (confirm("Are you sure to delete this document?") == false) {
                                    return;
                                }
                                var nRow = $(this).parents('tr')[0];
                                oTable.fnDeleteRow(nRow);

                                var ids = [$(this).parents("tr").find('.batch_id').val()];

                                $.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeleteDocuments') }}", {
                                    ids: ids
                                }, function() {
                                    for (var i in nRows)
                                        oTable.fnDeleteRow(nRows[i]);
                                });
                            });

                            $(".btn-delete-document").click(function(e) {
                                e.preventDefault();
                                var ids = [];
                                var nRows = [];

                                var nRow = $(this).parents('tr')[0];

                                $(".batch_id:checked").each(function() {
                                    ids.push($(this).val());
                                    nRows.push($(this).parents('tr')[0]);
                                });

                                if (ids.length == 0) {
                                    alert('No documents were selected.');
                                    return;
                                }

                                if (confirm('Are you sure you want to delete the selected documents?')) {
                                    $.post("{{ action('\App\Modules\Clients\Http\Controllers\ClientsController@postDeleteDocuments') }}", {
                                        ids: ids
                                    }, function() {
                                        for (var i in nRows)
                                            oTable.fnDeleteRow(nRows[i]);
                                    });
                                }
                            });
                        });
                    </script>
                </div>
            @endif
        </div>
    </div>

    <script type="text/javascript">
    	$(document).ready(function() {
    		var exploded = window.location.href.split("#");
    		if (exploded.length > 1)
    			$('a[href="#' + exploded[1] + '"]').click();
    	});
    </script>
@stop