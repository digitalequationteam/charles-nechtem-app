<?php

namespace App\Modules\Clients\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ClientsController extends Controller
{	
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

	public function getIndex() {
        $clients = \Client::where("id", ">", 0);

        if (!isset($_GET["statuses"]))
            $_GET["statuses"] = array("active", "cancelled");

        $appends = array();
        foreach ($_GET as $key => $value) {
            if ($key != "applyFilters" && !empty($value)) {
                $appends[$key] = $value;

                switch ($key) {
                    case 'name';
                        $clients = $clients->where("name", 'LIKE', '%'.$value.'%');
                    break;
                    case 'street';
                        $clients = $clients->where("primary_street", 'LIKE', '%'.$value.'%');
                    break;
                    case 'city';
                        $clients = $clients->where("primary_city", 'LIKE', '%'.$value.'%');
                    break;
                    case 'country';
                        $clients = $clients->where("primary_country", 'LIKE', '%'.$value.'%');
                    break;
                    case 'state';
                        $clients = $clients->where("primary_state", 'LIKE', '%'.\Common::getStateCode($value).'%');
                    break;
                    case 'zip';
                        $clients = $clients->where("primary_zip", 'LIKE', '%'.$value.'%');
                    break;
                    case 'contract_type';
                        $clients = $clients->whereIn("contract_type", $value);
                    break;
                    case 'statuses';
                        $clients = $clients->whereIn("status", $value);
                    break;
                    case 'range';
                        $z = new \ZipCode;
                        $zips_arr = array();
                        $zips = $z->get_zips_in_range(\Input::get("zip"), $value, _ZIPS_SORT_BY_DISTANCE_ASC, true); 

                        foreach ((array)$zips as $key3 => $zip)
                            $zips_arr[] = $key3;

                        $clients = $clients->whereIn("primary_zip", $zips_arr);
                    break;
                }
            }
        }

        if (\Input::get("sort_by")) {
            if (\Input::get("sort_by") == "head_count")
                $clients = $clients->addSelect(\DB::raw("(SELECT COUNT(`client_employees`.`id`) FROM `client_employees` WHERE `client_employees`.`client_id` = `clients`.`id`)  AS `head_count`, clients.*"));

            $clients = $clients->orderBy(\Input::get("sort_by"), \Input::get("sort_type"));
        }


        $page_size = (\Input::get("page_size")) ? \Input::get("page_size") : 10;
        if ($page_size != "All") {
            $clients = $clients->paginate($page_size);
            $clients_paginator = $clients;
            $clients_paginator = $clients_paginator->appends($appends);
        }
        else {
            $clients = $clients->get();
        }

        if (\Input::get("page") && $clients->count() == 0)
            return redirect($clients_paginator->url($clients_paginator->lastPage()));

        return view('clients::index', array('clients' => $clients, 'clients_paginator' => @$clients_paginator));
	}

	public function getAddClient() {
        return view('clients::add');
	}

    public function getEditClient($id = null)
    {
        $client = \Client::where('id', $id)->get()->first();

        $contacts = array();

        foreach ($client->contacts()->get() as $contact) {
            $contacts[] = array(
                "id" => $contact->id,
                "title" => $contact->title,
                "first_name" => $contact->first_name,
                "last_name" => $contact->last_name,
                "email" => $contact->email,
                "phone" => $contact->phone,
                "cell_phone" => $contact->cell_phone,
                "fax" => $contact->fax,
                "additional_info" => $contact->additional_info
            );
        }

        return view('clients::edit', array( 'client' => $client, 'contacts' => $contacts ));
    }

    public function getClientProfile($id = null)
    {
        $client = \Client::where('id', $id)->get()->first();

        return view('clients::profile', array( 'client' => $client));
    }

    public function postSaveClient($id = null)
    {
        $_POST["primary_address"] = \Input::get("primary_street").\Input::get("primary_city").\Input::get("primary_country").\Input::get("primary_state").\Input::get("primary_zip");
        \Input::replace($_POST);

        $rules = \App\Modules\Clients\Validators\ClientsValidator::$addRules;

        if (\Input::get("secondary_address_available")) {
            $_POST["secondary_address"] = \Input::get("secondary_street").\Input::get("secondary_city").\Input::get("secondary_country").\Input::get("secondary_state").\Input::get("secondary_zip");
            \Input::replace($_POST);
            $rules["secondary_address"] = "required";
        }

        if (!\Input::get("mailing_same_as_primary")) {
            $_POST["mailing_address"] = \Input::get("mailing_street").\Input::get("mailing_city").\Input::get("mailing_country").\Input::get("mailing_state").\Input::get("mailing_zip");
            \Input::replace($_POST);
            $rules["mailing_address"] = "required";
        }

        $validator = \Validator::make( \Input::all(), $rules );

        if ( $validator->passes() ) {
            if ($id == null) {
                $client = new \Client;
                $client->user_id = \Auth::user()->id;
            }
            else
                $client = \Client::find($id);

            $client->name = \Input::get( 'name' );
            $client->tax_id = \Input::get( 'tax_id' );
            $client->memo = \Input::get( 'memo' );

            $client->primary_street = \Input::get( 'primary_street' );
            $client->primary_city = \Input::get( 'primary_city' );
            $client->primary_country = \Input::get( 'primary_country' );
            $client->primary_state = \Input::get( 'primary_state' );
            $client->primary_zip = \Input::get( 'primary_zip' );

            if (\Input::get( 'secondary_address_available' ))
                $client->secondary_address_available = 1;
            else
                $client->secondary_address_available = 0;

            $client->secondary_street = \Input::get( 'secondary_street' );
            $client->secondary_city = \Input::get( 'secondary_city' );
            $client->secondary_country = \Input::get( 'secondary_country' );
            $client->secondary_state = \Input::get( 'secondary_state' );
            $client->secondary_zip = \Input::get( 'secondary_zip' );

            if (\Input::get( 'mailing_same_as_primary' ))
                $client->mailing_same_as_primary = 1;
            else
                $client->mailing_same_as_primary = 0;

            $client->mailing_street = \Input::get( 'mailing_street' );
            $client->mailing_city = \Input::get( 'mailing_city' );
            $client->mailing_country = \Input::get( 'mailing_country' );
            $client->mailing_state = \Input::get( 'mailing_state' );
            $client->mailing_zip = \Input::get( 'mailing_zip' );

            $client->contract_type = \Input::get( 'contract_type' );
            $client->contract_type_custom = (string)\Input::get( 'contract_type_custom' );
            $client->contract_period_from = \Input::get( 'contract_period_from' );
            $client->contract_period_to = \Input::get( 'contract_period_to' );
            $client->renewal_period_from = \Input::get( 'renewal_period_from' );
            $client->renewal_period_to = \Input::get( 'renewal_period_to' );
            $client->employee_head_count = (int)\Input::get( 'employee_head_count' );
            $client->invoice_type = (string)\Input::get( 'invoice_type' );
            $client->employee_fee = \Input::get( 'employee_fee' );


            if (\Input::get( 'status' ))
                $client->status = \Input::get( 'status' );

            if ($id == null)
                $client->status = "active";

            $client->save();

            foreach ($client->contacts()->get() as $contact) {
                $contact_removed = true;

                foreach ((array)\Input::get("contact") as $contact2) {
                    if (@$contact2['id'] == $contact->id) {
                        $contact_removed = false;
                    }
                }

                if ($contact_removed) {
                    $contact->delete();
                }
            }

            foreach ((array)\Input::get("contact") as $contact) {
                if (!empty($contact['first_name'])) {
                    if (isset($contact['id']))
                        $new_contact = $client->contacts()->where("id", $contact['id'])->get()->first();
                    else
                        $new_contact = new \Contact;

                    $new_contact->owner_id = $client->id;
                    $new_contact->title = @$contact['title'];
                    $new_contact->first_name = @$contact['first_name'];
                    $new_contact->last_name = @$contact['last_name'];
                    $new_contact->email = @$contact['email'];
                    $new_contact->phone = @$contact['phone'];
                    $new_contact->cell_phone = @$contact['cell_phone'];
                    $new_contact->fax = @$contact['fax'];
                    $new_contact->additional_info = @$contact['additional_info'];
                    $new_contact->save();
                }
            }

            if ($id == null) {
                \SecurityLog::addNew("add_client", \Auth::user()->getName()." added a new client.");
                return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', $client->id )->with( 'message', 'New client saved!' );
            }
            else {
                \SecurityLog::addNew("update_client", \Auth::user()->getName()." updated client <strong>".$client->name."</strong>.");
                return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', $client->id )->with( 'message', 'Client details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteClients()
    {
        foreach (\Input::get("ids") as $id) {
            $client = \Client::where('id', $id)->get()->first();
            if ($client) {
                $client->delete();
                \SecurityLog::addNew("delete_client", \Auth::user()->getName()." deleted client <strong>".$client->name."</strong>.");
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getAddPlan($client_id) {
        $client = \Client::where("id", $client_id)->get()->first();
        return view('clients::plans.add', array("client" => $client));
    }

    public function getEditPlan($id = null)
    {
        $plan = \ClientPlan::where('id', $id)->get()->first();
        $client = $plan->client()->get()->first();

        return view('clients::plans.edit', array( 'plan' => $plan, 'client' => $client ));
    }

    public function getPlanProfile($id = null)
    {
        $plan = \ClientPlan::where('id', $id)->get()->first();
        $client = $plan->client()->get()->first();

        return view('clients::plans.profile', array( 'plan' => $plan, 'client' => $client ));
    }

    public function postSavePlan($client_id, $id = null)
    {
        $validator = \Validator::make( \Input::all(), \App\Modules\Clients\Validators\ClientPlansValidator::$addRules );

        if ( $validator->passes() ) {
            if ($id == null)
                $plan = new \ClientPlan;
            else
                $plan = \ClientPlan::find($id);

            $plan->client_id = $client_id;
            if ($id == null)
                $plan->type = \Input::get( 'type' );

            $plan->name = \Input::get( 'name' );
            $plan->save();

            $client = $plan->client()->get()->first();

            if ($id == null) {
                \SecurityLog::addNew("add_client_plan", \Auth::user()->getName()." added a new plan for client <strong>".$client->name."</strong>.");
                return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($client_id, "#plans") )->with( 'message', 'New plan saved!' );
            }
            else {
                \SecurityLog::addNew("updated_client_plan", \Auth::user()->getName()." updated plan <strong>".$plan->name."</strong> for client <strong>".$client->name."</strong>.");
                return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($client_id, "#plans") )->with( 'message', 'Plan details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeletePlans()
    {
        foreach (\Input::get("ids") as $id) {
            $plan = \ClientPlan::where('id', $id)->get()->first();
            if ($plan) {
                $client = $plan->client()->get()->first();
                \SecurityLog::addNew("delete_client_plan", \Auth::user()->getName()." deleted plan from client <strong>".$client->name."</strong>.");
                $plan->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getAddEdition($id) {
        $plan = \ClientPlan::where('id', $id)->get()->first();
        $client = $plan->client()->get()->first();
        return view('clients::plans.editions.add', array("plan" => $plan, 'client' => $client));
    }

    public function getEditEdition($id = null)
    {
        $edition = \ClientPlanEdition::where('id', $id)->get()->first();
        $plan = $edition->plan()->get()->first();
        $client = $plan->client()->get()->first();

        return view('clients::plans.editions.edit', array( 'edition' => $edition, "plan" => $plan, 'client' => $client ));
    }

    public function postSaveEdition($client_plan_id, $id = null)
    {
        $plan = \ClientPlan::where('id', $client_plan_id)->get()->first();

        if ($plan->type == "A")
            $rules = \App\Modules\Clients\Validators\ClientPlanEditionsValidator::$aRules;
        else
            $rules = \App\Modules\Clients\Validators\ClientPlanEditionsValidator::$bRules;

        if ($id) {
            $rules['year'] = 'required|unique:client_plan_editions,year,' . $id;
        }

        $validator = \Validator::make( \Input::all(), $rules );

        if ( $validator->passes() ) {
            if ($id == null)
                $edition = new \ClientPlanEdition;
            else
                $edition = \ClientPlanEdition::find($id);

            $edition->client_plan_id = $client_plan_id;
            $edition->year = \Input::get("year");

            if ($plan->type == "A") {
                $edition->inin_copay_day = \Input::get("inin_copay_day");
                $edition->inin_copay_admission = \Input::get("inin_copay_admission");
                $edition->inin_php_copay_day = \Input::get("inin_php_copay_day");
                $edition->inin_php_copay_admission = \Input::get("inin_php_copay_admission");
                $edition->inou_copay = \Input::get("inou_copay");
                $edition->inouopm_individual = \Input::get("inouopm_individual");
                $edition->inouopm_family = \Input::get("inouopm_family");
                $edition->inot_copay = \Input::get("inot_copay");
                $edition->onde_individual = \Input::get("onde_individual");
                $edition->onde_family = \Input::get("onde_family");
                $edition->onin_copay = \Input::get("onin_copay");
                $edition->onin_php_copay = \Input::get("onin_php_copay");
                $edition->onou_copay = \Input::get("onou_copay");
                $edition->onouopm_individual = \Input::get("onouopm_individual");
                $edition->onouopm_family = \Input::get("onouopm_family");
                $edition->onot_copay = \Input::get("onot_copay");
            }
            else {
                $edition->incp_copay = \Input::get("incp_copay");
                $edition->inde_single = \Input::get("inde_single");
                $edition->inde_employee_1 = \Input::get("inde_employee_1");
                $edition->inde_employee_2 = \Input::get("inde_employee_2");
                $edition->inou_single = \Input::get("inou_single");
                $edition->inou_employee_1 = \Input::get("inou_employee_1");
                $edition->inou_employee_2 = \Input::get("inou_employee_2");
                $edition->oncp_copay = \Input::get("oncp_copay");
                $edition->onde_single = \Input::get("onde_single");
                $edition->onde_employee_1 = \Input::get("onde_employee_1");
                $edition->onde_employee_2 = \Input::get("onde_employee_2");
                $edition->onou_single = \Input::get("onou_single");
                $edition->onou_employee_1 = \Input::get("onou_employee_1");
                $edition->onou_employee_2 = \Input::get("onou_employee_2");
            }

            $edition->save();

            $plan = $edition->plan()->get()->first();
            $client = $plan->client()->get()->first();

            if ($id == null) {
                \SecurityLog::addNew("add_client_plan_edition", \Auth::user()->getName()." added edition for plan <strong>".$plan->name."</strong> of client <strong>".$client->name."</strong>.");
                return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getPlanProfile', array($client_plan_id) )->with( 'message', 'New edition saved!' );
            }
            else {
                \SecurityLog::addNew("update_client_plan_edition", \Auth::user()->getName()." updated edition <strong>".$edition->year."</strong> for plan <strong>".$plan->name."</strong> of client <strong>".$client->name."</strong>.");
                return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getPlanProfile', array($client_plan_id) )->with( 'message', 'Edition details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteEditions()
    {
        foreach (\Input::get("ids") as $id) {
            $edition = \ClientPlanEdition::where('id', $id)->get()->first();
            if ($edition) {
                $plan = $edition->plan()->get()->first();
                $client = $plan->client()->get()->first();
                \SecurityLog::addNew("delete_client_plan_edition", \Auth::user()->getName()." deleted edition <strong>".$edition->year."</strong> for plan <strong>".$plan->name."</strong> of client <strong>".$client->name."</strong>.");
                $edition->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getAddEmployee($client_id = null) {
        $client = \Client::where("id", $client_id)->get()->first();

        return view('clients::employees.add', array("client" => $client));
    }

    public function getEditEmployee($id = null)
    {
        $employee = \ClientEmployee::where('id', $id)->get()->first();
        $client = \Client::where('id', $employee->client_id)->get()->first();

        $dependents = array();

        foreach ($employee->dependents()->get() as $dependent) {
            $dependents[] = array(
                "id" => $dependent->id,
                "first_name" => $dependent->first_name,
                "last_name" => $dependent->last_name,
                "gender" => $dependent->gender,
                "cell_phone" => $dependent->cell_phone,
                "home_phone" => $dependent->home_phone,
                "work_phone" => $dependent->work_phone,
                "birthday" => $dependent->birthday,
                "street" => $dependent->street,
                "city" => $dependent->city,
                "country" => $dependent->country,
                "state" => $dependent->state,
                "zip" => $dependent->zip,
                "dependent_status" => $dependent->dependent_status
            );
        }

        return view('clients::employees.edit', array( 'client' => $client,  'employee' => $employee, 'dependents' => $dependents ));
    }

    public function getEmployeeProfile($id = null)
    {
        $employee = \ClientEmployee::where('id', $id)->get()->first();
        $client = \Client::where('id', $employee->client_id)->get()->first();

        return view('clients::employees.profile', array( 'client' => $client, 'employee' => $employee));
    }

    public function postSaveEmployee($client_id = null, $id = null)
    {
        if (!$client_id)
            return Redirect::back()->withErrors(["Please select a client first."])->withInput();

        $_POST["address"] = \Input::get("street").\Input::get("city").\Input::get("country").\Input::get("state").\Input::get("zip");
        \Input::replace($_POST);

        $rules = \App\Modules\Clients\Validators\ClientEmployeesValidator::$addRules;

        $validator = \Validator::make( \Input::all(), $rules );

        if ( $validator->passes() ) {
            if ($id == null) {
                $employee = new \ClientEmployee;
                $employee->type = \Input::get( 'type' );
            }
            else
                $employee = \ClientEmployee::find($id);

            $employee->client_id = \Input::get('client_id');

            $employee->first_name = (string)\Input::get( 'first_name' );
            $employee->last_name = (string)\Input::get( 'last_name' );
            $employee->email = (string)\Input::get( 'email' );
            $employee->gender = (string)\Input::get( 'gender' );
            $employee->cell_phone = (string)\Input::get( 'cell_phone' );
            $employee->home_phone = (string)\Input::get( 'home_phone' );
            $employee->work_phone = (string)\Input::get( 'work_phone' );
            $employee->birthday = (\Input::get( 'birthday' )) ? date("Y-m-d", strtotime(\Input::get( 'birthday' ))) : null;

            $employee->street = (string)\Input::get( 'street' );
            $employee->city = (string)\Input::get( 'city' );
            $employee->country = (string)\Input::get( 'country' );
            $employee->state = (string)\Input::get( 'state' );
            $employee->zip = (string)\Input::get( 'zip' );

            if (!@$employee->parent_id) {
                $employee->ssn = \Input::get( 'ssn' );
                $employee->client_plan_id = \Input::get( 'client_plan_id' );
                $employee->insurance_network_id = \Input::get( 'insurance_network_id' );
                $employee->insurance_type = \Input::get( 'insurance_type' );
                $employee->plan_coverage_termination_date = \Input::get( 'plan_coverage_termination_date' );
                $employee->work_department = (string)\Input::get( 'work_department' );
            }
            else {
                $employee->dependent_status = (string)\Input::get( 'dependent_status' );
            }

            $employee->save();

            $employee->saveEverywhere();

            $client = $employee->client()->get()->first();

            if ($id == null) {
                if (\Input::get("parent_exists")) {
                    ?>
                        <script type="text/javascript">
                            document.domain = '<?php echo \Config::get("app.domain"); ?>';
                            parent.selectEnrollee(<?php echo $employee->id; ?>);
                        </script>
                    <?php
                    exit();
                }

                \SecurityLog::addNew("add_client_employee", \Auth::user()->getName()." added a new employee for client <strong>".$client->name."</strong>.");
                return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($client_id, "#employees") )->with( 'message', 'New employee saved!' );
            }
            else {
                if (\Input::get("parent_exists")) {
                    ?>
                        <script type="text/javascript">
                            document.domain = '<?php echo \Config::get("app.domain"); ?>';
                            parent.finishEditEmployee(<?php echo $employee->id; ?>);
                        </script>
                    <?php
                    exit();
                }

                \SecurityLog::addNew("update_client_employee", \Auth::user()->getName()." updated employee <strong>".$employee->first_name." ".$employee->last_name."</strong> of client <strong>".$client->name."</strong>.");
                return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($client_id, "#employees") )->with( 'message', 'Employee details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteEmployees()
    {
        foreach (\Input::get("ids") as $id) {
            $employee = \ClientEmployee::where('id', $id)->get()->first();
            if ($employee) {
                $client = $employee->client()->get()->first();
                \SecurityLog::addNew("delete_client_employee", \Auth::user()->getName()." deleted employee <strong>".$employee->first_name." ".$employee->last_name."</strong> of client <strong>".$client->name."</strong>.");
                $employee->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getAddDocument($client_id) {
        return view('clients::documents.add', array("client_id" => $client_id));
    }


    public function postSaveDocument($client_id, $id = null)
    {
        $validator = \Validator::make( \Input::all(), \App\Modules\Clients\Validators\ClientDocumentsValidator::$addRules );

        if ( $validator->passes() ) {
            $document = new \ClientDocument;

            $document->client_id = $client_id;

            if (\Input::file('file')) {
                $fileName = "";
                $destinationPath = 'uploads'; // upload path
                //$extension = \Input::file('file')->getClientOriginalExtension();
                //$fileName = rand(111111111,999999999).'.'.$extension;
                $fileName = \Input::file('file')->getClientOriginalName();
                \Input::file('file')->move($destinationPath, $fileName);
                $document->file = $fileName;
            }

            $document->description = \Input::get( 'description' );
            $document->save();

            $client = $document->client()->get()->first();
            \SecurityLog::addNew("add_client_document", \Auth::user()->getName()." uploaded document for client <strong>".$client->name."</strong>.");

            return Redirect::action( '\App\Modules\Clients\Http\Controllers\ClientsController@getClientProfile', array($client_id, "#documents") )->with( 'message', 'New document saved!' );
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteDocuments()
    {
        foreach (\Input::get("ids") as $id) {
            $document = \ClientDocument::where('id', $id)->get()->first();
            if ($document) {
                $client = $document->client()->get()->first();
                \SecurityLog::addNew("delete_client_document", \Auth::user()->getName()." deleted document from client <strong>".$client->name."</strong>.");
                $document->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function getDownloadDocument($id)
    {
        $document = \ClientDocument::where('id', $id)->get()->first();

        \SecurityLog::addNew("download_client_document", \Auth::user()->getName()." downloaded document <strong>".$document->file."</strong> from client <strong>".$client->name."</strong>.");

        return \Response::download(base_path()."/public/uploads/".$document->file, $document->file);
    }

    public function postWidgetSearchEnrollee() {
        $enrollees = \ClientEmployee::where(\DB::raw("CONCAT(first_name,' ',last_name)"), 'LIKE', '%'.\Input::get("search_text").'%')->get();

        return view('clients::employees.widgets.results_employee', array("enrollees" => $enrollees));
    }

    public function postGetEnrolleeData() {
        $enrollee = \ClientEmployee::where("id", \Input::get("enrollee_id"))->get()->first();

        if ($enrollee->parent_id == 0)
            $employee = $enrollee;
        else
            $employee = $enrollee->parent()->get()->first();

        return view('clients::employees.widgets.employee_data', array("enrollee" => $enrollee, "employee" => $employee));
    }

    public function postSaveLatLng()
    {
        if (\Input::get("only_me"))
            $client = \Client::where("id", ">", 0)->where("user_id", \Auth::user()->id);
        else
            $client = \Client::where("id", ">", 0);

        $client = $client->skip(\Input::get("client_nr"))->take(1)->get()->first();

        $client->lat = \Input::get("lat");
        $client->lng = \Input::get("lng");
        $client->save();

        return response()->json(array('status' => "ok"));
    }
}
