<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('claims', function($table)
        {
            $table->increments('id');
            $table->string('type', 255)->default("HCFA-1500"); //EAP or OMC
            $table->string('authorization_page_uid', 255)->nullable();
            $table->string('creator_name', 255);
            $table->string('caller_name', 255);
            $table->string('company_name', 255);
            $table->string('UID', 255); // Creator Initials + incrementing number
            $table->longtext('enrollee_data')->nullable(); //array of employee data and enrollee data
            $table->longtext('provider_data')->nullable(); //array of provider data
            $table->integer('creator_id')->nullable();
            $table->boolean('accept_assignment')->default(0);
            $table->string('status', 255)->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claims');
    }
}