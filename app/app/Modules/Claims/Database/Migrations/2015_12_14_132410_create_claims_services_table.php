<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsServicesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('claim_services', function($table)
        {
            $table->increments('id');
            $table->integer('claim_id')->unsigned();
            $table->foreign('claim_id')->references('id')->on('claims')->onUpdate('cascade')->onDelete('cascade');

            $table->string('icd_code', 255);
            $table->string('service', 255);
            $table->string('service_plan', 2);
            $table->date('start_date');
            $table->date('end_date');

            $table->decimal('charge', 10, 2)->default(0.00);
            $table->integer('units')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claim_services');
    }
}