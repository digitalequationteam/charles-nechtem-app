<?php
namespace App\Modules\Claims\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ClaimsServiceProvider extends ServiceProvider
{
	/**
	 * Register the Claims module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Claims\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Claims module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('claims', realpath(__DIR__.'/../Resources/Lang'));
		
		View::addNamespace('claims', base_path('resources/views/vendor/claims'));
		View::addNamespace('claims', realpath(__DIR__.'/../Resources/Views'));
	}
}
