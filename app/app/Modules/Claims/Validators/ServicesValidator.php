<?php

namespace App\Modules\Claims\Validators;

class ServicesValidator
{
	public static $addRules = array(
        'icd_code' => 'required',
        'service' => 'required|integer|digits:11',
        'service_place' => 'required|integer|digits:2',
        'start_date' => 'required',
        'end_date' => 'required',
        'charge' => 'required',
        'units' => 'required'

    );
}