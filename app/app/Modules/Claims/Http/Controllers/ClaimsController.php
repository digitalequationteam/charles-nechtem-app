<?php

namespace App\Modules\Claims\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ClaimsController extends Controller
{	
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

	public function getIndex() {
        $claims = \Claim::where("id", "!=", 0);

        $appends = array();
        foreach ($_GET as $key => $value) {
            if ($key != "applyFilters" && !empty($value)) {
                $appends[$key] = $value;

                switch ($key) {
                    case 'name';
                        $claims = $claims->where("caller_name", 'LIKE', '%'.$value.'%');
                    break;
                    case 'UID';
                        $claims = $claims->where("UID", 'LIKE', '%'.$value.'%');
                    break;
                    case 'authorization_uid';
                        $claims = $claims->where("authorization_page_uid", 'LIKE', '%'.$value.'%');
                    break;
                    case 'company_name';
                        $claims = $claims->where("company_name", 'LIKE', '%'.$value.'%');
                    break;
                    case 'street';
                        $claims = $claims->where("enrollee_data", 'LIKE', '%'.$value.'%');
                    break;
                    case 'city';
                        $claims = $claims->where("enrollee_data", 'LIKE', '%'.$value.'%');
                    break;
                    case 'country';
                        $claims = $claims->where("enrollee_data", 'LIKE', '%'.\Common::getCountryName($value).'%');
                    break;
                    case 'state';
                        $claims = $claims->where("enrollee_data", 'LIKE', '%'.\Common::getStateCode($value).'%');
                    break;
                    case 'zip';
                        $claims = $claims->where("enrollee_data", 'LIKE', '%'.$value.'%');
                    break;
                    case 'contract_type';
                        $claims = $claims->whereIn("contract_type", $value);
                    break;
                }
            }
        }

        $page_size = (\Input::get("page_size")) ? \Input::get("page_size") : 10;
        if ($page_size != "All") {
            $claims = $claims->paginate($page_size);
            $claims_paginator = $claims;
            $claims_paginator = $claims_paginator->appends($appends);
        }
        else {
            $claims = $claims->get();
        }

        if (\Input::get("page") && $claims->count() == 0)
            return redirect($claims_paginator->url($claims_paginator->lastPage()));

        return view('claims::index', array('claims' => $claims, "claims_paginator" => @$claims_paginator));
	}

	public function getAddClaim($authorization_id = null) {
		if ($authorization_id)
			$authorization_page = \CaseAuthorizationPage::where("UID", $authorization_id)->get()->first();

		if (@$authorization_page)
			$case = $authorization_page->ecase()->get()->first();

		$data = array("authorization_id" => $authorization_id);

		if (@$case->type == "OMC") {
			$data["authorization_uid"] = $authorization_id;
			$data["provider_data"] = $authorization_page->getProviderData();
			$data["enrollee_data"] = $case->getEnrolleeData();
		}

        return view('claims::add', $data);
	}

	public function getCreateClaim() {
        if (!$this->me->hasPermission($this->owner->type."_claims_add")) return view("errors.403");

        return view('claims::create');
	}

	public function postSearchAuthorizationId() {
		$authorization_page = \CaseAuthorizationPage::where("UID", \Input::get("authorization_uid"))->get()->first();

		if (!$authorization_page)
			return Redirect::back()->withErrors(["Authorization page not found."])->withInput();

		if ($authorization_page->ecase()->get()->first()->type != "OMC")
			return Redirect::back()->withErrors(["Must be an OMC case."])->withInput();

		return redirect("/claims/add-claim/".$authorization_page->UID);
	}

    public function getEditClaim($id = null)
    {
        $claim = \Claim::where('id', $id)->get()->first();

        return view('claims::edit', array( 'claim' => $claim));
    }

    public function getClaimProfile($id = null)
    {
        $claim = \Claim::where('id', $id)->get()->first();

        return view('claims::profile', array( 'claim' => $claim));
    }

    public function postSaveClaim($id = null)
    {
        if ($id == null) {
            if (!\Input::get("enrollee_id") && $id == null && !\Input::get("authorization_uid"))
                return Redirect::back()->withErrors(["No enrollee selected."])->withInput();

            if (!\Input::get("provider") && $id == null && !\Input::get("authorization_uid"))
                return Redirect::back()->withErrors(["No provider selected."])->withInput();
        }

        //$rules = \App\Modules\Claims\Validators\ClaimsValidator::$addRules;
        $rules = array();

        $validator = \Validator::make( \Input::all(), $rules );

        if ( $validator->passes() ) {
            if ($id == null) {
                $claim = new \Claim;
                $claim->type = \Input::get( 'type' );
                $claim->creator_id = \Auth::user()->id;
                $claim->user_id = \Auth::user()->id;
                $claim->creator_name = \Auth::user()->first_name." ".\Auth::user()->last_name;
            }
            else {
                $claim = \Claim::find($id);
            }

            if ($id == null) {
                if (\Input::get("authorization_uid")) {
                	$authorization_page = \CaseAuthorizationPage::where("UID", \Input::get("authorization_uid"))->get()->first();
                	$case = $authorization_page->ecase()->get()->first();

                	$claim->authorization_page_uid = $authorization_page->UID;
                	$claim->enrollee_data = json_encode($case->getEnrolleeData());
                	$claim->provider_data = json_encode($authorization_page->getProviderData());

                	$claim->caller_name = $case->getEnrolleeData()->enrollee->first_name." ".$case->getEnrolleeData()->enrollee->last_name;
                	$claim->company_name = $case->getEnrolleeData()->enrollee->client;
                }
                else {
    	            $enrollee = \ClientEmployee::where("id", \Input::get("enrollee_id"))->get()->first();

    	            if (\Input::get("enrollee_id")) {
    	                if ($enrollee->parent_id == 0)
    	                    $employee = $enrollee;
    	                else
    	                    $employee = $enrollee->parent()->get()->first();

    	                $claim->enrollee_data = json_encode(array(
    	                    "enrollee" => array(
    	                        "id" => $enrollee->id,
    	                        "first_name" => $enrollee->first_name,
    	                        "last_name" => $enrollee->last_name,
    	                        "gender" => $enrollee->gender,
    	                        "ssn" => $enrollee->ssn,
    	                        "cell_phone" => $enrollee->cell_phone,
    	                        "home_phone" => $enrollee->home_phone,
    	                        "work_phone" => $enrollee->work_phone,
    	                        "birthday" => $enrollee->birthday,
    	                        "plan" => @$employee->plan()->get()->first()->name,
    	                        "insurance_network" => @$employee->insurance_network()->get()->first()->name,
    	                        "insurance_type" => $employee->insurance_type,
    	                        "client" => $employee->client()->get()->first()->name,
    	                        "address" =>$enrollee->getPrimaryAddress(2)
    	                    ),
    	                    "employee" => array(
    	                        "first_name" => $employee->first_name,
    	                        "last_name" => $employee->last_name,
    	                        "gender" => $employee->gender,
    	                        "ssn" => $employee->ssn,
    	                        "cell_phone" => $employee->cell_phone,
    	                        "home_phone" => $employee->home_phone,
    	                        "work_phone" => $employee->work_phone,
    	                        "birthday" => $employee->birthday,
    	                        "plan" => @$employee->plan()->get()->first()->name,
    	                        "insurance_network" => @$employee->insurance_network()->get()->first()->name,
    	                        "insurance_type" => $employee->insurance_type,
    	                        "client" => $employee->client()->get()->first()->name,
    	                        "address" =>$employee->getPrimaryAddress(2)
    	                    )
    	                ));

    					$provider = \Provider::where("id", \Input::get("provider"))->get()->first();

    					$claim->provider_data = json_encode(array(
    		                "id" => $provider->id,
                            "name" => $provider->name,
    		                "address" => $provider->getPrimaryAddress(3),
    		                "pos_code" => $provider->pos_code()->get()->first()->code." - ".$provider->pos_code()->get()->first()->name
    		            ));

    		            $claim->caller_name = $enrollee->first_name." ".$enrollee->last_name;
    		            $claim->company_name = $enrollee->client()->get()->first()->name;
    	            }
    	        }
            }

            $claim->type = \Input::get( 'type' );
            $claim->accept_assignment = isset($_POST['accept_assignment']);

            $claim->save();

            if ($id == null) {
                $uid = $claim->id;
                $length = strlen($uid);

                while (strlen($uid) < 6)
                    $uid = "0".$uid;

                $claim->UID = $uid;
                $claim->save();
            }

            if ($id == null) {
                \SecurityLog::addNew("add_claim", \Auth::user()->getName()." added a new claim.");
                return Redirect::action( '\App\Modules\Claims\Http\Controllers\ClaimsController@getClaimProfile', $claim->id )->with( 'message', 'New case saved!' );
            }
            else {
                \SecurityLog::addNew("update_claim", \Auth::user()->getName()." updated claim <strong>".$claim->UID."</strong>.");
                return Redirect::action( '\App\Modules\Claims\Http\Controllers\ClaimsController@getClaimProfile', $claim->id )->with( 'message', 'Claim details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function getAssignAuthorizationToClaim($claim_id) {
        $claim = \Claim::where("id", $claim_id)->get()->first();

        return view('claims::assign_authorization', array("claim" => $claim));
    }

    public function postSaveAuthorizationToClaim() {
        $claim = \Claim::where("id", \Input::get("claim_id"))->get()->first();

        $authorization_page = \CaseAuthorizationPage::where("UID", \Input::get("authorization_uid"))->get()->first();

        if (!$authorization_page)
            return Redirect::back()->withErrors(["Authorization page not found."])->withInput();

        $case = $authorization_page->ecase()->get()->first();

        if ($case->type != "OMC")
            return Redirect::back()->withErrors(["Must be an OMC case."])->withInput();

        $claim->authorization_page_uid = $authorization_page->UID;
        $claim->enrollee_data = json_encode($case->getEnrolleeData());
        $claim->provider_data = json_encode($authorization_page->getProviderData());

        $claim->caller_name = $case->getEnrolleeData()->enrollee->first_name." ".$case->getEnrolleeData()->enrollee->last_name;
        $claim->company_name = $case->getEnrolleeData()->enrollee->client;
        $claim->save();

        \SecurityLog::addNew("assign_apage_to_claim", \Auth::user()->getName()." assigned authorization page <strong>".$authorization_page->UID."</strong> to claim <strong>".$claim->UID."</strong>.");

        return redirect("/claims/claim-profile/".$claim->id);
    }

    public function postDeleteClaims()
    {
        foreach (\Input::get("ids") as $id) {
            $claim = \Claim::where('id', $id)->get()->first();
            if ($claim) {
                \SecurityLog::addNew("delete_claim", \Auth::user()->getName()." deleted claim <strong>".$claim->UID."</strong>.");
                $claim->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }

    public function postChangeClaimStatus()
    {
        $claim = \Claim::where("id", \Input::get("id"))->get()->first();
        $claim->status = \Input::get("status");
        $claim->save();

        if (\Input::get("status") == "on_hold")
            \SecurityLog::addNew("claim_on_hold", \Auth::user()->getName()." put claim <strong>".$claim->UID."</strong> on hold.");
        elseif (\Input::get("status") == "rejected")
            \SecurityLog::addNew("claim_rejected", \Auth::user()->getName()." rejected claim <strong>".$claim->UID."</strong>.");

        return response()->json(array('status' => "ok"));
    }

    public function getAddService($claim_id) {
        if (!$this->me->hasPermission($this->owner->type."_claims_services")) return view("errors.403");

        $claim = \Claim::where("id", $claim_id)->get()->first();
        return view('claims::services.add', array("claim" => $claim));
    }

    public function getEditService($id = null)
    {
        if (!$this->me->hasPermission($this->owner->type."_claims_services")) return view("errors.403");

        $service = \ClaimService::where('id', $id)->get()->first();

        return view('claims::services.edit', array( 'service' => $service));
    }

    public function postSaveService($id = null)
    {
        if (!$this->me->hasPermission($this->owner->type."_claims_services")) return view("errors.403");

        $validator = \Validator::make( \Input::all(), \App\Modules\Claims\Validators\ServicesValidator::$addRules );

        if ( $validator->passes() ) {
            $claim = \Claim::where("id", $claim_id)->get()->first();

            if ($id == null)
                $service = new \ClaimService;
            else
                $service = \ClaimService::find($id);

            if ($id == null)
                $service->claim_id = \Input::get( 'claim_id' );

            $service->icd_code = \Input::get("icd_code");
            $service->service = \Input::get("service");
            $service->service_plan = \Input::get("service_place");
            $service->start_date = \Input::get("start_date");
            $service->end_date = \Input::get("end_date");
            $service->charge = \Input::get("charge");
            $service->units = \Input::get("units");
            $service->save();

            if ($id == null) {
                \SecurityLog::addNew("add_claim_service", \Auth::user()->getName()." added service for claim <strong>".$claim->UID."</strong>.");
                return Redirect::action( '\App\Modules\Claims\Http\Controllers\ClaimsController@getClaimProfile', $service->claim_id )->with( 'message', 'New service saved!' );
            }
            else {
                \SecurityLog::addNew("update_claim_service", \Auth::user()->getName()." updated service <strong>".$service->service."</strong> for claim <strong>".$claim->UID."</strong>.");
                return Redirect::action( '\App\Modules\Claims\Http\Controllers\ClaimsController@getClaimProfile', $service->claim_id )->with( 'message', 'Service details saved!' );
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function postDeleteServices()
    {
        if (!$this->me->hasPermission($this->owner->type."_claims_services")) return view("errors.403");
        
        foreach (\Input::get("ids") as $id) {
            $service = \ClaimService::where('id', $id)->get()->first();
            if ($service) {
                $claim = \Claim::where("id", $service->claim_id)->get()->first();
                \SecurityLog::addNew("delete_claim_service", \Auth::user()->getName()." deleted service <strong>".$service->service."</strong> from claim <strong>".$claim->UID."</strong>.");
                $service->delete();
            }
        }

        return response()->json(array('status' => "ok"));
    }
}