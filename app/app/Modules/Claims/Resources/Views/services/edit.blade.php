@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/claims">Claims</a></li>
        <li><a href="/claims/claim-profile/{{$service->claim()->get()->first()->id}}">{{$service->claim()->get()->first()->UID}}</a></li>
        <li class="active">Edit Service</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Edit Service</strong></h3>
	</div> <!-- / .page-header -->

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Claims\Http\Controllers\ClaimsController@postSaveService', $service->id)}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif

			<div class="col-md-12">
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Diagnosis</label>
					<div class="col-sm-4">
						<select name="icd_code" class="form-control">
							@foreach (\DictionaryValue::where("category", "icd_codes")->get() as $icd_code)
						    	<option value="{{$icd_code->code}} - {{$icd_code->name}}" @if ((old() & old('icd_code') == $icd_code->code." - ".$icd_code->name) || (!old() && $service->icd_code == $icd_code->code." - ".$icd_code->name)) selected @endif>{{$icd_code->code}} - {{$icd_code->name}}</option>
						    @endforeach
						</select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Service <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="number" class="form-control" maxlength="100" name="service" id="service" value="<?php echo (old()) ? Input::old('service') : $service->service; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Place of Service <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="number" class="form-control" maxlength="100" name="service_place" id="service_place" value="<?php echo (old()) ? Input::old('service_place') : $service->service_plan; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Service Start Date <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control date" maxlength="100" name="start_date" id="start_date" value="<?php echo (old()) ? Input::old('start_date') : $service->start_date; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Service End Date <span class="text-danger">*</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control date" maxlength="100" name="end_date" id="end_date" value="<?php echo (old()) ? Input::old('end_date') : $service->end_date; ?>" />
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Charge</label>
					<div class="col-sm-4">
						<div class="input-group transparent">
                            <span class="input-group-addon bg-grey">
                              <i class="fa fa-dollar"></i>
                            </span>
                            <input type="number" class="form-control" maxlength="100" name="charge" id="charge" value="<?php echo (old()) ? Input::old('charge') : $service->charge; ?>" />
                        </div>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Units</label>
					<div class="col-sm-4">
						<input type="number" class="form-control" maxlength="100" name="units" id="units" value="<?php echo (old()) ? Input::old('units') : $service->units; ?>" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Save</button>
				</div>
			</div>
		</div>
	</form>
@stop