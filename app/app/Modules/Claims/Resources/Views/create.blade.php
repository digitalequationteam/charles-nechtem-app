@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/claims">Claims</a></li>
        <li class="active">Add Claim</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Add Claim</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getIndex') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>
	-->

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Claims\Http\Controllers\ClaimsController@postSearchAuthorizationId')}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif

			<div class="col-md-12">
				<br />
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Authorization UID</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="authorization_uid" id="authorization_uid" value="{{old('authorization_uid')}}" />

						@include('cases::authorization-pages.widgets.select_authorization_uid')
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-check"></span>&nbsp;Confirm</button>
					<button type="button" class="btn btn-success btn-labeled" onclick="window.location = '/claims/add-claim/';"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Create From Scratch</button>
				</div>
			</div>
		</div>
	</form>
@stop