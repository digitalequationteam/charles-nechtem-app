@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li class="active">Claims</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Claims</strong></h3>
	</div> <!-- / .page-header -->

	<div class="pull-right">
		@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_add"))
			<a href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getCreateClaim') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span> Add Claim</a>
		@endif
	</div>

	<div class="clearfix"></div>

	<div class="tabcordion">
	    <div class="tab-content">
			<div class="row users">
				<div class="col-md-12">
					<div class="panel panel-default">
		                <div class="panel-body">
				        
		                    <div class="row">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
								@if(Session::has('message'))
							        <div class="alert alert-success">
							            <button type="button" class="close" data-dismiss="alert">×</button>
							            <span>
							                {{ Session::get('message') }}
							            </span>
								    </div>
							    @endif

								<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
				                    <table id="claims-table" class="table table-tools table-striped">
				                        <thead>
				                            <tr>
				                                <th style="min-width:50px">
				                                	<div class="pull-left">
				                                		@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_delete"))
				                                    		<input type="checkbox" class="check_all"/>
				                                    	@endif
				                                    </div>
				                                    <div class="pull-left" style="padding-left: 30px; margin-top: -12px;">
				                                    	@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_delete"))
															<a href="#" class="btn btn-danger btn-delete btn-xs" style="margin: 0px;">Delete</a>
														@endif
													</div>
				                                </th>
				                                <th></th>
				                                <th class="text-right" style="width: 300px; padding-top: 6px; font-size: 12px;">
				                                </th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                            <tr class="bg-gray-light filters-tr">
				                                <td colspan="3" style="padding-top: 6px;">
				                                	<div class="pull-left" style="padding-left: 8px;">
				                                		<button id="toggle-filters-btn" class="btn btn-danger btn-xm" onclick="toggleFilters();"><i class="fa fa-arrow-down"></i> Search Filters</button>
				                                	</div>

				                                	<div class="clearfix"></div>

				                                	<div id="filters-wrapper" style="padding-top: 10px; padding-bottom: 10px; @if (!\Input::get('applyFilters')) display: none; @endif">
		                                			<input type="hidden" name="page_size" value="{{\Input::get('page_size')}}" />
				                                		<form action="" method="get">
					                                		<div class="row">
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Enrollee Name</label>
																		<div>
																			<input class="form-control" type="text" name="name" value="{{\Input::get('name')}}" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Company</label>
																		<div>
																			<input class="form-control" type="text" name="company_name" value="{{\Input::get('company_name')}}" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-2">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Street</label>
																		<div>
																			<input type="text" class="form-control" maxlength="100" name="street" id="street" value="<?php echo Input::get('street'); ?>" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-2">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>City</label>
																		<div>
																			<input type="text" class="form-control" maxlength="100" name="city" id="city" value="<?php echo Input::get('city'); ?>" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-2">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Country</label>
																		<div>
																			<select class="form-control" name="country" id="country">
																				<?php foreach (\Common::getAllCountries() as $code => $name) {
																					?>
																					<option value="<?php echo $code; ?>" <?php if (\Input::get("country") == $code) { ?>selected="selected"<?php } ?>><?php echo $name; ?></option>
																					<?php
																				}
																				?>
																			</select>
																		</div>
																	</div>
						                                		</div>
						                                	</div>

						                                	<div class="row">
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>UID</label>
																		<div>
																			<input class="form-control" type="text" name="UID" value="{{\Input::get('UID')}}" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Authorization UID</label>
																		<div>
																			<input class="form-control" type="text" name="authorization_uid" value="{{\Input::get('authorization_uid')}}" />
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>State</label>
																		<div>
																			<select class="form-control" name="state" id="state">
																				<option value="">Select state...</option>
																				<?php if (Input::get("country")) { ?>
																					<?php foreach (\Common::getStates(Input::get("country")) as $state) {
																						?>
																						<option value="<?php echo $state; ?>" <?php if ($state == Input::get("state")) { ?>selected="selected"<?php } ?>><?php echo $state; ?></option>
																						<?php
																					}
																					?>
																				<?php } ?>
																			</select>
																		</div>
																	</div>
						                                		</div>
						                                		<div class="col-md-3">
																	<div class="form-group no-margin-hr panel-padding-h">
																		<label>Zip Code</label>
																		<div>
																			<input type="text" class="form-control" maxlength="100" name="zip" id="zip" value="<?php echo Input::get('zip'); ?>" />
																		</div>
																	</div>
						                                		</div>
						                                	</div>
						                                	<button class="btn btn-default" name="applyFilters" value="Yes">Apply Filters</button>
						                                	<button onclick="window.location = '/claims'; return false;" class="btn btn-default">Reset</button>
						                                </form>
				                                	</div>
				                                </td>
				                            </tr>
				                        	<?php
											foreach ($claims as $claim) { ?>
												<tr class="odd gradeX">
													<td>
														<div class="pull-left" style="padding-top: 10px;">
															@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_delete"))
																<input type="checkbox" class="batch_id" value="{{$claim->id}}" />
															@endif
														</div>
														<div class="pull-left" style="padding-left: 30px; padding-top: 2px;">
															<a href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getClaimProfile', array($claim->id)) }}">
																({{$claim->UID}})
															 	<strong>{{$claim->getEnrolleeData()->enrollee->first_name}} {{$claim->getEnrolleeData()->enrollee->last_name}}</strong>
															</a>
														</div>
													</td>
													<td>
														 {{$claim->company_name}}
													</td>
													<td style="text-align: center;">
														<a href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getClaimProfile', array($claim->id)) }}" class="btn bg-blue btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Profile"><i class="fa fa-eye"></i></a>
														@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_edit"))
															<a href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getEditClaim', array($claim->id)) }}" class="btn bg-green btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Claim"><i class="fa fa-pencil"></i></a>
														@endif
														@if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_delete"))
															<a href="#" class="btn bg-red btn-delete-single btn-sm tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Claim"><i class="fa fa-times"></i></a>
														@endif
													</td>
												</tr>
											<?php 
												}
											?>
				                        </tbody>
				                    </table>

				                    @if ($claims->count() == 0)
				                    	No claims found.
				                    @endif

				                    <div class="pull-left" style="padding-right: 10px; padding-top: 20px;">
				                    	Per page:
				                    </div>

				                    <div class="pull-left" style="padding-top: 15px;">
		                            	<select id="page-size-dd" class="form-control">
		                            		<option value="10" @if (\Input::get("page_size") == "10") selected @endif>10</option>
		                            		<option value="25" @if (\Input::get("page_size") == "25") selected @endif>25</option>
		                            		<option value="100" @if (\Input::get("page_size") == "100") selected @endif>100</option>
		                            		<option value="All" @if (\Input::get("page_size") == "All") selected @endif>All</option>
		                            	</select>
				                    </div>

				                    <div class="pull-right">
				                    	@if ($claims_paginator)
				                    		{!! $claims_paginator->render() !!}
				                    	@endif
				                    </div>
				                </div>
				            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
	        /* Delete a product */
	        $('#claims-table a.btn-delete-single').live('click', function (e) {
	            e.preventDefault();
	            if (confirm("Are you sure to delete this claim?") == false) {
	                return;
	            }

	            var ids = [$(this).parents("tr").find('.batch_id').val()];

	            $.post("{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@postDeleteClaims') }}", {
	        		ids: ids
	        	}, function() {
	        		window.location = window.location.href;
	        	});
	        });

	        $(".btn-delete").click(function(e) {
	            e.preventDefault();
	        	var ids = [];

	        	$(".batch_id:checked").each(function() {
	        		ids.push($(this).val());
	        	});

	        	if (ids.length == 0) {
	        		alert('No claims were selected.');
	        		return;
	        	}

	        	if (confirm('Are you sure you want to delete the selected claims?')) {
		        	$.post("{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@postDeleteClaims') }}", {
		        		ids: ids
		        	}, function() {
		        		window.location = window.location.href;
		        	});
	        	}
	        });

			$("#sort-by-dd, #sort-type-dd, #page-size-dd").change(function() {
				$('input[name="sort_by"]').val($("#sort-by-dd").val());
				$('input[name="sort_type"]').val($("#sort-type-dd").val());
				$('input[name="page_size"]').val($("#page-size-dd").val());
				$("#filters-wrapper form").submit();
			});
	    });

		function toggleFilters() {
			if ($('#filters-wrapper').is(":visible")) {
				$('#filters-wrapper').slideUp();
				$("#toggle-filters-btn i").removeClass("fa-arrow-up").addClass("fa-arrow-down");
			}
			else {
				$('#filters-wrapper').slideDown();
				$("#toggle-filters-btn i").removeClass("fa-arrow-down").addClass("fa-arrow-up");
			}
		}
    </script>
@stop