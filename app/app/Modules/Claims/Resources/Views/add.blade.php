@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/claims">Claims</a></li>
        <li class="active">Add Claim</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Add Claim</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getIndex') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span>&nbsp;Back</a>
	</div>
	-->

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Claims\Http\Controllers\ClaimsController@postSaveClaim')}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

		@if (@$authorization_uid)
			<input type="hidden" name="authorization_uid" value="{{$authorization_uid}}" />
		@endif
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif
			
			@include('clients::employees.widgets.select_employee')
			
			<br />
			@include('providers::widgets.select_provider')

			<div class="col-md-12">
				<br />
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Claim Type</label>
					<div class="col-sm-4">
						<select id="type" name="type" class="form-control">
							<option value="HCFA-1500" @if (\Input::old("type") == "HCFA-1500") selected @endif>HCFA-1500</option>
							<option value="UB-04" @if (\Input::old("type") == "UB-04") selected @endif>UB-04</option>
							<option value="UB-92" @if (\Input::old("type") == "UB-92") selected @endif>UB-92</option>
						</select>
					</div>
				</div>
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Accept Assignment</label>
					<div class="col-sm-4">
						<input type="checkbox" class="switch sw2" name="accept_assignment" id="accept_assignment" value="<?php echo Input::old('accept_assignment'); ?>" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-arrow-right"></span>&nbsp;Confirm and Enter Services</button>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$(".switch.sw2").bootstrapSwitch({
				onText: "YES",
				offText: "NO"
			});

			$('.switch.sw1').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (state.value)
			    	$(".for-secondary").show();
			    else
			    	$(".for-secondary").hide();
			});

			$('.switch.sw2').on('switchChange.bootstrapSwitch', function (event, state) {
			    if (!state.value)
			    	$(".for-mailing").show();
			    else
			    	$(".for-mailing").hide();
			});

			$("#add-new-contact").click(function() {
				var key = $("#contacts>.contact-item").length + 1;
				$("#contacts").append('<div class="contact-item form-group no-margin-hr panel-padding-h">\
					<label class="control-label col-sm-1">&nbsp;</label>\
					<div class="col-sm-8">\
						<a href="#" id="contact_' + key + '" data-type="contact" data-pk="1" data-title="Contact Info" style="display: block;"></a>\
						<input type="hidden" class="title" name="contact[' + key + '][title]" />\
						<input type="hidden" class="first_name" name="contact[' + key + '][first_name]" />\
						<input type="hidden" class="last_name" name="contact[' + key + '][last_name]" />\
						<input type="hidden" class="email" name="contact[' + key + '][email]" />\
						<input type="hidden" class="phone" name="contact[' + key + '][phone]" />\
						<input type="hidden" class="cell_phone" name="contact[' + key + '][cell_phone]" />\
						<input type="hidden" class="fax" name="contact[' + key + '][fax]" />\
						<input type="hidden" class="additional_info" name="contact[' + key + '][additional_info]" />\
					</div>\
				</div>');

				$("#contact_" + key).editable({
			        validate: function(value) {
			            if(value.first_name == '') return 'First name is required!'; 
			        },
			        placement: 'left',
		        	emptytext: 'Add Contact Info'
			    });

				setTimeout(function() {
			    	$("#contact_" + key).click();
			    }, 100);
			});

			$("#primary_address, #secondary_address, #mailing_address").editable({
		        validate: function(value) {
		            if(value.street == '') return 'Street is required!'; 
		            if(value.city == '') return 'City is required!'; 
		            if(value.country == '') return 'Country is required!';
		            if(value.state == '') return 'State is required!'; 
		            if(value.zip == '') return 'Zip Code is required!'; 
		        },
		        placement: 'left',
		        emptytext: 'Add Address'
		    });

		    $("#problem_types_editable").editable({
		        placement: 'left',
		        emptytext: 'No Problems',
		        source: {!!json_encode(\DictionaryValue::getAll("problem_types"))!!},
                success: function() {
                    setTimeout(function() {
                        $('input[name="problem_types"]').val($('#problem_types_editable').html().replace(/<br>/g, ", "));
                    }, 100);
                }
		    });
		});

		var substringMatcher = function(strs) {
		  return function findMatches(q, cb) {
		    var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		      if (substrRegex.test(str)) {
		        matches.push(str);
		      }
		    });

		    cb(matches);
		  };
		};

		var contact_titles = ['Administrator, Benefits/Risk', 'Administrator, HRS-Benefits services', 'Benefits Administrator', 'Benefits Contact Person', 'Benefits Coordinator', 'Benefits Manager', 'Benefits Specialist', 'CEO & President', 'Chief Operating Officer', 'Director, Compensation & Benefits', 'Director, Human resources', 'Director of Administrator Services', 'Director of Benefits', 'Director of Social Services', 'Director of Workers Compensation & Employee Health', 'Division of Labor-Management Relations', 'Employees Relations', 'Executive Director', 'Financial Director', 'Financial Officer', 'Group Business Manager', 'Human Resources Manager', 'Lead Manager, HR Department', 'Office Manager', 'President', 'Senior President', 'Training Director', 'Vice President', 'Vice President & Chief Financial Officer', 'Vice President of Human Resources', 'Vice President of Life Operations'];
	</script>
@stop