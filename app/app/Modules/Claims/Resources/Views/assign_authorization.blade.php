@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/claims">Claims</a></li>
        <li><a href="/claims/claim-profile/{{$claim->id}}">{{$claim->UID}}</a></li>
        <li class="active">Assign Authorization</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Assign Authorization</strong></h3>
	</div> <!-- / .page-header -->

	<div class="clearfix"></div>


	<div class="clearfix"></div>

	<form method="post" action="{{action('\App\Modules\Claims\Http\Controllers\ClaimsController@postSaveAuthorizationToClaim')}}" class="panel form-horizontal">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<input type="hidden" name="claim_id" value="{{$claim->id}}" />
		
		<div class="panel-body no-padding-hr">
	    	@if($errors->all())
		        <div class="alert alert-danger">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            @foreach($errors->all() as $error_msg)
		        
		                <span>
		                 {{ $error_msg }}
		                </span><br/>
		            @endforeach
		        </div>
		    @endif

			<div class="col-md-12">
				<br />
				<div class="form-group no-margin-hr panel-padding-h">
					<label class="control-label col-sm-4">Authorization UID</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="authorization_uid" id="authorization_uid" value="{{old('authorization_uid')}}" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="text-center">
					<button class="btn btn-success btn-labeled"><span class="btn-label icon fa fa-search"></span>&nbsp;Search</button>
				</div>
			</div>
		</div>
	</form>
@stop