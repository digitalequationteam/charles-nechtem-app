@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li><a href="/claims">Claims</a></li>
        <li class="active">{{$claim->UID}}</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>{{$claim->UID}}</strong></h3>
	</div> <!-- / .page-header -->

    @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_edit"))
        <div class="pull-right" style="padding-left: 10px;">
            <a class="btn btn-success m-t-10" href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getEditClaim', array($claim->id)) }}"><i class="fa fa-pencil"></i> Edit Claim</a>
        </div>

        @if ($claim->status != "rejected")
            @if ($claim->status == "pending")
                <div class="pull-right" style="padding-left: 10px;">
                    <a class="btn btn-warning m-t-10" href="javascript: void(0);" onclick="changeStatus('on_hold');"><i class="fa fa-pencil"></i> On Hold</a>
                </div>
            @endif

            @if ($claim->status == "pending" || $claim->status == "on_hold")
                <div class="pull-right" style="padding-left: 10px;">
                    <a class="btn btn-warning m-t-10" href="javascript: void(0);" onclick="changeStatus('rejected');"><i class="fa fa-pencil"></i> Reject</a>
                </div>
            @endif

            @if (!$claim->authorization_page_uid)
                <div class="pull-right" style="padding-left: 10px;">
                    <a class="btn btn-default m-t-10" href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getAssignAuthorizationToClaim', array($claim->id)) }}"><i class="fa fa-arrow-right"></i> Assign Authorization</a>
                </div>
            @endif
        @endif
    @endif

	<div class="clearfix"></div>

    <div class="panel fade active in" id="profile">
        <div class="panel-body no-padding-hr">
            <div class="row p-20">
                <div class="col-md-6">
                    <h3 class="m-t-0">Enrollee</h3>
                    <strong>First Name: </strong>{{$claim->getEnrolleeData()->enrollee->first_name}}<br>
                    <strong>Last Name: </strong>{{$claim->getEnrolleeData()->enrollee->last_name}}<br>
                    <strong>Gender: </strong>{{$claim->getEnrolleeData()->enrollee->gender}}<br>
                    <strong>Cell Phone: </strong>{{$claim->getEnrolleeData()->enrollee->cell_phone}}<br>
                    <strong>Home Phone: </strong>{{$claim->getEnrolleeData()->enrollee->home_phone}}<br>
                    <strong>Work Phone: </strong>{{$claim->getEnrolleeData()->enrollee->work_phone}}<br>
                    <strong>Birthday: </strong>{{$claim->getEnrolleeData()->enrollee->birthday}}<br>
                </div>
                <div class="col-md-6">
                    <h3 class="m-t-0">&nbsp;</h3>
                    <strong>Plan: </strong>{{@$claim->getEnrolleeData()->employee->plan}}<br>
                    <strong>Insurance Network: </strong>{{@$claim->getEnrolleeData()->employee->insurance_network}}<br>
                    <strong>Insurance Type: </strong>{{$claim->getEnrolleeData()->employee->insurance_type}}<br>
                    <strong>Company: </strong>{{$claim->getEnrolleeData()->employee->client}}

                    <div class="address-info">
                        <strong>Address</strong>
                            {!! $claim->getEnrolleeData()->enrollee->address !!}
                      </div>
                </div>
            </div>

            <div class="div-separator"></div>

            <div class="row p-20">
                <div class="col-md-12">
                    <h3 class="m-t-0">Provider</h3>
                    <strong>Provider Name: </strong>{{$claim->getProviderData()["name"]}}<br>
                    <strong>POS Code: </strong>{{$claim->getProviderData()["pos_code"]}}

                    <div class="address-info">
                        <strong>Address</strong>
                            {!! $claim->getProviderData()["address"] !!}
                      </div>
                </div>
            </div>

            <div class="div-separator"></div>

            <div class="row p-20">
                <div class="col-md-6">
                    <h3 class="m-t-0">Claim Info</h3>
                    <strong>Claim Type: </strong>{{$claim->type}}<br>
                    <strong>Accept Assignment: </strong>{{($claim->accept_assignment) ? "Yes" : "No"}}
                </div>
                <div class="col-md-6">
                    <h3 class="m-t-0">&nbsp;</h3>
                    <strong>Status: </strong>{{ucwords(str_replace("_", " ", $claim->status))}}
                </div>
            </div>

        </div>
    </div>

    <div class="panel fade active in">
        <div class="panel-body no-padding-hr">
            <div class="row p-20">
                <div class="col-md-12">
                    <div class="pull-right">
                        @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_services"))
                            <a class="btn btn-success" href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getAddService', array($claim->id)) }}"><i class="fa fa-plus"></i> Add Service</a>
                        @endif
                    </div>
                    <h3 class="m-t-0">Services</h3>                    
                </div>
            </div>

            @foreach ($claim->services()->get() as $service)
                <div class="apage-wrapper">
                    <div class="row p-20">
                        <div class="col-md-6">
                            @if (\Auth::user()->hasPermission(\Auth::user()->Owner()->type."_claims_services"))
                                <a class="btn btn-success btn-xs tooltips" href="{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@getEditService', array($service->id)) }}" data-original-title="Edit Service"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-danger btn-xs tooltips delete-apage" data-id="{{$service->id}}" href="#" data-original-title="Delete Service"><i class="fa fa-times"></i></a>
                            @endif
                            <br>

                            <strong>Diagnosis: </strong>{{$service->icd_code}}<br>
                            <strong>Service: </strong>{{$service->service}}<br>
                            <strong>Place of Service: </strong>{{$service->service_plan}}<br>
                            <strong>Date Period: </strong>{{date("m/d/Y", strtotime($service->start_date))}} - {{date("m/d/Y", strtotime($service->end_date))}}<br>
                        </div>
                        <div class="col-md-6">
                            <h3 class="m-t-0">&nbsp;</h3>
                            <strong>Charge: </strong>${{$service->charge}}<br>
                            <strong>Units: </strong>{{$service->units}}<br>
                        </div>
                    </div>
                    <div class="div-separator"></div>
                </div>
            @endforeach
        </div>
    </div>

    <?php
    $comments_owner = $claim;
    $owner_type = "claim";
    ?>

    @include('comments::widgets.comments_section')

    <script type="text/javascript">
        $(document).ready(function() {
            $('.delete-apage').live('click', function (e) {
                e.preventDefault();
                if (confirm("Are you sure to delete this authorization page?") == false) {
                    return;
                }
                var that = $(this);
                var ids = [that.attr("data-id")];

                that.parents(".apage-wrapper").slideUp("fast", function() {
                    that.parents(".apage-wrapper").remove();
                });

                $.post("{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@postDeleteServices') }}", {
                    ids: ids
                }, function(response) {}, "json");
            });
        });

        function changeStatus(status) {
            var message = (status == "on_hold") ? "Are you sure you want to put this claim on hold?" : "Are you sure you want to reject this claim?";

            if (confirm(message)) {
                $.post("{{ action('\App\Modules\Claims\Http\Controllers\ClaimsController@postChangeClaimStatus') }}", {
                    status: status,
                    id: {{$claim->id}}
                }, function(response) {
                    window.location = window.location.href;
                }, "json");
            }
        }
    </script>
@stop