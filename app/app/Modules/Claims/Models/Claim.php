<?php

namespace App\Modules\Claims\Models;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'claims';

    public function comments() {
        return $this->hasMany("\Comment", "owner_id")->where("type", "claim");
    }

    public function services() {
        return $this->hasMany("\ClaimService");
    }

    public function getEnrolleeData() {
    	return json_decode($this->enrollee_data);
    }

    public function getProviderData() {
    	return json_decode($this->provider_data, true);
    }
}