<?php

namespace App\Modules\Claims\Models;

use Illuminate\Database\Eloquent\Model;

class ClaimService extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'claim_services';

    public function claim() {
        return $this->belongsTo("\Claim");
    }
}