<?php

namespace App\Modules\Frontend\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class EditController extends Controller
{
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

    public function getBasic()
    {
        $this->owner->checkWebsiteCreated();
        return view('frontend::edit.basic', array("frontend" => true));
    }

    public function getAdvanced()
    {
        $this->owner->checkWebsiteCreated();
        return view('frontend::edit.advanced', array("frontend" => true));
    }
}