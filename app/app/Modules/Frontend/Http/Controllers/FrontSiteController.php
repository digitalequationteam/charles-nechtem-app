<?php

namespace App\Modules\Frontend\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use Input;
use Illuminate\Support\Facades\Redirect;

class FrontSiteController extends Controller
{
    public function getIndex($page = "index.php")
    {
    	switch (FRONT_FOLDER) {
    		case 'global':
    			$user = \Common::getAdminUser();
    		break;
    		
    		default:
    			$user = \User::where('username', FRONT_FOLDER)->get()->first();
    		break;
    	}

        $user->checkWebsiteCreated();

        if (@$user->getSetting("front_site.access") && !\Common::checkIPValid($_SERVER["REMOTE_ADDR"], explode("\r\n", @$user->getSetting("front_site.ips.allowed"))) && $page != "prices.html")
                return view('errors.403');

        if (!@$user->getSetting("front_site.access") && \Common::checkIPValid($_SERVER["REMOTE_ADDR"], explode("\r\n", @$user->getSetting("front_site.ips.excluded"))) && $page != "prices.html")
                return view('errors.403');

        $exploded = explode("/app", __FILE__);

        $folder = $exploded[0]."/public/static_front_sites/".FRONT_FOLDER."/";

        if ($page == "prices.html") {
            View::addNamespace('static_front_sites', $exploded[0]."/public/static_front_sites/");

            $page_without_html = str_replace(".html", "", $page);

            //return view('static_front_sites.'.FRONT_FOLDER.'.'.$page_without_html, array(
            return view('static_front_sites::'.FRONT_FOLDER.'.'.$page_without_html, array(
                "FRONT_FOLDER" => FRONT_FOLDER,
                "user" => $user,
                "LOGO" => (!empty($user->logo)) ? '<img src="'.URL::to("/uploads/", $user->logo).'" />' : '',
                "products" => $user->products()->where("is_free", 0)->get()
            ));
        }
        else {
            /* Short and sweet */
            //require('./wp-blog-header.php');
            //require($folder.'/wp-blog-header.php');

            if ($page == "/")
                $page = "index.php";

            $exploded = explode(".", $page);

            if (count($exploded) == 1) {
                $page = "index.php";
            }

            $page = $folder.$page;
            if (!empty($page2))
                $page = $page."/".$page2;

            if (!file_exists($page)) {
                $user->checkWebsiteCreated();
            }

            ob_start();
            require_once($page);
            $content = ob_get_contents();
            ob_end_clean();
            $content = str_replace('<a href="http://'.\Config::get("app.domain").'/static_front_sites/global', '<a href="http://'.\Config::get("app.domain"), $content);
            echo $content;
            exit();
            $content = file_get_contents($folder.$page);
            var_dump($content);
            $content = str_replace("{FRONT_FOLDER}", FRONT_FOLDER, $content);
            $content = str_replace("{SUPPORT_EMAIL}", $user->getSupportEmail(), $content);
            $content = str_replace("{LOGIN_PAGE}", $user->getLoginPage(), $content);
            $content = str_replace("{REGISTER_PAGE}", $user->getRegisterPage(), $content);

            if (!empty($user->logo))
                $content = str_replace("{LOGO}", '<img src="'.URL::to("/uploads/", $user->logo).'" />', $content);
            else
                $content = str_replace("{LOGO}", '', $content);

            $content = str_replace("{ADDRESS}", $user->getLocation(), $content);
            $content = str_replace("{TOKEN}", csrf_token(), $content);
            $content = str_replace("{USER_ID}", $user->id, $content);
            $content = str_replace("{COMPANY_NAME}", $user->getReturnCompanyName(), $content);
            $content = str_replace("{SITE_URL}", $user->getSiteURL(), $content);

            echo $content;
        }
    }
}