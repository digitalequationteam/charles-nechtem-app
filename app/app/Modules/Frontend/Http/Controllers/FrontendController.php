<?php

namespace App\Modules\Frontend\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use Input;
use Illuminate\Support\Facades\Redirect;

class FrontendController extends Controller
{
	public function __construct() {
		$this->beforeFilter('auth');

        if (\Auth::check()) {
            $this->owner = \Auth::user()->Owner();
            $this->me = \Auth::user();
        }
	}

	public function getFrontEndDefault()
    {
        return view('frontend::default', array("frontend" => true));
    }

    public function getFrontEndBasic()
    {
        $this->owner->checkWebsiteCreated();
        return view('frontend::edit.basic', array("frontend" => true));
    }

    public function getFrontEndAdvanced()
    {
        $this->owner->checkWebsiteCreated();
        return view('frontend::edit.advanced', array("frontend" => true));
    }

    public function getReset()
    {
        $this->owner->checkWebsiteCreated(true);

        \SecurityLog::addNew("reset_frontend", \Auth::user()->getName()." reset the frontend site.");

        return redirect("/frontend/reset-done");
    }

    public function getBackup()
    {
        return view('frontend::backup', array("frontend" => true));
    }

    public function getResetDone()
    {
        return view('frontend::reset-done', array("frontend" => true));
    }

    public function getInfo()
    {
        return view('frontend::info', array("frontend" => true));
    }
}