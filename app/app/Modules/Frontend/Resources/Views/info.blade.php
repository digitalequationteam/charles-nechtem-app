@extends('layouts.main')
@section('content')

<?php
use App\Common;
?>
    <div class="page-title pull-left">
        <h3 class="pull-left"><strong>Frontend Site Info</strong></h3>
    </div> <!-- / .page-header -->

    <div class="pull-right">

    </div>

    <div class="clearfix"></div>

    <style type="text/css">
        #profile-tabs li {
            margin-bottom: 10px;
        }
    </style>

    @if(Session::has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>
                {{ Session::get('message') }}
            </span>
        </div>
    @endif
    @if($errors->all())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach($errors->all() as $error_msg)
        
                <span>
                 {{ $error_msg }}
                </span><br/>
            @endforeach
        </div>
    @endif

    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">View Your Frontend Site Info</span>
        </div>

        <div class="panel-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">External Home Page URL:</label>
                            <br />
                            <a href="{{Auth::user()->Owner()->getWordpressData()['external_home_url']}}" target="external_home_url">{{Auth::user()->Owner()->getWordpressData()["external_home_url"]}}</a>
                        </div>
                        <div class="form-group">
                            <label class="control-label">External Admin Login URL:</label>
                            <br />
                            <a href="{{Auth::user()->Owner()->getWordpressData()['external_admin_url']}}/wp-admin" target="external_admin_url">{{Auth::user()->Owner()->getWordpressData()["external_admin_url"]}}/wp-admin</a>
                        </div>
                    </div>
                    @if (Auth::user()->hasPermission("admin_frontend_edit_basic") || Auth::user()->hasPermission("admin_frontend_front-end-advanced"))
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Username:</label>
                                <br />
                                {{Auth::user()->Owner()->getWordpressData()["username"]}}
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password:</label>
                                <br />
                                {{Auth::user()->Owner()->getWordpressData()["password"]}}
                            </div>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@stop