@extends('layouts.main')
@section('content')
	<script type="text/javascript">
		document.domain = '{{MAIN_DOMAIN}}';
	</script>

	<iframe id="myIframe" onload="$(this).show(); $('#iframe_loading').hide();" src="http://{{Config::get('app.domain').'/static_front_sites/'.Auth::user()->Owner()->getSiteFolder().'/wp-admin'}}" style="width: calc(100% + 40px); border: 0px; padding: 0px; margin: 0px; display: none; margin-left: -20px; margin-top: -10px;"></iframe>

	<div id="iframe_loading" style="width: 100%; height: 100%; text-align: center; min-height: 600px; background-image: url(https://www.hereofamily.com/wp-content/themes/HereO/images/ajax-loader.gif); background-repeat: no-repeat; background-position: 50% 50%;"></div>

	<script type="text/javascript">
		doThis();

		$(window).resize(function() {
			doThis();
		});

		function resizeOnIframePages() {
			var padding = ($("#sidebar").is(":visible")) ? 21 : 28;
			$("#main-content").css('padding-top', padding + 'px');
		}
		
		function doThis() {
			resizeOnIframePages();

			var new_height = $(window).outerHeight() - $(".navbar-header").outerHeight();
			$("#myIframe").css('height', new_height + "px");
			$("#iframe_loading").css('height', new_height + "px");

			if (!$("body").hasClass("mmc")) $("#main-menu-toggle").click();
			
			setTimeout(function() {
				if ($("#menu-large").length == 0) {
					$('a.sidebar-toggle').click();
					$('a.sidebar-toggle').click();

					setTimeout(function() {
						$("body").removeClass("breakpoint-1200");
						$("body").removeClass("breakpoint-768");
						$("body").removeClass("breakpoint-480");

						if ($("html").hasClass("sidebar-thin")) {
							$("body").addClass("breakpoint-480");
						}
						else if ($("html").hasClass("sidebar-medium")) {
							$("body").addClass("breakpoint-768");
						}
						else if ($("html").hasClass("sidebar-large")) {
							$("body").addClass("breakpoint-1200");
						}
					}, 1000);
				}
			}, 200);
		}
	</script>

	<style type="text/css">
		html, body {
			overflow: hidden !important;
		}
		#content-wrapper {
			padding-left: 0px;
			padding-top: 46px;
			padding-bottom: 0px;
			padding-right: 0px;
			margin: 0px;
		}

		<?php
		if (\Auth::user()->getSetting('color'))
		    $userToCheck = \Auth::user();
		else
		    $userToCheck = \Auth::user()->Owner();
		?>

		::-webkit-scrollbar {width:10px;}  
		::-webkit-scrollbar-track {background-color: #eaeaea;border-left: 1px solid #c1c1c1;}
		::-webkit-scrollbar-thumb {background-color: #c1c1c1;}  
		::-webkit-scrollbar-thumb:hover { background-color: #aaa; }  
		::-webkit-scrollbar-track {border-radius: 0;box-shadow: none;border: 0;}
		::-webkit-scrollbar-thumb {border-radius: 0;box-shadow: none;border: 0;} 
	</style>

	<script type="text/javascript">
		var auth_type = 'advanced';
		var spewpp = 'admin.php?page=pb_backupbuddy_backup';
	</script>
@stop