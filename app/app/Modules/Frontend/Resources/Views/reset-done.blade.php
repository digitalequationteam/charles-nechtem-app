@extends('layouts.main')
@section('content')
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i></a></li>
        <li class="active">Reset</li>
    </ol>

	<div class="page-title pull-left">
		<h3 class="pull-left"><strong>Reset Done</strong></h3>
	</div> <!-- / .page-header -->

	<!--
	<div class="pull-right">
		<a href="{{ action('\App\Modules\InterfaceMenu\Http\Controllers\InterfaceMenuController@getMenu') }}" class="btn btn-primary m-t-10" style="width: 100%;"><span class="btn-label icon fa fa-arrow-left"></span> Back</a>
	</div>
	-->

	<div class="clearfix"></div>
		
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body no-padding-hr">
		        <div class="alert alert-success">
		            <span>
		                Frontend site has been successfully reset!
		            </span>
			    </div>

			    <div class="text-center">
			    	<a href="{{Auth::user()->Owner()->getSiteURL()}}" target="view-front-site">View Site</a>
			   	</div>
			</div>
		</div>
	</div>
@stop