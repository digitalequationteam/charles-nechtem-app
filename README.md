# Charles Nechtem [App]

This README describes the steps needed to run the **Charles Nechtem** app

## Pre-requisites

### 1. System requirements

- Node.js
- Composer

##### For Capistrano deployment:

- Ruby >= 2.3
- Bundler >= 1.15

### 2. Cloning

The standard way to install this project is to [clone and setup the Docker environment](https://bitbucket.org/digitalequationteam/charles-nechtem-docker/src/master/#markdown-header-3-cloning-this-repository-and-attached-projects).

### 3. Setup

**1**. Copy and edit the environment file:

```bash
$ cd app
$ cp .env.example .env
$ nano .env
```

**2**. Log into MySQL locally and create a project database:

```
DB_HOST: localhost
DB_PORT: 33061
DB_USER: root
DB_PASSWORD: root

Create DB `charles-nechtem` (default .env name)
```

**3**. __[Open a shell to the App Docker instance](https://bitbucket.org/digitalequationteam/charles-nechtem-docker/src/master/#markdown-header-2-open-a-shell-on-a-container)__ and run:

```bash
cd /code/charles-nechtem-app

# 1. Generate an initial encryption key
[Docker]$ php artisan key:generate

# 2. Install NPM dependencies
[Docker]$ npm install

# 3. Install Composer dependencies
[Docker]$ composer install

# 4. Run Laravel DB migrations
[Docker]$ php artisan migrate

# 5. Run Laravel DB seeders
[Docker]$ php artisan db:seed

# If you have a unix-like terminal then steps 2, 3 and 4 have a shortcut:
[Docker]$ sh setup.sh

### The following should be run outside of Docker context:

# Build project assets once
$ npm run dev

# Build project assets continuously
$ npm run watch
```

## Quick references

## Deployment [Capistrano]

```bash
# Install Bundler dependencies
$ bundle install

# Deploying to `staging`
$ bundle exec cap staging deploy
```

> Note: You can also run individual Capistrano tasks. To get a list of available tasks, run `cap -T` from your terminal.

> Example:
>
> - `$ bundle exec cap staging composer:install`
>
> - `$ bundle exec cap staging laravel:migrate`
